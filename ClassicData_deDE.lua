if GetLocale() ~= "deDE" then return end
ClassicData = {
    version = "de1",
    ["Große Unsichtbarkeit entdecken"] = {
        ["WARLOCK"] = 11743,
    },
    ["Rakete, Weiß"] = {
        ["Pats Feuerwerkstyp - WEISS"] = 26348,
    },
    ["Gedanken vergiften"] = {
        ["Sklavenjäger des Regalschwarms"] = 19469,
    },
    ["Schwacher Frostblitz"] = {
        ["Novize der Frostmane"] = 6949,
    },
    ["Gedankenbenebelndes Gift"] = {
        ["ROGUE"] = 5763,
    },
    ["Sayges dunkles Schicksal der Ausdauer"] = {
        ["Sayge"] = 23737,
    },
    ["Jadeschlangenklinge"] = {
        Professions = 3493,
    },
    ["Bronzener Streitkolben"] = {
        Professions = 2740,
    },
    ["Totem des Erdstoßes"] = {
        ["SHAMAN"] = 8143,
    },
    ["Rauer bronzener Kürass"] = {
        Professions = 2670,
    },
    ["Mächtiger Wuttrank"] = {
        Professions = 17552,
    },
    ["Blutgeheul"] = {
        ["Moonrage-Blutheuler"] = 3264,
        ["Serena Blutfeder"] = 3264,
        ["Geisterpfotenheuler"] = 3264,
        ["Blutschrei der Pirscher"] = 3264,
        ["Klippenspringer"] = 3264,
    },
    ["Streitross beschwören"] = {
        ["PALADIN"] = 23214,
    },
    ["Stein der Weisen"] = {
        Professions = 11459,
    },
    ["Rüstung schwächen"] = {
        ["ROGUE"] = 11198,
    },
    ["Stärke der Frostmane"] = {
        ["Grik'nir der Kalte"] = 6957,
    },
    ["Marduk den Schwarzen beschwören"] = {
        ["Marduk der Schwarze"] = 18650,
        ["Captain Redpath"] = 18650,
    },
    ["Schraubenschlüssel werfen"] = {
        ["Aussätziger Techniker"] = 13398,
        ["Aussätziger Maschinenschmied"] = 13398,
    },
    ["Mal der Flammen"] = {
        ["Grunzer der Feuerbrand"] = 15128,
        ["Legionär der Feuerbrand"] = 15128,
        ["Dunkelwirker der Feuerbrand"] = 15128,
        ["Herbeirufer der Feuerbrand"] = 15128,
        ["Schreckenswirker der Feuerbrand"] = 15128,
        ["Pyromant der Feuerbrand"] = 15128,
    },
    ["Schleichende Verdammnis"] = {
        ["Schleichende Verdammnis"] = 23589,
        ["Nelson der Nette"] = 23589,
    },
    ["Seelenzerreißung"] = {
        ["Trauerschwinge"] = 3405,
    },
    ["Elizas Wache beschwören"] = {
        ["Elizas Wache"] = 3107,
        ["Eliza"] = 3107,
    },
    ["Griff der Natur"] = {
        ["DRUID"] = 17329,
    },
    ["Göttlicher Willen"] = {
        ["PRIEST"] = 27841,
    },
    ["Myzrael-Erdbeben"] = {
        ["Myzrael"] = 4938,
    },
    ["Großer Segen der Weisheit"] = {
        ["PALADIN"] = 25918,
    },
    ["Edelsteinbesetzter Ledergürtel"] = {
        Professions = 3778,
    },
    ["Schild - Geringe Willenskraft"] = {
        Professions = 13485,
    },
    ["Oranges Kampfhemd"] = {
        Professions = 12064,
    },
    ["Graue Wollrobe"] = {
        Professions = 2403,
    },
    ["Grüne Drachenschuppengamaschen"] = {
        Professions = 19060,
    },
    ["Thekal Trigger"] = {
        ["Zelot Lor'Khan"] = 24172,
        ["Zelot Zath"] = 24172,
    },
    ["Versklaven"] = {
        ["Kerkermeister Eston"] = 3442,
    },
    ["Große Bronzebombe"] = {
        Professions = 3950,
    },
    ["Feuerblitz"] = {
        Pet = 11763,
    },
    ["Bronzene Schultern mit Versilberung"] = {
        Professions = 3330,
    },
    ["Gnomentodesstrahl"] = {
        Professions = 12759,
    },
    ["Weiße Banditenmaske"] = {
        Professions = 12059,
    },
    ["Mittleres Rüstungsset"] = {
        Professions = 2165,
    },
    ["Segen von Shahram"] = {
        ["Shahram"] = 16599,
    },
    ["Schweres Rüstungsset"] = {
        Professions = 3780,
    },
    ["Satanskreatur-Furor"] = {
        ["Skelettsatanskreatur"] = 3416,
    },
    ["Uralte Verzweiflung"] = {
        ["Uralter Kernhund"] = 19369,
    },
    ["Stich des Flügeldrachen"] = {
        ["HUNTER"] = 24133,
    },
    ["Blutläufer rufen"] = {
        ["Blutläufer"] = 18262,
        ["Jägerin Radley"] = 18262,
    },
    ["Schwerer Gewichtsstein"] = {
        Professions = 3117,
    },
    ["Eiszillioskop herstellen"] = {
        ["Tinkee Steamboil"] = 16029,
    },
    ["Verbrennung"] = {
        ["MAGE"] = 11129,
    },
    ["Trank trinken"] = {
        ["Bartleby"] = 9956,
    },
    ["Wundgift"] = {
        ["ROGUE"] = 13220,
    },
    ["Schwarze Magiestoffweste"] = {
        Professions = 12048,
    },
    ["Hochexplosive Bombe"] = {
        Professions = 12619,
    },
    ["Nachwachsen"] = {
        ["DRUID"] = 9858,
        ["Moonkinorakel"] = 16561,
    },
    ["Wütende Erinnerungen"] = {
        ["Mor'Ladim"] = 3547,
    },
    ["Feuersbrunst"] = {
        ["WARLOCK"] = 18932,
    },
    ["Grünes Festtagsshemd"] = {
        Professions = 21945,
    },
    ["Umhang - Verteidigung"] = {
        Professions = 13635,
    },
    ["Bombe"] = {
        ["Boss Copperplug"] = 9143,
        ["Demolierer der Dunkeleisenzwerge"] = 8858,
        ["Bombenschütze der Dunkeleisenzwerge"] = 8858,
        ["Geologe der Dunkeleisenzwerge"] = 8858,
        ["Bastionstechniker"] = 8858,
    },
    ["Veredelter Mithrilzylinder"] = {
        Professions = 11454,
    },
    ["Totemkorb der Fäulnisklauen beschwören"] = {
        ["Häuptling Murgut"] = 20818,
    },
    ["Arantir's Zorn"] = {
        ["Arantir"] = 9741,
    },
    ["Ritual der Verdammnis"] = {
        ["WARLOCK"] = 18540,
    },
    ["Verstohlenheit"] = {
        ["ROGUE"] = 1787,
        ["Colonel Kurzen"] = 8822,
    },
    ["Sturmblitz"] = {
        ["König Magni Bronzebeard"] = 20685,
        ["Vanndar Stormpike"] = 19136,
    },
    ["Aspekt des Geparden"] = {
        ["HUNTER"] = 5118,
    },
    ["Fluch der Schatten"] = {
        ["WARLOCK"] = 17937,
    },
    ["Lavaexplosion G beschwören"] = {
        ["Kernratte"] = 21905,
    },
    ["Schild - Ausdauer"] = {
        Professions = 13817,
    },
    ["Diebeswerkzeug-Regal herbeizaubern"] = {
        ["Wrenix' Elektrodingsda-Apparat"] = 9949,
    },
    ["Elixier der Dämonenentdeckung"] = {
        Professions = 11478,
    },
    ["Drachenfluch"] = {
        ["Legionär der Pechschwingen"] = 23967,
        ["Drachenbrut der Todeskrallen"] = 23967,
        ["Gardist der Pechschwingen"] = 23967,
    },
    ["Staubwolke"] = {
        ["Schreitergelegemutter"] = 7272,
        ["Alter Ebenenschreiter"] = 7272,
        ["Zwangsarbeiter der Kolkar"] = 7272,
    },
    ["Brust - Großes Mana"] = {
        Professions = 13663,
    },
    ["Toxinspucke"] = {
        ["Kluftmoosgiftspucker"] = 7951,
        ["Dunkelzahngiftspucker"] = 7951,
        ["Deviatgiftschwinge"] = 7951,
    },
    ["Verfallene Stärke"] = {
        ["Lepragnom"] = 6951,
        ["Verderbter Brandungskriecher"] = 6951,
    },
    ["Elektrifiziertes Netz"] = {
        ["Mechanischer Wächter"] = 11820,
    },
    ["Donnerstampfer"] = {
        Pet = 26188,
    },
    ["Rache der Knarzklauen"] = {
        ["Augur der Knarzklauen"] = 5628,
        ["Pfadfinder der Knarzklauen"] = 5628,
        ["Rächer der Knarzklauen"] = 5628,
        ["Totemiker der Knarzklauen"] = 5628,
    },
    ["Kriegsdonner"] = {
        Racial = 20549,
        ["Klippengänger"] = 11876,
        ["Wandernder Beschützer"] = 45,
        ["Harb Faulberg"] = 45,
    },
    ["Blutsegelbukaniergefährte"] = {
        ["Gefährte der Defias"] = 5172,
        ["Pirat der Defias"] = 5172,
    },
    ["Goblinscher Drachenwerfer"] = {
        Professions = 12908,
    },
    ["Proudmoores Verteidigung"] = {
        ["Erzmagier Tervosh"] = 7120,
    },
    ["Fläschchen mit chromatischem Widerstand"] = {
        Professions = 17638,
    },
    ["Chromatisches Chaos"] = {
        ["Lord Victor Nefarius"] = 16337,
    },
    ["Cenarischer Ranzen"] = {
        Professions = 27725,
    },
    ["Hühnerfuror"] = {
        ["Kampfhuhn"] = 13168,
    },
    ["Schatzsucher"] = {
        Racial = 2481,
    },
    ["Kahlen Worg rufen"] = {
        ["Kahler Worg"] = 7487,
    },
    ["Mithrilschrapnellbombe"] = {
        Professions = 12603,
    },
    ["Segen der Weisheit"] = {
        ["PALADIN"] = 25290,
    },
    ["Rolle herstellen"] = {
        ["Collin Mauren"] = 6671,
    },
    ["Bequemer Lederhut"] = {
        Professions = 10490,
    },
    ["Wolfwache-Worg beschwören"] = {
        ["Wolfswachenworg"] = 7107,
        ["Shadowfang-Wolfswache"] = 7107,
    },
    ["Inferno Effekt"] = {
        ["Höllenbestie"] = 22703,
        ["Verdorbene Höllenbestie"] = 22703,
    },
    ["Schneide des Winters"] = {
        Professions = 21913,
    },
    ["Umhang des Hügelwächters"] = {
        Professions = 3760,
    },
    ["Aura des Frostwiderstands"] = {
        ["PALADIN"] = 19898,
    },
    ["Robuster Gewichtsstein"] = {
        Professions = 9921,
    },
    ["Pläne: Veredelter Mithrilzylinder"] = {
        Professions = 12895,
    },
    ["Feenfeuer (Tiergestalt)"] = {
        ["DRUID"] = 17392,
    },
    ["Blaue Leinenweste"] = {
        Professions = 7630,
    },
    ["Überragender Heiltrank"] = {
        Professions = 11457,
    },
    ["Brust - Überragendes Mana"] = {
        Professions = 13917,
    },
    ["Instabiler Auslöser"] = {
        Professions = 12591,
    },
    ["Schlachtkräher"] = {
        ["Kampfhuhn"] = 23060,
    },
    ["Verletzender Schlag"] = {
        ["Schläger der Gordok"] = 22572,
    },
    ["Dunkeleisenschultern"] = {
        Professions = 15295,
    },
    ["Schattenbrand"] = {
        ["WARLOCK"] = 18871,
    },
    ["Inferno"] = {
        ["WARLOCK"] = 1122,
    },
    ["Get Gosssip, Test"] = {
        ["Stadtwache von Stormwind"] = 26683,
        ["Stadtpatrolleur von Stormwind"] = 26683,
        ["Behüter von Thunder Bluff"] = 26683,
        ["Grunzer von Orgrimmar"] = 26683,
        ["Arnold Leland"] = 26683,
        ["Wache von Ironforge"] = 26683,
        ["Grunzer Korf"] = 26683,
        ["Grunzer Bek'rah"] = 26683,
    },
    ["Elunes Anmut"] = {
        ["PRIEST"] = 19293,
    },
    ["Leiden"] = {
        Pet = 17752,
    },
    ["Theurgiker beschwören"] = {
        ["Theurgiker von Dalaran"] = 3658,
        ["Beschwörer von Dalaran"] = 3658,
    },
    ["Armschiene - Große Stärke"] = {
        Professions = 13939,
    },
    ["Magiestofftasche"] = {
        Professions = 12065,
    },
    ["Aspekt des Affen"] = {
        ["HUNTER"] = 13163,
    },
    ["Arkanfokus"] = {
        ["Spektraler Lehrmeister"] = 17633,
    },
    ["Mehrfach-Schuss"] = {
        ["Fürstin Sylvanas Windrunner"] = 20735,
    },
    ["Geringer Manatrank"] = {
        Professions = 3173,
    },
    ["Silberrute"] = {
        Professions = 7818,
    },
    ["Kupfermodulator"] = {
        Professions = 3926,
    },
    ["Geist der Ahnen"] = {
        ["SHAMAN"] = 20777,
    },
    ["Elixier der schwachen Seelenstärke"] = {
        Professions = 2334,
    },
    ["Erlösung"] = {
        ["PALADIN"] = 20773,
    },
    ["Phönixhandschuhe"] = {
        Professions = 3868,
    },
    ["Hammertoes Opfer"] = {
        ["Hammertoe Grez"] = 4984,
    },
    ["Konzentration"] = {
        ["ROGUE"] = 14183,
    },
    ["Blutfluch"] = {
        ["Botschafter Malcin"] = 12279,
        ["Irrsinniger Ghul"] = 8282,
        ["Blut von Agammagan"] = 8282,
        ["Hochinquisitor Fairbanks"] = 8282,
    },
    ["Runenverzierte Lederarmschienen"] = {
        Professions = 19065,
    },
    ["Kodobalgtasche"] = {
        Professions = 5244,
    },
    ["Fleisch verzehren"] = {
        ["Knirscher"] = 3393,
        ["Hochlandfleischpirscher"] = 3393,
    },
    ["Raue Kupferweste"] = {
        Professions = 12260,
    },
    ["Umhang - Geringer Feuerwiderstand"] = {
        Professions = 7861,
    },
    ["Dornen-Aura"] = {
        ["Speerbalg von Razorfen"] = 8148,
        ["Traumwandlerwyrmkin"] = 7966,
        ["Dornenwirker von Razorfen"] = 7966,
    },
    ["Toben"] = {
        ["Agathelos der Tobende"] = 8285,
    },
    ["Zorn"] = {
        ["DRUID"] = 9912,
        ["Pfadfinder der Knarzklauen"] = 9739,
        ["Gerber der Bleichmähnen"] = 9739,
        ["Abtrünniger Druide"] = 9739,
        ["Cenarischer Botaniker"] = 9739,
        ["Tyranis Malem"] = 9739,
        ["Mystiker der Knarzklauen"] = 9739,
        ["Moonkinorakel"] = 9739,
    },
    ["Spalten"] = {
        ["WARRIOR"] = 20569,
        ["Vorarbeiter der Weißstoppel"] = 15663,
        ["Irondeep-Wache"] = 15663,
        ["Kommandant Randolph"] = 15663,
        ["Kommandant Dardosh"] = 15663,
        ["Kommandant Malgor"] = 15663,
        ["Kommandant Mulfort"] = 15663,
        ["Kommandant Louis Philips"] = 15663,
        ["Kommandant Mortimer"] = 15663,
        ["Kommandant Duffy"] = 15663,
        ["Kommandant Karl Philips"] = 15663,
        ["Erfahrene Irondeep-Wache"] = 15663,
        ["Irondeep-Wachenveteran"] = 15663,
        ["Irondeep-Wachenchampion"] = 15663,
        ["Kommandant der Stormpike-Widderreiter"] = 15663,
        ["Jotek"] = 15663,
        ["Siechender Häscher"] = 11427,
        ["Teremus der Verschlinger"] = 11427,
        ["Grüne Bannschuppe"] = 15496,
        ["Kurzens Elitesoldat"] = 15496,
        ["Crushridge-Brandschatzer"] = 15496,
        ["Grunzer von Orgrimmar"] = 15496,
        ["Gestürzter Held"] = 15496,
        ["Häscher der Grimmtotem"] = 15496,
        ["Lord Kragaru"] = 15496,
        ["Elitesoldat der Kor'kron"] = 15496,
        ["Iceblood-Marshal"] = 15579,
        ["Turmstellungs-Marshal"] = 15579,
        ["Ostmarshal der Frostwolf"] = 15579,
        ["Westmarshal der Frostwolf"] = 15579,
        ["Dun Baldar Nord-Kriegsmeister"] = 15579,
        ["Dun Baldar Süd-Kriegsmeister"] = 15579,
        ["Icewing-Kriegsmeister"] = 15579,
        ["Stonehearth-Kriegsmeister"] = 15579,
        ["Westkriegsmeister der Frostwolf"] = 15579,
        ["Hochlord Bolvar Fordragon"] = 20684,
        ["Herod"] = 22540,
        ["Strashazmyrmidone"] = 15754,
        ["Töter von Hederine"] = 15754,
        ["Captain Galvangar"] = 15754,
        ["Goblin-Holzschnitzer"] = 5532,
        ["Captain Greenskin"] = 5532,
        ["Holzfäller der Venture Co."] = 5532,
        ["Skorpidhäscher"] = 5532,
        ["Flintauge"] = 5532,
    },
    ["Tintenschauer"] = {
        ["Marschentintenspucker"] = 9612,
    },
    ["Fester Skorpidhelm"] = {
        Professions = 10570,
    },
    ["Umhang - Großer Widerstand"] = {
        Professions = 20014,
    },
    ["Winterzwirntunika"] = {
        Professions = 18403,
    },
    ["Ilkruds Wächter"] = {
        ["Leerwandler"] = 6487,
        ["Ilkrud Magthrull"] = 6487,
    },
    ["Mondstofftasche"] = {
        Professions = 18445,
    },
    ["Nierenhieb"] = {
        ["ROGUE"] = 8643,
    },
    ["Schlafender Haudrauf"] = {
        ["Betrunkener Haudrauf"] = 26115,
    },
    ["Tödliches Gift II"] = {
        ["ROGUE"] = 2837,
    },
    ["Runenstoffgürtel"] = {
        Professions = 18402,
    },
    ["Sayges dunkles Schicksal der Stärke"] = {
        ["Sayge"] = 23735,
    },
    ["Aura des Schattenwiderstands"] = {
        ["PALADIN"] = 19896,
    },
    ["Gefertigtes robustes Geschoss"] = {
        Professions = 3947,
    },
    ["Geprägte Lederhose"] = {
        Professions = 3759,
    },
    ["Barbarische Eisenschultern"] = {
        Professions = 9811,
    },
    ["Saptasicht"] = {
        ["Tiev Mordune"] = 9735,
    },
    ["Prismatischen Drakoniden beschwören"] = {
        ["Prismatischer Drakonid"] = 22680,
        ["Black Drakonid Spawner"] = 22680,
        ["Red Drakonid Spawner"] = 22680,
        ["Green Drakonid Spawner"] = 22680,
        ["Bronze Drakonid Spawner"] = 22680,
        ["Blue Drakonid Spawner"] = 22680,
    },
    ["Bronzeröhre"] = {
        Professions = 3938,
    },
    ["Schwarze Drachenschuppenbrustplatte"] = {
        Professions = 19085,
    },
    ["Flammenpuffer-Totem"] = {
        ["Flammenpuffer-Totem"] = 15867,
        ["Hexendoktor der Gluthauer"] = 15867,
    },
    ["Totem der luftgleichen Anmut"] = {
        ["SHAMAN"] = 25359,
    },
    ["Sofort wirkendes Gift II"] = {
        ["ROGUE"] = 8687,
    },
    ["Yeh'kinyas Rolle herstellen"] = {
        ["Yeh'kinya"] = 12998,
    },
    ["Reitersoldaten wiederauferstehen lassen"] = {
        ["Demetria"] = 19721,
        ["Scharlachroter Reitersoldat"] = 19721,
    },
    ["Feste Skorpidhandschuhe"] = {
        Professions = 10542,
    },
    ["Flammenstoß"] = {
        ["MAGE"] = 10216,
        ["Feuerwirker der Splitterfäuste"] = 20296,
        ["Rufer der Defias"] = 11829,
        ["Seher der Burning Blade"] = 11829,
        ["Geomant der Borstennacken"] = 20794,
        ["Geomant der Grimmtotem"] = 20813,
        ["Singe"] = 12468,
        ["Delmanis der Verhasste"] = 7101,
    },
    ["Purpurrote Seidenschultern"] = {
        Professions = 8793,
    },
    ["Drohruf"] = {
        ["WARRIOR"] = 5246,
    },
    ["Loderflammenrapier"] = {
        Professions = 16978,
    },
    ["Diskombobulatorstrahl"] = {
        Professions = 3959,
    },
    ["Betrunkene Boxencrew"] = {
        ["Goblin-Boxencrew"] = 20436,
        ["Gnomen-Boxencrew"] = 20436,
        ["Gnomen-Boxenboss"] = 20436,
        ["Goblin-Boxenboss"] = 20436,
    },
    ["Mondstoffrobe"] = {
        Professions = 22902,
    },
    ["Manarubin herbeizaubern"] = {
        ["MAGE"] = 10054,
    },
    ["Leinentasche"] = {
        Professions = 3755,
    },
    ["Verschnörkelte Mithrilstiefel"] = {
        Professions = 9979,
    },
    ["Berserker"] = {
        Racial = 26296,
        Racial = 26297,
        Racial = 20554,
    },
    ["Armschiene - Stärke"] = {
        Professions = 13661,
    },
    ["Dichtes Dynamit"] = {
        Professions = 23070,
    },
    ["Eisdeflektor"] = {
        Professions = 3957,
    },
    ["Heckenschützen-Zielfernrohr"] = {
        Professions = 12620,
    },
    ["Armschiene - Manaregeneration"] = {
        Professions = 23801,
    },
    ["Gedankenbesänftigung"] = {
        ["PRIEST"] = 10953,
    },
    ["Zweihandwaffe - Überragender Einschlag"] = {
        Professions = 20030,
    },
    ["Verdichteter Gewichtsstein"] = {
        Professions = 16640,
    },
    ["Aura der Hingabe"] = {
        ["PALADIN"] = 10293,
        ["Held der Blackrock"] = 8258,
        ["Captain Melrache"] = 8258,
        ["Gebirgsjäger von Dun Garok"] = 8258,
        ["Lord der Felsfäuste"] = 8258,
        ["Scharlachroter Beschützer"] = 8258,
        ["Stachelwache von Razorfen"] = 8258,
        ["Stachelwache-Champion"] = 8258,
        ["Kampflord der Kolkar"] = 8258,
    },
    ["Echtsilberrute"] = {
        Professions = 14380,
    },
    ["Madenschleim"] = {
        ["Aaslarve"] = 16449,
        ["Aasverschlinger"] = 16449,
        ["Verseuchte Made"] = 16449,
    },
    ["Ewige Quintessenz herstellen"] = {
        ["Fürst Hydraxis"] = 28439,
    },
    ["Verfluchtes Blut"] = {
        ["Faulender Agam'ar"] = 8267,
        ["Faulender Agam'ar"] = 8268,
    },
    ["Wandernde Seuche"] = {
        ["Seuchenverbreiter"] = 3436,
        ["Pestfleckenhyäne"] = 3436,
    },
    ["Fluch des Dunkelmeisters"] = {
        ["Dunkelmeister Gandling"] = 18702,
    },
    ["Blitzeis"] = {
        ["Eiskalter Ghul"] = 16803,
    },
    ["Blaue Leinenrobe"] = {
        Professions = 7633,
    },
    ["Magie verschlingen"] = {
        Pet = 19736,
    },
    ["Wildlederweste"] = {
        Professions = 10544,
    },
    ["Trank der freien Aktion"] = {
        Professions = 6624,
    },
    ["Goldene Schuppenschultern"] = {
        Professions = 3505,
    },
    ["Wilder Biss"] = {
        ["DRUID"] = 31018,
    },
    ["Teufelsbeherrschung"] = {
        ["WARLOCK"] = 18708,
    },
    ["Treantverbündeten beschwören"] = {
        ["Treantverbündeter"] = 7993,
        ["Cenarius Sohn"] = 7993,
    },
    ["Tier wiederbeleben"] = {
        ["HUNTER"] = 982,
    },
    ["Wildtiere aufspüren"] = {
        ["HUNTER"] = 1494,
    },
    ["Baggererkrankung"] = {
        ["Baggerwurm"] = 14535,
    },
    ["Schlammtoxin"] = {
        ["Jadegrüner Schlamm"] = 6814,
    },
    ["Braunes Leinenhemd"] = {
        Professions = 3915,
    },
    ["Krankheitswolke"] = {
        ["Kranker Ghul"] = 17742,
        ["Wächter von Undercity"] = 12187,
        ["Gordo"] = 12187,
    },
    ["Schweres Leder"] = {
        Professions = 20649,
    },
    ["Irdene Wachen zerstört"] = {
        ["Irdener Wächter"] = 10666,
        ["Irdener Gewölbeformer"] = 10666,
        ["Irdener Wärter"] = 10666,
        ["Gewölbewärter"] = 10666,
    },
    ["Erdelementar beschwören"] = {
        ["Erdelementar"] = 19704,
        ["Steinrufer der Twilight"] = 19704,
    },
    ["Tödliches Gift IV"] = {
        ["ROGUE"] = 11358,
    },
    ["Herbeigezaubertes Wasser"] = {
        ["MAGE"] = 5350,
    },
    ["Große Voodoohose"] = {
        Professions = 10560,
    },
    ["Portal: Darnassus"] = {
        ["MAGE"] = 11419,
    },
    ["Blitzheilung"] = {
        ["PRIEST"] = 10917,
    },
    ["Elixier der Feuermacht"] = {
        Professions = 7845,
    },
    ["Reinigen"] = {
        ["SHAMAN"] = 8012,
    },
    ["Trankwurf"] = {
        ["Braumeister von Dalaran"] = 7638,
    },
    ["Rostige Truhe beschwören"] = {
        ["Faulender Brühschleimer"] = 6464,
    },
    ["Lavendelfarbenes Magiestoffhemd"] = {
        Professions = 12075,
    },
    ["Schild - Frostwiderstand"] = {
        Professions = 13933,
    },
    ["Raue Kupferbombe"] = {
        Professions = 3923,
    },
    ["Gold verhütten"] = {
        Professions = 3308,
    },
    ["Dunkles Wiederherstellen"] = {
        ["Shadowfang-Wolfswache"] = 7106,
    },
    ["Stichschuss"] = {
        ["Scharfschütze von Dun Garok"] = 6685,
        ["Zuchtmeister der Defias"] = 6685,
        ["Thaurissanischer Agent"] = 6685,
        ["Pfadfinder der Totenwaldfelle"] = 6685,
        ["Scharfschütze der Schattenschmiede"] = 6685,
    },
    ["Gurt der Dämmerung"] = {
        Professions = 23632,
    },
    ["Braune Leinenweste"] = {
        Professions = 2385,
    },
    ["Elixier der schwachen Beweglichkeit"] = {
        Professions = 3230,
    },
    ["Gallkröten-Infektion"] = {
        ["Gallkröte"] = 10251,
    },
    ["Lavaexplosion A beschwören"] = {
        ["Kernratte"] = 21886,
    },
    ["Schattenhornfluch"] = {
        ["Schattenhornhirsch"] = 6922,
        ["Alter Schattenhornhirsch"] = 6922,
    },
    ["Mächtiger Draufschlag"] = {
        ["Teufelssaurier"] = 14099,
        ["Seuchenmonster"] = 14099,
        ["Irondeep-Schädelhauer"] = 14099,
    },
    ["Netherwandler beschwören"] = {
        ["Netherwandler"] = 22876,
        ["Wanderndes Auge von Kilrogg"] = 22876,
    },
    ["Werwolfirrbild beschwören"] = {
        ["Werwolfirrbild"] = 7132,
        ["Werwolfschrecken"] = 7132,
    },
    ["Heiltrank"] = {
        Professions = 3447,
    },
    ["Schattenhornaufladung"] = {
        ["Schattenhornhirsch"] = 6921,
        ["Alter Schattenhornhirsch"] = 6921,
    },
    ["Wachsen"] = {
        ["Bom'bay"] = 16711,
    },
    ["Schattensichelaxt"] = {
        Professions = 3500,
    },
    ["Wildlederschultern"] = {
        Professions = 10529,
    },
    ["Manastachel"] = {
        ["Charlga Razorflank"] = 8358,
    },
    ["Schwimmtempotrank"] = {
        Professions = 7841,
    },
    ["Schultern des Hügelwächters"] = {
        Professions = 3768,
    },
    ["Grober Gewichtsstein"] = {
        Professions = 3116,
    },
    ["Runenverzierte stygische Gamaschen"] = {
        Professions = 24901,
    },
    ["Feste Skorpidgamaschen"] = {
        Professions = 10568,
    },
    ["Armschiene - Geringe Willenskraft"] = {
        Professions = 7859,
    },
    ["Verzaubertes Leder"] = {
        Professions = 17181,
    },
    ["Große Heilung"] = {
        ["PRIEST"] = 25314,
    },
    ["Kleines Seidenpack"] = {
        Professions = 3813,
    },
    ["Einfaches Kleid"] = {
        Professions = 8465,
    },
    ["Kopfkracher"] = {
        ["Krieger der Skullsplitter"] = 3148,
        ["Axtwerfer der Skullsplitter"] = 3148,
        ["Schädelhauer der Felsenkiefer"] = 3148,
        ["Devlin Agamand"] = 3148,
        ["Knochenknacker der Kieselsplittertroggs"] = 3148,
        ["Schmiedelehrling von Hillsbrad"] = 3148,
        ["Irondeep-Schädelhauer"] = 16172,
        ["Crushridge-Vollstrecker"] = 9791,
    },
    ["Transmutieren: Wasser zu Luft"] = {
        Professions = 17562,
    },
    ["Kleine grüne Rakete"] = {
        Professions = 26417,
    },
    ["Große Verzauberertasche"] = {
        Professions = 27660,
    },
    ["Mal der Wildnis"] = {
        ["DRUID"] = 9885,
    },
    ["Barbarische Armschienen"] = {
        Professions = 23399,
    },
    ["Hervorrufung"] = {
        ["MAGE"] = 12051,
    },
    ["Totem der Verbrennung"] = {
        ["SHAMAN"] = 10438,
    },
    ["Machtwort: Seelenstärke"] = {
        ["PRIEST"] = 10938,
        ["Bastionssanitäter"] = 13864,
    },
    ["Heiliges Schmettern"] = {
        ["Coldmine-Feldmesser"] = 15498,
        ["Irondeep-Feldmesser"] = 15498,
        ["Coldmine-Feldmesserveteran"] = 15498,
        ["Irondeep-Feldmesserveteran"] = 15498,
        ["Irondeep-Feldmesserchampion"] = 15498,
        ["Murlocorakel"] = 9734,
        ["Sirene der Sturmschuppen"] = 9734,
        ["Priester von Dun Garok"] = 9734,
        ["Bewahrerin von Theramore"] = 9734,
        ["Sucher der Verlassenen"] = 9734,
        ["Scharlachroter Jünger"] = 9734,
        ["Scharlachroter Adept"] = 9734,
        ["Kleriker von Nethergarde"] = 9734,
        ["Expeditionspriester"] = 9734,
    },
    ["Kleine rote Rakete"] = {
        Professions = 26418,
    },
    ["Einfaches schwarzes Kleid"] = {
        Professions = 12077,
    },
    ["Kraft der Nordschwingen"] = {
        ["Harpyie der Nordschwingen"] = 11014,
        ["Wildfeder der Nordschwingen"] = 11014,
        ["Töterin der Nordschwingen"] = 11014,
        ["Windruferin der Nordschwingen"] = 11014,
    },
    ["Elixier der Beweglichkeit"] = {
        Professions = 11449,
    },
    ["Leuchtfeuer"] = {
        ["HUNTER"] = 1543,
    },
    ["Gnomen-Schrumpfstrahl"] = {
        Professions = 12899,
    },
    ["Schwelle des Todes"] = {
        ["Verletzter Arbeiter"] = 23127,
        ["Verseuchter Arbeiter"] = 23127,
    },
    ["Siegel der Gerechtigkeit"] = {
        ["PALADIN"] = 20164,
    },
    ["Hose des Nachtschleichers"] = {
        Professions = 10548,
    },
    ["Wilddornpanzerung"] = {
        Professions = 16650,
    },
    ["Große rote Rakete"] = {
        Professions = 26422,
    },
    ["Dunkeleisengewehr"] = {
        Professions = 19796,
    },
    ["Flicken"] = {
        ["Techbot"] = 10860,
    },
    ["Schattenschutztrank"] = {
        Professions = 7256,
    },
    ["Stab des Befehls herbeizaubern"] = {
        ["Schleifer"] = 15539,
    },
    ["Hochentwickelte Zielattrappe - Ereignis 003"] = {
        ["Hochentwickelte Zielattrappe des Mörsertrupps"] = 19723,
        ["Späher Klemmy"] = 19723,
    },
    ["Fluch von Tuten'kash"] = {
        ["Tuten'kash"] = 12255,
    },
    ["Verschwinden"] = {
        ["ROGUE"] = 1857,
    },
    ["Traumzwirnweste"] = {
        Professions = 12070,
    },
    ["Handschuhe - Große Beweglichkeit"] = {
        Professions = 20012,
    },
    ["Verhexung von Rabenklaue"] = {
        ["Thule Rabenklaue"] = 7655,
        ["Thule Rabenklaue"] = 7656,
        ["Thule Rabenklaue"] = 7657,
    },
    ["Tier rufen"] = {
        ["HUNTER"] = 883,
    },
    ["Cenarische Kräutertasche"] = {
        Professions = 27724,
    },
    ["Zielattrappe - Ereignis 002"] = {
        ["Mörsertrupp-Zielattrappe"] = 18907,
        ["Späher Klemmy"] = 18907,
    },
    ["Aufwärtshaken"] = {
        ["Chok'sul"] = 18072,
        ["Taragaman der Hungerleider"] = 18072,
        ["Kriegshäscher"] = 10966,
        ["Arnak Grimmtotem"] = 10966,
    },
    ["Unverwüstliches Leder"] = {
        Professions = 22331,
    },
    ["Frostzauberschutz"] = {
        ["MAGE"] = 28609,
    },
    ["Bronzener Kriegshammer"] = {
        Professions = 9985,
    },
    ["Dornen"] = {
        ["DRUID"] = 9910,
    },
    ["Hieb"] = {
        ["DRUID"] = 8983,
    },
    ["Seelenbeutel"] = {
        Professions = 26085,
    },
    ["Schwarzwelpenumhang"] = {
        Professions = 9070,
    },
    ["Verändertes Kesseltoxin"] = {
        ["Bauer Dalson"] = 17650,
    },
    ["Lavaexplosion F beschwören"] = {
        ["Kernratte"] = 21904,
    },
    ["Blaue Drachenschuppenbrustplatte"] = {
        Professions = 19077,
    },
    ["Verschnörkelte Mithrilschultern"] = {
        Professions = 9952,
    },
    ["Goblin-Raketentreibstoff-Rezept"] = {
        Professions = 12715,
    },
    ["Inferno-Muschelschale"] = {
        ["Magistrat Burnside"] = 7739,
        ["Flammenwirker der Steingrufttroggs"] = 7739,
    },
    ["Winterzwirnrobe"] = {
        Professions = 18404,
    },
    ["Verderbte Stärke"] = {
        ["Krieger der Fäulnisklauen"] = 6816,
        ["Ursa der Fäulnisklauen"] = 6816,
        ["Knurrtatze"] = 6816,
        ["Wütende Fäulnisklaue"] = 6816,
    },
    ["Rumpler der Gelkis beschwören"] = {
        ["Rumpler der Gelkis"] = 9653,
        ["Erdenrufer der Gelkis"] = 9653,
    },
    ["Wille von Shahram"] = {
        ["Shahram"] = 16598,
    },
    ["Spawn Schwarzen Drakoniden"] = {
        ["Schwarzer Drakonid"] = 22654,
        ["Black Drakonid Spawner"] = 22654,
    },
    ["Weißes Lederwams"] = {
        Professions = 2163,
    },
    ["Arkane Blase"] = {
        ["Arkanist Doan"] = 9438,
    },
    ["Funke"] = {
        ["Leygrimmling"] = 21027,
        ["Managrimmling"] = 21027,
    },
    ["Bronze verhütten"] = {
        Professions = 2659,
    },
    ["Lebendige Schultern"] = {
        Professions = 19061,
    },
    ["Schweres Sprengpulver"] = {
        Professions = 3945,
    },
    ["Geisterschatten sichtbar"] = {
        ["Geisterschatten"] = 24809,
    },
    ["Moonkingestalt"] = {
        ["DRUID"] = 24858,
    },
    ["Tiefe Wunde"] = {
        ["Bösartiger Geist"] = 12721,
        ["Aspekt der Bosheit"] = 12721,
    },
    ["Flammkinfackler beschwören"] = {
        ["Flammkinfackler"] = 15710,
        ["Terrorstifter"] = 15710,
    },
    ["Siegel des Kreuzfahrers"] = {
        ["PALADIN"] = 20308,
    },
    ["Schwacher Verjüngungstrank"] = {
        Professions = 2332,
    },
    ["Handschuhe - Kräuterkunde"] = {
        Professions = 13617,
    },
    ["Fleisch zerreißen"] = {
        ["Bhag'thera"] = 3147,
        ["Hochlandsensenklaue"] = 3147,
        ["Scheckige Sensenklaue"] = 3147,
        ["Bjarn"] = 3147,
        ["Scharfzahnmatriarchin"] = 3147,
        ["Ausgebildeter Klingenschnabel"] = 3147,
        ["Klingenschnabelgreif"] = 3147,
        ["Klingenschnabelhimmelsfürst"] = 3147,
        ["Plünderin der Staubschwingen"] = 3147,
        ["Blutsumpfsensenklaue"] = 3147,
        ["Matschpanzerklingenklaue"] = 3147,
        ["Schreckensschlitzer"] = 3147,
        ["Wilder Wutschrammyeti"] = 3147,
    },
    ["Zweihandwaffe - Großer Einschlag"] = {
        Professions = 13937,
    },
    ["Verstärktes Leinencape"] = {
        Professions = 2397,
    },
    ["Uralte Hysterie"] = {
        ["Uralter Kernhund"] = 19372,
    },
    ["Larve der Gorishi beschwören"] = {
        ["Larve der Gorishi"] = 14206,
        ["Ei der Gorishi"] = 14206,
    },
    ["Schattenzwirnhandschuhe"] = {
        Professions = 12071,
    },
    ["Erdenrumpler beschwören"] = {
        ["Steinrumpler"] = 8270,
        ["Geomant von Razorfen"] = 8270,
        ["Erdenrufer Halmgar"] = 8270,
        ["Roogug"] = 8270,
    },
    ["Sneed hinauswerfen"] = {
        ["Sneed"] = 5141,
        ["Sneeds Schredder"] = 5141,
    },
    ["Rasende Regeneration"] = {
        ["DRUID"] = 22896,
    },
    ["Madenglibber"] = {
        ["Aasverschlinger"] = 17197,
    },
    ["Tetanus"] = {
        ["Rostiger Erntegolem"] = 8014,
        ["Bergbussard"] = 8014,
        ["Marodeur der Kolkar"] = 8014,
        ["Eindringling der Kolkar"] = 8014,
    },
    ["Schattenpirscher-Streich"] = {
        ["Schattenpirscher der Herzschinder"] = 6927,
    },
    ["Schwanzfeger"] = {
        ["Ysondre"] = 15847,
        ["Lethon"] = 15847,
        ["Emeriss"] = 15847,
        ["Taerar"] = 15847,
    },
    ["Fluch der Totenwaldfelle"] = {
        ["Krieger der Totenwaldfelle"] = 13583,
        ["Gärtner der Totenwaldfelle"] = 13583,
        ["Pfadfinder der Totenwaldfelle"] = 13583,
        ["Höhlenbehüter der Totenwaldfelle"] = 13583,
        ["Rächer der Totenwaldfelle"] = 13583,
        ["Schamane der Totenwaldfelle"] = 13583,
        ["Häuptling Blutmagen"] = 13583,
    },
    ["Hurtige Stiefel"] = {
        Professions = 9208,
    },
    ["Barbarische Gamaschen"] = {
        Professions = 7149,
    },
    ["Manacitrin herbeizaubern"] = {
        ["MAGE"] = 10053,
    },
    ["Brust - Geringe Gesundheit"] = {
        Professions = 7748,
    },
    ["Schildgenerator"] = {
        ["Dampfpanzer"] = 27759,
    },
    ["Schwarzes Schwadroneurshemd"] = {
        Professions = 3873,
    },
    ["Feine Lederhandschuhe"] = {
        Professions = 2164,
    },
    ["Rakete, Grün"] = {
        ["Pats Feuerwerkstyp - GRÜN"] = 26345,
    },
    ["Kriecher beschwören"] = {
        ["Kriecher"] = 8656,
        ["Murlocjäger"] = 8656,
    },
    ["Bronzegerüst"] = {
        Professions = 3953,
    },
    ["Erdstampfer"] = {
        ["Uralter Kernhund"] = 19364,
    },
    ["Teleportieren: Stormwind"] = {
        ["MAGE"] = 3561,
    },
    ["Höllenbestiendiener beschwören"] = {
        ["Höllenbestiendiener"] = 12740,
        ["Lady Sevine"] = 12740,
    },
    ["Blutpakt"] = {
        Pet = 11767,
    },
    ["Sturmattacke"] = {
        ["Steinhauereber"] = 6268,
        ["Langschnauze"] = 6268,
        ["Geiferzahn"] = 6268,
        ["Prinzessin"] = 6268,
        ["Stopfwanst"] = 6268,
        ["Gefolgsschwein"] = 6268,
        ["Hogger"] = 6268,
        ["Junger Geiferzahn"] = 6268,
        ["Felsnischeneber"] = 6268,
        ["Großer Geiferzahn"] = 6268,
        ["Murlocküstenschläger"] = 6268,
        ["Klippeneber"] = 6268,
        ["Großer Klippeneber"] = 6268,
        ["Alter Klippeneber"] = 6268,
        ["Bergeber"] = 6268,
        ["Räudiger Bergeber"] = 6268,
        ["Alter Bergeber"] = 6268,
        ["Vernarbter Klippeneber"] = 6268,
        ["Kavallerist von Stromgarde"] = 6268,
        ["Kodokalb"] = 6268,
        ["Mazzranache"] = 6268,
        ["Blutkrallenschwanzpeitscher"] = 6268,
        ["Blutkrallensensenschlund"] = 6268,
        ["Matrose von Kul Tiras"] = 6268,
        ["Verderbter Blutkrallensensenschlund"] = 6268,
        ["Großer Brachlandkodo"] = 6268,
        ["Wollkodo"] = 6268,
        ["Marodeur der Kolkar"] = 6268,
        ["Pfützenspringer der Salzflossen"] = 6268,
        ["Wildbock"] = 6268,
        ["Tobender Klippenstürmer"] = 6268,
        ["Gehörnter Renner"] = 6268,
        ["Marodeur der Galak"] = 6268,
        ["Agam'ar"] = 6268,
        ["Brontus"] = 6268,
        ["Aschmähneneber"] = 6268,
        ["Donnerkopfpatriarch"] = 6268,
        ["Siechender Krieger"] = 6268,
        ["Eindringling der Kolkar"] = 6268,
        ["Schläger der Grimmtotem"] = 6268,
        ["Herod"] = 8260,
        ["Agathelos der Tobende"] = 8260,
        ["Suhlaman"] = 8260,
    },
    ["Tigerfuror"] = {
        ["DRUID"] = 9846,
    },
    ["Gabelblitzschlag"] = {
        ["Lady Vespira"] = 12549,
        ["Lady Sarevess"] = 8435,
    },
    ["Einfache Leinenstiefel"] = {
        Professions = 12045,
    },
    ["Göttlicher Schutz"] = {
        ["PALADIN"] = 5573,
    },
    ["Geisterzwirnhose"] = {
        Professions = 18441,
    },
    ["Entrinnen"] = {
        ["ROGUE"] = 5277,
    },
    ["Windsors Raserei"] = {
        ["Marshal Windsor"] = 15167,
    },
    ["Katzenaugen-Ultraschutzbrille"] = {
        Professions = 12607,
    },
    ["Fluch der Heilung"] = {
        ["Fanatikerin des dunklen Strangs"] = 7098,
        ["Harpyie der Hexenschwingen"] = 7098,
    },
    ["Heilungszauberschutz"] = {
        ["Großer Heilungszauberschutz"] = 11899,
        ["Schamane der Mosh'Ogg"] = 11899,
        ["Hexendoktor der Sandfury"] = 11899,
        ["Heilungszauberschutz IV"] = 6274,
        ["Orakel der Blaukiemen"] = 6274,
        ["Seher der Grimmhauer"] = 6274,
        ["Totemiker der Distelfelle"] = 6274,
        ["Heilungszauberschutz V"] = 4971,
        ["Totemiker von Razorfen"] = 4971,
        ["Weiser der Totenköpfe"] = 4971,
        ["Seher der Totenköpfe"] = 4971,
        ["Heilungszauberschutz"] = 5605,
        ["Hexendoktor der Bloodscalp"] = 5605,
        ["Seepirscher der Finsterflossen"] = 5605,
        ["Totemiker der Knarzklauen"] = 5605,
        ["Totemiker der Schwarzfelle"] = 5605,
        ["Hexendoktor der Witherbark"] = 5605,
        ["Schamane der Steingrufttroggs"] = 5605,
        ["Alte Mystikerin Grimmschnauze"] = 5605,
        ["Orakel der Zackenkämme"] = 5605,
        ["Donnerstampfer der Grimmtotem"] = 5605,
    },
    ["Elixier der großen Beweglichkeit"] = {
        Professions = 11467,
    },
    ["Schreckgespenst des Syndikats beschwören"] = {
        ["Schreckgespenst des Syndikats"] = 3722,
        ["Buchhalter Herod"] = 3722,
    },
    ["Wildtier ängstigen"] = {
        ["HUNTER"] = 14327,
    },
    ["Rückhand"] = {
        ["Kazon"] = 6253,
        ["Muskelprotz"] = 6253,
        ["Eingekerkerter Defias"] = 6253,
        ["Crushridge-Raufer"] = 6253,
        ["Otto"] = 6253,
        ["Brigant der Südmeerfreibeuter"] = 6253,
        ["Ausgrabungsleiter Khazgorm"] = 6253,
        ["Rohling der Verlassenen"] = 6253,
        ["Tazan"] = 6253,
        ["Coldmine-Wache"] = 6253,
        ["Coldmine-Wachenveteran"] = 6253,
    },
    ["Wasseratmung"] = {
        ["SHAMAN"] = 131,
    },
    ["Kampfschrei"] = {
        ["Moonrage-Wachposten"] = 6507,
        ["Pyrewood-Wachposten"] = 6507,
        ["Terrowulf-Rudelführer"] = 6507,
        ["Alter Wutschrammyeti"] = 6507,
    },
    ["Transmutieren: Arkanit"] = {
        Professions = 17187,
    },
    ["Fluch der Elemente"] = {
        ["WARLOCK"] = 11722,
    },
    ["Grober Schleifstein"] = {
        Professions = 3326,
    },
    ["Grüner Eisenhelm"] = {
        Professions = 3502,
    },
    ["Spinnengürtel"] = {
        Professions = 3863,
    },
    ["Fluch der Sprachen"] = {
        ["WARLOCK"] = 11719,
    },
    ["Zielattrappe"] = {
        Professions = 3932,
    },
    ["Goblinscher Schutzhelm"] = {
        Professions = 12718,
    },
    ["Ätzgift"] = {
        ["Alter Mooskrabbler"] = 3396,
        ["Riesiger Mooskrabbler"] = 3396,
        ["Waldmooskrabbler"] = 3396,
        ["Ätzblutsaftbestie"] = 3396,
        ["Schimärenjunges"] = 3396,
        ["Schimärenmatriarchin"] = 3396,
    },
    ["Purpurrote Seidenpantalons"] = {
        Professions = 8799,
    },
    ["Kupferrohr"] = {
        Professions = 3924,
    },
    ["Echtsilberbrustplatte"] = {
        Professions = 9974,
    },
    ["Dämonenrüstung"] = {
        ["WARLOCK"] = 11735,
        ["Thule Rabenklaue"] = 13787,
    },
    ["Kochendes Blut"] = {
        Racial = 20572,
    },
    ["Runenstofftasche"] = {
        Professions = 18405,
    },
    ["Aura der Konzentration"] = {
        ["PALADIN"] = 19746,
    },
    ["Dicker Ledermunitionsbeutel"] = {
        Professions = 14932,
    },
    ["Transmutieren: Wasser zu Untod"] = {
        Professions = 17564,
    },
    ["Gegenangriff"] = {
        ["HUNTER"] = 20910,
    },
    ["Schwerkalibriges Mithrilgewehr"] = {
        Professions = 12614,
    },
    ["Taelans Leiden"] = {
        ["Hochlord Taelan Fordring"] = 18811,
    },
    ["Terrorbärengestalt"] = {
        ["DRUID"] = 9634,
    },
    ["Arajs Phylakterium"] = {
        ["Araj der Beschwörer"] = 18661,
    },
    ["Tier heilen"] = {
        ["HUNTER"] = 13544,
    },
    ["Mächtige Magie bannen"] = {
        ["Hohepriester Thel'danis"] = 24997,
    },
    ["Teufelsjäger beschwören"] = {
        ["WARLOCK"] = 691,
    },
    ["Welkstoß"] = {
        ["Moderrankenmorastbestie"] = 5337,
        ["Siechender Beschützer"] = 5337,
        ["Arei"] = 5337,
        ["Verräter von Darrowshire"] = 5337,
        ["Spektraler Verräter"] = 5337,
    },
    ["Verbessertes Blocken"] = {
        ["Zenturio der Dragonmaw"] = 3419,
        ["Kam Deepfury"] = 3419,
        ["Fußsoldat von Hillsbrad"] = 3419,
        ["Handlanger der Defias"] = 3248,
        ["Captain Vachon"] = 3248,
        ["Wächter von Rabenklaue"] = 3248,
        ["Krieger der Sturmschuppen"] = 3248,
        ["Gebirgsjäger von Dun Garok"] = 3248,
        ["Schlachtwache der Grimmhauer"] = 3248,
        ["Lieutenant Benedict"] = 3248,
        ["Verteidiger von Razorfen"] = 3248,
        ["Soldat von Nethergarde"] = 3248,
        ["Kurzens Dschungelkämpfer"] = 3639,
        ["Verteidiger von Stromgarde"] = 3639,
        ["Scharlachroter Soldat"] = 3639,
        ["Scharlachroter Verteidiger"] = 3639,
        ["Verteidiger der Zuflucht"] = 3639,
    },
    ["Smokingjacke"] = {
        Professions = 12093,
    },
    ["Schlange von Dalaran beschwören"] = {
        ["Schlange von Dalaran"] = 3615,
        ["Beschützer von Dalaran"] = 3615,
        ["Wärter von Dalaran"] = 3615,
    },
    ["Mungobiss"] = {
        ["HUNTER"] = 14271,
    },
    ["Schwärender Ausschlag"] = {
        ["Struppige Eulenbestie"] = 15848,
    },
    ["Stichschatten"] = {
        ["Thuzadinschattenzauberer"] = 16429,
    },
    ["Portal: Thunder Bluff"] = {
        ["MAGE"] = 11420,
    },
    ["Schwerer Lederball"] = {
        Professions = 23190,
    },
    ["Schlangengestalt"] = {
        ["Boahn"] = 8041,
        ["Druide des Giftzahns"] = 8041,
    },
    ["Kupferdolch"] = {
        Professions = 8880,
    },
    ["Azurblaue Seidenkapuze"] = {
        Professions = 8760,
    },
    ["Heiligschutztrank"] = {
        Professions = 7255,
    },
    ["Läutern"] = {
        ["PALADIN"] = 1152,
    },
    ["Befehlsruf"] = {
        ["Captain der Todeskrallen"] = 22440,
    },
    ["Rote Leinenweste"] = {
        Professions = 7629,
    },
    ["Thoriumröhre"] = {
        Professions = 19795,
    },
    ["Schattenzwirnrobe"] = {
        Professions = 12055,
    },
    ["Zombie beschwören"] = {
        ["Beschworener Zombie"] = 16590,
        ["Dunkler Beschwörer"] = 16590,
    },
    ["Moosfell"] = {
        ["Gnoll der Moosfelle"] = 3288,
        ["Bastard der Moosfelle"] = 3288,
        ["Nebelwirker der Moosfelle"] = 3288,
        ["Moorläufer der Moosfelle"] = 3288,
        ["Fallensteller der Moosfelle"] = 3288,
        ["Mystiker der Moosfelle"] = 3288,
        ["Alpha der Moosfelle"] = 3288,
    },
    ["Tödlicher Stich"] = {
        ["Grufttöter"] = 17170,
        ["Krellack"] = 17170,
    },
    ["Leinenschuhe mit weichen Sohlen"] = {
        Professions = 3845,
    },
    ["Stahl verhütten"] = {
        Professions = 3569,
    },
    ["Eistotem beschwören"] = {
        ["Eistotem"] = 18975,
        ["Schamane der Wildpfoten"] = 18975,
    },
    ["Voodoo"] = {
        ["Bom'bay"] = 17009,
    },
    ["Larvenglibber"] = {
        ["Elende Larve"] = 21069,
    },
    ["Mechanisches Reparierset"] = {
        Professions = 15255,
    },
    ["Gezähmten Kampfeber beschwören"] = {
        ["Gezähmter Kampfeber"] = 8274,
        ["Tierführer von Razorfen"] = 8274,
        ["Bestienausbilder von Razorfen"] = 8274,
    },
    ["Unverwüstliches Rüstungsset"] = {
        Professions = 19058,
    },
    ["Schattenschutz"] = {
        ["PRIEST"] = 10958,
    },
    ["Mechanischer Drachling"] = {
        Professions = 3969,
    },
    ["Stiefel - Schwache Ausdauer"] = {
        Professions = 7863,
    },
    ["Trank 'Stärke II'"] = {
        ["Kräuterkundiger der Flusspfoten"] = 3369,
    },
    ["Schildwall"] = {
        ["WARRIOR"] = 871,
    },
    ["Demoralisierungsruf"] = {
        ["WARRIOR"] = 11556,
    },
    ["Muskelriss"] = {
        ["Großer Fleischreißer"] = 12166,
        ["Fleischreißer"] = 12166,
        ["Wilder Nachtsäbler"] = 12166,
        ["Riffkriecher"] = 12166,
        ["Schaudermagenkrokilisk"] = 12166,
        ["Eindringling der Borstennacken"] = 12166,
        ["Der Kratzer"] = 12166,
    },
    ["Kreischen der Vergangenheit"] = {
        ["Klagender Gardist"] = 7074,
        ["Zorniges Phantom"] = 7074,
    },
    ["Inneres Feuer"] = {
        ["PRIEST"] = 10952,
    },
    ["Schwere Mithrilbrustplatte"] = {
        Professions = 9959,
    },
    ["Schneller Seitschritt"] = {
        ["Verkrusteter Brandungskriecher"] = 5426,
    },
    ["Rekonstruieren"] = {
        ["Irdener Gewölbeformer"] = 10260,
    },
    ["Mantel der Holzschlundfeste"] = {
        Professions = 23663,
    },
    ["Klauengriff"] = {
        ["Makruraschnappklaue"] = 5424,
        ["Makruraältester"] = 5424,
    },
    ["Robe der Macht"] = {
        Professions = 8770,
    },
    ["Mächtiger Eisenhammer"] = {
        Professions = 3297,
    },
    ["Menge verprügeln"] = {
        ["Ograbisi"] = 10887,
        ["Setis"] = 10887,
    },
    ["Wächterumhang"] = {
        Professions = 7153,
    },
    ["Rotes Schwadroneurshemd"] = {
        Professions = 8489,
    },
    ["Rüstungsschmelze"] = {
        ["Flammenwächter"] = 19631,
    },
    ["Leichte Lederhose"] = {
        Professions = 9068,
    },
    ["Fluch der Tollkühnheit"] = {
        ["WARLOCK"] = 11717,
        ["Töterin der Blutfurien"] = 16231,
    },
    ["Mörser"] = {
        ["Mörser"] = 25003,
        ["Dampfpanzer"] = 25003,
    },
    ["Stiefel des Nachtschleichers"] = {
        Professions = 10558,
    },
    ["Kampfnetz"] = {
        ["Techbot"] = 10852,
    },
    ["Obsidiansplitter beschwören"] = {
        ["Obsidiansplitter"] = 10061,
    },
    ["Seelen-Siphon"] = {
        ["Azshir der Schlaflose"] = 9373,
        ["Diener von Rabenklaue"] = 7290,
        ["Fizzle Dunkelsturm"] = 7290,
    },
    ["Goblinsche Pioniersprengladung"] = {
        Professions = 12760,
    },
    ["Großer Magiezauberstab"] = {
        Professions = 14807,
    },
    ["Verzaubertes Thorium"] = {
        Professions = 17180,
    },
    ["Zweihandwaffen - Beweglichkeit"] = {
        Professions = 27837,
    },
    ["Hände der Dunkelheit"] = {
        Professions = 8780,
    },
    ["Lederhandschuhe des Hügelwächters"] = {
        Professions = 3764,
    },
    ["Seelensplitter"] = {
        ["WARLOCK"] = 6265,
    },
    ["Totem des Frostwiderstands"] = {
        ["SHAMAN"] = 10479,
    },
    ["Hinterhalt"] = {
        ["ROGUE"] = 11269,
    },
    ["Verriegeln"] = {
        ["Kriegsgolem"] = 9576,
    },
    ["Rotes Feuerwerk"] = {
        Professions = 23066,
        ["Wrenix' Elektrodingsda-Apparat"] = 6668,
    },
    ["Göttliche Pein"] = {
        ["PRIEST"] = 10934,
    },
    ["Giftstachel"] = {
        ["Übelstich"] = 8257,
        ["Siechstachelskorpid"] = 5416,
        ["Skorpashischnapper"] = 5416,
        ["Skorpashipeitscher"] = 5416,
        ["Skorpashigiftpeitscher"] = 5416,
        ["Skorpiddünenpirscher"] = 5416,
        ["Besseleth"] = 5416,
    },
    ["Verschnörkeltes Handfernrohr"] = {
        Professions = 6458,
    },
    ["Totem des heilenden Flusses"] = {
        ["SHAMAN"] = 10463,
    },
    ["Flimmerkernhandschuhe"] = {
        Professions = 20849,
    },
    ["Großer Segen des Refugiums"] = {
        ["PALADIN"] = 25899,
    },
    ["Wilde Regeneration"] = {
        ["Deviatschlurfer"] = 7948,
        ["Frosthagelbewahrer"] = 9616,
    },
    ["Blutblütengift"] = {
        ["Blutblütenpeitscher"] = 14110,
        ["Blutblütenschinder"] = 14110,
        ["Blutblütendrescher"] = 14110,
        ["Blutblütenfalle"] = 14110,
    },
    ["Handschuhe - Angeln"] = {
        Professions = 13620,
    },
    ["Kupferstreitaxt"] = {
        Professions = 3293,
    },
    ["Dunkler Ledergürtel"] = {
        Professions = 3766,
    },
    ["Blutrichter"] = {
        ["Blutpriester der Hakkari"] = 24617,
    },
    ["Gruftskarabäen"] = {
        ["Gruftskarabäus"] = 16418,
        ["Gruftkriecher"] = 16418,
    },
    ["Hellstoffumhang"] = {
        Professions = 18420,
    },
    ["Ei der Gorishi"] = {
        ["Ei der Gorishi"] = 14205,
        ["Gelegemutter Zavas"] = 14205,
    },
    ["Portal: Orgrimmar"] = {
        ["MAGE"] = 11417,
    },
    ["Der Zertrümmerer"] = {
        Professions = 10003,
    },
    ["Unerträgliche Schmerzen"] = {
        ["Twilight-Hammer-Folterknecht"] = 13619,
        ["Aspekt der Verdorbenheit"] = 13619,
    },
    ["Dicke Kriegsaxt"] = {
        Professions = 3294,
    },
    ["Flasche Gift"] = {
        ["Futterwühler der Tunnelratten"] = 7365,
    },
    ["Gürtel des Erzmagiers"] = {
        Professions = 22866,
    },
    ["Unheilige Raserei"] = {
        ["Skelettwärter"] = 8699,
    },
    ["Kältekegel"] = {
        ["MAGE"] = 10161,
    },
    ["Zweihandwaffe - Geringer Einschlag"] = {
        Professions = 13529,
    },
    ["Lederweste des Hügelwächters"] = {
        Professions = 3762,
    },
    ["Aufgeladener Arkanblitz"] = {
        ["Prinz Schleifer"] = 16570,
    },
    ["Echtsilberstulpen"] = {
        Professions = 9954,
    },
    ["Machtwort: Schild"] = {
        ["PRIEST"] = 10901,
        ["Hochinquisitor Fairbanks"] = 11647,
        ["Kleriker von Nethergarde"] = 11974,
    },
    ["Totem des Naturwiderstands"] = {
        ["SHAMAN"] = 10601,
    },
    ["Tödlicher Stoß"] = {
        ["WARRIOR"] = 21553,
        ["Lieutenant Rugba"] = 15708,
        ["Lieutenant Spencer"] = 15708,
        ["Lieutenant Stronghoof"] = 15708,
        ["Lieutenant Vol'talar"] = 15708,
        ["Lieutenant Grummus"] = 15708,
        ["Lieutenant Murp"] = 15708,
        ["Lieutenant Lewis"] = 15708,
        ["Lieutenant Largent"] = 15708,
        ["Lieutenant Stouthandle"] = 15708,
        ["Lieutenant Greywand"] = 15708,
        ["Lieutenant Lonadin"] = 15708,
        ["Strashazkrieger"] = 16856,
        ["Töter von Hederine"] = 16856,
        ["Captain Galvangar"] = 16856,
        ["Irondeep-Wache"] = 16856,
        ["Irondeep-Wachenveteran"] = 16856,
        ["Irondeep-Wachenchampion"] = 16856,
        ["Kommandant der Stormpike-Widderreiter"] = 16856,
        ["Jotek"] = 16856,
    },
    ["Schlachtruf der Drachentöter"] = {
        ["Oberanführer Runthak"] = 22888,
        ["Major Mattingly"] = 22888,
        ["Hochfürst Saurfang"] = 22888,
        ["Feldmarschall Afrasiabi"] = 22888,
    },
    ["Manifestation endet"] = {
        ["Geist von Kolk"] = 21965,
        ["Geist von Veng"] = 21965,
    },
    ["Mithrildonnerbüchse"] = {
        Professions = 12595,
    },
    ["Runenverzierte Echtsilberrute"] = {
        Professions = 13702,
    },
    ["Blauer Overall"] = {
        Professions = 7639,
    },
    ["Thoriumrüstung"] = {
        Professions = 16642,
    },
    ["Grüne Linse"] = {
        Professions = 12622,
    },
    ["Erdschock"] = {
        ["SHAMAN"] = 10414,
        ["Orakel der Hakkari"] = 15501,
        ["Donnerstampfer der Grimmtotem"] = 13281,
        ["Felsbewahrer der Gogger"] = 13281,
    },
    ["Brust - Geringe Werte"] = {
        Professions = 13700,
    },
    ["Fläschchen mit oberster Macht"] = {
        Professions = 17637,
    },
    ["Verwirrtheit"] = {
        ["Shadowfang-Dunkelseele"] = 8140,
        ["Aspekt der Banalität"] = 8140,
    },
    ["Symbol der Offenbarung"] = {
        ["PALADIN"] = 17033,
    },
    ["Grüne Seidenrüstung"] = {
        Professions = 8784,
    },
    ["Imperialer Plattengürtel"] = {
        Professions = 16647,
    },
    ["Runenstoffballen"] = {
        Professions = 18401,
    },
    ["Aufgabe annehmen"] = {
        ["Thontek Rumblehoof"] = 17649,
    },
    ["Savannenjunge"] = {
        ["Savannenjunges"] = 8210,
        ["Dishu"] = 8210,
    },
    ["Sayges dunkles Schicksal des Widerstands"] = {
        ["Sayge"] = 23769,
    },
    ["Wolf der Grimmhauer"] = {
        ["Wolf der Grimmhauer"] = 6479,
        ["Jäger der Grimmhauer"] = 6479,
    },
    ["Azurblauer Seidengürtel"] = {
        Professions = 8766,
    },
    ["Feuerball"] = {
        ["MAGE"] = 25306,
        ["Singe"] = 13375,
        ["Schwarzer Brutling"] = 13375,
        ["Brühender Brutling"] = 13375,
        ["Übler Familiar"] = 11921,
        ["Große Lavaspinne"] = 11985,
        ["Twilight-Geomant"] = 14034,
        ["Schwarzwyrmkin"] = 14034,
        ["Verirrter Welpe"] = 11839,
        ["Schwarzer Großdrachenwelpe"] = 20793,
        ["Renegatenmagier der Defias"] = 20793,
        ["Koboldgeomant"] = 20793,
        ["Surena Caledon"] = 20793,
        ["Scharlachroter Initiand"] = 20793,
        ["Magier von Dalaran"] = 20793,
        ["Zauberhexerin der Blutfedern"] = 20793,
        ["Geomant der Borstennacken"] = 20793,
        ["Geomant der Grimmhauer"] = 20793,
        ["Roder der Venture Co."] = 20793,
        ["Beschwörer der Blackrock"] = 20793,
        ["Gibblewilt"] = 20793,
        ["Geomant der Gogger"] = 20793,
        ["Morganth"] = 20811,
        ["Verzauberer der Defias"] = 20811,
        ["Magistrat Burnside"] = 20811,
        ["Geomant der Grimmtotem"] = 20811,
        ["Hexer des Syndikats"] = 20815,
        ["Beschwörer von Dalaran"] = 20815,
        ["Erdenrufer der Gelkis"] = 20815,
        ["Magier der Blutsegelbukaniere"] = 20823,
        ["Aufseherin Belamoore"] = 20823,
        ["Feeboz"] = 20823,
        ["Geologe der Dunkeleisenzwerge"] = 20823,
        ["Adept von Jaedenar"] = 20823,
        ["Captain Balinda Stonehearth"] = 12466,
        ["Coldmine-Forscher"] = 15242,
        ["Irondeep-Forscher"] = 15242,
        ["Geomant der Weißstoppel"] = 15228,
        ["Beschwörer der Defias"] = 9053,
        ["Magicus der Defias"] = 9053,
        ["Höllenrufer von Xavian"] = 9053,
        ["Brennender Zerstörer"] = 9053,
        ["Scharlachroter Magicus"] = 9053,
        ["Scharlachroter Rufer"] = 9053,
        ["Scharlachroter Rutengänger"] = 9053,
        ["Scharlachroter Herbeizauberer"] = 9053,
        ["Sengender Welpe"] = 9053,
        ["Hexer der Defias"] = 9053,
        ["Feldmesser der Schattenschmiede"] = 9053,
        ["Geomantin der Totenköpfe"] = 9053,
        ["Plünderer der Defias"] = 19816,
        ["Feuerwirker der Splitterfäuste"] = 19816,
        ["Geomant der Kieselsplittertroggs"] = 19816,
        ["Adept der Burning Blade"] = 19816,
    },
    ["Besudeltes Blut"] = {
        Pet = 19660,
    },
    ["Lavaexplosion E beschwören"] = {
        ["Kernratte"] = 21903,
    },
    ["Rasender Befehl"] = {
        ["Vorarbeiter der Flusspfoten"] = 3136,
        ["Sergeant Geiferkralle"] = 3136,
    },
    ["Großes grünes Raketenbündel"] = {
        Professions = 26427,
    },
    ["Zauberöl"] = {
        Professions = 25128,
    },
    ["Schreckliches Kreischen"] = {
        ["Todessänger"] = 6605,
        ["Seuchenfledermaus"] = 6605,
        ["Pterrordax"] = 6605,
        ["Traumsucher"] = 6605,
    },
    ["Hochentwickelte Zielattrappe"] = {
        Professions = 3965,
    },
    ["Stiefel - Willenskraft"] = {
        Professions = 20024,
    },
    ["Gedankenschlag"] = {
        ["PRIEST"] = 10947,
        ["Strashazsirene"] = 15587,
        ["Twilight-Lord Kelris"] = 15587,
        ["Spukvision"] = 13860,
    },
    ["Hydroling beschwören"] = {
        ["Hydroling"] = 22714,
    },
    ["Teleportieren: Moonglade"] = {
        ["DRUID"] = 18960,
    },
    ["Armschiene - Schwache Stärke"] = {
        Professions = 7782,
    },
    ["Versteinern"] = {
        ["Glashautversteinerer"] = 11020,
    },
    ["Fluch der Stämme"] = {
        ["Verfluchter Zentaur"] = 21048,
    },
    ["Großer Frostschutztrank"] = {
        Professions = 17575,
    },
    ["Schlangenbiss"] = {
        ["HUNTER"] = 25295,
    },
    ["Gebet der Heilung"] = {
        ["PRIEST"] = 25316,
    },
    ["Fluch verstärken"] = {
        ["WARLOCK"] = 18288,
    },
    ["Magiebannung"] = {
        ["PRIEST"] = 988,
    },
    ["Wollcape"] = {
        Professions = 2402,
    },
    ["Parieren"] = {
        ["SHAMAN"] = 16268,
    },
    ["Magie dämpfen"] = {
        ["MAGE"] = 10174,
    },
    ["Genaues Zielfernrohr"] = {
        Professions = 3979,
    },
    ["Unsichtbarkeit entdecken"] = {
        ["WARLOCK"] = 2970,
    },
    ["Hellaugenschutzbrille"] = {
        Professions = 12587,
    },
    ["Segen der Rettung"] = {
        ["PALADIN"] = 1038,
    },
    ["Grüne Leinenarmschienen"] = {
        Professions = 3841,
    },
    ["Malathroms Diener"] = {
        ["Schlamm"] = 3537,
        ["Lord Malathrom"] = 3537,
    },
    ["Echtsilber-Umwandler"] = {
        Professions = 23071,
    },
    ["Opferung"] = {
        Pet = 19443,
    },
    ["Runenverzierte Kupferbrustplatte"] = {
        Professions = 2667,
    },
    ["Alchimistenstein"] = {
        Professions = 17632,
    },
    ["Fluch der Machtlosigkeit"] = {
        ["Schwindler der Wildhufe"] = 22371,
        ["Blutdiener von Kirtonos"] = 22371,
    },
    ["Gift"] = {
        ["Waldspinne"] = 11918,
        ["Minenspinne"] = 11918,
        ["Morgaine die Verschlagene"] = 11918,
        ["Giftzahnbrutmutter"] = 11918,
        ["Nachtwebermatriarchin"] = 11918,
        ["Githyiss die Üble"] = 11918,
        ["Waldweberlauerer"] = 11918,
        ["Waldwebergiftzahn"] = 11918,
        ["Waldweberseidenspinner"] = 11918,
        ["Riesige Waldweberspinne"] = 11918,
        ["Übler Grimmling"] = 11918,
        ["Rasselnder Skorpid"] = 11918,
        ["Verderbter Skorpid"] = 11918,
        ["Sarkoth"] = 11918,
        ["Murlocgezeitenjäger"] = 744,
        ["Giftweberspinne"] = 744,
        ["Tollwütiger Gnoll der Schattenfelle"] = 744,
        ["Tarantel"] = 744,
        ["Große Tarantel"] = 744,
        ["Zwerggiftweberspinne"] = 744,
        ["Grüner Eremit"] = 744,
        ["Auftragsmörder der Schattenfelle"] = 744,
        ["Aasfresser der Bloodscalp"] = 744,
        ["Trauerspinner"] = 744,
        ["Futterwühler der Salzschuppen"] = 744,
        ["Nachtklinge der Defias"] = 744,
        ["Besudelter Nachtheuler"] = 744,
        ["Junges einer Schwarzen Witwe"] = 744,
        ["Rohh der Schweigsame"] = 744,
        ["Futterwühler der Blaukiemen"] = 744,
        ["Sumpfläufer der Dragonmaw"] = 744,
        ["Moorlord"] = 744,
        ["Klippenlauerer"] = 744,
        ["Holzlauerer"] = 744,
        ["Waldlauerer"] = 744,
        ["Heimtückischer Nachtweber"] = 744,
        ["Moospirscher"] = 744,
        ["Nebelkrabbler"] = 744,
        ["Gezeitenjäger der Grauflossen"] = 744,
        ["Dieb des Syndikats"] = 744,
        ["Auftragsmörder des Syndikats"] = 744,
        ["Schurke des Syndikats"] = 744,
        ["Schattenhafter Auftragsmörder"] = 744,
        ["Ebenenkrabbler"] = 744,
        ["Riesiger Ebenenkrabbler"] = 744,
        ["Lieferant der Dunkeleisenzwerge"] = 744,
        ["Wassersucher der Borstennacken"] = 744,
        ["Schwindler der Herzschinder"] = 744,
        ["Spitzel der Verlassenen"] = 744,
        ["Wilddornlauerer"] = 744,
        ["Kluftmooskrabbler"] = 744,
        ["Junge Prachtschwinge"] = 744,
        ["Prachtschwingenflügeldrache"] = 744,
        ["Prachtschwingenhimmelsjäger"] = 744,
        ["Prachtschwingenpatriarch"] = 744,
        ["Steilhangflügeldrache"] = 744,
        ["Steilhangkonsort"] = 744,
        ["Steilhangpatriarch"] = 744,
        ["Graunebelspinne"] = 744,
        ["Dunkelzahnlauerer"] = 744,
        ["Dunkelzahnspinne"] = 744,
        ["Riesige Dunkelzahnspinne"] = 744,
        ["Futterwühler der Düsterkiemen"] = 744,
        ["Schwindler der Zornsäer"] = 744,
        ["Slims Freund"] = 744,
        ["Deviatotter"] = 744,
        ["Wespe der Centipaar"] = 744,
        ["Glasnetzspinne"] = 744,
        ["Gutspill"] = 744,
        ["Großer Wellenhauer"] = 744,
        ["Grabmalsatanskreatur"] = 744,
        ["Auftragsmörder der Galak"] = 744,
        ["Arikara"] = 744,
        ["Bazzalan"] = 744,
        ["Felspirscher"] = 744,
    },
    ["Kräuterkundigenhandschuhe"] = {
        Professions = 9146,
    },
    ["Wächterhose"] = {
        Professions = 7147,
    },
    ["Blutdurst"] = {
        ["WARRIOR"] = 23894,
    },
    ["Toxinspeichel"] = {
        ["Sabbernder Ghul"] = 7125,
    },
    ["Strahlende Stiefel"] = {
        Professions = 16656,
    },
    ["Waffe - Geringer Elementartöter"] = {
        Professions = 13655,
    },
    ["Schwerer Skorpidhelm"] = {
        Professions = 19088,
    },
    ["Schwacher Rekombobulator"] = {
        Professions = 3952,
    },
    ["Säureschleim"] = {
        ["Klebriger Brühschlammer"] = 14147,
    },
    ["Stiefel - Große Beweglichkeit"] = {
        Professions = 20023,
    },
    ["Chaotischer Fokus"] = {
        ["Teufelsanbeter der Wildhufe"] = 22418,
    },
    ["Schattenzwirnmaske"] = {
        Professions = 12086,
    },
    ["Wutgeheul"] = {
        Pet = 24597,
        ["Kojotenrudelanführer"] = 3149,
        ["Schwarzer Verheerermastiff"] = 3149,
    },
    ["Der Mörser: Wieder aufgeladen"] = {
        Professions = 13240,
    },
    ["Erheblicher Heiltrank"] = {
        Professions = 17556,
    },
    ["Arkane Intelligenz"] = {
        ["MAGE"] = 10157,
    },
    ["Liebesrausch"] = {
        ["Qixi Q. Pido"] = 27572,
    },
    ["Gedankenbenebelndes Gift III"] = {
        ["ROGUE"] = 11400,
    },
    ["Schwerer Ledermunitionsbeutel"] = {
        Professions = 9194,
    },
    ["Kürschnerei"] = {
        Professions = 8617,
        Professions = 8618,
        Professions = 10768,
    },
    ["Werwolfschrecken rufen"] = {
        ["Werwolfschrecken"] = 7489,
    },
    ["Magie entdecken"] = {
        ["MAGE"] = 2855,
    },
    ["Eisenstrebe"] = {
        Professions = 3958,
    },
    ["Leichter Lederköcher"] = {
        Professions = 9060,
    },
    ["Grüne Lederarmschienen"] = {
        Professions = 3776,
    },
    ["Flammenpuffer"] = {
        ["Seher der Burning Blade"] = 9658,
    },
    ["Schlangenexplosionsfeuerwerk"] = {
        Professions = 23507,
    },
    ["Xabraxxis-Dämonentasche"] = {
        ["Xabraxxis"] = 19127,
    },
    ["Straucheln"] = {
        ["Kränklicher Flüchtling"] = 101,
        ["Schneider von Hillsbrad"] = 101,
    },
    ["Weitreichende Stöße"] = {
        ["WARRIOR"] = 12292,
        ["Auferstandenes Konstrukt"] = 18765,
        ["Drek'Thar"] = 18765,
    },
    ["Echtsilberchampion"] = {
        Professions = 10015,
    },
    ["Ausbrennende Flammen"] = {
        ["Uralter Kernhund"] = 19366,
    },
    ["Zuschlagen"] = {
        ["WARRIOR"] = 6554,
    },
    ["Flügelschlag"] = {
        ["Somnus"] = 12882,
    },
    ["Gezähmte Hyäne beschwören"] = {
        ["Gezähmte Hyäne"] = 8276,
        ["Bestienmeister von Razorfen"] = 8276,
    },
    ["Verderbte Naturgewalten"] = {
        ["Verderbte Naturgewalt"] = 21968,
    },
    ["Moorbewohner beschwören"] = {
        ["Moorbewohner"] = 8857,
        ["Moorlord"] = 8857,
    },
    ["Gürtel des Hügelwächters"] = {
        Professions = 3767,
    },
    ["Nefarians Barriere"] = {
        ["Lord Victor Nefarius"] = 22663,
    },
    ["Lavaexplosion D beschwören"] = {
        ["Kernratte"] = 21902,
    },
    ["Dunkeleisenpulverisierer"] = {
        Professions = 15292,
    },
    ["Gegenwart des Todes"] = {
        ["Morbent Fel"] = 3109,
    },
    ["Kampfbefehl"] = {
        ["Vorarbeiter der Defias"] = 5115,
        ["Großknecht von Hillsbrad"] = 5115,
        ["Zuchtmeister der Dunkeleisenzwerge"] = 5115,
        ["Großknecht von Nethergarde"] = 5115,
        ["Blutsucherreiter"] = 5115,
    },
    ["Großes bronzenes Messer"] = {
        Professions = 3491,
    },
    ["Schwere Mithrilaxt"] = {
        Professions = 9993,
    },
    ["Antimagieschild"] = {
        ["Shadowfang-Mondwandler"] = 7121,
    },
    ["Finte"] = {
        ["ROGUE"] = 25302,
    },
    ["Van Cleefs Verbündete"] = {
        ["Lump der Defias"] = 5200,
        ["Edwin van Cleef"] = 5200,
    },
    ["Explodierendes Schaf"] = {
        Professions = 3955,
    },
    ["Rotes Magiestoffstirnband"] = {
        Professions = 12084,
    },
    ["Murlocschuppenbrustplatte"] = {
        Professions = 6703,
    },
    ["Feste Skorpidarmschienen"] = {
        Professions = 10533,
    },
    ["Schlaf"] = {
        ["Traumwandlerwyrmkin"] = 15970,
        ["Herbeizauberer des Syndikats"] = 15970,
        ["Priesterin der Rächerflossen"] = 15970,
        ["Somnus"] = 20989,
        ["Abtrünniger Schläfer"] = 8399,
        ["Hochinquisitor Fairbanks"] = 8399,
        ["Twilight-Lord Kelris"] = 8399,
    },
    ["Goblin-Raketentreibstoff"] = {
        Professions = 11456,
    },
    ["Behände Lederhandschuhe"] = {
        Professions = 9074,
    },
    ["Wundgift III"] = {
        ["ROGUE"] = 13229,
    },
    ["Lebendige Gamaschen"] = {
        Professions = 19078,
    },
    ["Segen des Kriegshäuptlings"] = {
        ["Thrall"] = 16609,
        ["Herold von Thrall"] = 16609,
    },
    ["Agammagans Kraft"] = {
        ["Quetschzahn"] = 16612,
    },
    ["Wollstoffballen"] = {
        Professions = 2964,
    },
    ["Tödliches Gift III"] = {
        ["ROGUE"] = 11357,
    },
    ["Wankelmut"] = {
        ["Kahler Worg"] = 7127,
    },
    ["Tollkühnheit"] = {
        ["WARRIOR"] = 1719,
    },
    ["Steinhaut"] = {
        ["Gesprungener Golem"] = 5810,
        ["Steinungetüm"] = 5810,
    },
    ["Elixier der Seelenstärke"] = {
        Professions = 3450,
    },
    ["Hervorragendes Manaöl"] = {
        Professions = 25130,
    },
    ["Runenverzierter stygischer Gürtel"] = {
        Professions = 24902,
    },
    ["Astraler Rückruf"] = {
        ["SHAMAN"] = 556,
    },
    ["Großer Mystikerzauberstab"] = {
        Professions = 14810,
    },
    ["Aspekt des Rudels"] = {
        ["HUNTER"] = 13159,
    },
    ["Hervorragendes Zauberöl"] = {
        Professions = 25129,
    },
    ["Elementarfeuer transmutieren"] = {
        Professions = 25146,
    },
    ["Runenstoffhose"] = {
        Professions = 18438,
    },
    ["Rote Magiestoffhandschuhe"] = {
        Professions = 12066,
    },
    ["Liebevoll hergestelltes Schießeisen"] = {
        Professions = 3939,
    },
    ["Brust - Erhebliches Mana"] = {
        Professions = 20028,
    },
    ["Sayges dunkles Schicksal der Rüstung"] = {
        ["Sayge"] = 23767,
    },
    ["Bodenzerkracher"] = {
        ["Grol der Zerstörer"] = 12734,
        ["Obsidion"] = 12734,
        ["Gor'tesh"] = 12734,
    },
    ["Wässrige Quintessenz herstellen"] = {
        ["Fürst Hydraxis"] = 21357,
    },
    ["Totem der Erdbindung"] = {
        ["SHAMAN"] = 2484,
        ["Totem der Erdbindung"] = 15786,
        ["Irondeep-Schamane"] = 15786,
        ["Primalist Thurloga"] = 15786,
    },
    ["Tödlicher Bronzepoignard"] = {
        Professions = 3295,
    },
    ["Schützen-Treffer"] = {
        ["Schütze der Dunkeleisenzwerge"] = 12198,
    },
    ["Verkrüppelnder Schnitt"] = {
        ["Nelson der Nette"] = 23279,
    },
    ["Schildkrötenschuppengamaschen"] = {
        Professions = 10556,
    },
    ["Lähmendes Toxin"] = {
        ["Naga der Zackenkämme"] = 7947,
        ["Krieger der Zackenkämme"] = 7947,
        ["Myrmidone der Zackenkämme"] = 7947,
        ["Klingenschwanz der Zackenkämme"] = 7947,
        ["Deviatviper"] = 7947,
        ["Toxinschrecken"] = 7947,
    },
    ["Elixier der verbesserten Wasseratmung"] = {
        Professions = 22808,
    },
    ["Blitzschlagbarriere"] = {
        ["Schwester Hasspeitsche"] = 6960,
    },
    ["Säure von Hakkar"] = {
        ["Brut von Hakkar"] = 12280,
    },
    ["Rohheit"] = {
        ["Heimtückischer Grell"] = 5515,
        ["Furie der Blutfedern"] = 5515,
    },
    ["Mondstoffreif"] = {
        Professions = 18452,
    },
    ["Wutanfall"] = {
        ["DRUID"] = 5229,
        ["Schläger der Schattenfelle"] = 8599,
        ["Krieger der Bloodscalp"] = 8599,
        ["Späher der Bloodscalp"] = 8599,
        ["Jäger der Bloodscalp"] = 8599,
        ["Berserker der Bloodscalp"] = 8599,
        ["Hexendoktor der Bloodscalp"] = 8599,
        ["Kopfjäger der Bloodscalp"] = 8599,
        ["Axtwerfer der Bloodscalp"] = 8599,
        ["Schamane der Bloodscalp"] = 8599,
        ["Bestienmeister der Bloodscalp"] = 8599,
        ["Mystiker der Bloodscalp"] = 8599,
        ["Aasfresser der Bloodscalp"] = 8599,
        ["Targorr der Schreckliche"] = 8599,
        ["Moonrage-Dunkelseele"] = 8599,
        ["Wilder Yeti"] = 8599,
        ["Grunzer von Orgrimmar"] = 8599,
        ["Vollstrecker des dunklen Strangs"] = 8599,
        ["Schurke der Zornsäer"] = 8599,
        ["Schwindler der Zornsäer"] = 8599,
        ["Teufelsanbeter der Zornsäer"] = 8599,
        ["Verräter der Zornsäer"] = 8599,
        ["Schattenpirscher der Zornsäer"] = 8599,
        ["Höllenrufer der Zornsäer"] = 8599,
        ["Tobende Donnerechse"] = 8599,
        ["Siechender Krieger"] = 8599,
        ["Siechender Häscher"] = 8599,
        ["Siechende Stachelwache"] = 8599,
        ["Siechender Speerträger"] = 8599,
        ["Mooshufrenner"] = 8599,
        ["Wutklauenbär"] = 8599,
        ["Wutklauengrizzly"] = 8599,
        ["Wutklauenraufer"] = 8599,
        ["Tobender Moonkin"] = 8599,
        ["Xabraxxis"] = 8599,
        ["Arikara"] = 8599,
        ["Draka"] = 8599,
        ["Duros"] = 8599,
        ["Wütender Riffkriecher"] = 8599,
        ["Zerfetzerin der Blutfurien"] = 8599,
        ["Splintertree-Räuber"] = 8599,
        ["Elitesoldat der Kor'kron"] = 8599,
        ["Iceblood-Marshal"] = 8599,
        ["Turmstellungs-Marshal"] = 8599,
        ["Ostmarshal der Frostwolf"] = 8599,
        ["Westmarshal der Frostwolf"] = 8599,
        ["Dun Baldar Nord-Kriegsmeister"] = 8599,
        ["Dun Baldar Süd-Kriegsmeister"] = 8599,
        ["Icewing-Kriegsmeister"] = 8599,
        ["Stonehearth-Kriegsmeister"] = 8599,
        ["Krieger der Gurubashi"] = 18501,
        ["Herod"] = 8269,
        ["Scharlachroter Myrmidone"] = 8269,
        ["Scharlachroter Abt"] = 8269,
        ["Agathelos der Tobende"] = 8269,
        ["Tobender Agam'ar"] = 8269,
        ["Bestrahlter Plünderer"] = 8269,
        ["Drek'Thar"] = 8269,
        ["Held der Blackrock"] = 3019,
        ["Schattenzauberer der Blackrock"] = 3019,
        ["Renegat der Blackrock"] = 3019,
        ["Grunzer der Blackrock"] = 3019,
        ["Kundschafter der Blackrock"] = 3019,
        ["Tharil'zun"] = 3019,
        ["Fährtenleser der Blackrock"] = 3019,
        ["Kreuzbrecher der Felsenkiefer"] = 3019,
        ["Berserkertrogg"] = 3019,
        ["Berserker der Witherbark"] = 3019,
        ["Grunzer von Hammerfall"] = 3019,
        ["Späher der Blackrock"] = 3019,
        ["Wachposten der Blackrock"] = 3019,
        ["Jäger der Blackrock"] = 3019,
        ["Beschwörer der Blackrock"] = 3019,
        ["Gladiator der Blackrock"] = 3019,
    },
    ["Wiedergeburt"] = {
        ["DRUID"] = 20748,
    },
    ["Zerfetzen"] = {
        ["DRUID"] = 9896,
    },
    ["Barkskin Effect (DND)"] = {
        ["DRUID"] = 22839,
    },
    ["Kurzens Gedankensklaven beschwören"] = {
        ["Kurzens Gedankensklave"] = 8813,
        ["Mogh der Unsterbliche"] = 8813,
    },
    ["Seelenedelstein herstellen"] = {
        ["Kin'weelay"] = 3660,
    },
    ["Doppeltgenähte Wollschultern"] = {
        Professions = 3848,
    },
    ["Wuttrank"] = {
        Professions = 6617,
    },
    ["Thoriumschildstachel"] = {
        Professions = 16651,
    },
    ["Zielattrappe Spawneffekt"] = {
        ["Zielattrappe"] = 4507,
        ["Hochentwickelte Zielattrappe"] = 4507,
        ["Meisterliche Zielattrappe"] = 4507,
        ["Liebesschwindler"] = 4507,
    },
    ["Gnomen-Gedankenkontrollkappe"] = {
        Professions = 12907,
    },
    ["Armschiene - Überragende Willenskraft"] = {
        Professions = 20009,
    },
    ["Totem der beruhigenden Winde"] = {
        ["SHAMAN"] = 25908,
    },
    ["Gefiederte Brustplatte"] = {
        Professions = 10647,
    },
    ["Waffe - Schlagen"] = {
        Professions = 13693,
    },
    ["Höllenfeuer"] = {
        ["WARLOCK"] = 11684,
    },
    ["Schloss knacken"] = {
        ["ROGUE"] = 1804,
    },
    ["Kräutersammeln"] = {
        Professions = 2368,
        Professions = 3570,
        Professions = 11993,
    },
    ["Massiver Ausbruch"] = {
        ["Feueranbeter"] = 20483,
    },
    ["Arkanitrute"] = {
        Professions = 20201,
    },
    ["Verwunden"] = {
        ["WARRIOR"] = 11574,
    },
    ["Handschuhe - Stärke"] = {
        Professions = 13887,
    },
    ["Druidenschlummer"] = {
        ["Boahn"] = 8040,
        ["Druide des Giftzahns"] = 8040,
    },
    ["Thoriumgürtel"] = {
        Professions = 16643,
    },
    ["Schädelkracher"] = {
        ["Buddler der Bruchschnauzen"] = 3551,
    },
    ["Goblin-Landmine"] = {
        Professions = 3968,
    },
    ["Frostnova"] = {
        ["MAGE"] = 10230,
        ["Baron Aquanis"] = 15531,
        ["Strashazzauberhexerin"] = 15532,
        ["Murlocgezeitenjäger"] = 11831,
        ["Eliza"] = 11831,
        ["Balgaras der Niederträchtige"] = 11831,
        ["Gezeitenrufer der Finsterflossen"] = 11831,
        ["Baron Vardus"] = 11831,
        ["Magus der Felsfäuste"] = 11831,
        ["Kuz"] = 11831,
        ["Zauberhexerin der Rächerflossen"] = 11831,
        ["Beschmutzter Wasserelementar"] = 11831,
        ["Sirene der Grollflossen"] = 11831,
        ["Tideress"] = 11831,
        ["Gezeitenjäger der Finsterflossen"] = 12748,
        ["Wassersucher der Borstennacken"] = 12748,
        ["Rachsüchtige Woge"] = 14907,
    },
    ["Nekrotisches Gift"] = {
        ["Maexxnaspinnling"] = 28776,
    },
    ["Schild - Geringe Ausdauer"] = {
        Professions = 13631,
    },
    ["Riss herstellen"] = {
        ["Tabetha"] = 9079,
    },
    ["Wundgift IV"] = {
        ["ROGUE"] = 13230,
    },
    ["Wahnsinnig"] = {
        ["Wilder Grell"] = 5915,
        ["Feuerrufer Radison"] = 5915,
    },
    ["Zuls Aura herstellen"] = {
        ["Yeh'kinya"] = 24186,
    },
    ["Gottesschild"] = {
        ["PALADIN"] = 1020,
    },
    ["Meucheln"] = {
        ["ROGUE"] = 25300,
        ["Thuros Lightfingers"] = 7159,
        ["Pfadpirscher der Defias"] = 7159,
        ["Morgan der Sammler"] = 7159,
        ["Schleicher der Defias"] = 7159,
        ["Ausplünderer der Defias"] = 7159,
        ["Aasfresser der Bloodscalp"] = 7159,
        ["Murloclauerer"] = 7159,
        ["Wegelagerer der Felsenkiefer"] = 7159,
        ["Balgabzieher der Frostmane"] = 7159,
        ["Matschkrabbler der Finsterflossen"] = 7159,
        ["Inhaftierter Defias"] = 7159,
        ["Schleicher des Syndikats"] = 7159,
        ["Dieb des Syndikats"] = 7159,
        ["Spion des Syndikats"] = 7159,
        ["Eindringling der Verlassenen"] = 7159,
        ["Analysator von Nethergarde"] = 7159,
        ["Pirscher von Razorfen"] = 7159,
        ["Leibwache der Defias"] = 7159,
        ["Schneeblinde Wegelagerin"] = 7159,
    },
    ["Totem der Erdung"] = {
        ["SHAMAN"] = 8177,
    },
    ["Zurechtstutzen"] = {
        ["HUNTER"] = 14268,
    },
    ["Geist von Mondklaue"] = {
        ["Geist von Mondklaue"] = 18986,
        ["Mondklaue"] = 18986,
    },
    ["Magiewiderstandtrank"] = {
        Professions = 11453,
    },
    ["Zerfleischen"] = {
        ["Schwarzer Verheerer"] = 13443,
        ["Futterwühler der Salzschuppen"] = 13443,
        ["Alter Schlingendorntiger"] = 13443,
        ["Schwarzer Verheerermastiff"] = 13443,
        ["Futterwühler der Düsterkiemen"] = 13443,
        ["Arkkoranzange"] = 13443,
        ["Ravasaurusjäger"] = 13443,
        ["Eisenschnabeljäger"] = 13443,
        ["Tobender Moonkin"] = 13443,
        ["Bayne"] = 13443,
        ["Frostwolf"] = 13443,
        ["Alteracwidder"] = 13443,
        ["Kaltschnappzange"] = 13443,
        ["Wirbelschredder"] = 13443,
        ["Ursangus"] = 13443,
        ["Blutmaul"] = 13445,
        ["Zerfetzerin der Blutfurien"] = 13445,
        ["Schattumbra"] = 13445,
        ["Myrmidone der Rächerflossen"] = 11977,
        ["Krieger der Zackenkämme"] = 11977,
        ["Gefreiter Hendel"] = 11977,
        ["Arnak Grimmtotem"] = 11977,
        ["Legionär der Frostwolf"] = 11977,
        ["Torek"] = 11977,
        ["Krieger der Silverwing"] = 11977,
        ["Sergeant Yazra Bloodsnarl"] = 11977,
        ["Strashazschlangenwache"] = 16509,
        ["Coldmine-Eindringling"] = 16509,
        ["Schwadronskommandant Ichman"] = 16509,
        ["Schwadronskommandant Slidore"] = 16509,
        ["Manapirscher von Hederine"] = 13738,
    },
    ["Schildhieb"] = {
        ["WARRIOR"] = 1672,
        ["Pfadpirscher der Defias"] = 11972,
        ["Gath'Ilzogg"] = 11972,
        ["Corporal Keeshan"] = 11972,
        ["Krieger der Bloodscalp"] = 11972,
        ["Krieger der Skullsplitter"] = 11972,
        ["Wachposten des Syndikats"] = 11972,
        ["Marinesoldat von Kul Tiras"] = 11972,
        ["Scharlachroter Verteidiger"] = 11972,
        ["Verteidiger von Razorfen"] = 11972,
        ["Khan Jehn"] = 11972,
        ["Wachkommandant Zalaphil"] = 11972,
        ["Königliche Schreckenswache"] = 11972,
    },
    ["Purpurroter Seidengürtel"] = {
        Professions = 8772,
    },
    ["Brust - Werte"] = {
        Professions = 13941,
    },
    ["Geringes Manaöl"] = {
        Professions = 25127,
    },
    ["Dunkellilanes Seidenhemd"] = {
        Professions = 3872,
    },
    ["Weißes Leinenhemd"] = {
        Professions = 2393,
    },
    ["Agammagans Beweglichkeit"] = {
        ["Quetschzahn"] = 17013,
    },
    ["Zweihandwaffe - Geringe Intelligenz"] = {
        Professions = 7793,
    },
    ["Flammen verstärken"] = {
        ["Dunkelwirker der Schattenschmiede"] = 9482,
    },
    ["Buße"] = {
        ["PALADIN"] = 20066,
    },
    ["Krähenhorstei stören"] = {
        ["Krähenhorstbrutwächter"] = 15746,
    },
    ["Wildledergamaschen"] = {
        Professions = 10572,
    },
    ["Hammertoes Geist beschwören"] = {
        ["Hammertoes Geist"] = 4985,
        ["Historiker Karnik"] = 4985,
    },
    ["Frostblitz"] = {
        ["MAGE"] = 25304,
        ["Abtrünniger Hexer der Defias"] = 13322,
        ["Dunkler Skelettmagier"] = 13322,
        ["Scharlachroter Neophyt"] = 13322,
        ["Lehrling von Dalaran"] = 13322,
        ["Zauberhexerin der Windfurien"] = 13322,
        ["Netherzauberhexerin"] = 20297,
        ["Skelettmagier"] = 20792,
        ["Nebelwirker der Moosfelle"] = 20792,
        ["Hexer von Dalaran"] = 20792,
        ["Erzmagier Ataeric"] = 20792,
        ["Zauberhexerin der Sturmschuppen"] = 20792,
        ["Kuz"] = 20792,
        ["Delmanis der Verhasste"] = 20792,
        ["Zauberhexerin der Rächerflossen"] = 20792,
        ["Geomant der Scherwindmine"] = 20792,
        ["Strashazzauberhexerin"] = 12737,
        ["Ratsherr von Hillsbrad"] = 20806,
        ["Sarilus Foulborne"] = 20806,
        ["Feldmesser der Bruchschnauzen"] = 20806,
        ["Eliza"] = 20819,
        ["Theurgiker von Dalaran"] = 20819,
        ["Zauberhexerin der Zackenkämme"] = 20819,
        ["Gezeitenlord der Salzschuppen"] = 20822,
        ["Baron Vardus"] = 20822,
        ["Magus der Felsfäuste"] = 20822,
        ["Feldvermesser der Trockenstoppel"] = 20822,
        ["Lord der Düsterkiemen"] = 20822,
        ["Makrinnischarrer"] = 20822,
        ["Lokholar der Eislord"] = 21369,
        ["Baron Aquanis"] = 15043,
        ["Schwacher Murlocgezeitenrufer"] = 9672,
        ["Skelettzauberhexer"] = 9672,
        ["Crushridge-Magier"] = 9672,
        ["Magus des Syndikats"] = 9672,
        ["Scharlachroter Zauberhexer"] = 9672,
        ["Adept der Totenköpfe"] = 9672,
        ["Blackfathom-Gezeitenpriesterin"] = 9672,
        ["Twilight-Aquamant"] = 9672,
        ["Drachischer Magiewirker"] = 9672,
    },
    ["Arkanitdietrich"] = {
        Professions = 19669,
    },
    ["Elixier des Riesenwuchses"] = {
        Professions = 8240,
    },
    ["Kuss der Verführerin"] = {
        ["Simone die Unauffällige"] = 23205,
    },
    ["Schildblock"] = {
        ["WARRIOR"] = 2565,
        ["Wache von Stormwind"] = 12169,
        ["Scharlachrote Leibwache"] = 12169,
        ["Pyrewood-Wachposten"] = 12169,
        ["Spektraler Verteidiger"] = 12169,
        ["Stormpike-Verteidiger"] = 12169,
        ["Gardist der Frostwolf"] = 12169,
        ["Aggi Rumblestomp"] = 12169,
        ["Erfahrener Verteidiger"] = 12169,
        ["Erfahrener Wächter"] = 12169,
        ["Verteidigerveteran"] = 12169,
        ["Wächterveteran"] = 12169,
        ["Wächterchampion"] = 12169,
        ["Verteidigerchampion"] = 12169,
        ["Corporal Noreg Stormpike"] = 12169,
    },
    ["Seelendieb"] = {
        ["WARLOCK"] = 11675,
    },
    ["Goldene Schuppenarmschienen"] = {
        Professions = 7223,
    },
    ["Schattenbefehl"] = {
        ["Lord Victor Nefarius"] = 22667,
    },
    ["Dämonische Verdammnis"] = {
        ["Artorius der Liebenswürdige"] = 23298,
    },
    ["Hammer der Gerechtigkeit"] = {
        ["PALADIN"] = 10308,
    },
    ["Bronzene Brustplatte mit Versilberung"] = {
        Professions = 2673,
    },
    ["Waffe - Schwacher Wildtiertöter"] = {
        Professions = 7786,
    },
    ["Feste Skorpidstiefel"] = {
        Professions = 10554,
    },
    ["Willen ermächtigen"] = {
        ["Kriegsherr der Felsspitzoger"] = 16171,
    },
    ["Abschreckknurren"] = {
        ["Eisbart der Alte"] = 3146,
    },
    ["Flammenschock"] = {
        ["SHAMAN"] = 29228,
        ["Primalist Thurloga"] = 15616,
        ["Garneg Brandschädel"] = 15039,
    },
    ["Imperiale Plattenschultern"] = {
        Professions = 16646,
    },
    ["Dunkeleisenplatte"] = {
        Professions = 15296,
    },
    ["Langsamer Fall"] = {
        ["MAGE"] = 130,
    },
    ["Wächterhandschuhe"] = {
        Professions = 7156,
    },
    ["Zul'Farrak Zombie ankündigen"] = {
        ["Toter Held aus Zul'Farrak"] = 10747,
        ["Zombie aus Zul'Farrak"] = 10747,
    },
    ["Schrumpfen"] = {
        ["Kurzens Kopfschrumpfer"] = 7289,
        ["Zalazane"] = 7289,
        ["Twilight-Dunkelschamane"] = 7289,
    },
    ["Schattenschutzbrille"] = {
        Professions = 3940,
    },
    ["Wirbelwind"] = {
        ["WARRIOR"] = 1680,
        ["Kurzens Elitesoldat"] = 17207,
        ["Vollstrecker des Syndikats"] = 17207,
        ["Nimar der Töter"] = 17207,
        ["Krieger der Schattenschmiede"] = 17207,
        ["Wache Jarad"] = 17207,
        ["Wache Kahil"] = 17207,
        ["Cyclonian"] = 17207,
        ["Keetar"] = 17207,
        ["Herod"] = 8989,
        ["Thelman Slatefist"] = 13736,
        ["Drek'Thar"] = 13736,
        ["Captain Galvangar"] = 13736,
        ["Iceblood-Marshal"] = 13736,
        ["Turmstellungs-Marshal"] = 13736,
        ["Ostmarshal der Frostwolf"] = 13736,
        ["Westmarshal der Frostwolf"] = 13736,
        ["Dun Baldar Nord-Kriegsmeister"] = 13736,
        ["Dun Baldar Süd-Kriegsmeister"] = 13736,
        ["Ostkriegsmeister der Frostwolf"] = 13736,
        ["Icewing-Kriegsmeister"] = 13736,
        ["Stonehearth-Kriegsmeister"] = 13736,
        ["Westkriegsmeister der Frostwolf"] = 13736,
        ["Lady Hoteshem"] = 13736,
    },
    ["Teufelsstoffhandschuhe"] = {
        Professions = 22867,
    },
    ["Frostblitz-Salve"] = {
        ["Aku'mai-Diener"] = 8398,
        ["Frostflügler der Hakkari"] = 8398,
        ["Ras Frostraunen"] = 8398,
    },
    ["Großer Naturschutztrank"] = {
        Professions = 17576,
    },
    ["Großer Heiltrank"] = {
        Professions = 7181,
    },
    ["Verdichteter Wetzstein"] = {
        Professions = 16641,
    },
    ["Kreuzfahrer-Hammer"] = {
        ["Oberster Kreuzzügler Dathrohan"] = 17286,
    },
    ["Vulkanische Brustplatte"] = {
        Professions = 19076,
    },
    ["Schleier des Schattens"] = {
        ["Terrowulf-Schattenwirker"] = 7068,
        ["Gequälter Toter"] = 7068,
        ["Fellicents Schemen"] = 7068,
        ["Vol'jin"] = 17820,
    },
    ["Kinelorys Bärengestalt"] = {
        ["Kinelory"] = 4948,
    },
    ["Totem der Erdstärke"] = {
        ["SHAMAN"] = 25361,
    },
    ["Bodenbeben"] = {
        ["Steinwüter"] = 6524,
        ["Rumpelnder Verbannter"] = 6524,
        ["Fozruk"] = 6524,
        ["Rumpler"] = 6524,
        ["Donnernder Felsklopper"] = 6524,
        ["Bodenrüttler von Razorfen"] = 6524,
        ["Steinbehüter"] = 6524,
        ["Atal'alarion"] = 6524,
        ["Lavinius"] = 6524,
        ["Gri'lek"] = 6524,
        ["Der Fürst der Splitter"] = 6524,
        ["Magmafürst Bokk"] = 6524,
    },
    ["Sengender Schmerz"] = {
        ["WARLOCK"] = 17923,
    },
    ["Stachelstich"] = {
        ["Tiefstecher"] = 14534,
    },
    ["Exorzismus"] = {
        ["PALADIN"] = 10314,
    },
    ["Dämon der Kugel beschwören"] = {
        ["Dämon der Kugel"] = 9097,
        ["Tabetha"] = 9097,
    },
    ["Waffe - Stärke"] = {
        Professions = 23799,
    },
    ["Surrendes bronzenes Dingsda"] = {
        Professions = 3942,
    },
    ["Schlafender Peon"] = {
        ["Fauler Peon"] = 17743,
    },
    ["Handschuhe - Kürschnerei"] = {
        Professions = 13698,
    },
    ["Lavaschwall-Totem"] = {
        ["Lavaschwall-Totem"] = 8264,
        ["Seher der Totenköpfe"] = 8264,
    },
    ["Toxischer Blitz"] = {
        ["Verseuchte Woge"] = 16554,
    },
    ["Untote vertreiben"] = {
        ["PALADIN"] = 10326,
    },
    ["Frostwiderstand"] = {
        ["HUNTER"] = 24478,
    },
    ["Rentierstaubeffekt"] = {
        ["Metzen das Rentier"] = 25952,
    },
    ["Mechanisches Eichhörnchen"] = {
        Professions = 3928,
    },
    ["Hurrikan"] = {
        ["DRUID"] = 17402,
    },
    ["Herz von Hakkar 'Riss' herstellen"] = {
        ["Herz von Hakkar"] = 24202,
    },
    ["Schimärengamaschen"] = {
        Professions = 19073,
    },
    ["Manaschild"] = {
        ["MAGE"] = 10193,
    },
    ["Gaeas Umarmung"] = {
        Professions = 28210,
    },
    ["Peitsche"] = {
        ["Schmetterschwanz"] = 6607,
        ["Hochlandschmetterschwanz"] = 6607,
        ["Donnerschwanzbasilisk"] = 6607,
        ["Schreitergelegemutter"] = 6607,
        ["Bluthornschmetterschwanz"] = 6607,
        ["Deviatdornenpeitscher"] = 6607,
        ["Skorpidschwanzpeitscher"] = 6607,
        ["Blutblütenpeitscher"] = 6607,
        ["Schemen von Hakkar"] = 6607,
        ["Baggerbrecher"] = 6607,
        ["Vorsha die Peitscherin"] = 6607,
    },
    ["Schnellziehköcher"] = {
        Professions = 14930,
    },
    ["Stiefel - Geringe Willenskraft"] = {
        Professions = 13687,
    },
    ["Goldener Eisenzerstörer"] = {
        Professions = 3495,
    },
    ["Fläschchen der Versteinerung"] = {
        Professions = 17634,
    },
    ["Frostrüstung"] = {
        ["MAGE"] = 7301,
    },
    ["Barbarischer Gürtel"] = {
        Professions = 3779,
    },
    ["Winterschlaf"] = {
        ["DRUID"] = 18658,
    },
    ["Erste Hilfe"] = {
        ["Sanitäterin Tamberlyn"] = 7162,
        ["Sanitäterin Helaina"] = 7162,
        ["Bruder Malach"] = 7162,
    },
    ["Berserkerwut"] = {
        ["WARRIOR"] = 18499,
    },
    ["Elixier der Verteidigung"] = {
        Professions = 3177,
    },
    ["Schwere Mithrilschulter"] = {
        Professions = 9926,
    },
    ["Spawn Bronzenen Drakoniden"] = {
        ["Bronzener Drakonid"] = 22657,
        ["Bronze Drakonid Spawner"] = 22657,
    },
    ["Umhang - Schwacher Widerstand"] = {
        Professions = 7454,
    },
    ["Geringer Magiezauberstab"] = {
        Professions = 14293,
    },
    ["Flammendeflektor"] = {
        Professions = 3944,
    },
    ["Theradrimsteinling beschwören"] = {
        ["Theradrimsteinling"] = 21057,
        ["Theradrimwächter"] = 21057,
    },
    ["Lotwils Beschwörung"] = {
        ["Lotwil Veriatus"] = 5001,
    },
    ["Sofort wirkendes Gift VI"] = {
        ["ROGUE"] = 11343,
    },
    ["Klonen"] = {
        ["Geklontes Ektoplasma"] = 7952,
        ["Verschlingendes Ektoplasma"] = 7952,
    },
    ["Knurren der Seelenstärke"] = {
        ["Alter Ashenvalebär"] = 4148,
        ["Riesiger Ashenvalebär"] = 4148,
    },
    ["Fäulnisaura"] = {
        ["Kleiner"] = 3106,
    },
    ["Zaubermachtschutzbrille Xtrem"] = {
        Professions = 12615,
    },
    ["Rakete, Gelb"] = {
        ["Pats Feuerwerkstyp - GELB"] = 26349,
    },
    ["Stahlplattenhelm"] = {
        Professions = 9935,
    },
    ["Flammenstachel"] = {
        ["Lo'Grosh"] = 8814,
        ["Blutmagier Thalnos"] = 8814,
        ["Geomantin der Totenköpfe"] = 8814,
        ["Garneg Brandschädel"] = 6725,
        ["Schwester Sichelschwinge"] = 6725,
    },
    ["Barbarische Leinenweste"] = {
        Professions = 2395,
    },
    ["Arantirs Ärger"] = {
        ["Arantir"] = 9733,
    },
    ["Elixier der Frostmacht"] = {
        Professions = 21923,
    },
    ["Heilung"] = {
        ["Bastionssanitäter"] = 22167,
    },
    ["Purpurroten Scharfschützen beschwören"] = {
        ["Scharlachroter Scharfschütze"] = 17279,
    },
    ["Feuerwasser der Winterfelle"] = {
        ["Höhlenbehüter der Winterfelle"] = 17205,
        ["Totemiker der Winterfelle"] = 17205,
        ["Pfadfinder der Winterfelle"] = 17205,
    },
    ["Elementare aufspüren"] = {
        ["HUNTER"] = 19880,
    },
    ["Rakete, Rot"] = {
        ["Feiernder von Stormwind"] = 26347,
        ["Feiernder von Thunder Bluff"] = 26347,
        ["Pats Feuerwerkstyp - ROT"] = 26347,
        ["Feiernder von Darnassus"] = 26347,
        ["Feiernder von Ironforge"] = 26347,
        ["Feiernder von Undercity"] = 26347,
        ["Feiernder von Orgrimmar"] = 26347,
    },
    ["Weißes Wollkleid"] = {
        Professions = 8467,
    },
    ["Unglaublicher Gestank"] = {
        ["Satyr der Teufelshufe"] = 6942,
        ["Schurke der Teufelshufe"] = 6942,
        ["Teufelsanbeter der Teufelshufe"] = 6942,
        ["Schattenpirscher der Teufelshufe"] = 6942,
    },
    ["Silithidenkrabblerei"] = {
        ["Silithidkrabblerei"] = 6587,
        ["Silithidkrabbler"] = 6587,
    },
    ["Fluch von Shahram"] = {
        ["Shahram"] = 16597,
    },
    ["Stampfen"] = {
        ["Schwerer Kriegsgolem"] = 12612,
    },
    ["Gabe der Wildnis"] = {
        ["DRUID"] = 21850,
    },
    ["Kupferrute"] = {
        Professions = 6217,
    },
    ["Siegel des Lichts"] = {
        ["PALADIN"] = 20349,
    },
    ["Overdrive"] = {
        ["Warsongschredder"] = 18546,
    },
    ["Szepter von Celebras herstellen"] = {
        ["Celebras der Erlöste"] = 21939,
    },
    ["Verzehrende Wut"] = {
        ["Beschworener Leerwandler"] = 7750,
    },
    ["Aschenstoffhose"] = {
        Professions = 18434,
    },
    ["Giftiger Atem"] = {
        ["Ysondre"] = 24818,
        ["Lethon"] = 24818,
        ["Emeriss"] = 24818,
        ["Taerar"] = 24818,
    },
    ["Segen des Refugiums"] = {
        ["PALADIN"] = 20914,
    },
    ["Umhang - Geringe Beweglichkeit"] = {
        Professions = 13882,
    },
    ["Siegel der Rechtschaffenheit"] = {
        ["PALADIN"] = 20293,
    },
    ["Aura des Feuerwiderstands"] = {
        ["PALADIN"] = 19900,
    },
    ["Gegenschlag"] = {
        ["WARRIOR"] = 20230,
    },
    ["Verborgenes aufspüren"] = {
        ["HUNTER"] = 19885,
    },
    ["Großes blaues Raketenbündel"] = {
        Professions = 26426,
    },
    ["Großer Feuerschutztrank"] = {
        Professions = 17574,
    },
    ["Eisengegengewicht"] = {
        Professions = 7222,
    },
    ["Welkberührung"] = {
        ["Siechende Stachelwache"] = 11442,
        ["Lebender Verfall"] = 11442,
        ["Troll der Witherbark"] = 4974,
        ["Axtwerfer der Witherbark"] = 4974,
        ["Kopfjäger der Witherbark"] = 4974,
        ["Berserker der Witherbark"] = 4974,
    },
    ["Eisenschildstachel"] = {
        Professions = 7221,
    },
    ["Große blaue Rakete"] = {
        Professions = 26420,
    },
    ["Robe des Erzmagiers"] = {
        Professions = 18457,
    },
    ["Skorpidgift"] = {
        Pet = 24587,
    },
    ["Reisegestalt"] = {
        ["DRUID"] = 783,
    },
    ["Zweihandwaffe - Einschlag"] = {
        Professions = 13695,
    },
    ["Schwärzliche Stiefel"] = {
        Professions = 9207,
    },
    ["Lavaexplosion I beschwören"] = {
        ["Kernratte"] = 21907,
    },
    ["Thules Wut"] = {
        ["Knurrmähne"] = 3387,
    },
    ["Geisterwolf"] = {
        ["SHAMAN"] = 2645,
    },
    ["Kupferner Kettengürtel"] = {
        Professions = 2661,
    },
    ["Vergehen"] = {
        ["Abgesandter Roman'khan"] = 23772,
    },
    ["Strahlende Brustplatte"] = {
        Professions = 16648,
    },
    ["Zauberstein herstellen (groß)"] = {
        ["WARLOCK"] = 17727,
    },
    ["Abbau-Schwarm"] = {
        ["Silithidernter"] = 7277,
    },
    ["Mithrilschildstachel"] = {
        Professions = 9939,
    },
    ["Parasit"] = {
        ["Blackfathom-Orakel"] = 8363,
    },
    ["Schlägel"] = {
        ["Ursal der Raufer"] = 15793,
        ["Splitterzahnraufer"] = 15793,
        ["Wutklauenraufer"] = 15793,
        ["Oberanführer Ror"] = 15793,
        ["Oberhäuptling der Winterfelle"] = 15793,
    },
    ["Kernhundstiefel"] = {
        Professions = 20853,
    },
    ["Sylvanweste"] = {
        Professions = 28480,
    },
    ["Schwaches Zauberöl"] = {
        Professions = 25124,
    },
    ["Barbarische Handschuhe"] = {
        Professions = 3771,
    },
    ["Sayges dunkles Schicksal der Willenskraft"] = {
        ["Sayge"] = 23738,
    },
    ["Frostlederumhang"] = {
        Professions = 9198,
    },
    ["Welle der Heilung"] = {
        ["SHAMAN"] = 25357,
        ["Mystiker der Bloodscalp"] = 11986,
        ["Mystiker der Skullsplitter"] = 11986,
        ["Mystiker der Moosfelle"] = 11986,
        ["Schamane der Distelfelle"] = 11986,
        ["Arkkoranorakel"] = 11986,
        ["Ragefire-Schamane"] = 11986,
        ["Orakel der Hakkari"] = 15982,
        ["Irondeep-Schamane"] = 12492,
    },
    ["Tier der Skullsplitter"] = {
        ["Panther der Skullsplitter"] = 3621,
        ["Jäger der Skullsplitter"] = 3621,
        ["Bestienmeister der Skullsplitter"] = 3621,
    },
    ["Raue bronzene Schultern"] = {
        Professions = 3328,
    },
    ["Seelenstein herstellen (groß)"] = {
        ["WARLOCK"] = 20756,
    },
    ["Flüchtige Infektion"] = {
        ["Balgaras der Niederträchtige"] = 3586,
        ["Old Murk-Eye"] = 3584,
    },
    ["Alarm-O-Bot"] = {
        Professions = 23096,
    },
    ["Ruhiger mechanischer Yeti"] = {
        Professions = 26011,
    },
    ["Blackhand-Schreckenswirker beschwören"] = {
        ["Beschworener Blackhand-Schreckenswirker"] = 15794,
        ["Blackhand-Beschwörer"] = 15794,
    },
    ["Wundgift II"] = {
        ["ROGUE"] = 13228,
    },
    ["Täuschkäfer beschwören"] = {
        ["Täuschkäfer"] = 10858,
        ["Techbot"] = 10858,
        ["Täuschkäfer"] = 10858,
    },
    ["Geprägte Lederstiefel"] = {
        Professions = 2161,
    },
    ["Totem des Feuerwiderstands"] = {
        ["SHAMAN"] = 10538,
    },
    ["Graues Wollhemd"] = {
        Professions = 2406,
    },
    ["Argentumstiefel"] = {
        Professions = 23664,
    },
    ["Glutseher - Despawn"] = {
        ["Feuerwache Glutseher"] = 16078,
    },
    ["Kröten freigeben"] = {
        ["Dschungelkröte"] = 24058,
        ["Hexendoktor der Hakkari"] = 24058,
    },
    ["Drachenschuppenbrustplatte"] = {
        Professions = 10650,
    },
    ["Kniesehne"] = {
        ["WARRIOR"] = 7373,
        ["Klauententakel"] = 26141,
        ["Skelettkrieger"] = 9080,
        ["Brack"] = 9080,
        ["Krieger der Blaukiemen"] = 9080,
        ["Lok Orcbane"] = 9080,
        ["Ursa der Fäulnisklauen"] = 9080,
        ["Aligar der Peiniger"] = 9080,
        ["Strashazkrieger"] = 9080,
        ["Krieger der Zackenkämme"] = 9080,
        ["Großknecht Grills"] = 9080,
        ["Verwirrter Aussätziger"] = 9080,
        ["Coldmine-Eindringling"] = 9080,
        ["Königliche Schreckenswache"] = 9080,
    },
    ["Gnomen-Raketenstiefel"] = {
        Professions = 12905,
    },
    ["Totem des Wachens"] = {
        ["SHAMAN"] = 6495,
    },
    ["Murlocschuppenarmschienen"] = {
        Professions = 6705,
    },
    ["Geisterzwirnweste"] = {
        Professions = 18416,
    },
    ["Ashcrombes Öffner"] = {
        ["Zauberhexer Ashcrombe"] = 6421,
    },
    ["Portal: Ironforge"] = {
        ["MAGE"] = 11416,
    },
    ["Winzige wandelnde Bombe"] = {
        Professions = 15628,
    },
    ["Illusionäres Trugbild beschwören"] = {
        ["Illusionäres Trugbild"] = 8986,
        ["Spuktrugbild"] = 8986,
    },
    ["Schattenöl"] = {
        Professions = 3449,
    },
    ["Amnennars Zorn"] = {
        ["Amnennar der Kältebringer"] = 13009,
    },
    ["Formelles weißes Hemd"] = {
        Professions = 3871,
    },
    ["Ehrenpunkte +398"] = {
        ["Kriegshetzer der Horde"] = 24923,
        ["Brigadegeneral der Allianz"] = 24923,
    },
    ["Mondfesteinladung herstellen"] = {
        ["Botin des Mondfests"] = 26375,
    },
    ["Vergiftung aufheben"] = {
        ["DRUID"] = 2893,
    },
    ["Kleiner Ledermunitionsbeutel"] = {
        Professions = 9062,
    },
    ["Elixier des Mungos"] = {
        Professions = 17571,
    },
    ["Gnomen-Kampfhuhn"] = {
        Professions = 12906,
    },
    ["Schneiderei"] = {
        Professions = 3909,
        Professions = 3910,
        Professions = 12180,
    },
    ["Trampeln"] = {
        ["Stopfwanst"] = 5568,
        ["Feindschnitter 4000"] = 5568,
        ["Gorlash"] = 5568,
        ["Bäumlingtrampler"] = 5568,
        ["Steinwüter"] = 5568,
        ["Fozruk"] = 5568,
        ["Kodobulle"] = 5568,
        ["Kodomatriarchin"] = 5568,
        ["Stampfer der Gelkis"] = 5568,
        ["Ghamoo-ra"] = 5568,
        ["Schwerer Kriegsgolem"] = 5568,
        ["Diener von Arkkoroc"] = 5568,
    },
    ["Insektenschwarm"] = {
        ["DRUID"] = 24977,
    },
    ["Modularer Stimmenverstärker"] = {
        Professions = 19819,
    },
    ["Fluch des gefallenen Magram"] = {
        ["Schreckgespenst der Magram"] = 18159,
    },
    ["Terrorknurren"] = {
        ["Bluthund"] = 13692,
        ["Dimetrodon"] = 13692,
        ["Blutläufer"] = 13692,
    },
    ["Paranoia"] = {
        Pet = 19480,
    },
    ["Schrei"] = {
        Pet = 24579,
    },
    ["Verlassene Fertigkeiten"] = {
        ["Gepeinigter Offizier"] = 7054,
    },
    ["Einlullender Schuss"] = {
        ["HUNTER"] = 19801,
    },
    ["Waffe - Feurige Waffe"] = {
        Professions = 13898,
    },
    ["Rakete, Grün Groß"] = {
        ["Pats Feuerwerkstyp - GRÜN GROSS"] = 26352,
    },
    ["Blitzschlagatem"] = {
        Pet = 25012,
        ["Eiswindschimäre"] = 15797,
    },
    ["Geringer Heiltrank"] = {
        Professions = 2337,
    },
    ["Zu Geröll zerkleinert"] = {
        ["Elementarsklave"] = 3673,
    },
    ["Schattenblitz"] = {
        ["WARLOCK"] = 25307,
        ["Morloch"] = 15537,
        ["Ausgestoßener Totenbeschwörer"] = 20298,
        ["Schattenzauberer der Frostmane"] = 20791,
        ["Herbeizauberer von Dalaran"] = 20791,
        ["Licillin"] = 20791,
        ["Schattenmagier des Syndikats"] = 20791,
        ["Lehrling der Burning Blade"] = 20791,
        ["Fizzle Dunkelsturm"] = 20791,
        ["Gazz'uz"] = 20791,
        ["Ilkrud Magthrull"] = 20791,
        ["Kultist des dunklen Strangs"] = 20791,
        ["Adept des dunklen Strangs"] = 20791,
        ["Apotheker Falthis"] = 20791,
        ["Balizar der Anstößige"] = 20791,
        ["Searingblade-Hexenmeister"] = 20791,
        ["Schattenwärter der Dragonmaw"] = 20807,
        ["Leerruferin des dunklen Strangs"] = 20807,
        ["Augur der Burning Blade"] = 20807,
        ["Buchhalter Herod"] = 20816,
        ["Schattenmagier der Arguswacht"] = 20816,
        ["Schattenzauberer der Witherbark"] = 20816,
        ["Schattenzauberer der Dunkeleisenzwerge"] = 20816,
        ["Schattenmagier der Burning Blade"] = 20816,
        ["Hexenmeister der Blutsegelbukaniere"] = 20825,
        ["Skelettakolyt"] = 20825,
        ["Dunkelwirker der Schattenschmiede"] = 20825,
        ["Beschwörer der Burning Blade"] = 20825,
        ["Initiand von Hederine"] = 15232,
        ["Dunkelwirker der Schattenfelle"] = 9613,
        ["Schattenzauberer der Blackrock"] = 9613,
        ["Schattenwirker der Nachtheuler"] = 9613,
        ["Skelettheiler"] = 9613,
        ["Kurzens Kopfschrumpfer"] = 9613,
        ["Hexendoktor von Zanzil"] = 9613,
        ["Schattengrimmling"] = 9613,
        ["Herbeizauberer des Syndikats"] = 9613,
        ["Darbel Montrose"] = 9613,
        ["Scharlachroter Augur"] = 9613,
        ["Scharlachroter Wahrsager"] = 9613,
        ["Todessprecher Jargba"] = 9613,
        ["Priester der Totenköpfe"] = 9613,
        ["Blutmagier Thalnos"] = 9613,
        ["Höllenrufer der Leidbringer"] = 9613,
        ["Twilight-Schattenmagier"] = 9613,
        ["Höllenwirker Hoohn"] = 9613,
        ["Nekromant der Totenköpfe"] = 9613,
        ["Kultistin der Totenköpfe"] = 9613,
        ["Mannoroc-Peitscher"] = 9613,
    },
    ["Verzauberter Magiestoffbeutel"] = {
        Professions = 27658,
    },
    ["Totem der Feuernova"] = {
        ["SHAMAN"] = 11315,
    },
    ["Große Ausdauer"] = {
        ["HUNTER"] = 5049,
    },
    ["Zeug abbrechen"] = {
        ["Fel'zerul"] = 7437,
    },
    ["Wolltasche"] = {
        Professions = 3757,
    },
    ["Teleportieren: Darnassus"] = {
        ["MAGE"] = 3565,
    },
    ["Dämonenhaut"] = {
        ["WARLOCK"] = 696,
        ["Schattenzauberer der Frostmane"] = 20798,
        ["Gazz'uz"] = 20798,
    },
    ["Explosivgeschoss"] = {
        ["Käpt'n Kielhol"] = 7896,
        ["Scharlachroter Bestienmeister"] = 7896,
        ["Waffentechniker"] = 7896,
        ["Verräter der Wildhufe"] = 7896,
    },
    ["Sukkubus beschwören"] = {
        ["WARLOCK"] = 712,
    },
    ["Gletscherhandschuhe"] = {
        Professions = 28205,
    },
    ["Schattenzwirnstiefel"] = {
        Professions = 12082,
    },
    ["Thoriumstiefel"] = {
        Professions = 16652,
    },
    ["Wasserwächter heraufbeschwören"] = {
        ["Wasserwächter"] = 8372,
        ["Twilight-Aquamant"] = 8372,
    },
    ["Moosbedeckte Füße"] = {
        ["Seekrabbler"] = 6870,
        ["Alter Seekrabbler"] = 6870,
        ["Schattendickichtmoosfresser"] = 6870,
    },
    ["Grimmhauer"] = {
        ["Stacheleber der Grimmhauer"] = 5280,
    },
    ["Zuchtmeister der Dunkeleisenzwerge-Tod"] = {
        ["Zuchtmeister der Dunkeleisenzwerge"] = 12613,
    },
    ["Entkräftende Berührung"] = {
        ["Hungerndes Gespenst"] = 16333,
        ["Ruheloser Schemen"] = 16333,
        ["Spektraler Bürger"] = 16333,
        ["Altersschwacher Wächter"] = 16333,
    },
    ["Gerechtigkeit des Hochlords"] = {
        ["Hochlord Bolvar Fordragon"] = 20683,
    },
    ["Schinderranke"] = {
        ["Blutblütenschinder"] = 14112,
    },
    ["Feueröl"] = {
        Professions = 7837,
    },
    ["Rend Blackhand beschwören"] = {
        ["Kriegshäuptling Rend Blackhand"] = 16328,
        ["Gyth"] = 16328,
    },
    ["Behälter von Mau'ari herstellen"] = {
        ["Hexendoktorin Mau'ari"] = 16351,
    },
    ["Schild - Geringes Blocken"] = {
        Professions = 13689,
    },
    ["Bronzenes Großschwert"] = {
        Professions = 9986,
    },
    ["Dämmerungstrickfalle erstellen"] = {
        ["Betina Bigglezink"] = 18367,
    },
    ["Helculars Marionetten beschwören"] = {
        ["Helculars Marionette"] = 4950,
        ["Helculars sterbliche Überreste"] = 4950,
    },
    ["Hexerzwirnrobe"] = {
        Professions = 18446,
    },
    ["Dickflüssigkeitsfieber"] = {
        ["Dickflüssiger Brühschlammer"] = 14130,
    },
    ["Giftbann"] = {
        ["Kayneth Stillwind"] = 6354,
    },
    ["Armschiene - Schwache Willenskraft"] = {
        Professions = 7766,
    },
    ["Totem der Steinhaut"] = {
        ["SHAMAN"] = 10408,
    },
    ["Toxin"] = {
        ["Toxischer Brühschleimer"] = 25989,
    },
    ["Großes Arkanelixier"] = {
        Professions = 17573,
    },
    ["Schneller Kampfrausch"] = {
        ["Zuchtmeister der Flusspfoten"] = 3229,
        ["Schamane der Mo'grosh"] = 3229,
        ["Grawmug"] = 3229,
    },
    ["Schild - Geringer Schutz"] = {
        Professions = 13464,
    },
    ["Gunthers Angesicht beschwören"] = {
        ["Gunthers Abbild"] = 7762,
    },
    ["Verbannen"] = {
        ["WARLOCK"] = 18647,
    },
    ["Hurtiger Wind"] = {
        ["Prate Cloudseer"] = 8385,
    },
    ["Barbarischer Eisenhelm"] = {
        Professions = 9814,
    },
    ["Frosttigerklinge"] = {
        Professions = 3497,
    },
    ["Wächterlederarmschienen"] = {
        Professions = 3777,
    },
    ["Regenerationstrank"] = {
        Professions = 11452,
    },
    ["Dampfstrahl"] = {
        ["Dampfschmied der Dunkeleisenzwerge"] = 11983,
        ["Siedender Elementar"] = 11983,
    },
    ["Transmutieren: Erde zu Wasser"] = {
        Professions = 17561,
    },
    ["Aschenstoffweste"] = {
        Professions = 18408,
    },
    ["Wasserelementar beschwören"] = {
        ["Beschworener Wasserelementar"] = 17162,
        ["Purpurroter Herbeizauberer"] = 17162,
    },
    ["Verlangsamender Brühschlammer"] = {
        ["Urzeitbrühschlammer"] = 16050,
    },
    ["Schwere Leinenhandschuhe"] = {
        Professions = 3840,
    },
    ["Elixier des Dämonentötens"] = {
        Professions = 11477,
    },
    ["Brustschmerzen"] = {
        ["Twilight-Hammer-Folterknecht"] = 6945,
        ["Marshal Windsor"] = 6945,
    },
    ["Geist anlocken"] = {
        ["Lethon"] = 24811,
    },
    ["Blackhand-Veteranen beschwören"] = {
        ["Beschworener Blackhand-Veteran"] = 15792,
        ["Blackhand-Beschwörer"] = 15792,
    },
    ["Hagelsturm"] = {
        ["Mechanofrostwandler"] = 10734,
    },
    ["Tückisches Melken"] = {
        ["Struppiger John"] = 16472,
    },
    ["Übungsschloss"] = {
        Professions = 8334,
    },
    ["Handgenähte Lederstiefel"] = {
        Professions = 2149,
    },
    ["Teufelssaurierstulpen"] = {
        Professions = 19084,
    },
    ["Kunstloses Zielfernrohr"] = {
        Professions = 3977,
    },
    ["Feuerschild II"] = {
        ["Purpurroter Welpe"] = 184,
        ["Geologe der Venture Co."] = 184,
        ["Feuertreiber der Splitterfäuste"] = 184,
        ["Garneg Brandschädel"] = 184,
        ["Geringe Teufelswache"] = 184,
        ["Brennender Verheerer"] = 184,
        ["Beschwörer der Blackrock"] = 184,
        ["Großknecht Cozzle"] = 184,
        ["Schwester Sichelschwinge"] = 184,
    },
    ["Sylvanschultern"] = {
        Professions = 28482,
    },
    ["Elixier der Riesen"] = {
        Professions = 11472,
    },
    ["Überragender Manatrank"] = {
        Professions = 17553,
    },
    ["Tier der Bloodscalp"] = {
        ["Tiger der Bloodscalp"] = 3612,
        ["Bestienmeister der Bloodscalp"] = 3612,
    },
    ["Brunnen des Lichts"] = {
        ["PRIEST"] = 27871,
    },
    ["Massive Eisenaxt"] = {
        Professions = 3498,
    },
    ["Kupferne Kettenstiefel"] = {
        Professions = 3319,
    },
    ["Schleichender Schimmelpilz"] = {
        ["Wassersucher der Grimmhauer"] = 6278,
        ["Matschkrabbler der Schlammflossen"] = 6278,
    },
    ["Schild - Erhebliche Willenskraft"] = {
        Professions = 20016,
    },
    ["Dynamit werfen"] = {
        ["Ingenieur der Venture Co."] = 7978,
        ["Buddler Flammenschmied"] = 7978,
        ["Arbeiter der Schattenschmiede"] = 7978,
    },
    ["Ablenken"] = {
        ["ROGUE"] = 1725,
    },
    ["Felsen"] = {
        ["Wyrmjäger der Staubspeier"] = 9483,
    },
    ["Waffe - Großes Schlagen"] = {
        Professions = 13943,
    },
    ["Große Eisenbombe"] = {
        Professions = 3967,
    },
    ["Schwarze Magiestoffgamaschen"] = {
        Professions = 12049,
    },
    ["Teufelsross beschwören"] = {
        ["WARLOCK"] = 5784,
    },
    ["Fehlgeschlagene Verwandlung"] = {
        ["Verwandelte Kopie"] = 28406,
        ["Krieger der Grollflossen"] = 28406,
        ["Kreischerin der Grollflossen"] = 28406,
        ["Schlangenwache der Grollflossen"] = 28406,
        ["Sirene der Grollflossen"] = 28406,
    },
    ["Schattenschock"] = {
        ["Krethis Shadowspinner"] = 17439,
        ["Beschworener Sukkubus"] = 16583,
        ["Vol'jin"] = 17289,
    },
    ["Aschenstoffhandschuhe"] = {
        Professions = 18412,
    },
    ["Naturwiderstand"] = {
        ["HUNTER"] = 24513,
    },
    ["Tödliches Egelgift"] = {
        ["Egelwitwe"] = 3388,
        ["Trauerschwinge"] = 3388,
    },
    ["Überwältigen"] = {
        ["WARRIOR"] = 11585,
    },
    ["Kupferkurzschwert"] = {
        Professions = 2739,
    },
    ["Göttlicher Hauch von Vaelastrasz"] = {
        ["Vaelastrasz der Rote"] = 16332,
    },
    ["Schneller Flammenzauberschutz"] = {
        ["Renegatenmagier der Defias"] = 4979,
        ["Mystiker der Bloodscalp"] = 4979,
        ["Skelettwärter"] = 4979,
        ["Geologe der Venture Co."] = 4979,
        ["Geomant der Splittersteintroggs"] = 4979,
        ["Geomant der Tunnelratten"] = 4979,
        ["Feuertreiber der Splitterfäuste"] = 4979,
        ["Rufer der Defias"] = 4979,
        ["Magier von Dalaran"] = 4979,
        ["Grel'borg der Horter"] = 4979,
        ["Geomant der Borstennacken"] = 4979,
        ["Hexer der Defias"] = 4979,
        ["Großknecht Cozzle"] = 4979,
    },
    ["Verderbte Intelligenz"] = {
        ["Verderbter scheckiger Eber"] = 6818,
        ["Schamane der Fäulnisklauen"] = 6818,
        ["Totemiker der Fäulnisklauen"] = 6818,
    },
    ["Anregen"] = {
        ["DRUID"] = 29166,
    },
    ["Schwere Wollhose"] = {
        Professions = 3850,
    },
    ["Tückische Lederstulpen"] = {
        Professions = 19049,
    },
    ["Windheuler beschwören"] = {
        ["Windheuler"] = 8271,
        ["Staubwirker von Razorfen"] = 8271,
    },
    ["Tödliches Zielfernrohr"] = {
        Professions = 12597,
    },
    ["Lebenslinie"] = {
        ["WARLOCK"] = 11695,
    },
    ["Zünder für Raketenbündel"] = {
        Professions = 26443,
    },
    ["Rhahk'Zor-Zerschmettern"] = {
        ["Rhahk'Zor"] = 6304,
    },
    ["Stiefel des Verzauberers"] = {
        Professions = 3860,
    },
    ["Blutung"] = {
        ["ROGUE"] = 11275,
    },
    ["Blitzstrahlbombe"] = {
        Professions = 8243,
    },
    ["Blutrausch"] = {
        ["WARRIOR"] = 2687,
    },
    ["Grollflosse"] = {
        ["Krieger der Grollflossen"] = 12545,
        ["Kreischerin der Grollflossen"] = 12545,
        ["Kampfmeister der Grollflossen"] = 12545,
    },
    ["Umhang - Große Verteidigung"] = {
        Professions = 13746,
    },
    ["Thoriumgewehr"] = {
        Professions = 19792,
    },
    ["Egelgift"] = {
        ["Egelpirscher"] = 3358,
        ["Höhlenpirscher"] = 3358,
        ["Matschkrabbler der Nachtaugen"] = 8382,
    },
    ["Sylvankrone"] = {
        Professions = 28481,
    },
    ["Flammenwerfer"] = {
        Pet = 25027,
    },
    ["Kadaver-Explosion"] = {
        ["Totenbeschwörer aus Scholomance"] = 17616,
    },
    ["Echtsilberdietrich"] = {
        Professions = 19668,
    },
    ["Bronzene Axt"] = {
        Professions = 2741,
    },
    ["Letztes Gefecht"] = {
        ["WARRIOR"] = 12975,
    },
    ["Kettenheilung"] = {
        ["SHAMAN"] = 10623,
        ["Aggem Dornfluch"] = 14900,
    },
    ["Betäubungsbombenangriff"] = {
        ["Aerie-Greif"] = 21188,
        ["Kriegsreiter"] = 21188,
        ["Guses Kriegsreiter"] = 21188,
        ["Jeztors Kriegsreiter"] = 21188,
        ["Mulvericks Kriegsreiter"] = 21188,
        ["Slidores Greif"] = 21188,
        ["Ichmans Greif"] = 21188,
        ["Vipores Greif"] = 21188,
    },
    ["Mondleuchtfeuer-Totem"] = {
        ["Mondleuchtfeuer-Totem"] = 15787,
        ["Totemiker der Winterfelle"] = 15787,
    },
    ["Qual"] = {
        Pet = 11775,
    },
    ["Schreddern"] = {
        ["DRUID"] = 9830,
        ["Schredder der Finsterflossen"] = 3252,
    },
    ["Gedankenbeben"] = {
        ["Erdenbrecher von Razorfen"] = 8272,
    },
    ["Untoten Skarabäus erwecken"] = {
        ["Untoter Skarabäus"] = 17235,
    },
    ["Bansheefluch"] = {
        ["Verfluchte Hochgeborene"] = 5884,
        ["Zuckender Hochgeborener"] = 5884,
        ["Klagende Hochgeborene"] = 5884,
        ["Anaya Dawnrunner"] = 5884,
    },
    ["Katzenaugenelixier"] = {
        Professions = 12609,
    },
    ["Blutrebenweste"] = {
        Professions = 24091,
    },
    ["Aura der Flammen"] = {
        ["Captain der Todeskrallen"] = 22436,
    },
    ["Deviatschuppenhandschuhe"] = {
        Professions = 7954,
    },
    ["Weiter Streich"] = {
        ["Ernteschnitter"] = 7342,
        ["Deviatpeitscher"] = 7342,
    },
    ["Windstoß"] = {
        ["Staubteufel"] = 6982,
        ["Windhexe der Blutfedern"] = 6982,
        ["Windsprecher der Schwarzfelle"] = 6982,
        ["Windhexe der Windfurien"] = 6982,
        ["Kreischende Windruferin"] = 6982,
        ["Schwester Wildkralle"] = 6982,
        ["Cyclonian"] = 6982,
        ["Windstoßvortex"] = 6982,
    },
    ["Lederverarbeitung"] = {
        Professions = 3104,
        Professions = 3811,
        Professions = 10662,
    },
    ["Brust - Schwaches Mana"] = {
        Professions = 7443,
    },
    ["Ablenkender Schmerz"] = {
        ["Sneeds Schredder"] = 3603,
    },
    ["Großer Wuttrank"] = {
        Professions = 6618,
    },
    ["Steinfaust"] = {
        ["Oger der Felsfäuste"] = 4955,
        ["Vollstrecker der Felsfäuste"] = 4955,
        ["Schläger der Felsfäuste"] = 4955,
        ["Raufer der Felsfäuste"] = 4955,
        ["Lord der Felsfäuste"] = 4955,
    },
    ["Mithrilhelmkappe"] = {
        Professions = 9961,
    },
    ["Stärke entziehen"] = {
        ["Blutsaftbestie"] = 7997,
    },
    ["Anspringen"] = {
        ["DRUID"] = 9827,
    },
    ["Rakete, Weiß Groß"] = {
        ["Pats Feuerwerkstyp - WEISS GROSS"] = 26355,
    },
    ["Thules Segen"] = {
        ["Zwangsarbeiter von Rabenklaue"] = 3269,
    },
    ["Springblitzschlag"] = {
        ["Sturmruferin der Maraudine"] = 9654,
    },
    ["Admiralshut"] = {
        Professions = 12081,
    },
    ["Mondpirscherwicht beschwören"] = {
        ["Mondpirscherwicht"] = 8594,
        ["Mondpirschermatriarchin"] = 8594,
    },
    ["Spawn Roten Drakoniden"] = {
        ["Roter Drakonid"] = 22655,
        ["Red Drakonid Spawner"] = 22655,
    },
    ["Leichte Lederarmschienen"] = {
        Professions = 9065,
    },
    ["Erdrosseln"] = {
        ["ROGUE"] = 11290,
    },
    ["Gespinst"] = {
        ["Giftzahnbrutmutter"] = 12023,
        ["Waldweberseidenspinner"] = 12023,
        ["Wilddornpirscher"] = 12023,
        ["Giftweberspinne"] = 745,
        ["Trauerspinner"] = 745,
        ["Kluftmoosnetzweber"] = 745,
        ["Glasnetzspinne"] = 745,
        ["Grabmalhäscher"] = 745,
        ["Baumspinnereremit"] = 745,
        ["Felspirscher"] = 745,
        ["Besseleth"] = 745,
    },
    ["Geringer Mystikerzauberstab"] = {
        Professions = 14809,
    },
    ["Transmutieren: Leben zu Erde"] = {
        Professions = 17565,
    },
    ["Faust von Shahram"] = {
        ["Shahram"] = 16601,
    },
    ["Rosa Magiestoffhemd"] = {
        Professions = 12080,
    },
    ["Blaue Drachenschuppenschultern"] = {
        Professions = 19089,
    },
    ["Mondstoffstiefel"] = {
        Professions = 19435,
    },
    ["Wasserwandeln"] = {
        ["SHAMAN"] = 546,
    },
    ["Dunkles Angebot"] = {
        ["Arugals Leerwandler"] = 7154,
    },
    ["Grünwelpenrüstung"] = {
        Professions = 9197,
    },
    ["Knolle herstellen"] = {
        ["Schnüffelnasenziesel"] = 6900,
    },
    ["Runenverzierte Kupferstulpen"] = {
        Professions = 3323,
    },
    ["Salzstreuer"] = {
        Professions = 19567,
    },
    ["Bergbau"] = {
        Professions = 2576,
        Professions = 3564,
        Professions = 10248,
    },
    ["Wucherwurzeln"] = {
        ["DRUID"] = 9853,
        ["Alter Seekrabbler"] = 11922,
        ["Dornenwirker von Razorfen"] = 11922,
        ["Wandernder Beschützer"] = 11922,
        ["Erzdruide Renferal"] = 22127,
        ["Druide des Hains"] = 22127,
        ["Dornenwirker der Borstennacken"] = 12747,
        ["Schattendickichtholzformer"] = 12747,
        ["Verkohltes Urtum"] = 12747,
        ["Bewahrer Ordanus"] = 12747,
        ["Teerfürst"] = 12747,
    },
    ["Schweres Kupferbreitschwert"] = {
        Professions = 3292,
    },
    ["Menschlichen Schädel beschwören"] = {
        ["Menschenschädel"] = 19031,
    },
    ["Blutzapfer"] = {
        ["Shadowfang-Nimmersatt"] = 7122,
    },
    ["Thoriumpatronen"] = {
        Professions = 19800,
    },
    ["Blitzschlagwolke"] = {
        ["Sturmruferin der Kolkar"] = 6535,
        ["Schattendickichtregenrufer"] = 6535,
        ["Mavoris Cloudsbreak"] = 6535,
        ["Sturmhexe der Blutfurien"] = 6535,
        ["Sturmruferin der Galak"] = 6535,
        ["Sturmruferin der Magram"] = 6535,
        ["Schwester Wildkralle"] = 6535,
        ["Sturmseher der Kolkar"] = 6535,
    },
    ["Robustes Sprengpulver"] = {
        Professions = 12585,
    },
    ["Untote fesseln"] = {
        ["PRIEST"] = 10955,
    },
    ["Gegenzauber"] = {
        ["MAGE"] = 2139,
    },
    ["Transmutieren: Luft zu Feuer"] = {
        Professions = 17559,
    },
    ["Geist von Kirith"] = {
        ["Geist von Kirith"] = 10853,
        ["Kirith der Verdammte"] = 10853,
    },
    ["Schneeballwolke"] = {
        ["Pats Schneewolkentyp"] = 26000,
    },
    ["Arglist des Raptors"] = {
        ["Gor'mul"] = 4153,
    },
    ["Schwärzlicher Gürtel"] = {
        Professions = 9206,
    },
    ["Mondsichtgewehr"] = {
        Professions = 3954,
    },
    ["Ducken"] = {
        ["DRUID"] = 9892,
        Pet = 16697,
    },
    ["Wille der Verlassenen"] = {
        Racial = 7744,
    },
    ["Baumrinde"] = {
        ["DRUID"] = 22812,
    },
    ["Eisrüstung"] = {
        ["MAGE"] = 10220,
    },
    ["Verzögerung"] = {
        ["Techbot"] = 10855,
    },
    ["Verdammniswache beschwören"] = {
        ["Diener der Verdammniswache"] = 22865,
        ["Hexenmeister der Gordok"] = 22865,
    },
    ["Robe der Leere"] = {
        Professions = 18458,
    },
    ["Bereitschaft"] = {
        ["HUNTER"] = 23989,
    },
    ["Verschnörkelte Thoriumhandaxt"] = {
        Professions = 16969,
    },
    ["Totstellen"] = {
        ["HUNTER"] = 5384,
    },
    ["Superaufladung"] = {
        ["Aussätziger Techniker"] = 10732,
        ["Aussätziger Maschinenschmied"] = 10732,
    },
    ["Handschuhe - Bergbau"] = {
        Professions = 13612,
    },
    ["Befrieden"] = {
        ["Friedensbewahrer-Sicherheitsanzug"] = 10730,
    },
    ["Einschüchterndes Knurren"] = {
        ["Pestfleckenkläffer"] = 6576,
    },
    ["Psychischer Schrei"] = {
        ["PRIEST"] = 10890,
    },
    ["Knochensplitter"] = {
        ["Wanderndes Skelett"] = 17014,
    },
    ["Fernsicht"] = {
        ["SHAMAN"] = 6196,
    },
    ["Heiliger Stoß"] = {
        ["Gebirgsjäger von Dun Garok"] = 13953,
        ["Lieutenant Heldenruf"] = 13953,
        ["Scharlachroter Bewahrer"] = 13953,
        ["Scharlachroter Held"] = 17143,
    },
    ["Teufelstampfen"] = {
        ["Teufelsross"] = 7139,
    },
    ["Einhüllende Gespinste"] = {
        ["Hohepriesterin Mar'li"] = 24110,
    },
    ["Mörserschuss"] = {
        ["Shorty"] = 16786,
    },
    ["Tritt"] = {
        ["ROGUE"] = 1769,
        ["Scharlachroter Mönch"] = 11978,
        ["Bandit der Grimmtotem"] = 11978,
        ["Untoter Verheerer"] = 11978,
    },
    ["Elixier der Weisen"] = {
        Professions = 17555,
    },
    ["Umhang - Geringer Schutz"] = {
        Professions = 13421,
    },
    ["Mithrilschuppenarmschienen"] = {
        Professions = 9937,
    },
    ["Raptorbalggürtel"] = {
        Professions = 4097,
    },
    ["Bröselnder Stein"] = {
        ["Elementarsklave"] = 3672,
    },
    ["Sengende Goldklinge"] = {
        Professions = 15973,
    },
    ["Tödliches Toxin+"] = {
        ["Klingenherz"] = 8256,
    },
    ["Schwarzwelpentunika"] = {
        Professions = 24940,
    },
    ["Armschiene - Große Ausdauer"] = {
        Professions = 13945,
    },
    ["Handschuhe des wahren Glaubens"] = {
        Professions = 8782,
    },
    ["Verdichteter Schleifstein"] = {
        Professions = 16639,
    },
    ["Geschmeidiger mittlerer Balg"] = {
        Professions = 3817,
    },
    ["Säurespucke"] = {
        ["Halbwüchsiger Welpe"] = 20821,
        ["Träumender Welpe"] = 20821,
    },
    ["Schleichen der Kundschafter von Ashenvale"] = {
        ["Kundschafter von Ashenvale"] = 20540,
    },
    ["Der Große"] = {
        Professions = 12754,
    },
    ["Tollwut"] = {
        ["Tollwütiger Gnoll der Schattenfelle"] = 3150,
        ["Tollwütiger Terrorwolf"] = 3150,
        ["Holzplanke"] = 3150,
        ["Tollwütiger Distelbär"] = 3150,
        ["Tollwütiger Klippenkojote"] = 3150,
        ["Tollwütige Skelettpfote"] = 3150,
        ["Tollwütiger Langzahn"] = 3150,
        ["Tollwütige Eiterpfote"] = 3150,
        ["Tollwütiger Splitterzahn"] = 3150,
    },
    ["Xandivious' Dämonentasche"] = {
        ["Xandivious"] = 25791,
    },
    ["Zauberschutz von Laze"] = {
        ["Zauberschutz von Laze"] = 3826,
        ["Zauberschutz von Laze"] = 3827,
        ["Hexendoktor der Mosh'Ogg"] = 3827,
    },
    ["Faultier"] = {
        ["Faultier"] = 3510,
    },
    ["Kluftmoosmatriarchin beschwören"] = {
        ["Kluftmoosmatriarchin"] = 6536,
        ["Kluftmoosjungtier"] = 6536,
    },
    ["Super Schrumpfstrahl"] = {
        ["Hochtüftler Mekkatorque"] = 22742,
    },
    ["Gedankenschinden"] = {
        ["PRIEST"] = 18807,
    },
    ["Großdrachen heilen"] = {
        ["Blackhand-Großdrachenführer"] = 16637,
    },
    ["Entzünden"] = {
        ["Hagerer Flüchtling"] = 3261,
    },
    ["Verlangsamendes Gift"] = {
        ["Murlocnachtkriecher"] = 7992,
        ["Nachtklinge der Defias"] = 7992,
        ["Skeletträuber"] = 7992,
        ["Wildfeder der Hexenschwingen"] = 7992,
        ["Auftragsmörder der Verlassenen"] = 7992,
        ["Dunkelzahnkrabbler"] = 7992,
        ["Slims Freund"] = 7992,
        ["Skorpidjäger"] = 7992,
    },
    ["Stulpen des Meeres"] = {
        Professions = 10630,
    },
    ["Wasser"] = {
        ["Massiver Geysir"] = 22422,
    },
    ["Langer Seidenumhang"] = {
        Professions = 3861,
    },
    ["Silberdietrich"] = {
        Professions = 19666,
    },
    ["Dunkle Lederhandschuhe"] = {
        Professions = 3765,
    },
    ["Frostsäblergamaschen"] = {
        Professions = 19074,
    },
    ["Beherrschung"] = {
        ["Oberster Kreuzzügler Dathrohan"] = 17405,
    },
    ["Goldene Schuppengamaschen"] = {
        Professions = 3507,
    },
    ["Verblassen"] = {
        ["PRIEST"] = 10942,
        ["Fürstin Sylvanas Windrunner"] = 20672,
        ["Knurrer"] = 5543,
    },
    ["Mächtige Zephyriumladung"] = {
        Professions = 23080,
    },
    ["Kühlung"] = {
        ["Blizzard"] = 28547,
    },
    ["Runenverzierter Kupfergürtel"] = {
        Professions = 2666,
    },
    ["Thoriumarmschienen"] = {
        Professions = 16644,
    },
    ["Sklavensauger"] = {
        ["Mogh der Unsterbliche"] = 8809,
    },
    ["Zaubersperre"] = {
        Pet = 19647,
    },
    ["Mana neutralisieren"] = {
        ["Siechdrache"] = 7994,
    },
    ["Strahlender Reif"] = {
        Professions = 16659,
    },
    ["Lavabrut beschwören"] = {
        ["Lavabrut"] = 19392,
        ["Feuerlord"] = 19392,
    },
    ["Heilende Berührung"] = {
        ["DRUID"] = 25297,
    },
    ["Ohrfeigen!"] = {
        ["Milton Beats"] = 6754,
    },
    ["Stiefel der Dämmerung"] = {
        Professions = 23705,
    },
    ["Todeswandlergeist der Atal'ai beschwören"] = {
        ["Todeswandlergeist der Atal'ai"] = 12095,
        ["Todeswandler der Atal'ai"] = 12095,
    },
    ["Schild - Schwache Ausdauer"] = {
        Professions = 13378,
    },
    ["Untote aufspüren"] = {
        ["HUNTER"] = 19884,
    },
    ["Stahlbrustplatte"] = {
        Professions = 9916,
    },
    ["Geprägte Lederweste"] = {
        Professions = 2160,
    },
    ["Sturmschleierhose"] = {
        Professions = 19067,
    },
    ["Summon Dreadsteed Spirit (DND)"] = {
        ["Schreckensrossgeist"] = 23159,
        ["Xorothianisches Schreckensross"] = 23159,
    },
    ["Wildlederumhang"] = {
        Professions = 10574,
    },
    ["Dampfpanzersteuerung"] = {
        Professions = 28327,
    },
    ["Blizzard"] = {
        ["MAGE"] = 10187,
        ["Lokholar der Eislord"] = 21367,
    },
    ["Grüne Eisenschultern"] = {
        Professions = 3504,
    },
    ["Warosh-Kitzeln"] = {
        ["Warosh"] = 16771,
    },
    ["Tränen des Windsuchers"] = {
        ["Prinz Donneraan"] = 23011,
    },
    ["Feuernova"] = {
        ["Magmaelementar"] = 11970,
        ["Mechanoflammenwandler"] = 11970,
        ["Taragaman der Hungerleider"] = 11970,
        ["Schamane der Bloodscalp"] = 11969,
        ["Magistrat Burnside"] = 11969,
        ["Blutmagier Thalnos"] = 12470,
    },
    ["Goldene Schuppenstulpen"] = {
        Professions = 11643,
    },
    ["Zombiegestalt"] = {
        ["Spitzel Marksen"] = 7293,
    },
    ["Blutrebengamaschen"] = {
        Professions = 24092,
    },
    ["Gedankenbenebelndes Gift II"] = {
        ["ROGUE"] = 8694,
    },
    ["Elixier der Untotenentdeckung"] = {
        Professions = 11460,
    },
    ["Erkrankte Spucke"] = {
        ["Verseuchtes Jungtier"] = 17745,
    },
    ["Umhang des Feuers"] = {
        Professions = 18422,
    },
    ["Erdriss"] = {
        ["Klauententakel"] = 26139,
        ["Augententakel"] = 26139,
    },
    ["Quest - Verwandlungsimpakt"] = {
        ["Yeh'kinya"] = 24180,
    },
    ["Schatten verzehren"] = {
        Pet = 17854,
    },
    ["Seelenstein herstellen"] = {
        ["WARLOCK"] = 20755,
    },
    ["Elixier der großen Verteidigung"] = {
        Professions = 11450,
    },
    ["Valentinstag, Geschenk gegeben"] = {
        ["Qixi Q. Pido"] = 27663,
    },
    ["Frostschock"] = {
        ["SHAMAN"] = 10473,
        ["Strashazzauberhexerin"] = 15499,
        ["Verzauberin der Grollflossen"] = 12548,
        ["Kaltschnappkriecher"] = 12548,
        ["Schamane der Frostwolf"] = 21401,
        ["Lokholar der Eislord"] = 19133,
        ["Grik'nir der Kalte"] = 21030,
    },
    ["Schildschlag"] = {
        ["WARRIOR"] = 23925,
        ["Renegat der Blackrock"] = 8242,
        ["Handlanger der Defias"] = 8242,
        ["Kam Deepfury"] = 8242,
        ["Grunzer der Dragonmaw"] = 8242,
        ["Searingblade-Vollstrecker"] = 8242,
    },
    ["Verfallene Beweglichkeit"] = {
        ["Verderbter Schaudermagenkrokilisk"] = 7901,
    },
    ["Fieberseuche"] = {
        ["Mumifizierter Atal'ai"] = 16186,
    },
    ["Erschöpft"] = {
        ["Zwangsarbeiter von Rabenklaue"] = 3271,
    },
    ["Karangs Banner zerstören"] = {
        ["Wütende Fäulnisklaue"] = 20786,
    },
    ["Gnomisches Verbergungsgerät"] = {
        Professions = 3971,
    },
    ["EZ-Thro-Dynamit II"] = {
        Professions = 23069,
    },
    ["Goblin-Raketenstiefel"] = {
        Professions = 8895,
    },
    ["Zweihandwaffe - Schwacher Einschlag"] = {
        Professions = 7745,
    },
    ["Gesundheitsstein herstellen (schwach)"] = {
        ["WARLOCK"] = 6201,
    },
    ["Arkane Explosion"] = {
        ["MAGE"] = 10202,
        ["Arkanist Doan"] = 9433,
        ["Captain Balinda Stonehearth"] = 13745,
    },
    ["Purpurrote Seidenhandschuhe"] = {
        Professions = 8804,
    },
    ["Donnerknall"] = {
        ["WARRIOR"] = 11581,
        ["Oberanführer Rammhauer"] = 15548,
        ["Stachelwache von Razorfen"] = 15548,
        ["Vanndar Stormpike"] = 15588,
        ["Großer Donnerfalke"] = 8078,
        ["Donnerfalkenwolkenkitzler"] = 8078,
        ["Tobender Klippenstürmer"] = 8078,
        ["Wache von Ironforge"] = 8078,
        ["Kriegsherr Kolkanis"] = 8078,
        ["Torek"] = 8078,
        ["Kriegstreiber der Mosh'Ogg"] = 8147,
        ["Klippendonnerer"] = 8147,
        ["Umi Thorson"] = 8147,
    },
    ["Azurblaue Schultern"] = {
        Professions = 8795,
    },
    ["Phönixhose"] = {
        Professions = 3851,
    },
    ["Blitzschlagwelle"] = {
        ["Ysondre"] = 24819,
    },
    ["Großer Segen der Macht"] = {
        ["PALADIN"] = 25916,
    },
    ["Elixier der überragenden Verteidigung"] = {
        Professions = 17554,
    },
    ["Zerschmettern"] = {
        ["WARRIOR"] = 11605,
        ["Schlachtwache von Razorfen"] = 11430,
    },
    ["Waffe der Flammenzunge"] = {
        ["SHAMAN"] = 16342,
    },
    ["Großer Segen der Rettung"] = {
        ["PALADIN"] = 25895,
    },
    ["Geschmeidiger unverwüstlicher Balg"] = {
        Professions = 19047,
    },
    ["Gezielter Schuss"] = {
        ["HUNTER"] = 20904,
    },
    ["Nitrobeschleuniger"] = {
        ["Dampfpanzer"] = 27746,
    },
    ["Gedanken verseuchen"] = {
        ["Hirnfresser"] = 3429,
        ["Magierjäger"] = 3429,
    },
    ["Krankheitsschuss"] = {
        ["Siechender Speerträger"] = 11397,
    },
    ["Feuerbrand"] = {
        ["WARLOCK"] = 25309,
        ["Entropische Bestie"] = 15661,
        ["Scharlachroter Folterknecht"] = 9275,
        ["Kultist der Burning Blade"] = 11962,
        ["Thule Rabenklaue"] = 20800,
        ["Jergosh der Herbeirufer"] = 20800,
        ["Befrager Vishas"] = 9034,
        ["Initiand von Hederine"] = 17883,
    },
    ["Schattenhauthandschuhe"] = {
        Professions = 22711,
    },
    ["Nagmaras Liebestrank"] = {
        ["Herrin Nagmara"] = 14928,
    },
    ["Maschinengewehr"] = {
        ["Mechanopanzer"] = 10346,
    },
    ["Spawn Grünen Drakoniden"] = {
        ["Grüner Drakonid"] = 22656,
        ["Green Drakonid Spawner"] = 22656,
    },
    ["Savannenjunges"] = {
        ["Savannenjunges"] = 6598,
        ["Savannenmatriarchin"] = 6598,
    },
    ["Mondfeuer"] = {
        ["DRUID"] = 9835,
        ["Erzdruide Renferal"] = 22206,
        ["Tyranis Malem"] = 15798,
        ["Moonkinorakel"] = 15798,
    },
    ["Katzengestalt"] = {
        ["DRUID"] = 768,
        ["Lord Melenas"] = 5759,
        ["Schildwache Amarassan"] = 5759,
    },
    ["Geist des Windes"] = {
        ["Quetschzahn"] = 16618,
    },
    ["Schultern der Morgenröte"] = {
        Professions = 16660,
    },
    ["Schwärende Bisse"] = {
        ["Verseuchtes Insekt"] = 16460,
    },
    ["Ehrenpunkte +378"] = {
        ["Kriegshetzer der Horde"] = 24964,
        ["Brigadegeneral der Allianz"] = 24964,
    },
    ["Schutzumhang der Verteidigung"] = {
        Professions = 22870,
    },
    ["Feine Lederstiefel"] = {
        Professions = 2158,
    },
    ["Natürliche Rüstung"] = {
        ["HUNTER"] = 24632,
    },
    ["Imperiale Plattenstiefel"] = {
        Professions = 16657,
    },
    ["Reflexionsfeld"] = {
        ["Arkaner Nullifizierer X-21"] = 10831,
    },
    ["Strahlungsblitz"] = {
        ["Bestrahlter Eindringling"] = 9771,
        ["Bestrahlter Plünderer"] = 9771,
    },
    ["Ebenholzmesser"] = {
        Professions = 10013,
    },
    ["Stiefel - Schwaches Tempo"] = {
        Professions = 13890,
    },
    ["Geringer Unsichtbarkeitstrank"] = {
        Professions = 3448,
    },
    ["Schuppenvertreibung"] = {
        ["Lord Victor Nefarius"] = 16404,
    },
    ["Eisgrabmal"] = {
        ["Lokholar der Eislord"] = 16869,
    },
    ["Leinenstoffballen"] = {
        Professions = 2963,
    },
    ["Geschmeidiger dicker Balg"] = {
        Professions = 10482,
    },
    ["Grünes Raketenbündel"] = {
        Professions = 26424,
    },
    ["Wächtergürtel"] = {
        Professions = 3775,
    },
    ["Geringer Steinschildtrank"] = {
        Professions = 4942,
    },
    ["Naturgewalt"] = {
        ["Naturgewalt"] = 6913,
        ["Cenarischer Verteidiger"] = 6913,
    },
    ["Eiserne Gürtelschnalle"] = {
        Professions = 8768,
    },
    ["Stiefel - Beweglichkeit"] = {
        Professions = 13935,
    },
    ["Stalvans Fluch"] = {
        ["Stalvan Mistmantle"] = 3105,
        ["Unglückseliger Geist"] = 3105,
    },
    ["Feuerregen"] = {
        ["WARLOCK"] = 11678,
        ["Grel'borg der Horter"] = 11990,
        ["Geomant der Gogger"] = 11990,
    },
    ["Waffe ergreifen"] = {
        ["Ausplünderer der Tiefentroggs"] = 10851,
    },
    ["Erdbohrersäure"] = {
        ["Erdbohrer"] = 18070,
    },
    ["Versengen"] = {
        ["MAGE"] = 10207,
    },
    ["Gletscherumhang"] = {
        Professions = 28208,
    },
    ["Virulentes Gift"] = {
        ["Grabmalhäscher"] = 12251,
    },
    ["Salve"] = {
        ["HUNTER"] = 14295,
    },
    ["Hexerzwirnturban"] = {
        Professions = 18450,
    },
    ["Wildtier zähmen"] = {
        ["HUNTER"] = 1515,
    },
    ["Tückische Mithrilklinge"] = {
        Professions = 9997,
    },
    ["Aschenstoffstiefel"] = {
        Professions = 12088,
    },
    ["Feuerschild III"] = {
        ["Magier der Blutsegelbukaniere"] = 2601,
        ["Lo'Grosh"] = 2601,
        ["Scharlachroter Rufer"] = 2601,
        ["Scharlachroter Hexer"] = 2601,
        ["Geringe Höllenbestie"] = 2601,
    },
    ["Handgenähter Lederumhang"] = {
        Professions = 9058,
    },
    ["Fluch der Verdammnis"] = {
        ["WARLOCK"] = 603,
    },
    ["Tod und Verfall"] = {
        ["Kultistin der Totenköpfe"] = 11433,
    },
    ["Druidentrank"] = {
        ["Jünger von Naralex"] = 8141,
    },
    ["Gnomen-Universalfernbedienung"] = {
        Professions = 9269,
    },
    ["Stiefel - Schwache Beweglichkeit"] = {
        Professions = 7867,
    },
    ["Antu'suls Diener"] = {
        ["Diener von Antu'sul"] = 11894,
    },
    ["Eisklaue"] = {
        ["Bjarn"] = 3130,
        ["Eisklauenbär"] = 3130,
    },
    ["Geringe Welle der Heilung"] = {
        ["SHAMAN"] = 10468,
    },
    ["Weisheit der Holzschlundfeste"] = {
        Professions = 23662,
    },
    ["Arkanes Elixier"] = {
        Professions = 11461,
    },
    ["Festlicher roter Hosenanzug"] = {
        Professions = 26407,
    },
    ["Krabblergift"] = {
        ["Höhlenkrabbler"] = 14532,
    },
    ["Bestrahlt"] = {
        ["Bestrahlter Eindringling"] = 9775,
        ["Bestrahlter Plünderer"] = 9775,
    },
    ["Bogenlichtschraubenschlüssel"] = {
        Professions = 7430,
    },
    ["Schwärzliche Lederrüstung"] = {
        Professions = 9196,
    },
    ["Ablenkender Schuss"] = {
        ["HUNTER"] = 15632,
    },
    ["Beißen"] = {
        Pet = 17261,
    },
    ["Magische Flügel"] = {
        ["Auslöser für Jahrmarktskanone"] = 24742,
    },
    ["Geprägte Lederhandschuhe"] = {
        Professions = 3756,
    },
    ["Transmutieren: Feuer zu Erde"] = {
        Professions = 17560,
    },
    ["Geisterhafter Stoß"] = {
        ["ROGUE"] = 14278,
    },
    ["Waffe - Unheilige Waffe"] = {
        Professions = 20033,
    },
    ["Pyroschlag"] = {
        ["MAGE"] = 18809,
    },
    ["Brust - Schwache Werte"] = {
        Professions = 13626,
    },
    ["Kristallisieren"] = {
        ["Spitzenspinne"] = 16104,
    },
    ["Mithrilrohr"] = {
        Professions = 12589,
    },
    ["Runenverzierte Arkanitrute"] = {
        Professions = 20051,
    },
    ["Sternenfeuer"] = {
        ["DRUID"] = 25298,
    },
    ["Kranker Brühschleimer"] = {
        ["Faulender Brühschleimer"] = 6907,
        ["Waldbrühschlammer"] = 6907,
    },
    ["Große Zephyriumladung"] = {
        Professions = 3972,
    },
    ["Seele der Macht"] = {
        ["PRIEST"] = 10060,
    },
    ["Waffe - Schwaches Schlagen"] = {
        Professions = 7788,
    },
    ["Totem der Windmauer"] = {
        ["SHAMAN"] = 15112,
    },
    ["Sofort wirkendes Gift IV"] = {
        ["ROGUE"] = 11341,
    },
    ["Manaeruption beschwören"] = {
        ["Manaeruption"] = 22939,
        ["Verbleibender Monsterrest"] = 22939,
    },
    ["Blaues Leinenhemd"] = {
        Professions = 2394,
    },
    ["Helm des Feuers"] = {
        Professions = 10632,
    },
    ["Welkendes Gift"] = {
        ["Skorpidschrecker"] = 13884,
        ["Skorpidhäscher"] = 13884,
    },
    ["Mithrilsporen"] = {
        Professions = 9964,
    },
    ["Pylon-Benutzerhandbuch erstellen"] = {
        ["J.D. Collie"] = 15211,
    },
    ["Wachsamkeit"] = {
        Racial = 20600,
    },
    ["Grässlicher Schrecken"] = {
        ["Nelson der Nette"] = 23275,
    },
    ["Gemusterte bronzene Armschienen"] = {
        Professions = 2672,
    },
    ["Verschnörkelter Mithrilhelm"] = {
        Professions = 9980,
    },
    ["Armschiene - Überragende Stärke"] = {
        Professions = 20010,
    },
    ["Streuschuss"] = {
        ["HUNTER"] = 19503,
    },
    ["Mehrfachschuss"] = {
        ["HUNTER"] = 25294,
        ["Mechaniker der Venture Co."] = 14443,
        ["Aussätziger Verteidiger"] = 14443,
    },
    ["Haut festigen"] = {
        ["Versteinerter Treant"] = 22693,
    },
    ["Frostfalle"] = {
        ["HUNTER"] = 13809,
    },
    ["Runenverzierte Goldrute"] = {
        Professions = 13628,
    },
    ["Gehärtetes Eisenkurzschwert"] = {
        Professions = 3492,
    },
    ["Geprägter Lederumhang"] = {
        Professions = 2162,
    },
    ["Gnomen-Net-o-Matik-Projektor"] = {
        Professions = 12902,
    },
    ["Robe der Winternacht"] = {
        Professions = 18436,
    },
    ["Hauen"] = {
        ["Kundschafter der Flusspfoten"] = 3391,
        ["Edwin van Cleef"] = 3391,
        ["Mr. Smite"] = 3391,
        ["Kobold der Tunnelratten"] = 3391,
        ["Targorr der Schreckliche"] = 3391,
        ["Pfadfinder der Schwarzfelle"] = 3391,
        ["Tobender Riffkriecher"] = 3391,
        ["Hochlandhauer"] = 3391,
        ["Raufer der Staubspeier"] = 3391,
        ["Wilder Klippenkojote"] = 3391,
        ["Bluthornsensenklaue"] = 3391,
        ["Offizier von Bael'dun"] = 3391,
        ["Pfadfinder der Grimmhauer"] = 3391,
        ["Höhlenbehüter der Fäulnisklauen"] = 3391,
        ["Wildfeder der Blutfurien"] = 3391,
        ["Strashazhydra"] = 3391,
        ["Düsterdrescher"] = 3391,
        ["Alter Düsterdrescher"] = 3391,
        ["Fardel Dabyrie"] = 3391,
        ["Scharlachroter Mönch"] = 3391,
        ["Raufer der Kolkar"] = 3391,
        ["Raufer der Magram"] = 3391,
        ["Wespe der Centipaar"] = 3391,
        ["Stecher der Centipaar"] = 3391,
        ["Schwärmer der Centipaar"] = 3391,
        ["Plünderer der Tiefentroggs"] = 3391,
        ["Wellenhauer"] = 3391,
        ["Großer Wellenhauer"] = 3391,
        ["Kriegshäscher"] = 3391,
        ["Ferocitas der Traumfresser"] = 3391,
        ["Teufelspfotenverheerer"] = 3391,
        ["Moonkin"] = 3391,
        ["Grummelkehle"] = 3391,
        ["Fährtenleser der Schwarzfelle"] = 3391,
        ["Steinpanzerschinder"] = 3391,
        ["Mondklaue"] = 3391,
        ["Vorsha die Peitscherin"] = 3391,
        ["Bluthund der Frostwolf"] = 3391,
        ["Stormpike-Eule"] = 3391,
        ["Harb Faulberg"] = 3391,
    },
    ["Großer Rekombobulator"] = {
        Professions = 23079,
    },
    ["Kupferclaymore"] = {
        Professions = 9983,
    },
    ["Mondpirschergestalt"] = {
        ["Terenthis"] = 6236,
    },
    ["König der Gordok"] = {
        ["Mizzle der Gewiefte"] = 22799,
    },
    ["Taschendiebstahl"] = {
        ["ROGUE"] = 921,
    },
    ["Wasserelementarer beschwören"] = {
        ["Beschworener Wasserelementar"] = 20681,
        ["Lady Jaina Proudmoore"] = 20681,
    },
    ["Runenstoffhandschuhe"] = {
        Professions = 18417,
    },
    ["Unsichtbarkeitstrank"] = {
        Professions = 11464,
    },
    ["Rote Wolltasche"] = {
        Professions = 6688,
    },
    ["Gifthautsekret"] = {
        ["Gifthautravasaurus"] = 14792,
    },
    ["Heilige Nova"] = {
        ["PRIEST"] = 27801,
    },
    ["Rissleuchtfeuer"] = {
        ["Risssucher der Verirrten"] = 9614,
    },
    ["Feine Ledertunika"] = {
        Professions = 3761,
    },
    ["Wütende Teufelswache Spawn"] = {
        ["Wütende Teufelswache"] = 22393,
    },
    ["Mondstoffgamaschen"] = {
        Professions = 18440,
    },
    ["Verstärkte Wollschultern"] = {
        Professions = 3849,
    },
    ["Entwaffnen"] = {
        ["WARRIOR"] = 676,
        ["Muckrake"] = 8379,
        ["Kerkermeister Borhuin"] = 8379,
        ["Kenata Dabyrie"] = 8379,
        ["Blackfathom-Myrmidone"] = 8379,
        ["Untiefenschnappklaue"] = 8379,
        ["Krieger der Bloodscalp"] = 6713,
        ["Ausplünderer der Defias"] = 6713,
        ["Sneed"] = 6713,
        ["Erster Maat Snellig"] = 6713,
        ["Gefangener Defias"] = 6713,
        ["Dieb des Syndikats"] = 6713,
        ["Bauer von Hillsbrad"] = 6713,
        ["Twilight-Rohling"] = 6713,
        ["Streuner des Syndikats"] = 6713,
        ["Lord Falkenstein"] = 6713,
        ["Vollstrecker der Venture Co."] = 6713,
        ["Marinesoldat von Theramore"] = 6713,
        ["Üble Fledermaus"] = 6713,
        ["Dal Blutklaue"] = 6713,
        ["Schreckensflieger"] = 6713,
        ["Leibwache der Defias"] = 6713,
        ["Bandit der Grimmtotem"] = 6713,
    },
    ["Fluch der Rache"] = {
        ["Arikara"] = 17213,
    },
    ["Illusionen beschwören"] = {
        ["Illusion von Jandice Barov"] = 17773,
        ["Jandice Barov"] = 17773,
    },
    ["Runenverzierte Lederstulpen"] = {
        Professions = 19055,
    },
    ["Eisenherz nimmt wieder Menschengestalt an"] = {
        ["Eisenherz Johnson"] = 9192,
    },
    ["Transmutieren: Eisen in Gold"] = {
        Professions = 11479,
    },
    ["Dunkeleisenbombe"] = {
        Professions = 19799,
    },
    ["Kettenbrand"] = {
        ["Bestrahlter Schrecken"] = 8211,
    },
    ["Feuerschutzbrille"] = {
        Professions = 12594,
    },
    ["Sprengfalle"] = {
        ["HUNTER"] = 14317,
    },
    ["Krom'zars Banner herstellen"] = {
        ["Kriegsherr Krom'zar"] = 13965,
    },
    ["Razelikhs Träne I"] = {
        ["Razelikh der Entweiher"] = 10864,
    },
    ["Geschmeidiger leichter Balg"] = {
        Professions = 3816,
    },
    ["Erschütternder Schlag"] = {
        ["WARRIOR"] = 12809,
    },
    ["Flammen von Shahram"] = {
        ["Shahram"] = 16596,
    },
    ["Zünder für Feuerwerk"] = {
        Professions = 26442,
    },
    ["Fixieren"] = {
        ["Bezauberter Atal'ai"] = 12021,
        ["Jademirastwache"] = 12021,
        ["Drachenbrut der Zornkrallen"] = 12021,
        ["Auferstandene Entartung"] = 12021,
        ["Smaragdonastwache"] = 12021,
        ["Tiefgrüne Astwache"] = 12021,
    },
    ["Grüne Drachenschuppenstulpen"] = {
        Professions = 24655,
    },
    ["Brust - Gesundheit"] = {
        Professions = 7857,
    },
    ["Totem der Manaflut"] = {
        ["SHAMAN"] = 17359,
    },
    ["Polierte Stahlstiefel"] = {
        Professions = 3513,
    },
    ["Eisketten"] = {
        ["Hexer der Defias"] = 113,
        ["Adept der Totenköpfe"] = 113,
        ["Erzmagier Ataeric"] = 512,
    },
    ["Brust - Schwache Absorption"] = {
        Professions = 7426,
    },
    ["Tier besänftigen"] = {
        ["DRUID"] = 9901,
    },
    ["Sayges dunkles Schicksal des Schadens"] = {
        ["Sayge"] = 23768,
    },
    ["Vampirumarmung"] = {
        ["PRIEST"] = 15286,
    },
    ["Elixier des Giftwiderstands"] = {
        Professions = 3174,
    },
    ["Schwerer Mithrilhelm"] = {
        Professions = 9970,
    },
    ["Umhang - Geringer Schattenwiderstand"] = {
        Professions = 13522,
    },
    ["Ghulseuche"] = {
        ["Seuchenghul"] = 16458,
    },
    ["Rauchendes Herz des Berges"] = {
        Professions = 15596,
    },
    ["Flammenpeitsche"] = {
        ["Roter Welpe"] = 3356,
        ["Schwester Sichelschwinge"] = 3356,
    },
    ["Eisblock"] = {
        ["MAGE"] = 11958,
    },
    ["Mörserbelebung"] = {
        ["Shorty"] = 18655,
    },
    ["Dunkeleisenarmschienen"] = {
        Professions = 20874,
    },
    ["Drohnen des Ashischwarms beschwören"] = {
        ["Drohne des Ashischwarms"] = 21327,
        ["Gequälter Druide"] = 21327,
        ["Gequälte Schildwache"] = 21327,
    },
    ["Pyroklasmus-Sperrfeuer"] = {
        ["Lavaelementar"] = 19641,
    },
    ["Flammenschauer"] = {
        ["Infernoelementar"] = 10733,
        ["Mechanoflammenwandler"] = 10733,
        ["Thaurissanischer Feuerwandler"] = 10733,
    },
    ["Runenstofftunika"] = {
        Professions = 18407,
    },
    ["Rote Magiestoffschultern"] = {
        Professions = 12078,
    },
    ["Zauberschutz des Auges"] = {
        ["Kapitän Halyndor"] = 3389,
    },
    ["Weißes Schwadroneurshemd"] = {
        Professions = 8483,
    },
    ["Läuterungstrank"] = {
        Professions = 17572,
    },
    ["Handschuhe - Hochentwickelte Kräuterkunde"] = {
        Professions = 13868,
    },
    ["Erschütternder Schuss"] = {
        ["HUNTER"] = 5116,
    },
    ["Seelenbrand"] = {
        ["Feuerlord"] = 19393,
    },
    ["Elende Kälte"] = {
        ["Beschmutzter Wasserelementar"] = 6873,
        ["Tideress"] = 6873,
    },
    ["Schwere Mithrilhose"] = {
        Professions = 9933,
    },
    ["Macht von Shahram"] = {
        ["Shahram"] = 16600,
    },
    ["Grüne Wolltasche"] = {
        Professions = 3758,
    },
    ["Schleichen"] = {
        ["DRUID"] = 9913,
        Pet = 24453,
        ["Nachtläufer der Defias"] = 22766,
        ["Auftragsmörder der Schattenfelle"] = 22766,
        ["Schattentatzenpanther"] = 22766,
        ["Kurzens Kommandosoldat"] = 22766,
        ["Moorkrabbler"] = 22766,
        ["Alter Schattentatzenpanther"] = 22766,
        ["Schattenklaue"] = 22766,
        ["Spion des Syndikats"] = 22766,
        ["Auftragsmörder des Syndikats"] = 22766,
        ["Straßenräuber des Syndikats"] = 22766,
        ["Streuner des Syndikats"] = 22766,
        ["Kammpirscher"] = 22766,
        ["Silbermähnenpirscher"] = 22766,
        ["Wegelagerin der Hexenschwingen"] = 22766,
        ["Schleicher der Grimmhauer"] = 22766,
        ["Deviatpirscher"] = 22766,
        ["Schattenpirscher der Teufelshufe"] = 22766,
        ["Schattenpirscher der Herzschinder"] = 22766,
        ["Klippenpirscher"] = 22766,
        ["Graunebellauerer"] = 22766,
        ["Dunkelzahnlauerer"] = 22766,
        ["Schurke der Leidbringer"] = 22766,
        ["Schattenpirscher der Leidbringer"] = 22766,
        ["Spitzel von Theramore"] = 22766,
        ["Pirscher von Razorfen"] = 22766,
    },
    ["Innerer Fokus"] = {
        ["PRIEST"] = 14751,
    },
    ["Ehrenpunkte +2388"] = {
        ["Kriegshetzer der Horde"] = 24966,
        ["Brigadegeneral der Allianz"] = 24966,
    },
    ["Sehnenriss"] = {
        ["Nefaru"] = 3604,
        ["Tethis"] = 3604,
        ["Alter Scharfzahn"] = 3604,
        ["Sägezahnschnapper"] = 3604,
        ["Flusskrokilisk"] = 3604,
        ["Salzwasserkrokilisk"] = 3604,
        ["Schnappkieferkrokilisk"] = 3604,
        ["Die Hülse"] = 3604,
        ["Riesiger Sumpflandkrokilisk"] = 3604,
        ["Alter Salzwasserkrokilisk"] = 3604,
        ["Schattendickichtborkenreißer"] = 3604,
        ["Terrowulf-Fleischreißer"] = 3604,
        ["Matschpanzerklacker"] = 3604,
        ["Tiefteichdreschflosse"] = 3604,
        ["Deviatkrokilisk"] = 3604,
        ["Snort der Spotter"] = 3604,
        ["Makrinnischnappklaue"] = 3604,
        ["Ravasaurusjäger"] = 3604,
        ["Splitterzahnbär"] = 3604,
        ["Wilder Eisdistelyeti"] = 3604,
        ["Eisdistelyeti"] = 3604,
        ["Reißerklaue"] = 3604,
        ["Klippenspringer"] = 3604,
        ["Suhlaman"] = 3604,
        ["Manahund"] = 3604,
        ["Arbeiter der Schattenschmiede"] = 3604,
        ["Teufelspfotenaasfresser"] = 3604,
        ["Junger Dimetrodon"] = 3604,
        ["Lar'korwi-Weibchen"] = 3604,
        ["Lar'korwi"] = 3604,
        ["Sabbernder Glutworg"] = 3604,
        ["Totenreißer"] = 3604,
        ["Bayne"] = 3604,
        ["Nebelheuler"] = 3604,
        ["Shy-Rotam"] = 3604,
        ["Sian-Rotam"] = 3604,
        ["Steinpanzerzange"] = 3604,
        ["Mastiff der Gordok"] = 3604,
        ["Dämmerpirscher"] = 3604,
        ["Zulianischer Krokilisk"] = 3604,
    },
    ["Totem der Gehirnwäsche beschwören"] = {
        ["Totem der Gehirnwäsche"] = 24262,
    },
    ["Schwäche ausnutzen"] = {
        ["Dunkelläufer der Nachtheuler"] = 6595,
        ["Grabräuber"] = 6595,
        ["Plünderer der Defias"] = 6595,
        ["Mondpirscherpatriarch"] = 6595,
        ["Wildfeder der Hexenschwingen"] = 6595,
        ["Wegelagerin der Hexenschwingen"] = 6595,
        ["Schurke von Xavian"] = 6595,
        ["Schurke der Teufelshufe"] = 6595,
        ["Kreischende Wildfeder"] = 6595,
        ["Schattenpirscher der Zornsäer"] = 6595,
        ["Schurke der Leidbringer"] = 8355,
    },
    ["Teleportieren: Thunder Bluff"] = {
        ["MAGE"] = 3566,
    },
    ["Herausforderungsruf"] = {
        ["WARRIOR"] = 1161,
    },
    ["Azurblaue Seidenhose"] = {
        Professions = 8758,
    },
    ["Leinengürtel"] = {
        Professions = 8776,
    },
    ["Wegschlagen"] = {
        ["Arkkorocs Sohn"] = 10101,
        ["Ursangus"] = 10101,
        ["Cyclonian"] = 18670,
        ["Teremus der Verschlinger"] = 11130,
        ["Geschmolzener Riese"] = 18945,
    },
    ["Vulkanischer Hammer"] = {
        Professions = 16984,
    },
    ["Runenverzierter Mithrilhammer"] = {
        Professions = 10009,
    },
    ["Sammlerschwarm beschwören"] = {
        ["Sammlerschwarm"] = 7278,
        ["Silithidernter"] = 7278,
    },
    ["Heiliges Licht"] = {
        ["PALADIN"] = 25292,
    },
    ["Sengende Flammen"] = {
        ["Botschafter Infernus"] = 9552,
    },
    ["Gebet der Seelenstärke"] = {
        ["PRIEST"] = 21564,
    },
    ["Elixier der Ogerstärke"] = {
        Professions = 3188,
    },
    ["Spukphantome"] = {
        ["Spektraler Bürger"] = 16336,
        ["Geisterhafter Bürger"] = 16336,
    },
    ["Imperiale Plattenarmschienen"] = {
        Professions = 16649,
    },
    ["Schneemeister 9000"] = {
        Professions = 21940,
    },
    ["Riposte"] = {
        ["ROGUE"] = 14251,
    },
    ["Elementar Spawn-in"] = {
        ["Baron Kazum"] = 25035,
        ["Der Fürst der Asche"] = 25035,
        ["Der Fürst der Tiefen"] = 25035,
        ["Der Fürst der Splitter"] = 25035,
        ["Purpurroter Templer"] = 25035,
        ["Azurblauer Templer"] = 25035,
        ["Weißgrauer Templer"] = 25035,
        ["Der Fürst der Stürme"] = 25035,
        ["Irdener Templer"] = 25035,
    },
    ["Hast-Aura"] = {
        ["Vorarbeiter der Zorneshämmer"] = 13589,
        ["Vorarbeiter der Weißstoppel"] = 13589,
    },
    ["Schmutz schleudern"] = {
        ["Oasenschnappkiefer"] = 6530,
    },
    ["Feuerzauberschutz"] = {
        ["MAGE"] = 10225,
    },
    ["Kleine blaue Rakete"] = {
        Professions = 26416,
    },
    ["Moosbedeckte Hände"] = {
        ["Seeschleicher"] = 6866,
        ["Alter Seeschleicher"] = 6866,
        ["Schattendickichtmoosfresser"] = 6866,
    },
    ["Schwarzes Magiestoffstirnband"] = {
        Professions = 12072,
    },
    ["Wildlederhelm"] = {
        Professions = 10546,
    },
    ["Drachkin aufspüren"] = {
        ["HUNTER"] = 19879,
    },
    ["Mithrilgehäuse"] = {
        Professions = 12599,
    },
    ["Kernteufelsstofftasche"] = {
        Professions = 26087,
    },
    ["Gesprungener Stein"] = {
        ["Elementarsklave"] = 3671,
    },
    ["Begrenzter Unverwundbarkeitstrank"] = {
        Professions = 3175,
    },
    ["Feuerschuss"] = {
        ["Scharlachroter Späher"] = 6979,
        ["Saboteur des Syndikats"] = 6980,
    },
    ["Erneuerung"] = {
        ["PRIEST"] = 25315,
        ["Sirene der Grollflossen"] = 11640,
        ["Bastionssanitäter"] = 22168,
        ["Priester von Dun Garok"] = 8362,
        ["Narillasanz"] = 8362,
        ["Scharlachroter Jünger"] = 8362,
        ["Scharlachroter Kaplan"] = 8362,
        ["Scharlachroter Abt"] = 8362,
        ["Akolyt der Totenköpfe"] = 8362,
        ["Twilight-Akolyt"] = 8362,
        ["Orakel der Nachtaugen"] = 8362,
        ["Expeditionspriester"] = 8362,
    },
    ["Dämmerungsschneide"] = {
        Professions = 16970,
    },
    ["Umhang - Schwache Beweglichkeit"] = {
        Professions = 13419,
    },
    ["Kriegsbärenharnisch"] = {
        Professions = 19068,
    },
    ["Tier füttern"] = {
        ["HUNTER"] = 6991,
    },
    ["Überraschungs-Angriff"] = {
        ["Klippenpirscher"] = 8151,
    },
    ["Erschrecken"] = {
        ["Skelettschrecken"] = 7399,
        ["Sneeds Schredder"] = 7399,
        ["Skorpidschrecker"] = 7399,
        ["Deviatschreckensfang"] = 7399,
        ["Azshir der Schlaflose"] = 7399,
        ["Rasender Pterrordax"] = 7399,
        ["Rak'shiri"] = 7399,
    },
    ["Berührung der Schwäche"] = {
        ["PRIEST"] = 19266,
    },
    ["Ätzender Säureatem"] = {
        ["Gyth"] = 16359,
        ["Somnus"] = 20667,
    },
    ["Mal des Jägers"] = {
        ["HUNTER"] = 14325,
    },
    ["Portal der Schmetterschilde"] = {
        ["Portal der Schmetterschilde"] = 15125,
        ["Hexenmeister der Schmetterschilde"] = 15125,
    },
    ["Drachenschuppenstulpen"] = {
        Professions = 10619,
    },
    ["Dunkle Ledertunika"] = {
        Professions = 2169,
    },
    ["Dämonengeschmiedete Brustplatte"] = {
        Professions = 16667,
    },
    ["Große Verwandlung"] = {
        ["Zauberbinder der Pechschwingen"] = 22274,
        ["Grethok der Aufseher"] = 22274,
    },
    ["Wutanfall-Auslöser"] = {
        ["Feueranbeter"] = 19515,
    },
    ["Gyro-Mithrilgeschoss"] = {
        Professions = 12621,
    },
    ["Razelikhs Träne II"] = {
        ["Razelikh der Entweiher"] = 10866,
    },
    ["Tragbarer bronzener Mörser"] = {
        Professions = 3960,
    },
    ["Eberangriff"] = {
        ["Kampfeber der Borstennacken"] = 3385,
        ["Scheckiger Terroreber"] = 3385,
        ["Alter scheckiger Eber"] = 3385,
        ["Verderbter scheckiger Eber"] = 3385,
    },
    ["Rakete, Gelb Groß"] = {
        ["Pats Feuerwerkstyp - GELB GROSS"] = 26356,
    },
    ["Stiefel - Ausdauer"] = {
        Professions = 13836,
    },
    ["Weihe"] = {
        ["PALADIN"] = 20924,
    },
    ["Levitieren"] = {
        ["PRIEST"] = 1706,
    },
    ["Netz"] = {
        ["Ruklar der Fallensteller"] = 12024,
        ["Fallensteller der Defias"] = 12024,
        ["Murlocnetzer"] = 12024,
        ["Räuber der Blaukiemen"] = 12024,
        ["Netzer der Grauflossen"] = 12024,
        ["Zänker der Kolkar"] = 12024,
        ["Haudrauf von Ratchet"] = 12024,
        ["Haudrauf von Booty Bay"] = 12024,
        ["Stummelspeer"] = 12024,
        ["Haudrauf von Gadgetzan"] = 12024,
        ["Kundschafter der Blackrock"] = 6533,
        ["Tharil'zun"] = 6533,
        ["Jäger der Bloodscalp"] = 6533,
        ["Kurzens Zänker"] = 6533,
        ["Fallensteller der Moosfelle"] = 6533,
        ["Räuber der Dragonmaw"] = 6533,
        ["Versklaver der Splitterfäuste"] = 6533,
        ["Räuber der Blutsegelbukaniere"] = 6533,
        ["Lady Mondblick"] = 6533,
        ["Kerkermeister Marlgen"] = 6533,
        ["Kerkermeister Borhuin"] = 6533,
        ["Kopfjäger der Witherbark"] = 6533,
        ["Räuber der Säbelflossen"] = 6533,
        ["Barak Kodobann"] = 6533,
        ["Zänker der Galak"] = 6533,
        ["Aufseher von Razorfen"] = 6533,
        ["Zänker der Magram"] = 6533,
        ["Friedensbewahrer-Sicherheitsanzug"] = 6533,
        ["Gelihast"] = 6533,
        ["Boss Copperplug"] = 6533,
        ["Gurgelbacke"] = 6533,
        ["Räuber Jhash"] = 6533,
        ["Auftragsmörder der Galak"] = 6533,
    },
    ["Thoriumhelm"] = {
        Professions = 16653,
    },
    ["Azurblaue Seidenhandschuhe"] = {
        Professions = 3854,
    },
    ["Schwarze Magiestoffschultern"] = {
        Professions = 12074,
    },
    ["Verjüngung"] = {
        ["DRUID"] = 25299,
        ["Traumwandlerwyrmkin"] = 12160,
        ["Zauberhexer der Grimmtotem"] = 12160,
        ["Erzdruide Renferal"] = 15981,
    },
    ["Schwäche aufdecken"] = {
        ["Blutsucher"] = 7140,
    },
    ["Attrappe NSC Beschwören"] = {
        ["Abtrünniger Hexer der Defias"] = 3361,
    },
    ["Aschenstoffumhang"] = {
        Professions = 18418,
    },
    ["Stiefel - Geringe Ausdauer"] = {
        Professions = 13644,
    },
    ["Lebensernte"] = {
        ["Madenauge"] = 3243,
    },
    ["Gifte"] = {
        ["ROGUE"] = 2842,
    },
    ["Uralter Schrecken"] = {
        ["Uralter Kernhund"] = 19365,
    },
    ["Handschuhe - Reitfertigkeit"] = {
        Professions = 13947,
    },
    ["Humanoide aufspüren"] = {
        ["DRUID"] = 5225,
        ["HUNTER"] = 19883,
    },
    ["Gemeinsame Bande"] = {
        ["Sklave von Rabenklaue"] = 7761,
    },
    ["Kannibalismus"] = {
        Racial = 20577,
    },
    ["Zermalmen"] = {
        ["DRUID"] = 9881,
        ["Uthil Mooncall"] = 12161,
        ["Cenarischer Verwalter"] = 12161,
        ["Zornklaue"] = 12161,
    },
    ["Brust - Große Gesundheit"] = {
        Professions = 13640,
    },
    ["Steingestalt"] = {
        Racial = 20594,
        ["Aufrührer der Dunkeleisenzwerge"] = 7020,
        ["Captain Eisenberg"] = 7020,
    },
    ["Rache"] = {
        ["WARRIOR"] = 25288,
        ["Wache von Stormwind"] = 12170,
        ["Dal Blutklaue"] = 12170,
        ["Rächer der Distelfelle"] = 8602,
        ["Stormpike-Verteidiger"] = 19130,
        ["Gardist der Frostwolf"] = 19130,
        ["Erfahrener Verteidiger"] = 19130,
        ["Erfahrener Wächter"] = 19130,
        ["Verteidigerveteran"] = 19130,
        ["Wächterveteran"] = 19130,
        ["Wächterchampion"] = 19130,
        ["Verteidigerchampion"] = 19130,
        ["Corporal Noreg Stormpike"] = 19130,
    },
    ["Grüne Eisenarmschienen"] = {
        Professions = 3501,
    },
    ["Stark einschlagende Mithrilpatronen"] = {
        Professions = 12596,
    },
    ["Armschiene - Schwache Beweglichkeit"] = {
        Professions = 7779,
    },
    ["Strahlende Handschuhe"] = {
        Professions = 16654,
    },
    ["Armschiene - Große Willenskraft"] = {
        Professions = 13846,
    },
    ["Riesen aufspüren"] = {
        ["HUNTER"] = 19882,
    },
    ["Mana entziehen"] = {
        ["WARLOCK"] = 11704,
    },
    ["Avatar"] = {
        ["Vanndar Stormpike"] = 19135,
    },
    ["Segen der Macht"] = {
        ["PALADIN"] = 25291,
    },
    ["Barbarische Eisenhandschuhe"] = {
        Professions = 9820,
    },
    ["Manajadestein herbeizaubern"] = {
        ["MAGE"] = 3552,
    },
    ["Teufelsstoffstiefel"] = {
        Professions = 18437,
    },
    ["Große Kupferbombe"] = {
        Professions = 3937,
    },
    ["Hühner-Ei beschwören"] = {
        ["Huhn"] = 13563,
    },
    ["Azurblauer Seidenumhang"] = {
        Professions = 8786,
    },
    ["Ritual der Beschwörung"] = {
        ["WARLOCK"] = 698,
    },
    ["Schwarzes Seidenpack"] = {
        Professions = 6695,
    },
    ["Schnelligkeit der Natur"] = {
        ["DRUID"] = 17116,
        ["SHAMAN"] = 16188,
    },
    ["Panzerschild"] = {
        Pet = 26064,
    },
    ["Leinenstiefel"] = {
        Professions = 2386,
    },
    ["Heiliger Schock"] = {
        ["PALADIN"] = 20930,
    },
    ["Wirbelndes Sperrfeuer"] = {
        ["Speerbalg von Razorfen"] = 8259,
    },
    ["Ehrenpunkte +82"] = {
        ["Kriegshetzer der Horde"] = 24961,
        ["Brigadegeneral der Allianz"] = 24961,
    },
    ["Gnomen-Schutzbrille"] = {
        Professions = 12897,
    },
    ["Elixier der großen Intelligenz"] = {
        Professions = 11465,
    },
    ["Umhang - Schwacher Schutz"] = {
        Professions = 7771,
    },
    ["Waffe des Windzorns"] = {
        ["SHAMAN"] = 16362,
    },
    ["Goldrute"] = {
        Professions = 14379,
    },
    ["Verbergen"] = {
        ["Wilddornlauerer"] = 6920,
        ["Schurke der Zornsäer"] = 6920,
    },
    ["Spinnenseidenslipper"] = {
        Professions = 3856,
    },
    ["Mal der Schande"] = {
        ["Parqual Fintallas"] = 6767,
    },
    ["Ätzender Brühschlammer"] = {
        ["Ätzender Lauerer"] = 9459,
    },
    ["Runenstoffrobe"] = {
        Professions = 18406,
    },
    ["Mit Köpfen sprechen"] = {
        ["Kin'weelay"] = 3644,
    },
    ["Grüne Seidenschultern"] = {
        Professions = 8774,
    },
    ["Dicke Murlocrüstung"] = {
        Professions = 6704,
    },
    ["Blaue glitzernde Axt"] = {
        Professions = 9995,
    },
    ["Rasche Heilung"] = {
        ["DRUID"] = 18562,
    },
    ["Kettenblitzschlag"] = {
        ["SHAMAN"] = 10605,
        ["Margol der Wüterich"] = 15549,
        ["Donnerkopfkonsort"] = 12058,
        ["Thrall"] = 16033,
        ["Orakel der Hakkari"] = 16006,
        ["Primalist Thurloga"] = 16006,
        ["Owatanka"] = 6254,
        ["Charlga Razorflank"] = 8292,
    },
    ["Bedrohliches Knurren"] = {
        ["Präriewolf"] = 5781,
        ["Präriewolfalpha"] = 5781,
    },
    ["Tödliche Donnerbüchse"] = {
        Professions = 3936,
    },
    ["Geistesgegenwart"] = {
        ["MAGE"] = 12043,
    },
    ["Feenfeuer"] = {
        ["DRUID"] = 9907,
        ["Schneeschreiter der Frostmane"] = 6950,
        ["Frecher Grimmling"] = 6950,
        ["Pfadfinder der Schwarzfelle"] = 6950,
        ["Staubläufer der Grimmhauer"] = 6950,
        ["Dornenwirker der Grimmhauer"] = 6950,
        ["Fährtenleser der Schwarzfelle"] = 6950,
        ["Marosh der Verschlagene"] = 6950,
    },
    ["Erz schmelzen"] = {
        ["Goblin-Handwerker"] = 5159,
    },
    ["Feuerstein herstellen (erheblich)"] = {
        ["WARLOCK"] = 17953,
    },
    ["Schnellschuss"] = {
        ["Wilderer der Bleichmähnen"] = 1516,
    },
    ["Blitzschlag"] = {
        ["SHAMAN"] = 15208,
        ["Mystiker der Flusspfoten"] = 9532,
        ["Schwaches Murlocorakel"] = 9532,
        ["Mystiker der Bloodscalp"] = 9532,
        ["Mystiker der Skullsplitter"] = 9532,
        ["Orakel der Salzschuppen"] = 9532,
        ["Mystiker der Moosfelle"] = 9532,
        ["Schamane der Flusspfoten"] = 9532,
        ["Geologe der Venture Co."] = 9532,
        ["Seher der Splittersteintroggs"] = 9532,
        ["Schamane der Mo'grosh"] = 9532,
        ["Mystiker der Mo'grosh"] = 9532,
        ["Seher der Frostmane"] = 9532,
        ["Magosh"] = 9532,
        ["Schwaches Orakel der Finsterflossen"] = 9532,
        ["Deeb"] = 9532,
        ["Matriarchin der Blutfedern"] = 9532,
        ["Schamane der Schwarzfelle"] = 9532,
        ["Orakel der Grauflossen"] = 9532,
        ["Sirene der Säbelflossen"] = 9532,
        ["Orakel der Fetzenflossen"] = 9532,
        ["Schamane der Felsfäuste"] = 9532,
        ["Schamane der Staubspeier"] = 9532,
        ["Donnernder Verbannter"] = 9532,
        ["Mystiker der Staubspeier"] = 9532,
        ["Schamane der Borstennacken"] = 9532,
        ["Windhexe der Windfurien"] = 9532,
        ["Matriarchin der Windfurien"] = 9532,
        ["Sturmhexe der Staubschwingen"] = 9532,
        ["Donnerfalkenjungtier"] = 9532,
        ["Sturmruferin der Kolkar"] = 9532,
        ["Nak"] = 9532,
        ["Boahn"] = 9532,
        ["Schattendickichtregenrufer"] = 9532,
        ["Druide des Giftzahns"] = 9532,
        ["Caedakar der Heimtückische"] = 9532,
        ["Sturmhexe der Blutfurien"] = 9532,
        ["Junge Schimäre"] = 9532,
        ["Sturmruferin der Galak"] = 9532,
        ["Bewahrer Ordanus"] = 9532,
        ["Orakel der Schlammflossen"] = 9532,
        ["Matschpanzerscharrer"] = 9532,
        ["Geomant von Razorfen"] = 9532,
        ["Windhetzer der Kolkar"] = 9532,
        ["Windhetzer der Magram"] = 9532,
        ["Windhetzer der Gelkis"] = 9532,
        ["Großknecht Cozzle"] = 9532,
        ["Orakel der Nachtaugen"] = 9532,
        ["Erdenrufer Halmgar"] = 9532,
        ["Roogug"] = 9532,
        ["Sturmbuchtorakel"] = 9532,
        ["Nebelschwingenwüter"] = 9532,
        ["Sturmseher der Kolkar"] = 9532,
        ["Ragefire-Schamane"] = 9532,
        ["Schneeblinde Windruferin"] = 9532,
        ["Lorgus Jett"] = 12167,
        ["Meerhexe der Rächerflossen"] = 8598,
        ["Mystiker der Rotkammgnolle"] = 20802,
        ["Zauberhexer der Grimmtotem"] = 20802,
        ["Schamane der Morastschnauzen"] = 20805,
        ["Zauberhexerin der Säbelflossen"] = 20824,
        ["Sterndeuter der Vilebranch"] = 20824,
        ["Wolkenschlange"] = 8246,
        ["Alte Wolkenschlange"] = 8246,
        ["Primalist Thurloga"] = 15234,
        ["Irondeep-Schamane"] = 15801,
    },
    ["Manabrand"] = {
        ["PRIEST"] = 10876,
        ["Verschlagener Siechdrache"] = 17630,
        ["Spukvision"] = 11981,
        ["Manapirscher von Hederine"] = 15980,
        ["Meerhexe der Rächerflossen"] = 2691,
        ["Teufelstöter"] = 2691,
        ["Abtrünniger Bewahrer"] = 2691,
        ["Beschworener Teufelsjäger"] = 2691,
        ["Akolyt der Totenköpfe"] = 15785,
    },
    ["Leuchtende silberne Brustplatte"] = {
        Professions = 2675,
    },
    ["Segen des Lichts"] = {
        ["PALADIN"] = 19979,
    },
    ["Vergiftete Harpune"] = {
        ["Captain Greenskin"] = 5208,
    },
    ["Lil' Smoky"] = {
        Professions = 15633,
    },
    ["Aufgeben"] = {
        ["Hulfnar Stonetotem"] = 17648,
    },
    ["Alchimie"] = {
        Professions = 3101,
        Professions = 3464,
        Professions = 11611,
    },
    ["Sumpfgeist beschwören"] = {
        ["Sumpfgeist"] = 9636,
        ["Sumpfredner"] = 9636,
    },
    ["Scharlachroten Hund beschwören"] = {
        ["Scharlachroter Hund"] = 17164,
        ["Scharlachroter Jäger"] = 17164,
    },
    ["Schwarze Magiestoffhandschuhe"] = {
        Professions = 12053,
    },
    ["Lavaexplosion B beschwören"] = {
        ["Kernratte"] = 21900,
    },
    ["Schwerer Wetzstein"] = {
        Professions = 2674,
    },
    ["Dunkler Lederumhang"] = {
        Professions = 2168,
    },
    ["Flimmerkernmantel"] = {
        Professions = 20848,
    },
    ["Fläschchen der Titanen"] = {
        Professions = 17635,
    },
    ["Fluch der Herzschinder"] = {
        ["Satyr der Herzschinder"] = 6946,
        ["Schwindler der Herzschinder"] = 6946,
        ["Schattenpirscher der Herzschinder"] = 6946,
        ["Höllenrufer der Herzschinder"] = 6946,
    },
    ["Großer Trank des traumlosen Schlafs"] = {
        Professions = 24366,
    },
    ["Waffe des Frostbrands"] = {
        ["SHAMAN"] = 16356,
    },
    ["Seelenflamme"] = {
        ["Nelson der Nette"] = 23272,
    },
    ["Große Adeptenrobe"] = {
        Professions = 7643,
    },
    ["Sabbernden Worg rufen"] = {
        ["Sabbernder Worg"] = 7488,
    },
    ["Mentale Kontrolle"] = {
        ["Doktor Weavil"] = 25772,
    },
    ["Strahlung"] = {
        ["Bestrahlter Eindringling"] = 9798,
        ["Bestrahlter Plünderer"] = 9798,
    },
    ["Brutlingessenz herstellen"] = {
        ["Schwarzer Brutling"] = 16027,
        ["Brühender Brutling"] = 16027,
        ["Flammenschuppenbrutling"] = 16027,
    },
    ["Kleine Bronzebombe"] = {
        Professions = 3941,
    },
    ["Handschuhe der Meditation"] = {
        Professions = 3852,
    },
    ["Gedankenkontrolle"] = {
        ["PRIEST"] = 10912,
    },
    ["Prismatischen Verbannten beschwören"] = {
        ["Prismatischer Verbannter"] = 10388,
        ["Myzrael"] = 10388,
        ["Prismatischer Verbannter"] = 4937,
        ["Myzrael"] = 4937,
    },
    ["Staub des Verfalls"] = {
        ["ROGUE"] = 2928,
    },
    ["Brust - Schwache Gesundheit"] = {
        Professions = 7420,
    },
    ["Grüne Eisenhalsberge"] = {
        Professions = 3508,
    },
    ["Schattenblitzsalve"] = {
        ["Varimathras"] = 20741,
        ["Morloch"] = 17228,
        ["Balgaras der Niederträchtige"] = 9081,
        ["Hohepriesterin Hai'watna"] = 14887,
    },
    ["Giftiger Stich"] = {
        ["Murlocküstenläufer"] = 7357,
        ["Bandit der Flusspfoten"] = 7357,
    },
    ["Arthas' Gabe"] = {
        Professions = 11466,
    },
    ["Klon"] = {
        ["Geklonter Brühschlammer"] = 14146,
        ["Urzeitbrühschlammer"] = 14146,
    },
    ["Fluch der Pein"] = {
        ["WARLOCK"] = 11713,
        ["Searingblade-Kultist"] = 18266,
        ["Balizar der Anstößige"] = 14868,
    },
    ["Runenverziertes Lederstirnband"] = {
        Professions = 19082,
    },
    ["Geisterfarbstoff"] = {
        Professions = 11473,
    },
    ["Eisnova"] = {
        ["Phasenpeitscher"] = 22519,
    },
    ["Großer Schattenschutztrank"] = {
        Professions = 17578,
    },
    ["Schwaches Gift"] = {
        ["Junger Nachtweber"] = 6751,
        ["Nachtweber"] = 6751,
        ["Waldweberspinne"] = 6751,
        ["Skorpidarbeiter"] = 6751,
    },
    ["Lebensentzug"] = {
        ["WARLOCK"] = 18881,
    },
    ["Hinrichten"] = {
        ["WARRIOR"] = 20662,
        ["Töter der Schattenfelle"] = 7160,
        ["Kopfjäger der Witherbark"] = 7160,
        ["Töterin der Hexenschwingen"] = 7160,
        ["Töterin der Blutfurien"] = 7160,
        ["Salzwüstenaasfresser"] = 7160,
        ["Salzwüstengeier"] = 7160,
    },
    ["Großer Steinschildtrank"] = {
        Professions = 17570,
    },
    ["Starker Trollbluttrank"] = {
        Professions = 3176,
    },
    ["Schneller Frostzauberschutz"] = {
        ["Murlocorakel"] = 4980,
        ["Mystiker der Skullsplitter"] = 4980,
        ["Skelettwärter"] = 4980,
        ["Gezeitenjäger der Finsterflossen"] = 4980,
        ["Hexer von Dalaran"] = 4980,
        ["Zauberhexerin der Rächerflossen"] = 4980,
        ["Lady Vespia"] = 4980,
    },
    ["Pfeilmacherhandschuhe"] = {
        Professions = 9145,
    },
    ["Lavaexplosion H beschwören"] = {
        ["Kernratte"] = 21906,
    },
    ["Empfindlicher Arkanitwandler"] = {
        Professions = 19815,
    },
    ["Willensverfall"] = {
        ["Bastard der Flusspfoten"] = 8016,
        ["Bastard der Moosfelle"] = 8016,
        ["Bluthornschmetterschwanz"] = 8016,
    },
    ["Handschuhe - Beweglichkeit"] = {
        Professions = 13815,
    },
    ["Schildkrötenschuppenarmschienen"] = {
        Professions = 10518,
    },
    ["Dunkles Seidenhemd"] = {
        Professions = 3870,
    },
    ["Steinkraft"] = {
        ["Schattendickichtsteinrücker"] = 6864,
    },
    ["Segen des Schutzes"] = {
        ["PALADIN"] = 10278,
    },
    ["Seelenverzehr"] = {
        ["Teremus der Verschlinger"] = 12667,
    },
    ["Dämonensklave"] = {
        ["WARLOCK"] = 11726,
    },
    ["Perlengriffdolch"] = {
        Professions = 6517,
    },
    ["Aspekt der Wildnis"] = {
        ["HUNTER"] = 20190,
    },
    ["Solarplexus"] = {
        ["ROGUE"] = 11286,
    },
    ["Stahlwaffenkette"] = {
        Professions = 7224,
    },
    ["Rekonstruktion"] = {
        ["Tor'gan"] = 4093,
    },
    ["Verschimmern"] = {
        ["Gazban"] = 3648,
    },
    ["Elixier der rohen Gewalt"] = {
        Professions = 17557,
    },
    ["Eisschlag"] = {
        ["Mechanofrostwandler"] = 11264,
    },
    ["Totem der Manaquelle"] = {
        ["SHAMAN"] = 10497,
    },
    ["Waffe - Wintermacht"] = {
        Professions = 21931,
    },
    ["Sturmschleierschultern"] = {
        Professions = 19090,
    },
    ["Entkräften"] = {
        ["Elender Verirrter"] = 11963,
        ["Verweilender Hochgeborener"] = 11963,
    },
    ["Ironforgebrustplatte"] = {
        Professions = 8367,
    },
    ["Flüssiges Feuer werfen"] = {
        ["Aerie-Greif"] = 23969,
        ["Kriegsreiter"] = 23969,
    },
    ["Kristallblitzstrahl"] = {
        ["Eisenkieferbasilisk"] = 5106,
        ["Schuppenbauch"] = 5106,
        ["Glashautbasilisk"] = 5106,
        ["Rotsteinkristallhaut"] = 5106,
    },
    ["Schwere Skorpidstulpen"] = {
        Professions = 19064,
    },
    ["Traumzwirnreif"] = {
        Professions = 12092,
    },
    ["Einhüllende Winde"] = {
        ["Sirene der Säbelflossen"] = 6728,
        ["Windruferin der Hexenschwingen"] = 6728,
        ["Windruferin der Blutfurien"] = 6728,
        ["Windhetzerin der Galak"] = 6728,
        ["Staubwirker von Razorfen"] = 6728,
        ["Windhetzer der Kolkar"] = 6728,
        ["Schwester Wildkralle"] = 6728,
    },
    ["Blendungspulver"] = {
        ["ROGUE"] = 6510,
    },
    ["Auferstandenen Lakaien beschwören"] = {
        ["Auferstandener Lakai"] = 17618,
        ["Dunkler Beschwörer aus Scholomance"] = 17618,
    },
    ["Spezialbräu"] = {
        ["Bom'bay"] = 16712,
    },
    ["Vulkanische Gamaschen"] = {
        Professions = 19059,
    },
    ["Dunkeleisenpanzer"] = {
        Professions = 15293,
    },
    ["Portal: Undercity"] = {
        ["MAGE"] = 11418,
    },
    ["Arkanem widerstehen"] = {
        ["Scharlachroter Sanitäter"] = 17175,
    },
    ["Fieser Trick"] = {
        ["ROGUE"] = 1833,
    },
    ["Totem der Krankheitsreinigung"] = {
        ["SHAMAN"] = 8170,
    },
    ["Azurblaue Seidenweste"] = {
        Professions = 3859,
    },
    ["Zerhäckseln"] = {
        ["ROGUE"] = 6774,
    },
    ["Fackelexplosion"] = {
        ["Wachmann des Syndikats"] = 3602,
        ["Kerkermeister Eston"] = 3602,
        ["Ausgräber des dunklen Strangs"] = 3602,
    },
    ["Große Voodoorobe"] = {
        Professions = 10520,
    },
    ["Mondfestglück"] = {
        ["Pats Feuerwerkstyp (ELUNE)"] = 26522,
    },
    ["Verschnörkelte Mithrilhandschuhe"] = {
        Professions = 9950,
    },
    ["Feuriger Kettengurt"] = {
        Professions = 20872,
    },
    ["Verwandlung"] = {
        ["MAGE"] = 12826,
        ["Hexer des Syndikats"] = 13323,
        ["Aufseherin Belamoore"] = 13323,
        ["Arkanist Doan"] = 13323,
        ["Captain Balinda Stonehearth"] = 13323,
    },
    ["Zweihandäxte und -Streitkolben"] = {
        ["SHAMAN"] = 16269,
    },
    ["Modisches rotes Hemd"] = {
        Professions = 3866,
    },
    ["Frostsäblerstiefel"] = {
        Professions = 19066,
    },
    ["Schild - Große Ausdauer"] = {
        Professions = 20017,
    },
    ["Heldenhafter Stoß"] = {
        ["WARRIOR"] = 25286,
    },
    ["Arkanes Vakuum"] = {
        ["Azuregos"] = 21147,
    },
    ["Ferngesteuerten Golem beschwören"] = {
        ["Ferngesteuerter Golem"] = 3605,
        ["Goblin-Ingenieur"] = 3605,
    },
    ["Goldkraftkern"] = {
        Professions = 12584,
    },
    ["Schwerer Schleifstein"] = {
        Professions = 3337,
    },
    ["Silithidenschwarm"] = {
        ["Silithidschwarm"] = 6589,
        ["Silithidschwärmer"] = 6589,
        ["Schwärmer der Hazzali"] = 6589,
        ["Schwärmer der Centipaar"] = 6589,
        ["Silithidschwarm"] = 10722,
        ["Silithidschwärmer"] = 10722,
        ["Schwärmer der Hazzali"] = 10722,
        ["Schwärmer der Centipaar"] = 10722,
    },
    ["Gedankenerforschung"] = {
        ["Spitzel der Schmetterschilde"] = 16037,
    },
    ["Erdengriff-Totem"] = {
        ["Erdengriff-Totem"] = 8376,
        ["Hexendoktor der Bloodscalp"] = 8376,
        ["Totemiker von Razorfen"] = 8376,
        ["Distelwurzelformer"] = 8376,
    },
    ["Schwere Skorpidgamaschen"] = {
        Professions = 19075,
    },
    ["Manaachat herbeizaubern"] = {
        ["MAGE"] = 759,
    },
    ["Kirtonos transformieren"] = {
        ["Kirtonos der Herold"] = 16467,
    },
    ["Ehrenpunkte +50"] = {
        ["Kriegshetzer der Horde"] = 24960,
        ["Brigadegeneral der Allianz"] = 24960,
    },
    ["Silithidenpocken"] = {
        ["Silithideindringling"] = 8137,
        ["Wespe der Gorishi"] = 8137,
        ["Arbeiter der Gorishi"] = 8137,
        ["Häscher der Gorishi"] = 8137,
        ["Stecher der Gorishi"] = 8137,
        ["Tunnelgräber der Gorishi"] = 8137,
        ["Schwarmwache der Gorishi"] = 8137,
        ["Schwarmkönigin der Gorishi"] = 8137,
    },
    ["Phasenverschiebung"] = {
        Pet = 4511,
    },
    ["Klingenwirbel"] = {
        ["ROGUE"] = 13877,
    },
    ["Elixier der Traumvision"] = {
        Professions = 11468,
    },
    ["Omen der Klarsicht"] = {
        ["DRUID"] = 16864,
    },
    ["Fanatische Klinge"] = {
        ["Fanatiker der Burning Blade"] = 5262,
    },
    ["Wassergestalt"] = {
        ["DRUID"] = 1066,
    },
    ["Brust - Geringes Mana"] = {
        Professions = 7776,
    },
    ["Geringe Unsichtbarkeit entdecken"] = {
        ["WARLOCK"] = 132,
    },
    ["Einstimmung auf den Kern"] = {
        ["Lothos Riftwaker"] = 22877,
    },
    ["Rotes Raketenbündel"] = {
        Professions = 26425,
    },
    ["Sofort wirkendes Gift III"] = {
        ["ROGUE"] = 8691,
    },
    ["Dunkle Lederstiefel"] = {
        Professions = 2167,
    },
    ["Mondstoffhandschuhe"] = {
        Professions = 22869,
    },
    ["Deviatschuppenumhang"] = {
        Professions = 7953,
    },
    ["Welkende Hitze"] = {
        ["Uralter Kernhund"] = 19367,
    },
    ["Göttliches Eingreifen"] = {
        ["PALADIN"] = 19752,
    },
    ["Beschworener Dämon"] = {
        ["Beschworener Leerwandler"] = 7741,
        ["Beschworener Sukkubus"] = 7741,
        ["Beschworener Teufelsjäger"] = 7741,
        ["Blutbewahrer der Hakkari"] = 7741,
        ["Nachtmahrunterdrücker"] = 7741,
        ["Kreischergeist"] = 7741,
        ["Arugal"] = 7741,
        ["Dämonengeist"] = 7741,
    },
    ["Ätzende Giftspucke"] = {
        ["Chimaerok"] = 20629,
    },
    ["Auge von Kilrogg"] = {
        ["WARLOCK"] = 126,
    },
    ["Essen herbeizaubern"] = {
        ["MAGE"] = 28612,
    },
    ["Hyperstrahlender Flammenreflektor"] = {
        Professions = 23081,
    },
    ["Handgenähte Armschienen"] = {
        Professions = 9059,
    },
    ["Rosarote Schutzbrille"] = {
        Professions = 12618,
    },
    ["Wächterglut des Weidenmanns"] = {
        ["Weidenmannwache"] = 25007,
    },
    ["Grüne Eisenstulpen"] = {
        Professions = 3336,
    },
    ["Smite-Stampfen"] = {
        ["Mr. Smite"] = 6432,
    },
    ["Verheerer der Magram beschwören"] = {
        ["Untoter Verheerer"] = 18166,
        ["Ausgestoßener Totenbeschwörer"] = 18166,
    },
    ["Robuster Eisenschlägel"] = {
        Professions = 3494,
    },
    ["Niederschlagen"] = {
        ["Schläger der Felsfäuste"] = 11428,
        ["Teufelswache"] = 11428,
        ["Platsch der Grausame"] = 11428,
        ["Krieger der Gurubashi"] = 11428,
        ["Duriel Moonfire"] = 11428,
        ["Zzarc'Vul"] = 5164,
        ["Berster der Rotkammgnolle"] = 5164,
        ["Sin'Dall"] = 5164,
        ["Knochenknacker der Felsenkiefer"] = 5164,
        ["Oger der Mo'grosh"] = 5164,
        ["Myrmidone der Sturmschuppen"] = 5164,
        ["Wutschäumender Steingeist"] = 5164,
        ["Wutklauengrizzly"] = 5164,
        ["Geringe Teufelswache"] = 18812,
        ["Strashazmyrmidone"] = 18812,
        ["Drek'Thar"] = 19128,
    },
    ["Gnomen-Unglücksverhinderungsgürtel"] = {
        Professions = 12903,
    },
    ["Fluch aufheben"] = {
        ["DRUID"] = 2782,
    },
    ["Flammkinwüterich beschwören"] = {
        ["Flammkinwüterich"] = 15711,
        ["Terrorstifter"] = 15711,
    },
    ["Seelenstein herstellen (erheblich)"] = {
        ["WARLOCK"] = 20757,
    },
    ["Schildkrötenschuppenhandschuhe"] = {
        Professions = 10509,
    },
    ["Blinzeln"] = {
        ["MAGE"] = 1953,
    },
    ["Trank des traumlosen Schlafs"] = {
        Professions = 15833,
    },
    ["Frostschuss"] = {
        ["Sirene der Grollflossen"] = 12551,
        ["Bestienausbilder von Razorfen"] = 6984,
    },
    ["Teufelsstoffhose"] = {
        Professions = 18419,
    },
    ["Gyromatischer Mikroregler"] = {
        Professions = 12590,
    },
    ["Blaues Raketenbündel"] = {
        Professions = 26423,
        ["Pats Feuerwerkstyp (BLAU)"] = 26304,
        ["Herold des Mondfests"] = 26304,
        ["Abgesandter des Mondfests"] = 26304,
    },
    ["Durchdringendes Heulen"] = {
        ["WARRIOR"] = 12323,
    },
    ["Verheeren"] = {
        ["DRUID"] = 9867,
        ["Großer Nachtsauger"] = 3242,
        ["Vampirnachtsauger"] = 3242,
        ["Marterklaue"] = 3242,
        ["Distelbär"] = 3242,
        ["Ergrauter Distelbär"] = 3242,
        ["Höhlenmutter"] = 3242,
        ["Distelbärjunges"] = 3242,
        ["Aku'mai-Schnappkiefer"] = 8391,
    },
    ["Der Narren Mühsal"] = {
        ["Simone die Unauffällige"] = 23504,
        ["Franklin der Freundliche"] = 23504,
        ["Artorius der Liebenswürdige"] = 23504,
        ["Nelson der Nette"] = 23504,
    },
    ["Wirbeltrip"] = {
        ["Kopfjäger der Gurubashi"] = 24048,
        ["Sprecher der Skullsplitter"] = 24048,
    },
    ["Rotwelpenhandschuhe"] = {
        Professions = 9072,
    },
    ["Geringe Unsichtbarkeit"] = {
        Pet = 7870,
    },
    ["Waffe - Beweglichkeit"] = {
        Professions = 23800,
    },
    ["Stiefel - Große Ausdauer"] = {
        Professions = 20020,
    },
    ["Hauch von Vaelastrasz"] = {
        ["Vaelastrasz der Rote"] = 16319,
    },
    ["Prankenhieb"] = {
        ["DRUID"] = 9908,
    },
    ["Großer Manatrank"] = {
        Professions = 11448,
    },
    ["Barbarische Eisenbrustplatte"] = {
        Professions = 9813,
    },
    ["Grüner Ledergürtel"] = {
        Professions = 3774,
    },
    ["Höllenbestienzerstörer rufen"] = {
        ["El Pollo Grande"] = 23056,
        ["Niby der Allmächtige"] = 23056,
    },
    ["Schattenzwirnhose"] = {
        Professions = 12052,
    },
    ["Armschiene - Geringe Ausdauer"] = {
        Professions = 13501,
    },
    ["Skelettdiener beschwören"] = {
        ["Skelettdiener"] = 12420,
        ["Nekromant der Totenköpfe"] = 12420,
        ["Botschafter Blutzorn"] = 12420,
        ["Totenbeschwörer"] = 12420,
        ["Thuzadintotenbeschwörer"] = 12420,
    },
    ["Vergrößern"] = {
        ["Twilight-Bewahrer der Lehren"] = 8365,
    },
    ["Elixier der großen Feuermacht"] = {
        Professions = 26277,
    },
    ["Tauch-Rundschlag"] = {
        ["Üble Fledermaus"] = 7145,
    },
    ["Robustes Dynamit"] = {
        Professions = 12586,
    },
    ["Verschlingende Seuche"] = {
        ["PRIEST"] = 19280,
    },
    ["Elementarbeherrschung"] = {
        ["SHAMAN"] = 16166,
    },
    ["Versengen-Totem"] = {
        ["Versengen-Totem"] = 15038,
    },
    ["Dunkle Lederschultern"] = {
        Professions = 3769,
    },
    ["Vergifteter Schuss"] = {
        ["Bestienmeister von Razorfen"] = 8275,
        ["Kurzens Schattenjäger"] = 8806,
        ["Jäger der Borstennacken"] = 8806,
    },
    ["Großer Segen des Lichts"] = {
        ["PALADIN"] = 25890,
    },
    ["Vorbereitung"] = {
        ["ROGUE"] = 14185,
    },
    ["Schattenzwirnschultern"] = {
        Professions = 12076,
    },
    ["Silithidenlarve beschwören"] = {
        ["Silithidlarve"] = 6588,
        ["Silithidkrabblerei"] = 6588,
    },
    ["R.C.V.K. herbeizaubern"] = {
        ["Wrenix' Elektrodingsda-Apparat"] = 9977,
    },
    ["Seidenes Stirnband"] = {
        Professions = 8762,
    },
    ["Eiskältefalle"] = {
        ["HUNTER"] = 14311,
    },
    ["Sayges dunkles Schicksal der Intelligenz"] = {
        ["Sayge"] = 23766,
    },
    ["Barbarischer Harnisch"] = {
        Professions = 6661,
    },
    ["Traumzwirnhandschuhe"] = {
        Professions = 12067,
    },
    ["Flammenkanone"] = {
        ["Kriegshammerkonstrukt"] = 15575,
    },
    ["Gerstenbräu trinken"] = {
        ["Yorus Gerstenbräu"] = 8554,
    },
    ["Macht der Holzschlundfeste"] = {
        Professions = 23703,
    },
    ["Todesminen-Dynamit"] = {
        ["Untoter Dynamitexperte"] = 7395,
    },
    ["Krankheitsberührung"] = {
        ["Faulender Toter"] = 3234,
        ["Verheerter Kadaver"] = 3234,
        ["Hungernder Toter"] = 3234,
        ["Torkelnder Schrecken"] = 3234,
    },
    ["Diener von Morganth"] = {
        ["Diener von Morganth"] = 3611,
        ["Morganth"] = 3611,
    },
    ["Schatten sichtbar"] = {
        ["Schemengestalt von Jin'do"] = 24313,
    },
    ["Sturmschlag"] = {
        ["SHAMAN"] = 17364,
    },
    ["Mondstoffweste"] = {
        Professions = 18447,
    },
    ["Azurachat verzaubern"] = {
        ["Lehrling Kryten"] = 4319,
    },
    ["Schattenzauberschutz"] = {
        ["WARLOCK"] = 28610,
    },
    ["Blutrebenstiefel"] = {
        Professions = 24093,
    },
    ["Zulianische Wächter beschwören"] = {
        ["Zulianischer Wächter"] = 24183,
    },
    ["Behände Reflexe"] = {
        ["Scharlachroter Krieger"] = 3238,
        ["Mr. Smite"] = 6264,
        ["Offizier von Bael'dun"] = 6264,
    },
    ["Zauberstein herstellen (erheblich)"] = {
        ["WARLOCK"] = 17728,
    },
    ["Wildtierkunde"] = {
        ["HUNTER"] = 1462,
    },
    ["Armschiene - Geringe Stärke"] = {
        Professions = 13536,
    },
    ["Schimärenstiefel"] = {
        Professions = 19063,
    },
    ["Tückische Lederarmschienen"] = {
        Professions = 19052,
    },
    ["Infiziertes Rückgrat"] = {
        ["Siechender Speerträger"] = 12245,
    },
    ["Kriegsbärenwollwäsche"] = {
        Professions = 19080,
    },
    ["Knochenrüstung"] = {
        ["Nekromant der Totenköpfe"] = 11445,
    },
    ["Zauberschutz von Zanzil"] = {
        ["Zauberschutz von Zanzil"] = 8832,
        ["Zanzil der Ausgestoßene"] = 8832,
    },
    ["Brust - Erhebliche Gesundheit"] = {
        Professions = 20026,
    },
    ["Seidenstoffballen"] = {
        Professions = 3839,
    },
    ["Segen der Könige"] = {
        ["PALADIN"] = 20217,
    },
    ["Smokinghemd"] = {
        Professions = 12085,
    },
    ["Kopfnuss"] = {
        ["ROGUE"] = 11297,
        ["Hogger"] = 6730,
        ["Sturmechse"] = 6730,
        ["Spiegelpanzerschnapper"] = 6730,
    },
    ["Große Voodoomaske"] = {
        Professions = 10531,
    },
    ["Ohrenbetäubendes Kreischen"] = {
        ["Kreischer der Säbelflossen"] = 3589,
        ["Kreischende Harpyie"] = 3589,
        ["Kreischerin der Grollflossen"] = 3589,
        ["Eisenschnabelkreischer"] = 3589,
        ["Winterspringkreischer"] = 3589,
        ["Hassschriller"] = 3589,
        ["Kreischende Banshee"] = 3589,
        ["Schneeblinde Harpyie"] = 3589,
        ["Furie Shelda"] = 3589,
    },
    ["Falle entschärfen"] = {
        ["ROGUE"] = 1842,
    },
    ["Verpestende Fäulnis"] = {
        ["Stinkender Leichnam"] = 7102,
        ["Kräuterkundiger der Verlassenen"] = 7102,
    },
    ["Sicht verdunkeln"] = {
        ["Dunkler Grimmling"] = 5514,
    },
    ["Ruf des Grabes"] = {
        ["Untoter Ausgräber"] = 5137,
        ["Azshir der Schlaflose"] = 5137,
    },
    ["Schlachtruf"] = {
        ["WARRIOR"] = 25289,
        ["Narg der Zuchtmeister"] = 9128,
        ["Bannschuppencaptain"] = 9128,
        ["Alpha der Moosfelle"] = 9128,
        ["Aufrührer der Defias"] = 9128,
        ["Bazil Thredd"] = 9128,
        ["Ma'ruk Wyrmschuppe"] = 9128,
        ["Rudelläufer der Kolkar"] = 9128,
        ["Vollstrecker der Venture Co."] = 9128,
        ["Captain Hellstieg"] = 9128,
        ["Oberanführer Rammhauer"] = 9128,
    },
    ["Klingenhaut"] = {
        ["Quetschzahn"] = 16610,
    },
    ["Verseuchung"] = {
        Professions = 10011,
    },
    ["Demoralisierendes Gebrüll"] = {
        ["DRUID"] = 9898,
        ["Cenarischer Beschützer"] = 15727,
    },
    ["Arugals Gabe"] = {
        ["Sohn von Arugal"] = 7124,
    },
    ["Entropischer Stich"] = {
        ["Franklin der Freundliche"] = 23260,
    },
    ["Kupferne Kettenweste"] = {
        Professions = 3321,
    },
    ["Gewaltschild"] = {
        ["Kurzens Stabschef"] = 3260,
        ["Schildwache von Dalaran"] = 3260,
    },
    ["Handschuhe des Altvaters"] = {
        Professions = 21943,
    },
    ["Einschüchterung"] = {
        ["HUNTER"] = 19577,
        ["Crushridge-Vollstrecker"] = 7093,
    },
    ["Leichte Feder"] = {
        ["PRIEST"] = 17056,
    },
    ["Kristallene Gefangenschaft"] = {
        ["Kommandant der Verdammniswache"] = 23020,
    },
    ["Druckwelle"] = {
        ["MAGE"] = 13021,
    },
    ["Manatrank"] = {
        Professions = 3452,
    },
    ["Tödliches Gift"] = {
        ["ROGUE"] = 2835,
        ["Giftnebellauerer"] = 3583,
        ["Mazzranache"] = 3583,
        ["Giftige Wolkenschlange"] = 3583,
        ["Graunebelremit"] = 3583,
        ["Fardel Dabyrie"] = 3583,
    },
    ["Teleportieren: Ironforge"] = {
        ["MAGE"] = 3562,
    },
    ["Mittleres Leder"] = {
        Professions = 20648,
    },
    ["Hellstoffhandschuhe"] = {
        Professions = 18415,
    },
    ["Dunkeleisenlandmine"] = {
        ["Dunkeleisenlandmine"] = 11802,
        ["Agent der Dunkeleisenzwerge"] = 11802,
        ["Thauris Balgarr"] = 11802,
    },
    ["Zorn der Gerechtigkeit"] = {
        ["PALADIN"] = 25780,
    },
    ["Zorn des Wildtiers"] = {
        ["HUNTER"] = 19574,
    },
    ["Matsch schleudern"] = {
        ["Ardo Schmutzpranke"] = 3650,
        ["Gnoll der Morastschnauzen"] = 3650,
    },
    ["Ätzende Säure"] = {
        ["Futterwühler der Bruchschnauzen"] = 8245,
    },
    ["Grünwelpenarmschienen"] = {
        Professions = 9202,
    },
    ["Eisbarriere"] = {
        ["MAGE"] = 13033,
    },
    ["Feuerbrandfalle"] = {
        ["HUNTER"] = 14305,
    },
    ["Ehrenpunkte +138"] = {
        ["Kriegshetzer der Horde"] = 24962,
        ["Brigadegeneral der Allianz"] = 24962,
    },
    ["Nachtseeles Klagelied"] = {
        ["Nachtseele"] = 3485,
    },
    ["Dunkler Pakt"] = {
        ["WARLOCK"] = 18938,
    },
    ["Gefüllten Verwahrungskasten herstellen"] = {
        ["Spaltbrut"] = 9010,
    },
    ["Schildkrötenschuppenhelm"] = {
        Professions = 10552,
    },
    ["Sofort wirkendes Gift"] = {
        ["ROGUE"] = 8681,
    },
    ["Magierkugel herstellen"] = {
        ["Tabetha"] = 9156,
    },
    ["Magie verstärken"] = {
        ["MAGE"] = 10170,
    },
    ["Bronzene Stiefel mit Versilberung"] = {
        Professions = 3331,
    },
    ["Totgeglaubt zurückgelassen"] = {
        ["Agathelos der Tobende"] = 8555,
    },
    ["Weiter Prankenhieb"] = {
        ["Oberanführer Mok'Morokk"] = 6749,
    },
    ["Grünes Seidenpack"] = {
        Professions = 6693,
    },
    ["Kreuzfahrerstoß"] = {
        ["Scharlachroter Kommandant Mograine"] = 14518,
        ["Scharlachroter Kavalier"] = 14517,
    },
    ["Schlangensäuberung"] = {
        ["Jünger von Naralex"] = 6270,
    },
    ["Beschaulichkeit"] = {
        Professions = 16983,
    },
    ["Zerkrachen"] = {
        ["Geschmolzener Riese"] = 18944,
    },
    ["Runenverzierte Kupferhose"] = {
        Professions = 3324,
    },
    ["Vipernbiss"] = {
        ["HUNTER"] = 14280,
    },
    ["Teufelsstofftasche"] = {
        Professions = 26086,
    },
    ["Schattenwort: Schmerz"] = {
        ["PRIEST"] = 10894,
        ["Xabraxxis"] = 11639,
        ["Vol'jin"] = 17146,
        ["Strashazsirene"] = 15654,
        ["Coldmine-Feldmesser"] = 15654,
        ["Irondeep-Feldmesser"] = 15654,
        ["Coldmine-Feldmesserveteran"] = 15654,
        ["Irondeep-Feldmesserveteran"] = 15654,
        ["Irondeep-Feldmesserchampion"] = 15654,
    },
    ["Auraschock"] = {
        ["Dunkelkreischer"] = 14538,
    },
    ["Starkes Spalten"] = {
        ["Silithidverheerer"] = 8255,
        ["Razelikh der Entweiher"] = 8255,
    },
    ["Handschuhe - Große Stärke"] = {
        Professions = 20013,
    },
    ["Schwärzliche Armschienen"] = {
        Professions = 9201,
    },
    ["Handgenähte Leinenkniehosen"] = {
        Professions = 3842,
    },
    ["Blutsauger"] = {
        ["WARLOCK"] = 11700,
        ["Varimathras"] = 20743,
    },
    ["Kompaktes Ernteschnitterset"] = {
        Professions = 3963,
    },
    ["Grobes Sprengpulver"] = {
        Professions = 3929,
    },
    ["Mine legen"] = {
        ["Panzermine"] = 25024,
        ["Dampfpanzer"] = 25024,
    },
    ["Kampfrausch"] = {
        ["Zuchtmeister Schnuffel"] = 16170,
        ["Hamhock"] = 6742,
        ["Crushridge-Magier"] = 6742,
        ["Grel'borg der Horter"] = 6742,
        ["Lo'Grosh"] = 6742,
        ["Blutstürmer der Kolkar"] = 6742,
        ["Schamane der Distelfelle"] = 6742,
        ["Hundemeister Loksey"] = 6742,
        ["Adept der Burning Blade"] = 6742,
    },
    ["Mithrilschuppenhose"] = {
        Professions = 9931,
    },
    ["Wolfskopfhelm"] = {
        Professions = 10621,
    },
    ["Rote Wollstiefel"] = {
        Professions = 3847,
    },
    ["Spinnenseidenstiefel"] = {
        Professions = 3855,
    },
    ["Brennende Hartnäckigkeit"] = {
        ["Telf Joolam"] = 8383,
    },
    ["Herausforderndes Gebrüll"] = {
        ["DRUID"] = 5209,
    },
    ["Schwere Mithrilstulpen"] = {
        Professions = 9928,
    },
    ["Rote Leinentasche"] = {
        Professions = 6686,
    },
    ["Untote spüren"] = {
        ["PALADIN"] = 5502,
    },
    ["Umhang - Feuerwiderstand"] = {
        Professions = 13657,
    },
    ["Kupferstreitkolben"] = {
        Professions = 2737,
    },
    ["Runenstoffstiefel"] = {
        Professions = 18423,
    },
    ["Purpurrote Seidenrobe"] = {
        Professions = 8802,
    },
    ["Segen der Opferung"] = {
        ["PALADIN"] = 20729,
    },
    ["Trachten des wahren Glaubens"] = {
        Professions = 18456,
    },
    ["Armschiene - Abwehr"] = {
        Professions = 13931,
    },
    ["Häscher rufen"] = {
        ["Häscher der Gordok"] = 22860,
    },
    ["Runenstoffumhang"] = {
        Professions = 18409,
    },
    ["Kupferarmschienen"] = {
        Professions = 2663,
    },
    ["Magische Rüstung"] = {
        ["MAGE"] = 22783,
    },
    ["Kodostampfen"] = {
        ["Verirrter Brachlandkodo"] = 6266,
        ["Brachlandkodo"] = 6266,
    },
    ["Robuster Schleifstein"] = {
        Professions = 9920,
    },
    ["Sarilus' Elementare"] = {
        ["Schwacher Wasserwächter"] = 6490,
        ["Sarilus Foulborne"] = 6490,
    },
    ["Grüne Eisenstiefel"] = {
        Professions = 3334,
    },
    ["Feuerwiderstand"] = {
        ["HUNTER"] = 24464,
    },
    ["Wollstiefel"] = {
        Professions = 2401,
    },
    ["Brust - Geringe Absorption"] = {
        Professions = 13538,
    },
    ["Dämonen spüren"] = {
        ["WARLOCK"] = 5500,
    },
    ["Wille von Hakkar"] = {
        ["Gurubashi"] = 24178,
    },
    ["Echsenschlag"] = {
        ["Donnerechse"] = 5401,
        ["Blitzechse"] = 5401,
        ["Sturmechse"] = 5401,
        ["Donnerkopf"] = 5401,
        ["Sturmschnauze"] = 5401,
        ["Klippenstürmer"] = 5401,
        ["Tobende Donnerechse"] = 5401,
        ["Alte Donnerechse"] = 5401,
    },
    ["Totem der Giftreinigung"] = {
        ["SHAMAN"] = 8166,
    },
    ["Schwerer Köcher"] = {
        Professions = 9193,
    },
    ["Entfesselungskünstler"] = {
        Racial = 20589,
    },
    ["Robuster Wetzstein"] = {
        Professions = 9918,
    },
    ["Dämonen aufspüren"] = {
        ["HUNTER"] = 19878,
    },
    ["Goldene Schuppenstiefel"] = {
        Professions = 3515,
    },
    ["Stiefel - Geringe Beweglichkeit"] = {
        Professions = 13637,
    },
    ["Hakkar beschwören"] = {
        ["Schemen von Hakkar"] = 12639,
    },
    ["Rotes Festtagskleid"] = {
        Professions = 26403,
    },
    ["Detonation"] = {
        ["Arkanist Doan"] = 9435,
        ["Dunkeleisenlandmine"] = 4043,
    },
    ["Handgenähte Lederhose"] = {
        Professions = 2153,
    },
    ["Selbstmord"] = {
        ["Silithidkrabblerei"] = 7,
    },
    ["Besudelte Gedanken"] = {
        ["Verfluchter Magier"] = 16567,
    },
    ["Geringe Hexerrobe"] = {
        Professions = 6690,
    },
    ["Handschuhe - Hochentwickelter Bergbau"] = {
        Professions = 13841,
    },
    ["Waffe - Geringes Schlagen"] = {
        Professions = 13503,
    },
    ["Frostatem"] = {
        ["Junger Wendigo"] = 3131,
        ["Wendigo"] = 3131,
        ["Bergyeti"] = 3131,
        ["Riesenyeti"] = 3131,
        ["Edan der Heuler"] = 3129,
    },
    ["Dickes Rüstungsset"] = {
        Professions = 10487,
    },
    ["Cookies Kochkunst"] = {
        ["Cookie"] = 5174,
    },
    ["Heiliges Spalten"] = {
        ["Hochlord Taelan Fordring"] = 18819,
        ["Lord Tirion Fordring"] = 18819,
    },
    ["Illusionäres Gespenst beschwören"] = {
        ["Illusionäres Gespenst"] = 17231,
        ["Araj der Beschwörer"] = 17231,
    },
    ["Schattenhaftigkeit"] = {
        Racial = 20580,
    },
    ["Arkanwiderstand"] = {
        ["HUNTER"] = 24510,
    },
    ["Goldener Schuppenkürass"] = {
        Professions = 3511,
    },
    ["Aura der Vergeltung"] = {
        ["PALADIN"] = 10301,
        ["Hochlord Bolvar Fordragon"] = 8990,
        ["Scharlachroter Kommandant Mograine"] = 8990,
    },
    ["Purpurrote Seidenweste"] = {
        Professions = 8791,
    },
    ["Feine Lederhose"] = {
        Professions = 7133,
    },
    ["Verderbte Beweglichkeit"] = {
        ["Pfadfinder der Fäulnisklauen"] = 6817,
        ["Knurrtatze"] = 6817,
    },
    ["Klaue"] = {
        ["DRUID"] = 9850,
        Pet = 3009,
    },
    ["Gletscherweste"] = {
        Professions = 28207,
    },
    ["Verwandlung: Kuh"] = {
        ["MAGE"] = 28270,
    },
    ["Sturzflug"] = {
        Pet = 23148,
        ["Terrorkondor"] = 5708,
        ["Vultros"] = 5708,
        ["Drahtiger Sturzflieger"] = 5708,
        ["Sturzflieger"] = 5708,
        ["Kralliger Sturzflieger"] = 5708,
        ["Prachtschwingenhimmelsjäger"] = 5708,
        ["Schreckenssturzflieger"] = 5708,
    },
    ["Schweres Dynamit"] = {
        Professions = 3946,
    },
    ["Totem der Flammenzunge"] = {
        ["SHAMAN"] = 16387,
    },
    ["Dunkler Schlamm"] = {
        ["Schwarzer Brühschleimer"] = 3335,
        ["Besudelter Brühschlammer"] = 3335,
        ["Schlicker"] = 3335,
    },
    ["Einfache Leinenhose"] = {
        Professions = 12044,
    },
    ["Rotes Leinenhemd"] = {
        Professions = 2392,
    },
    ["Goblin-Überbrückungskabel XL"] = {
        Professions = 23078,
    },
    ["Schwere Wollhandschuhe"] = {
        Professions = 3843,
    },
    ["Verlangsamen"] = {
        ["Mystiker der Moosfelle"] = 11436,
        ["Geomantin der Totenköpfe"] = 11436,
        ["Geomant der Grimmtotem"] = 11436,
        ["Orakel der Zackenkämme"] = 246,
        ["Lady Sarevess"] = 246,
        ["Scharlachroter Zauberhexer"] = 6146,
        ["Captain Balinda Stonehearth"] = 19137,
        ["Twilight-Bewahrer der Lehren"] = 18972,
    },
    ["Schwerer Wollumhang"] = {
        Professions = 3844,
    },
    ["Schwerer bronzener Streitkolben"] = {
        Professions = 3296,
    },
    ["Handschuhe - Schwache Hast"] = {
        Professions = 13948,
    },
    ["Verzaubererkutte"] = {
        Professions = 3857,
    },
    ["Orc-Kriegsgamaschen"] = {
        Professions = 9957,
    },
    ["Gletscherhandschutz"] = {
        Professions = 28209,
    },
    ["Gebrechlichkeit"] = {
        ["Seelenloser Ghul"] = 12530,
        ["Zul'Lor"] = 12530,
    },
    ["Großer Segen der Könige"] = {
        ["PALADIN"] = 25898,
    },
    ["Unheiliger Schild"] = {
        ["Morbent Fel"] = 8909,
    },
    ["Mondscheinweste"] = {
        Professions = 8322,
    },
    ["Rauer Schleifstein"] = {
        Professions = 3320,
    },
    ["Gesundheitsstein herstellen (erheblich)"] = {
        ["WARLOCK"] = 11730,
    },
    ["Rauchbombe"] = {
        ["Kurzens Kommandosoldat"] = 7964,
        ["Bazil Thredd"] = 7964,
        ["Colonel Kurzen"] = 8817,
    },
    ["Flimmerkerngamaschen"] = {
        Professions = 23667,
    },
    ["Verhexung der Schwäche"] = {
        ["PRIEST"] = 19285,
    },
    ["Säurespritzer"] = {
        ["Cookie"] = 6306,
        ["Grüner Wyrmkin"] = 6306,
        ["Alter Drachkin"] = 6306,
        ["Gluckser"] = 6306,
    },
    ["Blutegel"] = {
        ["Moonrage-Nimmersatt"] = 6958,
        ["Harpyie der Blutfedern"] = 6958,
        ["Hezrul Blutmal"] = 6958,
    },
    ["Thoriumapparat"] = {
        Professions = 19791,
    },
    ["Rüstung durchstechen"] = {
        ["Koboldminenarbeiter"] = 6016,
        ["Ruklar der Fallensteller"] = 6016,
        ["Knochenkauer"] = 6016,
        ["Hogger"] = 6016,
        ["Brack"] = 6016,
        ["Minenarbeiter der Defias"] = 6016,
        ["Minenarbeiter der Venture Co."] = 6016,
        ["Koboldbuddler"] = 6016,
        ["Bäumlingborkenreißer"] = 6016,
        ["Dunkelküstendrescher"] = 6016,
        ["Alter Dunkelküstendrescher"] = 6016,
        ["Arbeiter von Hillsbrad"] = 6016,
        ["Klingenschwanz der Rächerflossen"] = 6016,
        ["Silithideindringling"] = 6016,
        ["Spiegelpanzerbohrer"] = 6016,
        ["Moderrankenborkenreißer"] = 6016,
        ["Akkordminenarbeiter der Defias"] = 6016,
        ["Gobbler"] = 12097,
        ["Scharfkralle"] = 12097,
        ["Duriel Moonfire"] = 12097,
    },
    ["Sägebiss"] = {
        ["Kernhund"] = 19771,
    },
    ["Weisheit von Agammagan"] = {
        ["Quetschzahn"] = 7764,
    },
    ["Meisterliche Zielattrappe"] = {
        Professions = 19814,
    },
    ["Nagmaras Entschwinden"] = {
        ["Herrin Nagmara"] = 15341,
        ["Gefreiter Rocknot"] = 15341,
    },
    ["Gespinstschauer"] = {
        ["Tuten'kash"] = 12252,
    },
    ["Schießen"] = {
        ["Scharfschütze Middlecamp"] = 16768,
        ["Scharfschütze Wheeler"] = 16775,
        ["Scharfschütze Middlecamp"] = 16772,
        ["Scharfschütze Wheeler"] = 16778,
        ["Scharfschütze Middlecamp"] = 16779,
        ["Scharfschütze Middlecamp"] = 16776,
        ["Scharfschütze Middlecamp"] = 16777,
        ["Scharfschütze Middlecamp"] = 16780,
        ["Scharfschütze Wheeler"] = 16767,
        ["Scharfschütze Middlecamp"] = 16572,
        ["Scharfschütze der Dunkeleisenzwerge"] = 8995,
        ["Scharfschütze der Dunkeleisenzwerge"] = 8996,
        ["Scharfschütze der Dunkeleisenzwerge"] = 8997,
        ["Roggo Harlbarrow"] = 9008,
        ["Motley Garmason"] = 9008,
        ["Helena Atwood"] = 9008,
    },
    ["Verderbte Ausdauer"] = {
        ["Höhlenbehüter der Fäulnisklauen"] = 6819,
        ["Knurrtatze"] = 6819,
    },
    ["Smokinghose"] = {
        Professions = 12089,
    },
    ["Seuchenwolke"] = {
        ["Ardo Schmutzpranke"] = 3256,
        ["Seuchenwirker der Moderfelle"] = 3256,
    },
    ["Hallendes Gebrüll"] = {
        ["Kriegsherr der Gordunni"] = 10967,
        ["Feldmarschall Afrasiabi"] = 10967,
    },
    ["Verblasst"] = {
        ["Lump der Defias"] = 6408,
        ["Onin MacHammar"] = 6408,
    },
    ["Blutblütenschrumpfer erschaffen"] = {
        ["Larion"] = 22565,
    },
    ["Flammenknall"] = {
        ["Rufer der Blutäxte"] = 15743,
        ["Volchan"] = 15743,
    },
    ["Rote Magiestoffhose"] = {
        Professions = 12060,
    },
    ["Göttliche Gunst"] = {
        ["PALADIN"] = 20216,
    },
    ["Standardzielfernrohr"] = {
        Professions = 3978,
    },
    ["Schattenkapuze"] = {
        Professions = 3858,
    },
    ["Geistdiebstahl"] = {
        ["Nachtseele"] = 3477,
    },
    ["Teleportieren: Undercity"] = {
        ["MAGE"] = 3563,
    },
    ["Fliegerbrille"] = {
        Professions = 3934,
    },
    ["Schwarze Fäulnis"] = {
        ["Verseuchte Ratte"] = 16448,
    },
    ["Grobes Dynamit"] = {
        Professions = 3931,
        ["Ashlan Stonesmirk"] = 9002,
        ["Ashlan Stonesmirk"] = 9003,
        ["Ashlan Stonesmirk"] = 9004,
        ["Ashlan Stonesmirk"] = 9009,
    },
    ["Erheblicher Manatrank"] = {
        Professions = 17580,
    },
    ["Spurt"] = {
        ["DRUID"] = 9821,
        Pet = 23110,
    },
    ["Grüne Eisengamaschen"] = {
        Professions = 3506,
    },
    ["Kraft von Arko'narin"] = {
        ["Gefangener Arko'narin"] = 18163,
    },
    ["Schnellfeuer"] = {
        ["HUNTER"] = 3045,
    },
    ["Giftige Wolke"] = {
        ["Giftiger Brühschleimer"] = 21070,
    },
    ["Infizierte Wunde"] = {
        ["Faulreißer der Nachtheuler"] = 3427,
        ["Gath'Ilzogg"] = 3427,
        ["Bastard der Rotkammgnolle"] = 3427,
        ["Hochlandscharfzahn"] = 3427,
        ["Scheckiger Scharfzahn"] = 3427,
        ["Sarlatan"] = 3427,
        ["Inhaftierter Defias"] = 3427,
        ["Kranker Schwarzbär"] = 3427,
        ["Verkrusteter Gezeitenkriecher"] = 3427,
        ["Matschkrabbler der Fetzenflossen"] = 3427,
        ["Großer Lochkrokilisk"] = 3427,
        ["Grunzer von Hammerfall"] = 3427,
        ["Bussard"] = 3427,
        ["Riesenbussard"] = 3427,
        ["Deviatkrabbler"] = 3427,
        ["Gezücht der Bruchschnauzen"] = 3427,
        ["Aasschrecken"] = 3427,
        ["Teufelspfotenwolf"] = 3427,
        ["Teufelspfotenaasfresser"] = 3427,
        ["Teufelspfotenverheerer"] = 3427,
        ["Gezücht der Weißstoppel"] = 17230,
    },
    ["Schwacher Manatrank"] = {
        Professions = 2331,
    },
    ["Jadefeuer"] = {
        ["Jadefeuersatyr"] = 13578,
        ["Jadefeuerschurke"] = 13578,
        ["Jadefeuerschwindler"] = 13578,
        ["Jadefeuerverräter"] = 13578,
        ["Jadefeuerteufelsanbeter"] = 13578,
        ["Jadefeuerschattenpirscher"] = 13578,
        ["Jadefeuerhöllenrufer"] = 13578,
        ["Prinz Xavalis"] = 13578,
    },
    ["Dunkle Lederhose"] = {
        Professions = 7135,
    },
    ["Mithrilschuppenschultern"] = {
        Professions = 9966,
    },
    ["Wichtel beschwören"] = {
        ["WARLOCK"] = 688,
        ["Wichteldiener"] = 11939,
        ["Schattenzauberer der Witherbark"] = 11939,
        ["Kultist der Burning Blade"] = 11939,
        ["Fizzle Dunkelsturm"] = 11939,
        ["Kultist des dunklen Strangs"] = 11939,
        ["Höllenrufer der Herzschinder"] = 11939,
        ["Balizar der Anstößige"] = 11939,
        ["Beschwörer der Blackrock"] = 11939,
        ["Beschwörer der Burning Blade"] = 11939,
        ["Kayla Smithe"] = 11939,
        ["Gina Lang"] = 11939,
        ["Dane Winslow"] = 11939,
        ["Cylina Darkheart"] = 11939,
        ["Wren Darkspring"] = 11939,
        ["Morloch"] = 11939,
    },
    ["Aderlass"] = {
        ["WARLOCK"] = 11689,
    },
    ["Wächterrüstung"] = {
        Professions = 3773,
    },
    ["Schattengestalt"] = {
        ["PRIEST"] = 15473,
    },
    ["Fluch der Schwäche"] = {
        ["WARLOCK"] = 11708,
        ["Jergosh der Herbeirufer"] = 18267,
        ["Schattenzauberer der Frostmane"] = 11980,
        ["Hexendoktor der Mosh'Ogg"] = 11980,
        ["Licillin"] = 11980,
        ["Balizar der Anstößige"] = 11980,
        ["Xabraxxis"] = 11980,
        ["Darbel Montrose"] = 12741,
        ["Morloch"] = 12741,
        ["Schattenklaue"] = 17227,
        ["Eisenrücken"] = 21007,
    },
    ["Schwarze Magiestoffstiefel"] = {
        Professions = 12073,
    },
    ["Gedankenzersplitterung"] = {
        ["Doktor Weavil"] = 25774,
    },
    ["Rauer Wetzstein"] = {
        Professions = 2660,
    },
    ["Totem des Windzorns"] = {
        ["SHAMAN"] = 10614,
    },
    ["Ragnaros-Gabe"] = {
        ["Dunkeleisenzwerg"] = 7891,
        ["Saboteur der Dunkeleisenzwerge"] = 7891,
        ["Tunnelgräber der Dunkeleisenzwerge"] = 7891,
        ["Demolierer der Dunkeleisenzwerge"] = 7891,
        ["Scharfschütze der Dunkeleisenzwerge"] = 7891,
    },
    ["Glänzende Fischschuppen"] = {
        ["SHAMAN"] = 17057,
    },
    ["Krankheit aufheben"] = {
        ["PRIEST"] = 552,
    },
    ["Runenverzierte Kupferarmschienen"] = {
        Professions = 2664,
    },
    ["Reitersoldat Ping"] = {
        ["Scharlachroter Reitersoldat"] = 19749,
    },
    ["Schutzbrille mit grünen Gläsern"] = {
        Professions = 3956,
    },
    ["Schwere Mithrilstiefel"] = {
        Professions = 9968,
    },
    ["Ehrenpunkte +228"] = {
        ["Kriegshetzer der Horde"] = 24963,
        ["Brigadegeneral der Allianz"] = 24963,
    },
    ["Aura der Heiligkeit"] = {
        ["PALADIN"] = 20218,
    },
    ["Schwerer Kupferschlägel"] = {
        Professions = 7408,
    },
    ["Dichtes Sprengpulver"] = {
        Professions = 19788,
    },
    ["Eine Hand voll Kupferbolzen"] = {
        Professions = 3922,
    },
    ["Annalen von Darrowshire' verzaubern"] = {
        ["Chromie"] = 17285,
    },
    ["Elementarwetzstein"] = {
        Professions = 22757,
    },
    ["Geist der Vergangenheit beschwören"] = {
        ["Geist der Vergangenheit"] = 3652,
        ["Theurgiker von Dalaran"] = 3652,
    },
    ["Raues Schießeisen"] = {
        Professions = 3925,
    },
    ["Schallexplosion"] = {
        ["Große Kral-Fledermaus"] = 8281,
        ["Wilder Talkreischer"] = 8281,
        ["Monströse Seuchenfledermaus"] = 8281,
        ["Ressan der Aufstachler"] = 8281,
    },
    ["Kälteeinbruch"] = {
        ["MAGE"] = 12472,
    },
    ["Schwacher Heiltrank"] = {
        Professions = 2330,
    },
    ["Zweihandwaffe - Geringe Willenskraft"] = {
        Professions = 13380,
    },
    ["Raue bronzene Gamaschen"] = {
        Professions = 2668,
    },
    ["Verteidigungshaltung"] = {
        ["WARRIOR"] = 71,
        ["Garrick Padfoot"] = 7164,
        ["Gath'Ilzogg"] = 7164,
        ["Krieger der Schattenfelle"] = 7164,
        ["Zenturio der Dragonmaw"] = 7164,
        ["Tunnelgräber der Dunkeleisenzwerge"] = 7164,
        ["Buddler der Splittersteintroggs"] = 7164,
        ["Buddler der Tunnelratten"] = 7164,
        ["Scharlachroter Vorposten"] = 7164,
        ["Captain Vachon"] = 7164,
        ["Kam Deepfury"] = 7164,
        ["Pyrewood-Wachposten"] = 7164,
        ["Fußsoldat von Hillsbrad"] = 7164,
        ["Minenarbeiter von Hillsbrad"] = 7164,
        ["Küstenpirscher der Säbelflossen"] = 7164,
        ["Buddler der Trockenstoppel"] = 7164,
        ["Verteidiger von Stromgarde"] = 7164,
        ["Tunnelgräber der Schattenschmiede"] = 7164,
        ["Lieutenant Benedict"] = 7164,
        ["Ausgräber von Bael'dun"] = 7164,
        ["Marinesoldat von Theramore"] = 7164,
        ["Krieger der Salzflossen"] = 7164,
        ["Scharlachroter Gardist"] = 7164,
        ["Scharlachroter Verteidiger"] = 7164,
        ["Verteidiger von Razorfen"] = 7164,
        ["Gladiator der Blackrock"] = 7164,
        ["Marcel Dabyrie"] = 7164,
        ["Stachelwache-Champion"] = 7164,
        ["Satyr der Leidbringer"] = 7164,
        ["Murloc der Nachtaugen"] = 7164,
        ["Buddler der Schattenschmiede"] = 7164,
        ["Wache Edward"] = 7164,
        ["Wache Jarad"] = 7164,
        ["Wache Kahil"] = 7164,
        ["Wache Narrisha"] = 7164,
        ["Wache Tark"] = 7164,
        ["Khan Jehn"] = 7164,
        ["Wachkommandant Zalaphil"] = 7164,
        ["Vejrek"] = 7164,
        ["Einbuddler der Tiefentroggs"] = 7164,
        ["Soldat von Dun Garok"] = 7164,
        ["Verteidiger der Zuflucht"] = 7164,
    },
    ["Geburt"] = {
        ["Klauententakel"] = 26586,
        ["Augententakel"] = 26586,
    },
    ["Transmutieren: Untod zu Wasser"] = {
        Professions = 17563,
    },
    ["Geschmeidiger schwerer Balg"] = {
        Professions = 3818,
    },
    ["Feuerschlag"] = {
        ["MAGE"] = 10199,
    },
    ["Feuerwerk mit roten Streifen"] = {
        ["Sprengmeisterin Emi Shortfuse"] = 11542,
    },
    ["Kompakter Ernteschnitter"] = {
        ["Kompakter Ernteschnitter"] = 7979,
        ["Maschinenschmied der Venture Co."] = 7979,
        ["Ingenieur Whirleygig"] = 7979,
    },
    ["Verzauberte Schnelligkeit"] = {
        ["Verzauberer der Defias"] = 3443,
        ["Verzauberin der Grollflossen"] = 3443,
        ["Mith'rethis der Verzauberer"] = 3443,
    },
    ["Eiskühlenova"] = {
        ["Ras Frostraunen"] = 18099,
    },
    ["Bogenzerkracher"] = {
        ["Häscher der Burning Blade"] = 8374,
        ["Twilight-Häscher"] = 8374,
        ["Häscher der Tiefentroggs"] = 8374,
    },
    ["Abtrennung"] = {
        ["Grabende Donnerschnauze"] = 14533,
    },
    ["Schwerer Skorpidgürtel"] = {
        Professions = 19070,
    },
    ["Geringes Heilen"] = {
        ["PRIEST"] = 2053,
    },
    ["Heiliger Zorn"] = {
        ["PALADIN"] = 10318,
    },
    ["Rasende Wut"] = {
        ["Höhlenbehüter der Distelfelle"] = 3490,
        ["Aku'mai"] = 3490,
    },
    ["Purpurroter Seidenumhang"] = {
        Professions = 8789,
    },
    ["Eisiger Umhang"] = {
        Professions = 3862,
    },
    ["Dynamit"] = {
        ["Akkordminenarbeiter der Venture Co."] = 8800,
    },
    ["Waroshs Transformation"] = {
        ["Warosh"] = 16801,
    },
    ["Leerwandler"] = {
        ["Leerwandlerdiener"] = 5108,
        ["Beschwörer der Defias"] = 5108,
        ["Hexenmeister der Schattenanbeter"] = 5108,
    },
    ["Flammen anstacheln"] = {
        ["Feuergänger"] = 19635,
    },
    ["Zaubermachtschutzbrille Xtrem Plus"] = {
        Professions = 19794,
    },
    ["Schimärenweste"] = {
        Professions = 19081,
    },
    ["Neulieferung"] = {
        ["Lieferant der Dunkeleisenzwerge"] = 4961,
    },
    ["Sayges dunkles Schicksal der Beweglichkeit"] = {
        ["Sayge"] = 23736,
    },
    ["Frostsäblerhandschuhe"] = {
        Professions = 19087,
    },
    ["Handschuhe der Zauberbeherrschung"] = {
        Professions = 18454,
    },
    ["Armschiene - Geringe Abwehr"] = {
        Professions = 13646,
    },
    ["Imperialer Plattenhelm"] = {
        Professions = 16658,
    },
    ["Rote Magiestofftasche"] = {
        Professions = 12079,
    },
    ["Feste Skorpidschulterstücke"] = {
        Professions = 10564,
    },
    ["Eisenfederschultern"] = {
        Professions = 19062,
    },
    ["Massen-Neutralisierung"] = {
        ["Arkaner Nullifizierer X-21"] = 10832,
    },
    ["Explosion"] = {
        ["Sprengmeisterin Emi Shortfuse"] = 12158,
        ["Sprengmeisterin Emi Shortfuse"] = 12159,
    },
    ["Naraxis-Gespinst"] = {
        ["Naraxis"] = 3542,
    },
    ["Arkane Geschosse"] = {
        ["MAGE"] = 25345,
    },
    ["Geysir"] = {
        ["Wellenreiter der Hasskämme"] = 10987,
        ["Meeresschauer"] = 10987,
        ["Prinzessin Tempestria"] = 10987,
    },
    ["Tödliches Gift V"] = {
        ["ROGUE"] = 25347,
    },
    ["Wütende Seuche"] = {
        ["Verseuchter Arbeiter"] = 23072,
    },
    ["Argentumschultern"] = {
        Professions = 23665,
    },
    ["Schlammflossenfungus"] = {
        ["Matschkrabbler der Schlammflossen"] = 9462,
        ["Küstenläufer der Schlammflossen"] = 9462,
    },
    ["Schmiedekunst"] = {
        Professions = 3100,
        Professions = 3538,
        Professions = 9785,
    },
    ["Umschließende Gespinste"] = {
        ["Ebenenkrabbler"] = 4962,
        ["Riesiger Ebenenkrabbler"] = 4962,
        ["Gruftbestie"] = 4962,
    },
    ["Gekräftigte Lederhandschuhe"] = {
        Professions = 3770,
    },
    ["Gelassenheit"] = {
        ["DRUID"] = 9863,
    },
    ["Feuerballsalve"] = {
        ["Aerie-Greif"] = 15285,
        ["Kriegsreiter"] = 15285,
        ["Twilight-Feuerwache"] = 15243,
    },
    ["Plündererhandschuhe"] = {
        Professions = 9148,
    },
    ["Brust - Überragende Gesundheit"] = {
        Professions = 13858,
    },
    ["Weiße Leinenrobe"] = {
        Professions = 7624,
    },
    ["Schlag"] = {
        ["Betrunkener Schleicher"] = 13584,
        ["Zwingenkiefer"] = 13446,
        ["Coldmine-Wache"] = 15580,
        ["Schwadronskommandant Guse"] = 15580,
        ["Schwadronskommandant Jeztor"] = 15580,
        ["Schwadronskommandant Mulverick"] = 15580,
        ["Schwadronskommandant Ichman"] = 15580,
        ["Schwadronskommandant Slidore"] = 15580,
        ["Coldmine-Wachenveteran"] = 15580,
        ["Skelettminenarbeiter"] = 12057,
        ["Strashazschlangenwache"] = 12057,
        ["Lord Sündenbrecher"] = 12057,
        ["Buddler der Weißstoppel"] = 12057,
        ["Umi Thorson"] = 12057,
        ["Coldmine-Eindringling"] = 12057,
        ["Brack"] = 11976,
        ["Knochenknacker der Splittersteintroggs"] = 11976,
        ["Dextren Ward"] = 11976,
        ["Captain Melrache"] = 11976,
        ["Bauer Solliden"] = 11976,
        ["Krieger der Knarzklauen"] = 11976,
        ["Crushridge-Raufer"] = 11976,
        ["Knochenknacker der Steingrufttroggs"] = 11976,
        ["Marodeur der Kolkar"] = 11976,
        ["Myrmidone der Rächerflossen"] = 11976,
        ["Kobold der Bruchschnauzen"] = 11976,
        ["Wache Edward"] = 11976,
        ["Wache Jarad"] = 11976,
        ["Wache Kahil"] = 11976,
        ["Wache Lana"] = 11976,
        ["Wache Narrisha"] = 11976,
        ["Wache Tark"] = 11976,
        ["Offizier von Nethergarde"] = 11976,
        ["Sturmbuchtkrieger"] = 11976,
        ["Bastionskrieger"] = 11976,
        ["Platsch der Grausame"] = 11976,
        ["Kriegsherr Krom'zar"] = 11976,
        ["Eindringling der Kolkar"] = 11976,
        ["Häscher der Grimmtotem"] = 11976,
        ["Spektraler Verteidiger"] = 11976,
        ["Ragefire-Trogg"] = 11976,
        ["Stormpike-Gardist"] = 11976,
        ["Gebirgsjäger Boombellow"] = 11976,
        ["Threggil"] = 11976,
        ["Irondeep-Trogg"] = 14516,
        ["Königliche Schreckenswache"] = 14516,
    },
    ["Thules Fluch"] = {
        ["Gnoll der Moderfelle"] = 3237,
        ["Bastard der Moderfelle"] = 3237,
        ["Madenauge"] = 3237,
        ["Lichtungsläufer der Moderfelle"] = 3237,
        ["Mystiker der Moderfelle"] = 3237,
        ["Schläger der Moderfelle"] = 3237,
        ["Seuchenwirker der Moderfelle"] = 3237,
        ["Grabräuber der Moderfelle"] = 3237,
        ["Wilder der Moderfelle"] = 3237,
        ["Tobendes Moderfell"] = 3237,
    },
    ["Einfacher Kilt"] = {
        Professions = 12046,
    },
    ["Infizierter Biss"] = {
        ["Junges einer Schwarzen Witwe"] = 7367,
        ["Strashazhydra"] = 16128,
    },
    ["Reinigung des Glaubens"] = {
        ["PALADIN"] = 4987,
    },
    ["Feuerstein herstellen"] = {
        ["WARLOCK"] = 17951,
    },
    ["Arkanblitz"] = {
        ["Fellicents Schemen"] = 13901,
    },
    ["Schmerzenspeitsche"] = {
        Pet = 11780,
    },
    ["Automatischer Schuss"] = {
        ["HUNTER"] = 75,
    },
    ["Seelenbiss"] = {
        ["Seelenfresser der Vilebranch"] = 11016,
        ["Seelenfresser der Sandfury"] = 11016,
    },
    ["Trank der lebhaften Aktion"] = {
        Professions = 24367,
    },
    ["Manasturm"] = {
        ["Azuregos"] = 21097,
    },
    ["Schild - Willenskraft"] = {
        Professions = 13659,
    },
    ["Wasserstrahl"] = {
        ["Wellenreiter der Sturmschuppen"] = 13586,
        ["Lord Sündenbrecher"] = 13586,
    },
    ["Schlafwandeln"] = {
        ["Traumbrüller"] = 20668,
    },
    ["Braune Leinenrobe"] = {
        Professions = 7623,
    },
    ["Heiliger Schild"] = {
        ["PALADIN"] = 20928,
    },
    ["Blitzschlagschild"] = {
        ["SHAMAN"] = 10432,
        ["Schamane der Totenwaldfelle"] = 13585,
        ["Staubstürmer"] = 19514,
        ["Arkkoranorakel"] = 12550,
        ["Lebendiger Sturm"] = 12550,
        ["Lorgus Jett"] = 12550,
        ["Schamane der Frostwolf"] = 12550,
        ["Schamane der Bloodscalp"] = 8788,
        ["Sturmruferin der Magram"] = 8788,
        ["Sturmruferin der Maraudine"] = 8788,
    },
    ["Feiner Lederumhang"] = {
        Professions = 2159,
    },
    ["Naturschutztrank"] = {
        Professions = 7259,
    },
    ["Rakete, Blau"] = {
        ["Pats Feuerwerkstyp - BLAU"] = 26344,
    },
    ["Schildwache beschwören"] = {
        ["Schildwache von Dalaran"] = 3655,
        ["Beschwörer von Dalaran"] = 3655,
    },
    ["Zauberstein herstellen"] = {
        ["WARLOCK"] = 2362,
    },
    ["Leerwandler beschwören"] = {
        ["WARLOCK"] = 697,
    },
    ["Glitschiger Schritt"] = {
        ["Schattenzauberer der Hakkari"] = 6869,
    },
    ["Fieberhafte Erschöpfung"] = {
        ["Mesabussard"] = 8139,
        ["Alter Mesabussard"] = 8139,
        ["Räudige Silbermähne"] = 8139,
        ["Geomant der Scherwindmine"] = 8139,
        ["Oberanführer der Scherwindmine"] = 8139,
        ["Arkkoranmatschkrabbler"] = 8139,
    },
    ["Eitriger Gestank"] = {
        ["Putridius"] = 12946,
        ["Plaguemaw der Faulende"] = 12946,
        ["Kessellord Rachseele"] = 12946,
    },
    ["Rote Leinenrobe"] = {
        Professions = 2389,
    },
    ["Arkane Macht"] = {
        ["MAGE"] = 12042,
    },
    ["Bodenlose Tasche"] = {
        Professions = 18455,
    },
    ["Unendlicher Atem"] = {
        ["WARLOCK"] = 5697,
    },
    ["Spawn Blauen Drakoniden"] = {
        ["Blauer Drakonid"] = 22658,
        ["Blue Drakonid Spawner"] = 22658,
    },
    ["Umhang - Widerstand"] = {
        Professions = 13794,
    },
    ["Irdener Seidengürtel"] = {
        Professions = 8797,
    },
    ["Aufwallung der Seelen"] = {
        ["Lokholar der Eislord"] = 21307,
    },
    ["Wilde Attacke"] = {
        ["DRUID"] = 16979,
    },
    ["Starten"] = {
        ["Bom'bay"] = 16716,
    },
    ["Pionierexplosion"] = {
        ["Saboteur der Dunkeleisenzwerge"] = 3204,
        ["Pionier der Dunkeleisenzwerge"] = 3204,
    },
    ["Großes rotes Raketenbündel"] = {
        Professions = 26428,
    },
    ["Oranges Magiestoffhemd"] = {
        Professions = 12061,
    },
    ["Bansheekreischen"] = {
        ["Geist der Eldreth"] = 16838,
    },
    ["Schreckliches Geheul"] = {
        ["Nefaru"] = 8715,
        ["Steinschwingenkreischer"] = 8715,
        ["Nebelheuler"] = 8715,
    },
    ["Runenstoffstirnband"] = {
        Professions = 18444,
    },
    ["Armschiene - Intelligenz"] = {
        Professions = 13822,
    },
    ["Netzwirbel"] = {
        ["Razzashibrutwitwe"] = 24600,
    },
    ["Stille"] = {
        ["PRIEST"] = 15487,
        ["Arkanist Doan"] = 8988,
        ["Schattenjäger der Witherbark"] = 6726,
    },
    ["Großer Arkanschutztrank"] = {
        Professions = 17577,
    },
    ["Feuerkegel"] = {
        ["Flammenwächter"] = 19630,
    },
    ["Elixier der Weisheit"] = {
        Professions = 3171,
    },
    ["Verschnörkelte Mithrilbrustplatte"] = {
        Professions = 9972,
    },
    ["Verderbnis der Erde"] = {
        ["Emeriss"] = 24910,
    },
    ["Golddietrich"] = {
        Professions = 19667,
    },
    ["Bisstrauma"] = {
        ["Artorius der Liebenswürdige"] = 23299,
    },
    ["Spitzenspinnling beschwören"] = {
        ["Spitzenspinnling"] = 16103,
        ["Spitzenspinne"] = 16103,
    },
    ["Widerlicher Gestank"] = {
        ["Huhn"] = 24919,
        ["Wache von Southshore"] = 24919,
        ["Bauer Kent"] = 24919,
        ["Phin Odelic"] = 24919,
        ["Sergeant Hartman"] = 24919,
    },
    ["Tier freigeben"] = {
        ["HUNTER"] = 2641,
    },
    ["Armschiene - Geringe Intelligenz"] = {
        Professions = 13622,
    },
    ["Krankheit heilen"] = {
        ["PRIEST"] = 528,
        ["SHAMAN"] = 2870,
    },
    ["Elixier der Löwenstärke"] = {
        Professions = 2329,
    },
    ["Sprinten"] = {
        ["ROGUE"] = 11305,
    },
    ["Beule"] = {
        ["Haudrauf der Burning Blade"] = 4134,
    },
    ["Giftiger Katalysator"] = {
        ["Verderbter Skorpid"] = 5413,
    },
    ["Fluch des Auges"] = {
        ["Verfluchter Seemann"] = 10651,
        ["Verfluchter Marinesoldat"] = 10651,
        ["Erster Maat Snellig"] = 10651,
        ["Kapitän Halyndor"] = 10651,
        ["Verfluchter Seemann"] = 10653,
        ["Verfluchter Marinesoldat"] = 10653,
        ["Erster Maat Snellig"] = 10653,
        ["Kapitän Halyndor"] = 10653,
        ["Verfluchter Seemann"] = 3360,
        ["Verfluchter Marinesoldat"] = 3360,
        ["Erster Maat Snellig"] = 3360,
        ["Kapitän Halyndor"] = 3360,
    },
    ["Felshaut"] = {
        ["Seherin Ravenfeather"] = 8314,
        ["Kranal Fiss"] = 8314,
    },
    ["Illusionären Nachtmahr beschwören"] = {
        ["Illusionärer Nachtmahr"] = 6905,
        ["Abtrünniger Träumer"] = 6905,
    },
    ["Wilde Wut"] = {
        ["Wilder der Moderfelle"] = 3258,
        ["Stanley"] = 3258,
        ["Shadowfang-Wutzahn"] = 7072,
    },
    ["Felstrom-Auferstehung"] = {
        ["Kommandant Felstrom"] = 3488,
        ["Kommandant Felstrom"] = 3488,
    },
    ["Trelanes Eiskälteberührung"] = {
        ["Kor'gresh Frostzorn"] = 4320,
    },
    ["Treantverbündete beschwören"] = {
        ["Treantverbündeter"] = 20702,
        ["Erzdruide Fandral Staghelm"] = 20702,
    },
    ["Schnapptritt"] = {
        ["Bandit der Defias"] = 8646,
        ["Dockarbeiter der Defias"] = 8646,
        ["Kundschafter von Ashenvale"] = 8646,
    },
    ["Adrenalinrausch"] = {
        ["ROGUE"] = 13750,
    },
    ["Hexerzwirngamaschen"] = {
        Professions = 18421,
    },
    ["Feiner Ledergürtel"] = {
        Professions = 3763,
    },
    ["Grünes Leinenhemd"] = {
        Professions = 2396,
    },
    ["Widerstand-Aura"] = {
        ["Purpurroter Kurier"] = 19726,
    },
    ["Versilberte Schrotflinte"] = {
        Professions = 3949,
    },
    ["Runenverzierte Silberrute"] = {
        Professions = 7795,
    },
    ["Systemschock"] = {
        ["Abgesandter Roman'khan"] = 23774,
    },
    ["Weißes Hochzeitskleid"] = {
        Professions = 12091,
    },
    ["Weeglis Fass erstellen."] = {
        ["Weegli Lunte"] = 10772,
    },
    ["Gesundheitsstein herstellen"] = {
        ["WARLOCK"] = 5699,
    },
    ["Wiederbelebung"] = {
        ["Angeketteter Geist"] = 24341,
    },
    ["Sturmstulpen"] = {
        Professions = 16661,
    },
    ["Sofort wirkendes Gift V"] = {
        ["ROGUE"] = 11342,
    },
    ["Todeswunsch"] = {
        ["WARRIOR"] = 12328,
    },
    ["Magierbluttrank"] = {
        Professions = 24365,
    },
    ["Tunnelgräbersäure"] = {
        ["Tunnelgräber der Gorishi"] = 14120,
        ["Schwarmwache der Gorishi"] = 14120,
        ["Felsbohrer"] = 14120,
    },
    ["Seelenfeuer"] = {
        ["WARLOCK"] = 17924,
    },
    ["Skorpidstich"] = {
        ["HUNTER"] = 14277,
        ["Kundschafter von Ashenvale"] = 18545,
    },
    ["Abschreckung"] = {
        ["HUNTER"] = 19263,
    },
    ["Eisiger Griff"] = {
        ["Eisbart der Alte"] = 3145,
    },
    ["Schattenwiderstand"] = {
        ["HUNTER"] = 24516,
    },
    ["Gyrofrosteisreflektor"] = {
        Professions = 23077,
    },
    ["Furcht"] = {
        ["WARLOCK"] = 6215,
    },
    ["Weltvergrößerer"] = {
        Professions = 23129,
    },
    ["Sturmschleierrüstung"] = {
        Professions = 19079,
    },
    ["Hellgelbes Hemd"] = {
        Professions = 3869,
    },
    ["Rakete, Blau Groß"] = {
        ["Pats Feuerwerkstyp - BLAU GROSS"] = 26351,
    },
    ["Öl des Feuerbrandes"] = {
        Professions = 11451,
    },
    ["Lichtblitz"] = {
        ["PALADIN"] = 19943,
    },
    ["Niedriger Prankenhieb"] = {
        ["Zzarc'Vul"] = 8716,
        ["Bruder Ravenoak"] = 8716,
        ["Wutklauengrizzly"] = 8716,
    },
    ["Dickes Leder"] = {
        Professions = 20650,
    },
    ["Goblin-Bergbauhelm"] = {
        Professions = 12717,
    },
    ["Silber verhütten"] = {
        Professions = 2658,
    },
    ["Arkaner Schuss"] = {
        ["HUNTER"] = 14287,
    },
    ["Schwaches Manaöl"] = {
        Professions = 25125,
    },
    ["Frostöl"] = {
        Professions = 3454,
    },
    ["Scharlachroten Reitersoldaten beschwören"] = {
        ["Scharlachroter Reitersoldat"] = 19722,
        ["Scharlachroter Reitersoldat"] = 19722,
    },
    ["Besudelungsgeheul"] = {
        ["Besudelter Nachtheuler"] = 3424,
        ["Gutspill"] = 3424,
    },
    ["Peinigender Schmerz"] = {
        ["Humar der Stolze Lord"] = 3247,
    },
    ["Gedankensicht"] = {
        ["PRIEST"] = 10909,
    },
    ["Erheblicher Trollbluttrank"] = {
        Professions = 24368,
    },
    ["Elixier der schwachen Verteidigung"] = {
        Professions = 7183,
    },
    ["Ebergeist beschwören"] = {
        ["Ebergeist"] = 8286,
        ["Aggem Dornfluch"] = 8286,
    },
    ["Kampfhaltung"] = {
        ["WARRIOR"] = 2457,
        ["Skelettkrieger"] = 7165,
        ["Alpha der Rotkammgnolle"] = 7165,
        ["Krieger der Blaukiemen"] = 7165,
        ["Vollstrecker der Mo'grosh"] = 7165,
        ["Meisterbuddler"] = 7165,
        ["Dextren Ward"] = 7165,
        ["Verurteilter Defias"] = 7165,
        ["Forscher der Naga"] = 7165,
        ["Krieger der Schwarzfelle"] = 7165,
        ["Twilight-Rohling"] = 7165,
        ["Verteidiger der Grimmhauer"] = 7165,
        ["Lok Orcbane"] = 7165,
        ["Pfützenspringer der Salzflossen"] = 7165,
        ["Ursa der Fäulnisklauen"] = 7165,
        ["Aligar der Peiniger"] = 7165,
        ["Ursa der Distelfelle"] = 7165,
        ["Ruuzel"] = 7165,
        ["Oberanführer Rammhauer"] = 7165,
        ["Stachelwache von Razorfen"] = 7165,
        ["Kampflord der Kolkar"] = 7165,
        ["Krieger der Zackenkämme"] = 7165,
        ["Wache Edward"] = 7165,
        ["Wache Jarad"] = 7165,
        ["Wache Kahil"] = 7165,
        ["Wache Lana"] = 7165,
        ["Wache Narrisha"] = 7165,
        ["Wache Tark"] = 7165,
        ["Khan Dez'hepah"] = 7165,
        ["Khan Shaka"] = 7165,
        ["Captain Stumpfhauer"] = 7165,
        ["Großknecht Grills"] = 7165,
        ["Takk der Springer"] = 7165,
        ["Hagg Taurenbane"] = 7165,
        ["Swinegart-Speerbalg"] = 7165,
        ["Räuber der Defias"] = 7165,
        ["Verwirrter Aussätziger"] = 7165,
        ["Gelihast"] = 7165,
        ["Gnomeregan-Evakuierter"] = 7165,
    },
    ["Kupferne Kettenhose"] = {
        Professions = 2662,
    },
    ["Fackelwurf"] = {
        ["Sturmseher der Kolkar"] = 14292,
        ["Eindringling der Kolkar"] = 14292,
        ["Großknecht von Bael'dun"] = 6257,
    },
    ["Mondstoffschultern"] = {
        Professions = 18448,
    },
    ["Rote Magiestoffweste"] = {
        Professions = 12056,
    },
    ["Kristallener Schlummer"] = {
        ["Kaltaugenbasilisk"] = 3636,
        ["Versengter Basilisk"] = 3636,
        ["Angesengter Basilisk"] = 3636,
        ["Geschwärzter Basilisk"] = 3636,
        ["Salzsteinbasilisk"] = 3636,
        ["Knirschkieferbasilisk"] = 3636,
        ["Titanischer Knirschkieferbasilisk"] = 3636,
        ["Jaderückenbasilisk"] = 3636,
    },
    ["Rückzug"] = {
        ["HUNTER"] = 14273,
    },
    ["Seelenverbindung"] = {
        ["WARLOCK"] = 19028,
    },
    ["Mondstahlbreitschwert"] = {
        Professions = 3496,
    },
    ["Blutsturz"] = {
        ["ROGUE"] = 17348,
    },
    ["Bärengestalt"] = {
        ["DRUID"] = 5487,
        ["Kerlonian Evershade"] = 18309,
        ["Cenarischer Beschützer"] = 7090,
        ["Uthil Mooncall"] = 7090,
        ["Cenarischer Verwalter"] = 7090,
        ["Zornklaue"] = 7090,
        ["Naturalist der Grimmtotem"] = 19030,
    },
    ["Gabe von Xavian"] = {
        ["Schurke von Xavian"] = 6925,
        ["Verräter von Xavian"] = 6925,
        ["Teufelsanbeter von Xavian"] = 6925,
        ["Höllenrufer von Xavian"] = 6925,
        ["Prinz Schleifer"] = 6925,
    },
    ["Blaues Feuerwerk"] = {
        Professions = 23067,
    },
    ["Waffe - Geringer Wildtiertöter"] = {
        Professions = 13653,
    },
    ["Smite-Schmettern"] = {
        ["Mr. Smite"] = 6435,
    },
    ["Steinschuppenöl"] = {
        Professions = 17551,
    },
    ["Elixier der Wasseratmung"] = {
        Professions = 7179,
    },
    ["Flimmerkernrobe"] = {
        Professions = 23666,
    },
    ["Roben von Arcana"] = {
        Professions = 6692,
    },
    ["Wildlederstiefel"] = {
        Professions = 10566,
    },
    ["Goblin-Bombenspender"] = {
        Professions = 12755,
    },
    ["Auferstehung"] = {
        ["PRIEST"] = 20770,
    },
    ["Bunter Kilt"] = {
        Professions = 12047,
    },
    ["Gyrochronatom"] = {
        Professions = 3961,
    },
    ["Überragender Heilungszauberschutz"] = {
        ["Überragender Heilungszauberschutz"] = 15869,
        ["Hexendoktor der Gluthauer"] = 15869,
        ["Sprecher der Witherbark"] = 15869,
    },
    ["Sturmangriff"] = {
        ["WARRIOR"] = 11578,
        Pet = 27685,
    },
    ["Verzauberte Runenstofftasche"] = {
        Professions = 27659,
    },
    ["Gebet des Schattenschutzes"] = {
        ["PRIEST"] = 27683,
    },
    ["Runenverzierte stygische Stiefel"] = {
        Professions = 24903,
    },
    ["Stirnband des Nachtschleichers"] = {
        Professions = 10507,
    },
    ["Fläschchen mit destillierter Weisheit"] = {
        Professions = 17636,
    },
    ["Spukgeister"] = {
        ["Verhexter Bediensteter"] = 7057,
    },
    ["Flächenbrand"] = {
        ["Brennender Zerstörer"] = 8000,
    },
    ["Kampfgetümmel-Mitstreiter senden"] = {
        ["Twiggy Flathead"] = 8645,
    },
    ["Hukkus Wächter"] = {
        ["Hukkus Wichtel"] = 12790,
        ["Hukku"] = 12790,
    },
    ["Verkrüppeln"] = {
        ["Klippenbrecher"] = 11443,
        ["Nekromant der Totenköpfe"] = 11443,
    },
    ["Geist Spawn-in"] = {
        ["Redpath der Verderbte"] = 17321,
        ["Geist von Darrowshire"] = 17321,
        ["Geist von Mondklaue"] = 17321,
        ["Schreckensrossgeist"] = 17321,
        ["Schattensichels gefallenes Streitross"] = 17321,
        ["Diener der Hand"] = 17321,
    },
    ["Eisengranate"] = {
        Professions = 3962,
    },
    ["Brennenden Diener beschwören"] = {
        ["Brennender Diener"] = 10870,
    },
    ["Thoriumgranate"] = {
        Professions = 19790,
    },
    ["Schwere Skorpidarmschienen"] = {
        Professions = 19048,
    },
    ["Aura des Volltreffers"] = {
        ["HUNTER"] = 19506,
    },
    ["Handgenähter Ledergürtel"] = {
        Professions = 3753,
    },
    ["Raues Sprengpulver"] = {
        Professions = 3918,
    },
    ["Heilen"] = {
        ["PRIEST"] = 6064,
        ["Sirene der Sturmschuppen"] = 11642,
        ["Priesterin der Rächerflossen"] = 11642,
        ["Sturmbuchtorakel"] = 11642,
        ["Makrinnischarrer"] = 11642,
        ["Irondeep-Feldmesser"] = 15586,
        ["Irondeep-Feldmesserveteran"] = 15586,
        ["Irondeep-Feldmesserchampion"] = 15586,
        ["Hochinquisitor Fairbanks"] = 12039,
    },
    ["Spott"] = {
        ["WARRIOR"] = 355,
    },
    ["Verkrüppelndes Gift"] = {
        ["ROGUE"] = 3420,
    },
    ["Rußschicht"] = {
        ["Geschwärztes Urtum"] = 7998,
    },
    ["Massives Beben"] = {
        ["Geschmolzener Zerstörer"] = 19129,
        ["Baron Kazum"] = 19129,
    },
    ["Hellstoffrobe"] = {
        Professions = 18414,
    },
    ["Hellstoffhose"] = {
        Professions = 18439,
    },
    ["Gletschergebrüll"] = {
        ["Vagash"] = 3143,
    },
    ["Mirkfallonfungus"] = {
        ["Mirkfallonbewahrer"] = 8138,
    },
    ["Rückkopplung"] = {
        ["PRIEST"] = 19275,
    },
    ["Zinn verhütten"] = {
        Professions = 3304,
    },
    ["Flammen des schwarzen Drachenschwarms"] = {
        ["Aschenschwinge"] = 16054,
    },
    ["Spöttischer Schlag"] = {
        ["WARRIOR"] = 20560,
    },
    ["Totem des Elementarschutzes"] = {
        ["Totem des Elementarschutzes"] = 8262,
        ["Weiser der Totenköpfe"] = 8262,
        ["Totemiker der Holzschlundfeste"] = 8262,
        ["Hexendoktor der Gluthauer"] = 8262,
    },
    ["Instabile Substanz"] = {
        ["Blutsucherreiter"] = 24024,
    },
    ["Schwarzmaulöl"] = {
        Professions = 7836,
    },
    ["Rüstung zerreißen"] = {
        ["WARRIOR"] = 11597,
        ["Makrinniklingenklaue"] = 13444,
        ["Kaltschnappzange"] = 13444,
        ["Stachelwache-Champion"] = 15572,
        ["Buddler der Weißstoppel"] = 15572,
        ["Schwadronskommandant Guse"] = 15572,
        ["Schwadronskommandant Jeztor"] = 15572,
        ["Schwadronskommandant Mulverick"] = 15572,
        ["Coldmine-Peon"] = 15572,
        ["Coldmine-Minenarbeiter"] = 15572,
        ["Irondeep-Minenarbeiter"] = 15572,
        ["Irondeep-Peon"] = 15572,
        ["Offizier Jaxon"] = 15572,
        ["Murlocnetzer"] = 11971,
        ["Minenarbeiter der Flusspfoten"] = 11971,
        ["Schläger der Moderfelle"] = 11971,
        ["Wache von Ironforge"] = 11971,
        ["Gurgelbacke"] = 11971,
        ["Roder der Horde"] = 11971,
    },
    ["Murlocschuppengürtel"] = {
        Professions = 6702,
    },
    ["Barbarische Schultern"] = {
        Professions = 7151,
    },
    ["Teleportieren: Orgrimmar"] = {
        ["MAGE"] = 3567,
    },
    ["Mithril verhütten"] = {
        Professions = 10097,
    },
    ["Portal: Stormwind"] = {
        ["MAGE"] = 10059,
    },
    ["Feste Skorpidbrustplatte"] = {
        Professions = 10525,
    },
    ["Fluch der Erschöpfung"] = {
        ["WARLOCK"] = 18223,
    },
    ["Seelenstein herstellen (gering)"] = {
        ["WARLOCK"] = 20752,
    },
    ["Kleine Zephyriumladung"] = {
        Professions = 3933,
    },
    ["Große grüne Rakete"] = {
        Professions = 26421,
    },
    ["Dunkeleisenzerreißer"] = {
        Professions = 15294,
    },
    ["Schutzbrille des Meisteringenieurs"] = {
        Professions = 19825,
    },
    ["Schwacher Trollbluttrank"] = {
        Professions = 3170,
    },
    ["Skelett beschwören"] = {
        ["Skelett"] = 20464,
        ["Fürstin Sylvanas Windrunner"] = 20464,
        ["Skelett"] = 8853,
        ["Knochenwärter der Dragonmaw"] = 8853,
    },
    ["Wildrankentrank"] = {
        Professions = 11458,
    },
    ["Leichtes Leder"] = {
        Professions = 2881,
    },
    ["Gesundheitsstein herstellen (groß)"] = {
        ["WARLOCK"] = 11729,
    },
    ["Gesundheitsstein herstellen (gering)"] = {
        ["WARLOCK"] = 6202,
    },
    ["Grober Wetzstein"] = {
        Professions = 2665,
    },
    ["Verschnörkelte Mithrilhose"] = {
        Professions = 9945,
    },
    ["Mächtiger Trollbluttrank"] = {
        Professions = 3451,
    },
    ["Süßes oder Saures"] = {
        ["Gastwirt Farley"] = 24751,
        ["Gastwirt Belm"] = 24751,
        ["Gastwirt Helbrek"] = 24751,
        ["Gastwirt Anderson"] = 24751,
        ["Gastwirt Shay"] = 24751,
        ["Gastwirt Boorand Plainswind"] = 24751,
        ["Gastwirt Firebrew"] = 24751,
        ["Gastwirtin Renee"] = 24751,
        ["Gastwirt Thulbek"] = 24751,
        ["Gastwirtin Janene"] = 24751,
        ["Gastwirtin Brianna"] = 24751,
        ["Gastwirt Hearthstove"] = 24751,
        ["Gastwirtin Saelienne"] = 24751,
        ["Gastwirt Keldamyr"] = 24751,
        ["Gastwirtin Shaussiy"] = 24751,
        ["Gastwirtin Kimlya"] = 24751,
        ["Gastwirt Bates"] = 24751,
        ["Gastwirtin Allison"] = 24751,
        ["Gastwirt Norman"] = 24751,
        ["Gastwirtin Pala"] = 24751,
        ["Gastwirt Kauth"] = 24751,
        ["Gastwirtin Trelayne"] = 24751,
        ["Gastwirt Wiley"] = 24751,
        ["Gastwirt Skindle"] = 24751,
        ["Gastwirt Grosk"] = 24751,
        ["Gastwirtin Gryshka"] = 24751,
        ["Gastwirt Karakul"] = 24751,
        ["Gastwirt Byula"] = 24751,
        ["Gastwirt Jayka"] = 24751,
        ["Gastwirt Fizzgrimble"] = 24751,
        ["Gastwirtin Shyria"] = 24751,
        ["Gastwirtin Greul"] = 24751,
        ["Gastwirt Thulfram"] = 24751,
        ["Gastwirtin Heather"] = 24751,
        ["Gastwirt Shul'kar"] = 24751,
        ["Gastwirt Adegwa"] = 24751,
        ["Gastwirtin Lyshaerya"] = 24751,
        ["Gastwirtin Sikewa"] = 24751,
        ["Gastwirt Abeqwa"] = 24751,
        ["Gastwirt Vizzie"] = 24751,
        ["Gastwirtin Kaylisk"] = 24751,
        ["Lard"] = 24751,
        ["Calandrath"] = 24751,
        ["Gastwirtin Faralia"] = 24751,
    },
    ["Berührung von Rabenklaue"] = {
        ["Hand von Rabenklaue"] = 3263,
    },
    ["Lebendige Flamme beschwören"] = {
        ["Lebendige Flamme"] = 5110,
        ["Magicus der Defias"] = 5110,
    },
    ["Schattenzauberer beschwören"] = {
        ["Skelettschattenzauberer"] = 12258,
        ["Skelettbeschwörer"] = 12258,
    },
    ["Funkelndes Mithrilrapier"] = {
        Professions = 10005,
    },
    ["Eier ausbrüten"] = {
        ["Hohepriesterin Mar'li"] = 24083,
    },
    ["Berauschendes Gift"] = {
        ["Razzashigiftbrut"] = 24596,
    },
    ["Lebensechte mechanische Kröte"] = {
        Professions = 19793,
    },
    ["Berührung des Todes"] = {
        ["Morbent Fel"] = 3108,
    },
    ["Heiliges Feuer"] = {
        ["PRIEST"] = 15261,
    },
    ["Raptorbalgharnisch"] = {
        Professions = 4096,
    },
    ["Netheredelstein"] = {
        ["Bethor Iceshard"] = 7673,
    },
    ["Rauer Gewichtsstein"] = {
        Professions = 3115,
    },
    ["Gewaltige Thoriumstreitaxt"] = {
        Professions = 16971,
    },
    ["Winterzwirnhandschuhe"] = {
        Professions = 18411,
    },
    ["Besänftigender Kuss"] = {
        Pet = 11785,
    },
    ["Schwarzer Schlamm"] = {
        ["Schlammbestie"] = 7279,
        ["Teerlauerer"] = 7279,
        ["Der Reak"] = 7279,
        ["Faulender Schlamm"] = 7279,
    },
    ["Waffe des Felsbeißers"] = {
        ["SHAMAN"] = 16316,
    },
    ["Zerbrochenen Kadaver beschwören"] = {
        ["Zerbrochener Kadaver"] = 16324,
        ["Verheerter Kadaver"] = 16324,
    },
    ["Flüssiges Metall"] = {
        ["Gilnid"] = 5213,
        ["Smoldar"] = 5213,
    },
    ["Wellenbrecher"] = {
        ["Gazban"] = 5403,
    },
    ["Schwarze Magiestoffrobe"] = {
        Professions = 12050,
    },
    ["Großer schwarzer Streitkolben"] = {
        Professions = 10001,
    },
    ["Aspekt des Falken"] = {
        ["HUNTER"] = 25296,
    },
    ["Hammer des Zorns"] = {
        ["PALADIN"] = 24239,
    },
    ["Frostschutztrank"] = {
        Professions = 7258,
    },
    ["Sternengürtel"] = {
        Professions = 3864,
    },
    ["Bronzenes Kurzschwert"] = {
        Professions = 2742,
    },
    ["Massiver Geysir"] = {
        ["Massiver Geysir"] = 22421,
    },
    ["Aspekt des Wildtiers"] = {
        ["HUNTER"] = 13161,
    },
    ["Aufmotzen"] = {
        ["Aussätziger Techniker"] = 10348,
        ["Aussätziger Maschinenschmied"] = 10348,
    },
    ["Fluch der Feuerbrand"] = {
        ["Dunkelwirker der Feuerbrand"] = 16071,
        ["Schreckenswirker der Feuerbrand"] = 16071,
    },
    ["Klagender Toter"] = {
        ["Verirrte Seele"] = 7713,
        ["Wandernder Geist"] = 7713,
        ["Gepeinigter Geist"] = 7713,
        ["Klagende Vorfahrin"] = 7713,
        ["Klagender Tod"] = 7713,
        ["Untoter Postbote"] = 7713,
        ["Klagendes Schreckgespenst"] = 7713,
        ["Schreckenstalgeist"] = 7713,
    },
    ["Feuerschild"] = {
        Pet = 11771,
        ["Renegatenmagier der Defias"] = 134,
        ["Geomant der Tunnelratten"] = 134,
        ["Behüter von Dalaran"] = 134,
        ["Magier von Dalaran"] = 134,
        ["Schwache Feuermanifestation"] = 134,
        ["Geomant der Weißstoppel"] = 18968,
    },
    ["Gekräftigte Lederrüstung"] = {
        Professions = 2166,
    },
    ["Schwere Skorpidweste"] = {
        Professions = 19051,
    },
    ["Giftspucke"] = {
        ["Eiswindjunges"] = 16552,
        ["Mythosschuppenhydra"] = 6917,
        ["Wilddorngiftspucker"] = 6917,
    },
    ["Teufelsstoffkapuze"] = {
        Professions = 18442,
    },
    ["Silberkontakt"] = {
        Professions = 3973,
    },
    ["Dornenfluch"] = {
        ["Schattenmagier des Syndikats"] = 6909,
        ["Wahnsinniges Urtum"] = 6909,
        ["Welkes Urtum"] = 6909,
        ["Rachsüchtiges Urtum"] = 6909,
        ["Augur der Burning Blade"] = 6909,
    },
    ["Augenstrahl"] = {
        ["Auge von C'Thun"] = 26134,
    },
    ["Aquadynamischer Fischanlocker"] = {
        Professions = 9271,
    },
    ["Siegel des Befehls"] = {
        ["PALADIN"] = 20920,
    },
    ["Eisen verhütten"] = {
        Professions = 3307,
    },
    ["Heimtückischer Biss"] = {
        ["Uralter Kernhund"] = 19319,
        ["Grimmtatze"] = 19319,
    },
    ["Bronzene Streitaxt"] = {
        Professions = 9987,
    },
    ["Verzauberkunst"] = {
        Professions = 7412,
        Professions = 7413,
        Professions = 13920,
    },
    ["EZ-Thro-Dynamit"] = {
        Professions = 8339,
    },
    ["Augen des Wildtiers"] = {
        ["HUNTER"] = 1002,
    },
    ["Edelsteinbesetzte Kupferstulpen"] = {
        Professions = 3325,
    },
    ["Raues Dynamit"] = {
        Professions = 3919,
    },
    ["Mechanischer Mithrildrachling"] = {
        Professions = 12624,
    },
    ["Waffe - Eisiger Hauch"] = {
        Professions = 20029,
    },
    ["Gedanken beherrschen"] = {
        ["Singer"] = 14515,
        ["Todessprecher Jargba"] = 14515,
        ["Initiand von Hederine"] = 15859,
        ["Strashazsirene"] = 7645,
        ["Twilight-Schattenmagier"] = 7645,
    },
    ["Verführung"] = {
        Pet = 6358,
    },
    ["Raue bronzene Stiefel"] = {
        Professions = 7817,
    },
    ["Tückisches Lederstirnband"] = {
        Professions = 19071,
    },
    ["Tiefentaucherhelm"] = {
        Professions = 12617,
    },
    ["Schattenschild"] = {
        ["PRIEST"] = 19312,
        ["Krethis Shadowspinner"] = 12040,
    },
    ["Großer Voodooumhang"] = {
        Professions = 10562,
    },
    ["Segen der Freiheit"] = {
        ["PALADIN"] = 1044,
    },
    ["Glitzernder Stahldolch"] = {
        Professions = 15972,
    },
    ["Der Segen von Nordrassil"] = {
        ["Eris Havenfire"] = 23108,
    },
    ["Modisches blaues Hemd"] = {
        Professions = 7892,
    },
    ["Schild - Große Willenskraft"] = {
        Professions = 13905,
    },
    ["Gefertigtes schweres Geschoss"] = {
        Professions = 3930,
    },
    ["Leinenumhang"] = {
        Professions = 2387,
    },
    ["Kampf-Furor"] = {
        ["Tharil'zun"] = 3631,
        ["Zuchtmeister der Splitterfäuste"] = 3631,
        ["Arbeitsleiter der Venture Co."] = 3631,
        ["Oberanführer der Scherwindmine"] = 3631,
        ["Malgin Gerstenbräu"] = 3631,
        ["Henker der Twilight-Hammer"] = 3631,
    },
    ["Adlerauge"] = {
        ["HUNTER"] = 6197,
    },
    ["Reinheit"] = {
        ["Charlga Razorflank"] = 8361,
    },
    ["Magiestoffballen"] = {
        Professions = 3865,
    },
    ["Berserkerhaltung"] = {
        ["WARRIOR"] = 2458,
        ["Znort"] = 7366,
        ["Marodeur der Magram"] = 7366,
        ["Marodeur der Gelkis"] = 7366,
        ["Häscher der Tiefentroggs"] = 7366,
        ["Gestürzter Held"] = 7366,
    },
    ["Schock"] = {
        ["Twilight-Dunkelschamane"] = 15500,
        ["Thrall"] = 16034,
        ["Donnernder Verbannter"] = 11824,
        ["Zerstörer der Kolkar"] = 11824,
        ["Arkkoranorakel"] = 11824,
        ["Wirbelsturmgänger"] = 11824,
        ["Donnerkopfpatriarch"] = 12553,
        ["Donnerkopfkonsort"] = 12553,
        ["Orakel der Finsterflossen"] = 2606,
        ["Grünpfote"] = 2606,
        ["Schamane der Schwarzfelle"] = 2606,
        ["Schwaches Orakel der Finsterflossen"] = 2607,
        ["Nezzliok der Düstere"] = 2610,
        ["Orakel der Salzflossen"] = 2608,
        ["Wegelagerin der Blutfurien"] = 2608,
        ["Twilight-Elementarist"] = 2609,
    },
    ["Schildkrötenschuppenbrustplatte"] = {
        Professions = 10511,
    },
    ["Glut beschwören"] = {
        ["Glut"] = 10869,
        ["Brennender Diener"] = 10869,
    },
    ["Wahnsinniger Hunger"] = {
        ["Verhungernder Winterwolf"] = 3151,
    },
    ["Arkane Brillanz"] = {
        ["MAGE"] = 23028,
    },
    ["Gebet der Willenskraft"] = {
        ["PRIEST"] = 27681,
    },
    ["Geringen Fluch aufheben"] = {
        ["MAGE"] = 475,
    },
    ["Grüne Drachenschuppenbrustplatte"] = {
        Professions = 19050,
    },
    ["Deviatschuppengürtel"] = {
        Professions = 7955,
    },
    ["Schreckensross herbeirufen"] = {
        ["WARLOCK"] = 23161,
    },
    ["Brut von Bael'Gar beschwören"] = {
        ["Brut von Bael'Gar"] = 13895,
    },
    ["Transmutieren: Erde zu Leben"] = {
        Professions = 17566,
    },
    ["Lähmendes Gift"] = {
        ["Chatter"] = 3609,
        ["Aaskrabbler"] = 3609,
        ["Schwindler der Wildhufe"] = 3609,
        ["Blutdiener von Kirtonos"] = 3609,
        ["Totenstachel"] = 3609,
    },
    ["Totem der Steinklaue"] = {
        ["SHAMAN"] = 10428,
    },
    ["Stoß"] = {
        ["Somnus"] = 18368,
    },
    ["Brust - Mana"] = {
        Professions = 13607,
    },
    ["Grüne Lederrüstung"] = {
        Professions = 3772,
    },
    ["Säureatem"] = {
        ["Jade"] = 12533,
        ["Lord Captain Wyrmak"] = 12533,
    },
    ["Armschiene - Ausdauer"] = {
        Professions = 13648,
    },
    ["Sternensplitter"] = {
        ["PRIEST"] = 19305,
    },
    ["Bronzene Stulpen mit Versilberung"] = {
        Professions = 3333,
    },
    ["Irdene Lederschultern"] = {
        Professions = 9147,
    },
    ["Unverwüstliche Lederhose"] = {
        Professions = 9064,
    },
    ["Siegel der Weisheit"] = {
        ["PALADIN"] = 20357,
    },
    ["Armschiene - Große Intelligenz"] = {
        Professions = 20008,
    },
    ["Bronzene Gamaschen mit Versilberung"] = {
        Professions = 12259,
    },
    ["Thorium verhütten"] = {
        Professions = 16153,
    },
    ["Goldene Schuppenhelmkappe"] = {
        Professions = 3503,
    },
    ["Seelenstein herstellen (schwach)"] = {
        ["WARLOCK"] = 693,
    },
    ["Eitriges Enzym"] = {
        ["Bohrkäfer"] = 14539,
    },
    ["Grüne Wollweste"] = {
        Professions = 2399,
    },
    ["Abfangen"] = {
        ["WARRIOR"] = 20617,
    },
    ["Goblin-Überbrückungskabel"] = {
        Professions = 9273,
    },
    ["Phantomklinge"] = {
        Professions = 10007,
    },
    ["Aschenstoffrobe"] = {
        Professions = 12069,
    },
    ["Verhexung"] = {
        ["Bom'bay"] = 16707,
        ["Bom'bay"] = 16708,
        ["Bom'bay"] = 16709,
        ["Hohepriesterin Hai'watna"] = 18503,
    },
    ["Barbarische Eisenstiefel"] = {
        Professions = 9818,
    },
    ["Armschiene - Willenskraft"] = {
        Professions = 13642,
    },
    ["Verzauberter Kampfhammer"] = {
        Professions = 16973,
    },
    ["Goblin-Raketenhelm"] = {
        Professions = 12758,
    },
    ["Flammenatem"] = {
        ["Narillasanz"] = 9573,
        ["Teremus der Verschlinger"] = 9573,
        ["Abtrünniger schwarzer Drache"] = 9573,
    },
    ["Feuerelementar beschwören"] = {
        ["Feuerelementar"] = 8985,
        ["Scharlachroter Herbeizauberer"] = 8985,
    },
    ["Vergiftung heilen"] = {
        ["DRUID"] = 8946,
        ["SHAMAN"] = 526,
    },
    ["Verkrüppelndes Gift II"] = {
        ["ROGUE"] = 3421,
    },
    ["Verfärbter Heiltrank"] = {
        Professions = 4508,
    },
    ["Elixier der geringen Beweglichkeit"] = {
        Professions = 2333,
    },
    ["Verzweifeltes Gebet"] = {
        ["PRIEST"] = 19243,
    },
    ["Braune Leinenhose"] = {
        Professions = 3914,
    },
    ["Schwere irdene Handschuhe"] = {
        Professions = 9149,
    },
    ["Cyclonian beschwören"] = {
        ["Bath'rah der Windbehüter"] = 8606,
    },
    ["Elixier der Entd. geringer Unsichtbarkeit"] = {
        Professions = 3453,
    },
    ["Tunika des Nachtschleichers"] = {
        Professions = 10499,
    },
    ["Modisches grünes Hemd"] = {
        Professions = 7893,
    },
    ["Erheblicher Verjüngungstrank"] = {
        Professions = 22732,
    },
    ["Verderbnis"] = {
        ["WARLOCK"] = 25311,
        Professions = 16985,
    },
    ["Morastiger Matsch"] = {
        ["Morastbäumling"] = 5567,
    },
    ["Raptorstoß"] = {
        ["HUNTER"] = 14266,
    },
    ["Hurtigkeitstrank"] = {
        Professions = 2335,
    },
    ["Kristallblick"] = {
        ["Steinmagenbasilisk"] = 3635,
        ["Kristallrückenbasilisk"] = 3635,
        ["Salzsteinstarrer"] = 3635,
        ["Glashautstarrer"] = 3635,
        ["Rotsteinbasilisk"] = 3635,
        ["Todesauge"] = 3635,
    },
    ["Handauflegung"] = {
        ["PALADIN"] = 10310,
        ["Scharlachroter Kommandant Mograine"] = 9257,
    },
    ["Richturteil"] = {
        ["PALADIN"] = 20271,
    },
    ["Elixier der Schattenmacht"] = {
        Professions = 11476,
    },
    ["Schwerer Gürtel der Holzschlundfeste"] = {
        Professions = 23628,
    },
    ["Totem der glühenden Magma"] = {
        ["SHAMAN"] = 10587,
    },
    ["Einhüllendes Gespinst"] = {
        ["Gruftkriecher"] = 15471,
        ["Nerubischer Aufseher"] = 15471,
    },
    ["Finsterer Stoß"] = {
        ["ROGUE"] = 11294,
    },
    ["Kaltblütigkeit"] = {
        ["ROGUE"] = 14177,
    },
    ["Geisterzwirnhandschuhe"] = {
        Professions = 18413,
    },
    ["Knurren"] = {
        ["DRUID"] = 6795,
        ["HUNTER"] = 14927,
        Pet = 14921,
    },
    ["Geringes Zauberöl"] = {
        Professions = 25126,
    },
    ["Grünes Feuerwerk"] = {
        Professions = 23068,
    },
    ["Leichtes Rüstungsset"] = {
        Professions = 2152,
    },
    ["Handwerkermonokel"] = {
        Professions = 3966,
    },
    ["Giftwolke"] = {
        ["Aku'mai"] = 3815,
        ["Eisenrücken"] = 3815,
    },
    ["Zielattrappe - Ereignis 001"] = {
        ["Mörsertrupp-Zielattrappe"] = 18634,
        ["Späher Klemmy"] = 18634,
    },
    ["Schwachen Trank trinken"] = {
        ["Murlocfutterwühler"] = 3368,
        ["Kräuterkundiger der Flusspfoten"] = 3368,
    },
    ["Schlamm"] = {
        ["Schlamm"] = 3514,
    },
    ["Eisenfederbrustplatte"] = {
        Professions = 19086,
    },
    ["Geisterzwirngürtel"] = {
        Professions = 18410,
    },
    ["Rakete, Rot Groß"] = {
        ["Pats Feuerwerkstyp - ROT GROSS"] = 26354,
    },
    ["Strahlender Gürtel"] = {
        Professions = 16645,
    },
    ["Feuerstein herstellen (groß)"] = {
        ["WARLOCK"] = 17952,
    },
    ["Irdene Weste"] = {
        Professions = 8764,
    },
    ["Magierblick"] = {
        ["Erzmagier Ansirem Runeweaver"] = 3659,
    },
    ["Ausweiden"] = {
        ["ROGUE"] = 31016,
    },
    ["Schreckensgeheul"] = {
        ["WARLOCK"] = 17928,
    },
    ["Feuerstein herstellen (gering)"] = {
        ["WARLOCK"] = 6366,
    },
    ["Schlachtross beschwören"] = {
        ["PALADIN"] = 13819,
    },
    ["Mondstoff"] = {
        Professions = 18560,
    },
    ["Feuerschutztrank"] = {
        Professions = 7257,
    },
    ["Schultern des Nachtschleichers"] = {
        Professions = 10516,
    },
    ["Wasser herbeizaubern"] = {
        ["MAGE"] = 10140,
    },
    ["Ingenieurskunst"] = {
        Professions = 4037,
        Professions = 4038,
        Professions = 12656,
    },
    ["Waffe - Dämonentöten"] = {
        Professions = 13915,
    },
    ["Schwärzliche Ledergamaschen"] = {
        Professions = 9195,
    },
    ["Feurige Plattenstulpen"] = {
        Professions = 16655,
    },
    ["Strahlungswolke"] = {
        ["Bestrahlter Brühschleimer"] = 10341,
    },
    ["Dunkles Geflüster"] = {
        ["Übler Tutor"] = 16587,
    },
    ["Infernohandschuhe"] = {
        Professions = 22868,
    },
    ["Azrethocs Stampfen"] = {
        ["Lord Azrethocs Bild"] = 7961,
    },
    ["Fallschirmumhang"] = {
        Professions = 12616,
    },
    ["Perlenschnallenumhang"] = {
        Professions = 6521,
    },
    ["Schillernder Hammer"] = {
        Professions = 6518,
    },
    ["Ahornsamenkorn"] = {
        ["DRUID"] = 17034,
    },
    ["Blenden"] = {
        ["ROGUE"] = 2094,
    },
    ["Furchtzauberschutz"] = {
        ["PRIEST"] = 6346,
    },
    ["Todesmantel"] = {
        ["WARLOCK"] = 17926,
    },
    ["Umhang - Überragende Verteidigung"] = {
        Professions = 20015,
    },
    ["Handgenähte Lederweste"] = {
        Professions = 7126,
    },
    ["Schimärenhandschuhe"] = {
        Professions = 19053,
    },
    ["Schwacher Magiewiderstandstrank"] = {
        Professions = 3172,
    },
    ["Transmutieren: Mithril in Echtsilber"] = {
        Professions = 11480,
    },
    ["Kupferaxt"] = {
        Professions = 2738,
    },
    ["Winterzwirnhose"] = {
        Professions = 18424,
    },
    ["Wachsende Willenskraft"] = {
        ["Quetschzahn"] = 10767,
    },
    ["Lavaexplosion C beschwören"] = {
        ["Kernratte"] = 21901,
    },
    ["Armschiene - Schwache Ausdauer"] = {
        Professions = 7457,
    },
    ["Runenverzierter Ledergürtel"] = {
        Professions = 19072,
    },
    ["Flammenspeien"] = {
        ["Feuerroc"] = 11021,
    },
    ["Schattensichels gefallenes Streitross beschwören"] = {
        ["Schattensichels gefallenes Streitross"] = 23261,
        ["Todesritter Schattensichel"] = 23261,
    },
    ["Tückische Lederhose"] = {
        Professions = 19083,
    },
    ["Krallenhieb"] = {
        ["DRUID"] = 9904,
    },
}
