if GetLocale() ~= "frFR" then return end
ClassicData = {
    version = "fr1",
    ["Promptitude"] = {
        ["HUNTER"] = 23989,
    },
    ["Rugissement démoralisant"] = {
        ["DRUID"] = 9898,
        ["Protecteur cénarien"] = 15727,
    },
    ["Points d'honneur +378"] = {
        ["Porteguerre de la Horde"] = 24964,
        ["Général de brigade de l'Alliance"] = 24964,
    },
    ["Eclosion des œufs"] = {
        ["Grande prêtresse Mar'li"] = 24083,
    },
    ["Régénération frénétique"] = {
        ["DRUID"] = 22896,
    },
    ["Ceinture en cuir cousu main"] = {
        Professions = 3753,
    },
    ["Invocation d'une larve silithide"] = {
        ["Larve silithide"] = 6588,
        ["Œuf de rampant silithide"] = 6588,
    },
    ["Tunique de dragonnet noir"] = {
        Professions = 24940,
    },
    ["Rage d'Arantir"] = {
        ["Arantir"] = 9741,
    },
    ["Epaulières en gangrétoffe"] = {
        Professions = 18453,
    },
    ["Bottes en lin écru"] = {
        Professions = 12045,
    },
    ["Monocle d'artisan"] = {
        Professions = 3966,
    },
    ["Ench. de bracelets (Endurance mineure)"] = {
        Professions = 7457,
    },
    ["Potion de soins excellente"] = {
        Professions = 11457,
    },
    ["Totem de Peau de pierre"] = {
        ["SHAMAN"] = 10408,
    },
    ["Grande masse noire"] = {
        Professions = 10001,
    },
    ["Brassards en écailles de tortue"] = {
        Professions = 10518,
    },
    ["Epaulières lourdes en mithril"] = {
        Professions = 9926,
    },
    ["Ceinture runique en cuir"] = {
        Professions = 19072,
    },
    ["Peau moyenne traitée"] = {
        Professions = 3817,
    },
    ["Couronne sylvestre"] = {
        Professions = 28481,
    },
    ["Filet"] = {
        ["Ruklar le trappeur"] = 12024,
        ["Trappeur défias"] = 12024,
        ["Lanceur de filet murloc"] = 12024,
        ["Ecumeur Branchie-bleue"] = 12024,
        ["Pêcheur Grisebrume"] = 12024,
        ["Querelleur kolkar"] = 12024,
        ["Cogneur de Ratchet"] = 12024,
        ["Cogneur de Baie-du-Butin"] = 12024,
        ["Travépieu"] = 12024,
        ["Cogneur de Gadgetzan"] = 12024,
        ["Estafette Blackrock"] = 6533,
        ["Tharil'zun"] = 6533,
        ["Chasseur Bloodscalp"] = 6533,
        ["Fracasseur de Kurzen"] = 6533,
        ["Trappeur Poil-moussu"] = 6533,
        ["Ecumeur Dragonmaw"] = 6533,
        ["Esclavagiste Brisepoing"] = 6533,
        ["Ecumeur de la Voile sanglante"] = 6533,
        ["Dame Moongazer"] = 6533,
        ["Geôlier Marlgen"] = 6533,
        ["Geôlier Borhuin"] = 6533,
        ["Chasseur de têtes Witherbark"] = 6533,
        ["Ecumeur Daguéchine"] = 6533,
        ["Barak Kodobane"] = 6533,
        ["Querelleur galak"] = 6533,
        ["Gardien de Tranchebauge"] = 6533,
        ["Querelleur magram"] = 6533,
        ["Tenue de maintien de l'ordre"] = 6533,
        ["Gelihast"] = 6533,
        ["Chef Copperplug"] = 6533,
        ["Fondeboue"] = 6533,
        ["Ecumeur Jhash"] = 6533,
        ["Assassin galak"] = 6533,
        ["Techbot"] = 10852,
    },
    ["Gants du Grandpère"] = {
        Professions = 21943,
    },
    ["Petite comète verte"] = {
        Professions = 26417,
    },
    ["Gants en écailles de tortue"] = {
        Professions = 10509,
    },
    ["Création de la faille du Cœur d'Hakkar"] = {
        ["Coeur d'Hakkar"] = 24202,
    },
    ["Perception"] = {
        Racial = 20600,
    },
    ["Potion de soins majeure"] = {
        Professions = 17556,
    },
    ["Piège givrant"] = {
        ["HUNTER"] = 14311,
    },
    ["Entaille Traqueur de l'ombre"] = {
        ["Traqueur des ténèbres Mornecoeur"] = 6927,
    },
    ["Transmutation : Fer en Or"] = {
        Professions = 11479,
    },
    ["Gants en étoffe lumineuse"] = {
        Professions = 18415,
    },
    ["Poison douloureux III"] = {
        ["ROGUE"] = 13229,
    },
    ["Transmutation d'arcanite"] = {
        Professions = 17187,
    },
    ["Petit dragon mécanique en mithril"] = {
        Professions = 12624,
    },
    ["Murmures sombres"] = {
        ["Vil tuteur"] = 16587,
    },
    ["Casque armé du scorpide"] = {
        Professions = 10570,
    },
    ["Rituel de malédiction"] = {
        ["WARLOCK"] = 18540,
    },
    ["Crachat infectieux"] = {
        ["Jeune pestiféré"] = 17745,
    },
    ["Comète, JAUNE"] = {
        ["Type des feux d'artifice de Pat - JAUNE"] = 26349,
    },
    ["Crée le Sceptre de Celebras"] = {
        ["Celebras le Racheté"] = 21939,
    },
    ["Esprit de Lunegriffe"] = {
        ["Esprit de Lunegriffe"] = 18986,
        ["Lunegriffe"] = 18986,
    },
    ["Lancer de hache"] = {
        ["Sculpteur gobelin"] = 6466,
    },
    ["Puits de lumière"] = {
        ["PRIEST"] = 27871,
    },
    ["Totem de résistance au Givre"] = {
        ["SHAMAN"] = 10479,
    },
    ["Piqûre de wyverne"] = {
        ["HUNTER"] = 24133,
    },
    ["Puissant marteau en fer"] = {
        Professions = 3297,
    },
    ["Résurrection de Gangretrombe"] = {
        ["Commandant Gangretrombe"] = 3488,
        ["Commandant Gangretrombe"] = 3488,
    },
    ["Aura de concentration"] = {
        ["PALADIN"] = 19746,
    },
    ["Griffe"] = {
        ["DRUID"] = 9850,
        Pet = 3009,
    },
    ["Mot de l'ombre : Douleur"] = {
        ["Sirène Strashaz"] = 15654,
        ["Géomètre de Froidemine"] = 15654,
        ["Géomètre de Gouffrefer"] = 15654,
        ["Géomètre de Froidemine vétéran"] = 15654,
        ["Géomètre de Gouffrefer vétéran"] = 15654,
        ["Géomètre de Gouffrefer champion"] = 15654,
        ["Vol'jin"] = 17146,
    },
    ["Rénovation"] = {
        ["PRIEST"] = 25315,
        ["Sirène Fouette-bile"] = 11640,
        ["Infirmier du Hangar"] = 22168,
        ["Prêtre de Dun Garok"] = 8362,
        ["Narillasanz"] = 8362,
        ["Disciple écarlate"] = 8362,
        ["Chapelain écarlate"] = 8362,
        ["Abbé écarlate"] = 8362,
        ["Acolyte de la Tête de Mort"] = 8362,
        ["Acolyte du crépuscule"] = 8362,
        ["Oracle Noctant"] = 8362,
        ["Prêtre du corps expéditionnaire"] = 8362,
    },
    ["Rage berserker"] = {
        ["WARRIOR"] = 18499,
    },
    ["Gantelets runique en cuir"] = {
        Professions = 19055,
    },
    ["Grande comète rouge"] = {
        Professions = 26422,
    },
    ["Justice du généralissime"] = {
        ["Généralissime Bolvar Fordragon"] = 20683,
    },
    ["Créer un Oeufilloscope"] = {
        ["Tinkee Steamboil"] = 16029,
    },
    ["Fracas de vagues"] = {
        ["Gazban"] = 5403,
    },
    ["Epée de guerre en bronze"] = {
        Professions = 9986,
    },
    ["Silex à aiguiser dense"] = {
        Professions = 16639,
    },
    ["Flétrir"] = {
        ["Emissaire Roman'khan"] = 23772,
    },
    ["Téléportation : Ironforge"] = {
        ["MAGE"] = 3562,
    },
    ["Chapelet de comètes bleues"] = {
        Professions = 26423,
    },
    ["Rayon mortel gnome"] = {
        Professions = 12759,
    },
    ["Mutiler"] = {
        ["DRUID"] = 9881,
        ["Uthil Mooncall"] = 12161,
        ["Conservateur cénarien"] = 12161,
        ["Grifferage"] = 12161,
    },
    ["Cape d'homme des collines"] = {
        Professions = 3760,
    },
    ["Effet de la poudre de renne"] = {
        ["Metzen le Renne"] = 25952,
    },
    ["Gants du phénix"] = {
        Professions = 3868,
    },
    ["Création de Pierre de soins"] = {
        ["WARLOCK"] = 5699,
    },
    ["Fonte de l'or"] = {
        Professions = 3308,
    },
    ["Déclencheur enragé"] = {
        ["Lige du feu"] = 19515,
    },
    ["Esprit pestiféré"] = {
        ["Mangeur de cervelle"] = 3429,
        ["Chasseur de mages"] = 3429,
    },
    ["Pantalon de gardien"] = {
        Professions = 7147,
    },
    ["Elixir de Maîtrise du feu"] = {
        Professions = 7845,
    },
    ["Larmes du Cherchevent"] = {
        ["Prince Thunderaan"] = 23011,
    },
    ["Poignets glaciaires"] = {
        Professions = 28209,
    },
    ["Renfort d'armure robuste"] = {
        Professions = 19058,
    },
    ["Caquètement de guerre"] = {
        ["Coq de combat"] = 23060,
    },
    ["Rage sauvage"] = {
        ["Sauvage Poil-Putride"] = 3258,
        ["Stanley"] = 3258,
        ["Enragé d'Ombrecroc"] = 7072,
    },
    ["Nova de gel"] = {
        ["Ras Murmegivre"] = 18099,
    },
    ["Feu intérieur"] = {
        ["PRIEST"] = 10952,
    },
    ["Ench. d'arme (Frappe mineure)"] = {
        Professions = 7788,
    },
    ["Robe en tisse-sorcier"] = {
        Professions = 18446,
    },
    ["Régénération sauvage"] = {
        ["Protecteur Cognegivre"] = 9616,
        ["Glacial déviant"] = 7948,
    },
    ["Armure naturelle"] = {
        ["HUNTER"] = 24632,
    },
    ["Cuir enchanté"] = {
        Professions = 17181,
    },
    ["Bouquet rouge"] = {
        ["Maître-dynamiteur Emi Shortfuse"] = 11542,
    },
    ["Coup de pied"] = {
        ["ROGUE"] = 1769,
        ["Moine écarlate"] = 11978,
        ["Bandit du Totem sinistre"] = 11978,
        ["Ravageur mort-vivant"] = 11978,
    },
    ["Oeuf Gorishi"] = {
        ["Oeuf Gorishi"] = 14205,
        ["Matriarche Zavas"] = 14205,
    },
    ["Larme de Razelikh II"] = {
        ["Razelikh le Souilleur"] = 10866,
    },
    ["Canon à flammes"] = {
        ["Assemblage du marteau de colère"] = 15575,
    },
    ["Eclair de cristal"] = {
        ["Basilic gueule de fer"] = 5106,
        ["Ventrécaille"] = 5106,
        ["Basilic Peau-de-verre"] = 5106,
        ["Peau de cristal Rougepierre"] = 5106,
    },
    ["Ench. de bottes (Agilité)"] = {
        Professions = 13935,
    },
    ["Acide corrosif"] = {
        ["Fourrageur Mufle-de-gravier"] = 8245,
    },
    ["Poil-moussu"] = {
        ["Gnoll Poil-moussu"] = 3288,
        ["Bâtard Poil-moussu"] = 3288,
        ["Tisseur de brumes Poil-moussu"] = 3288,
        ["Paludier Poil-moussu"] = 3288,
        ["Trappeur Poil-moussu"] = 3288,
        ["Mystique Poil-moussu"] = 3288,
        ["Alpha Poil-moussu"] = 3288,
    },
    ["Eclair"] = {
        ["SHAMAN"] = 15208,
        ["Chaman de Gouffrefer"] = 15801,
        ["Mystique Rivepatte"] = 9532,
        ["Oracle mineur murloc"] = 9532,
        ["Mystique Bloodscalp"] = 9532,
        ["Mystique Skullsplitter"] = 9532,
        ["Oracle Selécaille"] = 9532,
        ["Mystique Poil-moussu"] = 9532,
        ["Chaman Rivepatte"] = 9532,
        ["Géologue de la KapitalRisk"] = 9532,
        ["Prophète Brisepierre"] = 9532,
        ["Chaman Mo'grosh"] = 9532,
        ["Mystique Mo'grosh"] = 9532,
        ["Prophète Frostmane"] = 9532,
        ["Magosh"] = 9532,
        ["Oracle mineur Aileron noir"] = 9532,
        ["Deeb"] = 9532,
        ["Matriarche Plumesang"] = 9532,
        ["Chaman Noirbois"] = 9532,
        ["Oracle Grisebrume"] = 9532,
        ["Sirène Daguéchine"] = 9532,
        ["Oracle Aileron déchiré"] = 9532,
        ["Chaman Rochepoing"] = 9532,
        ["Chaman Crache-poussières"] = 9532,
        ["Exilé foudroyant"] = 9532,
        ["Mystique Crache-poussières"] = 9532,
        ["Chaman Dos-hirsute"] = 9532,
        ["Sorcière du vent Furie-des-vents"] = 9532,
        ["Matriarche Furie-des-vents"] = 9532,
        ["Sorcière des tempêtes Terrevent"] = 9532,
        ["Jeune faucon-tonnerre"] = 9532,
        ["Légion-tempête kolkar"] = 9532,
        ["Nak"] = 9532,
        ["Boahn"] = 9532,
        ["Invocateur de pluie Sombretaillis"] = 9532,
        ["Druide du Croc"] = 9532,
        ["Caedakar le Vicieux"] = 9532,
        ["Sorcière des tempêtes Rougefurie"] = 9532,
        ["Jeune chimère"] = 9532,
        ["Légion-tempête galak"] = 9532,
        ["Gardien Ordanus"] = 9532,
        ["Oracle Aileron boueux"] = 9532,
        ["Fouilleur Bourbecoque"] = 9532,
        ["Géomancienne de Tranchebauge"] = 9532,
        ["Chasse-vent kolkar"] = 9532,
        ["Chasse-vent magram"] = 9532,
        ["Chasse-vent gelkis"] = 9532,
        ["Contremaître Cozzle"] = 9532,
        ["Oracle Noctant"] = 9532,
        ["Implorateur de la terre Halmgar"] = 9532,
        ["Roogug"] = 9532,
        ["Oracle de la baie des tempêtes"] = 9532,
        ["Brumaile insoumis"] = 9532,
        ["Prophète-tempête kolkar"] = 9532,
        ["Chaman Ragefeu"] = 9532,
        ["Imploratrice céleste Brûleglace"] = 9532,
        ["Lorgus Jett"] = 12167,
        ["Serpent-nuage"] = 8246,
        ["Ancien serpent-nuage"] = 8246,
        ["Mystique des Carmines"] = 20802,
        ["Sorcier du Totem sinistre"] = 20802,
        ["Chaman Fangemufle"] = 20805,
        ["Sorcière Daguéchine"] = 20824,
        ["Devin Vilebranch"] = 20824,
        ["Primaliste Thurloga"] = 15234,
    },
    ["Chapeau de cuir confortable"] = {
        Professions = 10490,
    },
    ["Explosion des arcanes"] = {
        ["MAGE"] = 10202,
        ["Arcaniste Doan"] = 9433,
        ["Capitaine Balinda Stonehearth"] = 13745,
    },
    ["Tube en cuivre"] = {
        Professions = 3924,
    },
    ["Perce-faille"] = {
        ["Sanguinaire"] = 7140,
    },
    ["Altérateur de vrai-argent"] = {
        Professions = 23071,
    },
    ["Délivrance de la malédiction mineure"] = {
        ["MAGE"] = 475,
    },
    ["Sac démoniaque de Xandivious"] = {
        ["Xandivious"] = 25791,
    },
    ["Portail : Orgrimmar"] = {
        ["MAGE"] = 11417,
    },
    ["Forme de zombie"] = {
        ["Infiltrateur Marksen"] = 7293,
    },
    ["Gants de méditation"] = {
        Professions = 3852,
    },
    ["Cuirasse en plumacier"] = {
        Professions = 19086,
    },
    ["Déchirure de l'âme"] = {
        ["Ailes du désespoir"] = 3405,
    },
    ["Détection de l'invisibilité supérieure"] = {
        ["WARLOCK"] = 11743,
    },
    ["Robe en étoffe lunaire"] = {
        Professions = 22902,
    },
    ["Gants barbares"] = {
        Professions = 3771,
    },
    ["Peau légère traitée"] = {
        Professions = 3816,
    },
    ["Bénédiction du chef de guerre"] = {
        ["Thrall"] = 16609,
        ["Héraut de Thrall"] = 16609,
    },
    ["Epaulières terrestres en cuir"] = {
        Professions = 9147,
    },
    ["Masse lourde en bronze"] = {
        Professions = 3296,
    },
    ["Grondement menaçant"] = {
        ["Loup des prairies"] = 5781,
        ["Loup Alpha des prairies"] = 5781,
    },
    ["Acide de Fouisseur"] = {
        ["Fouisseur"] = 18070,
    },
    ["Ralentissement"] = {
        ["Techbot"] = 10855,
    },
    ["Invocation du destrier déchu de Ravassombre"] = {
        ["Destrier déchu de Ravassombre"] = 23261,
        ["Chevalier de la mort Ravassombre"] = 23261,
    },
    ["Boire un Barleybrew Scalder"] = {
        ["Yorus Barleybrew"] = 8554,
    },
    ["Réflexes vifs"] = {
        ["Guerrier écarlate"] = 3238,
        ["M. Smite"] = 6264,
        ["Officier de Bael'dun"] = 6264,
    },
    ["Fragilité"] = {
        ["Goule dévastée"] = 12530,
        ["Zul'Lor"] = 12530,
    },
    ["Gardiens d’Ilkrud"] = {
        ["Marcheur du Vide"] = 6487,
        ["Ilkrud Magthrull"] = 6487,
    },
    ["Ench. d'arme (Arme flamboyante)"] = {
        Professions = 13898,
    },
    ["Arme Furie-des-vents"] = {
        ["SHAMAN"] = 16362,
    },
    ["Casque de feu"] = {
        Professions = 10632,
    },
    ["Provocation"] = {
        ["WARRIOR"] = 355,
    },
    ["Potion de Bouclier de pierre supérieure"] = {
        Professions = 17570,
    },
    ["Boire la potion"] = {
        ["Bartleby"] = 9956,
    },
    ["Soins supérieurs"] = {
        ["PRIEST"] = 25314,
    },
    ["Nuage empoisonné"] = {
        ["Aku'mai"] = 3815,
        ["Echine-de-fer"] = 3815,
    },
    ["Innervation"] = {
        ["DRUID"] = 29166,
    },
    ["Esprits hanteurs"] = {
        ["Serviteur hanté"] = 7057,
    },
    ["Structure en bronze"] = {
        Professions = 3953,
    },
    ["Epaulières noires en tisse-mage"] = {
        Professions = 12074,
    },
    ["Totem de Grâce aérienne"] = {
        ["SHAMAN"] = 25359,
    },
    ["Focalisation des arcanes"] = {
        ["Professeur spectral"] = 17633,
    },
    ["Potion de soins supérieure"] = {
        Professions = 7181,
    },
    ["Rédemption"] = {
        ["PALADIN"] = 20773,
    },
    ["Toucher glacial de Trelane"] = {
        ["Kor'gresh Coldrage"] = 4320,
    },
    ["Rets"] = {
        ["Mère Croc"] = 12023,
        ["Tisse-soie Tissebois"] = 12023,
        ["Traqueuse Ronce-sauvage"] = 12023,
        ["Araignée Tisse-venin"] = 745,
        ["Tisseuse des pleurs"] = 745,
        ["Tisseuse des mousses profondes"] = 745,
        ["Araignée verrière"] = 745,
        ["Ravageur des tombes"] = 745,
        ["Grumetoile solitaire"] = 745,
        ["Traqueuse des rochers"] = 745,
        ["Besseleth"] = 745,
    },
    ["Harmonisation avec le Cœur du magma"] = {
        ["Lothos Riftwaker"] = 22877,
    },
    ["Elixir de résistance au poison"] = {
        Professions = 3174,
    },
    ["Craquement de flammes"] = {
        ["Evocateur Bloodaxe"] = 15743,
        ["Volchan"] = 15743,
    },
    ["Volonté de Shahram"] = {
        ["Shahram"] = 16598,
    },
    ["Faim insatiable"] = {
        ["Loup blanc affamé"] = 3151,
    },
    ["Potion de sang de troll hargneux"] = {
        Professions = 3451,
    },
    ["Bombe fumigène"] = {
        ["Commando de Kurzen"] = 7964,
        ["Bazil Thredd"] = 7964,
        ["Colonel Kurzen"] = 8817,
    },
    ["Créer la Bannière de Krom'zar"] = {
        ["Chef de guerre Krom'zar"] = 13965,
    },
    ["Robe noire simple"] = {
        Professions = 12077,
    },
    ["Hémorragie"] = {
        ["ROGUE"] = 17348,
    },
    ["Dynamite basique"] = {
        Professions = 3919,
    },
    ["Oeuf de rampant silithide"] = {
        ["Œuf de rampant silithide"] = 6587,
        ["Rampant silithide"] = 6587,
    },
    ["Déluge de lames"] = {
        ["ROGUE"] = 13877,
    },
    ["Ench. de cape (Protection inférieure)"] = {
        Professions = 13421,
    },
    ["Malédiction de faiblesse"] = {
        ["WARLOCK"] = 11708,
        ["Ombregriffe"] = 17227,
        ["Exhalombre Frostmane"] = 11980,
        ["Sorcier-docteur Mosh'Ogg"] = 11980,
        ["Licillin"] = 11980,
        ["Balizar l’Ombrageux"] = 11980,
        ["Xabraxxis"] = 11980,
        ["Jergosh l'Invocateur"] = 18267,
        ["Darbel Montrose"] = 12741,
        ["Morloch"] = 12741,
        ["Echine-de-fer"] = 21007,
    },
    ["Invocation d'une trousse d'outils de voleur"] = {
        ["Appareil bidulotronique de Wrenix"] = 9949,
    },
    ["Déchirure"] = {
        ["DRUID"] = 9896,
    },
    ["Eclair lumineux"] = {
        ["PALADIN"] = 19943,
    },
    ["Jeune lion des savanes"] = {
        ["Jeune lion des savanes"] = 6598,
        ["Matriarche des savanes"] = 6598,
    },
    ["Ench. de plastron (Mana majeur)"] = {
        Professions = 20028,
    },
    ["Ceinture en thorium"] = {
        Professions = 16643,
    },
    ["Puanteur pourrie"] = {
        ["Putridius"] = 12946,
        ["Pestegueule le Pourrissant"] = 12946,
        ["Seigneur du Chaudron Irâme"] = 12946,
    },
    ["Explosion de flammes"] = {
        ["Delmanis le Haï"] = 7101,
    },
    ["Déposer une mine"] = {
        ["Télé-mine"] = 25024,
        ["Chariotte à vapeur"] = 25024,
    },
    ["Carquois lourd"] = {
        Professions = 9193,
    },
    ["Grognement d'intimidation"] = {
        ["Vieux Barbeglace"] = 3146,
    },
    ["Tube en mithril"] = {
        Professions = 12589,
    },
    ["Frénésie impie"] = {
        ["Gardien squelette"] = 8699,
    },
    ["Dévorer la magie"] = {
        Pet = 19736,
    },
    ["Fièvre torride"] = {
        ["Limon de mucus"] = 14130,
    },
    ["Aspect du faucon"] = {
        ["HUNTER"] = 25296,
    },
    ["Jugement"] = {
        ["PALADIN"] = 20271,
    },
    ["Focalisation améliorée"] = {
        ["PRIEST"] = 14751,
    },
    ["Crachat acide"] = {
        ["Jeune dragonnet"] = 20821,
        ["Dragonnet rêveur"] = 20821,
    },
    ["Peur"] = {
        ["WARLOCK"] = 6215,
    },
    ["Epaulières en écailles de dragon bleu"] = {
        Professions = 19089,
    },
    ["Invocation de Marduk le Noir"] = {
        ["Marduk le Noir"] = 18650,
        ["Capitaine Redpath"] = 18650,
    },
    ["Gants noirs en tisse-mage"] = {
        Professions = 12053,
    },
    ["Gants noirs en cuir"] = {
        Professions = 3765,
    },
    ["Robe de l'archimage"] = {
        Professions = 18457,
    },
    ["Gilet sylvestre"] = {
        Professions = 28480,
    },
    ["Infusion de puissance"] = {
        ["PRIEST"] = 10060,
    },
    ["Gantelets en écailles dorées"] = {
        Professions = 11643,
    },
    ["Reconstruction"] = {
        ["Tor'gan"] = 4093,
        ["Réformeur terrestre"] = 10260,
    },
    ["Dynamite grossière"] = {
        Professions = 3931,
        ["Ashlan Stonesmirk"] = 9002,
        ["Ashlan Stonesmirk"] = 9003,
        ["Ashlan Stonesmirk"] = 9004,
        ["Ashlan Stonesmirk"] = 9009,
    },
    ["Vague explosive"] = {
        ["MAGE"] = 13021,
    },
    ["Portail : Undercity"] = {
        ["MAGE"] = 11418,
    },
    ["Connaissance des bêtes"] = {
        ["HUNTER"] = 1462,
    },
    ["Cuirasse grossière en bronze"] = {
        Professions = 2670,
    },
    ["Pantalon en tisse-ombre"] = {
        Professions = 12052,
    },
    ["Création de Pierre de feu (supérieure)"] = {
        ["WARLOCK"] = 17952,
    },
    ["Etincelle"] = {
        ["Fée Ley"] = 21027,
        ["Fée de mana"] = 21027,
    },
    ["Voile de l'ombre"] = {
        ["Vol'jin"] = 17820,
        ["Tisseur des ombres Frayeloup"] = 7068,
        ["Mort angoissé"] = 7068,
        ["Ombre de Fellicent"] = 7068,
    },
    ["Lunette mortelle"] = {
        Professions = 12597,
    },
    ["Malédiction démoniaque"] = {
        ["Artorius l'Aimable"] = 23298,
    },
    ["Morsure vicieuse"] = {
        ["Ancien chien du Magma"] = 19319,
        ["Grimmaw"] = 19319,
    },
    ["Nuée maléfique"] = {
        ["Garde d’Undercity"] = 12187,
        ["Gordo"] = 12187,
    },
    ["Heaume en thorium"] = {
        Professions = 16653,
    },
    ["Libération des crapauds"] = {
        ["Crapaud de la jungle"] = 24058,
        ["Sorcier-docteur Hakkari"] = 24058,
    },
    ["Le mortier : rechargé"] = {
        Professions = 13240,
    },
    ["Bombe explosive"] = {
        Professions = 12619,
    },
    ["Garde de Laze"] = {
        ["Garde de Laze"] = 3827,
        ["Sorcier-docteur Mosh'Ogg"] = 3827,
    },
    ["Invocation d'un laquais rebelle"] = {
        ["Laquais ressuscité"] = 17618,
        ["Invocateur des ténèbres de Scholomance"] = 17618,
    },
    ["Invocation d'un grondeur gelkis"] = {
        ["Grondeur gelkis"] = 9653,
        ["Implorateur de la terre gelkis"] = 9653,
    },
    ["Potion de Nage rapide"] = {
        Professions = 7841,
    },
    ["Réflectoglace gyrogivre"] = {
        Professions = 23077,
    },
    ["Esprit divin"] = {
        ["PRIEST"] = 27841,
    },
    ["Inferno"] = {
        ["WARLOCK"] = 1122,
    },
    ["Bâtonnet runique en vrai-argent"] = {
        Professions = 13702,
    },
    ["Yéti mécanique paisible"] = {
        Professions = 26011,
    },
    ["Point d'apparition des Elémentaires"] = {
        ["Baron Kazum"] = 25035,
        ["Le duc des Cendres"] = 25035,
        ["Le duc des Profondeurs"] = 25035,
        ["Le duc des Eclats"] = 25035,
        ["Templier cramoisi"] = 25035,
        ["Templier d'azur"] = 25035,
        ["Templier chenu"] = 25035,
        ["Le duc des Zéphyrs"] = 25035,
        ["Templier terrestre"] = 25035,
    },
    ["Epaulières impériales en plaques"] = {
        Professions = 16646,
    },
    ["Poison mortel II"] = {
        ["ROGUE"] = 2837,
    },
    ["Sac en peau de kodo"] = {
        Professions = 5244,
    },
    ["Explosion morbide"] = {
        ["Nécromancien Scholomance"] = 17616,
    },
    ["Ench. de cape (Défense)"] = {
        Professions = 13635,
    },
    ["Blessure profonde"] = {
        ["Esprit malveillant"] = 12721,
        ["Aspect de malveillance"] = 12721,
    },
    ["Elixir de Sagesse"] = {
        Professions = 3171,
    },
    ["Appel du familier"] = {
        ["HUNTER"] = 883,
    },
    ["Gilet en tisse-rêve"] = {
        Professions = 12070,
    },
    ["Passe-partout en argent"] = {
        Professions = 19666,
    },
    ["Balle gyroscopique en mithril"] = {
        Professions = 12621,
    },
    ["Tir"] = {
        ["Fusilier Middlecamp"] = 16572,
        ["Fusilier Middlecamp"] = 16768,
        ["Fusilier Middlecamp"] = 16772,
        ["Fusilier Wheeler"] = 16775,
        ["Fusilier Middlecamp"] = 16776,
        ["Fusilier Middlecamp"] = 16777,
        ["Fusilier Wheeler"] = 16778,
        ["Fusilier Middlecamp"] = 16779,
        ["Fusilier Middlecamp"] = 16780,
        ["Fusilier Wheeler"] = 16767,
        ["Fusilier Sombrefer"] = 8995,
        ["Fusilier Sombrefer"] = 8996,
        ["Fusilier Sombrefer"] = 8997,
        ["Roggo Harlbarrow"] = 9008,
        ["Motley Garmason"] = 9008,
        ["Helena Atwood"] = 9008,
    },
    ["Pistage des draconiens"] = {
        ["HUNTER"] = 19879,
    },
    ["Potion de régénération majeure"] = {
        Professions = 22732,
    },
    ["Oeil de Kilrogg"] = {
        ["WARLOCK"] = 126,
    },
    ["Trait de choc"] = {
        ["HUNTER"] = 5116,
    },
    ["Connexion"] = {
        ["WARLOCK"] = 11689,
    },
    ["Bottes en écailles dorées"] = {
        Professions = 3515,
    },
    ["Mort du sous-chef Sombrefer"] = {
        ["Sous-chef Sombrefer"] = 12613,
    },
    ["Crochetage"] = {
        ["ROGUE"] = 1804,
    },
    ["Invocation d'un éclaté Theradrim"] = {
        ["Eclaté Theradrim"] = 21057,
        ["Gardien Theradrim"] = 21057,
    },
    ["Crapaud mécanique"] = {
        Professions = 19793,
    },
    ["Dynamite solide"] = {
        Professions = 12586,
    },
    ["Sceau de lumière"] = {
        ["PALADIN"] = 20349,
    },
    ["Ceinture en écailles de murloc"] = {
        Professions = 6702,
    },
    ["Amplifier flammes"] = {
        ["Tisseur d’ombre Ombreforge"] = 9482,
    },
    ["Broigne en écailles de scorpide épaisses"] = {
        Professions = 19051,
    },
    ["Cuirasse en écailles de murloc"] = {
        Professions = 6703,
    },
    ["Etreinte vampirique"] = {
        ["PRIEST"] = 15286,
    },
    ["Huile glaciale"] = {
        Professions = 3454,
    },
    ["Ench. d'arme (Frisson glacial)"] = {
        Professions = 20029,
    },
    ["Rosser"] = {
        ["Estafette Rivepatte"] = 3391,
        ["Edwin VanCleef"] = 3391,
        ["M. Smite"] = 3391,
        ["Kobold Rat des tunnels"] = 3391,
        ["Targorr le Terrifiant"] = 3391,
        ["Guide Noirbois"] = 3391,
        ["Clampant des récifs enragé"] = 3391,
        ["Vandale des hautes-terres"] = 3391,
        ["Marteleur Crache-poussières"] = 3391,
        ["Coyote farouche des rochers"] = 3391,
        ["Faucheur solécaille"] = 3391,
        ["Officier de Bael'dun"] = 3391,
        ["Guide Tranchecrin"] = 3391,
        ["Protecteur Vile-sylve"] = 3391,
        ["Volplume Rougefurie"] = 3391,
        ["Hydre Strashaz"] = 3391,
        ["Batteur des boues"] = 3391,
        ["Ancien batteur des boues"] = 3391,
        ["Fardel Dabyrie"] = 3391,
        ["Moine écarlate"] = 3391,
        ["Marteleur kolkar"] = 3391,
        ["Marteleur magram"] = 3391,
        ["Guêpe Centipaar"] = 3391,
        ["Dard Centipaar"] = 3391,
        ["Traînard Centipaar"] = 3391,
        ["Pilleur des Basses-cavernes"] = 3391,
        ["Vandale des flots"] = 3391,
        ["Grand Vandale des flots"] = 3391,
        ["Ravageur de guerre"] = 3391,
        ["Ferocitas le Mangeur de rêves"] = 3391,
        ["Ravageur Gangrepatte"] = 3391,
        ["Sélénien"] = 3391,
        ["Joufflu le croquant"] = 3391,
        ["Pisteur Noirbois"] = 3391,
        ["Ecorcheur Cinglepierre"] = 3391,
        ["Lunegriffe"] = 3391,
        ["Vorsha la Flagellante"] = 3391,
        ["Limier frostwolf"] = 3391,
        ["Hibou Stormpike"] = 3391,
        ["Harb Foulmountain"] = 3391,
    },
    ["Hache de guerre lourde"] = {
        Professions = 3294,
    },
    ["Lunettes d'oeil brillant"] = {
        Professions = 12587,
    },
    ["Forme de serpent"] = {
        ["Boahn"] = 8041,
        ["Druide du Croc"] = 8041,
    },
    ["Bourrasque"] = {
        ["WARRIOR"] = 12809,
        ["Diable de poussière"] = 6982,
        ["Sorcière du vent Plumesang"] = 6982,
        ["Messager du Vent Noirbois"] = 6982,
        ["Sorcière du vent Furie-des-vents"] = 6982,
        ["Imploratrice céleste hurlante"] = 6982,
        ["Sœur Rathtalon"] = 6982,
        ["Cyclonien"] = 6982,
        ["Vortex violent"] = 6982,
    },
    ["Invocation d'un traquelune chétif"] = {
        ["Traquelune chétif"] = 8594,
        ["Matriarche traquelune"] = 8594,
    },
    ["Coup fané"] = {
        ["Marche-bourbier Fane-liane"] = 5337,
        ["Protecteur flétri"] = 5337,
        ["Arei"] = 5337,
        ["Traître de Darrowshire"] = 5337,
        ["Traître spectral"] = 5337,
    },
    ["Hurlement terrifiant"] = {
        ["Chante-mort"] = 6605,
        ["Chauve-souris pestiférée"] = 6605,
        ["Pterreurdactyle"] = 6605,
        ["Cherche-rêve"] = 6605,
        ["Nefaru"] = 8715,
        ["Hurleuse Aile-de-roc"] = 8715,
        ["Hurleur des brumes"] = 8715,
    },
    ["Jambières armées du scorpide"] = {
        Professions = 10568,
    },
    ["Pieds couverts de mousse"] = {
        ["Rampant lacustre"] = 6870,
        ["Ancien Rampant lacustre"] = 6870,
        ["Mange-mousse Sombretaillis"] = 6870,
    },
    ["Grand maillet en cuivre"] = {
        Professions = 7408,
    },
    ["Invocation d'un grondeur terrestre"] = {
        ["Grondeur des pierres"] = 8270,
        ["Géomancienne de Tranchebauge"] = 8270,
        ["Implorateur de la terre Halmgar"] = 8270,
        ["Roogug"] = 8270,
    },
    ["Ench. d'arme (Frappe inférieure)"] = {
        Professions = 13503,
    },
    ["Elixir de Détection des morts-vivants"] = {
        Professions = 11460,
    },
    ["Masse en bronze"] = {
        Professions = 2740,
    },
    ["Griffeglace"] = {
        ["Bjarn"] = 3130,
        ["Ours Griffeglace"] = 3130,
    },
    ["Faiblesse"] = {
        ["Brise-falaises"] = 11443,
        ["Nécromancienne de la Tête de Mort"] = 11443,
    },
    ["Tempête de mana"] = {
        ["Azuregos"] = 21097,
    },
    ["Bulle des arcanes"] = {
        ["Arcaniste Doan"] = 9438,
    },
    ["Trait de feu"] = {
        ["MAGE"] = 10199,
    },
    ["Grand couteau en bronze"] = {
        Professions = 3491,
    },
    ["Diffuse"] = {
        ["Canaille défias"] = 6408,
        ["Onin MacHammar"] = 6408,
    },
    ["Commande de chariotte à vapeur"] = {
        Professions = 28327,
    },
    ["Vague de soins"] = {
        ["SHAMAN"] = 25357,
        ["Mystique Bloodscalp"] = 11986,
        ["Mystique Skullsplitter"] = 11986,
        ["Mystique Poil-moussu"] = 11986,
        ["Chaman Crin-de-Chardon"] = 11986,
        ["Oracle Arkkoran"] = 11986,
        ["Chaman Ragefeu"] = 11986,
        ["Oracle Hakkari"] = 15982,
        ["Chaman de Gouffrefer"] = 12492,
    },
    ["Ceinture en écailles de déviant"] = {
        Professions = 7955,
    },
    ["Frappe sacrée"] = {
        ["Montagnard de Dun Garok"] = 13953,
        ["Lieutenant Valorcall"] = 13953,
        ["Sauveur écarlate"] = 13953,
        ["Champion écarlate"] = 17143,
    },
    ["Fulgurance"] = {
        ["WARRIOR"] = 11585,
    },
    ["Sacrifice d’Hammertoe"] = {
        ["Hammertoe Grez"] = 4984,
    },
    ["Poison instantané IV"] = {
        ["ROGUE"] = 11341,
    },
    ["Chapelet de fusées bleu"] = {
        ["Type des chapelets de fusées de Pat (BLEU)"] = 26304,
        ["Héraut de la fête lunaire"] = 26304,
        ["Emissaire de la fête lunaire"] = 26304,
    },
    ["Intimidation"] = {
        ["HUNTER"] = 19577,
        ["Massacreur Cassecrête"] = 7093,
    },
    ["Varicelle de silithide"] = {
        ["Envahisseur silithide"] = 8137,
        ["Guêpe Gorishi"] = 8137,
        ["Ouvrier Gorishi"] = 8137,
        ["Ravageur Gorishi"] = 8137,
        ["Dard Gorishi"] = 8137,
        ["Tunnelière Gorishi"] = 8137,
        ["Garde Gorishi"] = 8137,
        ["Reine Gorishi"] = 8137,
    },
    ["Rapidité enchantée"] = {
        ["Enchanteur défias"] = 3443,
        ["Ensorceleuse Fouette-bile"] = 3443,
        ["Mith'rethis l'Enchanteur"] = 3443,
    },
    ["Bâtonnet en vrai-argent"] = {
        Professions = 14380,
    },
    ["Salve"] = {
        ["HUNTER"] = 14295,
    },
    ["Bouclier de mana"] = {
        ["MAGE"] = 10193,
    },
    ["Epaulières en plumacier"] = {
        Professions = 19062,
    },
    ["Elixir d'Agilité supérieure"] = {
        Professions = 11467,
    },
    ["Gigantesque hache en fer"] = {
        Professions = 3498,
    },
    ["Toucher de flétrissement"] = {
        ["Troll Witherbark"] = 4974,
        ["Lanceur de haches Witherbark"] = 4974,
        ["Chasseur de têtes Witherbark"] = 4974,
        ["Berserker Witherbark"] = 4974,
    },
    ["Posture berserker"] = {
        ["WARRIOR"] = 2458,
        ["Znort"] = 7366,
        ["Maraudeur magram"] = 7366,
        ["Maraudeur gelkis"] = 7366,
        ["Ravageur des Basses-cavernes"] = 7366,
        ["Champion mort"] = 7366,
    },
    ["Souffle d'acide corrosif"] = {
        ["Gyth"] = 16359,
    },
    ["Invocation d'un élémentaire de terre"] = {
        ["Elémentaire de terre"] = 19704,
        ["Invocateur des pierres du crépuscule"] = 19704,
    },
    ["Enchaînement"] = {
        ["WARRIOR"] = 20569,
        ["Ravageur flétri"] = 11427,
        ["Teremus le Dévoreur"] = 11427,
        ["Plaiedécaille vert"] = 15496,
        ["Troupe d'élite de Kurzen"] = 15496,
        ["Pilleur Cassecrête"] = 15496,
        ["Grunt d’Orgrimmar"] = 15496,
        ["Champion mort"] = 15496,
        ["Ravageur du Totem sinistre"] = 15496,
        ["Seigneur Kragaru"] = 15496,
        ["Kor'kron d'élite"] = 15496,
        ["Maréchal de Glace-sang"] = 15579,
        ["Maréchal de la Tour de la halte"] = 15579,
        ["Maréchal Frostwolf est"] = 15579,
        ["Maréchal Frostwolf ouest"] = 15579,
        ["Maître de guerre de Dun Baldar nord"] = 15579,
        ["Maître de guerre de Dun Baldar sud"] = 15579,
        ["Maître de guerre de l'Aile de glace"] = 15579,
        ["Maître de guerre de Stonehearth"] = 15579,
        ["Maître de guerre Frostwolf ouest"] = 15579,
        ["Myrmidon Strashaz"] = 15754,
        ["Pourfendeur Hederine"] = 15754,
        ["Capitaine Galvangar"] = 15754,
        ["Sculpteur gobelin"] = 5532,
        ["Capitaine Vertepeau"] = 5532,
        ["Bûcheron de la KapitalRisk"] = 5532,
        ["Scorpide ravageur"] = 5532,
        ["Lorgnesilex"] = 5532,
        ["Surveillant Blanche-moustache"] = 15663,
        ["Garde de Gouffrefer"] = 15663,
        ["Commandant Randolph"] = 15663,
        ["Commandant Dardosh"] = 15663,
        ["Commandant Malgor"] = 15663,
        ["Commandant Mulfort"] = 15663,
        ["Commandant Louis Philips"] = 15663,
        ["Commandant Mortimer"] = 15663,
        ["Commandant Duffy"] = 15663,
        ["Commandant Karl Philips"] = 15663,
        ["Garde de Gouffrefer aguerri"] = 15663,
        ["Garde de Gouffrefer vétéran"] = 15663,
        ["Garde de Gouffrefer champion"] = 15663,
        ["Commandant Chevaucheur de bélier Stormpike"] = 15663,
        ["Jotek"] = 15663,
        ["Généralissime Bolvar Fordragon"] = 20684,
        ["Herod"] = 22540,
    },
    ["Cape de gardien"] = {
        Professions = 7153,
    },
    ["Fureur sanguinaire"] = {
        Racial = 20572,
    },
    ["Pourriture noire"] = {
        ["Rat pestiféré"] = 16448,
    },
    ["Domination"] = {
        ["Grand croisé Dathrohan"] = 17405,
    },
    ["Lunette de précision"] = {
        Professions = 3979,
    },
    ["Robot d'alarme"] = {
        Professions = 23096,
    },
    ["Invocation d'un coffre rouillé"] = {
        ["Gelée pourrissante"] = 6464,
    },
    ["Acide d'Hakkar"] = {
        ["Rejeton d'Hakkar"] = 12280,
    },
    ["Fête - Saint-Valentin, cadeau donné"] = {
        ["Kalin Colportecoeur"] = 27663,
    },
    ["Invocation d'un squelette"] = {
        ["Squelette"] = 20464,
        ["Dame Sylvanas Windrunner"] = 20464,
        ["Squelette"] = 8853,
        ["Gardien Dragonmaw"] = 8853,
    },
    ["Potion de libre action"] = {
        Professions = 6624,
    },
    ["Emprise de la nature"] = {
        ["DRUID"] = 17329,
    },
    ["Cuirasse radieuse"] = {
        Professions = 16648,
    },
    ["Barrière de Nefarian"] = {
        ["Seigneur Victor Nefarius"] = 22663,
    },
    ["Dague d'acier luisante"] = {
        Professions = 15972,
    },
    ["Rouleau d'étoffe de soie"] = {
        Professions = 3839,
    },
    ["Flèches multiples"] = {
        ["HUNTER"] = 25294,
        ["Dame Sylvanas Windrunner"] = 20735,
        ["Mécanicien de la KapitalRisk"] = 14443,
        ["Défenseur lépreux"] = 14443,
    },
    ["Poison nécrotique"] = {
        ["Jeune araignée de Maexxna"] = 28776,
    },
    ["Cuirasse barbare en fer"] = {
        Professions = 9813,
    },
    ["Passe-partout en arcanite"] = {
        Professions = 19669,
    },
    ["Elixir de Respiration aquatique"] = {
        Professions = 7179,
    },
    ["Souffle nauséabond"] = {
        ["Ysondre"] = 24818,
        ["Léthon"] = 24818,
        ["Emeriss"] = 24818,
        ["Taerar"] = 24818,
    },
    ["Soins rapides"] = {
        ["PRIEST"] = 10917,
    },
    ["Bottes en étoffe cendrée"] = {
        Professions = 12088,
    },
    ["Ench. d'arme (Tueur de démons)"] = {
        Professions = 13915,
    },
    ["Teinture fantôme"] = {
        Professions = 11473,
    },
    ["Choc terrestre"] = {
        ["Ancien chien du Magma"] = 19364,
    },
    ["Comète, JAUNE GRANDE"] = {
        ["Type des feux d'artifice de Pat - GRAND JAUNE"] = 26356,
    },
    ["Apparition de drakônide bleu"] = {
        ["Drakônide bleu"] = 22658,
        ["Blue Drakonid Spawner"] = 22658,
    },
    ["Exploiter faiblesse"] = {
        ["Voleur Sans-racines"] = 8355,
        ["Messager Plaie-de-nuit"] = 6595,
        ["Pilleur de tombes"] = 6595,
        ["Pilleur défias"] = 6595,
        ["Géniteur traquelune"] = 6595,
        ["Volplume Ensorçaile"] = 6595,
        ["Embusquée Ensorçaile"] = 6595,
        ["Voleur de Xavian"] = 6595,
        ["Voleur Gangremusc"] = 6595,
        ["Volplume hurlante"] = 6595,
        ["Traqueur des ténèbres Fielfurie"] = 6595,
    },
    ["Disparition de Nagmara"] = {
        ["Gouvernante Nagmara"] = 15341,
        ["Soldat Rocknot"] = 15341,
    },
    ["Marteau de courroux"] = {
        ["PALADIN"] = 24239,
    },
    ["Cape en étoffe lumineuse"] = {
        Professions = 18420,
    },
    ["Epaulières en étoffe runique"] = {
        Professions = 18449,
    },
    ["Gilet en cuir estampé"] = {
        Professions = 2160,
    },
    ["Dynamite des Mortemines"] = {
        ["Dynamiteur mort-vivant"] = 7395,
    },
    ["Robe cramoisie en soie"] = {
        Professions = 8802,
    },
    ["Potion de protection contre le Givre supérieure"] = {
        Professions = 17575,
    },
    ["Pantalon en anneaux de cuivre"] = {
        Professions = 2662,
    },
    ["Robe blanche en laine"] = {
        Professions = 8467,
    },
    ["Gants terrestres lourds"] = {
        Professions = 9149,
    },
    ["Invocation de Rend Blackhand"] = {
        ["Chef de guerre Rend Blackhand"] = 16328,
        ["Gyth"] = 16328,
    },
    ["Sac en gangrétoffe du Magma"] = {
        Professions = 26087,
    },
    ["Pistage des camouflés"] = {
        ["HUNTER"] = 19885,
    },
    ["Pantalon runique en cuivre"] = {
        Professions = 3324,
    },
    ["Invocation de larve Gorishi"] = {
        ["Larve Gorishi"] = 14206,
        ["Oeuf Gorishi"] = 14206,
    },
    ["Focalisation chaotique"] = {
        ["Lige de la corruption Follengeance"] = 22418,
    },
    ["Eclair des arcanes chargé"] = {
        ["Prince Raze"] = 16570,
    },
    ["Ench. de gants (Force)"] = {
        Professions = 13887,
    },
    ["Familier Skullsplitter"] = {
        ["Panthère Skullsplitter"] = 3621,
        ["Chasseur Skullsplitter"] = 3621,
        ["Belluaire Skullsplitter"] = 3621,
    },
    ["Brassards en écailles de murloc"] = {
        Professions = 6705,
    },
    ["Mains couvertes de mousse"] = {
        ["Furtif lacustre"] = 6866,
        ["Ancien Furtif lacustre"] = 6866,
        ["Mange-mousse Sombretaillis"] = 6866,
    },
    ["Cuirasse en écailles de tortue"] = {
        Professions = 10511,
    },
    ["Huile de mana mineure"] = {
        Professions = 25125,
    },
    ["Invocation d'un marcheur du Vide"] = {
        ["WARLOCK"] = 697,
    },
    ["Défense de Proudmoore"] = {
        ["Archimage Tervosh"] = 7120,
    },
    ["Présence spirituelle"] = {
        ["MAGE"] = 12043,
    },
    ["Repousser au loin"] = {
        ["Teremus le Dévoreur"] = 11130,
        ["Fils d’Arkkoroc"] = 10101,
        ["Ursangous"] = 10101,
        ["Cyclonien"] = 18670,
        ["Géant de lave"] = 18945,
    },
    ["Tir automatique"] = {
        ["HUNTER"] = 75,
    },
    ["Ench. de bracelets (Régénération de mana)"] = {
        Professions = 23801,
    },
    ["Venin de rampant"] = {
        ["Rampant des cavernes"] = 14532,
    },
    ["Invocation d'un clampant"] = {
        ["Clampant"] = 8656,
        ["Chasseur murloc"] = 8656,
    },
    ["Bouclier de feu II"] = {
        ["Dragonnet cramoisi"] = 184,
        ["Géologue de la KapitalRisk"] = 184,
        ["Incendiaire Brisepoing"] = 184,
        ["Garneg Charskull"] = 184,
        ["Gangregarde inférieur"] = 184,
        ["Ravageur ardent"] = 184,
        ["Invocateur Blackrock"] = 184,
        ["Contremaître Cozzle"] = 184,
        ["Sœur Riven"] = 184,
    },
    ["Détruire la Bannière de Karang"] = {
        ["Vile-sylve enragé"] = 20786,
    },
    ["Invocation d'un gardien d'eau"] = {
        ["Gardien d'eau"] = 8372,
        ["Aquamancien du crépuscule"] = 8372,
    },
    ["Grand sac d'enchantement"] = {
        Professions = 27660,
    },
    ["Epaulières en fer émeraude"] = {
        Professions = 3504,
    },
    ["Petit sac en soie"] = {
        Professions = 3813,
    },
    ["Invocation d'un oeuf de poule"] = {
        ["Poulet"] = 13563,
    },
    ["Gardien guérisseur"] = {
        ["Gardien Guérisseur V"] = 4971,
        ["Totémique de Tranchebauge"] = 4971,
        ["Sage de la Tête de Mort"] = 4971,
        ["Prophète de la Tête de Mort"] = 4971,
        ["Gardien Guérisseur"] = 5605,
        ["Sorcier-docteur Bloodscalp"] = 5605,
        ["Rôdeur lacustre Aileron noir"] = 5605,
        ["Totémique Pin-tordu"] = 5605,
        ["Totémique Noirbois"] = 5605,
        ["Sorcier-docteur Witherbark"] = 5605,
        ["Chaman Cavepierre"] = 5605,
        ["Ancienne mystique Tranchegroin"] = 5605,
        ["Oracle Ondulame"] = 5605,
        ["Marteleur du Totem sinistre"] = 5605,
        ["Grand gardien guérisseur"] = 11899,
        ["Chaman Mosh'Ogg"] = 11899,
        ["Sorcier-docteur Sandfury"] = 11899,
        ["Gardien Guérisseur IV"] = 6274,
        ["Oracle Branchie-bleue"] = 6274,
        ["Prophète Tranchecrin"] = 6274,
        ["Totémique Crin-de-Chardon"] = 6274,
    },
    ["Poison d'affliction"] = {
        ["Scorpide terrifiant"] = 13884,
        ["Scorpide ravageur"] = 13884,
    },
    ["Invocation d'explosions de mana"] = {
        ["Explosion de mana"] = 22939,
        ["Monstruosité résiduelle"] = 22939,
    },
    ["Nova de givre"] = {
        ["MAGE"] = 10230,
        ["Baron Aquanis"] = 15531,
        ["Sorcière de Strashaz"] = 15532,
        ["Chasse-marée murloc"] = 11831,
        ["Eliza"] = 11831,
        ["Balgaras le Crasseux"] = 11831,
        ["Maître des flots Aileron noir"] = 11831,
        ["Baron Vardus"] = 11831,
        ["Magus Rochepoing"] = 11831,
        ["Kuz"] = 11831,
        ["Ensorceleuse Irequeue"] = 11831,
        ["Elémentaire d’eau souillé"] = 11831,
        ["Sirène Fouette-bile"] = 11831,
        ["Maréen"] = 11831,
        ["Chasse-marée Aileron noir"] = 12748,
        ["Sourcier Dos-hirsute"] = 12748,
        ["Vengeur"] = 14907,
    },
    ["Fureur du tigre"] = {
        ["DRUID"] = 9846,
    },
    ["Invocation du visage de Gunther"] = {
        ["Visage de Gunther"] = 7762,
    },
    ["Bénédiction de sagesse"] = {
        ["PALADIN"] = 25290,
    },
    ["Maillet"] = {
        ["Ursal le Mutileur"] = 15793,
        ["Marteleur Crocs acérés"] = 15793,
        ["Mutilateur de la griffe colérique"] = 15793,
        ["Seigneur Ror"] = 15793,
        ["Grand chef Tombe-hiver"] = 15793,
    },
    ["Pureté"] = {
        ["Charlga Trancheflanc"] = 8361,
    },
    ["Lance-filet automatique gnome"] = {
        Professions = 12902,
    },
    ["Vision de mage"] = {
        ["Archimage Ansirem Runeweaver"] = 3659,
    },
    ["Lunettes de feu"] = {
        Professions = 12594,
    },
    ["Morsure féroce"] = {
        ["DRUID"] = 31018,
    },
    ["Tunique tisse-givre"] = {
        Professions = 18403,
    },
    ["Ombre perforante"] = {
        ["Exhalombre Thuzadin"] = 16429,
    },
    ["Hachette ornée en thorium"] = {
        Professions = 16969,
    },
    ["Ceinture en peau de raptor"] = {
        Professions = 4097,
    },
    ["Assommer"] = {
        ["ROGUE"] = 11297,
    },
    ["Tétanos"] = {
        ["Golem des moissons rouillé"] = 8014,
        ["Busard des montagnes"] = 8014,
        ["Maraudeur kolkar"] = 8014,
        ["Envahisseur kolkar"] = 8014,
    },
    ["Bâtonnet en arcanite"] = {
        Professions = 20201,
    },
    ["Invocation d'une illusion lupine"] = {
        ["Illusion lupine"] = 7132,
        ["Horreur Lupine"] = 7132,
    },
    ["Réapprovisionnement"] = {
        ["Fournisseur Sombrefer"] = 4961,
    },
    ["Gants de maîtrise magique"] = {
        Professions = 18454,
    },
    ["Ench. de bracelets (Force supérieure)"] = {
        Professions = 13939,
    },
    ["Leurre - Evénement 001"] = {
        ["Leurre de l'équipe de mortier"] = 18634,
        ["Eclaireur Klemmy"] = 18634,
    },
    ["Super rayon réducteur"] = {
        ["Grand Bricoleur Mekkatorque"] = 22742,
    },
    ["Gantelets lourds en mithril"] = {
        Professions = 9928,
    },
    ["Reddition"] = {
        ["Hulfnar Stonetotem"] = 17648,
    },
    ["Création de Pierre d'âme (inférieure)"] = {
        ["WARLOCK"] = 20752,
    },
    ["Toxine mortelle"] = {
        ["Tranchecoeur"] = 8256,
    },
    ["Huile d'immolation"] = {
        Professions = 11451,
    },
    ["Corruption de la terre"] = {
        ["Emeriss"] = 24910,
    },
    ["Invocation d'une jade de mana"] = {
        ["MAGE"] = 3552,
    },
    ["Cape de dragonnet noir"] = {
        Professions = 9070,
    },
    ["Transmutation de la Terre en Vie"] = {
        Professions = 17566,
    },
    ["Peau durcie"] = {
        ["Tréant pétrifié"] = 22693,
    },
    ["Ench. de bottes (Endurance supérieure)"] = {
        Professions = 20020,
    },
    ["Armure épaisse de murloc"] = {
        Professions = 6704,
    },
    ["Châtiment sacré"] = {
        ["Géomètre de Froidemine"] = 15498,
        ["Géomètre de Gouffrefer"] = 15498,
        ["Géomètre de Froidemine vétéran"] = 15498,
        ["Géomètre de Gouffrefer vétéran"] = 15498,
        ["Géomètre de Gouffrefer champion"] = 15498,
        ["Oracle murloc"] = 9734,
        ["Sirène Tempécaille"] = 9734,
        ["Prêtre de Dun Garok"] = 9734,
        ["Protectrice de Theramore"] = 9734,
        ["Chercheur Réprouvé"] = 9734,
        ["Disciple écarlate"] = 9734,
        ["Adepte écarlate"] = 9734,
        ["Clerc de Rempart-du-Néant"] = 9734,
        ["Prêtre du corps expéditionnaire"] = 9734,
    },
    ["Chaperon des ombres"] = {
        Professions = 3858,
    },
    ["Mortier animé"] = {
        ["Courtebottes"] = 18655,
    },
    ["Marcheur du Vide"] = {
        ["Sbire marcheur du Vide"] = 5108,
        ["Invocateur défias"] = 5108,
        ["Démoniste lige d'ombre"] = 5108,
    },
    ["Baiser de la Tentatrice"] = {
        ["Simona la Discrète"] = 23205,
    },
    ["Flammes sacrées"] = {
        ["PRIEST"] = 15261,
    },
    ["Rétablissement"] = {
        ["DRUID"] = 9858,
        ["Oracle sélénien"] = 16561,
    },
    ["Chapelet de grandes comètes vertes"] = {
        Professions = 26427,
    },
    ["Sapeur explosion"] = {
        ["Saboteur Sombrefer"] = 3204,
        ["Sapeur Sombrefer"] = 3204,
    },
    ["Peste bouillonnante"] = {
        ["Paysan contaminé"] = 23072,
    },
    ["Brassards en sombrefer"] = {
        Professions = 20874,
    },
    ["Cuirasse en écailles de dragon vert"] = {
        Professions = 19050,
    },
    ["Etoffe lunaire"] = {
        Professions = 18560,
    },
    ["Elémentaires de Sarilus"] = {
        ["Gardien des eaux mineur"] = 6490,
        ["Sarilus Foulborne"] = 6490,
    },
    ["Ceinture de prévention gnome"] = {
        Professions = 12903,
    },
    ["Lunettes roses"] = {
        Professions = 12618,
    },
    ["Interception"] = {
        ["WARRIOR"] = 20617,
    },
    ["Toucher de Vaelastrasz"] = {
        ["Vaelastrasz le rouge"] = 16319,
    },
    ["Créer un Tubercule"] = {
        ["Ecureuil Sniffetarin"] = 6900,
    },
    ["Poignée de boulons en cuivre"] = {
        Professions = 3922,
    },
    ["Attaque sournoise"] = {
        ["ROGUE"] = 25300,
        ["Thuros Doigts-agiles"] = 7159,
        ["Eclaireur défias"] = 7159,
        ["Morgan le collecteur"] = 7159,
        ["Détrousseur défias"] = 7159,
        ["Pillard défias"] = 7159,
        ["Charognard Bloodscalp"] = 7159,
        ["Rôdeur murloc"] = 7159,
        ["Embusqué Mâcheroc"] = 7159,
        ["Ecorcheur Frostmane"] = 7159,
        ["Marche-boue Aileron noir"] = 7159,
        ["Captif défias"] = 7159,
        ["Détrousseur du Syndicat"] = 7159,
        ["Larron du Syndicat"] = 7159,
        ["Espion du Syndicat"] = 7159,
        ["Intrus Réprouvé"] = 7159,
        ["Analyste de Rempart-du-Néant"] = 7159,
        ["Traqueur de Tranchebauge"] = 7159,
        ["Garde du corps défias"] = 7159,
        ["Embusquée Brûleglace"] = 7159,
    },
    ["Tunique élégante en cuir"] = {
        Professions = 3761,
    },
    ["Ordre frénétique"] = {
        ["Surveillant Rivepatte"] = 3136,
        ["Sergent Brashclaw"] = 3136,
    },
    ["Flammes des dragons noirs"] = {
        ["Brandeguerre"] = 16054,
    },
    ["Bave de Ver"] = {
        ["Dévoreur putride"] = 17197,
    },
    ["Contact en argent"] = {
        Professions = 3973,
    },
    ["Recombobulateur mineur"] = {
        Professions = 3952,
    },
    ["Brûlure de l'ombre"] = {
        ["WARLOCK"] = 18871,
    },
    ["Cape glaciaire"] = {
        Professions = 28208,
    },
    ["Invocation du totem de lavage de cerveau"] = {
        ["Totem de lavage de cerveau"] = 24262,
    },
    ["Leurre ouvragé"] = {
        Professions = 19814,
    },
    ["Invocation d'un élémentaire de feu"] = {
        ["Elémentaire de feu"] = 8985,
        ["Conjurateur écarlate"] = 8985,
    },
    ["Apaisement"] = {
        ["PRIEST"] = 10953,
    },
    ["Cuirasse de plumes"] = {
        Professions = 10647,
    },
    ["Flammes de Shahram"] = {
        ["Shahram"] = 16596,
    },
    ["Fonte de l'étain"] = {
        Professions = 3304,
    },
    ["Fiole de poison"] = {
        ["Fourrageur Rat des tunnels"] = 7365,
    },
    ["Piétinement d’Azrethoc"] = {
        ["Image du seigneur Azrethoc"] = 7961,
    },
    ["Ench. de bracelets (Force)"] = {
        Professions = 13661,
    },
    ["Chemise rose en tisse-mage"] = {
        Professions = 12080,
    },
    ["Cape en laine"] = {
        Professions = 2402,
    },
    ["Bicorne d'amiral"] = {
        Professions = 12081,
    },
    ["Peau tranchante"] = {
        ["Mangletooth"] = 16610,
    },
    ["Balafre"] = {
        ["Moissonneur"] = 7342,
        ["Flagellant déviant"] = 7342,
    },
    ["Invocation de tréants alliés"] = {
        ["Tréant allié"] = 20702,
        ["Archidruide Fandral Staghelm"] = 20702,
    },
    ["Ceinture impériale en plaques"] = {
        Professions = 16647,
    },
    ["Souffle de givre"] = {
        ["Edan le Hurleur"] = 3129,
        ["Jeune wendigo"] = 3131,
        ["Wendigo"] = 3131,
        ["Yéti des montagnes"] = 3131,
        ["Yéti géant"] = 3131,
    },
    ["Epée courte en cuivre"] = {
        Professions = 2739,
    },
    ["Gantelets tempêtes"] = {
        Professions = 16661,
    },
    ["Le Brave reprend forme humaine"] = {
        ["Johnson \"le Brave\""] = 9192,
    },
    ["Fonte de l'argent"] = {
        Professions = 2658,
    },
    ["Toucher divin de Vaelastrasz"] = {
        ["Vaelastrasz le rouge"] = 16332,
    },
    ["Ceinture azur en soie"] = {
        Professions = 8766,
    },
    ["Rapetisser"] = {
        ["Réducteur de tête de Kurzen"] = 7289,
        ["Zalazane"] = 7289,
        ["Chaman noir du crépuscule"] = 7289,
    },
    ["Appel d'un Destructeur Infernal"] = {
        ["Poulet de Nouvel An"] = 23056,
        ["Niby le Tout-puissant"] = 23056,
    },
    ["Hurlement furieux"] = {
        Pet = 24597,
        ["Chef de meute coyote"] = 3149,
        ["Dogue Ravageur noir"] = 3149,
    },
    ["Salve de guérison"] = {
        ["SHAMAN"] = 10623,
        ["Aggem Mantépine"] = 14900,
    },
    ["Balles perçantes en mithril"] = {
        Professions = 12596,
    },
    ["Linceul de toiles"] = {
        ["Rampant des cryptes"] = 15471,
        ["Surveillant nérubien"] = 15471,
    },
    ["Cuir épais"] = {
        Professions = 20650,
    },
    ["Dévoreur de chair"] = {
        ["Grinceur"] = 3393,
        ["Charognard des hautes-terres"] = 3393,
    },
    ["Gyrochronatome"] = {
        Professions = 3961,
    },
    ["Création de Pierre de feu (majeure)"] = {
        ["WARLOCK"] = 17953,
    },
    ["Gants d'Inferno"] = {
        Professions = 22868,
    },
    ["Séduction"] = {
        Pet = 6358,
    },
    ["Projection aquatique"] = {
        ["Chevaucheur des vagues Tempécaille"] = 13586,
        ["Seigneur Salvassio"] = 13586,
    },
    ["Robe d'adepte supérieur"] = {
        Professions = 7643,
    },
    ["Apparition de drakônide bronze"] = {
        ["Drakônide bronze"] = 22657,
        ["Bronze Drakonid Spawner"] = 22657,
    },
    ["Détérioration"] = {
        ["Fel'zerul"] = 7437,
    },
    ["Caillou lourd"] = {
        Professions = 3117,
    },
    ["Attaques circulaires"] = {
        ["WARRIOR"] = 12292,
        ["Assemblage ressuscité"] = 18765,
        ["Drek'Thar"] = 18765,
    },
    ["Gants de gardien"] = {
        Professions = 7156,
    },
    ["Elixir des arcanes"] = {
        Professions = 11461,
    },
    ["Poussière projetée"] = {
        ["Gueule d'acier des oasis"] = 6530,
    },
    ["Cuisine de Macaron"] = {
        ["Macaron"] = 5174,
    },
    ["Bannissement de l'Ecaille"] = {
        ["Seigneur Victor Nefarius"] = 16404,
    },
    ["Choc"] = {
        ["Géant de lave"] = 18944,
    },
    ["Dissuasion"] = {
        ["HUNTER"] = 19263,
    },
    ["Transmutation : Feu élémentaire"] = {
        Professions = 25146,
    },
    ["Ench. de cape (Résistance au Feu inf.)"] = {
        Professions = 7861,
    },
    ["Hystérie ancienne"] = {
        ["Ancien chien du Magma"] = 19372,
    },
    ["Caillou grossier"] = {
        Professions = 3116,
    },
    ["Poison Pétale-de-sang"] = {
        ["Flagellant Pétale-de-sang"] = 14110,
        ["Ecorcheur Pétale-de-sang"] = 14110,
        ["Batteur Pétale-de-sang"] = 14110,
        ["Trappeur Pétale-de-sang"] = 14110,
    },
    ["Gardien de givre rapide"] = {
        ["Oracle murloc"] = 4980,
        ["Mystique Skullsplitter"] = 4980,
        ["Gardien squelette"] = 4980,
        ["Chasse-marée Aileron noir"] = 4980,
        ["Sorcier de Dalaran"] = 4980,
        ["Ensorceleuse Irequeue"] = 4980,
        ["Dame Vespia"] = 4980,
    },
    ["Urticaire"] = {
        ["Chouettard éreinté"] = 15848,
    },
    ["Piège d'Immolation"] = {
        ["HUNTER"] = 14305,
    },
    ["Ench. de bottes (Agilité supérieure)"] = {
        Professions = 20023,
    },
    ["Bâtonnet runique en or"] = {
        Professions = 13628,
    },
    ["Arme de givre"] = {
        ["SHAMAN"] = 16356,
    },
    ["Eau"] = {
        ["Geyser massif"] = 22422,
    },
    ["Prompte guérison"] = {
        ["DRUID"] = 18562,
    },
    ["Peau de pierre"] = {
        ["Golem lézardé"] = 5810,
        ["Béhémoth de pierre"] = 5810,
        ["Prophétesse Ravenfeather"] = 8314,
        ["Kranal Fiss"] = 8314,
    },
    ["Totem de Tranquillité de l'air"] = {
        ["SHAMAN"] = 25908,
    },
    ["Echo de rugissement"] = {
        ["Seigneur de guerre Gordunni"] = 10967,
        ["Grand maréchal Afrasiabi"] = 10967,
    },
    ["Huile de mana inférieure"] = {
        Professions = 25127,
    },
    ["Création de Pierre de sort (majeure)"] = {
        ["WARLOCK"] = 17728,
    },
    ["Marteau de guerre enchanté"] = {
        Professions = 16973,
    },
    ["Réduit en gravats"] = {
        ["Elémentaire asservi"] = 3673,
    },
    ["Cuirasse en écailles de dragon bleu"] = {
        Professions = 19077,
    },
    ["Souffle foudroyant"] = {
        ["Sorcière des mers Irequeue"] = 8598,
    },
    ["Peste galopante"] = {
        ["Répandeur de peste"] = 3436,
        ["Hyène Pestepoil"] = 3436,
    },
    ["Potion de vive action"] = {
        Professions = 24367,
    },
    ["Silex à aiguiser solide"] = {
        Professions = 9920,
    },
    ["Lancer Feu liquide"] = {
        ["Gryphon du Nid-de-l'Aigle"] = 23969,
        ["Chevaucheur de guerre"] = 23969,
    },
    ["Trait de l'ombre"] = {
        ["WARLOCK"] = 25307,
        ["Morloch"] = 15537,
        ["Tisseur d'ombre Sombrepoil"] = 9613,
        ["Exhalombre Blackrock"] = 9613,
        ["Tisseur d'ombre Plaie-de-nuit"] = 9613,
        ["Soigneur squelette"] = 9613,
        ["Réducteur de tête de Kurzen"] = 9613,
        ["Sorcier-docteur Zanzil"] = 9613,
        ["Fée des ombres"] = 9613,
        ["Conjurateur du Syndicat"] = 9613,
        ["Darbel Montrose"] = 9613,
        ["Augure écarlate"] = 9613,
        ["Clairvoyant écarlate"] = 9613,
        ["Nécrorateur Jargba"] = 9613,
        ["Prêtre de la Tête de Mort"] = 9613,
        ["Mage de sang Thalnos"] = 9613,
        ["Invocateur infernal Sans-racines"] = 9613,
        ["Ombremage du crépuscule"] = 9613,
        ["Gangretisseur Scornn"] = 9613,
        ["Nécromancienne de la Tête de Mort"] = 9613,
        ["Sectateur de la Tête de Mort"] = 9613,
        ["Flagellante de Mannoroc"] = 9613,
        ["Nécromancien renégat"] = 20298,
        ["Exhalombre Frostmane"] = 20791,
        ["Conjurateur de Dalaran"] = 20791,
        ["Licillin"] = 20791,
        ["Mage de l'ombre du Syndicat"] = 20791,
        ["Apprenti de la Lame ardente"] = 20791,
        ["Fizzle Darkstorm"] = 20791,
        ["Gazz'uz"] = 20791,
        ["Ilkrud Magthrull"] = 20791,
        ["Sectateur de la Rive noire"] = 20791,
        ["Adepte de la Rive noire"] = 20791,
        ["Apothicaire Falthis"] = 20791,
        ["Balizar l’Ombrageux"] = 20791,
        ["Démoniste de la Lame brûlante"] = 20791,
        ["Veilleur des ombres Dragonmaw"] = 20807,
        ["Anéantisseur de la Rive noire"] = 20807,
        ["Augure de la Lame ardente"] = 20807,
        ["Herod la Paperasse"] = 20816,
        ["Mage de l'ombre d'Argus"] = 20816,
        ["Exhalombre Witherbark"] = 20816,
        ["Exhalombre Sombrefer"] = 20816,
        ["Ombremage de la Lame ardente"] = 20816,
        ["Démoniste de la Voile sanglante"] = 20825,
        ["Acolyte Squelette"] = 20825,
        ["Tisseur d’ombre Ombreforge"] = 20825,
        ["Invocateur de la Lame ardente"] = 20825,
        ["Initiée Hederine"] = 15232,
    },
    ["Le Briseur"] = {
        Professions = 10003,
    },
    ["Siphon d’âme"] = {
        ["Azshir le Sans-sommeil"] = 9373,
        ["Serviteur Ravenclaw"] = 7290,
        ["Fizzle Darkstorm"] = 7290,
    },
    ["Potion de soins inférieure"] = {
        Professions = 2337,
    },
    ["Rage de Thule"] = {
        ["Snarlmane"] = 3387,
    },
    ["Corrompt les Forces de la nature"] = {
        ["Force de la Nature corrompue"] = 21968,
    },
    ["Peau robuste traitée"] = {
        Professions = 19047,
    },
    ["Rayon de l'Oeil"] = {
        ["Œil de C'Thun"] = 26134,
    },
    ["Epine infectieuse"] = {
        ["Lanceur flétri"] = 12245,
    },
    ["Cri de ralliement du tueur de dragon"] = {
        ["Seigneur Runthak"] = 22888,
        ["Major Mattingly"] = 22888,
        ["Haut seigneur Saurfang"] = 22888,
        ["Grand maréchal Afrasiabi"] = 22888,
    },
    ["Gantelets corrompus en cuir"] = {
        Professions = 19049,
    },
    ["Gardien guérisseur excellent"] = {
        ["Gardien guérisseur supérieur"] = 15869,
        ["Sorcier-docteur Smolderthorn"] = 15869,
        ["Porte-parole Witherbark"] = 15869,
    },
    ["Eclair toxique"] = {
        ["Fléau"] = 16554,
    },
    ["Courroux de l'aube"] = {
        Professions = 16970,
    },
    ["Poison de drain mortel"] = {
        ["Veuve sanguine"] = 3388,
        ["Ailes du désespoir"] = 3388,
    },
    ["Hache en bronze"] = {
        Professions = 2741,
    },
    ["Petit fumant"] = {
        Professions = 15633,
    },
    ["Flamme de l'âme"] = {
        ["Nelson le Gentil"] = 23272,
    },
    ["Boutoir"] = {
        ["Sanglier de guerre Dos-hirsute"] = 3385,
        ["Sanglier tacheté redoutable"] = 3385,
        ["Ancien sanglier tacheté"] = 3385,
        ["Sanglier tacheté corrompu"] = 3385,
    },
    ["Invocation d'une hyène apprivoisée"] = {
        ["Hyène apprivoisée"] = 8276,
        ["Belluaire de Tranchebauge"] = 8276,
    },
    ["Vide des arcanes"] = {
        ["Azuregos"] = 21147,
    },
    ["Gilet chimérique"] = {
        Professions = 19081,
    },
    ["Jambières en écailles dorées"] = {
        Professions = 3507,
    },
    ["Radiation"] = {
        ["Envahisseur irradié"] = 9798,
        ["Pilleur irradié"] = 9798,
    },
    ["Lorica en cuivre"] = {
        Professions = 3321,
    },
    ["Kilt simple"] = {
        Professions = 12046,
    },
    ["Coup bas"] = {
        ["ROGUE"] = 1833,
    },
    ["Tremblement de terre"] = {
        ["Furie-de-pierre"] = 6524,
        ["Exilé grondant"] = 6524,
        ["Fozruk"] = 6524,
        ["Grondeur"] = 6524,
        ["Eboulide fulminant"] = 6524,
        ["Tremble-terre de Tranchebauge"] = 6524,
        ["Régisseur des pierres"] = 6524,
        ["Atal'alarion"] = 6524,
        ["Avalanchion"] = 6524,
        ["Gri'lek"] = 6524,
        ["Le duc des Eclats"] = 6524,
        ["Seigneur-magma Bokk"] = 6524,
    },
    ["Destructeur en fer doré"] = {
        Professions = 3495,
    },
    ["Pacte de sang"] = {
        Pet = 11767,
    },
    ["Ench. de bracelets (Esprit supérieur)"] = {
        Professions = 13846,
    },
    ["Aiguillon perfide"] = {
        ["ROGUE"] = 8643,
    },
    ["Fléau du venin"] = {
        ["Kayneth Stillwind"] = 6354,
    },
    ["Ench. de plastron (Absorption mineure)"] = {
        Professions = 7426,
    },
    ["Résistance à l'Ombre"] = {
        ["HUNTER"] = 24516,
    },
    ["Lourde épée large en cuivre"] = {
        Professions = 3292,
    },
    ["Frappe héroïque"] = {
        ["WARRIOR"] = 25286,
    },
    ["Elixir de la Mangouste"] = {
        Professions = 17571,
    },
    ["Attaque du raptor"] = {
        ["HUNTER"] = 14266,
    },
    ["Faucher la foule"] = {
        ["Ograbisi"] = 10887,
        ["Setis"] = 10887,
    },
    ["Comète, ROUGE"] = {
        ["Fêtard de Stormwind"] = 26347,
        ["Fêtard de Thunder Bluff"] = 26347,
        ["Type des feux d'artifice de Pat - ROUGE"] = 26347,
        ["Fêtard de Darnassus"] = 26347,
        ["Fêtard d'Ironforge"] = 26347,
        ["Fêtard d'Undercity"] = 26347,
        ["Fêtard d'Orgrimmar"] = 26347,
    },
    ["Bracelets impériaux en plaques"] = {
        Professions = 16649,
    },
    ["Coup de queue"] = {
        ["Ysondre"] = 15847,
        ["Léthon"] = 15847,
        ["Emeriss"] = 15847,
        ["Taerar"] = 15847,
    },
    ["Crachat de flammes"] = {
        ["Roc de feu"] = 11021,
    },
    ["Quête - Impact de métamorphose"] = {
        ["Yeh'kinya"] = 24180,
    },
    ["Ench. d'arme 2M (Intelligence inférieure)"] = {
        Professions = 7793,
    },
    ["Cape en cuir estampé"] = {
        Professions = 2162,
    },
    ["Invocation d'eau"] = {
        ["MAGE"] = 10140,
    },
    ["Robe marron en lin"] = {
        Professions = 7623,
    },
    ["Poison faible"] = {
        ["Jeune Tisse-nuit"] = 6751,
        ["Araignée Tisse-nuit"] = 6751,
        ["Araignée Tissebois"] = 6751,
        ["Scorpide ouvrier"] = 6751,
    },
    ["Surmultiplicateur"] = {
        ["Déchireur Warsong"] = 18546,
    },
    ["Chemise bleue en lin"] = {
        Professions = 2394,
    },
    ["Braise du Gardien de l'homme d'osier"] = {
        ["Gardien de l'homme d'osier"] = 25007,
    },
    ["Bottes en vignesang"] = {
        Professions = 24093,
    },
    ["Aura de hâte"] = {
        ["Surveillant Ragenclume"] = 13589,
        ["Surveillant Blanche-moustache"] = 13589,
    },
    ["Epaulières barbares"] = {
        Professions = 7151,
    },
    ["Potion de sauvageonne"] = {
        Professions = 11458,
    },
    ["Ench. de gants (Herboristerie avancée)"] = {
        Professions = 13868,
    },
    ["Tempête de grêle"] = {
        ["Mécano-Glaceur"] = 10734,
    },
    ["Poison douloureux"] = {
        ["ROGUE"] = 13220,
    },
    ["Cape en écailles de déviant"] = {
        Professions = 7953,
    },
    ["Colère"] = {
        ["DRUID"] = 9912,
        ["Guide Pin-tordu"] = 9739,
        ["Tanneur crin-pâle"] = 9739,
        ["Druide austère"] = 9739,
        ["Botaniste cénarien"] = 9739,
        ["Tyranis Malem"] = 9739,
        ["Mystique Pin-tordu"] = 9739,
        ["Oracle sélénien"] = 9739,
    },
    ["Morsure lacérante"] = {
        ["Chien du Magma"] = 19771,
    },
    ["Suriner"] = {
        ["ROGUE"] = 11286,
    },
    ["Invocation d'une explosion de lave I"] = {
        ["Rat du Magma"] = 21907,
    },
    ["Invocation de garde funeste"] = {
        ["Serviteur de la Garde funeste"] = 22865,
        ["Démoniste Gordok"] = 22865,
    },
    ["Poing de Shahram"] = {
        ["Shahram"] = 16601,
    },
    ["Invocation d'une blatte mécanique"] = {
        ["Blatte mécanique"] = 10858,
        ["Techbot"] = 10858,
        ["Blatte mécanique"] = 10858,
    },
    ["Epaulières en cuir sauvage"] = {
        Professions = 10529,
    },
    ["Mantelet des Grumegueules"] = {
        Professions = 23663,
    },
    ["Invocation d'un chasseur corrompu"] = {
        ["WARLOCK"] = 691,
    },
    ["Pistage des bêtes"] = {
        ["HUNTER"] = 1494,
    },
    ["Piétiner"] = {
        ["Fouailleventre"] = 5568,
        ["Découpeur 4000"] = 5568,
        ["Gorlash"] = 5568,
        ["Piétineur sylvain"] = 5568,
        ["Furie-de-pierre"] = 5568,
        ["Fozruk"] = 5568,
        ["Kodo adulte"] = 5568,
        ["Matriarche kodo"] = 5568,
        ["Poinçonneur gelkis"] = 5568,
        ["Ghamoo-ra"] = 5568,
        ["Golem de guerre lourd"] = 5568,
        ["Servant d’Arkkoroc"] = 5568,
    },
    ["Pantalon en cuir léger"] = {
        Professions = 9068,
    },
    ["Volée de coups"] = {
        ["WARRIOR"] = 6554,
    },
    ["Caillou dense"] = {
        Professions = 16640,
    },
    ["Ench. de gants (Minage avancé)"] = {
        Professions = 13841,
    },
    ["Ench. de cape (Résistance)"] = {
        Professions = 13794,
    },
    ["Renverser"] = {
        ["Brute Rochepoing"] = 11428,
        ["Gangregarde"] = 11428,
        ["Flagglemurk le Cruel"] = 11428,
        ["Guerrier Gurubashi"] = 11428,
        ["Duriel Moonfire"] = 11428,
        ["Zzarc' Vul"] = 5164,
        ["Cogneur des Carmines"] = 5164,
        ["Sin'Dall"] = 5164,
        ["Briseur d’os Mâcheroc"] = 5164,
        ["Ogre Mo'grosh"] = 5164,
        ["Myrmidon Tempécaille"] = 5164,
        ["Esprit rocheux furieux"] = 5164,
        ["Grizzly de la griffe colérique"] = 5164,
        ["Gangregarde inférieur"] = 18812,
        ["Myrmidon Strashaz"] = 18812,
        ["Drek'Thar"] = 19128,
    },
    ["Force de Frostmane"] = {
        ["Grik'nir le Froid"] = 6957,
    },
    ["Créer un Coffre rempli"] = {
        ["Rejeton de la faille"] = 9010,
    },
    ["Embuscade"] = {
        ["ROGUE"] = 11269,
    },
    ["Lien spirituel"] = {
        ["WARLOCK"] = 19028,
    },
    ["Epaulières vertes en soie"] = {
        Professions = 8774,
    },
    ["Ench. de plastron (Caract. mineures)"] = {
        Professions = 13626,
    },
    ["Maladie térébrante"] = {
        ["Ver térébrant"] = 14535,
    },
    ["Fonte du thorium"] = {
        Professions = 16153,
    },
    ["Malédiction de Stalvan"] = {
        ["Stalvan Mistmantle"] = 3105,
        ["Esprit mélancolique"] = 3105,
    },
    ["Force de pierre"] = {
        ["Bouge-pierre Sombretaillis"] = 6864,
    },
    ["Baguette mystique supérieure"] = {
        Professions = 14810,
    },
    ["Fusée rouge"] = {
        Professions = 23066,
    },
    ["Agilité diminuée"] = {
        ["Crocilisque Gueule-d'effroi corrompu"] = 7901,
    },
    ["Fureur du combat"] = {
        ["Tharil'zun"] = 3631,
        ["Sous-chef Brisepoing"] = 3631,
        ["Régisseur de la KapitalRisk"] = 3631,
        ["Seigneur des Cisailles"] = 3631,
        ["Malgin Barleybrew"] = 3631,
        ["Bourreau du Marteau du crépuscule"] = 3631,
    },
    ["Consumer les ombres"] = {
        Pet = 17854,
    },
    ["Elixir d'Agilité inférieure"] = {
        Professions = 2333,
    },
    ["Bouclier divin"] = {
        ["PALADIN"] = 1020,
    },
    ["Effet inferno"] = {
        ["Infernal"] = 22703,
        ["Infernal corrompu"] = 22703,
    },
    ["Pare-flammes"] = {
        Professions = 3944,
    },
    ["Gantelets en fer émeraude"] = {
        Professions = 3336,
    },
    ["Bottes de rapidité"] = {
        Professions = 9208,
    },
    ["Lunette standard"] = {
        Professions = 3978,
    },
    ["Lunettes des ombres"] = {
        Professions = 3940,
    },
    ["Ouragan"] = {
        ["DRUID"] = 17402,
    },
    ["Célérité"] = {
        ["DRUID"] = 9821,
        Pet = 23110,
    },
    ["Brûlure"] = {
        ["MAGE"] = 10207,
    },
    ["Hurlement de Cinglenuit"] = {
        ["Cinglenuit"] = 3485,
    },
    ["Furie sanguinaire"] = {
        ["Sous-chef Snivvle"] = 16170,
        ["Hamhock"] = 6742,
        ["Mage Cassecrête"] = 6742,
        ["Grel'borg l'Avare"] = 6742,
        ["Lo'Grosh"] = 6742,
        ["Sanguinaire kolkar"] = 6742,
        ["Chaman Crin-de-Chardon"] = 6742,
        ["Maître-chien Loksey"] = 6742,
        ["Adepte de la Lame ardente"] = 6742,
    },
    ["Malédiction des Mort-bois"] = {
        ["Guerrier Mort-bois"] = 13583,
        ["Jardinier Mort-bois"] = 13583,
        ["Guide Mort-bois"] = 13583,
        ["Protecteur Mort-bois"] = 13583,
        ["Vengeur Mort-bois"] = 13583,
        ["Chaman Mort-bois"] = 13583,
        ["Chef de clan Saignegueule"] = 13583,
    },
    ["Limace des ténèbres"] = {
        ["Gelée noire"] = 3335,
        ["Limon corrompu"] = 3335,
        ["Bouillasseux"] = 3335,
    },
    ["Huile de mana brillante"] = {
        Professions = 25130,
    },
    ["Potion de Protection contre le Feu supérieure"] = {
        Professions = 17574,
    },
    ["Guérison des maladies"] = {
        ["PRIEST"] = 528,
        ["SHAMAN"] = 2870,
    },
    ["Familier Bloodscalp"] = {
        ["Tigre Bloodscalp"] = 3612,
        ["Belluaire Bloodscalp"] = 3612,
    },
    ["Rets enveloppants"] = {
        ["Rampant des plaines"] = 4962,
        ["Rampant des plaines géant"] = 4962,
        ["Bête des cryptes"] = 4962,
    },
    ["Lame de l'hiver"] = {
        Professions = 21913,
    },
    ["Linceul de vents"] = {
        ["Sirène Daguéchine"] = 6728,
        ["Imploratrice céleste Ensorçaile"] = 6728,
        ["Imploratrice céleste Rougefurie"] = 6728,
        ["Chasse-vent galak"] = 6728,
        ["Lève-poussière de Tranchebauge"] = 6728,
        ["Chasse-vent kolkar"] = 6728,
        ["Sœur Rathtalon"] = 6728,
    },
    ["Hache lourde en mithril"] = {
        Professions = 9993,
    },
    ["Potion de Restauration"] = {
        Professions = 11452,
    },
    ["Totem de rafale de flammes"] = {
        ["Totem de rafale de flammes"] = 15867,
        ["Sorcier-docteur Smolderthorn"] = 15867,
    },
    ["Gardien de l'ombre"] = {
        ["WARLOCK"] = 28610,
    },
    ["Enchantement de l’agate azur"] = {
        ["Apprenti Kryten"] = 4319,
    },
    ["Heaume en fer émeraude"] = {
        Professions = 3502,
    },
    ["Cri psychique"] = {
        ["PRIEST"] = 10890,
    },
    ["Cuirasse d'Ironforge"] = {
        Professions = 8367,
    },
    ["Brise-genou"] = {
        ["WARRIOR"] = 7373,
        ["Guerrier squelette"] = 9080,
        ["Brack"] = 9080,
        ["Guerrier Branchie-bleue"] = 9080,
        ["Lok Plaie-des-orcs"] = 9080,
        ["Ursa Vile-sylve"] = 9080,
        ["Aligar le Tourmenteur"] = 9080,
        ["Guerrier Strashaz"] = 9080,
        ["Guerrier Ondulame"] = 9080,
        ["Contremaître Grills"] = 9080,
        ["Lépreux perturbé"] = 9080,
        ["Envahisseur de Froidemine"] = 9080,
        ["Gardeffroi royal"] = 9080,
        ["Tentacule griffu"] = 26141,
    },
    ["Toucher de maladie"] = {
        ["Mort pourrissant"] = 3234,
        ["Cadavre décomposé"] = 3234,
        ["Mort affamé"] = 3234,
        ["Horreur titubante"] = 3234,
    },
    ["Gantelets ornés en cuivre"] = {
        Professions = 3325,
    },
    ["Tir tranquillisant"] = {
        ["HUNTER"] = 19801,
    },
    ["Poison de Peau-de-venin"] = {
        ["Ravasaure peau-de-venin"] = 14792,
    },
    ["Pantalon noir en cuir"] = {
        Professions = 7135,
    },
    ["Renvoi des morts-vivants"] = {
        ["PALADIN"] = 10326,
    },
    ["Ceinture de l'étoile"] = {
        Professions = 3864,
    },
    ["Détonation"] = {
        ["Arcaniste Doan"] = 9435,
        ["Mine terrestre sombrefer"] = 4043,
    },
    ["Lancement de torche"] = {
        ["Contremaître de Bael'dun"] = 6257,
    },
    ["Brassards guillochés en bronze"] = {
        Professions = 2672,
    },
    ["Baiser apaisant"] = {
        Pet = 11785,
    },
    ["Disparaître"] = {
        ["Grogneur"] = 5543,
    },
    ["Sombre offrande"] = {
        ["Marcheur du Vide d’Arugal"] = 7154,
    },
    ["Cri d’intimidation"] = {
        ["WARRIOR"] = 5246,
    },
    ["Vague de soins inférieurs"] = {
        ["SHAMAN"] = 10468,
    },
    ["Tir perforant"] = {
        ["Fusilier de Dun Garok"] = 6685,
        ["Sous-chef défias"] = 6685,
        ["Agent de Thaurissan"] = 6685,
        ["Guide Mort-bois"] = 6685,
        ["Tireur d'élite Ombreforge"] = 6685,
    },
    ["Nova sacrée"] = {
        ["PRIEST"] = 27801,
    },
    ["Gilet glaciaire"] = {
        Professions = 28207,
    },
    ["Marteau de la justice"] = {
        ["PALADIN"] = 10308,
    },
    ["Résistance au Givre"] = {
        ["HUNTER"] = 24478,
    },
    ["Bénédiction de salut supérieure"] = {
        ["PALADIN"] = 25895,
    },
    ["Croc-en-jambe"] = {
        ["Réfugié malade"] = 101,
        ["Tailleur d'Hillsbrad"] = 101,
    },
    ["Malédiction des éléments"] = {
        ["WARLOCK"] = 11722,
    },
    ["Colère d'Arantir"] = {
        ["Arantir"] = 9733,
    },
    ["Ench. de bouclier (Endurance mineure)"] = {
        Professions = 13378,
    },
    ["Elixir de défense excellente"] = {
        Professions = 17554,
    },
    ["Renvoyer le familier"] = {
        ["HUNTER"] = 2641,
    },
    ["Nova de feu"] = {
        ["Chaman Bloodscalp"] = 11969,
        ["Magistrat Burnside"] = 11969,
        ["Elémentaire de magma"] = 11970,
        ["Mécano-Brûleur"] = 11970,
        ["Taragaman l'Affameur"] = 11970,
        ["Mage de sang Thalnos"] = 12470,
    },
    ["Grondement puissant"] = {
        ["Vieil ours d’Ashenvale"] = 4148,
        ["Ours d’Ashenvale géant"] = 4148,
    },
    ["Bottes noires en tisse-mage"] = {
        Professions = 12073,
    },
    ["Furtivité de l'estafette d'Ashenvale"] = {
        ["Estafette d'Ashenvale"] = 20540,
    },
    ["Métamorphose : vache"] = {
        ["MAGE"] = 28270,
    },
    ["Garde de Zanzil"] = {
        ["Garde de Zanzil"] = 8832,
        ["Zanzil le Paria"] = 8832,
    },
    ["Chatouilles de Warosh"] = {
        ["Warosh"] = 16771,
    },
    ["Pantalon du grand vaudou"] = {
        Professions = 10560,
    },
    ["Ench. d'arme 2M (Esprit inférieur)"] = {
        Professions = 13380,
    },
    ["Attaque surprise"] = {
        ["Traqueur des combes"] = 8151,
    },
    ["Esprit ancestral"] = {
        ["SHAMAN"] = 20777,
    },
    ["Découpeuse en sombrefer"] = {
        Professions = 15294,
    },
    ["Détection des morts-vivants"] = {
        ["PALADIN"] = 5502,
    },
    ["Cape en laine épaisse"] = {
        Professions = 3844,
    },
    ["Ceinture runique en cuivre"] = {
        Professions = 2666,
    },
    ["Poison instantané V"] = {
        ["ROGUE"] = 11342,
    },
    ["Envoyer des Combattants cupides"] = {
        ["Twiggy Flathead"] = 8645,
    },
    ["Ceinture épaisse grumegueule"] = {
        Professions = 23628,
    },
    ["Dynamite Ev-Lan II"] = {
        Professions = 23069,
    },
    ["Masque en tisse-ombre"] = {
        Professions = 12086,
    },
    ["Armure démoniaque"] = {
        ["WARLOCK"] = 11735,
        ["Thule Ravenclaw"] = 13787,
    },
    ["Potion de Rapidité"] = {
        Professions = 2335,
    },
    ["Passe-partout en or"] = {
        Professions = 19667,
    },
    ["Coup railleur"] = {
        ["WARRIOR"] = 20560,
    },
    ["Aura d’épines"] = {
        ["Lanceur de Tranchebauge"] = 8148,
        ["Marcherêve wyrmide"] = 7966,
        ["Tisseur d'épines de Tranchebauge"] = 7966,
    },
    ["Heaume-fusée gobelin"] = {
        Professions = 12758,
    },
    ["Fouet de flammes"] = {
        ["Dragonnet rouge"] = 3356,
        ["Sœur Riven"] = 3356,
    },
    ["Invocation d'une explosion de lave A"] = {
        ["Rat du Magma"] = 21886,
    },
    ["Bottes en bronze argenté"] = {
        Professions = 3331,
    },
    ["Invocation de marcheur du néant"] = {
        ["Marcheur du néant"] = 22876,
        ["Œil errant de Kilrogg"] = 22876,
    },
    ["Dynamite"] = {
        ["Carrier de la KapitalRisk"] = 8800,
    },
    ["Détection de l'invisibilité"] = {
        ["WARLOCK"] = 2970,
    },
    ["Comète, BLEUE GRANDE"] = {
        ["Type des feux d'artifice de Pat - GRAND BLEU"] = 26351,
    },
    ["Culotte cramoisie en soie"] = {
        Professions = 8799,
    },
    ["Ench. de plastron (Vie inférieure)"] = {
        Professions = 7748,
    },
    ["Heaume lourd en mithril"] = {
        Professions = 9970,
    },
    ["Eclair des arcanes"] = {
        ["Ombre de Fellicent"] = 13901,
    },
    ["Totem Langue de feu"] = {
        ["SHAMAN"] = 16387,
    },
    ["Bénédiction de lumière"] = {
        ["PALADIN"] = 19979,
    },
    ["Ench. de bottes (Agilité mineure)"] = {
        Professions = 7867,
    },
    ["Boucle en fer"] = {
        Professions = 8768,
    },
    ["Repentir"] = {
        ["PALADIN"] = 20066,
    },
    ["Croissant des ombres"] = {
        Professions = 3500,
    },
    ["Totem de brûlure"] = {
        ["Totem de brûlure"] = 15038,
    },
    ["Morsure"] = {
        Pet = 17261,
    },
    ["Cuirasse démoniaque"] = {
        Professions = 16667,
    },
    ["Malédiction de Shahram"] = {
        ["Shahram"] = 16597,
    },
    ["Création de Pierre de feu (inférieure)"] = {
        ["WARLOCK"] = 6366,
    },
    ["Robe de sorcier inférieur"] = {
        Professions = 6690,
    },
    ["Pantalon élégant en cuir"] = {
        Professions = 7133,
    },
    ["Ench. de gants (Herboristerie)"] = {
        Professions = 13617,
    },
    ["Sauvagerie"] = {
        ["Grell vicieux"] = 5515,
        ["Furie Plumesang"] = 5515,
    },
    ["Rupture"] = {
        ["ROGUE"] = 11275,
    },
    ["Ench. de bracelets (Esprit)"] = {
        Professions = 13642,
    },
    ["Bracelets en fer émeraude"] = {
        Professions = 3501,
    },
    ["Prière du désespoir"] = {
        ["PRIEST"] = 19243,
    },
    ["Elixir de Respiration aquatique supérieure"] = {
        Professions = 22808,
    },
    ["Ench. de bottes (Esprit)"] = {
        Professions = 20024,
    },
    ["Loup Tranchecrin"] = {
        ["Loup Tranchecrin"] = 6479,
        ["Chasseur Tranchecrin"] = 6479,
    },
    ["Poison affaiblissant II"] = {
        ["ROGUE"] = 3421,
    },
    ["Elixir de Robustesse"] = {
        Professions = 3450,
    },
    ["Epaulières de la nuit"] = {
        Professions = 10516,
    },
    ["Robe en étoffe cendrée"] = {
        Professions = 12069,
    },
    ["Métal en fusion"] = {
        ["Gilnid"] = 5213,
        ["Fumar"] = 5213,
    },
    ["Cuirasse ornée en mithril"] = {
        Professions = 9972,
    },
    ["Crinière acérée"] = {
        ["Huran Tranchecrin"] = 5280,
    },
    ["Baguette magique inférieure"] = {
        Professions = 14293,
    },
    ["Poison mortel V"] = {
        ["ROGUE"] = 25347,
    },
    ["Jambières volcaniques"] = {
        Professions = 19059,
    },
    ["Tissage de toile"] = {
        ["Sombre-veuve Razzashi"] = 24600,
    },
    ["Brassards verts en lin"] = {
        Professions = 3841,
    },
    ["Bottes en étoffe runique"] = {
        Professions = 18423,
    },
    ["Hurlement corrompu"] = {
        ["Contaminé Plaie-de-nuit"] = 3424,
        ["Videssac"] = 3424,
    },
    ["Poison douloureux II"] = {
        ["ROGUE"] = 13228,
    },
    ["Création du Tonneau de Weegli"] = {
        ["Weegli Blastfuse"] = 10772,
    },
    ["Invocation d'un spectre du syndicat"] = {
        ["Spectre du Syndicat"] = 3722,
        ["Herod la Paperasse"] = 3722,
    },
    ["Epaulières en sombrefer"] = {
        Professions = 15295,
    },
    ["Désarmement de piège"] = {
        ["ROGUE"] = 1842,
    },
    ["Douleur accablante"] = {
        ["Déchiqueteur de Sneed"] = 3603,
    },
    ["Ceinture de l'archimage"] = {
        Professions = 22866,
    },
    ["Aura de dévotion"] = {
        ["PALADIN"] = 10293,
        ["Champion Blackrock"] = 8258,
        ["Capitaine Melrache"] = 8258,
        ["Montagnard de Dun Garok"] = 8258,
        ["Seigneur Rochepoing"] = 8258,
        ["Protecteur écarlate"] = 8258,
        ["Garde-huran de Tranchebauge"] = 8258,
        ["Champion garde-huran"] = 8258,
        ["Seigneur de guerre kolkar"] = 8258,
    },
    ["Léthargie druidique"] = {
        ["Boahn"] = 8040,
        ["Druide du Croc"] = 8040,
    },
    ["Poison instantané VI"] = {
        ["ROGUE"] = 11343,
    },
    ["Tromblon mortel"] = {
        Professions = 3936,
    },
    ["Hurlement sanguinaire"] = {
        ["Hurle-sang Ragelune"] = 3264,
        ["Serena Plumesang"] = 3264,
        ["Hurleur Patte-fantôme"] = 3264,
        ["Rugissang le Traqueur"] = 3264,
        ["Vieux Saute-falaise"] = 3264,
    },
    ["Exécution"] = {
        ["WARRIOR"] = 20662,
        ["Pourfendeur Sombrepoil"] = 7160,
        ["Chasseur de têtes Witherbark"] = 7160,
        ["Tueuse Ensorçaile"] = 7160,
        ["Tueuse Rougefurie"] = 7160,
        ["Charognard des Salines"] = 7160,
        ["Vautour des Salines"] = 7160,
    },
    ["Ench. de gants (Dépeçage)"] = {
        Professions = 13698,
    },
    ["Métamorphose supérieure"] = {
        ["Lieur de sort de l'Aile noire"] = 22274,
        ["Grethok le Contrôleur"] = 22274,
    },
    ["Comète, VERTE"] = {
        ["Type des feux d'artifice de Pat - VERT"] = 26345,
    },
    ["Gilet noir en tisse-mage"] = {
        Professions = 12048,
    },
    ["Ench. de bouclier (Endurance)"] = {
        Professions = 13817,
    },
    ["Rouleau d'étoffe de laine"] = {
        Professions = 2964,
    },
    ["Ench. de gants (Force supérieure)"] = {
        Professions = 20013,
    },
    ["Sombre prédiction d'Armure de Sayge"] = {
        ["Sayge"] = 23767,
    },
    ["Annoncer le zombie de Zul'Farrak"] = {
        ["Héros mort de Zul'Farrak"] = 10747,
        ["Zombie de Zul'Farrak"] = 10747,
    },
    ["Invocation de destrier"] = {
        ["PALADIN"] = 23214,
    },
    ["Mouton explosif"] = {
        Professions = 3955,
    },
    ["Malédiction de l’oeil"] = {
        ["Marin maudit"] = 3360,
        ["Soldat de marine maudit"] = 3360,
        ["Premier officier Snellig"] = 3360,
        ["Capitaine Halyndor"] = 3360,
        ["Marin maudit"] = 10651,
        ["Soldat de marine maudit"] = 10651,
        ["Premier officier Snellig"] = 10651,
        ["Capitaine Halyndor"] = 10651,
        ["Marin maudit"] = 10653,
        ["Soldat de marine maudit"] = 10653,
        ["Premier officier Snellig"] = 10653,
        ["Capitaine Halyndor"] = 10653,
    },
    ["Cuirasse en écailles dorées"] = {
        Professions = 3511,
    },
    ["Jambières vivantes"] = {
        Professions = 19078,
    },
    ["Cône de feu"] = {
        ["Garde des flammes"] = 19630,
    },
    ["Cueillette"] = {
        Professions = 2368,
        Professions = 3570,
        Professions = 11993,
    },
    ["Gardien de flammes rapide"] = {
        ["Mage renégat défias"] = 4979,
        ["Mystique Bloodscalp"] = 4979,
        ["Gardien squelette"] = 4979,
        ["Géologue de la KapitalRisk"] = 4979,
        ["Géomancien Brisepierre"] = 4979,
        ["Géomancien Rat des tunnels"] = 4979,
        ["Incendiaire Brisepoing"] = 4979,
        ["Evocateur défias"] = 4979,
        ["Mage de Dalaran"] = 4979,
        ["Grel'borg l'Avare"] = 4979,
        ["Géomancienne Dos-hirsute"] = 4979,
        ["Sorcier défias"] = 4979,
        ["Contremaître Cozzle"] = 4979,
    },
    ["Ench. d'arme 2M (Impact excellent)"] = {
        Professions = 20030,
    },
    ["Masque du grand vaudou"] = {
        Professions = 10531,
    },
    ["Cri démoralisant"] = {
        ["WARRIOR"] = 11556,
    },
    ["Potion de rage puissante"] = {
        Professions = 17552,
    },
    ["Malédiction du Sombre maître"] = {
        ["Sombre Maître Gandling"] = 18702,
    },
    ["Gardien de givre"] = {
        ["MAGE"] = 28609,
    },
    ["Eviscération"] = {
        ["ROGUE"] = 31016,
    },
    ["Appel d'un worg écumant"] = {
        ["Worg écumant"] = 7488,
    },
    ["Jet de rets"] = {
        ["Tuten'kash"] = 12252,
    },
    ["Gants de chapardeur"] = {
        Professions = 9148,
    },
    ["Effet de souffrance de Taelan"] = {
        ["Généralissime Taelan Fordring"] = 18811,
    },
    ["Sombre prédiction d'Esprit de Sayge"] = {
        ["Sayge"] = 23738,
    },
    ["Poignée griffue"] = {
        ["Makrura claque-pinces"] = 5424,
        ["Ancien makrura"] = 5424,
    },
    ["Carapace bouclier"] = {
        Pet = 26064,
    },
    ["Naissance"] = {
        ["Tentacule griffu"] = 26586,
        ["Tentacule oculaire"] = 26586,
    },
    ["Le Mastoc"] = {
        Professions = 12754,
    },
    ["Bâtonnet doré"] = {
        Professions = 14379,
    },
    ["Nuage d'éclairs"] = {
        ["Légion-tempête kolkar"] = 6535,
        ["Invocateur de pluie Sombretaillis"] = 6535,
        ["Mavoris Cloudsbreak"] = 6535,
        ["Sorcière des tempêtes Rougefurie"] = 6535,
        ["Légion-tempête galak"] = 6535,
        ["Légion-tempête magram"] = 6535,
        ["Sœur Rathtalon"] = 6535,
        ["Prophète-tempête kolkar"] = 6535,
    },
    ["Serrure d'entraînement"] = {
        Professions = 8334,
    },
    ["Bottes de l'Aube d'argent"] = {
        Professions = 23664,
    },
    ["Aura de résistance au Feu"] = {
        ["PALADIN"] = 19900,
    },
    ["Bave toxique"] = {
        ["Goule écumante"] = 7125,
    },
    ["Salve d'éclairs de givre"] = {
        ["Serviteur Aku'mai"] = 8398,
        ["Aile-de-givre hakkari"] = 8398,
        ["Ras Murmegivre"] = 8398,
    },
    ["Chemise verte d'apparat"] = {
        Professions = 7893,
    },
    ["Potion de force II"] = {
        ["Herboriste Rivepatte"] = 3369,
    },
    ["Douleurs intercostales"] = {
        ["Tortionnaire du Marteau du crépuscule"] = 6945,
        ["Maréchal Windsor"] = 6945,
    },
    ["Potion de protection contre l'Ombre supérieure"] = {
        Professions = 17578,
    },
    ["Enrager"] = {
        ["DRUID"] = 5229,
        ["Guerrier Gurubashi"] = 18501,
        ["Herod"] = 8269,
        ["Myrmidon écarlate"] = 8269,
        ["Abbé écarlate"] = 8269,
        ["Agathelos l'Enragé"] = 8269,
        ["Agam'ar enragé"] = 8269,
        ["Pilleur irradié"] = 8269,
        ["Drek'Thar"] = 8269,
        ["Brute Sombrepoil"] = 8599,
        ["Guerrier Bloodscalp"] = 8599,
        ["Eclaireur Bloodscalp"] = 8599,
        ["Chasseur Bloodscalp"] = 8599,
        ["Berserker Bloodscalp"] = 8599,
        ["Sorcier-docteur Bloodscalp"] = 8599,
        ["Chasseur de tête Bloodscalp"] = 8599,
        ["Lanceur de haches Bloodscalp"] = 8599,
        ["Chaman Bloodscalp"] = 8599,
        ["Belluaire Bloodscalp"] = 8599,
        ["Mystique Bloodscalp"] = 8599,
        ["Charognard Bloodscalp"] = 8599,
        ["Targorr le Terrifiant"] = 8599,
        ["Ténébreux Ragelune"] = 8599,
        ["Yéti féroce"] = 8599,
        ["Grunt d’Orgrimmar"] = 8599,
        ["Massacreur de la Rive noire"] = 8599,
        ["Voleur Fielfurie"] = 8599,
        ["Entourloupeur Fielfurie"] = 8599,
        ["Lige de la corruption Fielfurie"] = 8599,
        ["Traître Fielfurie"] = 8599,
        ["Traqueur des ténèbres Fielfurie"] = 8599,
        ["Implorateur de l’enfer Fielfurie"] = 8599,
        ["Lézard-tonnerre enragé"] = 8599,
        ["Guerrier flétri"] = 8599,
        ["Ravageur flétri"] = 8599,
        ["Garde-huran flétri"] = 8599,
        ["Lanceur flétri"] = 8599,
        ["Coursier Sabots-de-mousse"] = 8599,
        ["Ours de la griffe colérique"] = 8599,
        ["Grizzly de la griffe colérique"] = 8599,
        ["Mutilateur de la griffe colérique"] = 8599,
        ["Sélénien enragé"] = 8599,
        ["Xabraxxis"] = 8599,
        ["Arikara"] = 8599,
        ["Draka"] = 8599,
        ["Duros"] = 8599,
        ["Clampant des récifs enragé"] = 8599,
        ["Déchiqueteuse Rougefurie"] = 8599,
        ["Ecumeur de Bois-brisé"] = 8599,
        ["Kor'kron d'élite"] = 8599,
        ["Maréchal de Glace-sang"] = 8599,
        ["Maréchal de la Tour de la halte"] = 8599,
        ["Maréchal Frostwolf est"] = 8599,
        ["Maréchal Frostwolf ouest"] = 8599,
        ["Maître de guerre de Dun Baldar nord"] = 8599,
        ["Maître de guerre de Dun Baldar sud"] = 8599,
        ["Maître de guerre de l'Aile de glace"] = 8599,
        ["Maître de guerre de Stonehearth"] = 8599,
        ["Champion Blackrock"] = 3019,
        ["Exhalombre Blackrock"] = 3019,
        ["Renégat Blackrock"] = 3019,
        ["Grunt Blackrock"] = 3019,
        ["Estafette Blackrock"] = 3019,
        ["Tharil'zun"] = 3019,
        ["Pisteur Blackrock"] = 3019,
        ["Brise-dos Mâcheroc"] = 3019,
        ["Trogg berserk"] = 3019,
        ["Berserker Witherbark"] = 3019,
        ["Grunt de Trépas-d'Orgrim"] = 3019,
        ["Eclaireur Blackrock"] = 3019,
        ["Sentinelle Blackrock"] = 3019,
        ["Chasseur Blackrock"] = 3019,
        ["Invocateur Blackrock"] = 3019,
        ["Gladiateur Blackrock"] = 3019,
    },
    ["Balayage"] = {
        ["DRUID"] = 9908,
    },
    ["Voile mortel"] = {
        ["WARLOCK"] = 17926,
    },
    ["Apprivoise une bête"] = {
        ["HUNTER"] = 1515,
    },
    ["Flèche d'infection"] = {
        ["Lanceur flétri"] = 11397,
    },
    ["Cape en étoffe runique"] = {
        Professions = 18409,
    },
    ["Minage"] = {
        Professions = 2576,
        Professions = 3564,
        Professions = 10248,
    },
    ["Poudre noire solide"] = {
        Professions = 12585,
    },
    ["Jeunes lions des savanes"] = {
        ["Jeune lion des savanes"] = 8210,
        ["Dishu"] = 8210,
    },
    ["Potion de mana inférieure"] = {
        Professions = 3173,
    },
    ["Frappe en arc de cercle"] = {
        ["Ravageur de la Lame ardente"] = 8374,
        ["Ravageur du crépuscule"] = 8374,
        ["Ravageur des Basses-cavernes"] = 8374,
    },
    ["Aspect de la bête"] = {
        ["HUNTER"] = 13161,
    },
    ["Flèche de feu"] = {
        ["Eclaireur écarlate"] = 6979,
        ["Saboteur du Syndicat"] = 6980,
    },
    ["Fonte du mithril"] = {
        Professions = 10097,
    },
    ["Invocation d'un esclave de Kurzen"] = {
        ["Esclave de Kurzen"] = 8813,
        ["Mogh l'Immortel"] = 8813,
    },
    ["Téléportation : Undercity"] = {
        ["MAGE"] = 3563,
    },
    ["Chemise rouge en lin"] = {
        Professions = 2392,
    },
    ["Bottes élégantes en cuir"] = {
        Professions = 2158,
    },
    ["Potion de protection contre la Nature supérieure"] = {
        Professions = 17576,
    },
    ["Volonté vacillante"] = {
        ["Worg pâle"] = 7127,
    },
    ["Créer l'Aura de Zul"] = {
        ["Yeh'kinya"] = 24186,
    },
    ["Mise à jour"] = {
        ["Techbot"] = 10860,
    },
    ["Coupure d'ailes"] = {
        ["HUNTER"] = 14268,
    },
    ["Invocation de l'esprit de marche-mort atal'ai"] = {
        ["Esprit de marche-mort atal'ai"] = 12095,
        ["Marche-mort atal'ai"] = 12095,
    },
    ["Silex à aiguiser lourd"] = {
        Professions = 3337,
    },
    ["Casque de scaphandrier"] = {
        Professions = 12617,
    },
    ["Chemise lavande en tisse-mage"] = {
        Professions = 12075,
    },
    ["Exposer l'armure"] = {
        ["ROGUE"] = 11198,
    },
    ["Camail en écailles dorées"] = {
        Professions = 3503,
    },
    ["Gelée acide"] = {
        ["Limon gluant"] = 14147,
    },
    ["Ench. d'arme 2M (impact mineur)"] = {
        Professions = 7745,
    },
    ["Balle dure fabriquée"] = {
        Professions = 3947,
    },
    ["Barrage tourbillonnant"] = {
        ["Lanceur de Tranchebauge"] = 8259,
    },
    ["Effet Bouclier de fracas"] = {
        ["Lieutenant de Kurzen"] = 3260,
        ["Défenseur de Dalaran"] = 3260,
    },
    ["Corruption sanguine"] = {
        Pet = 19660,
    },
    ["Solide maillet en fer"] = {
        Professions = 3494,
    },
    ["Invocation d'une engeance de lave"] = {
        ["Engeance de lave"] = 19392,
        ["Seigneur du feu"] = 19392,
    },
    ["Création de baguette Pétale-de-sang"] = {
        ["Larion"] = 22565,
    },
    ["Cape brodée de perles"] = {
        Professions = 6521,
    },
    ["Alchimie"] = {
        Professions = 3101,
        Professions = 3464,
        Professions = 11611,
    },
    ["Armure de dragonnet vert"] = {
        Professions = 9197,
    },
    ["Bière spéciale"] = {
        ["Bom'bay"] = 16712,
    },
    ["Robe Coeur-de-braise"] = {
        Professions = 23666,
    },
    ["Ceinturon en anneaux de feu"] = {
        Professions = 20872,
    },
    ["Ench. de bracelets (Force mineure)"] = {
        Professions = 7782,
    },
    ["Posture de combat"] = {
        ["WARRIOR"] = 2457,
        ["Guerrier squelette"] = 7165,
        ["Alpha des Carmines"] = 7165,
        ["Guerrier Branchie-bleue"] = 7165,
        ["Massacreur Mo'grosh"] = 7165,
        ["Maître Terrassier"] = 7165,
        ["Dextren Ward"] = 7165,
        ["Interné défias"] = 7165,
        ["Explorateur naga"] = 7165,
        ["Guerrier Noirbois"] = 7165,
        ["Nervi du crépuscule"] = 7165,
        ["Défenseur Tranchecrin"] = 7165,
        ["Lok Plaie-des-orcs"] = 7165,
        ["Bondisseur Crache-sel"] = 7165,
        ["Ursa Vile-sylve"] = 7165,
        ["Aligar le Tourmenteur"] = 7165,
        ["Ursa Crin-de-Chardon"] = 7165,
        ["Ruuzel"] = 7165,
        ["Seigneur Brusquebroche"] = 7165,
        ["Garde-huran de Tranchebauge"] = 7165,
        ["Seigneur de guerre kolkar"] = 7165,
        ["Guerrier Ondulame"] = 7165,
        ["Garde Edward"] = 7165,
        ["Garde Jarad"] = 7165,
        ["Garde Kahil"] = 7165,
        ["Garde Lana"] = 7165,
        ["Garde Narrisha"] = 7165,
        ["Garde Tark"] = 7165,
        ["Khan Dez'hepah"] = 7165,
        ["Khan Shaka"] = 7165,
        ["Capitaine Plate-défense"] = 7165,
        ["Contremaître Grills"] = 7165,
        ["Takk le Bondisseur"] = 7165,
        ["Hagg Taurenbane"] = 7165,
        ["Peau-piquante Swinegart"] = 7165,
        ["Ecumeur défias"] = 7165,
        ["Lépreux perturbé"] = 7165,
        ["Gelihast"] = 7165,
        ["Survivant de Gnomeregan"] = 7165,
    },
    ["Trait de glace"] = {
        ["Mécano-Glaceur"] = 11264,
    },
    ["Cuirasse en bronze argenté"] = {
        Professions = 2673,
    },
    ["Barkskin Effect (DND)"] = {
        ["DRUID"] = 22839,
    },
    ["Nuage de boules de neige"] = {
        ["Type nuage de neige de Pat"] = 26000,
    },
    ["Moissonneur compact"] = {
        ["Moissonneur compact"] = 7979,
        ["Forgeur de machines de la KapitalRisk"] = 7979,
        ["Ingénieur Whirleygig"] = 7979,
    },
    ["Huile des ténèbres"] = {
        Professions = 3449,
    },
    ["Bottes en lin"] = {
        Professions = 2386,
    },
    ["Bénédiction de Shahram"] = {
        ["Shahram"] = 16599,
    },
    ["Contrepoids en fer"] = {
        Professions = 7222,
    },
    ["Domination corrompue"] = {
        ["WARLOCK"] = 18708,
    },
    ["Chaînes de Brûlure"] = {
        ["Horreur irradiée"] = 8211,
    },
    ["Furtivité"] = {
        ["Cours-la-nuit défias"] = 22766,
        ["Assassin Sombrepoil"] = 22766,
        ["Panthère Ombregueule"] = 22766,
        ["Commando de Kurzen"] = 22766,
        ["Rampant des tourbières"] = 22766,
        ["Ancienne panthère Ombregueule"] = 22766,
        ["Ombregriffe"] = 22766,
        ["Espion du Syndicat"] = 22766,
        ["Assassin du Syndicat"] = 22766,
        ["Malandrin du Syndicat"] = 22766,
        ["Rôdeur du Syndicat"] = 22766,
        ["Traqueur des crêtes"] = 22766,
        ["Traqueur Crins-d'argent"] = 22766,
        ["Embusquée Ensorçaile"] = 22766,
        ["Traqueur Tranchecrin"] = 22766,
        ["Traqueur déviant"] = 22766,
        ["Traqueur des ténèbres Gangremusc"] = 22766,
        ["Traqueur des ténèbres Mornecoeur"] = 22766,
        ["Traqueur des combes"] = 22766,
        ["Rôdeuse de Sombrebrume"] = 22766,
        ["Rôdeuse Sombrecroc"] = 22766,
        ["Voleur Sans-racines"] = 22766,
        ["Traqueur des ombres Sans-racines"] = 22766,
        ["Infiltrateur de Theramore"] = 22766,
        ["Traqueur de Tranchebauge"] = 22766,
    },
    ["Morsure de vipère"] = {
        ["HUNTER"] = 14280,
    },
    ["Ressusciter le familier"] = {
        ["HUNTER"] = 982,
    },
    ["Bottes en cuir estampé"] = {
        Professions = 2161,
    },
    ["Invocation d'une explosion de lave G"] = {
        ["Rat du Magma"] = 21905,
    },
    ["Meurtrissure"] = {
        ["Brute Gordok"] = 22572,
    },
    ["Camouflage dans l'ombre"] = {
        Racial = 20580,
    },
    ["Sombre prédiction de Force de Sayge"] = {
        ["Sayge"] = 23735,
    },
    ["Aura de sainteté"] = {
        ["PALADIN"] = 20218,
    },
    ["Jambières Coeur-de-braise"] = {
        Professions = 23667,
    },
    ["Combustion"] = {
        ["MAGE"] = 11129,
    },
    ["Jambières en écailles de scorpide épaisses"] = {
        Professions = 19075,
    },
    ["Lumière sacrée"] = {
        ["PALADIN"] = 25292,
    },
    ["Apparition de drakônide noir"] = {
        ["Drakônide noir"] = 22654,
        ["Black Drakonid Spawner"] = 22654,
    },
    ["Frappe fantomatique"] = {
        ["ROGUE"] = 14278,
    },
    ["Bottes en soie d'araignée"] = {
        Professions = 3855,
    },
    ["Dernier rempart"] = {
        ["WARRIOR"] = 12975,
    },
    ["Transmutation de la Vie en Terre"] = {
        Professions = 17565,
    },
    ["Sac en lin"] = {
        Professions = 3755,
    },
    ["Ecorce"] = {
        ["DRUID"] = 22812,
    },
    ["Vol d'esprit"] = {
        ["Cinglenuit"] = 3477,
    },
    ["Transfert"] = {
        ["MAGE"] = 1953,
    },
    ["Potion d'invulnérabilité limitée"] = {
        Professions = 3175,
    },
    ["Demi-bottes de l'aube"] = {
        Professions = 23705,
    },
    ["Tranquillité"] = {
        ["DRUID"] = 9863,
    },
    ["Couture"] = {
        Professions = 3909,
        Professions = 3910,
        Professions = 12180,
    },
    ["Poudre aveuglante"] = {
        ["ROGUE"] = 6510,
    },
    ["Potion de régénération mineure"] = {
        Professions = 2332,
    },
    ["Effet Apparition Leurre"] = {
        ["Leurre"] = 4507,
        ["Leurre perfectionné"] = 4507,
        ["Leurre ouvragé"] = 4507,
        ["Tourtereau"] = 4507,
    },
    ["Invocation de nourriture"] = {
        ["MAGE"] = 28612,
    },
    ["Préméditation"] = {
        ["ROGUE"] = 14183,
    },
    ["Cape glaciale"] = {
        Professions = 3862,
    },
    ["Balle en cuir lourd"] = {
        Professions = 23190,
    },
    ["Frappe de Rhahk'Zor"] = {
        ["Rhahk'Zor"] = 6304,
    },
    ["Irradié"] = {
        ["Envahisseur irradié"] = 9775,
        ["Pilleur irradié"] = 9775,
    },
    ["Sac enchanté en étoffe runique"] = {
        Professions = 27659,
    },
    ["Get Gosssip, Test"] = {
        ["Guet de Stormwind"] = 26683,
        ["Patrouilleur de Stormwind"] = 26683,
        ["Gardien des Hauts"] = 26683,
        ["Grunt d’Orgrimmar"] = 26683,
        ["Arnold Leland"] = 26683,
        ["Garde d'Ironforge"] = 26683,
        ["Grunt Korf"] = 26683,
        ["Grunt Bek'rah"] = 26683,
    },
    ["Brûlure d'âme"] = {
        ["Seigneur du feu"] = 19393,
    },
    ["Invocation des exilés prismatiques"] = {
        ["Exilé prismatique"] = 4937,
        ["Myzrael"] = 4937,
    },
    ["Ench. de cape (Résistance mineure)"] = {
        Professions = 7454,
    },
    ["Elixir des Sages"] = {
        Professions = 17555,
    },
    ["Ench. de plastron (Caract. inférieures)"] = {
        Professions = 13700,
    },
    ["Ench. de bouclier (Esprit excellent)"] = {
        Professions = 20016,
    },
    ["Consécration"] = {
        ["PALADIN"] = 20924,
    },
    ["Invocation d'un esprit du marais"] = {
        ["Esprit des Marais"] = 9636,
        ["Parleur des marécages"] = 9636,
    },
    ["Bottes en tisse-ombre"] = {
        Professions = 12082,
    },
    ["Sombre prédiction d'Agilité de Sayge"] = {
        ["Sayge"] = 23736,
    },
    ["Créer une Sphère de mage"] = {
        ["Tabetha"] = 9156,
    },
    ["Jet d’acide"] = {
        ["Macaron"] = 6306,
        ["Wyrmide vert"] = 6306,
        ["Ancien draconien"] = 6306,
        ["Gluggle"] = 6306,
    },
    ["Invocation d'un destrier de l'effroi"] = {
        ["WARLOCK"] = 23161,
    },
    ["Carabine en thorium"] = {
        Professions = 19792,
    },
    ["Elixir d'oeil de chat"] = {
        Professions = 12609,
    },
    ["Bottes de l'enchanteur"] = {
        Professions = 3860,
    },
    ["Contrôle mental"] = {
        ["PRIEST"] = 10912,
    },
    ["Abolir le poison"] = {
        ["DRUID"] = 2893,
    },
    ["Ench. de gants (Equitation)"] = {
        Professions = 13947,
    },
    ["Fin de la manifestation"] = {
        ["Esprit de Kolk"] = 21965,
        ["Esprit de Veng"] = 21965,
    },
    ["Larve de Gelée"] = {
        ["Larve maléfique"] = 21069,
    },
    ["Invocation d'un tréant allié"] = {
        ["Tréant allié"] = 7993,
        ["Fils de Cénarius"] = 7993,
    },
    ["Flacon de pouvoir suprême"] = {
        Professions = 17637,
    },
    ["Cuirasse en vrai-argent"] = {
        Professions = 9974,
    },
    ["Geyser"] = {
        ["Chevaucheur des vagues Hainecrête"] = 10987,
        ["Embruns"] = 10987,
        ["Princesse Tempestria"] = 10987,
    },
    ["Verrouillage"] = {
        ["Golem de guerre"] = 9576,
    },
    ["Lanceur de fusées"] = {
        Professions = 26442,
    },
    ["Poison mortel"] = {
        ["ROGUE"] = 2835,
        ["Rôdeuse venimeuse des brumes"] = 3583,
        ["Mazzranache"] = 3583,
        ["Serpent-nuage venimeux"] = 3583,
        ["Solitaire de Sombrebrume"] = 3583,
        ["Fardel Dabyrie"] = 3583,
    },
    ["Force altérée"] = {
        ["Guerrier Vile-sylve"] = 6816,
        ["Ursa Vile-sylve"] = 6816,
        ["Chênepatte"] = 6816,
        ["Vile-sylve enragé"] = 6816,
    },
    ["Onde de choc"] = {
        ["Grol le Destructeur"] = 12734,
        ["Obsidion"] = 12734,
        ["Gor'tesh"] = 12734,
    },
    ["Paranoïa"] = {
        Pet = 19480,
    },
    ["Flammes incendiaires"] = {
        ["Ambassadeur Infernus"] = 9552,
    },
    ["Carquois de tir rapide"] = {
        Professions = 14930,
    },
    ["Malédiction amplifiée"] = {
        ["WARLOCK"] = 18288,
    },
    ["Haches et masses à deux mains"] = {
        ["SHAMAN"] = 16269,
    },
    ["Rapace"] = {
        ["Condor redoutable"] = 5708,
        ["Vultros"] = 5708,
        ["Rapace nerveux"] = 5708,
        ["Rapace"] = 5708,
        ["Rapace acéré"] = 5708,
        ["Chasseresse céleste Aile-fière"] = 5708,
        ["Rapace de l'effroi"] = 5708,
    },
    ["Epaulières noires en cuir"] = {
        Professions = 3769,
    },
    ["Diadème en tisse-rêve"] = {
        Professions = 12092,
    },
    ["Oubli"] = {
        ["PRIEST"] = 10942,
        ["Dame Sylvanas Windrunner"] = 20672,
    },
    ["L'essor de l'esprit"] = {
        ["Mangletooth"] = 10767,
    },
    ["Potion de sommeil sans rêve supérieure"] = {
        Professions = 24366,
    },
    ["Téléportation : Darnassus"] = {
        ["MAGE"] = 3565,
    },
    ["Petite bombe en bronze"] = {
        Professions = 3941,
    },
    ["Toucher guérisseur"] = {
        ["DRUID"] = 25297,
    },
    ["Malédiction d'impuissance"] = {
        ["Entourloupeur Follengeance"] = 22371,
        ["Régisseuse sanglante de Kirtonos"] = 22371,
    },
    ["Traite corrompue"] = {
        ["John le Loqueteux"] = 16472,
    },
    ["Toucher de Flétrissure"] = {
        ["Garde-huran flétri"] = 11442,
        ["Pourriture vivante"] = 11442,
    },
    ["Courroux bestial"] = {
        ["HUNTER"] = 19574,
    },
    ["Pointe de bouclier en thorium"] = {
        Professions = 16651,
    },
    ["Poison"] = {
        ["Araignée forestière"] = 11918,
        ["Araignée minière"] = 11918,
        ["Morgaine la rusée"] = 11918,
        ["Mère Croc"] = 11918,
        ["Matriarche Tisse-nuit"] = 11918,
        ["Githyiss la Vile"] = 11918,
        ["Rôdeuse Tissebois"] = 11918,
        ["Croc-venin Tissebois"] = 11918,
        ["Tisse-soie Tissebois"] = 11918,
        ["Araignée Tissebois géante"] = 11918,
        ["Fée vile"] = 11918,
        ["Scorpide cliquetant"] = 11918,
        ["Scorpide corrompu"] = 11918,
        ["Sarkoth"] = 11918,
        ["Chasse-marée murloc"] = 744,
        ["Araignée Tisse-venin"] = 744,
        ["Gnoll Sombrepoil enragé"] = 744,
        ["Tarentule"] = 744,
        ["Grande tarentule"] = 744,
        ["Araignée Tisse-venin pygmée"] = 744,
        ["Solitaire verte"] = 744,
        ["Assassin Sombrepoil"] = 744,
        ["Charognard Bloodscalp"] = 744,
        ["Tisseuse des pleurs"] = 744,
        ["Fourrageur Selécaille"] = 744,
        ["Lame nocturne défias"] = 744,
        ["Contaminé Plaie-de-nuit"] = 744,
        ["Jeune veuve noire"] = 744,
        ["Rohh le silencieux"] = 744,
        ["Fourrageur Branchie-bleue"] = 744,
        ["Arpenteur Dragonmaw"] = 744,
        ["Seigneur des tourbières"] = 744,
        ["Rôdeuse des falaises"] = 744,
        ["Rôdeuse des bois"] = 744,
        ["Rôdeuse des forêts"] = 744,
        ["Tisse-nuit vicieuse"] = 744,
        ["Rôdeuse des sous-bois"] = 744,
        ["Rampant des brumes"] = 744,
        ["Chasse-marée Grisebrume"] = 744,
        ["Larron du Syndicat"] = 744,
        ["Assassin du Syndicat"] = 744,
        ["Voleur du Syndicat"] = 744,
        ["Assassin des ténèbres"] = 744,
        ["Rampant des plaines"] = 744,
        ["Rampant des plaines géant"] = 744,
        ["Fournisseur Sombrefer"] = 744,
        ["Sourcier Dos-hirsute"] = 744,
        ["Entourloupeur Mornecoeur"] = 744,
        ["Infiltrateur Réprouvé"] = 744,
        ["Rôdeuse Ronce-sauvage"] = 744,
        ["Rampant des mousses profondes"] = 744,
        ["Jeune Aile-fière"] = 744,
        ["Wyverne Aile-fière"] = 744,
        ["Chasseresse céleste Aile-fière"] = 744,
        ["Patriarche Aile-fière"] = 744,
        ["Wyverne de Haut-perchoir"] = 744,
        ["Concubine de Haut-perchoir"] = 744,
        ["Patriarche de Haut-perchoir"] = 744,
        ["Araignée de Sombrebrume"] = 744,
        ["Rôdeuse Sombrecroc"] = 744,
        ["Araignée Sombrecroc"] = 744,
        ["Araignée Sombrecroc géante"] = 744,
        ["Fourrageur Branchie-bistre"] = 744,
        ["Entourloupeur Fielfurie"] = 744,
        ["Ami de Slim"] = 744,
        ["Serpent déviant"] = 744,
        ["Guêpe Centipaar"] = 744,
        ["Araignée verrière"] = 744,
        ["Videssac"] = 744,
        ["Grand Vandale des flots"] = 744,
        ["Démon des tombes"] = 744,
        ["Assassin galak"] = 744,
        ["Arikara"] = 744,
        ["Bazzalan"] = 744,
        ["Traqueuse des rochers"] = 744,
    },
    ["Terreur ancienne"] = {
        ["Ancien chien du Magma"] = 19365,
    },
    ["Nuage pestilentiel"] = {
        ["Goule infectée"] = 17742,
    },
    ["Carabine de gros calibre en mithril"] = {
        Professions = 12614,
    },
    ["Aspect de la nature"] = {
        ["HUNTER"] = 20190,
    },
    ["Invocation d'un élémentaire d'eau"] = {
        ["Elémentaire d'eau invoqué"] = 17162,
        ["Conjurateur cramoisi"] = 17162,
    },
    ["Gilet terrestre"] = {
        Professions = 8764,
    },
    ["Manteau de suie"] = {
        ["Ancien noirci"] = 7998,
    },
    ["Chemise noire en soie"] = {
        Professions = 3870,
    },
    ["Forme d’ours redoutable"] = {
        ["DRUID"] = 9634,
    },
    ["Pierre à aiguiser dense"] = {
        Professions = 16641,
    },
    ["Pantalon noir en tisse-mage"] = {
        Professions = 12049,
    },
    ["Invocation d'une explosion de lave D"] = {
        ["Rat du Magma"] = 21902,
    },
    ["Ceinture noire en cuir"] = {
        Professions = 3766,
    },
    ["Vague de foudre"] = {
        ["Ysondre"] = 24819,
    },
    ["Robe de puissance"] = {
        Professions = 8770,
    },
    ["Contre-attaque"] = {
        ["HUNTER"] = 20910,
    },
    ["Feu de l'âme"] = {
        ["WARLOCK"] = 17924,
    },
    ["Ench. de bracelets (Endurance supérieure)"] = {
        Professions = 13945,
    },
    ["Cuir robuste"] = {
        Professions = 22331,
    },
    ["Pantalon en gangrétoffe"] = {
        Professions = 18419,
    },
    ["Invocation d'un rubis de mana"] = {
        ["MAGE"] = 10054,
    },
    ["Bottes du Magma"] = {
        Professions = 20853,
    },
    ["Epaulières rouges en tisse-mage"] = {
        Professions = 12078,
    },
    ["Pistage des démons"] = {
        ["HUNTER"] = 19878,
    },
    ["Bottes en anneaux de cuivre"] = {
        Professions = 3319,
    },
    ["Vol à la tire"] = {
        ["ROGUE"] = 921,
    },
    ["Poison affaiblissant"] = {
        ["ROGUE"] = 3420,
    },
    ["Huile de sorcier brillante"] = {
        Professions = 25129,
    },
    ["Transmutation du Non-mort en Eau"] = {
        Professions = 17563,
    },
    ["Rugissement provocateur"] = {
        ["DRUID"] = 5209,
    },
    ["Gants en cuir de dragonnet rouge"] = {
        Professions = 9072,
    },
    ["Cape en lin renforcé"] = {
        Professions = 2397,
    },
    ["Douleur déchirante"] = {
        ["Humar le Fier"] = 3247,
    },
    ["Gantelets en bronze argenté"] = {
        Professions = 3333,
    },
    ["Potion de soins décolorée"] = {
        Professions = 4508,
    },
    ["Endurance supérieure"] = {
        ["HUNTER"] = 5049,
    },
    ["Bottes-fusées gobelines"] = {
        Professions = 8895,
    },
    ["Expiation"] = {
        ["SHAMAN"] = 8012,
    },
    ["Téléportation : Orgrimmar"] = {
        ["MAGE"] = 3567,
    },
    ["Apparition de drakônide vert"] = {
        ["Drakônide vert"] = 22656,
        ["Green Drakonid Spawner"] = 22656,
    },
    ["Invocation d'une citrine de mana"] = {
        ["MAGE"] = 10053,
    },
    ["Distraction"] = {
        ["ROGUE"] = 1725,
    },
    ["Poudre d'explosion grossière"] = {
        Professions = 3929,
    },
    ["Plongeon"] = {
        Pet = 23148,
    },
    ["Gilet en cuir sauvage"] = {
        Professions = 10544,
    },
    ["Ceinture de gardien"] = {
        Professions = 3775,
    },
    ["Récupération"] = {
        ["DRUID"] = 25299,
        ["Archidruide Renferal"] = 15981,
        ["Marcherêve wyrmide"] = 12160,
        ["Sorcier du Totem sinistre"] = 12160,
    },
    ["Téléportation : Reflet-de-Lune"] = {
        ["DRUID"] = 18960,
    },
    ["Bloc de glace"] = {
        ["MAGE"] = 11958,
    },
    ["Robe bleue en lin"] = {
        Professions = 7633,
    },
    ["Gants de vraie foi"] = {
        Professions = 8782,
    },
    ["Toiles collantes"] = {
        ["Grande prêtresse Mar'li"] = 24110,
    },
    ["La force d’Agammagan"] = {
        ["Mangletooth"] = 16612,
    },
    ["Décrépitude spirituelle"] = {
        ["Bâtard Rivepatte"] = 8016,
        ["Bâtard Poil-moussu"] = 8016,
        ["Flagellant solécaille"] = 8016,
    },
    ["Marteau de croisé"] = {
        ["Grand croisé Dathrohan"] = 17286,
    },
    ["Batterie en or"] = {
        Professions = 12584,
    },
    ["Gants de sabre-de-givre"] = {
        Professions = 19087,
    },
    ["Pierre d'alchimiste"] = {
        Professions = 17632,
    },
    ["Mules en soie d'araignée"] = {
        Professions = 3856,
    },
    ["Invocation d'un défenseur"] = {
        ["Défenseur de Dalaran"] = 3655,
        ["Invocateur de Dalaran"] = 3655,
    },
    ["Enzyme putride"] = {
        ["Hanneton perceur"] = 14539,
    },
    ["Souffrance"] = {
        Pet = 17752,
    },
    ["Oeil de la bête"] = {
        ["HUNTER"] = 1002,
    },
    ["Camouflage"] = {
        ["ROGUE"] = 1787,
        ["Colonel Kurzen"] = 8822,
    },
    ["Ceinture terrestre en soie"] = {
        Professions = 8797,
    },
    ["Horion de l'ombre"] = {
        ["Krethis Shadowspinner"] = 17439,
        ["Succube invoquée"] = 16583,
        ["Vol'jin"] = 17289,
    },
    ["Décoction instable"] = {
        ["Chevaucheur de sanguinaire"] = 24024,
    },
    ["Peste dévorante"] = {
        ["PRIEST"] = 19280,
    },
    ["Enchanter les Annales de Darrowshire"] = {
        ["Chromie"] = 17285,
    },
    ["Carabine vise-lune"] = {
        Professions = 3954,
    },
    ["Brassards de gardien en cuir"] = {
        Professions = 3777,
    },
    ["Ench. de plastron (Vie majeure)"] = {
        Professions = 20026,
    },
    ["Marteau runique en mithril"] = {
        Professions = 10009,
    },
    ["Chemise blanche de bretteur"] = {
        Professions = 8483,
    },
    ["Forme de pierre"] = {
        Racial = 20594,
        ["Insurgé Sombrefer"] = 7020,
        ["Capitaine Ironhill"] = 7020,
    },
    ["Bouclier de feu III"] = {
        ["Mage de la Voile sanglante"] = 2601,
        ["Lo'Grosh"] = 2601,
        ["Evocateur écarlate"] = 2601,
        ["Sorcier écarlate"] = 2601,
        ["Infernal mineur"] = 2601,
    },
    ["Sceau du Croisé"] = {
        ["PALADIN"] = 20308,
    },
    ["Désespoir ancestral"] = {
        ["Ancien chien du Magma"] = 19369,
    },
    ["Transmutation de l'Eau en Air"] = {
        Professions = 17562,
    },
    ["Eclats stellaires"] = {
        ["PRIEST"] = 19305,
    },
    ["Invocation du panier du totem Vile-sylve"] = {
        ["Chef Murgut"] = 20818,
    },
    ["Lame dorée flamboyante"] = {
        Professions = 15973,
    },
    ["Elixir de défense mineure"] = {
        Professions = 7183,
    },
    ["Masse en cuivre"] = {
        Professions = 2737,
    },
    ["Hache de guerre en cuivre"] = {
        Professions = 3293,
    },
    ["Invocation d'un serviteur enflammé"] = {
        ["Serviteur enflammé"] = 10870,
    },
    ["Invocation d'un serviteur squelettique"] = {
        ["Serviteur squelettique"] = 12420,
        ["Nécromancienne de la Tête de Mort"] = 12420,
        ["Ambassadeur Ragesang"] = 12420,
        ["Nécromancien"] = 12420,
        ["Nécromancien Thuzadin"] = 12420,
    },
    ["Chaleur d'affliction"] = {
        ["Ancien chien du Magma"] = 19367,
    },
    ["Pluie de flammes"] = {
        ["Elémentaire infernal"] = 10733,
        ["Mécano-Brûleur"] = 10733,
        ["Marchefeu de Thaurissan"] = 10733,
    },
    ["Forme de félin"] = {
        ["DRUID"] = 768,
        ["Seigneur Melenas"] = 5759,
        ["Sentinelle Amarassan"] = 5759,
    },
    ["Comète, BLANCHE"] = {
        ["Type des feux d'artifice de Pat - BLANC"] = 26348,
    },
    ["Poison de scorpide"] = {
        Pet = 24587,
    },
    ["Tir réflexe"] = {
        ["Braconnier crin-pâle"] = 1516,
    },
    ["Charge farouche"] = {
        ["DRUID"] = 16979,
    },
    ["Grande charge d'hydroglycérine"] = {
        Professions = 3972,
    },
    ["Maître de l'évasion"] = {
        Racial = 20589,
    },
    ["Habit de vraie foi"] = {
        Professions = 18456,
    },
    ["Petite comète bleue"] = {
        Professions = 26416,
    },
    ["Sanguinaire"] = {
        ["WARRIOR"] = 23894,
    },
    ["Mains des ténèbres"] = {
        Professions = 8780,
    },
    ["Eclairs en série"] = {
        ["Margol l'Enragée"] = 15549,
        ["Owatanka"] = 6254,
    },
    ["Elixir des arcanes supérieur"] = {
        Professions = 17573,
    },
    ["Prière de robustesse"] = {
        ["PRIEST"] = 21564,
    },
    ["Totem de Protection élémentaire"] = {
        ["Totem de Protection élémentaire"] = 8262,
        ["Sage de la Tête de Mort"] = 8262,
        ["Totémique Grumegueule"] = 8262,
        ["Sorcier-docteur Smolderthorn"] = 8262,
    },
    ["Brassards mats"] = {
        Professions = 9201,
    },
    ["Esquive rapide"] = {
        ["Clampant de l'écume incrusté"] = 5426,
    },
    ["Ordre d'ombre"] = {
        ["Seigneur Victor Nefarius"] = 22667,
    },
    ["Grondement d’intimidation"] = {
        ["Grogneuse Pestepoil"] = 6576,
    },
    ["Soldat Ping"] = {
        ["Soldat écarlate"] = 19749,
    },
    ["Gardien de feu"] = {
        ["MAGE"] = 10225,
    },
    ["Force diminuée"] = {
        ["Gnome lépreux"] = 6951,
        ["Clampant de l'écume corrompu"] = 6951,
    },
    ["Tigresse glaciale"] = {
        Professions = 3497,
    },
    ["Fouet de la douleur"] = {
        Pet = 11780,
    },
    ["Témérité"] = {
        ["WARRIOR"] = 1719,
    },
    ["Cuir lourd"] = {
        Professions = 20649,
    },
    ["Appel de la tombe"] = {
        ["Excavateur mort-vivant"] = 5137,
        ["Azshir le Sans-sommeil"] = 5137,
    },
    ["Totem Fontaine de mana"] = {
        ["SHAMAN"] = 10497,
    },
    ["Elixir de Vision de rêve"] = {
        Professions = 11468,
    },
    ["Invocation d'un golem télécommandé"] = {
        ["Golem télécommandé"] = 3605,
        ["Ingénieur gobelin"] = 3605,
    },
    ["Bottes barbares en fer"] = {
        Professions = 9818,
    },
    ["Totem Furie-des-vents"] = {
        ["SHAMAN"] = 10614,
    },
    ["Chemise rouge de bretteur"] = {
        Professions = 8489,
    },
    ["Elixir de Détection des démons"] = {
        Professions = 11478,
    },
    ["Bottes en laine"] = {
        Professions = 2401,
    },
    ["Chute lente"] = {
        ["MAGE"] = 130,
    },
    ["Cotte de mailles en sombrefer"] = {
        Professions = 15293,
    },
    ["Assaut fulgurant"] = {
        ["Sanglier Brocheroc"] = 6268,
        ["Longroin"] = 6268,
        ["Broche-tripes"] = 6268,
        ["Princesse"] = 6268,
        ["Fouailleventre"] = 6268,
        ["Entourage porcin"] = 6268,
        ["Lardeur"] = 6268,
        ["Jeune Broche-tripes"] = 6268,
        ["Sanglier Rochecuir"] = 6268,
        ["Grand Broche-tripes"] = 6268,
        ["Guerrier des rivages murloc"] = 6268,
        ["Sanglier des rochers"] = 6268,
        ["Grand sanglier des rochers"] = 6268,
        ["Ancien sanglier des rochers"] = 6268,
        ["Sanglier des montagnes"] = 6268,
        ["Sanglier des montagnes galeux"] = 6268,
        ["Ancien sanglier des montagnes"] = 6268,
        ["Sanglier des rochers balafré"] = 6268,
        ["Cavalier de Stromgarde"] = 6268,
        ["Jeune kodo"] = 6268,
        ["Mazzranache"] = 6268,
        ["Fouette-queue Griffesang"] = 6268,
        ["Tranche-gueule Griffesang"] = 6268,
        ["Marin de Kul Tiras"] = 6268,
        ["Tranche-gueule Griffesang corrompu"] = 6268,
        ["Grand kodo des Tarides"] = 6268,
        ["Kodo laineux"] = 6268,
        ["Maraudeur kolkar"] = 6268,
        ["Bondisseur Crache-sel"] = 6268,
        ["Cerf sauvage"] = 6268,
        ["Lézard-tempête enragé des falaises"] = 6268,
        ["Coursier à ramure"] = 6268,
        ["Maraudeur galak"] = 6268,
        ["Agam'ar"] = 6268,
        ["Brontus"] = 6268,
        ["Sanglier cendré"] = 6268,
        ["Patriarche Tête-tonnerre"] = 6268,
        ["Guerrier flétri"] = 6268,
        ["Envahisseur kolkar"] = 6268,
        ["Brute du Totem sinistre"] = 6268,
        ["Herod"] = 8260,
        ["Agathelos l'Enragé"] = 8260,
        ["Grunter"] = 8260,
    },
    ["Réaction"] = {
        ["PRIEST"] = 19275,
    },
    ["Totem de Griffes de pierre"] = {
        ["SHAMAN"] = 10428,
    },
    ["Vision télépathique"] = {
        ["PRIEST"] = 10909,
    },
    ["Robe en étoffe runique"] = {
        Professions = 18406,
    },
    ["Equipage ivre"] = {
        ["Mécano gobelin"] = 20436,
        ["Mécano gnome"] = 20436,
        ["Chef-mécanicien gnome"] = 20436,
        ["Chef-mécanicien gobelin"] = 20436,
    },
    ["Renfort d'armure épais"] = {
        Professions = 10487,
    },
    ["Choc systémique"] = {
        ["Emissaire Roman'khan"] = 23774,
    },
    ["Heurt de bouclier"] = {
        ["WARRIOR"] = 23925,
        ["Renégat Blackrock"] = 8242,
        ["Malfrat défias"] = 8242,
        ["Kam Deepfury"] = 8242,
        ["Grunt Dragonmaw"] = 8242,
        ["Massacreur de la Lame brûlante"] = 8242,
    },
    ["Souffle de flammes"] = {
        ["Narillasanz"] = 9573,
        ["Teremus le Dévoreur"] = 9573,
        ["Drake noir insoumis"] = 9573,
    },
    ["Armure du mage"] = {
        ["MAGE"] = 22783,
    },
    ["Rets de Naraxis"] = {
        ["Naraxis"] = 3542,
    },
    ["Cuirasse en acier"] = {
        Professions = 9916,
    },
    ["Tromblon en mithril"] = {
        Professions = 12595,
    },
    ["Gilet marron en lin"] = {
        Professions = 2385,
    },
    ["Invocation d'une succube"] = {
        ["WARLOCK"] = 712,
    },
    ["Jambières en vignesang"] = {
        Professions = 24092,
    },
    ["Chaîne d'éclairs"] = {
        ["SHAMAN"] = 10605,
        ["Thrall"] = 16033,
        ["Oracle Hakkari"] = 16006,
        ["Primaliste Thurloga"] = 16006,
        ["Concubine Tête-tonnerre"] = 12058,
        ["Charlga Trancheflanc"] = 8292,
    },
    ["Ailes magiques"] = {
        ["Déclencheur du canon de la foire"] = 24742,
    },
    ["Baguette magique supérieure"] = {
        Professions = 14807,
    },
    ["Ceinture verte en cuir"] = {
        Professions = 3774,
    },
    ["Plainte mortelle"] = {
        ["Ame égarée"] = 7713,
        ["Esprit vagabond"] = 7713,
        ["Esprit tourmenté"] = 7713,
        ["Ancêtre gémissante"] = 7713,
        ["Morte gémissante"] = 7713,
        ["Postier mort-vivant"] = 7713,
        ["Spectre gémissant"] = 7713,
        ["Esprit de Val-Terreur"] = 7713,
    },
    ["Invocation d'un serpent de Dalaran"] = {
        ["Serpent de Dalaran"] = 3615,
        ["Protecteur de Dalaran"] = 3615,
        ["Garde de Dalaran"] = 3615,
    },
    ["Lucioles"] = {
        ["DRUID"] = 9907,
        ["Marche-neige Frostmane"] = 6950,
        ["Fée maudite"] = 6950,
        ["Guide Noirbois"] = 6950,
        ["Lève-poussière Tranchecrin"] = 6950,
        ["Tisseuse d’épines Tranchecrin"] = 6950,
        ["Pisteur Noirbois"] = 6950,
        ["Marosh le Sournois"] = 6950,
    },
    ["Métamorphose"] = {
        ["MAGE"] = 12826,
        ["Sorcier du Syndicat"] = 13323,
        ["Gardienne Belamoore"] = 13323,
        ["Arcaniste Doan"] = 13323,
        ["Capitaine Balinda Stonehearth"] = 13323,
    },
    ["Bénédiction du sanctuaire"] = {
        ["PALADIN"] = 20914,
    },
    ["Gardiens terrestres détruits"] = {
        ["Gardien terrestre"] = 10666,
        ["Réformeur terrestre"] = 10666,
        ["Protecteur terrestre"] = 10666,
        ["Garde de la Banque"] = 10666,
    },
    ["Elixir de Robustesse mineure"] = {
        Professions = 2334,
    },
    ["Forge"] = {
        Professions = 3100,
        Professions = 3538,
        Professions = 9785,
    },
    ["Nuée de peste"] = {
        ["Ardo Crassepatte"] = 3256,
        ["Pestilentiel Poil-Putride"] = 3256,
    },
    ["Réflectoflamme hyper-radiant"] = {
        Professions = 23081,
    },
    ["Points d'honneur +2388"] = {
        ["Porteguerre de la Horde"] = 24966,
        ["Général de brigade de l'Alliance"] = 24966,
    },
    ["Potion d'Invisibilité inférieure"] = {
        Professions = 3448,
    },
    ["Lunettes de tigre volant"] = {
        Professions = 3934,
    },
    ["Salve de Traits de l'ombre"] = {
        ["Morloch"] = 17228,
        ["Balgaras le Crasseux"] = 9081,
        ["Varimathras"] = 20741,
        ["Grande prêtresse Hai'watna"] = 14887,
    },
    ["Frénésie de Windsor"] = {
        ["Maréchal Windsor"] = 15167,
    },
    ["Emprise"] = {
        ["Initiée Hederine"] = 15859,
        ["Sirène Strashaz"] = 7645,
        ["Ombremage du crépuscule"] = 7645,
        ["Singer"] = 14515,
        ["Nécrorateur Jargba"] = 14515,
    },
    ["Fonte du fer"] = {
        Professions = 3307,
    },
    ["Puanteur accablante"] = {
        ["Satyre Gangremusc"] = 6942,
        ["Voleur Gangremusc"] = 6942,
        ["Lige de la corruption Gangremusc"] = 6942,
        ["Traqueur des ténèbres Gangremusc"] = 6942,
    },
    ["Marque du chasseur"] = {
        ["HUNTER"] = 14325,
    },
    ["Brassards en écailles dorées"] = {
        Professions = 7223,
    },
    ["Ench. d'arme (Force)"] = {
        Professions = 23799,
    },
    ["Terrifier"] = {
        ["Squelette infernal"] = 7399,
        ["Déchiqueteur de Sneed"] = 7399,
        ["Scorpide terrifiant"] = 7399,
        ["Croc-d'effroi déviant"] = 7399,
        ["Azshir le Sans-sommeil"] = 7399,
        ["Pterreurdactyle frénétique"] = 7399,
        ["Rak'shiri"] = 7399,
    },
    ["Gardien de peur"] = {
        ["PRIEST"] = 6346,
    },
    ["Cape de givre en cuir"] = {
        Professions = 9198,
    },
    ["Renfort d'armure moyen"] = {
        Professions = 2165,
    },
    ["Nourrir le familier"] = {
        ["HUNTER"] = 6991,
    },
    ["Pierre à aiguiser élémentaire"] = {
        Professions = 22757,
    },
    ["Garde de l'ombre"] = {
        ["PRIEST"] = 19312,
    },
    ["Feu stellaire"] = {
        ["DRUID"] = 25298,
    },
    ["Epaulières en tisse-ombre"] = {
        Professions = 12076,
    },
    ["Captation de vie"] = {
        ["WARLOCK"] = 11695,
    },
    ["Aura de vindicte"] = {
        ["PALADIN"] = 10301,
        ["Généralissime Bolvar Fordragon"] = 8990,
        ["Commandant écarlate Mograine"] = 8990,
    },
    ["Créer la Quintessence éternelle"] = {
        ["Duc Hydraxis"] = 28439,
    },
    ["Chemise orange en tisse-mage"] = {
        Professions = 12061,
    },
    ["Changement de phase"] = {
        Pet = 4511,
    },
    ["Invocation d'un destrier infernal illusoire"] = {
        ["Cavale de cauchemar illusoire"] = 6905,
        ["Rêveur austère"] = 6905,
    },
    ["Agrandisseur de monde"] = {
        Professions = 23129,
    },
    ["Sac en tisse-mage"] = {
        Professions = 12065,
    },
    ["Poudre d'explosion dense"] = {
        Professions = 19788,
    },
    ["Bouclier de l'ombre"] = {
        ["Krethis Shadowspinner"] = 12040,
    },
    ["Elixir de Puissance du givre"] = {
        Professions = 21923,
    },
    ["Epaulières d'homme des collines"] = {
        Professions = 3768,
    },
    ["Bénédiction de sacrifice"] = {
        ["PALADIN"] = 20729,
    },
    ["Invocation d'un ravageur magram"] = {
        ["Ravageur mort-vivant"] = 18166,
        ["Nécromancien renégat"] = 18166,
    },
    ["Poison instantané"] = {
        ["ROGUE"] = 8681,
    },
    ["Fouette-bile"] = {
        ["Guerrier Fouette-bile"] = 12545,
        ["Hurleuse Fouette-bile"] = 12545,
        ["Maître de guerre Fouette-bile"] = 12545,
    },
    ["Gants rouges en tisse-mage"] = {
        Professions = 12066,
    },
    ["Elixir de Puissance de l'ombre"] = {
        Professions = 11476,
    },
    ["Cape de sauvegarde"] = {
        Professions = 22870,
    },
    ["Pouvoir des arcanes"] = {
        ["MAGE"] = 12042,
    },
    ["Charge de sapeur gobelin"] = {
        Professions = 12760,
    },
    ["Gilet Lueur-de-lune"] = {
        Professions = 8322,
    },
    ["Lance-flammes"] = {
        Pet = 25027,
    },
    ["Elixir de Maîtrise du feu supérieure"] = {
        Professions = 26277,
    },
    ["Apparition de Gangregarde enragé"] = {
        ["Gangregarde enragé"] = 22393,
    },
    ["Ench. de plastron (Vie mineure)"] = {
        Professions = 7420,
    },
    ["Ench. de plastron (Mana excellent)"] = {
        Professions = 13917,
    },
    ["Horion"] = {
        ["Chaman noir du crépuscule"] = 15500,
        ["Thrall"] = 16034,
        ["Exilé foudroyant"] = 11824,
        ["Destructeur kolkar"] = 11824,
        ["Oracle Arkkoran"] = 11824,
        ["Tourbillon Marchetempête"] = 11824,
        ["Patriarche Tête-tonnerre"] = 12553,
        ["Concubine Tête-tonnerre"] = 12553,
        ["Oracle Aileron noir"] = 2606,
        ["Patte-verte"] = 2606,
        ["Chaman Noirbois"] = 2606,
        ["Oracle mineur Aileron noir"] = 2607,
        ["Oracle Crache-sel"] = 2608,
        ["Embusquée Rougefurie"] = 2608,
        ["Elémentaliste du crépuscule"] = 2609,
        ["Nezzliok le Redoutable"] = 2610,
    },
    ["Hurlement de terreur"] = {
        ["WARLOCK"] = 17928,
    },
    ["Gilet vert en laine"] = {
        Professions = 2399,
    },
    ["Epée courte en fer trempé"] = {
        Professions = 3492,
    },
    ["Ceinture en tisse-fantôme"] = {
        Professions = 18410,
    },
    ["Pointe de bouclier en fer"] = {
        Professions = 7221,
    },
    ["Sagesse des Grumegueules"] = {
        Professions = 23662,
    },
    ["Boue projetée"] = {
        ["Ardo Crassepatte"] = 3650,
        ["Gnoll Fangemufle"] = 3650,
    },
    ["Grand balayage"] = {
        ["Seigneur Mok'Morokk"] = 6749,
    },
    ["Garrot"] = {
        ["ROGUE"] = 11290,
    },
    ["Potion de mana supérieure"] = {
        Professions = 11448,
    },
    ["Epaulières de l'Aube d'argent"] = {
        Professions = 23665,
    },
    ["Aura de précision"] = {
        ["HUNTER"] = 19506,
    },
    ["Jadefeu"] = {
        ["Satyre Jadefeu"] = 13578,
        ["Voleur Jadefeu"] = 13578,
        ["Entourloupeur Jadefeu"] = 13578,
        ["Traître Jadefeu"] = 13578,
        ["Lige de la corruption Jadefeu"] = 13578,
        ["Traqueur des ombres Jadefeu"] = 13578,
        ["Implorateur de l'enfer Jadefeu"] = 13578,
        ["Prince Xavalis"] = 13578,
    },
    ["Heurtoir"] = {
        ["WARRIOR"] = 11605,
        ["Garde de guerre de Tranchebauge"] = 11430,
    },
    ["Limace noire"] = {
        ["Limace bestiale"] = 7279,
        ["Rôdeur du goudron"] = 7279,
        ["Le Jonc"] = 7279,
        ["Vase pourrissante"] = 7279,
    },
    ["Rouleau d'étoffe de lin"] = {
        Professions = 2963,
    },
    ["Lame fanatique"] = {
        ["Fanatique de la Lame ardente"] = 5262,
    },
    ["Trait provocateur"] = {
        ["HUNTER"] = 15632,
    },
    ["Gantelets radieux"] = {
        Professions = 16654,
    },
    ["Malédiction de Thule"] = {
        ["Gnoll Poil-Putride"] = 3237,
        ["Bâtard Poil-Putride"] = 3237,
        ["Œil-de-ver"] = 3237,
        ["Coureur des champs Poil-Putride"] = 3237,
        ["Mystique Poil-Putride"] = 3237,
        ["Brute Poil-Putride"] = 3237,
        ["Pestilentiel Poil-Putride"] = 3237,
        ["Profanateur Poil-Putride"] = 3237,
        ["Sauvage Poil-Putride"] = 3237,
        ["Furieux Poil-Putride"] = 3237,
    },
    ["Invocation d'un zombie"] = {
        ["Zombie invoqué"] = 16590,
        ["Invocateur des ténèbres"] = 16590,
    },
    ["Casque de chantier gobelin"] = {
        Professions = 12718,
    },
    ["Comète, BLANCHE GRANDE"] = {
        ["Type des feux d'artifice de Pat - GRAND BLANC"] = 26355,
    },
    ["Toxine localisée"] = {
        ["Naga Ondulame"] = 7947,
        ["Guerrier Ondulame"] = 7947,
        ["Myrmidon Ondulame"] = 7947,
        ["Queue-rasoir Ondulame"] = 7947,
        ["Vipère déviante"] = 7947,
        ["Horreur toxique"] = 7947,
    },
    ["Tube de bronze"] = {
        Professions = 3938,
    },
    ["Invocation d'hydrogénide"] = {
        ["Hydrogénide"] = 22714,
    },
    ["Sac rouge en tisse-mage"] = {
        Professions = 12079,
    },
    ["Appel d'un worg pâle"] = {
        ["Worg pâle"] = 7487,
    },
    ["Infection volatile"] = {
        ["Vieux Troubloeil"] = 3584,
        ["Balgaras le Crasseux"] = 3586,
    },
    ["Jambières grossières en bronze"] = {
        Professions = 2668,
    },
    ["Pantalon en étoffe runique"] = {
        Professions = 18438,
    },
    ["Tunique en étoffe runique"] = {
        Professions = 18407,
    },
    ["Furie sanguinaire rapide"] = {
        ["Sous-chef Rivepatte"] = 3229,
        ["Chaman Mo'grosh"] = 3229,
        ["Grawmug"] = 3229,
    },
    ["Caillou solide"] = {
        Professions = 9921,
    },
    ["Bénédiction de protection"] = {
        ["PALADIN"] = 10278,
    },
    ["Portail : Thunder Bluff"] = {
        ["MAGE"] = 11420,
    },
    ["Invocation d'un flammide incendiaire"] = {
        ["Flammide incendiaire"] = 15710,
        ["Lueur terrifiante"] = 15710,
    },
    ["Augure de clarté"] = {
        ["DRUID"] = 16864,
    },
    ["Emprisonnement dans le cristal"] = {
        ["Commandant de la Garde funeste"] = 23020,
    },
    ["Infection de Crapaud-bile"] = {
        ["Crapaud-bile"] = 10251,
    },
    ["Maléfice de faiblesse"] = {
        ["PRIEST"] = 19285,
    },
    ["Boule de feu"] = {
        ["MAGE"] = 25306,
        ["Géomancien Blanche-moustache"] = 15228,
        ["Invocateur défias"] = 9053,
        ["Magicien défias"] = 9053,
        ["Implorateur de l’enfer de Xavian"] = 9053,
        ["Destructeur ardent"] = 9053,
        ["Magicien écarlate"] = 9053,
        ["Evocateur écarlate"] = 9053,
        ["Devin écarlate"] = 9053,
        ["Conjurateur écarlate"] = 9053,
        ["Dragonnet incendiaire"] = 9053,
        ["Sorcier défias"] = 9053,
        ["Géomètre Ombreforge"] = 9053,
        ["Géomancienne de la Tête de Mort"] = 9053,
        ["Roussi"] = 13375,
        ["Rejeton noir"] = 13375,
        ["Rejeton brûlant"] = 13375,
        ["Pilleur défias"] = 19816,
        ["Tisseur de flamme Brisepoing"] = 19816,
        ["Géomancien des Gravières"] = 19816,
        ["Adepte de la Lame ardente"] = 19816,
        ["Vil quasit"] = 11921,
        ["Géomancien du crépuscule"] = 14034,
        ["Wyrmide noir"] = 14034,
        ["Grande araignée de lave"] = 11985,
        ["Dragonnet égaré"] = 11839,
        ["Capitaine Balinda Stonehearth"] = 12466,
        ["Dragonnet noir"] = 20793,
        ["Mage renégat défias"] = 20793,
        ["Géomancien kobold"] = 20793,
        ["Surena Caledon"] = 20793,
        ["Initié écarlate"] = 20793,
        ["Mage de Dalaran"] = 20793,
        ["Ensorceleuse Plumesang"] = 20793,
        ["Géomancienne Dos-hirsute"] = 20793,
        ["Géomancienne Tranchecrin"] = 20793,
        ["Défricheur de la KapitalRisk"] = 20793,
        ["Invocateur Blackrock"] = 20793,
        ["Gibblewilt"] = 20793,
        ["Géomancien de Gogger"] = 20793,
        ["Morganth"] = 20811,
        ["Enchanteur défias"] = 20811,
        ["Magistrat Burnside"] = 20811,
        ["Géomancien du Totem sinistre"] = 20811,
        ["Sorcier du Syndicat"] = 20815,
        ["Invocateur de Dalaran"] = 20815,
        ["Implorateur de la terre gelkis"] = 20815,
        ["Mage de la Voile sanglante"] = 20823,
        ["Gardienne Belamoore"] = 20823,
        ["Feeboz"] = 20823,
        ["Géologue Sombrefer"] = 20823,
        ["Adepte de Jaedenar"] = 20823,
        ["Explorateur de Froidemine"] = 15242,
        ["Explorateur de Gouffrefer"] = 15242,
    },
    ["Pantalon de smoking"] = {
        Professions = 12089,
    },
    ["Entraves des morts-vivants"] = {
        ["PRIEST"] = 10955,
    },
    ["Horion de flammes"] = {
        ["SHAMAN"] = 29228,
        ["Primaliste Thurloga"] = 15616,
        ["Garneg Charskull"] = 15039,
    },
    ["Rapière éblouissante en mithril"] = {
        Professions = 10005,
    },
    ["Pluie de feu"] = {
        ["WARLOCK"] = 11678,
        ["Grel'borg l'Avare"] = 11990,
        ["Géomancien de Gogger"] = 11990,
    },
    ["Eau invoquée"] = {
        ["MAGE"] = 5350,
    },
    ["Cuirasse armée du scorpide"] = {
        Professions = 10525,
    },
    ["Petit dragon mécanique"] = {
        Professions = 3969,
    },
    ["Verrou magique"] = {
        Pet = 19647,
    },
    ["Invocation d'un hurleur des vents"] = {
        ["Hurleur des vents"] = 8271,
        ["Lève-poussière de Tranchebauge"] = 8271,
    },
    ["Battement d'aile"] = {
        ["Somnus"] = 12882,
    },
    ["Invocation d'un flammide enragé"] = {
        ["Flammide enragé"] = 15711,
        ["Lueur terrifiante"] = 15711,
    },
    ["Flammes de cautérisation"] = {
        ["Ancien chien du Magma"] = 19366,
    },
    ["Ench. de bracelets (Esprit mineur)"] = {
        Professions = 7766,
    },
    ["Souffle d'acide"] = {
        ["Jade"] = 12533,
        ["Seigneur-capitaine Wyrmak"] = 12533,
    },
    ["Toucher mortel"] = {
        ["Morbent Fel"] = 3108,
    },
    ["Potion de protection contre le Feu"] = {
        Professions = 7257,
    },
    ["Coup de tonnerre"] = {
        ["WARRIOR"] = 11581,
        ["Seigneur Brusquebroche"] = 15548,
        ["Garde-huran de Tranchebauge"] = 15548,
        ["Vanndar Stormpike"] = 15588,
        ["Grand faucon-tonnerre"] = 8078,
        ["Faucon-tonnerre des nuages"] = 8078,
        ["Lézard-tempête enragé des falaises"] = 8078,
        ["Garde d'Ironforge"] = 8078,
        ["Seigneur de guerre Kolkanis"] = 8078,
        ["Torek"] = 8078,
        ["Combattant Mosh'Ogg"] = 8147,
        ["Foudroyeur des falaises"] = 8147,
        ["Umi Thorson"] = 8147,
    },
    ["Jambières de guerre orques"] = {
        Professions = 9957,
    },
    ["Balle explosive"] = {
        ["Capitaine Keelhaul"] = 7896,
        ["Belluaire écarlate"] = 7896,
        ["Ingénieur de siège"] = 7896,
        ["Traître Follengeance"] = 7896,
    },
    ["Epaulières barbares en fer"] = {
        Professions = 9811,
    },
    ["Malédiction des langages"] = {
        ["WARLOCK"] = 11719,
    },
    ["Parler avec les têtes"] = {
        ["Kin'weelay"] = 3644,
    },
    ["Cri de commandement"] = {
        ["Capitaine Griffemort"] = 22440,
    },
    ["Jambières de l'ours de guerre"] = {
        Professions = 19080,
    },
    ["Micro-ajusteur gyromatique"] = {
        Professions = 12590,
    },
    ["Création de Pierre d'âme (supérieure)"] = {
        ["WARLOCK"] = 20756,
    },
    ["Ceinture en étoffe runique"] = {
        Professions = 18402,
    },
    ["Bonne fortune lunaire"] = {
        ["Type des chapelets de fusées de Pat (ELUNE)"] = 26522,
    },
    ["Parasite"] = {
        ["Oracle de Brassenoire"] = 8363,
    },
    ["Potion de soins mineure"] = {
        Professions = 2330,
    },
    ["Brûlure de mana"] = {
        ["PRIEST"] = 10876,
        ["Dragon-faë rusé"] = 17630,
        ["Acolyte de la Tête de Mort"] = 15785,
        ["Vision cauchemardesque"] = 11981,
        ["Traque-mana Hederine"] = 15980,
        ["Sorcière des mers Irequeue"] = 2691,
        ["Gangretueur"] = 2691,
        ["Gardien austère"] = 2691,
        ["Chasseur corrompu invoqué"] = 2691,
    },
    ["Fouet mental"] = {
        ["PRIEST"] = 18807,
    },
    ["Pantalon en lin écru"] = {
        Professions = 12044,
    },
    ["Coup de bouclier"] = {
        ["WARRIOR"] = 1672,
        ["Eclaireur défias"] = 11972,
        ["Gath'Ilzogg"] = 11972,
        ["Caporal Keeshan"] = 11972,
        ["Guerrier Bloodscalp"] = 11972,
        ["Guerrier Skullsplitter"] = 11972,
        ["Sentinelle du Syndicat"] = 11972,
        ["Soldat de marine de Kul Tiras"] = 11972,
        ["Défenseur écarlate"] = 11972,
        ["Défenseur de Tranchebauge"] = 11972,
        ["Khan Jehn"] = 11972,
        ["Commandant de la garde Zalaphil"] = 11972,
        ["Gardeffroi royal"] = 11972,
    },
    ["Epaulières en écailles dorées"] = {
        Professions = 3505,
    },
    ["Bottes lourdes en mithril"] = {
        Professions = 9968,
    },
    ["Cuir léger"] = {
        Professions = 2881,
    },
    ["Jambières de sabre-de-givre"] = {
        Professions = 19074,
    },
    ["Rupture du sol"] = {
        ["Tentacule griffu"] = 26139,
        ["Tentacule oculaire"] = 26139,
    },
    ["Forme de voyage"] = {
        ["DRUID"] = 783,
    },
    ["Epaulières tempétueuses"] = {
        Professions = 19090,
    },
    ["Posture défensive"] = {
        ["WARRIOR"] = 71,
        ["Garrick Padfoot"] = 7164,
        ["Gath'Ilzogg"] = 7164,
        ["Guerrier Sombrepoil"] = 7164,
        ["Centurion Dragonmaw"] = 7164,
        ["Tunnelier Sombrefer"] = 7164,
        ["Terrassier Brisepierre"] = 7164,
        ["Terrassier Rat des tunnels"] = 7164,
        ["Avant-garde écarlate"] = 7164,
        ["Capitaine Vachon"] = 7164,
        ["Kam Deepfury"] = 7164,
        ["Sentinelle de Bois-du-Bûcher"] = 7164,
        ["Fantassin d'Hillsbrad"] = 7164,
        ["Mineur d'Hillsbrad"] = 7164,
        ["Rôdeur du rivage Daguéchine"] = 7164,
        ["Terrassier Sèche-moustache"] = 7164,
        ["Défenseur de Stromgarde"] = 7164,
        ["Tunnelier Ombreforge"] = 7164,
        ["Lieutenant Benedict"] = 7164,
        ["Excavateur de Bael'dun"] = 7164,
        ["Soldat de marine de Theramore"] = 7164,
        ["Guerrier Crache-sel"] = 7164,
        ["Gardien écarlate"] = 7164,
        ["Défenseur écarlate"] = 7164,
        ["Défenseur de Tranchebauge"] = 7164,
        ["Gladiateur Blackrock"] = 7164,
        ["Marcel Dabyrie"] = 7164,
        ["Champion garde-huran"] = 7164,
        ["Satyre Sans-racines"] = 7164,
        ["Murloc Noctant"] = 7164,
        ["Terrassier Ombreforge"] = 7164,
        ["Garde Edward"] = 7164,
        ["Garde Jarad"] = 7164,
        ["Garde Kahil"] = 7164,
        ["Garde Narrisha"] = 7164,
        ["Garde Tark"] = 7164,
        ["Khan Jehn"] = 7164,
        ["Commandant de la garde Zalaphil"] = 7164,
        ["Vejrek"] = 7164,
        ["Creuseur des Basses-cavernes"] = 7164,
        ["Soldat de Dun Garok"] = 7164,
        ["Défenseur du Refuge de l'Ornière"] = 7164,
    },
    ["L'agilité d'Agammagan"] = {
        ["Mangletooth"] = 17013,
    },
    ["Humus rampant"] = {
        ["Sourcier Tranchecrin"] = 6278,
        ["Marche-boue Aileron boueux"] = 6278,
    },
    ["Pantalon rouge en tisse-mage"] = {
        Professions = 12060,
    },
    ["Créer la Quintessence aquatique"] = {
        ["Duc Hydraxis"] = 21357,
    },
    ["Gardiens d'Hukku"] = {
        ["Diablotin d'Hukku"] = 12790,
        ["Hukku"] = 12790,
    },
    ["Laissé pour mort"] = {
        ["Agathelos l'Enragé"] = 8555,
    },
    ["Gantelets en plaques de feu"] = {
        Professions = 16655,
    },
    ["Grand enchaînement"] = {
        ["Ravageur silithide"] = 8255,
        ["Razelikh le Souilleur"] = 8255,
    },
    ["Ench. de cape (Résistance à l'Ombre inf.)"] = {
        Professions = 13522,
    },
    ["Serpentine de jade"] = {
        Professions = 3493,
    },
    ["Sacrifice"] = {
        Pet = 19443,
    },
    ["Rocher"] = {
        ["Chasseur Wyrm Crache-poussières"] = 9483,
    },
    ["Silex à aiguiser brut"] = {
        Professions = 3320,
    },
    ["Eclair de tempête"] = {
        ["Roi Magni Bronzebeard"] = 20685,
        ["Vanndar Stormpike"] = 19136,
    },
    ["Dérobade"] = {
        ["DRUID"] = 9892,
        Pet = 16697,
    },
    ["Dague à perles"] = {
        Professions = 6517,
    },
    ["Projeter"] = {
        ["Bom'bay"] = 16716,
    },
    ["Gants en tisse-rêve"] = {
        Professions = 12067,
    },
    ["Asservir"] = {
        ["Geôlier Eston"] = 3442,
    },
    ["Endurance altérée"] = {
        ["Protecteur Vile-sylve"] = 6819,
        ["Chênepatte"] = 6819,
    },
    ["Dissipation de la magie"] = {
        ["PRIEST"] = 988,
    },
    ["Jet de vapeur"] = {
        ["Forge-vapeur Sombrefer"] = 11983,
        ["Elémentaire bouillonnant"] = 11983,
    },
    ["Epouvante"] = {
        ["Nelson le Gentil"] = 23275,
    },
    ["Jet d'encre"] = {
        ["Cracheur des marais"] = 9612,
    },
    ["Caillou brut"] = {
        Professions = 3115,
    },
    ["Maîtrise élémentaire"] = {
        ["SHAMAN"] = 16166,
    },
    ["Totem de Purification du poison"] = {
        ["SHAMAN"] = 8166,
    },
    ["Protection contre l'Ombre"] = {
        ["PRIEST"] = 10958,
    },
    ["Poison mortel III"] = {
        ["ROGUE"] = 11357,
    },
    ["Pantalon en laine épaisse"] = {
        Professions = 3850,
    },
    ["Contrecoup de métamorphose"] = {
        ["Clone de métamorphose"] = 28406,
        ["Guerrier Fouette-bile"] = 28406,
        ["Hurleuse Fouette-bile"] = 28406,
        ["Garde-serpent Fouette-bile"] = 28406,
        ["Sirène Fouette-bile"] = 28406,
    },
    ["Cuirasse en écailles de dragon noir"] = {
        Professions = 19085,
    },
    ["Piétinement de kodo"] = {
        ["Kodo égaré des Tarides"] = 6266,
        ["Kodo des Tarides"] = 6266,
    },
    ["Bottes armées du scorpide"] = {
        Professions = 10554,
    },
    ["Gants en écailles de déviant"] = {
        Professions = 7954,
    },
    ["Charge"] = {
        ["WARRIOR"] = 11578,
        Pet = 27685,
    },
    ["Gantelets de la mer"] = {
        Professions = 10630,
    },
    ["Bâtonnet en cuivre"] = {
        Professions = 6217,
    },
    ["Totem de résistance au Feu"] = {
        ["SHAMAN"] = 10538,
    },
    ["Piège explosif"] = {
        ["HUNTER"] = 14317,
    },
    ["Bottes de sabre-de-givre"] = {
        Professions = 19066,
    },
    ["Invocation d'une agate de mana"] = {
        ["MAGE"] = 759,
    },
    ["Purification de serpentine"] = {
        ["Disciple de Naralex"] = 6270,
    },
    ["Gifle !"] = {
        ["Milton Beats"] = 6754,
    },
    ["Invocation d'une jeune araignée aiguillon"] = {
        ["Jeune araignée du pic"] = 16103,
        ["Araignée du pic"] = 16103,
    },
    ["Huile de sorcier inférieure"] = {
        Professions = 25126,
    },
    ["Gantelets diablosaures"] = {
        Professions = 19084,
    },
    ["Ténacité brûlante"] = {
        ["Telf Joolam"] = 8383,
    },
    ["Créer la Cache de Mau'ari"] = {
        ["Sorcier-docteur Mau'ari"] = 16351,
    },
    ["Sac vert en soie"] = {
        Professions = 6693,
    },
    ["Lunette basique"] = {
        Professions = 3977,
    },
    ["Transmutation de mithril en vrai-argent"] = {
        Professions = 11480,
    },
    ["Fonte de l'acier"] = {
        Professions = 3569,
    },
    ["Brassards runiques en cuir"] = {
        Professions = 19065,
    },
    ["Poison de distraction mentale III"] = {
        ["ROGUE"] = 11400,
    },
    ["Ench. de cape (Agilité mineure)"] = {
        Professions = 13419,
    },
    ["Epaulières renforcées en laine"] = {
        Professions = 3849,
    },
    ["Affaiblir"] = {
        ["Perdu décati"] = 11963,
        ["Bien-né survivant"] = 11963,
    },
    ["Déclencheur instable"] = {
        Professions = 12591,
    },
    ["Potion de sang de troll majeure"] = {
        Professions = 24368,
    },
    ["Totem de Purification des maladies"] = {
        ["SHAMAN"] = 8170,
    },
    ["Potion de purification"] = {
        Professions = 17572,
    },
    ["Kriss d'ébène"] = {
        Professions = 10013,
    },
    ["Epée d'acier lunaire"] = {
        Professions = 3496,
    },
    ["Bandeau rouge en tisse-mage"] = {
        Professions = 12084,
    },
    ["Abolir maladie"] = {
        ["PRIEST"] = 552,
    },
    ["Gants en étoffe cendrée"] = {
        Professions = 18412,
    },
    ["Sac en laine"] = {
        Professions = 3757,
    },
    ["Ordre de combat"] = {
        ["Surveillant défias"] = 5115,
        ["Contremaître d'Hillsbrad"] = 5115,
        ["Sous-chef Sombrefer"] = 5115,
        ["Contremaître de Rempart-du-Néant"] = 5115,
        ["Chevaucheur de sanguinaire"] = 5115,
    },
    ["Ench. de bouclier (Esprit inférieur)"] = {
        Professions = 13485,
    },
    ["Agrandir"] = {
        ["Erudit du crépuscule"] = 8365,
    },
    ["Purification"] = {
        ["PALADIN"] = 1152,
    },
    ["Souffle d'Acide corrosif"] = {
        ["Somnus"] = 20667,
    },
    ["Gilet bleu en lin"] = {
        Professions = 7630,
    },
    ["Trait de lézard"] = {
        ["Lézard-tonnerre"] = 5401,
        ["Foudroyant"] = 5401,
        ["Orageux"] = 5401,
        ["Tête-tonnerre"] = 5401,
        ["Groin-tempête"] = 5401,
        ["Lézard-tempête des falaises"] = 5401,
        ["Lézard-tonnerre enragé"] = 5401,
        ["Ancien lézard-tonnerre"] = 5401,
    },
    ["Brassards en écailles de scorpide épaisses"] = {
        Professions = 19048,
    },
    ["Gemme du néant"] = {
        ["Bethor Iceshard"] = 7673,
    },
    ["Malédiction de l'ombre"] = {
        ["WARLOCK"] = 17937,
    },
    ["Hache en cuivre"] = {
        Professions = 2738,
    },
    ["Cône de froid"] = {
        ["MAGE"] = 10161,
    },
    ["Aiguille de mana"] = {
        ["Charlga Trancheflanc"] = 8358,
    },
    ["Peau de démon"] = {
        ["WARLOCK"] = 696,
        ["Exhalombre Frostmane"] = 20798,
        ["Gazz'uz"] = 20798,
    },
    ["Points d'honneur +228"] = {
        ["Porteguerre de la Horde"] = 24963,
        ["Général de brigade de l'Alliance"] = 24963,
    },
    ["Champignon d'Aileron boueux"] = {
        ["Marche-boue Aileron boueux"] = 9462,
        ["Cours-la-côte Aileron boueux"] = 9462,
    },
    ["Crachat de venin corrosif"] = {
        ["Chimaerok"] = 20629,
    },
    ["Pare-glace"] = {
        Professions = 3957,
    },
    ["Maîtrise du blocage"] = {
        ["WARRIOR"] = 2565,
        ["Garde de Stormwind"] = 12169,
        ["Garde du corps écarlate"] = 12169,
        ["Sentinelle de Bois-du-Bûcher"] = 12169,
        ["Défenseur spectral"] = 12169,
        ["Défenseur Stormpike"] = 12169,
        ["Gardien Frostwolf"] = 12169,
        ["Aggi Rumblestomp"] = 12169,
        ["Défenseur aguerri"] = 12169,
        ["Gardien aguerri"] = 12169,
        ["Défenseur vétéran"] = 12169,
        ["Gardien vétéran"] = 12169,
        ["Gardien champion"] = 12169,
        ["Défenseur champion"] = 12169,
        ["Caporal Noreg Stormpike"] = 12169,
    },
    ["Coeur fumant de la montagne"] = {
        Professions = 15596,
    },
    ["Poudre d'explosion majeure"] = {
        Professions = 3945,
    },
    ["Berserker"] = {
        Racial = 26296,
        Racial = 26297,
        Racial = 20554,
    },
    ["Gants en tisse-fantôme"] = {
        Professions = 18413,
    },
    ["Appel des ravageurs"] = {
        ["Ravageur Gordok"] = 22860,
    },
    ["Déchirure du tendon"] = {
        ["Nefaru"] = 3604,
        ["Téthis"] = 3604,
        ["Ancien tranchegueule"] = 3604,
        ["Mordeur Dents-de-scie"] = 3604,
        ["Crocilisque des rivières"] = 3604,
        ["Crocilisque marin"] = 3604,
        ["Crocilisque Gueule d'acier"] = 3604,
        ["La Bogue"] = 3604,
        ["Crocilisque géant des Paluns"] = 3604,
        ["Ancien crocilisque marin"] = 3604,
        ["Arracheur Sombretaillis"] = 3604,
        ["Déchireur Frayeloup"] = 3604,
        ["Claqueur Bourbecoque"] = 3604,
        ["Battraileron du bassin profond"] = 3604,
        ["Crocilisque déviant"] = 3604,
        ["Snort la Moqueuse"] = 3604,
        ["Claque-pinces Makrinni"] = 3604,
        ["Ravasaure chasseur"] = 3604,
        ["Ours Crocs acérés"] = 3604,
        ["Chardon de glace insoumis"] = 3604,
        ["Yéti Chardon de glace"] = 3604,
        ["Trancheserre"] = 3604,
        ["Vieux Saute-falaise"] = 3604,
        ["Grunter"] = 3604,
        ["Molosse de mana"] = 3604,
        ["Paysan Ombreforge"] = 3604,
        ["Charognard Gangrepatte"] = 3604,
        ["Jeune dimeurtrodon"] = 3604,
        ["Compagne de Lar'korwi"] = 3604,
        ["Lar'korwi"] = 3604,
        ["Worg de braise écumant"] = 3604,
        ["Gueule-du-trépas"] = 3604,
        ["Bayne"] = 3604,
        ["Hurleur des brumes"] = 3604,
        ["Shy-Rotam"] = 3604,
        ["Sian-Rotam"] = 3604,
        ["Pinceur Cinglepierre"] = 3604,
        ["Mastiff Gordok"] = 3604,
        ["Traqueur du crépuscule"] = 3604,
        ["Crocilisque zulien"] = 3604,
    },
    ["Claymore en cuivre"] = {
        Professions = 9983,
    },
    ["Créer le Manuel d'utilisateur du Pilon"] = {
        ["J.D. Collie"] = 15211,
    },
    ["Forme d'ours"] = {
        ["Kerlonian Evershade"] = 18309,
    },
    ["Transformation de Warosh"] = {
        ["Warosh"] = 16801,
    },
    ["Barrière de glace"] = {
        ["MAGE"] = 13033,
    },
    ["Visuel de l'ombre"] = {
        ["Ombre de Jin'do"] = 24313,
    },
    ["Brûlure d'âmes"] = {
        ["Teremus le Dévoreur"] = 12667,
    },
    ["Malédiction de Tuten'kash"] = {
        ["Tuten'kash"] = 12255,
    },
    ["Ench. de cape (Résistance au Feu)"] = {
        Professions = 13657,
    },
    ["Huile de sorcier mineure"] = {
        Professions = 25124,
    },
    ["Prière de protection contre l'Ombre"] = {
        ["PRIEST"] = 27683,
    },
    ["Traquenard"] = {
        ["DRUID"] = 9827,
    },
    ["Poison de distraction mentale II"] = {
        ["ROGUE"] = 8694,
    },
    ["Renfort d'armure lourd"] = {
        Professions = 3780,
    },
    ["Brassards barbares"] = {
        Professions = 23399,
    },
    ["Sonner"] = {
        ["DRUID"] = 8983,
    },
    ["Défibrillateur gobelin"] = {
        Professions = 9273,
    },
    ["Invocation d'un vétéran Blackhand"] = {
        ["Vétéran Blackhand invoqué"] = 15792,
        ["Invocateur Blackhand"] = 15792,
    },
    ["Invocation de bourdons de la Ruche'Ashi"] = {
        ["Bourdon de la Ruche'Ashi"] = 21327,
        ["Druide torturé"] = 21327,
        ["Sentinelle torturée"] = 21327,
    },
    ["Bâtonnet runique en arcanite"] = {
        Professions = 20051,
    },
    ["Séisme de Myzrael"] = {
        ["Myzrael"] = 4938,
    },
    ["Dynamite Ev-Lan"] = {
        Professions = 8339,
    },
    ["Chapelet de grandes comètes rouges"] = {
        Professions = 26428,
    },
    ["Brassards armés du scorpide"] = {
        Professions = 10533,
    },
    ["Volonté de Hakkar"] = {
        ["Gurubashi"] = 24178,
    },
    ["Don de Ragnaros"] = {
        ["Nain Sombrefer"] = 7891,
        ["Saboteur Sombrefer"] = 7891,
        ["Tunnelier Sombrefer"] = 7891,
        ["Démolisseur Sombrefer"] = 7891,
        ["Fusilier Sombrefer"] = 7891,
    },
    ["Bottes noires en cuir"] = {
        Professions = 2167,
    },
    ["Philtre d'amour de Nagmara"] = {
        ["Gouvernante Nagmara"] = 14928,
    },
    ["Rapidité de la nature"] = {
        ["DRUID"] = 17116,
        ["SHAMAN"] = 16188,
    },
    ["Eclat lunaire"] = {
        ["DRUID"] = 9835,
        ["Tyranis Malem"] = 15798,
        ["Oracle sélénien"] = 15798,
        ["Archidruide Renferal"] = 22206,
    },
    ["Disparition du Prophète ardent"] = {
        ["Pyrogarde Prophète ardent"] = 16078,
    },
    ["Epaulières grossières en bronze"] = {
        Professions = 3328,
    },
    ["Morsure d'âme"] = {
        ["Mangeur d’âmes Vilebranch"] = 11016,
        ["Mangeur d'âmes Sandfury"] = 11016,
    },
    ["Invocation de l’esprit d’Hammertoe"] = {
        ["Esprit d’Hammertoe"] = 4985,
        ["Historien Karnik"] = 4985,
    },
    ["Ench. de gants (Agilité)"] = {
        Professions = 13815,
    },
    ["Bottes-fusées gnomes"] = {
        Professions = 12905,
    },
    ["Maître-neige 9000"] = {
        Professions = 21940,
    },
    ["Feindre la mort"] = {
        ["HUNTER"] = 5384,
    },
    ["Poison instantané II"] = {
        ["ROGUE"] = 8687,
    },
    ["Pantalon marron en lin"] = {
        Professions = 3914,
    },
    ["Grenade en fer"] = {
        Professions = 3962,
    },
    ["Invocation d'un diablotin"] = {
        ["WARLOCK"] = 688,
        ["Diablotin serviteur"] = 11939,
        ["Exhalombre Witherbark"] = 11939,
        ["Sectateur de la Lame ardente"] = 11939,
        ["Fizzle Darkstorm"] = 11939,
        ["Sectateur de la Rive noire"] = 11939,
        ["Invocateur infernal Mornecoeur"] = 11939,
        ["Balizar l’Ombrageux"] = 11939,
        ["Invocateur Blackrock"] = 11939,
        ["Invocateur de la Lame ardente"] = 11939,
        ["Kayla Smithe"] = 11939,
        ["Gina Lang"] = 11939,
        ["Dane Winslow"] = 11939,
        ["Cylina Darkheart"] = 11939,
        ["Wren Darkspring"] = 11939,
        ["Morloch"] = 11939,
    },
    ["Tunique de la nuit"] = {
        Professions = 10499,
    },
    ["Vigne flagellante"] = {
        ["Ecorcheur Pétale-de-sang"] = 14112,
    },
    ["Frappe mortelle"] = {
        ["WARRIOR"] = 21553,
        ["Lieutenant Rugba"] = 15708,
        ["Lieutenant Spencer"] = 15708,
        ["Lieutenant Stronghoof"] = 15708,
        ["Lieutenant Vol'talar"] = 15708,
        ["Lieutenant Grummus"] = 15708,
        ["Lieutenant Murp"] = 15708,
        ["Lieutenant Lewis"] = 15708,
        ["Lieutenant Largent"] = 15708,
        ["Lieutenant Stouthandle"] = 15708,
        ["Lieutenant Greywand"] = 15708,
        ["Lieutenant Lonadin"] = 15708,
        ["Guerrier Strashaz"] = 16856,
        ["Pourfendeur Hederine"] = 16856,
        ["Capitaine Galvangar"] = 16856,
        ["Garde de Gouffrefer"] = 16856,
        ["Garde de Gouffrefer vétéran"] = 16856,
        ["Garde de Gouffrefer champion"] = 16856,
        ["Commandant Chevaucheur de bélier Stormpike"] = 16856,
        ["Jotek"] = 16856,
    },
    ["Totem Sentinelle"] = {
        ["SHAMAN"] = 6495,
    },
    ["Création de Pierre de soins (inférieure)"] = {
        ["WARLOCK"] = 6202,
    },
    ["Obus en thorium"] = {
        Professions = 19800,
    },
    ["Brassards en écailles de mithril"] = {
        Professions = 9937,
    },
    ["Jambières en écailles de tortue"] = {
        Professions = 10556,
    },
    ["Bénédiction de lumière supérieure"] = {
        ["PALADIN"] = 25890,
    },
    ["Fusée verte"] = {
        Professions = 23068,
    },
    ["Porte de la mort"] = {
        ["Paysan blessé"] = 23127,
        ["Paysan contaminé"] = 23127,
    },
    ["Dépeçage"] = {
        Professions = 8617,
        Professions = 8618,
        Professions = 10768,
    },
    ["Mortier portable en bronze"] = {
        Professions = 3960,
    },
    ["Représailles"] = {
        ["WARRIOR"] = 20230,
    },
    ["Serviteur de Morganth"] = {
        ["Serviteur de Morganth"] = 3611,
        ["Morganth"] = 3611,
    },
    ["Harnois en sombrefer"] = {
        Professions = 15296,
    },
    ["Elixir de Détection de l'invisibilité inférieure"] = {
        Professions = 3453,
    },
    ["Résistance au Feu"] = {
        ["HUNTER"] = 24464,
    },
    ["Gants ornés en mithril"] = {
        Professions = 9950,
    },
    ["Balle lourde fabriquée"] = {
        Professions = 3930,
    },
    ["Potion de mana"] = {
        Professions = 3452,
    },
    ["Bénédiction de puissance supérieure"] = {
        ["PALADIN"] = 25916,
    },
    ["Morsure fatale"] = {
        ["Pourfendeur des cryptes"] = 17170,
        ["Krellack"] = 17170,
    },
    ["Grenade en thorium"] = {
        Professions = 19790,
    },
    ["Marque des flammes"] = {
        ["Grunt de Brandefeu"] = 15128,
        ["Légionnaire de Brandefeu"] = 15128,
        ["Tisseur d’ombre de Brandefeu"] = 15128,
        ["Invocateur de Brandefeu"] = 15128,
        ["Tisseur d’effroi de Brandefeu"] = 15128,
        ["Pyromancien de Brandefeu"] = 15128,
    },
    ["Convertisseur d'arcanite délicat"] = {
        Professions = 19815,
    },
    ["Elixir de Taille de géant"] = {
        Professions = 8240,
    },
    ["Dynamite dense"] = {
        Professions = 23070,
    },
    ["Potion du druide"] = {
        ["Disciple de Naralex"] = 8141,
    },
    ["Fatigué"] = {
        ["Manouvrier Ravenclaw"] = 3271,
    },
    ["Contusion"] = {
        ["Cogneur de la Lame ardente"] = 4134,
    },
    ["Tremblement psychique"] = {
        ["Brise-terre de Tranchebauge"] = 8272,
    },
    ["Aiguillon entropique"] = {
        ["Franklin l'Amical"] = 23260,
    },
    ["Création d'une Invitation à la Fête lunaire"] = {
        ["Messagère de la fête lunaire"] = 26375,
    },
    ["Limon corrosif"] = {
        ["Rôdeur corrosif"] = 9459,
    },
    ["Création de Pierre d'âme (mineure)"] = {
        ["WARLOCK"] = 693,
    },
    ["Préparation"] = {
        ["ROGUE"] = 14185,
    },
    ["Ceinture radieuse"] = {
        Professions = 16645,
    },
    ["Potion de protection contre la Nature"] = {
        Professions = 7259,
    },
    ["Sac d'herbes cénarien"] = {
        Professions = 27724,
    },
    ["Bottes en gangrétoffe"] = {
        Professions = 18437,
    },
    ["Portail : Darnassus"] = {
        ["MAGE"] = 11419,
    },
    ["Robe d'Arcana"] = {
        Professions = 6692,
    },
    ["Rouleau de tisse-mage"] = {
        Professions = 3865,
    },
    ["Chaos chromatique"] = {
        ["Seigneur Victor Nefarius"] = 16337,
    },
    ["Illumination des arcanes"] = {
        ["MAGE"] = 23028,
    },
    ["Elixir de défense supérieure"] = {
        Professions = 11450,
    },
    ["Grondement"] = {
        ["DRUID"] = 6795,
        ["HUNTER"] = 14927,
        Pet = 14921,
    },
    ["Riposte"] = {
        ["ROGUE"] = 14251,
    },
    ["Jambières en écailles de dragon vert"] = {
        Professions = 19060,
    },
    ["Potion de sang de troll faible"] = {
        Professions = 3170,
    },
    ["Potion de protection contre les Arcanes supérieure"] = {
        Professions = 17577,
    },
    ["Clé plate"] = {
        Professions = 7430,
    },
    ["Attraper une arme"] = {
        ["Pillard des Basses-Cavernes"] = 10851,
    },
    ["Sang maudit"] = {
        ["Agam'ar décrépi"] = 8267,
        ["Agam'ar décrépi"] = 8268,
    },
    ["Gants en cuir raffermi"] = {
        Professions = 3770,
    },
    ["Petit Balayage"] = {
        ["Zzarc' Vul"] = 8716,
        ["Frère Ravenoak"] = 8716,
        ["Grizzly de la griffe colérique"] = 8716,
    },
    ["Totem Nova de feu"] = {
        ["SHAMAN"] = 11315,
    },
    ["Flacon de résistance chromatique"] = {
        Professions = 17638,
    },
    ["Toxine de Limace"] = {
        ["Limace de jade"] = 6814,
    },
    ["Souvenirs enrageants"] = {
        ["Mor'Ladim"] = 3547,
    },
    ["Hibernation"] = {
        ["DRUID"] = 18658,
    },
    ["Prière d'Esprit"] = {
        ["PRIEST"] = 27681,
    },
    ["Visuel d'Ombre spirituelle"] = {
        ["Ombre spirituelle"] = 24809,
    },
    ["Elixir de Force d'ogre"] = {
        Professions = 3188,
    },
    ["Aura de flammes"] = {
        ["Capitaine Griffemort"] = 22436,
    },
    ["Gilet barbare en lin"] = {
        Professions = 2395,
    },
    ["Venin enivrant"] = {
        ["Fils du venin Razzashi"] = 24596,
    },
    ["Invocation d'un cheval de guerre"] = {
        ["PALADIN"] = 13819,
    },
    ["Ench. d'arme (Tueur de bête mineur)"] = {
        Professions = 7786,
    },
    ["Don d’Arugal"] = {
        ["Fils d'Arugal"] = 7124,
    },
    ["Clone"] = {
        ["Ectoplasme cloné"] = 7952,
        ["Ectoplasme dévorant"] = 7952,
        ["Limon cloné"] = 14146,
        ["Limon primitif"] = 14146,
    },
    ["Pantalon en étoffe cendrée"] = {
        Professions = 18434,
    },
    ["Cylindre damasquiné en mithril"] = {
        Professions = 11454,
        Professions = 12895,
    },
    ["Invocation d'un crâne humain"] = {
        ["Crâne humain"] = 19031,
    },
    ["Pantalon en étoffe lumineuse"] = {
        Professions = 18439,
    },
    ["Poudre d'explosion basique"] = {
        Professions = 3918,
    },
    ["Comète, VERTE GRANDE"] = {
        ["Type des feux d'artifice de Pat - GRAND VERT"] = 26352,
    },
    ["Gelée urticante"] = {
        ["Larve putride"] = 16449,
        ["Dévoreur putride"] = 16449,
        ["Ver pestiféré"] = 16449,
    },
    ["Bourse d'âme"] = {
        Professions = 26085,
    },
    ["Casque en cuir sauvage"] = {
        Professions = 10546,
    },
    ["L'esprit du vent"] = {
        ["Mangletooth"] = 16618,
    },
    ["Invocation d'un éclat d'obsidienne"] = {
        ["Eclat d'obsidienne"] = 10061,
    },
    ["Forme d'Ombre"] = {
        ["PRIEST"] = 15473,
    },
    ["Ejecte Sneed"] = {
        ["Sneed"] = 5141,
        ["Déchiqueteur de Sneed"] = 5141,
    },
    ["Espingole amoureusement construite"] = {
        Professions = 3939,
    },
    ["Jambières ornées en mithril"] = {
        Professions = 9945,
    },
    ["Gants en tisse-ombre"] = {
        Professions = 12071,
    },
    ["Lentille verte"] = {
        Professions = 12622,
    },
    ["Compétence des Réprouvés"] = {
        ["Officier tourmenté"] = 7054,
    },
    ["Ench. de bracelets (Esprit excellent)"] = {
        Professions = 20009,
    },
    ["Intervention divine"] = {
        ["PALADIN"] = 19752,
    },
    ["Sceau de justice"] = {
        ["PALADIN"] = 20164,
    },
    ["Attaque mentale"] = {
        ["PRIEST"] = 10947,
        ["Sirène Strashaz"] = 15587,
        ["Seigneur du crépuscule Kelris"] = 15587,
        ["Vision cauchemardesque"] = 13860,
    },
    ["Epuration"] = {
        ["PALADIN"] = 4987,
    },
    ["Lucioles (farouche)"] = {
        ["DRUID"] = 17392,
    },
    ["Ench. de cape (Défense excellente)"] = {
        Professions = 20015,
    },
    ["Bottes en thorium"] = {
        Professions = 16652,
    },
    ["Elixir de force du lion"] = {
        Professions = 2329,
    },
    ["Mitrailleuse"] = {
        ["Mécano-Tank"] = 10346,
    },
    ["Gants élégants en cuir"] = {
        Professions = 2164,
    },
    ["Forme de sélénien"] = {
        ["DRUID"] = 24858,
    },
    ["Gants d'agilité en cuir"] = {
        Professions = 9074,
    },
    ["Immolation"] = {
        ["WARLOCK"] = 25309,
        ["Interrogateur Vishas"] = 9034,
        ["Tortionnaire écarlate"] = 9275,
        ["Initiée Hederine"] = 17883,
        ["Bête d'entropie"] = 15661,
        ["Sectateur de la Lame ardente"] = 11962,
        ["Thule Ravenclaw"] = 20800,
        ["Jergosh l'Invocateur"] = 20800,
    },
    ["Cape cramoisie en soie"] = {
        Professions = 8789,
    },
    ["Leurre"] = {
        Professions = 3932,
    },
    ["Ceinture mate"] = {
        Professions = 9206,
    },
    ["Mur protecteur"] = {
        ["WARRIOR"] = 871,
    },
    ["Grondeterre"] = {
        Pet = 26188,
    },
    ["Carabine en sombrefer"] = {
        Professions = 19796,
    },
    ["Invocation d'un cadavre démembré"] = {
        ["Cadavre démembré"] = 16324,
        ["Cadavre en décomposition"] = 16324,
    },
    ["Frappe"] = {
        ["Vieux Vile mâchoire"] = 13446,
        ["Garde de Froidemine"] = 15580,
        ["Chef d'escadrille Guse"] = 15580,
        ["Chef d'escadrille Jeztor"] = 15580,
        ["Chef d'escadrille Mulverick"] = 15580,
        ["Chef d'escadrille Ichman"] = 15580,
        ["Chef d'escadrille Slidore"] = 15580,
        ["Garde de Froidemine vétéran"] = 15580,
        ["Détrousseur ivre"] = 13584,
        ["Brack"] = 11976,
        ["Briseur d’os Brisepierre"] = 11976,
        ["Dextren Ward"] = 11976,
        ["Capitaine Melrache"] = 11976,
        ["Fermier de Solliden"] = 11976,
        ["Guerrier Pin-tordu"] = 11976,
        ["Mutileur Cassecrête"] = 11976,
        ["Briseur d’os Cavepierre"] = 11976,
        ["Maraudeur kolkar"] = 11976,
        ["Myrmidon Irequeue"] = 11976,
        ["Kobold Mufle-de-gravier"] = 11976,
        ["Garde Edward"] = 11976,
        ["Garde Jarad"] = 11976,
        ["Garde Kahil"] = 11976,
        ["Garde Lana"] = 11976,
        ["Garde Narrisha"] = 11976,
        ["Garde Tark"] = 11976,
        ["Officier de Rempart-du-Néant"] = 11976,
        ["Guerrier de la baie des tempêtes"] = 11976,
        ["Guerrier du Hangar"] = 11976,
        ["Flagglemurk le Cruel"] = 11976,
        ["Chef de guerre Krom'zar"] = 11976,
        ["Envahisseur kolkar"] = 11976,
        ["Ravageur du Totem sinistre"] = 11976,
        ["Défenseur spectral"] = 11976,
        ["Trogg Ragefeu"] = 11976,
        ["Garde Stormpike"] = 11976,
        ["Montagnard Boombellow"] = 11976,
        ["Threggil"] = 11976,
        ["Somnus"] = 18368,
        ["Mineur squelette"] = 12057,
        ["Garde-serpent Strashaz"] = 12057,
        ["Seigneur Salvassio"] = 12057,
        ["Terrassier Blanche-moustache"] = 12057,
        ["Umi Thorson"] = 12057,
        ["Envahisseur de Froidemine"] = 12057,
        ["Trogg de Gouffrefer"] = 14516,
        ["Gardeffroi royal"] = 14516,
    },
    ["Déclencheur de Thekal"] = {
        ["Zélote Lor'Khan"] = 24172,
        ["Zélote Zath"] = 24172,
    },
    ["Robe de la nuit hivernale"] = {
        Professions = 18436,
    },
    ["Gants de fabricant d'arcs"] = {
        Professions = 9145,
    },
    ["Ench. de bottes (Endurance mineure)"] = {
        Professions = 7863,
    },
    ["Invocation d'un chien écarlate"] = {
        ["Chien écarlate"] = 17164,
        ["Chasseur écarlate"] = 17164,
    },
    ["Fureur démoniaque"] = {
        ["Démon squelette"] = 3416,
    },
    ["Hache lumineuse bleue"] = {
        Professions = 9995,
    },
    ["Invocation d'une explosion de lave B"] = {
        ["Rat du Magma"] = 21900,
    },
    ["Ench. de bracelets (Esprit inférieur)"] = {
        Professions = 7859,
    },
    ["Bénédiction de liberté"] = {
        ["PALADIN"] = 1044,
    },
    ["Chemise blanche habillée"] = {
        Professions = 3871,
    },
    ["Brassards runiques en cuivre"] = {
        Professions = 2664,
    },
    ["Potion de sommeil sans rêve"] = {
        Professions = 15833,
    },
    ["Sceau de piété"] = {
        ["PALADIN"] = 20293,
    },
    ["Création de Pierre de soins (supérieure)"] = {
        ["WARLOCK"] = 11729,
    },
    ["Invocation d'un Bâton de commandement"] = {
        ["Raze"] = 15539,
    },
    ["Puissance de Shahram"] = {
        ["Shahram"] = 16600,
    },
    ["Fixer"] = {
        ["Exalté atal'ai"] = 12021,
        ["Gardien des marais Jademir"] = 12021,
        ["Draconide de la Griffe enragée"] = 12021,
        ["Aberration ressuscitée"] = 12021,
        ["Gardien Emeraldon"] = 12021,
        ["Gardien de Verdantis"] = 12021,
    },
    ["Ench. de bracelets (Force excellente)"] = {
        Professions = 20010,
    },
    ["Débiter"] = {
        ["ROGUE"] = 6774,
    },
    ["Attaque bombe étourdissante"] = {
        ["Gryphon du Nid-de-l'Aigle"] = 21188,
        ["Chevaucheur de guerre"] = 21188,
        ["Chevaucheur de guerre de Guse"] = 21188,
        ["Chevaucheur de guerre de Jeztor"] = 21188,
        ["Chevaucheur de guerre de Mulverick"] = 21188,
        ["Gryphon de Slidore"] = 21188,
        ["Gryphon d'Ichman"] = 21188,
        ["Gryphon de Vipore"] = 21188,
    },
    ["Sac rouge en laine"] = {
        Professions = 6688,
    },
    ["Rouleau d'étoffe runique"] = {
        Professions = 18401,
    },
    ["Cuirasse volcanique"] = {
        Professions = 19076,
    },
    ["Thorium enchanté"] = {
        Professions = 17180,
    },
    ["Bombe en sombrefer"] = {
        Professions = 19799,
    },
    ["Fouet"] = {
        ["Raptor flagellant"] = 6607,
        ["Flagellant des hautes-terres"] = 6607,
        ["Basilic flagelleur"] = 6607,
        ["Matriarche trotteuse"] = 6607,
        ["Flagellant solécaille"] = 6607,
        ["Aiguillonneur déviant"] = 6607,
        ["Scorpide flagellant"] = 6607,
        ["Flagellant Pétale-de-sang"] = 6607,
        ["Ombre d'Hakkar"] = 6607,
        ["Ecraseur térébrant"] = 6607,
        ["Vorsha la Flagellante"] = 6607,
    },
    ["Marteau volcanique"] = {
        Professions = 16984,
    },
    ["Bidule bourdonnant en bronze"] = {
        Professions = 3942,
    },
    ["Harnais de l'ours de guerre"] = {
        Professions = 19068,
    },
    ["Invocation d'un tisseur d'effroi Blackhand"] = {
        ["Tisseur d’effroi Blackhand invoqué"] = 15794,
        ["Invocateur Blackhand"] = 15794,
    },
    ["Ench. d'arme (Arme impie)"] = {
        Professions = 20033,
    },
    ["Heaume orné en mithril"] = {
        Professions = 9980,
    },
    ["Bandeau en étoffe runique"] = {
        Professions = 18444,
    },
    ["Gants en gangrétoffe"] = {
        Professions = 22867,
    },
    ["La ruse du raptor"] = {
        ["Gor'mul"] = 4153,
    },
    ["Cape noire en cuir"] = {
        Professions = 2168,
    },
    ["Dard venimeux"] = {
        ["Scorpide queue-venin"] = 5416,
        ["Scorpashi mordeur"] = 5416,
        ["Flagellant scorpashi"] = 5416,
        ["Empoisonneur scorpashi"] = 5416,
        ["Scorpide des sables"] = 5416,
        ["Besseleth"] = 5416,
        ["Dardeur"] = 8257,
    },
    ["Epée courte en bronze"] = {
        Professions = 2742,
    },
    ["Jambières en cuir sauvage"] = {
        Professions = 10572,
    },
    ["Création de Pierre d'âme (majeure)"] = {
        ["WARLOCK"] = 20757,
    },
    ["Sommeil"] = {
        ["Marcherêve wyrmide"] = 15970,
        ["Conjurateur du Syndicat"] = 15970,
        ["Prêtresse Irequeue"] = 15970,
        ["Dormeur austère"] = 8399,
        ["Grand Inquisiteur Fairbanks"] = 8399,
        ["Seigneur du crépuscule Kelris"] = 8399,
        ["Somnus"] = 20989,
    },
    ["Coup tordu"] = {
        ["Palefroi corrompu"] = 7139,
    },
    ["Mot de l'ombre : Douleur"] = {
        ["PRIEST"] = 10894,
        ["Xabraxxis"] = 11639,
    },
    ["Contrefiche en fer"] = {
        Professions = 3958,
    },
    ["Pacifier"] = {
        ["Tenue de maintien de l'ordre"] = 10730,
    },
    ["Bottes en cuir sauvage"] = {
        Professions = 10566,
    },
    ["Malédiction des Tribus"] = {
        ["Centaure maudit"] = 21048,
    },
    ["Bottes impériales en plaques"] = {
        Professions = 16657,
    },
    ["Invocation d'une matriarche des mousses profondes"] = {
        ["Matriarche des mousses profondes"] = 6536,
        ["Jeune des mousses profondes"] = 6536,
    },
    ["Leurre perfectionné"] = {
        Professions = 3965,
    },
    ["Ceinture en écailles de scorpide épaisses"] = {
        Professions = 19070,
    },
    ["Chemise orange martiale"] = {
        Professions = 12064,
    },
    ["Malédiction des Magram défunts"] = {
        ["Spectre magrami"] = 18159,
    },
    ["Enchantement"] = {
        Professions = 7412,
        Professions = 7413,
        Professions = 13920,
    },
    ["Plume légère"] = {
        ["PRIEST"] = 17056,
    },
    ["Ench. d'arme (Puissance de l'hiver)"] = {
        Professions = 21931,
    },
    ["Ench. de plastron (Mana inférieur)"] = {
        Professions = 7776,
    },
    ["Rituel d'invocation"] = {
        ["WARLOCK"] = 698,
    },
    ["Grande bombe en fer"] = {
        Professions = 3967,
    },
    ["Eclair de feu"] = {
        Pet = 11763,
    },
    ["Bandeau runique en cuir"] = {
        Professions = 19082,
    },
    ["Cécité"] = {
        ["ROGUE"] = 2094,
    },
    ["Ceinturon de l'Aube"] = {
        Professions = 23632,
    },
    ["Bottes radieuses"] = {
        Professions = 16656,
    },
    ["Silex à aiguiser grossier"] = {
        Professions = 3326,
    },
    ["Appel de Sanguin"] = {
        ["Sanguin"] = 18262,
        ["Veneur Radley"] = 18262,
    },
    ["Heaume en écailles de scorpide épaisses"] = {
        Professions = 19088,
    },
    ["Malédiction funeste"] = {
        ["WARLOCK"] = 603,
    },
    ["Forme d’ours de Kinelory"] = {
        ["Kinelory"] = 4948,
    },
    ["Ench. de bracelets (Agilité mineure)"] = {
        Professions = 7779,
    },
    ["Modulateur en cuivre"] = {
        Professions = 3926,
    },
    ["Rugissement de bataille"] = {
        ["Sentinelle Ragelune"] = 6507,
        ["Sentinelle de Bois-du-Bûcher"] = 6507,
        ["Chef de meute Frayeloup"] = 6507,
        ["Ancien Griffe féroce"] = 6507,
    },
    ["Puissance étourdissante"] = {
        ["Bête de sève"] = 7997,
    },
    ["Léthargie cristalline"] = {
        ["Basilic Oeil-de-glace"] = 3636,
        ["Basilic cramoisi"] = 3636,
        ["Basilic flamboyant"] = 3636,
        ["Basilic noirci"] = 3636,
        ["Basilic du sel gemme"] = 3636,
        ["Basilic Mâchesable"] = 3636,
        ["Basilic Mâchesable massif"] = 3636,
        ["Basilic Jadéchine"] = 3636,
    },
    ["Horion sonore"] = {
        ["Hurleur noir"] = 14538,
    },
    ["Invisibilité inférieure"] = {
        Pet = 7870,
    },
    ["Disjonction"] = {
        ["Groin-tonnerre fouisseur"] = 14533,
    },
    ["Mort & décomposition"] = {
        ["Sectateur de la Tête de Mort"] = 11433,
    },
    ["Ench. de bracelets (Déviation)"] = {
        Professions = 13931,
    },
    ["Bombe éclairante"] = {
        Professions = 8243,
    },
    ["Respiration aquatique"] = {
        ["SHAMAN"] = 131,
    },
    ["Création de Pierre d'âme"] = {
        ["WARLOCK"] = 20755,
    },
    ["Ench. de gants (Agilité supérieure)"] = {
        Professions = 20012,
    },
    ["Gilet cramoisi en soie"] = {
        Professions = 8791,
    },
    ["La sagesse d'Agammagan"] = {
        ["Mangletooth"] = 7764,
    },
    ["Peau épaisse traitée"] = {
        Professions = 10482,
    },
    ["Chemise rouge d'apparat"] = {
        Professions = 3866,
    },
    ["Rappel astral"] = {
        ["SHAMAN"] = 556,
    },
    ["Peau lourde traitée"] = {
        Professions = 3818,
    },
    ["Nuage nauséabond"] = {
        ["Gelée nuisible"] = 21070,
    },
    ["Malédiction de Brandefeu"] = {
        ["Tisseur d’ombre de Brandefeu"] = 16071,
        ["Tisseur d’effroi de Brandefeu"] = 16071,
    },
    ["Souffle de foudre"] = {
        Pet = 25012,
        ["Chimère Noroît"] = 15797,
    },
    ["Toucher débilitant"] = {
        ["Ame en peine affamée"] = 16333,
        ["Ame errante"] = 16333,
        ["Citoyen spectral"] = 16333,
        ["Gardien décrépit"] = 16333,
    },
    ["Travail du cuir"] = {
        Professions = 3104,
        Professions = 3811,
        Professions = 10662,
    },
    ["Rage sanguinaire"] = {
        ["WARRIOR"] = 2687,
    },
    ["Créer une Gemme d'âme"] = {
        ["Kin'weelay"] = 3660,
    },
    ["Gants tisse-givre"] = {
        Professions = 18411,
    },
    ["Pantalon azur en soie"] = {
        Professions = 8758,
    },
    ["Ench. d'arme (Agilité)"] = {
        Professions = 23800,
    },
    ["Ench. de bracelets (Intelligence inférieure)"] = {
        Professions = 13622,
    },
    ["Ench. d'arme 2M (Agilité)"] = {
        Professions = 27837,
    },
    ["Bandeau noir en tisse-mage"] = {
        Professions = 12072,
    },
    ["Flacon des Titans"] = {
        Professions = 17635,
    },
    ["Attracteur de poissons aquadynamique"] = {
        Professions = 9271,
    },
    ["Lunettes de concentration extrême"] = {
        Professions = 19794,
    },
    ["Sacrifice démoniaque"] = {
        ["WARLOCK"] = 18788,
    },
    ["Brassards de dragonnet vert"] = {
        Professions = 9202,
    },
    ["Sac vert en laine"] = {
        Professions = 3758,
    },
    ["Lévitation"] = {
        ["PRIEST"] = 1706,
    },
    ["Sangsue"] = {
        ["Glouton Ragelune"] = 6958,
        ["Harpie Plumesang"] = 6958,
        ["Hezrul Marque-de-sang"] = 6958,
    },
    ["Cape parachute"] = {
        Professions = 12616,
    },
    ["Crachat venimeux"] = {
        ["Hydre Fouettebrume"] = 6917,
        ["Crache-venin Ronce-sauvage"] = 6917,
        ["Petite Noroît"] = 16552,
    },
    ["Summon Dreadsteed Spirit (DND)"] = {
        ["Esprit de Destrier de l'effroi"] = 23159,
        ["Destrier de l'effroi xorothien"] = 23159,
    },
    ["Avatar"] = {
        ["Vanndar Stormpike"] = 19135,
    },
    ["Détection des démons"] = {
        ["WARLOCK"] = 5500,
    },
    ["Cape en cuir cousu main"] = {
        Professions = 9058,
    },
    ["Invocation d'un rejeton de Bael'Gar"] = {
        ["Rejeton de Bael'Gar"] = 13895,
    },
    ["Résistance à la Nature"] = {
        ["HUNTER"] = 24513,
    },
    ["Bombe explosive en mithril"] = {
        Professions = 12603,
    },
    ["Bouclier réflecteur"] = {
        ["Annulateur d’Arcane X-21"] = 10831,
    },
    ["Lame corrompue en mithril"] = {
        Professions = 9997,
    },
    ["Ecureuil mécanique"] = {
        Professions = 3928,
    },
    ["Points d'honneur +50"] = {
        ["Porteguerre de la Horde"] = 24960,
        ["Général de brigade de l'Alliance"] = 24960,
    },
    ["Casque barbare en fer"] = {
        Professions = 9814,
    },
    ["Puissant soufflet"] = {
        ["M. Smite"] = 6435,
    },
    ["Malédiction d'agonie"] = {
        ["WARLOCK"] = 11713,
        ["Sectateur de la Lame brûlante"] = 18266,
        ["Balizar l’Ombrageux"] = 14868,
    },
    ["Apparition de drakônide chromatique"] = {
        ["Drakônide chromatique"] = 22680,
        ["Black Drakonid Spawner"] = 22680,
        ["Red Drakonid Spawner"] = 22680,
        ["Green Drakonid Spawner"] = 22680,
        ["Bronze Drakonid Spawner"] = 22680,
        ["Blue Drakonid Spawner"] = 22680,
    },
    ["Gilet en étoffe cendrée"] = {
        Professions = 18408,
    },
    ["Ench. d'arme (Tueur de bête inférieur)"] = {
        Professions = 13653,
    },
    ["Ench. de plastron (Absorption inférieure)"] = {
        Professions = 13538,
    },
    ["Pouvoir des Grumegueules"] = {
        Professions = 23703,
    },
    ["Feu d'artifice \"Explosion serpentine\""] = {
        Professions = 23507,
    },
    ["Fondre armure"] = {
        ["Garde des flammes"] = 19631,
    },
    ["Invocation d'illusions"] = {
        ["Illusion de Jandice Barov"] = 17773,
        ["Jandice Barov"] = 17773,
    },
    ["Collecte de vie"] = {
        ["Œil-de-ver"] = 3243,
    },
    ["Gants en cuir estampé"] = {
        Professions = 3756,
    },
    ["Fourche d'éclairs"] = {
        ["Dame Sarevess"] = 8435,
        ["Dame Vespira"] = 12549,
    },
    ["Invocation d'un serviteur Infernal"] = {
        ["Serviteur Infernal"] = 12740,
        ["Dame Sevine"] = 12740,
    },
    ["Enchaînement sacré"] = {
        ["Généralissime Taelan Fordring"] = 18819,
        ["Seigneur Tirion Fordring"] = 18819,
    },
    ["Bandeau corrompu en cuir"] = {
        Professions = 19071,
    },
    ["Giberne en cuir épais"] = {
        Professions = 14932,
    },
    ["Dard acéré"] = {
        ["Dard des Abysses"] = 14534,
    },
    ["Totem de Force de la Terre"] = {
        ["SHAMAN"] = 25361,
    },
    ["Ench. de bouclier (Protection inférieure)"] = {
        Professions = 13464,
    },
    ["Petite giberne en cuir"] = {
        Professions = 9062,
    },
    ["Toxine"] = {
        ["Gelée toxique"] = 25989,
    },
    ["Gilet rouge en tisse-mage"] = {
        Professions = 12056,
    },
    ["Cape en cuir sauvage"] = {
        Professions = 10574,
    },
    ["Robe du Vide"] = {
        Professions = 18458,
    },
    ["Totem de Jet de Lave"] = {
        ["Totem de Jet de Lave"] = 8264,
        ["Prophète de la Tête de Mort"] = 8264,
    },
    ["Chemise jaune vif"] = {
        Professions = 3869,
    },
    ["Gants en étoffe lunaire"] = {
        Professions = 22869,
    },
    ["Armure d'os"] = {
        ["Nécromancienne de la Tête de Mort"] = 11445,
    },
    ["Ench. de bouclier (Résistance au Givre)"] = {
        Professions = 13933,
    },
    ["Armure tempétueuse"] = {
        Professions = 19079,
    },
    ["Potion de protection contre le Givre"] = {
        Professions = 7258,
    },
    ["Ench. de plastron (Vie)"] = {
        Professions = 7857,
    },
    ["Régler"] = {
        ["Technicien lépreux"] = 10348,
        ["Forgeur de machines lépreux"] = 10348,
    },
    ["Invocation d'un exhalombre"] = {
        ["Exhalombre squelettique"] = 12258,
        ["Invocateur squelettique"] = 12258,
    },
    ["Revers"] = {
        ["Kazon"] = 6253,
        ["Bagarreur"] = 6253,
        ["Détenu défias"] = 6253,
        ["Mutileur Cassecrête"] = 6253,
        ["Otto"] = 6253,
        ["Brigand des Mers du sud"] = 6253,
        ["Prospecteur Khazgorm"] = 6253,
        ["Nervi Réprouvé"] = 6253,
        ["Tazan"] = 6253,
        ["Garde de Froidemine"] = 6253,
        ["Garde de Froidemine vétéran"] = 6253,
    },
    ["Disparition"] = {
        ["ROGUE"] = 1857,
    },
    ["Premiers soins"] = {
        ["Docteur Tamberlyn"] = 7162,
        ["Docteur Helaina"] = 7162,
        ["Frère Malach"] = 7162,
    },
    ["Tunique noire en cuir"] = {
        Professions = 2169,
    },
    ["Puissante charge d'hydroglycérine"] = {
        Professions = 23080,
    },
    ["Blessure infectée"] = {
        ["Vermine Blanche-moustache"] = 17230,
        ["Croc noir Plaie-de-nuit"] = 3427,
        ["Gath'Ilzogg"] = 3427,
        ["Bâtard des Carmines"] = 3427,
        ["Tranchegueule des hautes-terres"] = 3427,
        ["Tranchegueule tacheté"] = 3427,
        ["Grondecroc"] = 3427,
        ["Captif défias"] = 3427,
        ["Ours noir malade"] = 3427,
        ["Clampant des marées incrusté"] = 3427,
        ["Marche-boue Aileron déchiré"] = 3427,
        ["Grand crocilisque du loch"] = 3427,
        ["Grunt de Trépas-d'Orgrim"] = 3427,
        ["Busard"] = 3427,
        ["Busard géant"] = 3427,
        ["Rampant déviant"] = 3427,
        ["Vermine Mufle-de-gravier"] = 3427,
        ["Horreur putride"] = 3427,
        ["Loup Gangrepatte"] = 3427,
        ["Charognard Gangrepatte"] = 3427,
        ["Ravageur Gangrepatte"] = 3427,
    },
    ["Création de Pierre de sort"] = {
        ["WARLOCK"] = 2362,
    },
    ["Culottes en lin cousu main"] = {
        Professions = 3842,
    },
    ["Détection de la magie"] = {
        ["MAGE"] = 2855,
    },
    ["Lunettes d'oeil de chat"] = {
        Professions = 12607,
    },
    ["Brassards en thorium"] = {
        Professions = 16644,
    },
    ["Ecailles de poisson brillantes"] = {
        ["SHAMAN"] = 17057,
    },
    ["Hurlement assourdissant"] = {
        ["Hurleuse Daguéchine"] = 3589,
        ["Harpie hurlante"] = 3589,
        ["Hurleuse Fouette-bile"] = 3589,
        ["Hurleur Bec-de-fer"] = 3589,
        ["Hurleur du Berceau-de-l'Hiver"] = 3589,
        ["Hurleuse haineuse"] = 3589,
        ["Banshee hurlante"] = 3589,
        ["Harpie Brûleglace"] = 3589,
        ["Furie Shelda"] = 3589,
    },
    ["Malédiction de guérison"] = {
        ["Fanatique de la Rive noire"] = 7098,
        ["Harpie Ensorçaile"] = 7098,
    },
    ["Création de Pierre de feu"] = {
        ["WARLOCK"] = 17951,
    },
    ["Evocation"] = {
        ["MAGE"] = 12051,
    },
    ["Apaiser les animaux"] = {
        ["DRUID"] = 9901,
    },
    ["Invocation d'un cyclonien"] = {
        ["Bath'rah la Vigie des vents"] = 8606,
    },
    ["Recette du carburant de fusée gobelin"] = {
        Professions = 12715,
    },
    ["Gilet en tisse-fantôme"] = {
        Professions = 18416,
    },
    ["Bottes de la nuit"] = {
        Professions = 10558,
    },
    ["Filet électrifié"] = {
        ["Gardien mécanique"] = 11820,
    },
    ["Nauséeux"] = {
        ["Poulet"] = 24919,
        ["Garde de Southshore"] = 24919,
        ["Kent le fermier"] = 24919,
        ["Phin Odelic"] = 24919,
        ["Sergent Hartman"] = 24919,
    },
    ["Cotte de mailles Ronce-sauvage"] = {
        Professions = 16650,
    },
    ["Supercharge"] = {
        ["Technicien lépreux"] = 10732,
        ["Forgeur de machines lépreux"] = 10732,
    },
    ["Gilet en vignesang"] = {
        Professions = 24091,
    },
    ["Casque en écailles de tortue"] = {
        Professions = 10552,
    },
    ["Bénédiction des rois"] = {
        ["PALADIN"] = 20217,
    },
    ["Kit de moissonneur compact"] = {
        Professions = 3963,
    },
    ["Toucher de faiblesse"] = {
        ["PRIEST"] = 19266,
    },
    ["Ench. de cape (Agilité inférieure)"] = {
        Professions = 13882,
    },
    ["Transmutation de la Terre en Eau"] = {
        Professions = 17561,
    },
    ["Totem de Vague de mana"] = {
        ["SHAMAN"] = 17359,
    },
    ["Ench. de gants (Hâte mineure)"] = {
        Professions = 13948,
    },
    ["Armure en thorium"] = {
        Professions = 16642,
    },
    ["Liens partagés"] = {
        ["Esclave Ravenclaw"] = 7761,
    },
    ["Somnambulisme"] = {
        ["Hurle-rêve"] = 20668,
    },
    ["Ench. de bracelets (Intelligence supérieure)"] = {
        Professions = 20008,
    },
    ["Barrière d’éclairs"] = {
        ["Sœur Cinglehaine"] = 6960,
    },
    ["Ceinture en anneaux de cuivre"] = {
        Professions = 2661,
    },
    ["Enflammer"] = {
        ["Réfugié hagard"] = 3261,
    },
    ["Cuir moyen"] = {
        Professions = 20648,
    },
    ["Fureur du poulet"] = {
        ["Coq de combat"] = 13168,
    },
    ["Lancer la clé"] = {
        ["Technicien lépreux"] = 13398,
        ["Forgeur de machines lépreux"] = 13398,
    },
    ["Potion de résistance à la magie"] = {
        Professions = 11453,
    },
    ["Ench. de bouclier (Endurance inférieure)"] = {
        Professions = 13631,
    },
    ["Corruption"] = {
        ["WARLOCK"] = 25311,
        Professions = 16985,
    },
    ["Sac en étoffe runique"] = {
        Professions = 18405,
    },
    ["Bombinette"] = {
        Professions = 15628,
    },
    ["Morsure de glace"] = {
        ["MAGE"] = 12472,
    },
    ["Rayon réducteur gnome"] = {
        Professions = 12899,
    },
    ["Robe de fête rouge"] = {
        Professions = 26403,
    },
    ["Pierre à aiguiser grossière"] = {
        Professions = 2665,
    },
    ["Bouclier impie"] = {
        ["Morbent Fel"] = 8909,
    },
    ["Aura de résistance à l'Ombre"] = {
        ["PALADIN"] = 19896,
    },
    ["Inquisition"] = {
        ["Vaillant écarlate"] = 14517,
        ["Commandant écarlate Mograine"] = 14518,
    },
    ["Gantelets en écailles de scorpide épaisses"] = {
        Professions = 19064,
    },
    ["Croc-en-jambe circulaire"] = {
        ["Chasseur de têtes Gurubashi"] = 24048,
        ["Porte-parole Skullsplitter"] = 24048,
    },
    ["Invocation d'une âme en peine illusoire"] = {
        ["Ame en peine illusoire"] = 17231,
        ["Araj l'Invocateur"] = 17231,
    },
    ["Bombe grossière en cuivre"] = {
        Professions = 3923,
    },
    ["Lenteur"] = {
        ["Mystique Poil-moussu"] = 11436,
        ["Géomancienne de la Tête de Mort"] = 11436,
        ["Géomancien du Totem sinistre"] = 11436,
        ["Oracle Ondulame"] = 246,
        ["Dame Sarevess"] = 246,
        ["Ensorceleur écarlate"] = 6146,
        ["Capitaine Balinda Stonehearth"] = 19137,
        ["Erudit du crépuscule"] = 18972,
    },
    ["Douleur brûlante"] = {
        ["WARLOCK"] = 17923,
    },
    ["Habit de fête rouge"] = {
        Professions = 26407,
    },
    ["Double vue"] = {
        ["SHAMAN"] = 6196,
    },
    ["Nuage de radiation"] = {
        ["Gelée irradiée"] = 10341,
    },
    ["Tir des arcanes"] = {
        ["HUNTER"] = 14287,
    },
    ["Portail : Stormwind"] = {
        ["MAGE"] = 10059,
    },
    ["Briser l'esprit"] = {
        ["Docteur Weavil"] = 25774,
    },
    ["Ingénieur"] = {
        Professions = 4037,
        Professions = 4038,
        Professions = 12656,
    },
    ["Invocation d'un théurge"] = {
        ["Théurge de Dalaran"] = 3658,
        ["Invocateur de Dalaran"] = 3658,
    },
    ["Scarabées des cryptes"] = {
        ["Scarabée des cryptes"] = 16418,
        ["Rampant des cryptes"] = 16418,
    },
    ["Forme aquatique"] = {
        ["DRUID"] = 1066,
    },
    ["Explosion sonore"] = {
        ["Grande chauve-souris du kraal"] = 8281,
        ["Hurleur des vallées insoumis"] = 8281,
        ["Chauve-souris pestiférée monstrueuse"] = 8281,
        ["Ressan le Harceleur"] = 8281,
    },
    ["Bottes en acier poli"] = {
        Professions = 3513,
    },
    ["Harnais en peau de raptor"] = {
        Professions = 4096,
    },
    ["Invocation d'une illusion impalpable"] = {
        ["Illusion impalpable"] = 8986,
        ["Illusion spectrale"] = 8986,
    },
    ["Soins"] = {
        ["PRIEST"] = 6064,
        ["Géomètre de Gouffrefer"] = 15586,
        ["Géomètre de Gouffrefer vétéran"] = 15586,
        ["Géomètre de Gouffrefer champion"] = 15586,
        ["Sirène Tempécaille"] = 11642,
        ["Prêtresse Irequeue"] = 11642,
        ["Oracle de la baie des tempêtes"] = 11642,
        ["Fouilleur Makrinni"] = 11642,
        ["Infirmier du Hangar"] = 22167,
        ["Grand Inquisiteur Fairbanks"] = 12039,
    },
    ["Ench. de cape (Défense supérieure)"] = {
        Professions = 13746,
    },
    ["Malédiction du Sang"] = {
        ["Ambassadeur Malcin"] = 12279,
        ["Goule folle"] = 8282,
        ["Sang d'Agamaggan"] = 8282,
        ["Grand Inquisiteur Fairbanks"] = 8282,
    },
    ["Ceinture runique ténébreuse"] = {
        Professions = 24902,
    },
    ["Lunettes de fortification des sorts"] = {
        Professions = 12615,
    },
    ["Gilet d'homme des collines en cuir"] = {
        Professions = 3762,
    },
    ["Sac rouge en lin"] = {
        Professions = 6686,
    },
    ["Sac noir en soie"] = {
        Professions = 6695,
    },
    ["Fusée éclairante"] = {
        ["HUNTER"] = 1543,
    },
    ["Déchirure musculaire"] = {
        ["Grand déchireur"] = 12166,
        ["Déchireur"] = 12166,
        ["Sabre-de-nuit farouche"] = 12166,
        ["Clampant des récifs"] = 12166,
        ["Crocilisque Gueule-d'effroi"] = 12166,
        ["Intrus Dos-hirsute"] = 12166,
        ["Le Rake"] = 12166,
    },
    ["Champignon de Mirkfallon"] = {
        ["Gardien de Mirkfallon"] = 8138,
    },
    ["Pistage des humanoïdes"] = {
        ["DRUID"] = 5225,
        ["HUNTER"] = 19883,
    },
    ["Marque du fauve"] = {
        ["DRUID"] = 9885,
    },
    ["Poison de distraction mentale"] = {
        ["ROGUE"] = 5763,
    },
    ["Pierre brisante"] = {
        ["Elémentaire asservi"] = 3671,
    },
    ["Mantelet Coeur-de-braise"] = {
        Professions = 20848,
    },
    ["Attaque du Tireur de précision"] = {
        ["Tireur d'élite Sombrefer"] = 12198,
    },
    ["Pantalon tempétueux"] = {
        Professions = 19067,
    },
    ["Invocation d'une explosion de lave E"] = {
        ["Rat du Magma"] = 21903,
    },
    ["Ench. de bracelets (Déviation inférieure)"] = {
        Professions = 13646,
    },
    ["Grande comète bleue"] = {
        Professions = 26420,
    },
    ["Gilet en cuir cousu main"] = {
        Professions = 7126,
    },
    ["Jambières en écailles de mithril"] = {
        Professions = 9931,
    },
    ["Gants en étoffe runique"] = {
        Professions = 18417,
    },
    ["Grondement redoutable"] = {
        ["Limier"] = 13692,
        ["Dimeurtrodon"] = 13692,
        ["Sanguin"] = 13692,
    },
    ["Bombe"] = {
        ["Démolisseur Sombrefer"] = 8858,
        ["Bombardier Sombrefer"] = 8858,
        ["Géologue Sombrefer"] = 8858,
        ["Technicien du Hangar"] = 8858,
        ["Chef Copperplug"] = 9143,
    },
    ["Jambières en cuir mat"] = {
        Professions = 9195,
    },
    ["Jambières en fer émeraude"] = {
        Professions = 3506,
    },
    ["Chemise verte en lin"] = {
        Professions = 2396,
    },
    ["Effet Garde de Laze"] = {
        ["Garde de Laze"] = 3826,
    },
    ["Flacon de pétrification"] = {
        Professions = 17634,
    },
    ["Guérison des dragons"] = {
        ["Dresseur de dragons Blackhand"] = 16637,
    },
    ["Sérénité"] = {
        Professions = 16983,
    },
    ["Jambières en tisse-sorcier"] = {
        Professions = 18421,
    },
    ["Gants chimériques"] = {
        Professions = 19053,
    },
    ["Mot de pouvoir : Bouclier"] = {
        ["PRIEST"] = 10901,
        ["Grand Inquisiteur Fairbanks"] = 11647,
        ["Clerc de Rempart-du-Néant"] = 11974,
    },
    ["Carquois en cuir léger"] = {
        Professions = 9060,
    },
    ["Petite comète rouge"] = {
        Professions = 26418,
    },
    ["Pacte noir"] = {
        ["WARLOCK"] = 18938,
    },
    ["Ceinture en cuir clouté de gemmes"] = {
        Professions = 3778,
    },
    ["Invocation d'un scarabée putride"] = {
        ["Scarabée putride"] = 17169,
        ["Horreur des cryptes"] = 17169,
    },
    ["Explosion"] = {
        ["Maître-dynamiteur Emi Shortfuse"] = 12158,
        ["Maître-dynamiteur Emi Shortfuse"] = 12159,
    },
    ["Portail : Ironforge"] = {
        ["MAGE"] = 11416,
    },
    ["Brassards en cuir léger"] = {
        Professions = 9065,
    },
    ["Protection divine"] = {
        ["PALADIN"] = 5573,
    },
    ["Fureur vertueuse"] = {
        ["PALADIN"] = 25780,
    },
    ["Renaissance"] = {
        ["DRUID"] = 20748,
    },
    ["Cogneur dormant"] = {
        ["Cogneur ivre"] = 26115,
    },
    ["Exorcisme"] = {
        ["PALADIN"] = 10314,
    },
    ["Bottes rouges en laine"] = {
        Professions = 3847,
    },
    ["Ench. d'arme 2M (Impact supérieur)"] = {
        Professions = 13937,
    },
    ["Tir empoisonné"] = {
        ["Belluaire de Tranchebauge"] = 8275,
        ["Chasseur des ombres de Kurzen"] = 8806,
        ["Chasseur Dos-hirsute"] = 8806,
    },
    ["Rage dévorante"] = {
        ["Marcheur du Vide invoqué"] = 7750,
    },
    ["Pourfendre la chair"] = {
        ["Bhag'thera"] = 3147,
        ["Faucheur des hautes-terres"] = 3147,
        ["Faucheur tacheté"] = 3147,
        ["Bjarn"] = 3147,
        ["Matriarche tranchegueules"] = 3147,
        ["Bec-rasoir apprivoisé"] = 3147,
        ["Gryphon Bec-rasoir"] = 3147,
        ["Seigneur du ciel Bec-rasoir"] = 3147,
        ["Pilleuse Terrevent"] = 3147,
        ["Faucheur Rougefange"] = 3147,
        ["Pince-tranchante Bourbecoque"] = 3147,
        ["Déchiqueteur de l’effroi"] = 3147,
        ["Griffe féroce enragé"] = 3147,
    },
    ["Epaulières ornées en mithril"] = {
        Professions = 9952,
    },
    ["Brûlure de zone"] = {
        ["Destructeur ardent"] = 8000,
    },
    ["Totem de Mur des vents"] = {
        ["SHAMAN"] = 15112,
    },
    ["Péon dormant"] = {
        ["Péon fainéant"] = 17743,
    },
    ["Respiration interminable"] = {
        ["WARLOCK"] = 5697,
    },
    ["Points d'honneur +82"] = {
        ["Porteguerre de la Horde"] = 24961,
        ["Général de brigade de l'Alliance"] = 24961,
    },
    ["Chemise noire de bretteur"] = {
        Professions = 3873,
    },
    ["Casque tête-de-loup"] = {
        Professions = 10621,
    },
    ["Fatigue fièvreuse"] = {
        ["Busard des mésas"] = 8139,
        ["Ancien busard des mésas"] = 8139,
        ["Crins-d'argent galeux"] = 8139,
        ["Géomancien des Cisailles"] = 8139,
        ["Seigneur des Cisailles"] = 8139,
        ["Habitant du bourbier Arkkoran"] = 8139,
    },
    ["Créer le Parchemin de Yeh'kinya"] = {
        ["Yeh'kinya"] = 12998,
    },
    ["Tir rapide"] = {
        ["HUNTER"] = 3045,
    },
    ["Poussée d'adrénaline"] = {
        ["ROGUE"] = 13750,
    },
    ["Se cacher"] = {
        ["Rôdeuse Ronce-sauvage"] = 6920,
        ["Voleur Fielfurie"] = 6920,
    },
    ["Bénédiction de sagesse supérieure"] = {
        ["PALADIN"] = 25918,
    },
    ["Pourfendre"] = {
        ["WARRIOR"] = 11574,
        ["Ravageur noir"] = 13443,
        ["Fourrageur Selécaille"] = 13443,
        ["Ancien tigre de Strangleronce"] = 13443,
        ["Dogue Ravageur noir"] = 13443,
        ["Fourrageur Branchie-bistre"] = 13443,
        ["Pinceur Arkkoran"] = 13443,
        ["Ravasaure chasseur"] = 13443,
        ["Chasseur Bec-de-fer"] = 13443,
        ["Sélénien enragé"] = 13443,
        ["Bayne"] = 13443,
        ["Loup des glaces"] = 13443,
        ["Bélier d'Alterac"] = 13443,
        ["Pinceur Claquesec"] = 13443,
        ["Déchireur de tourbillon"] = 13443,
        ["Ursangous"] = 13443,
        ["Gorefang"] = 13445,
        ["Déchiqueteuse Rougefurie"] = 13445,
        ["Shadumbra"] = 13445,
        ["Traque-mana Hederine"] = 13738,
        ["Myrmidon Irequeue"] = 11977,
        ["Guerrier Ondulame"] = 11977,
        ["Soldat Hendel"] = 11977,
        ["Arnak Grimtotem"] = 11977,
        ["Légionnaire Frostwolf"] = 11977,
        ["Torek"] = 11977,
        ["Guerrier d'Aile-argent"] = 11977,
        ["Sergent Yazra Bloodsnarl"] = 11977,
        ["Garde-serpent Strashaz"] = 16509,
        ["Envahisseur de Froidemine"] = 16509,
        ["Chef d'escadrille Ichman"] = 16509,
        ["Chef d'escadrille Slidore"] = 16509,
    },
    ["Création du Gambit de l'aube"] = {
        ["Betina Bigglezink"] = 18367,
    },
    ["Potion de protection contre l'Ombre"] = {
        Professions = 7256,
    },
    ["Sbire d'Antu'sul"] = {
        ["Serviteur d'Antu'sul"] = 11894,
    },
    ["Bottes runiques ténébreuses"] = {
        Professions = 24903,
    },
    ["Visée"] = {
        ["HUNTER"] = 20904,
    },
    ["Armure de gardien"] = {
        Professions = 3773,
    },
    ["Création d’une faille"] = {
        ["Tabetha"] = 9079,
    },
    ["Pantalon en cuir cousu main"] = {
        Professions = 2153,
    },
    ["Horion de terre"] = {
        ["SHAMAN"] = 10414,
        ["Oracle Hakkari"] = 15501,
        ["Marteleur du Totem sinistre"] = 13281,
        ["Gardien des rochers de Gogger"] = 13281,
    },
    ["Kilt coloré"] = {
        Professions = 12047,
    },
    ["Oeil d'aigle"] = {
        ["HUNTER"] = 6197,
    },
    ["Servants de Malathrom"] = {
        ["Limace"] = 3537,
        ["Seigneur Malathrom"] = 3537,
    },
    ["Pointe de bouclier en mithril"] = {
        Professions = 9939,
    },
    ["Gantelets runiques en cuivre"] = {
        Professions = 3323,
    },
    ["Elixir d'Intelligence supérieure"] = {
        Professions = 11465,
    },
    ["Potion Magesang"] = {
        Professions = 24365,
    },
    ["Souhait mortel"] = {
        ["WARRIOR"] = 12328,
    },
    ["Poison instantané III"] = {
        ["ROGUE"] = 8691,
    },
    ["Armure de glace"] = {
        ["MAGE"] = 10220,
    },
    ["Nuage de poussière"] = {
        ["Matriarche trotteuse"] = 7272,
        ["Vieux trotteur des plaines"] = 7272,
        ["Manoeuvre kolkar"] = 7272,
    },
    ["Bottes ornées en mithril"] = {
        Professions = 9979,
    },
    ["Totem de lien terrestre"] = {
        ["SHAMAN"] = 2484,
        ["Totem de lien terrestre"] = 15786,
        ["Chaman de Gouffrefer"] = 15786,
        ["Primaliste Thurloga"] = 15786,
    },
    ["Ranimer un Scarabée mort-vivant"] = {
        ["Scarabée mort-vivant"] = 17235,
    },
    ["Ench. de plastron (Vie supérieure)"] = {
        Professions = 13640,
    },
    ["Lambeau"] = {
        ["DRUID"] = 9830,
        ["Déchiqueteur Aileron noir"] = 3252,
    },
    ["Poison engourdissant"] = {
        ["Marcheur de l'ombre murloc"] = 7992,
        ["Lame nocturne défias"] = 7992,
        ["Ecumeur squelette"] = 7992,
        ["Volplume Ensorçaile"] = 7992,
        ["Assassin Réprouvé"] = 7992,
        ["Rampant Sombrecroc"] = 7992,
        ["Ami de Slim"] = 7992,
        ["Scorpide chasseur"] = 7992,
    },
    ["Vaudou"] = {
        ["Bom'bay"] = 17009,
    },
    ["Jambières chimériques"] = {
        Professions = 19073,
    },
    ["Tir de mortier"] = {
        ["Courtebottes"] = 16786,
    },
    ["Créer un Parchemin"] = {
        ["Collin Mauren"] = 6671,
    },
    ["Epaulières en bronze argenté"] = {
        Professions = 3330,
    },
    ["Jambières lourdes en mithril"] = {
        Professions = 9933,
    },
    ["Pistage des morts-vivants"] = {
        ["HUNTER"] = 19884,
    },
    ["Carburant de fusée gobelin"] = {
        Professions = 11456,
    },
    ["Pantalon en cuir robuste"] = {
        Professions = 9064,
    },
    ["Transmutation de l'Eau en Non-mort"] = {
        Professions = 17564,
    },
    ["Captation de sang"] = {
        ["Grand prêtre Hakkari"] = 24617,
    },
    ["Effrayer une bête"] = {
        ["HUNTER"] = 14327,
    },
    ["Bâtonnet d'argent"] = {
        Professions = 7818,
    },
    ["Heaume impérial en plaques"] = {
        Professions = 16658,
    },
    ["Arme Langue de feu"] = {
        ["SHAMAN"] = 16342,
    },
    ["Gants d'herboriste"] = {
        Professions = 9146,
    },
    ["Rage frénétique"] = {
        ["Protecteur Crin-de-Chardon"] = 3490,
        ["Aku'mai"] = 3490,
    },
    ["Ceinture élégante en cuir"] = {
        Professions = 3763,
    },
    ["Invocation d'un soldat écarlate"] = {
        ["Soldat écarlate"] = 19722,
        ["Soldat écarlate"] = 19722,
    },
    ["Championne en vrai-argent"] = {
        Professions = 10015,
    },
    ["Point d'apparition des Esprits"] = {
        ["Redpath le Corrompu"] = 17321,
        ["Esprit de Darrowshire"] = 17321,
        ["Esprit de Lunegriffe"] = 17321,
        ["Esprit de Destrier de l'effroi"] = 17321,
        ["Destrier déchu de Ravassombre"] = 17321,
        ["Serviteur de la Main"] = 17321,
    },
    ["Désespoir de l'idiot"] = {
        ["Simona la Discrète"] = 23504,
        ["Franklin l'Amical"] = 23504,
        ["Artorius l'Aimable"] = 23504,
        ["Nelson le Gentil"] = 23504,
    },
    ["Griffure"] = {
        ["DRUID"] = 9904,
    },
    ["Gel instantané"] = {
        ["Goule glaciale"] = 16803,
    },
    ["Bourbier"] = {
        ["Boueux sylvain"] = 5567,
    },
    ["Sombre prédiction de dégâts de Sayge"] = {
        ["Sayge"] = 23768,
    },
    ["Pierre à aiguiser brute"] = {
        Professions = 2660,
    },
    ["Graine d'érable"] = {
        ["DRUID"] = 17034,
    },
    ["Epaulières en écailles de mithril"] = {
        Professions = 9966,
    },
    ["Jambières runiques ténébreuses"] = {
        Professions = 24901,
    },
    ["Cristalliser"] = {
        ["Araignée du pic"] = 16104,
    },
    ["Invocation d'un esprit d'antan"] = {
        ["Esprit d'antan"] = 3652,
        ["Théurge de Dalaran"] = 3652,
    },
    ["Douleurs insupportables"] = {
        ["Tortionnaire du Marteau du crépuscule"] = 13619,
        ["Aspect de corruption"] = 13619,
    },
    ["Houle des âmes"] = {
        ["Lokholar le Seigneur de glace"] = 21307,
    },
    ["Poison de l'esprit"] = {
        ["Esclavagiste de la Ruche'Regal"] = 19469,
    },
    ["Pantalon de la nuit"] = {
        Professions = 10548,
    },
    ["Brassards corrompus en cuir"] = {
        Professions = 19052,
    },
    ["Pierre à aiguiser lourde"] = {
        Professions = 2674,
    },
    ["Morsure infectieuse"] = {
        ["Jeune veuve noire"] = 7367,
        ["Hydre Strashaz"] = 16128,
    },
    ["Parade"] = {
        ["SHAMAN"] = 16268,
    },
    ["Crachat toxique"] = {
        ["Venimeux des mousses profondes"] = 7951,
        ["Crache-venin Sombrecroc"] = 7951,
        ["Venimeux des airs déviant"] = 7951,
    },
    ["Pantalon en cuir estampé"] = {
        Professions = 3759,
    },
    ["Courant des Ruissenord"] = {
        ["Harpie Ruissenord"] = 11014,
        ["Volplume Ruissenord"] = 11014,
        ["Pourfendeuse Ruissenord"] = 11014,
        ["Imploratrice céleste Ruissenord"] = 11014,
    },
    ["Epaulières du point du jour"] = {
        Professions = 16660,
    },
    ["Fragment d'âme"] = {
        ["WARLOCK"] = 6265,
    },
    ["Hurlement de la Banshee"] = {
        ["Esprit Eldreth"] = 16838,
    },
    ["Cape de feu"] = {
        Professions = 18422,
    },
    ["Potion de protection contre le Sacré"] = {
        Professions = 7255,
    },
    ["Rapière flamboyante"] = {
        Professions = 16978,
    },
    ["Siphon de vie"] = {
        ["WARLOCK"] = 18881,
    },
    ["Dards de flammes"] = {
        ["Lo'Grosh"] = 8814,
        ["Mage de sang Thalnos"] = 8814,
        ["Géomancienne de la Tête de Mort"] = 8814,
        ["Garneg Charskull"] = 6725,
        ["Sœur Riven"] = 6725,
    },
    ["Aura de résistance au Givre"] = {
        ["PALADIN"] = 19898,
    },
    ["Grande bombe en cuivre"] = {
        Professions = 3937,
    },
    ["Chemise blanche en lin"] = {
        Professions = 2393,
    },
    ["Heaume en plaques d'acier"] = {
        Professions = 9935,
    },
    ["Jet de torche"] = {
        ["Prophète-tempête kolkar"] = 14292,
        ["Envahisseur kolkar"] = 14292,
    },
    ["Courroux naturel"] = {
        ["SHAMAN"] = 17364,
    },
    ["Déranger l'Oeuf de la colonie"] = {
        ["Démolisseur de la colonie"] = 15746,
    },
    ["Malédiction de la corne noire"] = {
        ["Cerf à corne noire"] = 6922,
        ["Vieux cerf à corne noire"] = 6922,
    },
    ["Cuirasse lourde en mithril"] = {
        Professions = 9959,
    },
    ["Perce-armure"] = {
        ["Mineur kobold"] = 6016,
        ["Ruklar le trappeur"] = 6016,
        ["Mâcheur d'os"] = 6016,
        ["Lardeur"] = 6016,
        ["Brack"] = 6016,
        ["Mineur défias"] = 6016,
        ["Mineur de la KapitalRisk"] = 6016,
        ["Terrassier kobold"] = 6016,
        ["Ecorceur sylvain"] = 6016,
        ["Batteur de Sombrivage"] = 6016,
        ["Ancien Batteur de Sombrivage"] = 6016,
        ["Paysan d'Hillsbrad"] = 6016,
        ["Rasoir Irequeue"] = 6016,
        ["Envahisseur silithide"] = 6016,
        ["Tortue fouisseuse nacrétoile"] = 6016,
        ["Arracheur Fane-liane"] = 6016,
        ["Carrier défias"] = 6016,
        ["Enfourneur"] = 12097,
        ["Serres-tranchantes"] = 12097,
        ["Duriel Moonfire"] = 12097,
    },
    ["Coq de combat gnome"] = {
        Professions = 12906,
    },
    ["Horion de givre"] = {
        ["SHAMAN"] = 10473,
        ["Sorcière de Strashaz"] = 15499,
        ["Ensorceleuse Fouette-bile"] = 12548,
        ["Rampant Claquesec"] = 12548,
        ["Lokholar le Seigneur de glace"] = 19133,
        ["Grik'nir le Froid"] = 21030,
        ["Chaman Frostwolf"] = 21401,
    },
    ["Gilet rouge en lin"] = {
        Professions = 7629,
    },
    ["Téléportation : Thunder Bluff"] = {
        ["MAGE"] = 3566,
    },
    ["Contresort"] = {
        ["MAGE"] = 2139,
    },
    ["Pulvériseur en sombrefer"] = {
        Professions = 15292,
    },
    ["Robe en étoffe lumineuse"] = {
        Professions = 18414,
    },
    ["Grande comète verte"] = {
        Professions = 26421,
    },
    ["Corruptrice"] = {
        Professions = 10011,
    },
    ["Trousse de réparation mécanique"] = {
        Professions = 15255,
    },
    ["Bottes en étoffe lunaire"] = {
        Professions = 19435,
    },
    ["Potion d'invisibilité"] = {
        Professions = 11464,
    },
    ["Rayon discombobulateur"] = {
        Professions = 3959,
    },
    ["Plastron runique en cuivre"] = {
        Professions = 2667,
    },
    ["Invocation d'une nuée des moissons"] = {
        ["Nuée des moissons"] = 7278,
        ["Moissonneur silithide"] = 7278,
    },
    ["Vengeance des Pins-tordus"] = {
        ["Augure Pin-tordu"] = 5628,
        ["Guide Pin-tordu"] = 5628,
        ["Vengeur Pin-tordu"] = 5628,
        ["Totémique Pin-tordu"] = 5628,
    },
    ["Carapace infernale"] = {
        ["Magistrat Burnside"] = 7739,
        ["Tisseur de flammes Cavepierre"] = 7739,
    },
    ["Renfort d'armure léger"] = {
        Professions = 2152,
    },
    ["Etreinte de Gaïa"] = {
        Professions = 28210,
    },
    ["Ench. de plastron (Mana supérieur)"] = {
        Professions = 13663,
    },
    ["Poison de sangsue"] = {
        ["Traqueuse sanguine"] = 3358,
        ["Traqueuse des grottes"] = 3358,
        ["Marche-boue Noctant"] = 8382,
    },
    ["Flammes infernales"] = {
        ["WARLOCK"] = 11684,
    },
    ["Ench. de bracelets (Endurance)"] = {
        Professions = 13648,
    },
    ["Ench. d'arme (Frappe)"] = {
        Professions = 13693,
    },
    ["Invocation d'un palefroi corrompu"] = {
        ["WARLOCK"] = 5784,
    },
    ["Sac sans fond"] = {
        Professions = 18455,
    },
    ["Toxine de chaudron altérée"] = {
        ["Fermier Dalson"] = 17650,
    },
    ["Guérison du familier"] = {
        ["HUNTER"] = 13544,
    },
    ["Saccager"] = {
        ["Agathelos l'Enragé"] = 8285,
    },
    ["Harpon empoisonné"] = {
        ["Capitaine Vertepeau"] = 5208,
    },
    ["Effet Sloth"] = {
        ["Paresse"] = 3510,
    },
    ["Don d'Arthas"] = {
        Professions = 11466,
    },
    ["Restauration des ténèbres"] = {
        ["Garde-loup d'Ombrecroc"] = 7106,
    },
    ["Elixir d'Agilité"] = {
        Professions = 11449,
    },
    ["Sceau de sagesse"] = {
        ["PALADIN"] = 20357,
    },
    ["Maléfice de Ravenclaw"] = {
        ["Thule Ravenclaw"] = 7655,
        ["Thule Ravenclaw"] = 7656,
        ["Thule Ravenclaw"] = 7657,
    },
    ["Attaque pernicieuse"] = {
        ["ROGUE"] = 11294,
    },
    ["Haubergeon grossier en cuivre"] = {
        Professions = 12260,
    },
    ["Robe tisse-givre"] = {
        Professions = 18404,
    },
    ["Rouage en thorium"] = {
        Professions = 19791,
    },
    ["Armure verte en cuir"] = {
        Professions = 3772,
    },
    ["Totem de Feu lunaire"] = {
        ["Totem de Feu lunaire"] = 15787,
        ["Totémique Tombe-hiver"] = 15787,
    },
    ["Faible Eclair de givre"] = {
        ["Novice Frostmane"] = 6949,
    },
    ["Chapelet de grandes comètes bleues"] = {
        Professions = 26426,
    },
    ["Mélasse"] = {
        ["Limace"] = 3514,
    },
    ["Lancer de potion"] = {
        ["Brasseur de Dalaran"] = 7638,
    },
    ["Jambières en étoffe lunaire"] = {
        Professions = 18440,
    },
    ["Flèche de dispersion"] = {
        ["HUNTER"] = 19503,
    },
    ["Hystérique"] = {
        ["Grell sauvage"] = 5915,
        ["Flamboyeur Radison"] = 5915,
    },
    ["Marque de la honte"] = {
        ["Parqual Fintallas"] = 6767,
    },
    ["Jambières en bronze argenté"] = {
        Professions = 12259,
    },
    ["Brassards en cuir cousu main"] = {
        Professions = 9059,
    },
    ["Portail du Bouclier balafré"] = {
        ["Portail du Bouclier balafré"] = 15125,
        ["Démoniste du Bouclier balafré"] = 15125,
    },
    ["Evasion"] = {
        ["ROGUE"] = 5277,
    },
    ["Hache de guerre en bronze"] = {
        Professions = 9987,
    },
    ["Fusil-dragon des gobelins"] = {
        Professions = 12908,
    },
    ["Epines"] = {
        ["DRUID"] = 9910,
    },
    ["Fantômes hanteurs"] = {
        ["Citoyen spectral"] = 16336,
        ["Citoyen fantomatique"] = 16336,
    },
    ["Don du fauve"] = {
        ["DRUID"] = 21850,
    },
    ["Rafale de flammes"] = {
        ["Prophète de la Lame ardente"] = 9658,
    },
    ["Diadème radieux"] = {
        Professions = 16659,
    },
    ["Invocation d'un garde-loup worg"] = {
        ["Garde-loup worg"] = 7107,
        ["Garde-loup d'Ombrecroc"] = 7107,
    },
    ["Désarmement"] = {
        ["WARRIOR"] = 676,
        ["Fouilleboue"] = 8379,
        ["Geôlier Borhuin"] = 8379,
        ["Kenata Dabyrie"] = 8379,
        ["Myrmidon de Brassenoire"] = 8379,
        ["Claque-pinces Troublefond"] = 8379,
        ["Guerrier Bloodscalp"] = 6713,
        ["Pillard défias"] = 6713,
        ["Sneed"] = 6713,
        ["Premier officier Snellig"] = 6713,
        ["Prisonnier défias"] = 6713,
        ["Larron du Syndicat"] = 6713,
        ["Fermier d'Hillsbrad"] = 6713,
        ["Nervi du crépuscule"] = 6713,
        ["Rôdeur du Syndicat"] = 6713,
        ["Seigneur Falconcrest"] = 6713,
        ["Massacreur de la KapitalRisk"] = 6713,
        ["Soldat de marine de Theramore"] = 6713,
        ["Chauve-souris vile"] = 6713,
        ["Dal Griffesang"] = 6713,
        ["Volant de l'effroi"] = 6713,
        ["Garde du corps défias"] = 6713,
        ["Bandit du Totem sinistre"] = 6713,
    },
    ["Froid épouvantable"] = {
        ["Elémentaire d’eau souillé"] = 6873,
        ["Maréen"] = 6873,
    },
    ["Transi(e) d'amour"] = {
        ["Kalin Colportecoeur"] = 27572,
    },
    ["Sombre prédiction de résistance de Sayge"] = {
        ["Sayge"] = 23769,
    },
    ["Comète, ROUGE GRANDE"] = {
        ["Type des feux d'artifice de Pat - GRAND ROUGE"] = 26354,
    },
    ["Grâce d'Elune"] = {
        ["PRIEST"] = 19293,
    },
    ["Pierre à aiguiser solide"] = {
        Professions = 9918,
    },
    ["Robe noire en tisse-mage"] = {
        Professions = 12050,
    },
    ["Dard empoisonné"] = {
        ["Cours-la-côte murloc"] = 7357,
        ["Bandit Rivepatte"] = 7357,
    },
    ["Huile de sorcier"] = {
        Professions = 25128,
    },
    ["Eclair de radiation"] = {
        ["Envahisseur irradié"] = 9771,
        ["Pilleur irradié"] = 9771,
    },
    ["Bénédiction du sanctuaire supérieure"] = {
        ["PALADIN"] = 25899,
    },
    ["Morsure de serpent"] = {
        ["HUNTER"] = 25295,
    },
    ["Robe blanche en lin"] = {
        Professions = 7624,
    },
    ["Transmutation du Feu en Terre"] = {
        Professions = 17560,
    },
    ["Piétinement"] = {
        ["Golem de guerre lourd"] = 12612,
    },
    ["Invocation d'une explosion de lave H"] = {
        ["Rat du Magma"] = 21906,
    },
    ["Potion de soins"] = {
        Professions = 3447,
    },
    ["Brouillage"] = {
        ["Ténébreux d'Ombrecroc"] = 8140,
        ["Aspect de banalité"] = 8140,
    },
    ["Découverte de trésors"] = {
        Racial = 2481,
    },
    ["Robe grise en laine"] = {
        Professions = 2403,
    },
    ["Sang froid"] = {
        ["ROGUE"] = 14177,
    },
    ["Ceinture d'homme des collines"] = {
        Professions = 3767,
    },
    ["Malédiction de Mornecoeur"] = {
        ["Satyre Mornecoeur"] = 6946,
        ["Entourloupeur Mornecoeur"] = 6946,
        ["Traqueur des ténèbres Mornecoeur"] = 6946,
        ["Invocateur infernal Mornecoeur"] = 6946,
    },
    ["Casque de mineur gobelin"] = {
        Professions = 12717,
    },
    ["Coupure mutilante"] = {
        ["Nelson le Gentil"] = 23279,
    },
    ["Lanceur de chapelets de fusées"] = {
        Professions = 26443,
    },
    ["Sombre prédiction d'Endurance de Sayge"] = {
        ["Sayge"] = 23737,
    },
    ["Explosion pyrotechnique"] = {
        ["MAGE"] = 18809,
    },
    ["Rappelle le soldat à la vie"] = {
        ["Demetria"] = 19721,
        ["Soldat écarlate"] = 19721,
    },
    ["Lunettes de maître ingénieur"] = {
        Professions = 19825,
    },
    ["Invocation de Lotwil"] = {
        ["Lotwil Veriatus"] = 5001,
    },
    ["Ench. de plastron (Caractéristiques)"] = {
        Professions = 13941,
    },
    ["Bouclier sacré"] = {
        ["PALADIN"] = 20928,
    },
    ["Bandeau de la nuit"] = {
        Professions = 10507,
    },
    ["Invocation d'un totem de glace"] = {
        ["Totem de glace"] = 18975,
        ["Chaman Follepatte"] = 18975,
    },
    ["Longue-vue ornée"] = {
        Professions = 6458,
    },
    ["Siphon d'âme"] = {
        ["WARLOCK"] = 11675,
    },
    ["Bénédiction de Nordrassil"] = {
        ["Eris Havenfire"] = 23108,
    },
    ["Esprit de Kirith"] = {
        ["Esprit de Kirith"] = 10853,
        ["Kirith le Damné"] = 10853,
    },
    ["Ench. de bottes (Esprit inférieur)"] = {
        Professions = 13687,
    },
    ["Ravage"] = {
        ["DRUID"] = 9867,
        ["Grande chauve-souris du crépuscule"] = 3242,
        ["Chauve-souris du crépuscule vampirique"] = 3242,
        ["Griffe galeuse"] = 3242,
        ["Ours des chardons"] = 3242,
        ["Vieil ours des chardons"] = 3242,
        ["Mère de la tanière"] = 3242,
        ["Jeune chardon"] = 3242,
        ["Gueule d'acier d'Aku'mai"] = 8391,
    },
    ["Bâtonnet runique en argent"] = {
        Professions = 7795,
    },
    ["Vent léger"] = {
        ["Prate Cloudseer"] = 8385,
    },
    ["Capuche d'enchanteur"] = {
        Professions = 3857,
    },
    ["Sonde mentale"] = {
        ["Infiltrateur du Bouclier balafré"] = 16037,
    },
    ["Chemise de smoking"] = {
        Professions = 12085,
    },
    ["Ench. de plastron (Mana mineur)"] = {
        Professions = 7443,
    },
    ["Ceinture cramoisie en soie"] = {
        Professions = 8772,
    },
    ["Choc martial"] = {
        Racial = 20549,
        ["Marcheur des falaises"] = 11876,
        ["Protecteur errant"] = 45,
        ["Harb Foulmountain"] = 45,
    },
    ["Invocation des braises"] = {
        ["Braise"] = 10869,
        ["Serviteur enflammé"] = 10869,
    },
    ["Contagion pourrissante"] = {
        ["Cadavre fétide"] = 7102,
        ["Herboriste Réprouvé"] = 7102,
    },
    ["Horion sacré"] = {
        ["PALADIN"] = 20930,
    },
    ["Chapelet de comètes rouges"] = {
        Professions = 26425,
    },
    ["Petite charge d'hydroglycérine"] = {
        Professions = 3933,
    },
    ["Prière de soins"] = {
        ["PRIEST"] = 25316,
    },
    ["Désengagement"] = {
        ["HUNTER"] = 14273,
    },
    ["Ench. de cape (Protection mineure)"] = {
        Professions = 7771,
    },
    ["Chute"] = {
        ["Exhalombre Hakkari"] = 6869,
    },
    ["Robe rouge en lin"] = {
        Professions = 2389,
    },
    ["Compagnon de la Voile sanglante"] = {
        ["Compagnon défias"] = 5172,
        ["Pirate défias"] = 5172,
    },
    ["Poignard mortel en bronze"] = {
        Professions = 3295,
    },
    ["Volonté renforcée"] = {
        ["Chef de guerre Pierre-du-pic"] = 16171,
    },
    ["Drain de mana"] = {
        ["WARLOCK"] = 11704,
    },
    ["Bénédiction de Thule"] = {
        ["Manouvrier Ravenclaw"] = 3269,
    },
    ["Coup de pied rapide"] = {
        ["Bandit défias"] = 8646,
        ["Portefaix défias"] = 8646,
        ["Estafette d'Ashenvale"] = 8646,
    },
    ["Gilet en étoffe lunaire"] = {
        Professions = 18447,
    },
    ["Ench. de bouclier (Esprit)"] = {
        Professions = 13659,
    },
    ["Limon ralentisseur"] = {
        ["Limon primitif"] = 16050,
    },
    ["Essence de douleur"] = {
        ["ROGUE"] = 2930,
    },
    ["Intelligence altérée"] = {
        ["Sanglier tacheté corrompu"] = 6818,
        ["Chaman Vile-sylve"] = 6818,
        ["Totémique Vile-sylve"] = 6818,
    },
    ["Acide de tunnelier"] = {
        ["Tunnelière Gorishi"] = 14120,
        ["Garde Gorishi"] = 14120,
        ["Fouisseur des rochers"] = 14120,
    },
    ["Création de Pierre de sort (supérieure)"] = {
        ["WARLOCK"] = 17727,
    },
    ["Tape-crâne"] = {
        ["Mineur Mufle-de-gravier"] = 3551,
    },
    ["Générateur de bouclier"] = {
        ["Chariotte à vapeur"] = 27759,
    },
    ["Blizzard"] = {
        ["MAGE"] = 10187,
        ["Lokholar le Seigneur de glace"] = 21367,
    },
    ["Sombre prédiction d'Intelligence de Sayge"] = {
        ["Sayge"] = 23766,
    },
    ["Bouclier de feu"] = {
        Pet = 11771,
        ["Mage renégat défias"] = 134,
        ["Géomancien Rat des tunnels"] = 134,
        ["Gardien de Dalaran"] = 134,
        ["Mage de Dalaran"] = 134,
        ["Manifestation de feu mineure"] = 134,
        ["Géomancien Blanche-moustache"] = 18968,
    },
    ["Bénédiction de puissance"] = {
        ["PALADIN"] = 25291,
    },
    ["Volonté des Réprouvés"] = {
        Racial = 7744,
    },
    ["Plaie du dragon"] = {
        ["Légionnaire de l'Aile noire"] = 23967,
        ["Draconide Griffemort"] = 23967,
        ["Garde de l'Aile noire"] = 23967,
    },
    ["Mort rampante"] = {
        ["Mort rampante"] = 23589,
        ["Nelson le Gentil"] = 23589,
    },
    ["Poison douloureux IV"] = {
        ["ROGUE"] = 13230,
    },
    ["Harnais barbare"] = {
        Professions = 6661,
    },
    ["Gantelets en écailles de dragon"] = {
        Professions = 10619,
    },
    ["Aspect de la meute"] = {
        ["HUNTER"] = 13159,
    },
    ["Projectiles des arcanes"] = {
        ["MAGE"] = 25345,
    },
    ["Force d'Arko'narin"] = {
        ["Arko'narin capturée"] = 18163,
    },
    ["Poison mortel IV"] = {
        ["ROGUE"] = 11358,
    },
    ["Amplification de la magie"] = {
        ["MAGE"] = 10170,
    },
    ["Tombeau de glace"] = {
        ["Lokholar le Seigneur de glace"] = 16869,
    },
    ["Crée une Essence de Rejeton"] = {
        ["Rejeton noir"] = 16027,
        ["Rejeton brûlant"] = 16027,
        ["Rejeton Flammécaille"] = 16027,
    },
    ["Grande hache en thorium"] = {
        Professions = 16971,
    },
    ["Aura de résistance"] = {
        ["Messager cramoisi"] = 19726,
    },
    ["Fracasser armure"] = {
        ["WARRIOR"] = 11597,
        ["Pince-tranchante Makrinni"] = 13444,
        ["Pinceur Claquesec"] = 13444,
        ["Champion garde-huran"] = 15572,
        ["Terrassier Blanche-moustache"] = 15572,
        ["Chef d'escadrille Guse"] = 15572,
        ["Chef d'escadrille Jeztor"] = 15572,
        ["Chef d'escadrille Mulverick"] = 15572,
        ["Péon de Froidemine"] = 15572,
        ["Mineur de Froidemine"] = 15572,
        ["Mineur de Gouffrefer"] = 15572,
        ["Péon de Gouffrefer"] = 15572,
        ["Officier Jaxon"] = 15572,
        ["Lanceur de filet murloc"] = 11971,
        ["Mineur Rivepatte"] = 11971,
        ["Brute Poil-Putride"] = 11971,
        ["Garde d'Ironforge"] = 11971,
        ["Fondeboue"] = 11971,
        ["Défricheur de la Horde"] = 11971,
    },
    ["Lunettes gnomes"] = {
        Professions = 12897,
    },
    ["Poing de pierre"] = {
        ["Ogre Rochepoing"] = 4955,
        ["Massacreur Rochepoing"] = 4955,
        ["Brute Rochepoing"] = 4955,
        ["Marteleur Rochepoing"] = 4955,
        ["Seigneur Rochepoing"] = 4955,
    },
    ["Bandeau en soie"] = {
        Professions = 8762,
    },
    ["Gants barbares en fer"] = {
        Professions = 9820,
    },
    ["Cri de guerre"] = {
        ["WARRIOR"] = 25289,
        ["Narg le Sous-chef"] = 9128,
        ["Capitaine Plaiedécaille"] = 9128,
        ["Alpha Poil-moussu"] = 9128,
        ["Insurgé défias"] = 9128,
        ["Bazil Thredd"] = 9128,
        ["Ma'ruk Wyrmscale"] = 9128,
        ["Coursier kolkar"] = 9128,
        ["Massacreur de la KapitalRisk"] = 9128,
        ["Capitaine Fairmount"] = 9128,
        ["Seigneur Brusquebroche"] = 9128,
    },
    ["Hurlement perçant"] = {
        ["WARRIOR"] = 12323,
    },
    ["Atténuation de la magie"] = {
        ["MAGE"] = 10174,
    },
    ["Aspect du singe"] = {
        ["HUNTER"] = 13163,
    },
    ["Cri de défi"] = {
        ["WARRIOR"] = 1161,
    },
    ["Secousse violente"] = {
        ["Destructeur de lave"] = 19129,
        ["Baron Kazum"] = 19129,
    },
    ["Gants en peau d'ombre"] = {
        Professions = 22711,
    },
    ["Ench. d'arme (Tueur d'élémentaire inférieur)"] = {
        Professions = 13655,
    },
    ["Suicide"] = {
        ["Œuf de rampant silithide"] = 7,
    },
    ["Pistage des élémentaires"] = {
        ["HUNTER"] = 19880,
    },
    ["Chemise grise en laine"] = {
        Professions = 2406,
    },
    ["Nova de glace"] = {
        ["Flagellant phasique"] = 22519,
    },
    ["Elixir de Défense"] = {
        Professions = 3177,
    },
    ["Totem guérisseur"] = {
        ["SHAMAN"] = 10463,
    },
    ["Gardien de l'oeil"] = {
        ["Capitaine Halyndor"] = 3389,
    },
    ["Démon invoqué"] = {
        ["Marcheur du Vide invoqué"] = 7741,
        ["Succube invoquée"] = 7741,
        ["Chasseur corrompu invoqué"] = 7741,
        ["Garde de sang Hakkar'i"] = 7741,
        ["Confineur de cauchemar"] = 7741,
        ["Esprit d'Hurleur"] = 7741,
        ["Arugal"] = 7741,
        ["Esprit démon"] = 7741,
    },
    ["Chaperon azur en soie"] = {
        Professions = 8760,
    },
    ["Potion de Rage"] = {
        Professions = 6617,
    },
    ["Création de Pierre de soins (majeure)"] = {
        ["WARLOCK"] = 11730,
    },
    ["Invocation d'un exilé prismatique"] = {
        ["Exilé prismatique"] = 10388,
        ["Myzrael"] = 10388,
    },
    ["Potion de Bouclier de pierre inférieure"] = {
        Professions = 4942,
    },
    ["Cape azur en soie"] = {
        Professions = 8786,
    },
    ["Annulation de masse"] = {
        ["Annulateur d’Arcane X-21"] = 10832,
    },
    ["Dragonne en acier"] = {
        Professions = 7224,
    },
    ["Giberne en cuir lourd"] = {
        Professions = 9194,
    },
    ["Ench. de bottes (Agilité inférieure)"] = {
        Professions = 13637,
    },
    ["Cuirasse en argent lumineux"] = {
        Professions = 2675,
    },
    ["Peste de goule"] = {
        ["Goule pestilentielle"] = 16458,
    },
    ["Intelligence des arcanes"] = {
        ["MAGE"] = 10157,
    },
    ["Tourment"] = {
        Pet = 11775,
    },
    ["Eclats d'os"] = {
        ["Squelette errant"] = 17014,
    },
    ["Pierre effritée"] = {
        ["Elémentaire asservi"] = 3672,
    },
    ["Arme Croque-roc"] = {
        ["SHAMAN"] = 16316,
    },
    ["Essaim d'insectes"] = {
        ["DRUID"] = 24977,
    },
    ["Esprit corrompu"] = {
        ["Mage maudit"] = 16567,
    },
    ["Lunette de sniper"] = {
        Professions = 12620,
    },
    ["Mine gobeline"] = {
        Professions = 3968,
    },
    ["Bottes grossières en bronze"] = {
        Professions = 7817,
    },
    ["Armure en cuir raffermi"] = {
        Professions = 2166,
    },
    ["Morsures purulentes"] = {
        ["Insecte pestiféré"] = 16460,
    },
    ["Vision du sapta"] = {
        ["Tiev Mordune"] = 9735,
    },
    ["Lunettes teintées vertes"] = {
        Professions = 3956,
    },
    ["Force de la nature"] = {
        ["Force de la nature"] = 6913,
        ["Redresseur de torts cénarien"] = 6913,
    },
    ["Totem de résistance à la Nature"] = {
        ["SHAMAN"] = 10601,
    },
    ["Drain de vie"] = {
        ["WARLOCK"] = 11700,
        ["Varimathras"] = 20743,
    },
    ["Grande bombe en bronze"] = {
        Professions = 3950,
    },
    ["Ench. de plastron (Santé excellente)"] = {
        Professions = 13858,
    },
    ["Eclair de givre"] = {
        ["MAGE"] = 25304,
        ["Lokholar le Seigneur de glace"] = 21369,
        ["Sorcier-voleur défias"] = 13322,
        ["Ossomancien Sombroeil"] = 13322,
        ["Néophyte écarlate"] = 13322,
        ["Apprenti de Dalaran"] = 13322,
        ["Ensorceleuse Furie-des-vents"] = 13322,
        ["Maître des flots murloc mineur"] = 9672,
        ["Ensorceleur Squelette"] = 9672,
        ["Mage Cassecrête"] = 9672,
        ["Magus du Syndicat"] = 9672,
        ["Ensorceleur écarlate"] = 9672,
        ["Adepte de la Tête de Mort"] = 9672,
        ["Prêtresse des marées de Brassenoire"] = 9672,
        ["Aquamancien du crépuscule"] = 9672,
        ["Tisse-magie draconique"] = 9672,
        ["Ensorceleuse du Néant"] = 20297,
        ["Sorcière de Strashaz"] = 12737,
        ["Mage squelette"] = 20792,
        ["Tisseur de brumes Poil-moussu"] = 20792,
        ["Sorcier de Dalaran"] = 20792,
        ["Archimage Ataeric"] = 20792,
        ["Ensorceleuse Tempécaille"] = 20792,
        ["Kuz"] = 20792,
        ["Delmanis le Haï"] = 20792,
        ["Ensorceleuse Irequeue"] = 20792,
        ["Géomancien des Cisailles"] = 20792,
        ["Conseiller d'Hillsbrad"] = 20806,
        ["Sarilus Foulborne"] = 20806,
        ["Géomètre Mufle-de-gravier"] = 20806,
        ["Eliza"] = 20819,
        ["Théurge de Dalaran"] = 20819,
        ["Sorcière Ondulame"] = 20819,
        ["Seigneur des marées Selécaille"] = 20822,
        ["Baron Vardus"] = 20822,
        ["Magus Rochepoing"] = 20822,
        ["Géomètre Sèche-moustache"] = 20822,
        ["Seigneur Branchie-bistre"] = 20822,
        ["Fouilleur Makrinni"] = 20822,
        ["Baron Aquanis"] = 15043,
    },
    ["Robe en gangrétoffe"] = {
        Professions = 18451,
    },
    ["Haubert en fer émeraude"] = {
        Professions = 3508,
    },
    ["Conflagration"] = {
        ["WARLOCK"] = 18932,
    },
    ["Marteau iridescent"] = {
        Professions = 6518,
    },
    ["Gantelets en écailles de dragon vert"] = {
        Professions = 24655,
    },
    ["Sprint"] = {
        ["ROGUE"] = 11305,
    },
    ["Aura de putréfaction"] = {
        ["Balafré"] = 3106,
    },
    ["Totem de Glèbe"] = {
        ["SHAMAN"] = 8177,
    },
    ["Colère d'Amnennar"] = {
        ["Amnennar le Porte-froid"] = 13009,
    },
    ["Ench. de bracelets (Force inférieure)"] = {
        Professions = 13536,
    },
    ["Marteau de guerre en bronze"] = {
        Professions = 9985,
    },
    ["Sac démon de Xabraxxis"] = {
        ["Xabraxxis"] = 19127,
    },
    ["Attirer esprit"] = {
        ["Léthon"] = 24811,
    },
    ["Piège de givre"] = {
        ["HUNTER"] = 13809,
    },
    ["Leurre perfectionné - Evénement 003"] = {
        ["Leurre perfectionné de l'équipe du mortier"] = 19723,
        ["Eclaireur Klemmy"] = 19723,
    },
    ["Dynamite puissante"] = {
        Professions = 3946,
    },
    ["Invocation d'un démon de l'Orbe"] = {
        ["Démon de l'Orbe"] = 9097,
        ["Tabetha"] = 9097,
    },
    ["Ench. d'arme 2M (Impact inférieur)"] = {
        Professions = 13529,
    },
    ["Gantelets en vrai-argent"] = {
        Professions = 9954,
    },
    ["Blocage amélioré"] = {
        ["Malfrat défias"] = 3248,
        ["Capitaine Vachon"] = 3248,
        ["Gardien Ravenclaw"] = 3248,
        ["Guerrier Tempécaille"] = 3248,
        ["Montagnard de Dun Garok"] = 3248,
        ["Garde de guerre Tranchecrin"] = 3248,
        ["Lieutenant Benedict"] = 3248,
        ["Défenseur de Tranchebauge"] = 3248,
        ["Soldat de Rempart-du-Néant"] = 3248,
        ["Centurion Dragonmaw"] = 3419,
        ["Kam Deepfury"] = 3419,
        ["Fantassin d'Hillsbrad"] = 3419,
        ["Guerrier de la jungle de Kurzen"] = 3639,
        ["Défenseur de Stromgarde"] = 3639,
        ["Soldat écarlate"] = 3639,
        ["Défenseur écarlate"] = 3639,
        ["Défenseur du Refuge de l'Ornière"] = 3639,
    },
    ["Bottes en lin à semelle souple"] = {
        Professions = 3845,
    },
    ["Mot de pouvoir : Robustesse"] = {
        ["PRIEST"] = 10938,
        ["Infirmier du Hangar"] = 13864,
    },
    ["Brassards verts en cuir"] = {
        Professions = 3776,
    },
    ["Phylactère d’Araj"] = {
        ["Araj l'Invocateur"] = 18661,
    },
    ["Distributeur de bombes des gobelins"] = {
        Professions = 12755,
    },
    ["Epaulières armées du scorpide"] = {
        Professions = 10564,
    },
    ["Bottes en cuir cousu main"] = {
        Professions = 2149,
    },
    ["Loup fantôme"] = {
        ["SHAMAN"] = 2645,
    },
    ["Potion de sang de troll forte"] = {
        Professions = 3176,
    },
    ["Feinte"] = {
        ["ROGUE"] = 25302,
    },
    ["Ench. de bracelets (Endurance inférieure)"] = {
        Professions = 13501,
    },
    ["Cape du grand vaudou"] = {
        Professions = 10562,
    },
    ["Sac en gangrétoffe"] = {
        Professions = 26086,
    },
    ["Riche chemise violette en soie"] = {
        Professions = 3872,
    },
    ["Ench. de gants (Pêche)"] = {
        Professions = 13620,
    },
    ["Détection de l'invisibilité inférieure"] = {
        ["WARLOCK"] = 132,
    },
    ["Morsure de la mangouste"] = {
        ["HUNTER"] = 14271,
    },
    ["Téléportation : Stormwind"] = {
        ["MAGE"] = 3561,
    },
    ["Ressusciter"] = {
        ["Esprit enchaîné"] = 24341,
    },
    ["Ceinture barbare"] = {
        Professions = 3779,
    },
    ["Ench. de bouclier (Endurance supérieure)"] = {
        Professions = 20017,
    },
    ["Assaut de la corne noire"] = {
        ["Cerf à corne noire"] = 6921,
        ["Vieux cerf à corne noire"] = 6921,
    },
    ["Flèche de givre"] = {
        ["Maître des bêtes de Tranchebauge"] = 6984,
        ["Sirène Fouette-bile"] = 12551,
    },
    ["Présence de la mort"] = {
        ["Morbent Fel"] = 3109,
    },
    ["Invocation d'un fusilier cramoisi"] = {
        ["Fusillier cramoisi"] = 17279,
    },
    ["Propulseur nitreux"] = {
        ["Chariotte à vapeur"] = 27746,
    },
    ["Regard de cristal"] = {
        ["Basilic de pierre"] = 3635,
        ["Basilic de cristal"] = 3635,
        ["Contemplateur du sel gemme"] = 3635,
        ["Contemplateur Peau-de-verre"] = 3635,
        ["Basilic Rougepierre"] = 3635,
        ["Oeil-de-mort"] = 3635,
    },
    ["Bottes chimériques"] = {
        Professions = 19063,
    },
    ["Drain d’esclave"] = {
        ["Mogh l'Immortel"] = 8809,
    },
    ["Inciter les Flammes"] = {
        ["Marchefeu"] = 19635,
    },
    ["Rugissement glacial"] = {
        ["Vagash"] = 3143,
    },
    ["Espingole grossière"] = {
        Professions = 3925,
    },
    ["Catalyseur nauséabond"] = {
        ["Scorpide corrompu"] = 5413,
    },
    ["Maléfice"] = {
        ["Grande prêtresse Hai'watna"] = 18503,
        ["Bom'bay"] = 16707,
        ["Bom'bay"] = 16708,
        ["Bom'bay"] = 16709,
    },
    ["Camail en mithril"] = {
        Professions = 9961,
    },
    ["Bouclier de foudre"] = {
        ["SHAMAN"] = 10432,
        ["Tempétueux de poussière"] = 19514,
        ["Chaman Mort-bois"] = 13585,
        ["Oracle Arkkoran"] = 12550,
        ["Tempête vivante"] = 12550,
        ["Lorgus Jett"] = 12550,
        ["Chaman Frostwolf"] = 12550,
        ["Chaman Bloodscalp"] = 8788,
        ["Légion-tempête magram"] = 8788,
        ["Légion-tempête maraudine"] = 8788,
    },
    ["Choc de flammes"] = {
        ["MAGE"] = 10216,
        ["Evocateur défias"] = 11829,
        ["Prophète de la Lame ardente"] = 11829,
        ["Tisseur de flamme Brisepoing"] = 20296,
        ["Roussi"] = 12468,
        ["Géomancienne Dos-hirsute"] = 20794,
        ["Géomancien du Totem sinistre"] = 20813,
    },
    ["Vision assombrie"] = {
        ["Fée sombre"] = 5514,
    },
    ["Huile de feu"] = {
        Professions = 7837,
    },
    ["Télécommande universelle gnome"] = {
        Professions = 9269,
    },
    ["Soins inférieurs"] = {
        ["PRIEST"] = 2053,
    },
    ["Invocation des marionnettes d'Helcular"] = {
        ["Marionnette d'Helcular"] = 4950,
        ["Cadavre d'Helcular"] = 4950,
    },
    ["Sceau d'autorité"] = {
        ["PALADIN"] = 20920,
    },
    ["Roi des Gordok"] = {
        ["Mizzle l'Ingénieux"] = 22799,
    },
    ["Elixir de Tueur de démons"] = {
        Professions = 11477,
    },
    ["Tamis à sel"] = {
        Professions = 19567,
    },
    ["Jambières barbares"] = {
        Professions = 7149,
    },
    ["Totem de Magma"] = {
        ["SHAMAN"] = 10587,
    },
    ["Mine terrestre sombrefer"] = {
        ["Mine terrestre sombrefer"] = 11802,
        ["Agent Sombrefer"] = 11802,
        ["Thauris Balgarr"] = 11802,
    },
    ["Asservir démon"] = {
        ["WARLOCK"] = 11726,
    },
    ["Phase terminée"] = {
        ["Gazban"] = 3648,
    },
    ["Un bonbon ou une blague"] = {
        ["Aubergiste Farley"] = 24751,
        ["Belm l'aubergiste"] = 24751,
        ["Helbrek l'aubergiste"] = 24751,
        ["Anderson l'aubergiste"] = 24751,
        ["Shay l'aubergiste"] = 24751,
        ["Boorand Plainswind l'aubergiste"] = 24751,
        ["Firebrew l'aubergiste"] = 24751,
        ["Renée l'aubergiste"] = 24751,
        ["Aubergiste Thulbek"] = 24751,
        ["Janene l'aubergiste"] = 24751,
        ["Brianna l'aubergiste"] = 24751,
        ["Hearthstove l'aubergiste"] = 24751,
        ["Saelienne l'aubergiste"] = 24751,
        ["Keldamyr l'aubergiste"] = 24751,
        ["Shaussiy l'aubergiste"] = 24751,
        ["Kimlya l'aubergiste"] = 24751,
        ["Aubergiste Bates"] = 24751,
        ["Aubergiste Allison"] = 24751,
        ["Norman l'aubergiste"] = 24751,
        ["Pala l'aubergiste"] = 24751,
        ["Kauth l'aubergiste"] = 24751,
        ["Trelayne l'aubergiste"] = 24751,
        ["Wiley l'aubergiste"] = 24751,
        ["Skindle l'aubergiste"] = 24751,
        ["Grosk l'aubergiste"] = 24751,
        ["Gryshka l'aubergiste"] = 24751,
        ["Karakul l'aubergiste"] = 24751,
        ["Aubergiste Byula"] = 24751,
        ["Aubergiste Jayka"] = 24751,
        ["Aubergiste Fizzgrimble"] = 24751,
        ["Aubergiste Shyria"] = 24751,
        ["Aubergiste Greul"] = 24751,
        ["Aubergiste Thulfram"] = 24751,
        ["Aubergiste Heather"] = 24751,
        ["Aubergiste Shul'kar"] = 24751,
        ["Aubergiste Adegwa"] = 24751,
        ["Aubergiste Lyshaerya"] = 24751,
        ["Aubergiste Sikewa"] = 24751,
        ["Aubergiste Abeqwa"] = 24751,
        ["Aubergiste Vizzie"] = 24751,
        ["Aubergiste Kaylisk"] = 24751,
        ["Lard"] = 24751,
        ["Calandrath"] = 24751,
        ["Aubergiste Faralia"] = 24751,
    },
    ["Vortex de mana"] = {
        ["Dragon-faë"] = 7994,
    },
    ["Cape élégante en cuir"] = {
        Professions = 2159,
    },
    ["Etreinte de glace"] = {
        ["Vieux Barbeglace"] = 3145,
    },
    ["Délivrance de la malédiction"] = {
        ["DRUID"] = 2782,
    },
    ["Drain sanglant"] = {
        ["Glouton d'Ombrecroc"] = 7122,
    },
    ["Poison paralysant"] = {
        ["Cliqueteuse"] = 3609,
        ["Solitaire putride"] = 3609,
        ["Entourloupeur Follengeance"] = 3609,
        ["Régisseuse sanglante de Kirtonos"] = 3609,
        ["Poigne-de-mort"] = 3609,
    },
    ["Pantalon corrompu en cuir"] = {
        Professions = 19083,
    },
    ["Ench. de bottes (Endurance)"] = {
        Professions = 13836,
    },
    ["Eruption massive"] = {
        ["Lige du feu"] = 20483,
    },
    ["Bottes en fer émeraude"] = {
        Professions = 3334,
    },
    ["Eperons en mithril"] = {
        Professions = 9964,
    },
    ["Piqûre de scorpide"] = {
        ["HUNTER"] = 14277,
        ["Estafette d'Ashenvale"] = 18545,
    },
    ["Forme de traquelune"] = {
        ["Terenthis"] = 6236,
    },
    ["Transformation de Kirtonos"] = {
        ["Kirtonos le Héraut"] = 16467,
    },
    ["Invocation d'une explosion de lave C"] = {
        ["Rat du Magma"] = 21901,
    },
    ["Invocation d'un sanglier de guerre apprivoisé"] = {
        ["Sanglier de guerre apprivoisé"] = 8274,
        ["Dresseur de Tranchebauge"] = 8274,
        ["Maître des bêtes de Tranchebauge"] = 8274,
    },
    ["Invocation d'une créature des tourbières"] = {
        ["Créature des tourbières"] = 8857,
        ["Seigneur des tourbières"] = 8857,
    },
    ["Jupe simple"] = {
        Professions = 8465,
    },
    ["Invocation de gardiens zuliens"] = {
        ["Gardien zulien"] = 24183,
    },
    ["Totem incendiaire"] = {
        ["SHAMAN"] = 10438,
    },
    ["Chaînes de glace"] = {
        ["Sorcier défias"] = 113,
        ["Adepte de la Tête de Mort"] = 113,
        ["Archimage Ataeric"] = 512,
    },
    ["Défibrillateur gobelin XL"] = {
        Professions = 23078,
    },
    ["Aspect du guépard"] = {
        ["HUNTER"] = 5118,
    },
    ["Elixir de Force de brute"] = {
        Professions = 17557,
    },
    ["Feu rouge"] = {
        ["Appareil bidulotronique de Wrenix"] = 6668,
    },
    ["Pierre philosophale"] = {
        Professions = 11459,
    },
    ["Lancer de dynamite"] = {
        ["Ingénieur de la KapitalRisk"] = 7978,
        ["Terrassier Flameforge"] = 7978,
        ["Paysan Ombreforge"] = 7978,
    },
    ["Invocation du B.G.A.C."] = {
        ["Appareil bidulotronique de Wrenix"] = 9977,
    },
    ["Points d'honneur +398"] = {
        ["Porteguerre de la Horde"] = 24923,
        ["Général de brigade de l'Alliance"] = 24923,
    },
    ["Forme d’ours"] = {
        ["DRUID"] = 5487,
        ["Naturaliste du Totem sinistre"] = 19030,
        ["Protecteur cénarien"] = 7090,
        ["Uthil Mooncall"] = 7090,
        ["Conservateur cénarien"] = 7090,
        ["Grifferage"] = 7090,
    },
    ["Fonte du bronze"] = {
        Professions = 2659,
    },
    ["Ceinture de l'araignée"] = {
        Professions = 3863,
    },
    ["Gelée putride"] = {
        ["Gelée pourrissante"] = 6907,
        ["Limon forestier"] = 6907,
    },
    ["Invocation d'Hakkar"] = {
        ["Ombre d'Hakkar"] = 12639,
    },
    ["Pétrifier"] = {
        ["Pétrificateur Peau-de-verre"] = 11020,
    },
    ["Ench. de bouclier (Esprit supérieur)"] = {
        Professions = 13905,
    },
    ["Ench. de gants (Minage)"] = {
        Professions = 13612,
    },
    ["Baguette mystique inférieure"] = {
        Professions = 14809,
    },
    ["Masque blanc de bandit"] = {
        Professions = 12059,
    },
    ["Potion de résistance à la magie mineure"] = {
        Professions = 3172,
    },
    ["Chemise bleue d'apparat"] = {
        Professions = 7892,
    },
    ["Imposition des mains"] = {
        ["PALADIN"] = 10310,
        ["Commandant écarlate Mograine"] = 9257,
    },
    ["Huile de pierre-écaille"] = {
        Professions = 17551,
    },
    ["Epaulières sylvestres"] = {
        Professions = 28482,
    },
    ["Ench. de plastron (Mana)"] = {
        Professions = 13607,
    },
    ["Bottes mates"] = {
        Professions = 9207,
    },
    ["Ench. de bracelets (Intelligence)"] = {
        Professions = 13822,
    },
    ["Gants en lin épais"] = {
        Professions = 3840,
    },
    ["Symbole de divinité"] = {
        ["PALADIN"] = 17033,
    },
    ["Résurrection"] = {
        ["PRIEST"] = 20770,
    },
    ["Gilet azur en soie"] = {
        Professions = 3859,
    },
    ["Domination mentale"] = {
        ["Docteur Weavil"] = 25772,
    },
    ["Poison virulent"] = {
        ["Ravageur des tombes"] = 12251,
    },
    ["Invocation d’un faux PNJ"] = {
        ["Sorcier-voleur défias"] = 3361,
    },
    ["Potion de mana excellente"] = {
        Professions = 17553,
    },
    ["Ench. de bottes (Endurance inférieure)"] = {
        Professions = 13644,
    },
    ["Coiffe de contrôle mental gnome"] = {
        Professions = 12907,
    },
    ["Cannibalisme"] = {
        Racial = 20577,
    },
    ["Robe en tisse-ombre"] = {
        Professions = 12055,
    },
    ["Malédiction de témérité"] = {
        ["WARLOCK"] = 11717,
        ["Tueuse Rougefurie"] = 16231,
    },
    ["Huile de bouche-noire"] = {
        Professions = 7836,
    },
    ["Rôder"] = {
        ["DRUID"] = 9913,
        Pet = 24453,
    },
    ["Don du Xavian"] = {
        ["Voleur de Xavian"] = 6925,
        ["Traître de Xavian"] = 6925,
        ["Lige de la corruption de Xavian"] = 6925,
        ["Implorateur de l’enfer de Xavian"] = 6925,
        ["Prince Raze"] = 6925,
    },
    ["Gants azur en soie"] = {
        Professions = 3854,
    },
    ["Uppercut"] = {
        ["Chok'sul"] = 18072,
        ["Taragaman l'Affameur"] = 18072,
        ["Ravageur de guerre"] = 10966,
        ["Arnak Grimtotem"] = 10966,
    },
    ["Chemise verte de fête"] = {
        Professions = 21945,
    },
    ["Cris du passé"] = {
        ["Gardien gémissant"] = 7074,
        ["Fantôme de colère"] = 7074,
    },
    ["Gants d'homme des collines en cuir"] = {
        Professions = 3764,
    },
    ["Armure en cuir mat"] = {
        Professions = 9196,
    },
    ["Epaulières cramoisies en soie"] = {
        Professions = 8793,
    },
    ["Alliés de VanCleef"] = {
        ["Canaille défias"] = 5200,
        ["Edwin VanCleef"] = 5200,
    },
    ["Poisons"] = {
        ["ROGUE"] = 2842,
    },
    ["Invocation du garde d’Eliza"] = {
        ["Garde d’Eliza"] = 3107,
        ["Eliza"] = 3107,
    },
    ["Recombobulateur majeur"] = {
        Professions = 23079,
    },
    ["Création de Pierre de soins (mineure)"] = {
        ["WARLOCK"] = 6201,
    },
    ["Salopette bleue"] = {
        Professions = 7639,
    },
    ["Rage"] = {
        ["Gnoll Sombrepoil enragé"] = 3150,
        ["Loup redoutable enragé"] = 3150,
        ["Grumeux"] = 3150,
        ["Ours des chardons enragé"] = 3150,
        ["Coyote des rochers enragé"] = 3150,
        ["Pattes-d'os enragée"] = 3150,
        ["Longues-dents enragé"] = 3150,
        ["Brûlepatte enragée"] = 3150,
        ["Croc acéré enragé"] = 3150,
    },
    ["Eau de feu des Tombe-hiver"] = {
        ["Protecteur Tombe-hiver"] = 17205,
        ["Totémique Tombe-hiver"] = 17205,
        ["Guide Tombe-hiver"] = 17205,
    },
    ["Invocation d'élémentaires d'eau"] = {
        ["Elémentaire d'eau invoqué"] = 20681,
        ["Dame Jaina Proudmoore"] = 20681,
    },
    ["Malédiction des épines"] = {
        ["Mage de l'ombre du Syndicat"] = 6909,
        ["Ancien fou"] = 6909,
        ["Ancien flétri"] = 6909,
        ["Ancien vengeur"] = 6909,
        ["Augure de la Lame ardente"] = 6909,
    },
    ["Silence"] = {
        ["PRIEST"] = 15487,
        ["Arcaniste Doan"] = 8988,
        ["Chasseur des ombres Witherbark"] = 6726,
    },
    ["Potion de mana mineure"] = {
        Professions = 2331,
    },
    ["Armure verte en soie"] = {
        Professions = 8784,
    },
    ["Flacon de sagesse distillée"] = {
        Professions = 17636,
    },
    ["Ench. d'arme 2M (Impact)"] = {
        Professions = 13695,
    },
    ["Toucher de Ravenclaw"] = {
        ["Main de Ravenclaw"] = 3263,
    },
    ["Epaulières d'azur"] = {
        Professions = 8795,
    },
    ["Veste de smoking"] = {
        Professions = 12093,
    },
    ["Elixir des géants"] = {
        Professions = 11472,
    },
    ["Piqué"] = {
        ["Chauve-souris vile"] = 7145,
    },
    ["Robe blanche de mariée"] = {
        Professions = 12091,
    },
    ["Armure de givre"] = {
        ["MAGE"] = 7301,
    },
    ["Volée de Boules de feu"] = {
        ["Gryphon du Nid-de-l'Aigle"] = 15285,
        ["Chevaucheur de guerre"] = 15285,
        ["Gardefeu du crépuscule"] = 15243,
    },
    ["Points d'honneur +138"] = {
        ["Porteguerre de la Horde"] = 24962,
        ["Général de brigade de l'Alliance"] = 24962,
    },
    ["Bracelets en cuivre"] = {
        Professions = 2663,
    },
    ["Hurlement"] = {
        Pet = 24579,
    },
    ["Résistance aux Arcanes"] = {
        ["HUNTER"] = 24510,
        ["Médecin écarlate"] = 17175,
    },
    ["Pistage des géants"] = {
        ["HUNTER"] = 19882,
    },
    ["Larme de Razelikh I"] = {
        ["Razelikh le Souilleur"] = 10864,
    },
    ["Cuirasse en écailles de dragon"] = {
        Professions = 10650,
    },
    ["Gants en laine épaisse"] = {
        Professions = 3843,
    },
    ["Trépanation"] = {
        ["Guerrier Skullsplitter"] = 3148,
        ["Lanceur de haches Skullsplitter"] = 3148,
        ["Brise-crâne Mâcheroc"] = 3148,
        ["Devlin Agamand"] = 3148,
        ["Briseur d’os des Gravières"] = 3148,
        ["Apprenti forgeron d'Hillsbrad"] = 3148,
        ["Massacreur Cassecrête"] = 9791,
        ["Brise-crâne de Gouffrefer"] = 16172,
    },
    ["Pantalon tisse-givre"] = {
        Professions = 18424,
    },
    ["Diadème en étoffe lunaire"] = {
        Professions = 18452,
    },
    ["Chapelet de comètes vertes"] = {
        Professions = 26424,
    },
    ["Peste fiévreuse"] = {
        ["Atal'ai momifié"] = 16186,
    },
    ["Barrage pyroclaste"] = {
        ["Elémentaire de lave"] = 19641,
    },
    ["Vengeance"] = {
        ["WARRIOR"] = 25288,
        ["Garde de Stormwind"] = 12170,
        ["Dal Griffesang"] = 12170,
        ["Vengeur Crin-de-Chardon"] = 8602,
        ["Défenseur Stormpike"] = 19130,
        ["Gardien Frostwolf"] = 19130,
        ["Défenseur aguerri"] = 19130,
        ["Gardien aguerri"] = 19130,
        ["Défenseur vétéran"] = 19130,
        ["Gardien vétéran"] = 19130,
        ["Gardien champion"] = 19130,
        ["Défenseur champion"] = 19130,
        ["Caporal Noreg Stormpike"] = 19130,
    },
    ["Elixir d'Agilité mineure"] = {
        Professions = 3230,
    },
    ["Totem de Séisme"] = {
        ["SHAMAN"] = 8143,
    },
    ["Totem de Poignée de terre"] = {
        ["Totem de Poignée de terre"] = 8376,
        ["Sorcier-docteur Bloodscalp"] = 8376,
        ["Totémique de Tranchebauge"] = 8376,
        ["Chardonnier taille-racine"] = 8376,
    },
    ["Malédiction de vengeance"] = {
        ["Arikara"] = 17213,
    },
    ["Retour de flammes"] = {
        ["Sentinelle du Syndicat"] = 3602,
        ["Geôlier Eston"] = 3602,
        ["Excavateur de la Rive noire"] = 3602,
    },
    ["Châtiment"] = {
        ["PRIEST"] = 10934,
    },
    ["Coup puissant"] = {
        ["Diablosaure"] = 14099,
        ["Monstruosité de la peste"] = 14099,
        ["Brise-crâne de Gouffrefer"] = 14099,
    },
    ["Bénédiction des rois supérieure"] = {
        ["PALADIN"] = 25898,
    },
    ["Dague en cuivre"] = {
        Professions = 8880,
    },
    ["Potion de mana majeure"] = {
        Professions = 17580,
    },
    ["Invocation de l'esprit du sanglier"] = {
        ["Esprit du Sanglier"] = 8286,
        ["Aggem Mantépine"] = 8286,
    },
    ["Cape en étoffe cendrée"] = {
        Professions = 18418,
    },
    ["Potion de Grande rage"] = {
        Professions = 6618,
    },
    ["Apparition de drakônide rouge"] = {
        ["Drakônide rouge"] = 22655,
        ["Red Drakonid Spawner"] = 22655,
    },
    ["Tube en thorium"] = {
        Professions = 19795,
    },
    ["Tourbillon"] = {
        ["WARRIOR"] = 1680,
        ["Herod"] = 8989,
        ["Thelman Slatefist"] = 13736,
        ["Drek'Thar"] = 13736,
        ["Capitaine Galvangar"] = 13736,
        ["Maréchal de Glace-sang"] = 13736,
        ["Maréchal de la Tour de la halte"] = 13736,
        ["Maréchal Frostwolf est"] = 13736,
        ["Maréchal Frostwolf ouest"] = 13736,
        ["Maître de guerre de Dun Baldar nord"] = 13736,
        ["Maître de guerre de Dun Baldar sud"] = 13736,
        ["Maître de guerre Frostwolf est"] = 13736,
        ["Maître de guerre de l'Aile de glace"] = 13736,
        ["Maître de guerre de Stonehearth"] = 13736,
        ["Maître de guerre Frostwolf ouest"] = 13736,
        ["Dame Hoteshem"] = 13736,
        ["Troupe d'élite de Kurzen"] = 17207,
        ["Massacreur du Syndicat"] = 17207,
        ["Nimar le Pourfendeur"] = 17207,
        ["Guerrier Ombreforge"] = 17207,
        ["Garde Jarad"] = 17207,
        ["Garde Kahil"] = 17207,
        ["Cyclonien"] = 17207,
        ["Keetar"] = 17207,
    },
    ["Malédiction de fatigue"] = {
        ["WARLOCK"] = 18223,
    },
    ["Poison corrosif"] = {
        ["Ancienne Rampemousse"] = 3396,
        ["Rampemousse géante"] = 3396,
        ["Rampemousse forestière"] = 3396,
        ["Bête de sève corrosive"] = 3396,
        ["Petite chimère"] = 3396,
        ["Matriarche chimère"] = 3396,
    },
    ["Gants cramoisis en soie"] = {
        Professions = 8804,
    },
    ["Robe du grand vaudou"] = {
        Professions = 10520,
    },
    ["Dissipation supérieure"] = {
        ["Grand prêtre Thel'danis"] = 24997,
    },
    ["Bouclier anti-magie"] = {
        ["Marche-lune d'Ombrecroc"] = 7121,
    },
    ["Epaulières vivantes"] = {
        Professions = 19061,
    },
    ["Martèlement violent"] = {
        ["M. Smite"] = 6432,
    },
    ["Leurre - Evénement 002"] = {
        ["Leurre de l'équipe de mortier"] = 18907,
        ["Eclaireur Klemmy"] = 18907,
    },
    ["Epaulières en étoffe lunaire"] = {
        Professions = 18448,
    },
    ["Pourpoint blanc en cuir"] = {
        Professions = 2163,
    },
    ["Agilité altérée"] = {
        ["Guide Vile-sylve"] = 6817,
        ["Chênepatte"] = 6817,
    },
    ["Transmutation de l'Air en Feu"] = {
        Professions = 17559,
    },
    ["Longue cape en soie"] = {
        Professions = 3861,
    },
    ["Colère divine"] = {
        ["PALADIN"] = 10318,
    },
    ["Croissance"] = {
        ["Bom'bay"] = 16711,
    },
    ["Eclairs bondissants"] = {
        ["Légion-tempête maraudine"] = 9654,
    },
    ["Appel d'une Horreur Lupine"] = {
        ["Horreur Lupine"] = 7489,
    },
    ["Essaim de silithides"] = {
        ["Essaim silithide"] = 6589,
        ["Grouillant silithide"] = 6589,
        ["Traînard Hazzali"] = 6589,
        ["Traînard Centipaar"] = 6589,
        ["Essaim silithide"] = 10722,
        ["Grouillant silithide"] = 10722,
        ["Traînard Hazzali"] = 10722,
        ["Traînard Centipaar"] = 10722,
    },
    ["Invocation d'une explosion de lave F"] = {
        ["Rat du Magma"] = 21904,
    },
    ["Chaperon en gangrétoffe"] = {
        Professions = 18442,
    },
    ["Traumatisme de l'aiguillon"] = {
        ["Artorius l'Aimable"] = 23299,
    },
    ["Ench. de cape (Résistance supérieure)"] = {
        Professions = 20014,
    },
    ["Balise de la faille"] = {
        ["Fouille-rivage Perdu"] = 9614,
    },
    ["Sarments"] = {
        ["DRUID"] = 9853,
        ["Ancien Rampant lacustre"] = 11922,
        ["Tisseur d'épines de Tranchebauge"] = 11922,
        ["Protecteur errant"] = 11922,
        ["Archidruide Renferal"] = 22127,
        ["Druide du Bosquet"] = 22127,
        ["Tisseur d’épines Dos-hirsute"] = 12747,
        ["Forme-bois Sombretaillis"] = 12747,
        ["Ancien calciné"] = 12747,
        ["Gardien Ordanus"] = 12747,
        ["Seigneur du goudron"] = 12747,
    },
    ["Fusil plaqué argent"] = {
        Professions = 3949,
    },
    ["Bourse enchantée en tisse-mage"] = {
        Professions = 27658,
    },
    ["Bénédiction de salut"] = {
        ["PALADIN"] = 1038,
    },
    ["Ceinture en lin"] = {
        Professions = 8776,
    },
    ["Ench. de bouclier (Blocage inférieur)"] = {
        Professions = 13689,
    },
    ["Essaim des moissons"] = {
        ["Moissonneur silithide"] = 7277,
    },
    ["Epaulières à double couture en laine"] = {
        Professions = 3848,
    },
    ["Bannir"] = {
        ["WARLOCK"] = 18647,
    },
    ["Coup de tête"] = {
        ["Lardeur"] = 6730,
        ["Orageux"] = 6730,
        ["Mordeuse nacrétoile"] = 6730,
    },
    ["Lame fantôme"] = {
        Professions = 10007,
    },
    ["Pantalon en tisse-fantôme"] = {
        Professions = 18441,
    },
    ["Gants glaciaires"] = {
        Professions = 28205,
    },
    ["Ench. d'arme (Frappe supérieure)"] = {
        Professions = 13943,
    },
    ["Caisse en mithril"] = {
        Professions = 12599,
    },
    ["Chemise marron en lin"] = {
        Professions = 3915,
    },
    ["Cape en lin"] = {
        Professions = 2387,
    },
    ["Boire une potion mineure"] = {
        ["Fourrageur murloc"] = 3368,
        ["Herboriste Rivepatte"] = 3368,
    },
    ["Déverrouillage d’Ashcrombe"] = {
        ["Ensorceleur Ashcrombe"] = 6421,
    },
    ["Passe-partout en vrai-argent"] = {
        Professions = 19668,
    },
    ["Gants armés du scorpide"] = {
        Professions = 10542,
    },
    ["Invocation d'une flamme vive"] = {
        ["Flamme vive"] = 5110,
        ["Magicien défias"] = 5110,
    },
    ["Fusée bleue"] = {
        Professions = 23067,
    },
    ["Transir"] = {
        ["Blizzard"] = 28547,
    },
    ["Mortier"] = {
        ["Mortier"] = 25003,
        ["Chariotte à vapeur"] = 25003,
    },
    ["Malédiction de la Banshee"] = {
        ["Bien-née maudite"] = 5884,
        ["Bien-né frémissant"] = 5884,
        ["Bien-née gémissante"] = 5884,
        ["Anaya Dawnrunner"] = 5884,
    },
    ["Système d'occultation gnome"] = {
        Professions = 3971,
    },
    ["Gants Coeur-de-braise"] = {
        Professions = 20849,
    },
    ["Modulateur d'amplification vocale"] = {
        Professions = 19819,
    },
    ["Guérison du poison"] = {
        ["DRUID"] = 8946,
        ["SHAMAN"] = 526,
    },
    ["Ench. de bottes (Vitesse mineure)"] = {
        Professions = 13890,
    },
    ["Faveur divine"] = {
        ["PALADIN"] = 20216,
    },
    ["Geyser massif"] = {
        ["Geyser massif"] = 22421,
    },
    ["Pantalon du phénix"] = {
        Professions = 3851,
    },
    ["Marche sur l’eau"] = {
        ["SHAMAN"] = 546,
    },
    ["Fonte de minerai"] = {
        ["Artisan gobelin"] = 5159,
    },
    ["Comète, BLEUE"] = {
        ["Type des feux d'artifice de Pat - BLEU"] = 26344,
    },
}
