## Interface: 11506
## Title: SpellTimelines
## Notes: Shows casts performed by player and other targets in a timeline fashion
## Version: 0.1
## Author: WobLight
## SavedVariables: SpellTimelinesSettings
## SavedVariablesPerCharacter: SpellTimelinesProfile
## Dependencies: EmeraldFramework
SpellTimelines.lua
Timeline.lua
Options.lua
ClassicData.lua
ClassicData_ruRU.lua
ClassicData_frFR.lua
ClassicData_esES.lua
ClassicData_deDE.lua
ClassicData_koKR.lua
ClassicData_zhCN.lua
