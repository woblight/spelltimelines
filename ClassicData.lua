if GetLocale() ~= "enUS" then return end
ClassicData = {
    version = 4,
    ["Portal: Thunder Bluff"] = {
        ["MAGE"] = 11420,
    },
    ["Demonic Doom"] = {
        ["Artorius the Amiable"] = 23298,
    },
    ["Ambush"] = {
        ["ROGUE"] = 11269,
    },
    ["Hillman's Belt"] = {
        Professions = 3767,
    },
    ["Arcane Missiles"] = {
        ["MAGE"] = 25345,
    },
    ["Fade"] = {
        ["PRIEST"] = 10942,
        ["Lady Sylvanas Windrunner"] = 20672,
    },
    ["Surrender"] = {
        ["Hulfnar Stonetotem"] = 17648,
    },
    ["Elixir of Firepower"] = {
        Professions = 7845,
    },
    ["Flametongue Weapon"] = {
        ["SHAMAN"] = 16342,
    },
    ["Soul Link"] = {
        ["WARLOCK"] = 19028,
    },
    ["Portal: Undercity"] = {
        ["MAGE"] = 11418,
    },
    ["Nature's Grasp"] = {
        ["DRUID"] = 17329,
    },
    ["Summon Water Elementals"] = {
        ["Summoned Water Elemental"] = 20681,
        ["Lady Jaina Proudmoore"] = 20681,
    },
    ["Silk Headband"] = {
        Professions = 8762,
    },
    ["Thick Armor Kit"] = {
        Professions = 10487,
    },
    ["Track Beasts"] = {
        ["HUNTER"] = 1494,
    },
    ["Crystalline Slumber"] = {
        ["Cold Eye Basilisk"] = 3636,
        ["Scorched Basilisk"] = 3636,
        ["Singed Basilisk"] = 3636,
        ["Blackened Basilisk"] = 3636,
        ["Saltstone Basilisk"] = 3636,
        ["Gritjaw Basilisk"] = 3636,
        ["Hulking Gritjaw Basilisk"] = 3636,
        ["Jadespine Basilisk"] = 3636,
    },
    ["Divine Favor"] = {
        ["PALADIN"] = 20216,
    },
    ["Frostweave Pants"] = {
        Professions = 18424,
    },
    ["Elixir of Greater Defense"] = {
        Professions = 11450,
    },
    ["Hillman's Shoulders"] = {
        Professions = 3768,
    },
    ["Sayge's Dark Fortune of Stamina"] = {
        ["Sayge"] = 23737,
    },
    ["Summon Felsteed"] = {
        ["WARLOCK"] = 5784,
    },
    ["Sayge's Dark Fortune of Agility"] = {
        ["Sayge"] = 23736,
    },
    ["Transmute: Elemental Fire"] = {
        Professions = 25146,
    },
    ["Bronze Greatsword"] = {
        Professions = 9986,
    },
    ["Ornate Mithril Pants"] = {
        Professions = 9945,
    },
    ["Lash of Pain"] = {
        Pet = 11780,
    },
    ["Gloves of the Greatfather"] = {
        Professions = 21943,
    },
    ["Goblin Rocket Boots"] = {
        Professions = 8895,
    },
    ["Call Bloodshot"] = {
        ["Bloodshot"] = 18262,
        ["Huntsman Radley"] = 18262,
    },
    ["Polished Steel Boots"] = {
        Professions = 3513,
    },
    ["Minor Recombobulator"] = {
        Professions = 3952,
    },
    ["Cannibalize"] = {
        Racial = 20577,
    },
    ["Quick Bloodlust"] = {
        ["Riverpaw Taskmaster"] = 3229,
        ["Mo'grosh Shaman"] = 3229,
        ["Grawmug"] = 3229,
    },
    ["Turtle Scale Gloves"] = {
        Professions = 10509,
    },
    ["Compact Harvest Reaper Kit"] = {
        Professions = 3963,
    },
    ["Handful of Copper Bolts"] = {
        Professions = 3922,
    },
    ["Wilt"] = {
        ["Emissary Roman'khan"] = 23772,
    },
    ["Blue Firework Cluster"] = {
        ["Pat's Firework Cluster Guy (BLUE)"] = 26304,
        ["Lunar Festival Herald"] = 26304,
        ["Lunar Festival Emissary"] = 26304,
    },
    ["Phase Out"] = {
        ["Gazban"] = 3648,
    },
    ["Guardian Armor"] = {
        Professions = 3773,
    },
    ["Red Firework"] = {
        Professions = 23066,
        ["Wrenix's Gizmotronic Apparatus"] = 6668,
    },
    ["Shadow Protection Potion"] = {
        Professions = 7256,
    },
    ["Black Swashbuckler's Shirt"] = {
        Professions = 3873,
    },
    ["Chromatic Chaos"] = {
        ["Lord Victor Nefarius"] = 16337,
    },
    ["Elixir of Detect Lesser Invisibility"] = {
        Professions = 3453,
    },
    ["Mithril Heavy-bore Rifle"] = {
        Professions = 12614,
    },
    ["Fool's Plight"] = {
        ["Simone the Inconspicuous"] = 23504,
        ["Franklin the Friendly"] = 23504,
        ["Artorius the Amiable"] = 23504,
        ["Nelson the Nice"] = 23504,
    },
    ["Silvered Bronze Gauntlets"] = {
        Professions = 3333,
    },
    ["Elixir of Detect Undead"] = {
        Professions = 11460,
    },
    ["Create Dawn's Gambit"] = {
        ["Betina Bigglezink"] = 18367,
    },
    ["Goblin Jumper Cables XL"] = {
        Professions = 23078,
    },
    ["Desperate Prayer"] = {
        ["PRIEST"] = 19243,
    },
    ["Mithril Scale Bracers"] = {
        Professions = 9937,
    },
    ["Felcloth Robe"] = {
        Professions = 18451,
    },
    ["Dredge Sickness"] = {
        ["Dredge Worm"] = 14535,
    },
    ["Enchant Cloak - Resistance"] = {
        Professions = 13794,
    },
    ["Tainted Blood"] = {
        Pet = 19660,
    },
    ["Mana Potion"] = {
        Professions = 3452,
    },
    ["Fiend Fury"] = {
        ["Skeletal Fiend"] = 3416,
    },
    ["Enchant Shield - Minor Stamina"] = {
        Professions = 13378,
    },
    ["Arcane Power"] = {
        ["MAGE"] = 12042,
    },
    ["Acid Slime"] = {
        ["Glutinous Ooze"] = 14147,
    },
    ["Hi-Explosive Bomb"] = {
        Professions = 12619,
    },
    ["War Stomp"] = {
        Racial = 20549,
        ["Wandering Protector"] = 45,
        ["Harb Foulmountain"] = 45,
        ["Cliff Walker"] = 11876,
    },
    ["Fire Goggles"] = {
        Professions = 12594,
    },
    ["Brightcloth Pants"] = {
        Professions = 18439,
    },
    ["Copper Axe"] = {
        Professions = 2738,
    },
    ["Rugged Leather"] = {
        Professions = 22331,
    },
    ["Murloc Scale Bracers"] = {
        Professions = 6705,
    },
    ["Amplify Flames"] = {
        ["Shadowforge Darkweaver"] = 9482,
    },
    ["Cure Poison"] = {
        ["DRUID"] = 8946,
        ["SHAMAN"] = 526,
    },
    ["Black Mageweave Headband"] = {
        Professions = 12072,
    },
    ["Torment"] = {
        Pet = 11775,
    },
    ["Gnarlpine Vengeance"] = {
        ["Gnarlpine Augur"] = 5628,
        ["Gnarlpine Pathfinder"] = 5628,
        ["Gnarlpine Avenger"] = 5628,
        ["Gnarlpine Totemic"] = 5628,
    },
    ["Crystallize"] = {
        ["Spire Spider"] = 16104,
    },
    ["Conjure Mana Jade"] = {
        ["MAGE"] = 3552,
    },
    ["Shadow Protection"] = {
        ["PRIEST"] = 10958,
    },
    ["Judgement"] = {
        ["PALADIN"] = 20271,
    },
    ["Hands of Darkness"] = {
        Professions = 8780,
    },
    ["Contagion of Rot"] = {
        ["Fetid Corpse"] = 7102,
        ["Forsaken Herbalist"] = 7102,
    },
    ["Enchant Cloak - Superior Defense"] = {
        Professions = 20015,
    },
    ["Lightning Barrier"] = {
        ["Sister Hatelash"] = 6960,
    },
    ["Poison Mind"] = {
        ["Hive'Regal Slavemaker"] = 19469,
    },
    ["Light Leather Quiver"] = {
        Professions = 9060,
    },
    ["Heavy Weightstone"] = {
        Professions = 3117,
    },
    ["Corrupted Intellect"] = {
        ["Corrupted Mottled Boar"] = 6818,
        ["Foulweald Shaman"] = 6818,
        ["Foulweald Totemic"] = 6818,
    },
    ["Amnennar's Wrath"] = {
        ["Amnennar the Coldbringer"] = 13009,
    },
    ["Lizard Bolt"] = {
        ["Thunder Lizard"] = 5401,
        ["Lightning Hide"] = 5401,
        ["Stormhide"] = 5401,
        ["Thunderhead"] = 5401,
        ["Stormsnout"] = 5401,
        ["Cliff Stormer"] = 5401,
        ["Raging Thunder Lizard"] = 5401,
        ["Elder Thunder Lizard"] = 5401,
    },
    ["Greater Mystic Wand"] = {
        Professions = 14810,
    },
    ["Berserker Stance"] = {
        ["WARRIOR"] = 2458,
        ["Znort"] = 7366,
        ["Magram Marauder"] = 7366,
        ["Gelkis Marauder"] = 7366,
        ["Caverndeep Reaver"] = 7366,
        ["Fallen Champion"] = 7366,
    },
    ["Sapper Explode"] = {
        ["Dark Iron Saboteur"] = 3204,
        ["Dark Iron Sapper"] = 3204,
    },
    ["Wisdom of the Timbermaw"] = {
        Professions = 23662,
    },
    ["Proudmoore's Defense"] = {
        ["Archmage Tervosh"] = 7120,
    },
    ["Strength of Earth Totem"] = {
        ["SHAMAN"] = 25361,
    },
    ["Earthen Leather Shoulders"] = {
        Professions = 9147,
    },
    ["Explosive Trap"] = {
        ["HUNTER"] = 14317,
    },
    ["Runic Leather Bracers"] = {
        Professions = 19065,
    },
    ["Enchant Bracer - Minor Stamina"] = {
        Professions = 7457,
    },
    ["Heavy Timbermaw Belt"] = {
        Professions = 23628,
    },
    ["Mind Shatter"] = {
        ["Doctor Weavil"] = 25774,
    },
    ["Attunement to the Core"] = {
        ["Lothos Riftwaker"] = 22877,
    },
    ["Runed Copper Pants"] = {
        Professions = 3324,
    },
    ["Felcloth Gloves"] = {
        Professions = 22867,
    },
    ["Blazing Rapier"] = {
        Professions = 16978,
    },
    ["Copper Rod"] = {
        Professions = 6217,
    },
    ["Dark Leather Cloak"] = {
        Professions = 2168,
    },
    ["Wildvine Potion"] = {
        Professions = 11458,
    },
    ["Target Dummy Spawn Effect"] = {
        ["Target Dummy"] = 4507,
        ["Advanced Target Dummy"] = 4507,
        ["Masterwork Target Dummy"] = 4507,
        ["Love Fool"] = 4507,
    },
    ["Crimson Silk Pantaloons"] = {
        Professions = 8799,
    },
    ["Kirtonos Transform"] = {
        ["Kirtonos the Herald"] = 16467,
    },
    ["Frostsaber Boots"] = {
        Professions = 19066,
    },
    ["Chain Lightning"] = {
        ["SHAMAN"] = 10605,
        ["Thunderhead Consort"] = 12058,
        ["Thrall"] = 16033,
        ["Hakkari Oracle"] = 16006,
        ["Primalist Thurloga"] = 16006,
    },
    ["Summon Chicken Egg"] = {
        ["Chicken"] = 13563,
    },
    ["Soul Flame"] = {
        ["Nelson the Nice"] = 23272,
    },
    ["Rugged Leather Pants"] = {
        Professions = 9064,
    },
    ["Nightscape Headband"] = {
        Professions = 10507,
    },
    ["Nagmara's Love Potion"] = {
        ["Mistress Nagmara"] = 14928,
    },
    ["Limited Invulnerability Potion"] = {
        Professions = 3175,
    },
    ["Infected Spine"] = {
        ["Withered Spearhide"] = 12245,
    },
    ["Elune's Grace"] = {
        ["PRIEST"] = 19293,
    },
    ["Shadow Crescent Axe"] = {
        Professions = 3500,
    },
    ["Moss Hide"] = {
        ["Mosshide Gnoll"] = 3288,
        ["Mosshide Mongrel"] = 3288,
        ["Mosshide Mistweaver"] = 3288,
        ["Mosshide Fenrunner"] = 3288,
        ["Mosshide Trapper"] = 3288,
        ["Mosshide Mystic"] = 3288,
        ["Mosshide Alpha"] = 3288,
    },
    ["Curse of the Bleakheart"] = {
        ["Bleakheart Satyr"] = 6946,
        ["Bleakheart Trickster"] = 6946,
        ["Bleakheart Shadowstalker"] = 6946,
        ["Bleakheart Hellcaller"] = 6946,
    },
    ["Summon Lava Spawn"] = {
        ["Lava Spawn"] = 19392,
        ["Firelord"] = 19392,
    },
    ["Unstable Concoction"] = {
        ["Gurubashi Bat Rider"] = 24024,
    },
    ["Reinforced Linen Cape"] = {
        Professions = 2397,
    },
    ["Recklessness"] = {
        ["WARRIOR"] = 1719,
    },
    ["Death Coil"] = {
        ["WARLOCK"] = 17926,
    },
    ["Serpent Form"] = {
        ["Boahn"] = 8041,
        ["Druid of the Fang"] = 8041,
    },
    ["Miring Mud"] = {
        ["Timberling Mire Beast"] = 5567,
    },
    ["Advanced Target Dummy"] = {
        Professions = 3965,
    },
    ["Lag"] = {
        ["Techbot"] = 10855,
    },
    ["Mongoose Bite"] = {
        ["HUNTER"] = 14271,
    },
    ["Backhand"] = {
        ["Kazon"] = 6253,
        ["Brawler"] = 6253,
        ["Defias Convict"] = 6253,
        ["Crushridge Mauler"] = 6253,
        ["Otto"] = 6253,
        ["Southsea Brigand"] = 6253,
        ["Prospector Khazgorm"] = 6253,
        ["Forsaken Thug"] = 6253,
        ["Tazan"] = 6253,
        ["Coldmine Guard"] = 6253,
        ["Veteran Coldmine Guard"] = 6253,
    },
    ["Rage Potion"] = {
        Professions = 6617,
    },
    ["Create Spellstone"] = {
        ["WARLOCK"] = 2362,
    },
    ["Green Lens"] = {
        Professions = 12622,
    },
    ["Ghostweave Belt"] = {
        Professions = 18410,
    },
    ["Mind Soothe"] = {
        ["PRIEST"] = 10953,
    },
    ["Strike"] = {
        ["Skeletal Miner"] = 12057,
        ["Strashaz Serpent Guard"] = 12057,
        ["Lord Sinslayer"] = 12057,
        ["Whitewhisker Digger"] = 12057,
        ["Umi Thorson"] = 12057,
        ["Coldmine Invader"] = 12057,
        ["Irondeep Trogg"] = 14516,
        ["Royal Dreadguard"] = 14516,
        ["Old Vicejaw"] = 13446,
        ["Coldmine Guard"] = 15580,
        ["Wing Commander Guse"] = 15580,
        ["Wing Commander Jeztor"] = 15580,
        ["Wing Commander Mulverick"] = 15580,
        ["Wing Commander Ichman"] = 15580,
        ["Wing Commander Slidore"] = 15580,
        ["Veteran Coldmine Guard"] = 15580,
        ["Drunken Footpad"] = 13584,
        ["Brack"] = 11976,
        ["Stonesplinter Bonesnapper"] = 11976,
        ["Dextren Ward"] = 11976,
        ["Captain Melrache"] = 11976,
        ["Farmer Solliden"] = 11976,
        ["Gnarlpine Warrior"] = 11976,
        ["Crushridge Mauler"] = 11976,
        ["Stonevault Bonesnapper"] = 11976,
        ["Kolkar Marauder"] = 11976,
        ["Wrathtail Myrmidon"] = 11976,
        ["Gravelsnout Kobold"] = 11976,
        ["Guard Edward"] = 11976,
        ["Guard Jarad"] = 11976,
        ["Guard Kahil"] = 11976,
        ["Guard Lana"] = 11976,
        ["Guard Narrisha"] = 11976,
        ["Guard Tark"] = 11976,
        ["Nethergarde Officer"] = 11976,
        ["Storm Bay Warrior"] = 11976,
        ["Holdout Warrior"] = 11976,
        ["Flagglemurk the Cruel"] = 11976,
        ["Warlord Krom'zar"] = 11976,
        ["Kolkar Invader"] = 11976,
        ["Grimtotem Reaver"] = 11976,
        ["Spectral Defender"] = 11976,
        ["Ragefire Trogg"] = 11976,
        ["Stormpike Guardsman"] = 11976,
        ["Mountaineer Boombellow"] = 11976,
        ["Threggil"] = 11976,
        ["Somnus"] = 18368,
    },
    ["Thorium Helm"] = {
        Professions = 16653,
    },
    ["Large Blue Rocket"] = {
        Professions = 26420,
    },
    ["Wisdom of Agamaggan"] = {
        ["Mangletooth"] = 7764,
    },
    ["Fling Torch"] = {
        ["Kolkar Stormseer"] = 14292,
        ["Kolkar Invader"] = 14292,
    },
    ["Blue Linen Shirt"] = {
        Professions = 2394,
    },
    ["Magic Wings"] = {
        ["Faire Cannon Trigger"] = 24742,
    },
    ["Bloodsail Companion"] = {
        ["Defias Companion"] = 5172,
        ["Defias Pirate"] = 5172,
    },
    ["Seduction"] = {
        Pet = 6358,
    },
    ["Thorium Boots"] = {
        Professions = 16652,
    },
    ["Diseased Slime"] = {
        ["Rotting Slime"] = 6907,
        ["Forest Ooze"] = 6907,
    },
    ["Spirit Steal"] = {
        ["Nightlash"] = 3477,
    },
    ["Enchanted Thorium"] = {
        Professions = 17180,
    },
    ["Eye Beam"] = {
        ["Eye of C'Thun"] = 26134,
    },
    ["Irradiated"] = {
        ["Irradiated Invader"] = 9775,
        ["Irradiated Pillager"] = 9775,
    },
    ["Robe of the Void"] = {
        Professions = 18458,
    },
    ["Elixir of Ogre's Strength"] = {
        Professions = 3188,
    },
    ["Linen Bag"] = {
        Professions = 3755,
    },
    ["Divine Protection"] = {
        ["PALADIN"] = 5573,
    },
    ["Greater Arcane Elixir"] = {
        Professions = 17573,
    },
    ["Call Lupine Horror"] = {
        ["Lupine Horror"] = 7489,
    },
    ["Radiation Bolt"] = {
        ["Irradiated Invader"] = 9771,
        ["Irradiated Pillager"] = 9771,
    },
    ["Green Linen Bracers"] = {
        Professions = 3841,
    },
    ["Felcloth Boots"] = {
        Professions = 18437,
    },
    ["Solid Grinding Stone"] = {
        Professions = 9920,
    },
    ["Pounce"] = {
        ["DRUID"] = 9827,
    },
    ["Reinforced Woolen Shoulders"] = {
        Professions = 3849,
    },
    ["Sneak"] = {
        ["Defias Night Runner"] = 22766,
        ["Shadowhide Assassin"] = 22766,
        ["Shadowmaw Panther"] = 22766,
        ["Kurzen Commando"] = 22766,
        ["Fen Creeper"] = 22766,
        ["Elder Shadowmaw Panther"] = 22766,
        ["Shadowclaw"] = 22766,
        ["Syndicate Spy"] = 22766,
        ["Syndicate Assassin"] = 22766,
        ["Syndicate Highwayman"] = 22766,
        ["Syndicate Prowler"] = 22766,
        ["Ridge Stalker"] = 22766,
        ["Silvermane Stalker"] = 22766,
        ["Witchwing Ambusher"] = 22766,
        ["Razormane Stalker"] = 22766,
        ["Deviate Stalker"] = 22766,
        ["Felmusk Shadowstalker"] = 22766,
        ["Bleakheart Shadowstalker"] = 22766,
        ["Crag Stalker"] = 22766,
        ["Darkmist Lurker"] = 22766,
        ["Darkfang Lurker"] = 22766,
        ["Fallenroot Rogue"] = 22766,
        ["Fallenroot Shadowstalker"] = 22766,
        ["Theramore Infiltrator"] = 22766,
        ["Razorfen Stalker"] = 22766,
    },
    ["Drain Mana"] = {
        ["WARLOCK"] = 11704,
    },
    ["Rough Dynamite"] = {
        Professions = 3919,
    },
    ["Fall down"] = {
        ["Hakkari Shadowcaster"] = 6869,
    },
    ["Rushing Charge"] = {
        ["Stonetusk Boar"] = 6268,
        ["Longsnout"] = 6268,
        ["Goretusk"] = 6268,
        ["Princess"] = 6268,
        ["Bellygrub"] = 6268,
        ["Porcine Entourage"] = 6268,
        ["Hogger"] = 6268,
        ["Young Goretusk"] = 6268,
        ["Rockhide Boar"] = 6268,
        ["Great Goretusk"] = 6268,
        ["Murloc Shorestriker"] = 6268,
        ["Crag Boar"] = 6268,
        ["Large Crag Boar"] = 6268,
        ["Elder Crag Boar"] = 6268,
        ["Mountain Boar"] = 6268,
        ["Mangy Mountain Boar"] = 6268,
        ["Elder Mountain Boar"] = 6268,
        ["Scarred Crag Boar"] = 6268,
        ["Stromgarde Cavalryman"] = 6268,
        ["Kodo Calf"] = 6268,
        ["Mazzranache"] = 6268,
        ["Bloodtalon Taillasher"] = 6268,
        ["Bloodtalon Scythemaw"] = 6268,
        ["Kul Tiras Sailor"] = 6268,
        ["Corrupted Bloodtalon Scythemaw"] = 6268,
        ["Greater Barrens Kodo"] = 6268,
        ["Wooly Kodo"] = 6268,
        ["Kolkar Marauder"] = 6268,
        ["Saltspittle Puddlejumper"] = 6268,
        ["Wild Buck"] = 6268,
        ["Raging Cliff Stormer"] = 6268,
        ["Antlered Courser"] = 6268,
        ["Galak Marauder"] = 6268,
        ["Agam'ar"] = 6268,
        ["Brontus"] = 6268,
        ["Ashmane Boar"] = 6268,
        ["Thunderhead Patriarch"] = 6268,
        ["Withered Warrior"] = 6268,
        ["Kolkar Invader"] = 6268,
        ["Grimtotem Brute"] = 6268,
        ["Herod"] = 8260,
        ["Agathelos the Raging"] = 8260,
        ["Grunter"] = 8260,
    },
    ["Force of Nature"] = {
        ["Force of Nature"] = 6913,
        ["Cenarion Vindicator"] = 6913,
    },
    ["Truesilver Gauntlets"] = {
        Professions = 9954,
    },
    ["Tainted Howl"] = {
        ["Nightbane Tainted One"] = 3424,
        ["Gutspill"] = 3424,
    },
    ["Storm Bolt"] = {
        ["King Magni Bronzebeard"] = 20685,
    },
    ["Deadmines Dynamite"] = {
        ["Undead Dynamiter"] = 7395,
    },
    ["Rough Copper Vest"] = {
        Professions = 12260,
    },
    ["Create Healthstone (Greater)"] = {
        ["WARLOCK"] = 11729,
    },
    ["Blessing of Kings"] = {
        ["PALADIN"] = 20217,
    },
    ["Overdrive"] = {
        ["Warsong Shredder"] = 18546,
    },
    ["Bronze Battle Axe"] = {
        Professions = 9987,
    },
    ["Growl of Fortitude"] = {
        ["Elder Ashenvale Bear"] = 4148,
        ["Giant Ashenvale Bear"] = 4148,
    },
    ["Ice Claw"] = {
        ["Bjarn"] = 3130,
        ["Ice Claw Bear"] = 3130,
    },
    ["Enchant Cloak - Lesser Shadow Resistance"] = {
        Professions = 13522,
    },
    ["Large Red Rocket"] = {
        Professions = 26422,
    },
    ["Runed Stygian Belt"] = {
        Professions = 24902,
    },
    ["Soul Rend"] = {
        ["Sorrow Wing"] = 3405,
    },
    ["Linen Cloak"] = {
        Professions = 2387,
    },
    ["Soul Pouch"] = {
        Professions = 26085,
    },
    ["Bronze Axe"] = {
        Professions = 2741,
    },
    ["Rough Grinding Stone"] = {
        Professions = 3320,
    },
    ["Lash"] = {
        ["Lashtail Raptor"] = 6607,
        ["Highland Lashtail"] = 6607,
        ["Thrashtail Basilisk"] = 6607,
        ["Strider Clutchmother"] = 6607,
        ["Sunscale Lashtail"] = 6607,
        ["Deviate Stinglash"] = 6607,
        ["Scorpid Tail Lasher"] = 6607,
        ["Bloodpetal Lasher"] = 6607,
        ["Shade of Hakkar"] = 6607,
        ["Dredge Crusher"] = 6607,
        ["Vorsha the Lasher"] = 6607,
    },
    ["Resurrect Trooper"] = {
        ["Demetria"] = 19721,
        ["Scarlet Trooper"] = 19721,
    },
    ["Dark Iron Land Mine"] = {
        ["Dark Iron Land Mine"] = 11802,
        ["Dark Iron Agent"] = 11802,
        ["Thauris Balgarr"] = 11802,
    },
    ["Slowing Ooze"] = {
        ["Primal Ooze"] = 16050,
    },
    ["Inner Focus"] = {
        ["PRIEST"] = 14751,
    },
    ["Dragonscale Breastplate"] = {
        Professions = 10650,
    },
    ["Frostsaber Leggings"] = {
        Professions = 19074,
    },
    ["Sense Undead"] = {
        ["PALADIN"] = 5502,
    },
    ["Enchant Gloves - Advanced Herbalism"] = {
        Professions = 13868,
    },
    ["Phase Shift"] = {
        Pet = 4511,
    },
    ["Sayge's Dark Fortune of Armor"] = {
        ["Sayge"] = 23767,
    },
    ["Dark Iron Pulverizer"] = {
        Professions = 15292,
    },
    ["Aspect of the Beast"] = {
        ["HUNTER"] = 13161,
    },
    ["Serpent Sting"] = {
        ["HUNTER"] = 25295,
    },
    ["Mind-numbing Poison III"] = {
        ["ROGUE"] = 11400,
    },
    ["Summon Illusionary Phantasm"] = {
        ["Illusionary Phantasm"] = 8986,
        ["Haunting Phantasm"] = 8986,
    },
    ["Spell Lock"] = {
        Pet = 19647,
    },
    ["Blessing of Salvation"] = {
        ["PALADIN"] = 1038,
    },
    ["Frost Resistance Aura"] = {
        ["PALADIN"] = 19898,
    },
    ["Glacial Vest"] = {
        Professions = 28207,
    },
    ["Big Voodoo Cloak"] = {
        Professions = 10562,
    },
    ["Enchant Cloak - Greater Defense"] = {
        Professions = 13746,
    },
    ["Lightning Shield"] = {
        ["SHAMAN"] = 10432,
        ["Arkkoran Oracle"] = 12550,
        ["Living Storm"] = 12550,
        ["Lorgus Jett"] = 12550,
        ["Frostwolf Shaman"] = 12550,
        ["Bloodscalp Shaman"] = 8788,
        ["Magram Stormer"] = 8788,
        ["Maraudine Stormer"] = 8788,
        ["Dust Stormer"] = 19514,
        ["Deadwood Shaman"] = 13585,
    },
    ["Dark Iron Bracers"] = {
        Professions = 20874,
    },
    ["Enchanter's Cowl"] = {
        Professions = 3857,
    },
    ["Spirit Shade Visual"] = {
        ["Spirit Shade"] = 24809,
    },
    ["Withering Heat"] = {
        ["Ancient Core Hound"] = 19367,
    },
    ["Nightscape Pants"] = {
        Professions = 10548,
    },
    ["Disarm"] = {
        ["WARRIOR"] = 676,
        ["Muckrake"] = 8379,
        ["Jailor Borhuin"] = 8379,
        ["Kenata Dabyrie"] = 8379,
        ["Blackfathom Myrmidon"] = 8379,
        ["Murkshallow Snapclaw"] = 8379,
        ["Bloodscalp Warrior"] = 6713,
        ["Defias Looter"] = 6713,
        ["Sneed"] = 6713,
        ["First Mate Snellig"] = 6713,
        ["Defias Prisoner"] = 6713,
        ["Syndicate Thief"] = 6713,
        ["Hillsbrad Farmer"] = 6713,
        ["Twilight Thug"] = 6713,
        ["Syndicate Prowler"] = 6713,
        ["Lord Falconcrest"] = 6713,
        ["Venture Co. Enforcer"] = 6713,
        ["Theramore Marine"] = 6713,
        ["Vile Bat"] = 6713,
        ["Dal Bloodclaw"] = 6713,
        ["Dread Flyer"] = 6713,
        ["Defias Bodyguard"] = 6713,
        ["Grimtotem Bandit"] = 6713,
    },
    ["Tetanus"] = {
        ["Rusty Harvest Golem"] = 8014,
        ["Mountain Buzzard"] = 8014,
        ["Kolkar Marauder"] = 8014,
        ["Kolkar Invader"] = 8014,
    },
    ["Flash Bomb"] = {
        Professions = 8243,
    },
    ["Hunter's Mark"] = {
        ["HUNTER"] = 14325,
    },
    ["Blood Tap"] = {
        ["Shadowfang Glutton"] = 7122,
    },
    ["Cured Thick Hide"] = {
        Professions = 10482,
    },
    ["Golden Scale Bracers"] = {
        Professions = 7223,
    },
    ["Aspect of the Wild"] = {
        ["HUNTER"] = 20190,
    },
    ["Mithril Mechanical Dragonling"] = {
        Professions = 12624,
    },
    ["Bloodvine Boots"] = {
        Professions = 24093,
    },
    ["Immolate"] = {
        ["WARLOCK"] = 25309,
        ["Thule Ravenclaw"] = 20800,
        ["Jergosh the Invoker"] = 20800,
        ["Interrogator Vishas"] = 9034,
        ["Scarlet Torturer"] = 9275,
        ["Hederine Initiate"] = 17883,
        ["Entropic Beast"] = 15661,
        ["Burning Blade Cultist"] = 11962,
    },
    ["Crippling Poison II"] = {
        ["ROGUE"] = 3421,
    },
    ["Leech Poison"] = {
        ["Blindlight Muckdweller"] = 8382,
        ["Leech Stalker"] = 3358,
        ["Cave Stalker"] = 3358,
    },
    ["Simple Linen Pants"] = {
        Professions = 12044,
    },
    ["Mooncloth Shoulders"] = {
        Professions = 18448,
    },
    ["Parry"] = {
        ["SHAMAN"] = 16268,
    },
    ["Moonflare Totem"] = {
        ["Moonflare Totem"] = 15787,
        ["Winterfall Totemic"] = 15787,
    },
    ["Rose Colored Goggles"] = {
        Professions = 12618,
    },
    ["Frostweave Tunic"] = {
        Professions = 18403,
    },
    ["Black Silk Pack"] = {
        Professions = 6695,
    },
    ["Poison"] = {
        ["Murloc Tidehunter"] = 744,
        ["Venom Web Spider"] = 744,
        ["Rabid Shadowhide Gnoll"] = 744,
        ["Tarantula"] = 744,
        ["Greater Tarantula"] = 744,
        ["Pygmy Venom Web Spider"] = 744,
        ["Green Recluse"] = 744,
        ["Shadowhide Assassin"] = 744,
        ["Bloodscalp Scavenger"] = 744,
        ["Sorrow Spinner"] = 744,
        ["Saltscale Forager"] = 744,
        ["Defias Night Blade"] = 744,
        ["Nightbane Tainted One"] = 744,
        ["Black Widow Hatchling"] = 744,
        ["Rohh the Silent"] = 744,
        ["Bluegill Forager"] = 744,
        ["Dragonmaw Swamprunner"] = 744,
        ["Fen Lord"] = 744,
        ["Cliff Lurker"] = 744,
        ["Wood Lurker"] = 744,
        ["Forest Lurker"] = 744,
        ["Vicious Night Web Spider"] = 744,
        ["Moss Stalker"] = 744,
        ["Mist Creeper"] = 744,
        ["Greymist Tidehunter"] = 744,
        ["Syndicate Thief"] = 744,
        ["Syndicate Assassin"] = 744,
        ["Syndicate Rogue"] = 744,
        ["Shadowy Assassin"] = 744,
        ["Plains Creeper"] = 744,
        ["Giant Plains Creeper"] = 744,
        ["Dark Iron Supplier"] = 744,
        ["Bristleback Water Seeker"] = 744,
        ["Bleakheart Trickster"] = 744,
        ["Forsaken Infiltrator"] = 744,
        ["Wildthorn Lurker"] = 744,
        ["Deepmoss Creeper"] = 744,
        ["Young Pridewing"] = 744,
        ["Pridewing Wyvern"] = 744,
        ["Pridewing Skyhunter"] = 744,
        ["Pridewing Patriarch"] = 744,
        ["Highperch Wyvern"] = 744,
        ["Highperch Consort"] = 744,
        ["Highperch Patriarch"] = 744,
        ["Darkmist Spider"] = 744,
        ["Darkfang Lurker"] = 744,
        ["Darkfang Spider"] = 744,
        ["Giant Darkfang Spider"] = 744,
        ["Murkgill Forager"] = 744,
        ["Hatefury Trickster"] = 744,
        ["Slim's Friend"] = 744,
        ["Deviate Adder"] = 744,
        ["Centipaar Wasp"] = 744,
        ["Glassweb Spider"] = 744,
        ["Gutspill"] = 744,
        ["Great Wavethrasher"] = 744,
        ["Tomb Fiend"] = 744,
        ["Galak Assassin"] = 744,
        ["Arikara"] = 744,
        ["Bazzalan"] = 744,
        ["Rock Stalker"] = 744,
        ["Forest Spider"] = 11918,
        ["Mine Spider"] = 11918,
        ["Morgaine the Sly"] = 11918,
        ["Mother Fang"] = 11918,
        ["Night Web Matriarch"] = 11918,
        ["Githyiss the Vile"] = 11918,
        ["Webwood Lurker"] = 11918,
        ["Webwood Venomfang"] = 11918,
        ["Webwood Silkspinner"] = 11918,
        ["Giant Webwood Spider"] = 11918,
        ["Vile Sprite"] = 11918,
        ["Clattering Scorpid"] = 11918,
        ["Corrupted Scorpid"] = 11918,
        ["Sarkoth"] = 11918,
    },
    ["Pyroblast"] = {
        ["MAGE"] = 18809,
    },
    ["Fire Oil"] = {
        Professions = 7837,
    },
    ["Two-Handed Axes and Maces"] = {
        ["SHAMAN"] = 16269,
    },
    ["Wide Slash"] = {
        ["Harvest Reaper"] = 7342,
        ["Deviate Lasher"] = 7342,
    },
    ["Hillman's Leather Gloves"] = {
        Professions = 3764,
    },
    ["Sap"] = {
        ["ROGUE"] = 11297,
    },
    ["Venom's Bane"] = {
        ["Kayneth Stillwind"] = 6354,
    },
    ["Curse of the Darkmaster"] = {
        ["Darkmaster Gandling"] = 18702,
    },
    ["Wyvern Sting"] = {
        ["HUNTER"] = 24133,
    },
    ["Honor Points +2388"] = {
        ["Horde Warbringer"] = 24966,
        ["Alliance Brigadier General"] = 24966,
    },
    ["Poison Cleansing Totem"] = {
        ["SHAMAN"] = 8166,
    },
    ["Big Bag of Enchantment"] = {
        Professions = 27660,
    },
    ["Wild Leather Cloak"] = {
        Professions = 10574,
    },
    ["Blizzard"] = {
        ["MAGE"] = 10187,
        ["Lokholar the Ice Lord"] = 21367,
    },
    ["Soul Siphon"] = {
        ["Azshir the Sleepless"] = 9373,
        ["Ravenclaw Servant"] = 7290,
        ["Fizzle Darkstorm"] = 7290,
    },
    ["Accurate Scope"] = {
        Professions = 3979,
    },
    ["Emberseer Despawn"] = {
        ["Pyroguard Emberseer"] = 16078,
    },
    ["Demonic Sacrifice"] = {
        ["WARLOCK"] = 18788,
    },
    ["Volley"] = {
        ["HUNTER"] = 14295,
    },
    ["Gold Power Core"] = {
        Professions = 12584,
    },
    ["Prayer of Spirit"] = {
        ["PRIEST"] = 27681,
    },
    ["Quick Shot"] = {
        ["Palemane Poacher"] = 1516,
    },
    ["Quickdraw Quiver"] = {
        Professions = 14930,
    },
    ["Summon Mana Bursts"] = {
        ["Mana Burst"] = 22939,
        ["Residual Monstrosity"] = 22939,
    },
    ["Grounding Totem"] = {
        ["SHAMAN"] = 8177,
    },
    ["Portable Bronze Mortar"] = {
        Professions = 3960,
    },
    ["Runic Leather Belt"] = {
        Professions = 19072,
    },
    ["Rocket, BLUE BIG"] = {
        ["Pat's Firework Guy - BLUE BIG"] = 26351,
    },
    ["The Big One"] = {
        Professions = 12754,
    },
    ["Devour Magic"] = {
        Pet = 19736,
    },
    ["Philosophers' Stone"] = {
        Professions = 11459,
    },
    ["Delicate Arcanite Converter"] = {
        Professions = 19815,
    },
    ["Summon Human Skull"] = {
        ["Human Skull"] = 19031,
    },
    ["Massive Eruption"] = {
        ["Firesworn"] = 20483,
    },
    ["Inlaid Mithril Cylinder Plans"] = {
        Professions = 12895,
    },
    ["Flamespit"] = {
        ["Fire Roc"] = 11021,
    },
    ["Deadly Poison III"] = {
        ["ROGUE"] = 11357,
    },
    ["Earthen Vest"] = {
        Professions = 8764,
    },
    ["Lightning Blast"] = {
        ["Wrathtail Sea Witch"] = 8598,
    },
    ["Spark"] = {
        ["Ley Sprite"] = 21027,
        ["Mana Sprite"] = 21027,
    },
    ["Spider Belt"] = {
        Professions = 3863,
    },
    ["Snowball Cloud"] = {
        ["Pat's Snowcloud Guy"] = 26000,
    },
    ["Summon Moonstalker Runt"] = {
        ["Moonstalker Runt"] = 8594,
        ["Moonstalker Matriarch"] = 8594,
    },
    ["Ghost Wolf"] = {
        ["SHAMAN"] = 2645,
    },
    ["Pink Mageweave Shirt"] = {
        Professions = 12080,
    },
    ["Rich Purple Silk Shirt"] = {
        Professions = 3872,
    },
    ["Gray Woolen Shirt"] = {
        Professions = 2406,
    },
    ["Berserking"] = {
        Racial = 26296,
        Racial = 26297,
        Racial = 20554,
    },
    ["Chained Bolt"] = {
        ["Owatanka"] = 6254,
        ["Margol the Rager"] = 15549,
    },
    ["Summon Helcular's Puppets"] = {
        ["Puppet of Helcular"] = 4950,
        ["Helcular's Remains"] = 4950,
    },
    ["Cold Blood"] = {
        ["ROGUE"] = 14177,
    },
    ["Shoot"] = {
        ["Rifleman Middlecamp"] = 16572,
        ["Rifleman Wheeler"] = 16775,
        ["Rifleman Middlecamp"] = 16772,
        ["Rifleman Middlecamp"] = 16768,
        ["Rifleman Middlecamp"] = 16780,
        ["Rifleman Wheeler"] = 16778,
        ["Rifleman Middlecamp"] = 16779,
        ["Rifleman Middlecamp"] = 16776,
        ["Rifleman Middlecamp"] = 16777,
        ["Rifleman Wheeler"] = 16767,
        ["Dark Iron Rifleman"] = 8996,
        ["Dark Iron Rifleman"] = 8997,
        ["Dark Iron Rifleman"] = 8995,
        ["Roggo Harlbarrow"] = 9008,
        ["Motley Garmason"] = 9008,
        ["Helena Atwood"] = 9008,
    },
    ["Heavy Scorpid Belt"] = {
        Professions = 19070,
    },
    ["Hemorrhage"] = {
        ["ROGUE"] = 17348,
    },
    ["Small Leather Ammo Pouch"] = {
        Professions = 9062,
    },
    ["Summon Tamed Hyena"] = {
        ["Tamed Hyena"] = 8276,
        ["Razorfen Beastmaster"] = 8276,
    },
    ["Darken Vision"] = {
        ["Dark Sprite"] = 5514,
    },
    ["Mass Nullify"] = {
        ["Arcane Nullifier X-21"] = 10832,
    },
    ["Green Leather Bracers"] = {
        Professions = 3776,
    },
    ["Mind Probe"] = {
        ["Scarshield Infiltrator"] = 16037,
    },
    ["Wide Swipe"] = {
        ["Overlord Mok'Morokk"] = 6749,
    },
    ["Rockbiter Weapon"] = {
        ["SHAMAN"] = 16316,
    },
    ["Enchant Shield - Lesser Spirit"] = {
        Professions = 13485,
    },
    ["Guardian Cloak"] = {
        Professions = 7153,
    },
    ["Arclight Spanner"] = {
        Professions = 7430,
    },
    ["Green Silken Shoulders"] = {
        Professions = 8774,
    },
    ["Frenzied Rage"] = {
        ["Thistlefur Den Watcher"] = 3490,
        ["Aku'mai"] = 3490,
    },
    ["Enchant Gloves - Greater Agility"] = {
        Professions = 20012,
    },
    ["Divine Intervention"] = {
        ["PALADIN"] = 19752,
    },
    ["Elixir of Fortitude"] = {
        Professions = 3450,
    },
    ["Spirit of Kirith"] = {
        ["Spirit of Kirith"] = 10853,
        ["Kirith the Damned"] = 10853,
    },
    ["Black Sludge"] = {
        ["Sludge Beast"] = 7279,
        ["Tar Lurker"] = 7279,
        ["The Reak"] = 7279,
        ["Rotting Sludge"] = 7279,
    },
    ["Honor Points +138"] = {
        ["Horde Warbringer"] = 24962,
        ["Alliance Brigadier General"] = 24962,
    },
    ["Sayge's Dark Fortune of Resistance"] = {
        ["Sayge"] = 23769,
    },
    ["Crypt Scarabs"] = {
        ["Crypt Scarab"] = 16418,
        ["Crypt Crawler"] = 16418,
    },
    ["Mooncloth Boots"] = {
        Professions = 19435,
    },
    ["Elixir of Superior Defense"] = {
        Professions = 17554,
    },
    ["Blood Fury"] = {
        Racial = 20572,
    },
    ["Ornate Thorium Handaxe"] = {
        Professions = 16969,
    },
    ["Silithid Creeper Egg"] = {
        ["Silithid Creeper Egg"] = 6587,
        ["Silithid Creeper"] = 6587,
    },
    ["Elixir of Dream Vision"] = {
        Professions = 11468,
    },
    ["Mortal Strike"] = {
        ["WARRIOR"] = 21553,
        ["Strashaz Warrior"] = 16856,
        ["Hederine Slayer"] = 16856,
        ["Captain Galvangar"] = 16856,
        ["Irondeep Guard"] = 16856,
        ["Veteran Irondeep Guard"] = 16856,
        ["Champion Irondeep Guard"] = 16856,
        ["Stormpike Ram Rider Commander"] = 16856,
        ["Jotek"] = 16856,
        ["Lieutenant Rugba"] = 15708,
        ["Lieutenant Spencer"] = 15708,
        ["Lieutenant Stronghoof"] = 15708,
        ["Lieutenant Vol'talar"] = 15708,
        ["Lieutenant Grummus"] = 15708,
        ["Lieutenant Murp"] = 15708,
        ["Lieutenant Lewis"] = 15708,
        ["Lieutenant Largent"] = 15708,
        ["Lieutenant Stouthandle"] = 15708,
        ["Lieutenant Greywand"] = 15708,
        ["Lieutenant Lonadin"] = 15708,
    },
    ["Furious Howl"] = {
        Pet = 24597,
        ["Coyote Packleader"] = 3149,
        ["Black Ravager Mastiff"] = 3149,
    },
    ["Large Green Rocket Cluster"] = {
        Professions = 26427,
    },
    ["Frost Tiger Blade"] = {
        Professions = 3497,
    },
    ["Curse of Weakness"] = {
        ["WARLOCK"] = 11708,
        ["Jergosh the Invoker"] = 18267,
        ["Darbel Montrose"] = 12741,
        ["Morloch"] = 12741,
        ["Ironspine"] = 21007,
        ["Shadowclaw"] = 17227,
        ["Frostmane Shadowcaster"] = 11980,
        ["Mosh'Ogg Witch Doctor"] = 11980,
        ["Licillin"] = 11980,
        ["Balizar the Umbrage"] = 11980,
        ["Xabraxxis"] = 11980,
    },
    ["Area Burn"] = {
        ["Burning Destroyer"] = 8000,
    },
    ["Dark Leather Shoulders"] = {
        Professions = 3769,
    },
    ["Toughened Leather Armor"] = {
        Professions = 2166,
    },
    ["Frostmane Strength"] = {
        ["Grik'nir the Cold"] = 6957,
    },
    ["Polymorph"] = {
        ["MAGE"] = 12826,
        ["Syndicate Wizard"] = 13323,
        ["Warden Belamoore"] = 13323,
        ["Arcanist Doan"] = 13323,
        ["Captain Balinda Stonehearth"] = 13323,
    },
    ["Teleport: Thunder Bluff"] = {
        ["MAGE"] = 3566,
    },
    ["Foul Chill"] = {
        ["Befouled Water Elemental"] = 6873,
        ["Tideress"] = 6873,
    },
    ["Woolen Boots"] = {
        Professions = 2401,
    },
    ["Elixir of Greater Intellect"] = {
        Professions = 11465,
    },
    ["Acid of Hakkar"] = {
        ["Spawn of Hakkar"] = 12280,
    },
    ["Decayed Strength"] = {
        ["Leper Gnome"] = 6951,
        ["Corrupted Surf Crawler"] = 6951,
    },
    ["Runed Copper Gauntlets"] = {
        Professions = 3323,
    },
    ["Herbalist's Gloves"] = {
        Professions = 9146,
    },
    ["Cleave"] = {
        ["WARRIOR"] = 20569,
        ["Highlord Bolvar Fordragon"] = 20684,
        ["Herod"] = 22540,
        ["Withered Reaver"] = 11427,
        ["Teremus the Devourer"] = 11427,
        ["Green Scalebane"] = 15496,
        ["Kurzen Elite"] = 15496,
        ["Crushridge Plunderer"] = 15496,
        ["Orgrimmar Grunt"] = 15496,
        ["Fallen Champion"] = 15496,
        ["Grimtotem Reaver"] = 15496,
        ["Lord Kragaru"] = 15496,
        ["Kor'kron Elite"] = 15496,
        ["Iceblood Marshal"] = 15579,
        ["Tower Point Marshal"] = 15579,
        ["East Frostwolf Marshal"] = 15579,
        ["West Frostwolf Marshal"] = 15579,
        ["Dun Baldar North Warmaster"] = 15579,
        ["Dun Baldar South Warmaster"] = 15579,
        ["Icewing Warmaster"] = 15579,
        ["Stonehearth Warmaster"] = 15579,
        ["West Frostwolf Warmaster"] = 15579,
        ["Goblin Woodcarver"] = 5532,
        ["Captain Greenskin"] = 5532,
        ["Venture Co. Lumberjack"] = 5532,
        ["Scorpid Reaver"] = 5532,
        ["Oggleflint"] = 5532,
        ["Strashaz Myrmidon"] = 15754,
        ["Hederine Slayer"] = 15754,
        ["Captain Galvangar"] = 15754,
        ["Whitewhisker Overseer"] = 15663,
        ["Irondeep Guard"] = 15663,
        ["Commander Randolph"] = 15663,
        ["Commander Dardosh"] = 15663,
        ["Commander Malgor"] = 15663,
        ["Commander Mulfort"] = 15663,
        ["Commander Louis Philips"] = 15663,
        ["Commander Mortimer"] = 15663,
        ["Commander Duffy"] = 15663,
        ["Commander Karl Philips"] = 15663,
        ["Seasoned Irondeep Guard"] = 15663,
        ["Veteran Irondeep Guard"] = 15663,
        ["Champion Irondeep Guard"] = 15663,
        ["Stormpike Ram Rider Commander"] = 15663,
        ["Jotek"] = 15663,
    },
    ["Serpentine Cleansing"] = {
        ["Disciple of Naralex"] = 6270,
    },
    ["Stealth"] = {
        ["ROGUE"] = 1787,
        ["Colonel Kurzen"] = 8822,
    },
    ["Handstitched Leather Pants"] = {
        Professions = 2153,
    },
    ["Slam"] = {
        ["WARRIOR"] = 11605,
        ["Razorfen Battleguard"] = 11430,
    },
    ["Enchant Gloves - Fishing"] = {
        Professions = 13620,
    },
    ["Summon Dalaran Serpent"] = {
        ["Dalaran Serpent"] = 3615,
        ["Dalaran Protector"] = 3615,
        ["Dalaran Warder"] = 3615,
    },
    ["Enchant Boots - Greater Agility"] = {
        Professions = 20023,
    },
    ["Shadoweave Gloves"] = {
        Professions = 12071,
    },
    ["Expose Armor"] = {
        ["ROGUE"] = 11198,
    },
    ["Windsor's Frenzy"] = {
        ["Marshal Windsor"] = 15167,
    },
    ["Healing Potion"] = {
        Professions = 3447,
    },
    ["Threatening Growl"] = {
        ["Prairie Wolf"] = 5781,
        ["Prairie Wolf Alpha"] = 5781,
    },
    ["Steel Weapon Chain"] = {
        Professions = 7224,
    },
    ["Wracking Pains"] = {
        ["Twilight's Hammer Torturer"] = 13619,
        ["Aspect of Corruption"] = 13619,
    },
    ["Ornate Mithril Gloves"] = {
        Professions = 9950,
    },
    ["Red Linen Shirt"] = {
        Professions = 2392,
    },
    ["Guardian Gloves"] = {
        Professions = 7156,
    },
    ["Dark Silk Shirt"] = {
        Professions = 3870,
    },
    ["Holy Shield"] = {
        ["PALADIN"] = 20928,
    },
    ["Summon Lava Burst E"] = {
        ["Core Rat"] = 21903,
    },
    ["Searing Golden Blade"] = {
        Professions = 15973,
    },
    ["Dark Iron Rifle"] = {
        Professions = 19796,
    },
    ["Enchant Gloves - Herbalism"] = {
        Professions = 13617,
    },
    ["Steel Plate Helm"] = {
        Professions = 9935,
    },
    ["Dreamless Sleep Potion"] = {
        Professions = 15833,
    },
    ["Heavy Dynamite"] = {
        Professions = 3946,
    },
    ["Flarecore Gloves"] = {
        Professions = 20849,
    },
    ["Blackmouth Oil"] = {
        Professions = 7836,
    },
    ["Jadefire"] = {
        ["Jadefire Satyr"] = 13578,
        ["Jadefire Rogue"] = 13578,
        ["Jadefire Trickster"] = 13578,
        ["Jadefire Betrayer"] = 13578,
        ["Jadefire Felsworn"] = 13578,
        ["Jadefire Shadowstalker"] = 13578,
        ["Jadefire Hellcaller"] = 13578,
        ["Prince Xavalis"] = 13578,
    },
    ["Cleanse"] = {
        ["PALADIN"] = 4987,
    },
    ["Fire Nova Totem"] = {
        ["SHAMAN"] = 11315,
    },
    ["Frost Armor"] = {
        ["MAGE"] = 7301,
    },
    ["Blight"] = {
        Professions = 10011,
    },
    ["Mithril Scale Pants"] = {
        Professions = 9931,
    },
    ["Elixir of Wisdom"] = {
        Professions = 3171,
    },
    ["Swift Boots"] = {
        Professions = 9208,
    },
    ["Volcanic Hammer"] = {
        Professions = 16984,
    },
    ["Curse of Recklessness"] = {
        ["WARLOCK"] = 11717,
        ["Bloodfury Slayer"] = 16231,
    },
    ["Web Spray"] = {
        ["Tuten'kash"] = 12252,
    },
    ["Green Iron Gauntlets"] = {
        Professions = 3336,
    },
    ["Piercing Shadow"] = {
        ["Thuzadin Shadowcaster"] = 16429,
    },
    ["Felstrom Resurrection"] = {
        ["Commander Felstrom"] = 3488,
        ["Commander Felstrom"] = 3488,
    },
    ["Cured Medium Hide"] = {
        Professions = 3817,
    },
    ["Goblin Dragon Gun"] = {
        Professions = 12908,
    },
    ["Trelane's Freezing Touch"] = {
        ["Kor'gresh Coldrage"] = 4320,
    },
    ["Drop Mine"] = {
        ["Tonk Mine"] = 25024,
        ["Steam Tonk"] = 25024,
    },
    ["Frenzied Command"] = {
        ["Riverpaw Overseer"] = 3136,
        ["Sergeant Brashclaw"] = 3136,
    },
    ["Enchant Chest - Lesser Absorption"] = {
        Professions = 13538,
    },
    ["Slowing Poison"] = {
        ["Murloc Nightcrawler"] = 7992,
        ["Defias Night Blade"] = 7992,
        ["Skeletal Raider"] = 7992,
        ["Witchwing Roguefeather"] = 7992,
        ["Forsaken Assassin"] = 7992,
        ["Darkfang Creeper"] = 7992,
        ["Slim's Friend"] = 7992,
        ["Scorpid Hunter"] = 7992,
    },
    ["Wild Leather Leggings"] = {
        Professions = 10572,
    },
    ["Handstitched Leather Cloak"] = {
        Professions = 9058,
    },
    ["Rocket, GREEN BIG"] = {
        ["Pat's Firework Guy - GREEN BIG"] = 26352,
    },
    ["Toughened Leather Gloves"] = {
        Professions = 3770,
    },
    ["Rough Bronze Cuirass"] = {
        Professions = 2670,
    },
    ["Unholy Frenzy"] = {
        ["Skeletal Warder"] = 8699,
    },
    ["Mageblood Potion"] = {
        Professions = 24365,
    },
    ["Enchant Weapon - Icy Chill"] = {
        Professions = 20029,
    },
    ["Mechanical Repair Kit"] = {
        Professions = 15255,
    },
    ["Instant Poison IV"] = {
        ["ROGUE"] = 11341,
    },
    ["Shield Generator"] = {
        ["Steam Tonk"] = 27759,
    },
    ["Mana Burn"] = {
        ["PRIEST"] = 10876,
        ["Wrathtail Sea Witch"] = 2691,
        ["Felslayer"] = 2691,
        ["Severed Keeper"] = 2691,
        ["Summoned Felhunter"] = 2691,
        ["Wily Fey Dragon"] = 17630,
        ["Death's Head Acolyte"] = 15785,
        ["Haunting Vision"] = 11981,
        ["Hederine Manastalker"] = 15980,
    },
    ["Gorishi Egg"] = {
        ["Gorishi Egg"] = 14205,
        ["Clutchmother Zavas"] = 14205,
    },
    ["Bronze Shortsword"] = {
        Professions = 2742,
    },
    ["Coarse Weightstone"] = {
        Professions = 3116,
    },
    ["Seal of Justice"] = {
        ["PALADIN"] = 20164,
    },
    ["Arcane Brilliance"] = {
        ["MAGE"] = 23028,
    },
    ["Wicked Mithril Blade"] = {
        Professions = 9997,
    },
    ["Dark Leather Gloves"] = {
        Professions = 3765,
    },
    ["Lesser Heal"] = {
        ["PRIEST"] = 2053,
    },
    ["Enchant Chest - Lesser Stats"] = {
        Professions = 13700,
    },
    ["Evocation"] = {
        ["MAGE"] = 12051,
    },
    ["Bone Armor"] = {
        ["Death's Head Necromancer"] = 11445,
    },
    ["Scorpid Sting"] = {
        ["HUNTER"] = 14277,
        ["Ashenvale Outrunner"] = 18545,
    },
    ["Shock"] = {
        ["Thunderhead Patriarch"] = 12553,
        ["Thunderhead Consort"] = 12553,
        ["Vile Fin Oracle"] = 2606,
        ["Greenpaw"] = 2606,
        ["Blackwood Shaman"] = 2606,
        ["Vile Fin Minor Oracle"] = 2607,
        ["Nezzliok the Dire"] = 2610,
        ["Saltspittle Oracle"] = 2608,
        ["Bloodfury Ambusher"] = 2608,
        ["Twilight Elementalist"] = 2609,
        ["Twilight Dark Shaman"] = 15500,
        ["Thrall"] = 16034,
        ["Thundering Exile"] = 11824,
        ["Kolkar Destroyer"] = 11824,
        ["Arkkoran Oracle"] = 11824,
        ["Whirlwind Stormwalker"] = 11824,
    },
    ["Elixir of Poison Resistance"] = {
        Professions = 3174,
    },
    ["Mana Shield"] = {
        ["MAGE"] = 10193,
    },
    ["Wild Regeneration"] = {
        ["Deviate Shambler"] = 7948,
        ["Frostmaul Preserver"] = 9616,
    },
    ["Charge"] = {
        ["WARRIOR"] = 11578,
        Pet = 27685,
    },
    ["Resist Arcane"] = {
        ["Scarlet Medic"] = 17175,
    },
    ["White Leather Jerkin"] = {
        Professions = 2163,
    },
    ["Spawn Red Drakonid"] = {
        ["Red Drakonid"] = 22655,
        ["Red Drakonid Spawner"] = 22655,
    },
    ["Summon Earth Rumbler"] = {
        ["Stone Rumbler"] = 8270,
        ["Razorfen Geomancer"] = 8270,
        ["Earthcaller Halmgar"] = 8270,
        ["Roogug"] = 8270,
    },
    ["Sylvan Crown"] = {
        Professions = 28481,
    },
    ["Resistance Aura"] = {
        ["Crimson Courier"] = 19726,
    },
    ["Holy Nova"] = {
        ["PRIEST"] = 27801,
    },
    ["Frenzied Regeneration"] = {
        ["DRUID"] = 22896,
    },
    ["Perception"] = {
        Racial = 20600,
    },
    ["Ferocious Bite"] = {
        ["DRUID"] = 31018,
    },
    ["Thunderstomp"] = {
        Pet = 26188,
    },
    ["Tendon Rip"] = {
        ["Nefaru"] = 3604,
        ["Tethis"] = 3604,
        ["Elder Razormaw"] = 3604,
        ["Sawtooth Snapper"] = 3604,
        ["River Crocolisk"] = 3604,
        ["Saltwater Crocolisk"] = 3604,
        ["Snapjaw Crocolisk"] = 3604,
        ["The Husk"] = 3604,
        ["Giant Wetlands Crocolisk"] = 3604,
        ["Elder Saltwater Crocolisk"] = 3604,
        ["Shadethicket Bark Ripper"] = 3604,
        ["Terrowulf Fleshripper"] = 3604,
        ["Muckshell Clacker"] = 3604,
        ["Deep Pool Threshfin"] = 3604,
        ["Deviate Crocolisk"] = 3604,
        ["Snort the Heckler"] = 3604,
        ["Makrinni Snapclaw"] = 3604,
        ["Ravasaur Hunter"] = 3604,
        ["Shardtooth Bear"] = 3604,
        ["Rogue Ice Thistle"] = 3604,
        ["Ice Thistle Yeti"] = 3604,
        ["Razortalon"] = 3604,
        ["Old Cliff Jumper"] = 3604,
        ["Grunter"] = 3604,
        ["Manahound"] = 3604,
        ["Shadowforge Peasant"] = 3604,
        ["Felpaw Scavenger"] = 3604,
        ["Young Diemetradon"] = 3604,
        ["Lar'korwi Mate"] = 3604,
        ["Lar'korwi"] = 3604,
        ["Slavering Ember Worg"] = 3604,
        ["Deathmaw"] = 3604,
        ["Bayne"] = 3604,
        ["Mist Howler"] = 3604,
        ["Shy-Rotam"] = 3604,
        ["Sian-Rotam"] = 3604,
        ["Stonelash Pincer"] = 3604,
        ["Gordok Mastiff"] = 3604,
        ["Duskstalker"] = 3604,
        ["Zulian Crocolisk"] = 3604,
    },
    ["Thorium Belt"] = {
        Professions = 16643,
    },
    ["Major Troll's Blood Potion"] = {
        Professions = 24368,
    },
    ["Gray Woolen Robe"] = {
        Professions = 2403,
    },
    ["Rend"] = {
        ["WARRIOR"] = 11574,
        ["Strashaz Serpent Guard"] = 16509,
        ["Coldmine Invader"] = 16509,
        ["Wing Commander Ichman"] = 16509,
        ["Wing Commander Slidore"] = 16509,
        ["Gorefang"] = 13445,
        ["Bloodfury Ripper"] = 13445,
        ["Shadumbra"] = 13445,
        ["Black Ravager"] = 13443,
        ["Saltscale Forager"] = 13443,
        ["Elder Stranglethorn Tiger"] = 13443,
        ["Black Ravager Mastiff"] = 13443,
        ["Murkgill Forager"] = 13443,
        ["Arkkoran Pincer"] = 13443,
        ["Ravasaur Hunter"] = 13443,
        ["Ironbeak Hunter"] = 13443,
        ["Raging Moonkin"] = 13443,
        ["Bayne"] = 13443,
        ["Frostwolf"] = 13443,
        ["Alterac Ram"] = 13443,
        ["Drysnap Pincer"] = 13443,
        ["Whirlwind Shredder"] = 13443,
        ["Ursangous"] = 13443,
        ["Hederine Manastalker"] = 13738,
        ["Wrathtail Myrmidon"] = 11977,
        ["Slitherblade Warrior"] = 11977,
        ["Private Hendel"] = 11977,
        ["Arnak Grimtotem"] = 11977,
        ["Frostwolf Legionnaire"] = 11977,
        ["Torek"] = 11977,
        ["Silverwing Warrior"] = 11977,
        ["Sergeant Yazra Bloodsnarl"] = 11977,
    },
    ["Head Butt"] = {
        ["Hogger"] = 6730,
        ["Stormhide"] = 6730,
        ["Sparkleshell Snapper"] = 6730,
    },
    ["Agamaggan's Agility"] = {
        ["Mangletooth"] = 17013,
    },
    ["Golden Scale Coif"] = {
        Professions = 3503,
    },
    ["Corrupt Forces of Nature"] = {
        ["Corrupt Force of Nature"] = 21968,
    },
    ["Greater Arcane Protection Potion"] = {
        Professions = 17577,
    },
    ["Entropic Sting"] = {
        ["Franklin the Friendly"] = 23260,
    },
    ["Amplify Magic"] = {
        ["MAGE"] = 10170,
    },
    ["Nullify Mana"] = {
        ["Fey Dragon"] = 7994,
    },
    ["Ice Block"] = {
        ["MAGE"] = 11958,
    },
    ["Swift Wind"] = {
        ["Prate Cloudseer"] = 8385,
    },
    ["Quick Frost Ward"] = {
        ["Murloc Oracle"] = 4980,
        ["Skullsplitter Mystic"] = 4980,
        ["Skeletal Warder"] = 4980,
        ["Vile Fin Tidehunter"] = 4980,
        ["Dalaran Wizard"] = 4980,
        ["Wrathtail Sorceress"] = 4980,
        ["Lady Vespia"] = 4980,
    },
    ["Red Linen Bag"] = {
        Professions = 6686,
    },
    ["Faerie Fire"] = {
        ["DRUID"] = 9907,
        ["Frostmane Snowstrider"] = 6950,
        ["Rascal Sprite"] = 6950,
        ["Blackwood Pathfinder"] = 6950,
        ["Razormane Dustrunner"] = 6950,
        ["Razormane Thornweaver"] = 6950,
        ["Blackwood Tracker"] = 6950,
        ["Marosh the Devious"] = 6950,
    },
    ["Enchant Weapon - Minor Beastslayer"] = {
        Professions = 7786,
    },
    ["Azure Silk Pants"] = {
        Professions = 8758,
    },
    ["Spawn Blue Drakonid"] = {
        ["Blue Drakonid"] = 22658,
        ["Blue Drakonid Spawner"] = 22658,
    },
    ["Coarse Blasting Powder"] = {
        Professions = 3929,
    },
    ["Elemental Spawn-in"] = {
        ["Baron Kazum"] = 25035,
        ["The Duke of Cynders"] = 25035,
        ["The Duke of Fathoms"] = 25035,
        ["The Duke of Shards"] = 25035,
        ["Crimson Templar"] = 25035,
        ["Azure Templar"] = 25035,
        ["Hoary Templar"] = 25035,
        ["The Duke of Zephyrs"] = 25035,
        ["Earthen Templar"] = 25035,
    },
    ["Cenarion Herb Bag"] = {
        Professions = 27724,
    },
    ["Rough Boomstick"] = {
        Professions = 3925,
    },
    ["Reindeer Dust Effect"] = {
        ["Metzen the Reindeer"] = 25952,
    },
    ["Corrosive Poison"] = {
        ["Elder Moss Creeper"] = 3396,
        ["Giant Moss Creeper"] = 3396,
        ["Forest Moss Creeper"] = 3396,
        ["Corrosive Sap Beast"] = 3396,
        ["Fledgling Chimaera"] = 3396,
        ["Chimaera Matriarch"] = 3396,
    },
    ["Goblin Bomb Dispenser"] = {
        Professions = 12755,
    },
    ["Greater Frost Protection Potion"] = {
        Professions = 17575,
    },
    ["Dark Sludge"] = {
        ["Black Slime"] = 3335,
        ["Tainted Ooze"] = 3335,
        ["Sludginn"] = 3335,
    },
    ["Seal of Wisdom"] = {
        ["PALADIN"] = 20357,
    },
    ["Prowl"] = {
        ["DRUID"] = 9913,
        Pet = 24453,
    },
    ["Holy Cleave"] = {
        ["Highlord Taelan Fordring"] = 18819,
        ["Lord Tirion Fordring"] = 18819,
    },
    ["Robe of Power"] = {
        Professions = 8770,
    },
    ["Invisibility Potion"] = {
        Professions = 11464,
    },
    ["Shadow Shock"] = {
        ["Summoned Succubus"] = 16583,
        ["Vol'jin"] = 17289,
        ["Krethis Shadowspinner"] = 17439,
    },
    ["Bruiser Sleeping"] = {
        ["Drunken Bruiser"] = 26115,
    },
    ["Mithril Spurs"] = {
        Professions = 9964,
    },
    ["Imperial Plate Helm"] = {
        Professions = 16658,
    },
    ["Radiant Boots"] = {
        Professions = 16656,
    },
    ["Mind Tremor"] = {
        ["Razorfen Earthbreaker"] = 8272,
    },
    ["Essence of Pain"] = {
        ["ROGUE"] = 2930,
    },
    ["Sapta Sight"] = {
        ["Tiev Mordune"] = 9735,
    },
    ["Clone"] = {
        ["Cloned Ectoplasm"] = 7952,
        ["Devouring Ectoplasm"] = 7952,
        ["Cloned Ooze"] = 14146,
        ["Primal Ooze"] = 14146,
    },
    ["Phantom Blade"] = {
        Professions = 10007,
    },
    ["Portal: Orgrimmar"] = {
        ["MAGE"] = 11417,
    },
    ["Wandering Plague"] = {
        ["Plague Spreader"] = 3436,
        ["Pesterhide Hyena"] = 3436,
    },
    ["Barbed Sting"] = {
        ["Deep Stinger"] = 14534,
    },
    ["Glacial Cloak"] = {
        Professions = 28208,
    },
    ["Red Mageweave Pants"] = {
        Professions = 12060,
    },
    ["Nature's Swiftness"] = {
        ["DRUID"] = 17116,
        ["SHAMAN"] = 16188,
    },
    ["Redemption"] = {
        ["PALADIN"] = 20773,
    },
    ["Curse of Shahram"] = {
        ["Shahram"] = 16597,
    },
    ["Fine Leather Belt"] = {
        Professions = 3763,
    },
    ["Aspect of the Monkey"] = {
        ["HUNTER"] = 13163,
    },
    ["Festering Rash"] = {
        ["Ragged Owlbeast"] = 15848,
    },
    ["Truesilver Transformer"] = {
        Professions = 23071,
    },
    ["Sap Might"] = {
        ["Sap Beast"] = 7997,
    },
    ["Diseased Shot"] = {
        ["Withered Spearhide"] = 11397,
    },
    ["Might of the Timbermaw"] = {
        Professions = 23703,
    },
    ["Wildthorn Mail"] = {
        Professions = 16650,
    },
    ["Stormshroud Shoulders"] = {
        Professions = 19090,
    },
    ["Thorium Widget"] = {
        Professions = 19791,
    },
    ["Summon Hakkar"] = {
        ["Shade of Hakkar"] = 12639,
    },
    ["Minor Wizard Oil"] = {
        Professions = 25124,
    },
    ["Azrethoc's Stomp"] = {
        ["Lord Azrethoc's Image"] = 7961,
    },
    ["Iron Buckle"] = {
        Professions = 8768,
    },
    ["Starfire"] = {
        ["DRUID"] = 25298,
    },
    ["Enchant Gloves - Advanced Mining"] = {
        Professions = 13841,
    },
    ["Elixir of Greater Agility"] = {
        Professions = 11467,
    },
    ["Nimble Leather Gloves"] = {
        Professions = 9074,
    },
    ["Patterned Bronze Bracers"] = {
        Professions = 2672,
    },
    ["Solid Iron Maul"] = {
        Professions = 3494,
    },
    ["Discombobulator Ray"] = {
        Professions = 3959,
    },
    ["Dawn's Edge"] = {
        Professions = 16970,
    },
    ["Stoneskin Totem"] = {
        ["SHAMAN"] = 10408,
    },
    ["Dark Offering"] = {
        ["Arugal's Voidwalker"] = 7154,
    },
    ["Summon Burning Servant"] = {
        ["Burning Servant"] = 10870,
    },
    ["Necrotic Poison"] = {
        ["Maexxna Spiderling"] = 28776,
    },
    ["Feign Death"] = {
        ["HUNTER"] = 5384,
    },
    ["Storm Gauntlets"] = {
        Professions = 16661,
    },
    ["Mocking Blow"] = {
        ["WARRIOR"] = 20560,
    },
    ["Quick Flame Ward"] = {
        ["Defias Renegade Mage"] = 4979,
        ["Bloodscalp Mystic"] = 4979,
        ["Skeletal Warder"] = 4979,
        ["Venture Co. Geologist"] = 4979,
        ["Stonesplinter Geomancer"] = 4979,
        ["Tunnel Rat Geomancer"] = 4979,
        ["Splinter Fist Firemonger"] = 4979,
        ["Defias Evoker"] = 4979,
        ["Dalaran Mage"] = 4979,
        ["Grel'borg the Miser"] = 4979,
        ["Bristleback Geomancer"] = 4979,
        ["Defias Wizard"] = 4979,
        ["Foreman Cozzle"] = 4979,
    },
    ["Life Tap"] = {
        ["WARLOCK"] = 11689,
    },
    ["Runed Copper Belt"] = {
        Professions = 2666,
    },
    ["Summon Prismatic Exile"] = {
        ["Prismatic Exile"] = 10388,
        ["Myzrael"] = 10388,
    },
    ["Eviscerate"] = {
        ["ROGUE"] = 31016,
    },
    ["Shadow Bolt"] = {
        ["WARLOCK"] = 25307,
        ["Outcast Necromancer"] = 20298,
        ["Frostmane Shadowcaster"] = 20791,
        ["Dalaran Conjuror"] = 20791,
        ["Licillin"] = 20791,
        ["Syndicate Shadow Mage"] = 20791,
        ["Burning Blade Apprentice"] = 20791,
        ["Fizzle Darkstorm"] = 20791,
        ["Gazz'uz"] = 20791,
        ["Ilkrud Magthrull"] = 20791,
        ["Dark Strand Cultist"] = 20791,
        ["Dark Strand Adept"] = 20791,
        ["Apothecary Falthis"] = 20791,
        ["Balizar the Umbrage"] = 20791,
        ["Searing Blade Warlock"] = 20791,
        ["Dragonmaw Shadowwarder"] = 20807,
        ["Dark Strand Voidcaller"] = 20807,
        ["Burning Blade Augur"] = 20807,
        ["Bookie Herod"] = 20816,
        ["Argus Shadow Mage"] = 20816,
        ["Witherbark Shadowcaster"] = 20816,
        ["Dark Iron Shadowcaster"] = 20816,
        ["Burning Blade Shadowmage"] = 20816,
        ["Bloodsail Warlock"] = 20825,
        ["Skeletal Acolyte"] = 20825,
        ["Shadowforge Darkweaver"] = 20825,
        ["Burning Blade Summoner"] = 20825,
        ["Hederine Initiate"] = 15232,
        ["Morloch"] = 15537,
        ["Shadowhide Darkweaver"] = 9613,
        ["Blackrock Shadowcaster"] = 9613,
        ["Nightbane Shadow Weaver"] = 9613,
        ["Skeletal Healer"] = 9613,
        ["Kurzen Headshrinker"] = 9613,
        ["Zanzil Witch Doctor"] = 9613,
        ["Shadow Sprite"] = 9613,
        ["Syndicate Conjuror"] = 9613,
        ["Darbel Montrose"] = 9613,
        ["Scarlet Augur"] = 9613,
        ["Scarlet Scryer"] = 9613,
        ["Death Speaker Jargba"] = 9613,
        ["Death's Head Priest"] = 9613,
        ["Bloodmage Thalnos"] = 9613,
        ["Fallenroot Hellcaller"] = 9613,
        ["Twilight Shadowmage"] = 9613,
        ["Felweaver Scornn"] = 9613,
        ["Death's Head Necromancer"] = 9613,
        ["Death's Head Cultist"] = 9613,
        ["Mannoroc Lasher"] = 9613,
    },
    ["Enchant Shield - Superior Spirit"] = {
        Professions = 20016,
    },
    ["Dense Blasting Powder"] = {
        Professions = 19788,
    },
    ["Enchanted Quickness"] = {
        ["Defias Enchanter"] = 3443,
        ["Spitelash Enchantress"] = 3443,
        ["Mith'rethis the Enchanter"] = 3443,
    },
    ["Lightning Breath"] = {
        Pet = 25012,
        ["Chillwind Chimaera"] = 15797,
    },
    ["Repentance"] = {
        ["PALADIN"] = 20066,
    },
    ["Summon Blackhand Veteran"] = {
        ["Summoned Blackhand Veteran"] = 15792,
        ["Blackhand Summoner"] = 15792,
    },
    ["Lesser Mana Potion"] = {
        Professions = 3173,
    },
    ["Light Armor Kit"] = {
        Professions = 2152,
    },
    ["Smelt Iron"] = {
        Professions = 3307,
    },
    ["Summon Demon of the Orb"] = {
        ["Demon of the Orb"] = 9097,
        ["Tabetha"] = 9097,
    },
    ["Potion Strength II"] = {
        ["Riverpaw Herbalist"] = 3369,
    },
    ["Cloak of Fire"] = {
        Professions = 18422,
    },
    ["Razelikh's Tear II"] = {
        ["Razelikh the Defiler"] = 10866,
    },
    ["Feathered Breastplate"] = {
        Professions = 10647,
    },
    ["Wizardweave Leggings"] = {
        Professions = 18421,
    },
    ["Nature Resistance Totem"] = {
        ["SHAMAN"] = 10601,
    },
    ["Supercharge"] = {
        ["Leprous Technician"] = 10732,
        ["Leprous Machinesmith"] = 10732,
    },
    ["Brightcloth Robe"] = {
        Professions = 18414,
    },
    ["Mage Sight"] = {
        ["Archmage Ansirem Runeweaver"] = 3659,
    },
    ["Enchant 2H Weapon - Minor Impact"] = {
        Professions = 7745,
    },
    ["Summon Skeletal Servant"] = {
        ["Skeletal Servant"] = 12420,
        ["Death's Head Necromancer"] = 12420,
        ["Ambassador Bloodrage"] = 12420,
        ["Necromancer"] = 12420,
        ["Thuzadin Necromancer"] = 12420,
    },
    ["Stormshroud Pants"] = {
        Professions = 19067,
    },
    ["Mana Spike"] = {
        ["Charlga Razorflank"] = 8358,
    },
    ["Curse of Impotence"] = {
        ["Wildspawn Trickster"] = 22371,
        ["Blood Steward of Kirtonos"] = 22371,
    },
    ["Execute"] = {
        ["WARRIOR"] = 20662,
        ["Shadowhide Slayer"] = 7160,
        ["Witherbark Headhunter"] = 7160,
        ["Witchwing Slayer"] = 7160,
        ["Bloodfury Slayer"] = 7160,
        ["Salt Flats Scavenger"] = 7160,
        ["Salt Flats Vulture"] = 7160,
    },
    ["Harden Skin"] = {
        ["Petrified Treant"] = 22693,
    },
    ["Elemental Protection Totem"] = {
        ["Elemental Protection Totem"] = 8262,
        ["Death's Head Sage"] = 8262,
        ["Timbermaw Totemic"] = 8262,
        ["Smolderthorn Witch Doctor"] = 8262,
    },
    ["Fire Resistance Aura"] = {
        ["PALADIN"] = 19900,
    },
    ["Summon Shield Guard"] = {
        ["Dalaran Shield Guard"] = 3655,
        ["Dalaran Summoner"] = 3655,
    },
    ["Elixir of Greater Water Breathing"] = {
        Professions = 22808,
    },
    ["Rift Beacon"] = {
        ["Lost One Riftseeker"] = 9614,
    },
    ["Aura of Flames"] = {
        ["Death Talon Captain"] = 22436,
    },
    ["Black Dragonscale Breastplate"] = {
        Professions = 19085,
    },
    ["Seal of Light"] = {
        ["PALADIN"] = 20349,
    },
    ["Raptor Strike"] = {
        ["HUNTER"] = 14266,
    },
    ["Howl of Terror"] = {
        ["WARLOCK"] = 17928,
    },
    ["Frostbolt Volley"] = {
        ["Aku'mai Servant"] = 8398,
        ["Hakkari Frostwing"] = 8398,
        ["Ras Frostwhisper"] = 8398,
    },
    ["Power Word: Shield"] = {
        ["PRIEST"] = 10901,
        ["High Inquisitor Fairbanks"] = 11647,
        ["Nethergarde Cleric"] = 11974,
    },
    ["Summon Lava Burst B"] = {
        ["Core Rat"] = 21900,
    },
    ["Elixir of Brute Force"] = {
        Professions = 17557,
    },
    ["Divine Touch of Vaelastrasz"] = {
        ["Vaelastrasz the Red"] = 16332,
    },
    ["Blessing of Shahram"] = {
        ["Shahram"] = 16599,
    },
    ["Heavy Scorpid Vest"] = {
        Professions = 19051,
    },
    ["Snap Kick"] = {
        ["Defias Bandit"] = 8646,
        ["Defias Dockworker"] = 8646,
        ["Ashenvale Outrunner"] = 8646,
    },
    ["Claw"] = {
        ["DRUID"] = 9850,
        Pet = 3009,
    },
    ["Transmute: Mithril to Truesilver"] = {
        Professions = 11480,
    },
    ["Enlarge"] = {
        ["Twilight Loreseeker"] = 8365,
    },
    ["Corrupted Agility"] = {
        ["Foulweald Pathfinder"] = 6817,
        ["Oakpaw"] = 6817,
    },
    ["Heavy Scorpid Bracers"] = {
        Professions = 19048,
    },
    ["Deafening Screech"] = {
        ["Daggerspine Screamer"] = 3589,
        ["Screeching Harpy"] = 3589,
        ["Spitelash Screamer"] = 3589,
        ["Ironbeak Screecher"] = 3589,
        ["Winterspring Screecher"] = 3589,
        ["Hate Shrieker"] = 3589,
        ["Shrieking Banshee"] = 3589,
        ["Snowblind Harpy"] = 3589,
        ["Fury Shelda"] = 3589,
    },
    ["Track Demons"] = {
        ["HUNTER"] = 19878,
    },
    ["Shadowhorn Charge"] = {
        ["Shadowhorn Stag"] = 6921,
        ["Elder Shadowhorn Stag"] = 6921,
    },
    ["Multi-Shot"] = {
        ["HUNTER"] = 25294,
        ["Lady Sylvanas Windrunner"] = 20735,
        ["Venture Co. Mechanic"] = 14443,
        ["Leprous Defender"] = 14443,
    },
    ["Antu'sul's Minion"] = {
        ["Servant of Antu'sul"] = 11894,
    },
    ["Banshee Curse"] = {
        ["Cursed Highborne"] = 5884,
        ["Writhing Highborne"] = 5884,
        ["Wailing Highborne"] = 5884,
        ["Anaya Dawnrunner"] = 5884,
    },
    ["Fine Leather Gloves"] = {
        Professions = 2164,
    },
    ["Jade Serpentblade"] = {
        Professions = 3493,
    },
    ["Cracking Stone"] = {
        ["Elemental Slave"] = 3671,
    },
    ["Consume Flesh"] = {
        ["Gnasher"] = 3393,
        ["Highland Fleshstalker"] = 3393,
    },
    ["Mithril Casing"] = {
        Professions = 12599,
    },
    ["Radiant Gloves"] = {
        Professions = 16654,
    },
    ["Myzrael Earthquake"] = {
        ["Myzrael"] = 4938,
    },
    ["Enchant Bracer - Minor Agility"] = {
        Professions = 7779,
    },
    ["Crazed Hunger"] = {
        ["Starving Winter Wolf"] = 3151,
    },
    ["Copper Claymore"] = {
        Professions = 9983,
    },
    ["Highlord's Justice"] = {
        ["Highlord Bolvar Fordragon"] = 20683,
    },
    ["Alchemist's Stone"] = {
        Professions = 17632,
    },
    ["Gnomish Net-o-Matic Projector"] = {
        Professions = 12902,
    },
    ["Demoralizing Roar"] = {
        ["DRUID"] = 9898,
        ["Cenarion Protector"] = 15727,
    },
    ["Crowd Pummel"] = {
        ["Ograbisi"] = 10887,
        ["Setis"] = 10887,
    },
    ["Enchant Bracer - Lesser Spirit"] = {
        Professions = 7859,
    },
    ["Bash"] = {
        ["DRUID"] = 8983,
    },
    ["Colorful Kilt"] = {
        Professions = 12047,
    },
    ["Ornate Spyglass"] = {
        Professions = 6458,
    },
    ["Withered Touch"] = {
        ["Withered Quilguard"] = 11442,
        ["Living Decay"] = 11442,
    },
    ["Dense Dynamite"] = {
        Professions = 23070,
    },
    ["Mend Pet"] = {
        ["HUNTER"] = 13544,
    },
    ["Enchant Cloak - Minor Agility"] = {
        Professions = 13419,
    },
    ["Poisonous Stab"] = {
        ["Murloc Coastrunner"] = 7357,
        ["Riverpaw Bandit"] = 7357,
    },
    ["Escape Artist"] = {
        Racial = 20589,
    },
    ["Rhahk'Zor Slam"] = {
        ["Rhahk'Zor"] = 6304,
    },
    ["Thorns Aura"] = {
        ["Wyrmkin Dreamwalker"] = 7966,
        ["Razorfen Thornweaver"] = 7966,
        ["Razorfen Spearhide"] = 8148,
    },
    ["Tranquilizing Shot"] = {
        ["HUNTER"] = 19801,
    },
    ["Blast Wave"] = {
        ["MAGE"] = 13021,
    },
    ["Sentry Totem"] = {
        ["SHAMAN"] = 6495,
    },
    ["Radiation"] = {
        ["Irradiated Invader"] = 9798,
        ["Irradiated Pillager"] = 9798,
    },
    ["Dynamite"] = {
        ["Venture Co. Strip Miner"] = 8800,
    },
    ["Glacial Roar"] = {
        ["Vagash"] = 3143,
    },
    ["Cone of Cold"] = {
        ["MAGE"] = 10161,
    },
    ["Will of Hakkar"] = {
        ["Gurubashi"] = 24178,
    },
    ["Enchant Chest - Minor Stats"] = {
        Professions = 13626,
    },
    ["Flow of the Northspring"] = {
        ["Northspring Harpy"] = 11014,
        ["Northspring Roguefeather"] = 11014,
        ["Northspring Slayer"] = 11014,
        ["Northspring Windcaller"] = 11014,
    },
    ["Create Heart of Hakkar Rift"] = {
        ["Heart of Hakkar"] = 24202,
    },
    ["Runecloth Gloves"] = {
        Professions = 18417,
    },
    ["Iron Shield Spike"] = {
        Professions = 7221,
    },
    ["Prayer of Fortitude"] = {
        ["PRIEST"] = 21564,
    },
    ["Astral Recall"] = {
        ["SHAMAN"] = 556,
    },
    ["Scorpid Poison"] = {
        Pet = 24587,
    },
    ["Flarecore Leggings"] = {
        Professions = 23667,
    },
    ["Goblin Sapper Charge"] = {
        Professions = 12760,
    },
    ["Runed Golden Rod"] = {
        Professions = 13628,
    },
    ["Scarshield Portal"] = {
        ["Scarshield Portal"] = 15125,
        ["Scarshield Warlock"] = 15125,
    },
    ["Toxic Bolt"] = {
        ["Blighted Surge"] = 16554,
    },
    ["Discolored Healing Potion"] = {
        Professions = 4508,
    },
    ["Wail of Nightlash"] = {
        ["Nightlash"] = 3485,
    },
    ["Ironforge Breastplate"] = {
        Professions = 8367,
    },
    ["Nature Protection Potion"] = {
        Professions = 7259,
    },
    ["Tail Sweep"] = {
        ["Ysondre"] = 15847,
        ["Lethon"] = 15847,
        ["Emeriss"] = 15847,
        ["Taerar"] = 15847,
    },
    ["Prayer of Shadow Protection"] = {
        ["PRIEST"] = 27683,
    },
    ["Summon Wolfguard Worg"] = {
        ["Wolfguard Worg"] = 7107,
        ["Shadowfang Wolfguard"] = 7107,
    },
    ["Holiday - Valentine, gift given"] = {
        ["Kwee Q. Peddlefeet"] = 27663,
    },
    ["Arcane Bubble"] = {
        ["Arcanist Doan"] = 9438,
    },
    ["Corpse Explosion"] = {
        ["Scholomance Necromancer"] = 17616,
    },
    ["Masterwork Target Dummy"] = {
        Professions = 19814,
    },
    ["Smelt Thorium"] = {
        Professions = 16153,
    },
    ["Enraged Felguard Spawn"] = {
        ["Enraged Felguard"] = 22393,
    },
    ["Minor Mana Potion"] = {
        Professions = 2331,
    },
    ["Runed Copper Breastplate"] = {
        Professions = 2667,
    },
    ["Crash of Waves"] = {
        ["Gazban"] = 5403,
    },
    ["Dusky Bracers"] = {
        Professions = 9201,
    },
    ["Fireball Volley"] = {
        ["Aerie Gryphon"] = 15285,
        ["War Rider"] = 15285,
        ["Twilight Fire Guard"] = 15243,
    },
    ["Copper Dagger"] = {
        Professions = 8880,
    },
    ["Eye of Kilrogg"] = {
        ["WARLOCK"] = 126,
    },
    ["Enchant Cloak - Fire Resistance"] = {
        Professions = 13657,
    },
    ["Summon Crawler"] = {
        ["Crawler"] = 8656,
        ["Murloc Hunter"] = 8656,
    },
    ["Mind Flay"] = {
        ["PRIEST"] = 18807,
    },
    ["Enchant Chest - Lesser Mana"] = {
        Professions = 7776,
    },
    ["Lil' Smoky"] = {
        Professions = 15633,
    },
    ["Curse of the Firebrand"] = {
        ["Firebrand Darkweaver"] = 16071,
        ["Firebrand Dreadweaver"] = 16071,
    },
    ["Shadow Goggles"] = {
        Professions = 3940,
    },
    ["Battle Command"] = {
        ["Defias Overseer"] = 5115,
        ["Hillsbrad Foreman"] = 5115,
        ["Dark Iron Taskmaster"] = 5115,
        ["Nethergarde Foreman"] = 5115,
        ["Gurubashi Bat Rider"] = 5115,
    },
    ["Enchant Chest - Lesser Health"] = {
        Professions = 7748,
    },
    ["Arcane Bolt"] = {
        ["Fellicent's Shade"] = 13901,
    },
    ["Ebon Shiv"] = {
        Professions = 10013,
    },
    ["Runecloth Cloak"] = {
        Professions = 18409,
    },
    ["Guardian Belt"] = {
        Professions = 3775,
    },
    ["Wild Leather Shoulders"] = {
        Professions = 10529,
    },
    ["Blessing of Thule"] = {
        ["Ravenclaw Drudger"] = 3269,
    },
    ["Great Rage Potion"] = {
        Professions = 6618,
    },
    ["Dive"] = {
        Pet = 23148,
    },
    ["Summon Prismatic Exiles"] = {
        ["Prismatic Exile"] = 4937,
        ["Myzrael"] = 4937,
    },
    ["Red Mageweave Shoulders"] = {
        Professions = 12078,
    },
    ["Elixir of Giant Growth"] = {
        Professions = 8240,
    },
    ["Enchant Weapon - Strength"] = {
        Professions = 23799,
    },
    ["Heavy Woolen Cloak"] = {
        Professions = 3844,
    },
    ["Silver Contact"] = {
        Professions = 3973,
    },
    ["Helm of Fire"] = {
        Professions = 10632,
    },
    ["Mooncloth Bag"] = {
        Professions = 18445,
    },
    ["Barbaric Shoulders"] = {
        Professions = 7151,
    },
    ["Greater Healing Potion"] = {
        Professions = 7181,
    },
    ["Smite Slam"] = {
        ["Mr. Smite"] = 6435,
    },
    ["Frost Ward"] = {
        ["MAGE"] = 28609,
    },
    ["Disease Cleansing Totem"] = {
        ["SHAMAN"] = 8170,
    },
    ["Azure Silk Belt"] = {
        Professions = 8766,
    },
    ["Bolt of Mageweave"] = {
        Professions = 3865,
    },
    ["Trick or Treat"] = {
        ["Innkeeper Farley"] = 24751,
        ["Innkeeper Belm"] = 24751,
        ["Innkeeper Helbrek"] = 24751,
        ["Innkeeper Anderson"] = 24751,
        ["Innkeeper Shay"] = 24751,
        ["Innkeeper Boorand Plainswind"] = 24751,
        ["Innkeeper Firebrew"] = 24751,
        ["Innkeeper Renee"] = 24751,
        ["Innkeeper Thulbek"] = 24751,
        ["Innkeeper Janene"] = 24751,
        ["Innkeeper Brianna"] = 24751,
        ["Innkeeper Hearthstove"] = 24751,
        ["Innkeeper Saelienne"] = 24751,
        ["Innkeeper Keldamyr"] = 24751,
        ["Innkeeper Shaussiy"] = 24751,
        ["Innkeeper Kimlya"] = 24751,
        ["Innkeeper Bates"] = 24751,
        ["Innkeeper Allison"] = 24751,
        ["Innkeeper Norman"] = 24751,
        ["Innkeeper Pala"] = 24751,
        ["Innkeeper Kauth"] = 24751,
        ["Innkeeper Trelayne"] = 24751,
        ["Innkeeper Wiley"] = 24751,
        ["Innkeeper Skindle"] = 24751,
        ["Innkeeper Grosk"] = 24751,
        ["Innkeeper Gryshka"] = 24751,
        ["Innkeeper Karakul"] = 24751,
        ["Innkeeper Byula"] = 24751,
        ["Innkeeper Jayka"] = 24751,
        ["Innkeeper Fizzgrimble"] = 24751,
        ["Innkeeper Shyria"] = 24751,
        ["Innkeeper Greul"] = 24751,
        ["Innkeeper Thulfram"] = 24751,
        ["Innkeeper Heather"] = 24751,
        ["Innkeeper Shul'kar"] = 24751,
        ["Innkeeper Adegwa"] = 24751,
        ["Innkeeper Lyshaerya"] = 24751,
        ["Innkeeper Sikewa"] = 24751,
        ["Innkeeper Abeqwa"] = 24751,
        ["Innkeeper Vizzie"] = 24751,
        ["Innkeeper Kaylisk"] = 24751,
        ["Lard"] = 24751,
        ["Calandrath"] = 24751,
        ["Innkeeper Faralia"] = 24751,
    },
    ["Glacial Gloves"] = {
        Professions = 28205,
    },
    ["Elixir of Demonslaying"] = {
        Professions = 11477,
    },
    ["Practice Lock"] = {
        Professions = 8334,
    },
    ["Alarm-O-Bot"] = {
        Professions = 23096,
    },
    ["Goblin Rocket Helmet"] = {
        Professions = 12758,
    },
    ["Whirling Barrage"] = {
        ["Razorfen Spearhide"] = 8259,
    },
    ["Conjure Food"] = {
        ["MAGE"] = 28612,
    },
    ["Crafted Solid Shot"] = {
        Professions = 3947,
    },
    ["Overpower"] = {
        ["WARRIOR"] = 11585,
    },
    ["Red Mageweave Vest"] = {
        Professions = 12056,
    },
    ["Life Harvest"] = {
        ["Maggot Eye"] = 3243,
    },
    ["Stylish Red Shirt"] = {
        Professions = 3866,
    },
    ["Mooncloth Leggings"] = {
        Professions = 18440,
    },
    ["Greater Shadow Protection Potion"] = {
        Professions = 17578,
    },
    ["Enchant Weapon - Winter's Might"] = {
        Professions = 21931,
    },
    ["Runed Arcanite Rod"] = {
        Professions = 20051,
    },
    ["Mage Armor"] = {
        ["MAGE"] = 22783,
    },
    ["Berserker Rage"] = {
        ["WARRIOR"] = 18499,
    },
    ["Scorching Totem"] = {
        ["Scorching Totem"] = 15038,
    },
    ["Fel Stomp"] = {
        ["Fel Steed"] = 7139,
    },
    ["Brilliant Mana Oil"] = {
        Professions = 25130,
    },
    ["Ice Tomb"] = {
        ["Lokholar the Ice Lord"] = 16869,
    },
    ["Muculent Fever"] = {
        ["Muculent Ooze"] = 14130,
    },
    ["Fatigued"] = {
        ["Ravenclaw Drudger"] = 3271,
    },
    ["Embossed Leather Cloak"] = {
        Professions = 2162,
    },
    ["Shadoweave Pants"] = {
        Professions = 12052,
    },
    ["Thorium Tube"] = {
        Professions = 19795,
    },
    ["Gnomish Universal Remote"] = {
        Professions = 9269,
    },
    ["Teleport: Moonglade"] = {
        ["DRUID"] = 18960,
    },
    ["Enchant Cloak - Defense"] = {
        Professions = 13635,
    },
    ["Rocket, YELLOW"] = {
        ["Pat's Firework Guy - YELLOW"] = 26349,
    },
    ["Polymorph Backfire"] = {
        ["Polymorph Clone"] = 28406,
        ["Spitelash Warrior"] = 28406,
        ["Spitelash Screamer"] = 28406,
        ["Spitelash Serpent Guard"] = 28406,
        ["Spitelash Siren"] = 28406,
    },
    ["Beast Lore"] = {
        ["HUNTER"] = 1462,
    },
    ["Crusader Strike"] = {
        ["Scarlet Commander Mograine"] = 14518,
        ["Scarlet Gallant"] = 14517,
    },
    ["Shadow Ward"] = {
        ["WARLOCK"] = 28610,
    },
    ["Pet Bombling"] = {
        Professions = 15628,
    },
    ["Core Felcloth Bag"] = {
        Professions = 26087,
    },
    ["Paranoia"] = {
        Pet = 19480,
    },
    ["Remove Curse"] = {
        ["DRUID"] = 2782,
    },
    ["Searing Totem"] = {
        ["SHAMAN"] = 10438,
    },
    ["Compact Harvest Reaper"] = {
        ["Compact Harvest Reaper"] = 7979,
        ["Venture Co. Machine Smith"] = 7979,
        ["Engineer Whirleygig"] = 7979,
    },
    ["Catseye Ultra Goggles"] = {
        Professions = 12607,
    },
    ["Death & Decay"] = {
        ["Death's Head Cultist"] = 11433,
    },
    ["Barkskin"] = {
        ["DRUID"] = 22812,
    },
    ["Surprise Attack"] = {
        ["Crag Stalker"] = 8151,
    },
    ["Sweeping Strikes"] = {
        ["WARRIOR"] = 12292,
        ["Risen Construct"] = 18765,
        ["Drek'Thar"] = 18765,
    },
    ["Elixir of Detect Demon"] = {
        Professions = 11478,
    },
    ["Summon Swamp Spirit"] = {
        ["Swamp Spirit"] = 9636,
        ["Swamp Talker"] = 9636,
    },
    ["Druid's Potion"] = {
        ["Disciple of Naralex"] = 8141,
    },
    ["Dispel Magic"] = {
        ["PRIEST"] = 988,
    },
    ["Maggot Goo"] = {
        ["Carrion Devourer"] = 17197,
    },
    ["Instant Poison"] = {
        ["ROGUE"] = 8681,
    },
    ["Dazzling Mithril Rapier"] = {
        Professions = 10005,
    },
    ["Orange Martial Shirt"] = {
        Professions = 12064,
    },
    ["Greater Blessing of Might"] = {
        ["PALADIN"] = 25916,
    },
    ["Barbaric Harness"] = {
        Professions = 6661,
    },
    ["Summon Infernal Servant"] = {
        ["Infernal Servant"] = 12740,
        ["Lady Sevine"] = 12740,
    },
    ["Vicious Bite"] = {
        ["Ancient Core Hound"] = 19319,
        ["Grimmaw"] = 19319,
    },
    ["Dawn Treaders"] = {
        Professions = 23705,
    },
    ["Summon Rusty Chest"] = {
        ["Rotting Slime"] = 6464,
    },
    ["Aural Shock"] = {
        ["Dark Screecher"] = 14538,
    },
    ["Flame Buffet Totem"] = {
        ["Flame Buffet Totem"] = 15867,
        ["Smolderthorn Witch Doctor"] = 15867,
    },
    ["Trample"] = {
        ["Bellygrub"] = 5568,
        ["Foe Reaper 4000"] = 5568,
        ["Gorlash"] = 5568,
        ["Timberling Trampler"] = 5568,
        ["Stone Fury"] = 5568,
        ["Fozruk"] = 5568,
        ["Kodo Bull"] = 5568,
        ["Kodo Matriarch"] = 5568,
        ["Gelkis Stamper"] = 5568,
        ["Ghamoo-ra"] = 5568,
        ["Heavy War Golem"] = 5568,
        ["Servant of Arkkoroc"] = 5568,
    },
    ["Guardian Leather Bracers"] = {
        Professions = 3777,
    },
    ["Greater Fire Protection Potion"] = {
        Professions = 17574,
    },
    ["Shared Bonds"] = {
        ["Ravenclaw Slave"] = 7761,
    },
    ["Taelan's Suffering Effect"] = {
        ["Highlord Taelan Fordring"] = 18811,
    },
    ["Bottomless Bag"] = {
        Professions = 18455,
    },
    ["Thunderclap"] = {
        ["Overlord Ramtusk"] = 15548,
        ["Razorfen Quilguard"] = 15548,
        ["Vanndar Stormpike"] = 15588,
        ["Greater Thunderhawk"] = 8078,
        ["Thunderhawk Cloudscraper"] = 8078,
        ["Raging Cliff Stormer"] = 8078,
        ["Ironforge Guard"] = 8078,
        ["Warlord Kolkanis"] = 8078,
        ["Torek"] = 8078,
        ["Mosh'Ogg Warmonger"] = 8147,
        ["Cliff Thunderer"] = 8147,
        ["Umi Thorson"] = 8147,
    },
    ["Dark Iron Taskmaster Death"] = {
        ["Dark Iron Taskmaster"] = 12613,
    },
    ["Torch Burst"] = {
        ["Syndicate Watchman"] = 3602,
        ["Jailor Eston"] = 3602,
        ["Dark Strand Excavator"] = 3602,
    },
    ["Seal of the Crusader"] = {
        ["PALADIN"] = 20308,
    },
    ["Fletcher's Gloves"] = {
        Professions = 9145,
    },
    ["Faerie Fire (Feral)"] = {
        ["DRUID"] = 17392,
    },
    ["Enchant Bracer - Greater Stamina"] = {
        Professions = 13945,
    },
    ["Cookie's Cooking"] = {
        ["Cookie"] = 5174,
    },
    ["Stormstrike"] = {
        ["SHAMAN"] = 17364,
    },
    ["Mirkfallon Fungus"] = {
        ["Mirkfallon Keeper"] = 8138,
    },
    ["Terrifying Screech"] = {
        ["Death Singer"] = 6605,
        ["Plaguebat"] = 6605,
        ["Pterrordax"] = 6605,
        ["Dreamtracker"] = 6605,
    },
    ["Restorative Potion"] = {
        Professions = 11452,
    },
    ["Riposte"] = {
        ["ROGUE"] = 14251,
    },
    ["Warosh's Transform"] = {
        ["Warosh"] = 16801,
    },
    ["Flamecrack"] = {
        ["Bloodaxe Evoker"] = 15743,
        ["Volchan"] = 15743,
    },
    ["Mithril Scale Shoulders"] = {
        Professions = 9966,
    },
    ["Windwall Totem"] = {
        ["SHAMAN"] = 15112,
    },
    ["Muscle Tear"] = {
        ["Greater Fleshripper"] = 12166,
        ["Fleshripper"] = 12166,
        ["Feral Nightsaber"] = 12166,
        ["Reef Crawler"] = 12166,
        ["Dreadmaw Crocolisk"] = 12166,
        ["Bristleback Interloper"] = 12166,
        ["The Rake"] = 12166,
    },
    ["Enchant Gloves - Mining"] = {
        Professions = 13612,
    },
    ["Runed Silver Rod"] = {
        Professions = 7795,
    },
    ["Cindercloth Cloak"] = {
        Professions = 18418,
    },
    ["Sylvan Vest"] = {
        Professions = 28480,
    },
    ["Rocket, WHITE"] = {
        ["Pat's Firework Guy - WHITE"] = 26348,
    },
    ["Lunaclaw Spirit"] = {
        ["Lunaclaw Spirit"] = 18986,
        ["Lunaclaw"] = 18986,
    },
    ["Death Wish"] = {
        ["WARRIOR"] = 12328,
    },
    ["Transmute: Undeath to Water"] = {
        Professions = 17563,
    },
    ["Smite Stomp"] = {
        ["Mr. Smite"] = 6432,
    },
    ["Coarse Sharpening Stone"] = {
        Professions = 2665,
    },
    ["Levitate"] = {
        ["PRIEST"] = 1706,
    },
    ["Shred"] = {
        ["DRUID"] = 9830,
        ["Vile Fin Shredder"] = 3252,
    },
    ["Agamaggan's Strength"] = {
        ["Mangletooth"] = 16612,
    },
    ["Spirit of the Wind"] = {
        ["Mangletooth"] = 16618,
    },
    ["Fiery Chain Girdle"] = {
        Professions = 20872,
    },
    ["Living Leggings"] = {
        Professions = 19078,
    },
    ["Copper Chain Boots"] = {
        Professions = 3319,
    },
    ["Mooncloth"] = {
        Professions = 18560,
    },
    ["Bronze Mace"] = {
        Professions = 2740,
    },
    ["Consecration"] = {
        ["PALADIN"] = 20924,
    },
    ["Machine Gun"] = {
        ["Mechano-Tank"] = 10346,
    },
    ["Fine Leather Cloak"] = {
        Professions = 2159,
    },
    ["Track Giants"] = {
        ["HUNTER"] = 19882,
    },
    ["Heavy Scorpid Helm"] = {
        Professions = 19088,
    },
    ["Summon Harvester Swarm"] = {
        ["Harvester Swarm"] = 7278,
        ["Silithid Harvester"] = 7278,
    },
    ["Super Shrink Ray"] = {
        ["High Tinker Mekkatorque"] = 22742,
    },
    ["Searing Flames"] = {
        ["Ambassador Infernus"] = 9552,
    },
    ["Commanding Shout"] = {
        ["Death Talon Captain"] = 22440,
    },
    ["Felcloth Shoulders"] = {
        Professions = 18453,
    },
    ["Chimeric Vest"] = {
        Professions = 19081,
    },
    ["Ground Tremor"] = {
        ["Stone Fury"] = 6524,
        ["Rumbling Exile"] = 6524,
        ["Fozruk"] = 6524,
        ["Rumbler"] = 6524,
        ["Thundering Boulderkin"] = 6524,
        ["Razorfen Groundshaker"] = 6524,
        ["Stone Steward"] = 6524,
        ["Atal'alarion"] = 6524,
        ["Avalanchion"] = 6524,
        ["Gri'lek"] = 6524,
        ["The Duke of Shards"] = 6524,
        ["Magma Lord Bokk"] = 6524,
    },
    ["Earthen Silk Belt"] = {
        Professions = 8797,
    },
    ["Overwhelming Stench"] = {
        ["Felmusk Satyr"] = 6942,
        ["Felmusk Rogue"] = 6942,
        ["Felmusk Felsworn"] = 6942,
        ["Felmusk Shadowstalker"] = 6942,
    },
    ["Vanish"] = {
        ["ROGUE"] = 1857,
    },
    ["Radiation Cloud"] = {
        ["Irradiated Slime"] = 10341,
    },
    ["Inlaid Mithril Cylinder"] = {
        Professions = 11454,
    },
    ["Cloud of Disease"] = {
        ["Diseased Ghoul"] = 17742,
    },
    ["Spawn Chromatic Drakonid"] = {
        ["Chromatic Drakonid"] = 22680,
        ["Black Drakonid Spawner"] = 22680,
        ["Red Drakonid Spawner"] = 22680,
        ["Green Drakonid Spawner"] = 22680,
        ["Bronze Drakonid Spawner"] = 22680,
        ["Blue Drakonid Spawner"] = 22680,
    },
    ["Runed Mithril Hammer"] = {
        Professions = 10009,
    },
    ["Handstitched Leather Belt"] = {
        Professions = 3753,
    },
    ["Major Recombobulator"] = {
        Professions = 23079,
    },
    ["Runic Leather Headband"] = {
        Professions = 19082,
    },
    ["Star Belt"] = {
        Professions = 3864,
    },
    ["Deadly Poison"] = {
        ["ROGUE"] = 2835,
        ["Venom Mist Lurker"] = 3583,
        ["Mazzranache"] = 3583,
        ["Venomous Cloud Serpent"] = 3583,
        ["Darkmist Recluse"] = 3583,
        ["Fardel Dabyrie"] = 3583,
    },
    ["Rake"] = {
        ["DRUID"] = 9904,
    },
    ["Sunder Armor"] = {
        ["WARRIOR"] = 11597,
        ["Makrinni Razorclaw"] = 13444,
        ["Drysnap Pincer"] = 13444,
        ["Quilguard Champion"] = 15572,
        ["Whitewhisker Digger"] = 15572,
        ["Wing Commander Guse"] = 15572,
        ["Wing Commander Jeztor"] = 15572,
        ["Wing Commander Mulverick"] = 15572,
        ["Coldmine Peon"] = 15572,
        ["Coldmine Miner"] = 15572,
        ["Irondeep Miner"] = 15572,
        ["Irondeep Peon"] = 15572,
        ["Officer Jaxon"] = 15572,
        ["Murloc Netter"] = 11971,
        ["Riverpaw Miner"] = 11971,
        ["Rot Hide Brute"] = 11971,
        ["Ironforge Guard"] = 11971,
        ["Murkdeep"] = 11971,
        ["Horde Deforester"] = 11971,
    },
    ["Cauterizing Flames"] = {
        ["Ancient Core Hound"] = 19366,
    },
    ["Warchief's Blessing"] = {
        ["Thrall"] = 16609,
        ["Herald of Thrall"] = 16609,
    },
    ["Manifestation Ends"] = {
        ["Spirit of Kolk"] = 21965,
        ["Spirit of Veng"] = 21965,
    },
    ["Blessing of Protection"] = {
        ["PALADIN"] = 10278,
    },
    ["Glacial Wrists"] = {
        Professions = 28209,
    },
    ["Bloodthirst"] = {
        ["WARRIOR"] = 23894,
    },
    ["Ornate Mithril Shoulders"] = {
        Professions = 9952,
    },
    ["Virulent Poison"] = {
        ["Tomb Reaver"] = 12251,
    },
    ["Weak Troll's Blood Potion"] = {
        Professions = 3170,
    },
    ["Deepdive Helmet"] = {
        Professions = 12617,
    },
    ["Ornate Mithril Boots"] = {
        Professions = 9979,
    },
    ["Infected Wound"] = {
        ["Whitewhisker Vermin"] = 17230,
        ["Nightbane Vile Fang"] = 3427,
        ["Gath'Ilzogg"] = 3427,
        ["Redridge Mongrel"] = 3427,
        ["Highland Razormaw"] = 3427,
        ["Mottled Razormaw"] = 3427,
        ["Sarltooth"] = 3427,
        ["Defias Captive"] = 3427,
        ["Diseased Black Bear"] = 3427,
        ["Encrusted Tide Crawler"] = 3427,
        ["Torn Fin Muckdweller"] = 3427,
        ["Large Loch Crocolisk"] = 3427,
        ["Hammerfall Grunt"] = 3427,
        ["Buzzard"] = 3427,
        ["Giant Buzzard"] = 3427,
        ["Deviate Creeper"] = 3427,
        ["Gravelsnout Vermin"] = 3427,
        ["Carrion Horror"] = 3427,
        ["Felpaw Wolf"] = 3427,
        ["Felpaw Scavenger"] = 3427,
        ["Felpaw Ravager"] = 3427,
    },
    ["Empower Will"] = {
        ["Spirestone Warlord"] = 16171,
    },
    ["Azure Silk Vest"] = {
        Professions = 3859,
    },
    ["Entangling Roots"] = {
        ["DRUID"] = 9853,
        ["Bristleback Thornweaver"] = 12747,
        ["Shadethicket Wood Shaper"] = 12747,
        ["Charred Ancient"] = 12747,
        ["Keeper Ordanus"] = 12747,
        ["Tar Lord"] = 12747,
        ["Elder Lake Creeper"] = 11922,
        ["Razorfen Thornweaver"] = 11922,
        ["Wandering Protector"] = 11922,
        ["Arch Druid Renferal"] = 22127,
        ["Druid of the Grove"] = 22127,
    },
    ["Pacify"] = {
        ["Peacekeeper Security Suit"] = 10730,
    },
    ["Blue Linen Robe"] = {
        Professions = 7633,
    },
    ["Enchanting"] = {
        Professions = 7412,
        Professions = 7413,
        Professions = 13920,
    },
    ["Cindercloth Pants"] = {
        Professions = 18434,
    },
    ["Imperial Plate Boots"] = {
        Professions = 16657,
    },
    ["Charged Arcane Bolt"] = {
        ["Prince Raze"] = 16570,
    },
    ["Shadow Word: Pain"] = {
        ["PRIEST"] = 10894,
        ["Vol'jin"] = 17146,
        ["Strashaz Siren"] = 15654,
        ["Coldmine Surveyor"] = 15654,
        ["Irondeep Surveyor"] = 15654,
        ["Veteran Coldmine Surveyor"] = 15654,
        ["Veteran Irondeep Surveyor"] = 15654,
        ["Champion Irondeep Surveyor"] = 15654,
        ["Xabraxxis"] = 11639,
    },
    ["Heavy Mithril Axe"] = {
        Professions = 9993,
    },
    ["Hellfire"] = {
        ["WARLOCK"] = 11684,
    },
    ["Enchant Weapon - Demonslaying"] = {
        Professions = 13915,
    },
    ["Flames of Shahram"] = {
        ["Shahram"] = 16596,
    },
    ["The Shatterer"] = {
        Professions = 10003,
    },
    ["Crystal Flash"] = {
        ["Ironjaw Basilisk"] = 5106,
        ["Scale Belly"] = 5106,
        ["Glasshide Basilisk"] = 5106,
        ["Redstone Crystalhide"] = 5106,
    },
    ["Ice Blast"] = {
        ["Mechano-Frostwalker"] = 11264,
    },
    ["Enchant Shield - Lesser Block"] = {
        Professions = 13689,
    },
    ["Detect Lesser Invisibility"] = {
        ["WARLOCK"] = 132,
    },
    ["Mighty Iron Hammer"] = {
        Professions = 3297,
    },
    ["Brightcloth Gloves"] = {
        Professions = 18415,
    },
    ["Curse of Mending"] = {
        ["Dark Strand Fanatic"] = 7098,
        ["Witchwing Harpy"] = 7098,
    },
    ["Kidney Shot"] = {
        ["ROGUE"] = 8643,
    },
    ["Copper Mace"] = {
        Professions = 2737,
    },
    ["Call Reavers"] = {
        ["Gordok Reaver"] = 22860,
    },
    ["Enveloping Web"] = {
        ["Crypt Crawler"] = 15471,
        ["Nerubian Overseer"] = 15471,
    },
    ["Shadowstalker Slash"] = {
        ["Bleakheart Shadowstalker"] = 6927,
    },
    ["Summon Dupe Bug"] = {
        ["Dupe Bug"] = 10858,
        ["Techbot"] = 10858,
        ["Dupe Bug"] = 10858,
    },
    ["Naraxis Web"] = {
        ["Naraxis"] = 3542,
    },
    ["Net"] = {
        ["Blackrock Outrunner"] = 6533,
        ["Tharil'zun"] = 6533,
        ["Bloodscalp Hunter"] = 6533,
        ["Kurzen Wrangler"] = 6533,
        ["Mosshide Trapper"] = 6533,
        ["Dragonmaw Raider"] = 6533,
        ["Splinter Fist Enslaver"] = 6533,
        ["Bloodsail Raider"] = 6533,
        ["Lady Moongazer"] = 6533,
        ["Jailor Marlgen"] = 6533,
        ["Jailor Borhuin"] = 6533,
        ["Witherbark Headhunter"] = 6533,
        ["Daggerspine Raider"] = 6533,
        ["Barak Kodobane"] = 6533,
        ["Galak Wrangler"] = 6533,
        ["Razorfen Warden"] = 6533,
        ["Magram Wrangler"] = 6533,
        ["Peacekeeper Security Suit"] = 6533,
        ["Gelihast"] = 6533,
        ["Boss Copperplug"] = 6533,
        ["Murkdeep"] = 6533,
        ["Raider Jhash"] = 6533,
        ["Galak Assassin"] = 6533,
        ["Ruklar the Trapper"] = 12024,
        ["Defias Trapper"] = 12024,
        ["Murloc Netter"] = 12024,
        ["Bluegill Raider"] = 12024,
        ["Greymist Netter"] = 12024,
        ["Kolkar Wrangler"] = 12024,
        ["Ratchet Bruiser"] = 12024,
        ["Booty Bay Bruiser"] = 12024,
        ["Snagglespear"] = 12024,
        ["Gadgetzan Bruiser"] = 12024,
    },
    ["Dusky Leather Leggings"] = {
        Professions = 9195,
    },
    ["Voice Amplification Modulator"] = {
        Professions = 19819,
    },
    ["Summon Kurzen Mindslave"] = {
        ["Kurzen Mindslave"] = 8813,
        ["Mogh the Undying"] = 8813,
    },
    ["Smelt Tin"] = {
        Professions = 3304,
    },
    ["Dragonbane"] = {
        ["Blackwing Legionnaire"] = 23967,
        ["Death Talon Dragonspawn"] = 23967,
        ["Blackwing Guardsman"] = 23967,
    },
    ["Savannah Cub"] = {
        ["Savannah Cub"] = 6598,
        ["Savannah Matriarch"] = 6598,
    },
    ["Chimeric Boots"] = {
        Professions = 19063,
    },
    ["Green Iron Boots"] = {
        Professions = 3334,
    },
    ["Brightcloth Cloak"] = {
        Professions = 18420,
    },
    ["Retribution Aura"] = {
        ["PALADIN"] = 10301,
        ["Highlord Bolvar Fordragon"] = 8990,
        ["Scarlet Commander Mograine"] = 8990,
    },
    ["Rocket, GREEN"] = {
        ["Pat's Firework Guy - GREEN"] = 26345,
    },
    ["Fixate"] = {
        ["Enthralled Atal'ai"] = 12021,
        ["Jademir Boughguard"] = 12021,
        ["Rage Talon Dragonspawn"] = 12021,
        ["Risen Aberration"] = 12021,
        ["Emeraldon Boughguard"] = 12021,
        ["Verdantine Boughguard"] = 12021,
    },
    ["Debilitating Touch"] = {
        ["Hungering Wraith"] = 16333,
        ["Restless Shade"] = 16333,
        ["Spectral Citizen"] = 16333,
        ["Decrepit Guardian"] = 16333,
    },
    ["Heavy Grinding Stone"] = {
        Professions = 3337,
    },
    ["Elixir of Lion's Strength"] = {
        Professions = 2329,
    },
    ["Create Eggscilliscope"] = {
        ["Tinkee Steamboil"] = 16029,
    },
    ["Auto Shot"] = {
        ["HUNTER"] = 75,
    },
    ["Summon Warhorse"] = {
        ["PALADIN"] = 13819,
    },
    ["Oil of Immolation"] = {
        Professions = 11451,
    },
    ["Flarecore Robe"] = {
        Professions = 23666,
    },
    ["Holy Shock"] = {
        ["PALADIN"] = 20930,
    },
    ["Screams of the Past"] = {
        ["Wailing Guardsman"] = 7074,
        ["Wrath Phantom"] = 7074,
    },
    ["Enchant Azure Agate"] = {
        ["Apprentice Kryten"] = 4319,
    },
    ["Conjure E.C.A.C"] = {
        ["Wrenix's Gizmotronic Apparatus"] = 9977,
    },
    ["Gyrofreeze Ice Reflector"] = {
        Professions = 23077,
    },
    ["Lay on Hands"] = {
        ["PALADIN"] = 10310,
        ["Scarlet Commander Mograine"] = 9257,
    },
    ["Enchant Shield - Greater Stamina"] = {
        Professions = 20017,
    },
    ["Send Fray Combatant"] = {
        ["Twiggy Flathead"] = 8645,
    },
    ["Medium Armor Kit"] = {
        Professions = 2165,
    },
    ["Brown Linen Vest"] = {
        Professions = 2385,
    },
    ["Shell Shield"] = {
        Pet = 26064,
    },
    ["Magic Resistance Potion"] = {
        Professions = 11453,
    },
    ["Thick Leather Ammo Pouch"] = {
        Professions = 14932,
    },
    ["Enchant Weapon - Greater Striking"] = {
        Professions = 13943,
    },
    ["Call Bleak Worg"] = {
        ["Bleak Worg"] = 7487,
    },
    ["Detonation"] = {
        ["Arcanist Doan"] = 9435,
        ["Dark Iron Land Mine"] = 4043,
    },
    ["Sayge's Dark Fortune of Strength"] = {
        ["Sayge"] = 23735,
    },
    ["Linen Belt"] = {
        Professions = 8776,
    },
    ["Golden Skeleton Key"] = {
        Professions = 19667,
    },
    ["Unending Breath"] = {
        ["WARLOCK"] = 5697,
    },
    ["Swiftmend"] = {
        ["DRUID"] = 18562,
    },
    ["Silithid Swarm"] = {
        ["Silithid Swarm"] = 6589,
        ["Silithid Swarmer"] = 6589,
        ["Hazzali Swarmer"] = 6589,
        ["Centipaar Swarmer"] = 6589,
        ["Silithid Swarm"] = 10722,
        ["Silithid Swarmer"] = 10722,
        ["Hazzali Swarmer"] = 10722,
        ["Centipaar Swarmer"] = 10722,
    },
    ["Dusky Belt"] = {
        Professions = 9206,
    },
    ["Hillman's Cloak"] = {
        Professions = 3760,
    },
    ["Arcing Smash"] = {
        ["Burning Blade Reaver"] = 8374,
        ["Twilight Reaver"] = 8374,
        ["Caverndeep Reaver"] = 8374,
    },
    ["Greater Blessing of Sanctuary"] = {
        ["PALADIN"] = 25899,
    },
    ["Sylvan Shoulders"] = {
        Professions = 28482,
    },
    ["Ward of Laze effect"] = {
        ["Ward of Laze"] = 3826,
    },
    ["Soft-soled Linen Boots"] = {
        Professions = 3845,
    },
    ["Stylish Green Shirt"] = {
        Professions = 7893,
    },
    ["Frailty"] = {
        ["Soulless Ghoul"] = 12530,
        ["Zul'Lor"] = 12530,
    },
    ["Raptor Hide Belt"] = {
        Professions = 4097,
    },
    ["Rip"] = {
        ["DRUID"] = 9896,
    },
    ["Blade Flurry"] = {
        ["ROGUE"] = 13877,
    },
    ["Enchant Gloves - Greater Strength"] = {
        Professions = 20013,
    },
    ["Dreamweave Circlet"] = {
        Professions = 12092,
    },
    ["Purity"] = {
        ["Charlga Razorflank"] = 8361,
    },
    ["Standard Scope"] = {
        Professions = 3978,
    },
    ["Aimed Shot"] = {
        ["HUNTER"] = 20904,
    },
    ["System Shock"] = {
        ["Emissary Roman'khan"] = 23774,
    },
    ["Gaea's Embrace"] = {
        Professions = 28210,
    },
    ["Truesilver Champion"] = {
        Professions = 10015,
    },
    ["Summon Darkreaver's Fallen Charger"] = {
        ["Darkreaver's Fallen Charger"] = 23261,
        ["Death Knight Darkreaver"] = 23261,
    },
    ["Mighty Troll's Blood Potion"] = {
        Professions = 3451,
    },
    ["Enchant 2H Weapon - Lesser Spirit"] = {
        Professions = 13380,
    },
    ["Blind"] = {
        ["ROGUE"] = 2094,
    },
    ["Curse of Tongues"] = {
        ["WARLOCK"] = 11719,
    },
    ["Solid Dynamite"] = {
        Professions = 12586,
    },
    ["Starshards"] = {
        ["PRIEST"] = 19305,
    },
    ["Truesilver Breastplate"] = {
        Professions = 9974,
    },
    ["Rocket, RED"] = {
        ["Stormwind Reveler"] = 26347,
        ["Thunder Bluff Reveler"] = 26347,
        ["Pat's Firework Guy - RED"] = 26347,
        ["Darnassus Reveler"] = 26347,
        ["Ironforge Reveler"] = 26347,
        ["Undercity Reveler"] = 26347,
        ["Orgrimmar Reveler"] = 26347,
    },
    ["Mana Spring Totem"] = {
        ["SHAMAN"] = 10497,
    },
    ["Summon Lava Burst I"] = {
        ["Core Rat"] = 21907,
    },
    ["Argent Shoulders"] = {
        Professions = 23665,
    },
    ["Large Blue Rocket Cluster"] = {
        Professions = 26426,
    },
    ["Felcloth Bag"] = {
        Professions = 26086,
    },
    ["Resurrection"] = {
        ["PRIEST"] = 20770,
    },
    ["Greater Polymorph"] = {
        ["Blackwing Spellbinder"] = 22274,
        ["Grethok the Controller"] = 22274,
    },
    ["Gem-studded Leather Belt"] = {
        Professions = 3778,
    },
    ["Elixir of Giants"] = {
        Professions = 11472,
    },
    ["Ice Deflector"] = {
        Professions = 3957,
    },
    ["Azure Silk Gloves"] = {
        Professions = 3854,
    },
    ["Dire Growl"] = {
        ["Bloodhound"] = 13692,
        ["Diemetradon"] = 13692,
        ["Bloodshot"] = 13692,
    },
    ["Enchant Chest - Health"] = {
        Professions = 7857,
    },
    ["Enchant Shield - Spirit"] = {
        Professions = 13659,
    },
    ["Runecloth Pants"] = {
        Professions = 18438,
    },
    ["Holy Strike"] = {
        ["Scarlet Champion"] = 17143,
        ["Dun Garok Mountaineer"] = 13953,
        ["Lieutenant Valorcall"] = 13953,
        ["Scarlet Preserver"] = 13953,
    },
    ["Improved Blocking"] = {
        ["Defias Henchman"] = 3248,
        ["Captain Vachon"] = 3248,
        ["Ravenclaw Guardian"] = 3248,
        ["Stormscale Warrior"] = 3248,
        ["Dun Garok Mountaineer"] = 3248,
        ["Razormane Battleguard"] = 3248,
        ["Lieutenant Benedict"] = 3248,
        ["Razorfen Defender"] = 3248,
        ["Nethergarde Soldier"] = 3248,
        ["Dragonmaw Centurion"] = 3419,
        ["Kam Deepfury"] = 3419,
        ["Hillsbrad Footman"] = 3419,
        ["Kurzen Jungle Fighter"] = 3639,
        ["Stromgarde Defender"] = 3639,
        ["Scarlet Soldier"] = 3639,
        ["Scarlet Defender"] = 3639,
        ["Refuge Pointe Defender"] = 3639,
    },
    ["Portal: Ironforge"] = {
        ["MAGE"] = 11416,
    },
    ["Thick Leather"] = {
        Professions = 20650,
    },
    ["Summon Broken Cadaver"] = {
        ["Broken Cadaver"] = 16324,
        ["Ravaged Cadaver"] = 16324,
    },
    ["Wolfshead Helm"] = {
        Professions = 10621,
    },
    ["Eagle Eye"] = {
        ["HUNTER"] = 6197,
    },
    ["Corrosive Acid Breath"] = {
        ["Somnus"] = 20667,
        ["Gyth"] = 16359,
    },
    ["Greater Blessing of Kings"] = {
        ["PALADIN"] = 25898,
    },
    ["Coarse Dynamite"] = {
        Professions = 3931,
        ["Ashlan Stonesmirk"] = 9004,
        ["Ashlan Stonesmirk"] = 9002,
        ["Ashlan Stonesmirk"] = 9003,
        ["Ashlan Stonesmirk"] = 9009,
    },
    ["Lotwil's Summoning"] = {
        ["Lotwil Veriatus"] = 5001,
    },
    ["Dawnbringer Shoulders"] = {
        Professions = 16660,
    },
    ["Tune Up"] = {
        ["Leprous Technician"] = 10348,
        ["Leprous Machinesmith"] = 10348,
    },
    ["Green Dragonscale Leggings"] = {
        Professions = 19060,
    },
    ["Corrupted Stamina"] = {
        ["Foulweald Den Watcher"] = 6819,
        ["Oakpaw"] = 6819,
    },
    ["Insect Swarm"] = {
        ["DRUID"] = 24977,
    },
    ["Summon Fen Dweller"] = {
        ["Fen Dweller"] = 8857,
        ["Fen Lord"] = 8857,
    },
    ["Bloodvine Vest"] = {
        Professions = 24091,
    },
    ["Aspect of the Hawk"] = {
        ["HUNTER"] = 25296,
    },
    ["Fire Nova"] = {
        ["Bloodmage Thalnos"] = 12470,
        ["Magma Elemental"] = 11970,
        ["Mechano-Flamewalker"] = 11970,
        ["Taragaman the Hungerer"] = 11970,
        ["Bloodscalp Shaman"] = 11969,
        ["Magistrate Burnside"] = 11969,
    },
    ["Summon Hydroling"] = {
        ["Hydroling"] = 22714,
    },
    ["Hex of Ravenclaw"] = {
        ["Thule Ravenclaw"] = 7655,
        ["Thule Ravenclaw"] = 7656,
        ["Thule Ravenclaw"] = 7657,
    },
    ["Blessing of Sacrifice"] = {
        ["PALADIN"] = 20729,
    },
    ["Superior Healing Ward"] = {
        ["Superior Healing Ward"] = 15869,
        ["Smolderthorn Witch Doctor"] = 15869,
        ["Witherbark Speaker"] = 15869,
    },
    ["Skinning"] = {
        Professions = 8617,
        Professions = 8618,
        Professions = 10768,
    },
    ["Enchant Bracer - Intellect"] = {
        Professions = 13822,
    },
    ["Bloodpetal Poison"] = {
        ["Bloodpetal Lasher"] = 14110,
        ["Bloodpetal Flayer"] = 14110,
        ["Bloodpetal Thresher"] = 14110,
        ["Bloodpetal Trapper"] = 14110,
    },
    ["Aquadynamic Fish Attractor"] = {
        Professions = 9271,
    },
    ["Enchant Bracer - Lesser Deflection"] = {
        Professions = 13646,
    },
    ["Enchant Bracer - Superior Spirit"] = {
        Professions = 20009,
    },
    ["Massive Iron Axe"] = {
        Professions = 3498,
    },
    ["Omen of Clarity"] = {
        ["DRUID"] = 16864,
    },
    ["Creeping Doom"] = {
        ["Creeping Doom"] = 23589,
        ["Nelson the Nice"] = 23589,
    },
    ["Spawn Bronze Drakonid"] = {
        ["Bronze Drakonid"] = 22657,
        ["Bronze Drakonid Spawner"] = 22657,
    },
    ["Solid Blasting Powder"] = {
        Professions = 12585,
    },
    ["Flask of Chromatic Resistance"] = {
        Professions = 17638,
    },
    ["Create Weegli's Barrel"] = {
        ["Weegli Blastfuse"] = 10772,
    },
    ["Knock Away"] = {
        ["Son of Arkkoroc"] = 10101,
        ["Ursangous"] = 10101,
        ["Cyclonian"] = 18670,
        ["Molten Giant"] = 18945,
        ["Teremus the Devourer"] = 11130,
    },
    ["Dark Iron Plate"] = {
        Professions = 15296,
    },
    ["Melt Ore"] = {
        ["Goblin Craftsman"] = 5159,
    },
    ["Minor Mana Oil"] = {
        Professions = 25125,
    },
    ["Fire Shot"] = {
        ["Syndicate Saboteur"] = 6980,
        ["Scarlet Scout"] = 6979,
    },
    ["Exploit Weakness"] = {
        ["Fallenroot Rogue"] = 8355,
        ["Nightbane Dark Runner"] = 6595,
        ["Grave Robber"] = 6595,
        ["Defias Pillager"] = 6595,
        ["Moonstalker Sire"] = 6595,
        ["Witchwing Roguefeather"] = 6595,
        ["Witchwing Ambusher"] = 6595,
        ["Xavian Rogue"] = 6595,
        ["Felmusk Rogue"] = 6595,
        ["Screeching Roguefeather"] = 6595,
        ["Hatefury Shadowstalker"] = 6595,
    },
    ["Gust of Wind"] = {
        ["Dust Devil"] = 6982,
        ["Bloodfeather Wind Witch"] = 6982,
        ["Blackwood Windtalker"] = 6982,
        ["Windfury Wind Witch"] = 6982,
        ["Screeching Windcaller"] = 6982,
        ["Sister Rathtalon"] = 6982,
        ["Cyclonian"] = 6982,
        ["Gusting Vortex"] = 6982,
    },
    ["Ironfeather Shoulders"] = {
        Professions = 19062,
    },
    ["Enchant Bracer - Lesser Intellect"] = {
        Professions = 13622,
    },
    ["Demon Forged Breastplate"] = {
        Professions = 16667,
    },
    ["Explosive Sheep"] = {
        Professions = 3955,
    },
    ["Frost Protection Potion"] = {
        Professions = 7258,
    },
    ["Enchant Weapon - Minor Striking"] = {
        Professions = 7788,
    },
    ["Sayge's Dark Fortune of Spirit"] = {
        ["Sayge"] = 23738,
    },
    ["Razormane Wolf"] = {
        ["Razormane Wolf"] = 6479,
        ["Razormane Hunter"] = 6479,
    },
    ["Death's Door"] = {
        ["Injured Peasant"] = 23127,
        ["Plagued Peasant"] = 23127,
    },
    ["Barbaric Linen Vest"] = {
        Professions = 2395,
    },
    ["Shackle Undead"] = {
        ["PRIEST"] = 10955,
    },
    ["Heavy Leather Ball"] = {
        Professions = 23190,
    },
    ["Polymorph: Cow"] = {
        ["MAGE"] = 28270,
    },
    ["Wicked Leather Pants"] = {
        Professions = 19083,
    },
    ["Wild Leather Helmet"] = {
        Professions = 10546,
    },
    ["Truefaith Gloves"] = {
        Professions = 8782,
    },
    ["Huge Thorium Battleaxe"] = {
        Professions = 16971,
    },
    ["Spider Silk Slippers"] = {
        Professions = 3856,
    },
    ["Festive Red Pant Suit"] = {
        Professions = 26407,
    },
    ["Moonkin Form"] = {
        ["DRUID"] = 24858,
    },
    ["Fire Resistance Totem"] = {
        ["SHAMAN"] = 10538,
    },
    ["Thorium Grenade"] = {
        Professions = 19790,
    },
    ["Acid Breath"] = {
        ["Jade"] = 12533,
        ["Lord Captain Wyrmak"] = 12533,
    },
    ["Runed Copper Bracers"] = {
        Professions = 2664,
    },
    ["Green Leather Armor"] = {
        Professions = 3772,
    },
    ["Soothe Animal"] = {
        ["DRUID"] = 9901,
    },
    ["Flame Deflector"] = {
        Professions = 3944,
    },
    ["Black Mageweave Vest"] = {
        Professions = 12048,
    },
    ["Hyper-Radiant Flame Reflector"] = {
        Professions = 23081,
    },
    ["Imperial Plate Bracers"] = {
        Professions = 16649,
    },
    ["Intimidating Growl"] = {
        ["Pesterhide Snarler"] = 6576,
    },
    ["Create Spellstone (Greater)"] = {
        ["WARLOCK"] = 17727,
    },
    ["Venomhide Poison"] = {
        ["Venomhide Ravasaur"] = 14792,
    },
    ["Hillman's Leather Vest"] = {
        Professions = 3762,
    },
    ["Arcanite Skeleton Key"] = {
        Professions = 19669,
    },
    ["Enchant Chest - Superior Mana"] = {
        Professions = 13917,
    },
    ["Summon Theurgist"] = {
        ["Dalaran Theurgist"] = 3658,
        ["Dalaran Summoner"] = 3658,
    },
    ["Azure Shoulders"] = {
        Professions = 8795,
    },
    ["Wild Rage"] = {
        ["Shadowfang Ragetooth"] = 7072,
    },
    ["Swim Speed Potion"] = {
        Professions = 7841,
    },
    ["Silver-plated Shotgun"] = {
        Professions = 3949,
    },
    ["Free Action Potion"] = {
        Professions = 6624,
    },
    ["Red Woolen Boots"] = {
        Professions = 3847,
    },
    ["Gnomish Rocket Boots"] = {
        Professions = 12905,
    },
    ["Enslave"] = {
        ["Jailor Eston"] = 3442,
    },
    ["Blue Overalls"] = {
        Professions = 7639,
    },
    ["Feint"] = {
        ["ROGUE"] = 25302,
    },
    ["Ancient Hysteria"] = {
        ["Ancient Core Hound"] = 19372,
    },
    ["Summon Lupine Delusions"] = {
        ["Lupine Delusion"] = 7132,
        ["Lupine Horror"] = 7132,
    },
    ["Enchant Bracer - Mana Regeneration"] = {
        Professions = 23801,
    },
    ["Silvered Bronze Leggings"] = {
        Professions = 12259,
    },
    ["Voidwalker"] = {
        ["Voidwalker Minion"] = 5108,
        ["Defias Conjurer"] = 5108,
        ["Shadowsworn Warlock"] = 5108,
    },
    ["Summon Obsidian Shard"] = {
        ["Obsidian Shard"] = 10061,
    },
    ["Deviate Scale Belt"] = {
        Professions = 7955,
    },
    ["Presence of Death"] = {
        ["Morbent Fel"] = 3109,
    },
    ["Purge"] = {
        ["SHAMAN"] = 8012,
    },
    ["Distract"] = {
        ["ROGUE"] = 1725,
    },
    ["Truesilver Skeleton Key"] = {
        Professions = 19668,
    },
    ["Create Rift"] = {
        ["Tabetha"] = 9079,
    },
    ["Feral Charge"] = {
        ["DRUID"] = 16979,
    },
    ["Nightscape Boots"] = {
        Professions = 10558,
    },
    ["Aspect of the Pack"] = {
        ["HUNTER"] = 13159,
    },
    ["Elixir of Lesser Agility"] = {
        Professions = 2333,
    },
    ["Frost Leather Cloak"] = {
        Professions = 9198,
    },
    ["Frost Breath"] = {
        ["Young Wendigo"] = 3131,
        ["Wendigo"] = 3131,
        ["Mountain Yeti"] = 3131,
        ["Giant Yeti"] = 3131,
        ["Edan the Howler"] = 3129,
    },
    ["Spirit Decay"] = {
        ["Riverpaw Mongrel"] = 8016,
        ["Mosshide Mongrel"] = 8016,
        ["Sunscale Lashtail"] = 8016,
    },
    ["Summon Lava Burst A"] = {
        ["Core Rat"] = 21886,
    },
    ["Putrid Stench"] = {
        ["Putridius"] = 12946,
        ["Plaguemaw the Rotting"] = 12946,
        ["Cauldron Lord Soulwrath"] = 12946,
    },
    ["Barbaric Bracers"] = {
        Professions = 23399,
    },
    ["Soul Burn"] = {
        ["Firelord"] = 19393,
    },
    ["Truesilver Rod"] = {
        Professions = 14380,
    },
    ["Copper Bracers"] = {
        Professions = 2663,
    },
    ["Slap!"] = {
        ["Milton Beats"] = 6754,
    },
    ["Summoned Demon"] = {
        ["Summoned Voidwalker"] = 7741,
        ["Summoned Succubus"] = 7741,
        ["Summoned Felhunter"] = 7741,
        ["Hakkari Bloodkeeper"] = 7741,
        ["Nightmare Suppressor"] = 7741,
        ["Screecher Spirit"] = 7741,
        ["Arugal"] = 7741,
        ["Demon Spirit"] = 7741,
    },
    ["Transmute: Air to Fire"] = {
        Professions = 17559,
    },
    ["Simple Kilt"] = {
        Professions = 12046,
    },
    ["Creeper Venom"] = {
        ["Cave Creeper"] = 14532,
    },
    ["Ancient Dread"] = {
        ["Ancient Core Hound"] = 19365,
    },
    ["Aqua Jet"] = {
        ["Stormscale Wave Rider"] = 13586,
        ["Lord Sinslayer"] = 13586,
    },
    ["Gyrochronatom"] = {
        Professions = 3961,
    },
    ["Curse of Thorns"] = {
        ["Syndicate Shadow Mage"] = 6909,
        ["Crazed Ancient"] = 6909,
        ["Withered Ancient"] = 6909,
        ["Vengeful Ancient"] = 6909,
        ["Burning Blade Augur"] = 6909,
    },
    ["Explosion"] = {
        ["Blastmaster Emi Shortfuse"] = 12158,
        ["Blastmaster Emi Shortfuse"] = 12159,
    },
    ["Chaotic Focus"] = {
        ["Wildspawn Felsworn"] = 22418,
    },
    ["Moonsteel Broadsword"] = {
        Professions = 3496,
    },
    ["Tough Scorpid Boots"] = {
        Professions = 10554,
    },
    ["Arcane Focus"] = {
        ["Spectral Teacher"] = 17633,
    },
    ["Dismiss Pet"] = {
        ["HUNTER"] = 2641,
    },
    ["Plague Mind"] = {
        ["Brain Eater"] = 3429,
        ["Mage Hunter"] = 3429,
    },
    ["Summon Flamekin Torcher"] = {
        ["Flamekin Torcher"] = 15710,
        ["Terrorspark"] = 15710,
    },
    ["Heavy Quiver"] = {
        Professions = 9193,
    },
    ["Hurricane"] = {
        ["DRUID"] = 17402,
    },
    ["Wailing Dead"] = {
        ["Lost Soul"] = 7713,
        ["Wandering Spirit"] = 7713,
        ["Tormented Spirit"] = 7713,
        ["Wailing Ancestor"] = 7713,
        ["Wailing Death"] = 7713,
        ["Undead Postman"] = 7713,
        ["Wailing Spectre"] = 7713,
        ["Terrordale Spirit"] = 7713,
    },
    ["Poison Cloud"] = {
        ["Aku'mai"] = 3815,
        ["Ironspine"] = 3815,
    },
    ["Psychic Scream"] = {
        ["PRIEST"] = 10890,
    },
    ["Wicked Milking"] = {
        ["Ragged John"] = 16472,
    },
    ["Slow"] = {
        ["Slitherblade Oracle"] = 246,
        ["Lady Sarevess"] = 246,
        ["Scarlet Sorcerer"] = 6146,
        ["Captain Balinda Stonehearth"] = 19137,
        ["Twilight Loreseeker"] = 18972,
        ["Mosshide Mystic"] = 11436,
        ["Death's Head Geomancer"] = 11436,
        ["Grimtotem Geomancer"] = 11436,
    },
    ["Deterrence"] = {
        ["HUNTER"] = 19263,
    },
    ["Summon Crimson Rifleman"] = {
        ["Crimson Rifleman"] = 17279,
    },
    ["Purify"] = {
        ["PALADIN"] = 1152,
    },
    ["Golden Scale Leggings"] = {
        Professions = 3507,
    },
    ["Light Leather"] = {
        Professions = 2881,
    },
    ["Minor Healing Potion"] = {
        Professions = 2330,
    },
    ["Serenity"] = {
        Professions = 16983,
    },
    ["Warbear Woolies"] = {
        Professions = 19080,
    },
    ["Green Whelp Bracers"] = {
        Professions = 9202,
    },
    ["Nefarian's Barrier"] = {
        ["Lord Victor Nefarius"] = 22663,
    },
    ["Lesser Mana Oil"] = {
        Professions = 25127,
    },
    ["Befuddlement"] = {
        ["Shadowfang Darksoul"] = 8140,
        ["Aspect of Banality"] = 8140,
    },
    ["Dark Restore"] = {
        ["Shadowfang Wolfguard"] = 7106,
    },
    ["Enraging Memories"] = {
        ["Mor'Ladim"] = 3547,
    },
    ["Intercept"] = {
        ["WARRIOR"] = 20617,
    },
    ["Blessing of Sanctuary"] = {
        ["PALADIN"] = 20914,
    },
    ["Haste Aura"] = {
        ["Anvilrage Overseer"] = 13589,
        ["Whitewhisker Overseer"] = 13589,
    },
    ["Gift of the Wild"] = {
        ["DRUID"] = 21850,
    },
    ["Solid Sharpening Stone"] = {
        Professions = 9918,
    },
    ["Ironfeather Breastplate"] = {
        Professions = 19086,
    },
    ["Dense Grinding Stone"] = {
        Professions = 16639,
    },
    ["Create Broodling Essence"] = {
        ["Black Broodling"] = 16027,
        ["Scalding Broodling"] = 16027,
        ["Flamescale Broodling"] = 16027,
    },
    ["Blessing of Freedom"] = {
        ["PALADIN"] = 1044,
    },
    ["Summon Aqua Guardian"] = {
        ["Aqua Guardian"] = 8372,
        ["Twilight Aquamancer"] = 8372,
    },
    ["Big Voodoo Robe"] = {
        Professions = 10520,
    },
    ["Enchant Chest - Major Health"] = {
        Professions = 20026,
    },
    ["Lunar Fortune"] = {
        ["Pat's Firework Cluster Guy (ELUNE)"] = 26522,
    },
    ["Devouring Plague"] = {
        ["PRIEST"] = 19280,
    },
    ["Mithril Gyro-Shot"] = {
        Professions = 12621,
    },
    ["Dark Leather Boots"] = {
        Professions = 2167,
    },
    ["Festive Red Dress"] = {
        Professions = 26403,
    },
    ["Trooper Ping"] = {
        ["Scarlet Trooper"] = 19749,
    },
    ["Holy Fire"] = {
        ["PRIEST"] = 15261,
    },
    ["Magma Totem"] = {
        ["SHAMAN"] = 10587,
    },
    ["Feed Pet"] = {
        ["HUNTER"] = 6991,
    },
    ["Rough Bronze Boots"] = {
        Professions = 7817,
    },
    ["Dust Cloud"] = {
        ["Strider Clutchmother"] = 7272,
        ["Elder Plainstrider"] = 7272,
        ["Kolkar Drudge"] = 7272,
    },
    ["Enchant Boots - Agility"] = {
        Professions = 13935,
    },
    ["Thunder Clap"] = {
        ["WARRIOR"] = 11581,
    },
    ["Summon Cyclonian"] = {
        ["Bath'rah the Windwatcher"] = 8606,
    },
    ["Vampiric Embrace"] = {
        ["PRIEST"] = 15286,
    },
    ["Instant Poison II"] = {
        ["ROGUE"] = 8687,
    },
    ["Tough Scorpid Leggings"] = {
        Professions = 10568,
    },
    ["Ward of the Eye"] = {
        ["Captain Halyndor"] = 3389,
    },
    ["Wild Leather Vest"] = {
        Professions = 10544,
    },
    ["Lesser Magic Wand"] = {
        Professions = 14293,
    },
    ["Lesser Healing Potion"] = {
        Professions = 2337,
    },
    ["Create Scroll"] = {
        ["Collin Mauren"] = 6671,
    },
    ["VanCleef's Allies"] = {
        ["Defias Blackguard"] = 5200,
        ["Edwin VanCleef"] = 5200,
    },
    ["Harvest Swarm"] = {
        ["Silithid Harvester"] = 7277,
    },
    ["Teleport: Orgrimmar"] = {
        ["MAGE"] = 3567,
    },
    ["Summon Lava Burst D"] = {
        ["Core Rat"] = 21902,
    },
    ["Shrink"] = {
        ["Kurzen Headshrinker"] = 7289,
        ["Zalazane"] = 7289,
        ["Twilight Dark Shaman"] = 7289,
    },
    ["Deviate Scale Cloak"] = {
        Professions = 7953,
    },
    ["Combustion"] = {
        ["MAGE"] = 11129,
    },
    ["Summon Deepmoss Matriarch"] = {
        ["Deepmoss Matriarch"] = 6536,
        ["Deepmoss Hatchling"] = 6536,
    },
    ["Enchant Boots - Lesser Stamina"] = {
        Professions = 13644,
    },
    ["Icy Grasp"] = {
        ["Old Icebeard"] = 3145,
    },
    ["Create Soulstone (Minor)"] = {
        ["WARLOCK"] = 693,
    },
    ["Red Streaks Firework"] = {
        ["Blastmaster Emi Shortfuse"] = 11542,
    },
    ["Elixir of Water Breathing"] = {
        Professions = 7179,
    },
    ["Nightscape Tunic"] = {
        Professions = 10499,
    },
    ["Woolen Cape"] = {
        Professions = 2402,
    },
    ["Light Leather Pants"] = {
        Professions = 9068,
    },
    ["Truefaith Vestments"] = {
        Professions = 18456,
    },
    ["Create Healthstone (Major)"] = {
        ["WARLOCK"] = 11730,
    },
    ["Ground Rupture"] = {
        ["Claw Tentacle"] = 26139,
        ["Eye Tentacle"] = 26139,
    },
    ["Curse of the Shadowhorn"] = {
        ["Shadowhorn Stag"] = 6922,
        ["Elder Shadowhorn Stag"] = 6922,
    },
    ["Consume Shadows"] = {
        Pet = 17854,
    },
    ["Detect Magic"] = {
        ["MAGE"] = 2855,
    },
    ["Thorium Bracers"] = {
        Professions = 16644,
    },
    ["Crimson Silk Shoulders"] = {
        Professions = 8793,
    },
    ["Green Linen Shirt"] = {
        Professions = 2396,
    },
    ["Ice Nova"] = {
        ["Phase Lasher"] = 22519,
    },
    ["Tuxedo Shirt"] = {
        Professions = 12085,
    },
    ["Feedback"] = {
        ["PRIEST"] = 19275,
    },
    ["Deep Wound"] = {
        ["Malicious Spirit"] = 12721,
        ["Aspect of Malice"] = 12721,
    },
    ["Strength of Arko'narin"] = {
        ["Captured Arko'narin"] = 18163,
    },
    ["Boulder"] = {
        ["Dustbelcher Wyrmhunter"] = 9483,
    },
    ["Brilliant Wizard Oil"] = {
        Professions = 25129,
    },
    ["Spawn Green Drakonid"] = {
        ["Green Drakonid"] = 22656,
        ["Green Drakonid Spawner"] = 22656,
    },
    ["Maple Seed"] = {
        ["DRUID"] = 17034,
    },
    ["Kodo Stomp"] = {
        ["Lost Barrens Kodo"] = 6266,
        ["Barrens Kodo"] = 6266,
    },
    ["Shadowskin Gloves"] = {
        Professions = 22711,
    },
    ["Lesser Stoneshield Potion"] = {
        Professions = 4942,
    },
    ["Dusky Leather Armor"] = {
        Professions = 9196,
    },
    ["Tough Scorpid Breastplate"] = {
        Professions = 10525,
    },
    ["Solid Weightstone"] = {
        Professions = 9921,
    },
    ["Wild Leather Boots"] = {
        Professions = 10566,
    },
    ["Blue Dragonscale Shoulders"] = {
        Professions = 19089,
    },
    ["Trueshot Aura"] = {
        ["HUNTER"] = 19506,
    },
    ["Conjured Water"] = {
        ["MAGE"] = 5350,
    },
    ["Savage Rage"] = {
        ["Rot Hide Savage"] = 3258,
        ["Stanley"] = 3258,
    },
    ["Shield Block"] = {
        ["WARRIOR"] = 2565,
        ["Stormwind Guard"] = 12169,
        ["Scarlet Bodyguard"] = 12169,
        ["Pyrewood Sentry"] = 12169,
        ["Spectral Defender"] = 12169,
        ["Stormpike Defender"] = 12169,
        ["Frostwolf Guardian"] = 12169,
        ["Aggi Rumblestomp"] = 12169,
        ["Seasoned Defender"] = 12169,
        ["Seasoned Guardian"] = 12169,
        ["Veteran Defender"] = 12169,
        ["Veteran Guardian"] = 12169,
        ["Champion Guardian"] = 12169,
        ["Champion Defender"] = 12169,
        ["Corporal Noreg Stormpike"] = 12169,
    },
    ["Create Soulstone (Greater)"] = {
        ["WARLOCK"] = 20756,
    },
    ["Summon Gunther's Visage"] = {
        ["Gunther's Visage"] = 7762,
    },
    ["Smelt Steel"] = {
        Professions = 3569,
    },
    ["Fine Leather Tunic"] = {
        Professions = 3761,
    },
    ["Windfury Totem"] = {
        ["SHAMAN"] = 10614,
    },
    ["Enchant Bracer - Greater Spirit"] = {
        Professions = 13846,
    },
    ["Kinelory's Bear Form"] = {
        ["Kinelory"] = 4948,
    },
    ["Hex"] = {
        ["High Priestess Hai'watna"] = 18503,
        ["Bom'bay"] = 16708,
        ["Bom'bay"] = 16709,
        ["Bom'bay"] = 16707,
    },
    ["Steam Tonk Controller"] = {
        Professions = 28327,
    },
    ["Ashenvale Outrunner Sneak"] = {
        ["Ashenvale Outrunner"] = 20540,
    },
    ["Flame Shock"] = {
        ["SHAMAN"] = 29228,
        ["Garneg Charskull"] = 15039,
        ["Primalist Thurloga"] = 15616,
    },
    ["Summon Illusory Wraith"] = {
        ["Illusory Wraith"] = 17231,
        ["Araj the Summoner"] = 17231,
    },
    ["Small Bronze Bomb"] = {
        Professions = 3941,
    },
    ["Forked Lightning"] = {
        ["Lady Sarevess"] = 8435,
        ["Lady Vespira"] = 12549,
    },
    ["Spirit Spawn-in"] = {
        ["Redpath the Corrupted"] = 17321,
        ["Darrowshire Spirit"] = 17321,
        ["Lunaclaw Spirit"] = 17321,
        ["Dreadsteed Spirit"] = 17321,
        ["Darkreaver's Fallen Charger"] = 17321,
        ["Servant of the Hand"] = 17321,
    },
    ["Create Soulstone (Major)"] = {
        ["WARLOCK"] = 20757,
    },
    ["Cure Disease"] = {
        ["PRIEST"] = 528,
        ["SHAMAN"] = 2870,
    },
    ["Summon Lava Burst G"] = {
        ["Core Rat"] = 21905,
    },
    ["Enchant Shield - Stamina"] = {
        Professions = 13817,
    },
    ["Shadowguard"] = {
        ["PRIEST"] = 19312,
    },
    ["Orcish War Leggings"] = {
        Professions = 9957,
    },
    ["Murloc Scale Breastplate"] = {
        Professions = 6703,
    },
    ["Seal of Righteousness"] = {
        ["PALADIN"] = 20293,
    },
    ["Crippling Clip"] = {
        ["Nelson the Nice"] = 23279,
    },
    ["Ravage"] = {
        ["DRUID"] = 9867,
        ["Aku'mai Snapjaw"] = 8391,
        ["Greater Duskbat"] = 3242,
        ["Vampiric Duskbat"] = 3242,
        ["Mangeclaw"] = 3242,
        ["Thistle Bear"] = 3242,
        ["Grizzled Thistle Bear"] = 3242,
        ["Den Mother"] = 3242,
        ["Thistle Cub"] = 3242,
    },
    ["Goblin Jumper Cables"] = {
        Professions = 9273,
    },
    ["Stonescale Oil"] = {
        Professions = 17551,
    },
    ["Dark Leather Belt"] = {
        Professions = 3766,
    },
    ["Enchant Gloves - Strength"] = {
        Professions = 13887,
    },
    ["Banish"] = {
        ["WARLOCK"] = 18647,
    },
    ["Steel Breastplate"] = {
        Professions = 9916,
    },
    ["Cheap Shot"] = {
        ["ROGUE"] = 1833,
    },
    ["Green Whelp Armor"] = {
        Professions = 9197,
    },
    ["Dense Weightstone"] = {
        Professions = 16640,
    },
    ["Molten Metal"] = {
        ["Gilnid"] = 5213,
        ["Smoldar"] = 5213,
    },
    ["Crumbling Stone"] = {
        ["Elemental Slave"] = 3672,
    },
    ["Shadoweave Robe"] = {
        Professions = 12055,
    },
    ["Dreamweave Gloves"] = {
        Professions = 12067,
    },
    ["Moonglow Vest"] = {
        Professions = 8322,
    },
    ["Rend Flesh"] = {
        ["Bhag'thera"] = 3147,
        ["Highland Scytheclaw"] = 3147,
        ["Mottled Scytheclaw"] = 3147,
        ["Bjarn"] = 3147,
        ["Razormaw Matriarch"] = 3147,
        ["Trained Razorbeak"] = 3147,
        ["Razorbeak Gryphon"] = 3147,
        ["Razorbeak Skylord"] = 3147,
        ["Dustwind Pillager"] = 3147,
        ["Bloodfen Scytheclaw"] = 3147,
        ["Muckshell Razorclaw"] = 3147,
        ["Dread Ripper"] = 3147,
        ["Ferocious Rage Scar"] = 3147,
    },
    ["Temptress' Kiss"] = {
        ["Simone the Inconspicuous"] = 23205,
    },
    ["Heavy Blasting Powder"] = {
        Professions = 3945,
    },
    ["Shadoweave Shoulders"] = {
        Professions = 12076,
    },
    ["Bronze Warhammer"] = {
        Professions = 9985,
    },
    ["Enchant Cloak - Lesser Protection"] = {
        Professions = 13421,
    },
    ["Tunneler Acid"] = {
        ["Gorishi Tunneler"] = 14120,
        ["Gorishi Hive Guard"] = 14120,
        ["Rock Borer"] = 14120,
    },
    ["Snake Burst Firework"] = {
        Professions = 23507,
    },
    ["Will of Shahram"] = {
        ["Shahram"] = 16598,
    },
    ["Divine Shield"] = {
        ["PALADIN"] = 1020,
    },
    ["Dummy NPC Summon"] = {
        ["Defias Rogue Wizard"] = 3361,
    },
    ["Create Firestone (Lesser)"] = {
        ["WARLOCK"] = 6366,
    },
    ["Aura of Rot"] = {
        ["Stitches"] = 3106,
    },
    ["Drain Soul"] = {
        ["WARLOCK"] = 11675,
    },
    ["Xandivious Demon Bag"] = {
        ["Xandivious"] = 25791,
    },
    ["Enchant Boots - Lesser Agility"] = {
        Professions = 13637,
    },
    ["Inferno Effect"] = {
        ["Infernal"] = 22703,
        ["Corrupted Infernal"] = 22703,
    },
    ["Resupply"] = {
        ["Dark Iron Supplier"] = 4961,
    },
    ["Spellpower Goggles Xtreme Plus"] = {
        Professions = 19794,
    },
    ["Silithid Pox"] = {
        ["Silithid Invader"] = 8137,
        ["Gorishi Wasp"] = 8137,
        ["Gorishi Worker"] = 8137,
        ["Gorishi Reaver"] = 8137,
        ["Gorishi Stinger"] = 8137,
        ["Gorishi Tunneler"] = 8137,
        ["Gorishi Hive Guard"] = 8137,
        ["Gorishi Hive Queen"] = 8137,
    },
    ["Symbol of Divinity"] = {
        ["PALADIN"] = 17033,
    },
    ["Create Firestone"] = {
        ["WARLOCK"] = 17951,
    },
    ["Curse of the Deadwood"] = {
        ["Deadwood Warrior"] = 13583,
        ["Deadwood Gardener"] = 13583,
        ["Deadwood Pathfinder"] = 13583,
        ["Deadwood Den Watcher"] = 13583,
        ["Deadwood Avenger"] = 13583,
        ["Deadwood Shaman"] = 13583,
        ["Chieftain Bloodmaw"] = 13583,
    },
    ["Lesser Mystic Wand"] = {
        Professions = 14809,
    },
    ["Raise Undead Scarab"] = {
        ["Undead Scarab"] = 17235,
    },
    ["Turtle Scale Breastplate"] = {
        Professions = 10511,
    },
    ["Edge of Winter"] = {
        Professions = 21913,
    },
    ["Heavy Copper Broadsword"] = {
        Professions = 3292,
    },
    ["Green Tinted Goggles"] = {
        Professions = 3956,
    },
    ["Flying Tiger Goggles"] = {
        Professions = 3934,
    },
    ["Create Mage's Orb"] = {
        ["Tabetha"] = 9156,
    },
    ["Tiger's Fury"] = {
        ["DRUID"] = 9846,
    },
    ["Greater Heal"] = {
        ["PRIEST"] = 25314,
    },
    ["Battle Net"] = {
        ["Techbot"] = 10852,
    },
    ["Rough Weightstone"] = {
        Professions = 3115,
    },
    ["Dark Iron Shoulders"] = {
        Professions = 15295,
    },
    ["Imperial Plate Belt"] = {
        Professions = 16647,
    },
    ["Cripple"] = {
        ["Cliff Breaker"] = 11443,
        ["Death's Head Necromancer"] = 11443,
    },
    ["Enchant Chest - Stats"] = {
        Professions = 13941,
    },
    ["Heavy Woolen Gloves"] = {
        Professions = 3843,
    },
    ["Wing Clip"] = {
        ["HUNTER"] = 14268,
    },
    ["Create Zul's Aura"] = {
        ["Yeh'kinya"] = 24186,
    },
    ["Heavy Sharpening Stone"] = {
        Professions = 2674,
    },
    ["Lifelike Mechanical Toad"] = {
        Professions = 19793,
    },
    ["Natural Armor"] = {
        ["HUNTER"] = 24632,
    },
    ["Massive Geyser"] = {
        ["Massive Geyser"] = 22421,
    },
    ["Toxic Spit"] = {
        ["Deepmoss Venomspitter"] = 7951,
        ["Darkfang Venomspitter"] = 7951,
        ["Deviate Venomwing"] = 7951,
    },
    ["Living Shoulders"] = {
        Professions = 19061,
    },
    ["Phoenix Gloves"] = {
        Professions = 3868,
    },
    ["Summon Illusionary Nightmare"] = {
        ["Illusionary Nightmare"] = 6905,
        ["Severed Dreamer"] = 6905,
    },
    ["Crystal Gaze"] = {
        ["Stone Maw Basilisk"] = 3635,
        ["Crystal Spine Basilisk"] = 3635,
        ["Saltstone Gazer"] = 3635,
        ["Glasshide Gazer"] = 3635,
        ["Redstone Basilisk"] = 3635,
        ["Deatheye"] = 3635,
    },
    ["Iron Grenade"] = {
        Professions = 3962,
    },
    ["Handstitched Linen Britches"] = {
        Professions = 3842,
    },
    ["Tremor Totem"] = {
        ["SHAMAN"] = 8143,
    },
    ["Track Humanoids"] = {
        ["DRUID"] = 5225,
        ["HUNTER"] = 19883,
    },
    ["Reduced to Rubble"] = {
        ["Elemental Slave"] = 3673,
    },
    ["Ancestral Spirit"] = {
        ["SHAMAN"] = 20777,
    },
    ["Lesser Healing Wave"] = {
        ["SHAMAN"] = 10468,
    },
    ["Bruise"] = {
        ["Burning Blade Bruiser"] = 4134,
    },
    ["Blue Linen Vest"] = {
        Professions = 7630,
    },
    ["Thorium Shield Spike"] = {
        Professions = 16651,
    },
    ["Crafted Heavy Shot"] = {
        Professions = 3930,
    },
    ["Cured Heavy Hide"] = {
        Professions = 3818,
    },
    ["Pick Lock"] = {
        ["ROGUE"] = 1804,
    },
    ["Tears of the Wind Seeker"] = {
        ["Prince Thunderaan"] = 23011,
    },
    ["Burning Tenacity"] = {
        ["Telf Joolam"] = 8383,
    },
    ["Gloves of Meditation"] = {
        Professions = 3852,
    },
    ["Shadowburn"] = {
        ["WARLOCK"] = 18871,
    },
    ["Manastorm"] = {
        ["Azuregos"] = 21097,
    },
    ["Strength of Stone"] = {
        ["Shadethicket Stone Mover"] = 6864,
    },
    ["Green Rocket Cluster"] = {
        Professions = 26424,
    },
    ["Sludge Toxin"] = {
        ["Jade Sludge"] = 6814,
    },
    ["Challenging Shout"] = {
        ["WARRIOR"] = 1161,
    },
    ["Girdle of the Dawn"] = {
        Professions = 23632,
    },
    ["Brown Linen Robe"] = {
        Professions = 7623,
    },
    ["Haunting Spirits"] = {
        ["Haunted Servitor"] = 7057,
    },
    ["Sloth Effect"] = {
        ["Sloth"] = 3510,
    },
    ["Sinister Strike"] = {
        ["ROGUE"] = 11294,
    },
    ["Elemental Mastery"] = {
        ["SHAMAN"] = 16166,
    },
    ["Acid Splash"] = {
        ["Cookie"] = 6306,
        ["Green Wyrmkin"] = 6306,
        ["Elder Dragonkin"] = 6306,
        ["Gluggle"] = 6306,
    },
    ["Ornate Mithril Helm"] = {
        Professions = 9980,
    },
    ["Consuming Rage"] = {
        ["Summoned Voidwalker"] = 7750,
    },
    ["Fist of Shahram"] = {
        ["Shahram"] = 16601,
    },
    ["Enchant Chest - Minor Health"] = {
        Professions = 7420,
    },
    ["Enchant Boots - Lesser Spirit"] = {
        Professions = 13687,
    },
    ["Screech"] = {
        Pet = 24579,
    },
    ["Golden Iron Destroyer"] = {
        Professions = 3495,
    },
    ["Power Infusion"] = {
        ["PRIEST"] = 10060,
    },
    ["Summon Scarlet Hound"] = {
        ["Scarlet Hound"] = 17164,
        ["Scarlet Hunter"] = 17164,
    },
    ["Small Green Rocket"] = {
        Professions = 26417,
    },
    ["Curse of Shadow"] = {
        ["WARLOCK"] = 17937,
    },
    ["Maul"] = {
        ["DRUID"] = 9881,
        ["Ursal the Mauler"] = 15793,
        ["Shardtooth Mauler"] = 15793,
        ["Angerclaw Mauler"] = 15793,
        ["Overlord Ror"] = 15793,
        ["High Chief Winterfall"] = 15793,
        ["Uthil Mooncall"] = 12161,
        ["Cenarion Caretaker"] = 12161,
        ["Rageclaw"] = 12161,
    },
    ["Distracting Pain"] = {
        ["Sneed's Shredder"] = 3603,
    },
    ["Pyroclast Barrage"] = {
        ["Lava Elemental"] = 19641,
    },
    ["Small Red Rocket"] = {
        Professions = 26418,
    },
    ["Elixir of the Mongoose"] = {
        Professions = 17571,
    },
    ["Curse of Exhaustion"] = {
        ["WARLOCK"] = 18223,
    },
    ["Grab Weapon"] = {
        ["Caverndeep Looter"] = 10851,
    },
    ["Bomb"] = {
        ["Dark Iron Demolitionist"] = 8858,
        ["Dark Iron Bombardier"] = 8858,
        ["Dark Iron Geologist"] = 8858,
        ["Holdout Technician"] = 8858,
        ["Boss Copperplug"] = 9143,
    },
    ["Faded"] = {
        ["Defias Blackguard"] = 6408,
        ["Onin MacHammar"] = 6408,
    },
    ["Green Silk Pack"] = {
        Professions = 6693,
    },
    ["Radiant Circlet"] = {
        Professions = 16659,
    },
    ["Target Dummy - Event 002"] = {
        ["Mortar Team Target Dummy"] = 18907,
        ["Spotter Klemmy"] = 18907,
    },
    ["Crude Scope"] = {
        Professions = 3977,
    },
    ["Quest - Polymorph Impact"] = {
        ["Yeh'kinya"] = 24180,
    },
    ["Release Toads"] = {
        ["Jungle Toad"] = 24058,
        ["Hakkari Witch Doctor"] = 24058,
    },
    ["Superior Mana Potion"] = {
        Professions = 17553,
    },
    ["Thick War Axe"] = {
        Professions = 3294,
    },
    ["Massive Tremor"] = {
        ["Molten Destroyer"] = 19129,
        ["Baron Kazum"] = 19129,
    },
    ["Flask of the Titans"] = {
        Professions = 17635,
    },
    ["Crippling Poison"] = {
        ["ROGUE"] = 3420,
    },
    ["Shadow Hood"] = {
        Professions = 3858,
    },
    ["Expose Weakness"] = {
        ["Blood Seeker"] = 7140,
    },
    ["Iron Counterweight"] = {
        Professions = 7222,
    },
    ["Araj's Phylactery"] = {
        ["Araj the Summoner"] = 18661,
    },
    ["Thorium Armor"] = {
        Professions = 16642,
    },
    ["Disease Cloud"] = {
        ["Undercity Guardian"] = 12187,
        ["Gordo"] = 12187,
    },
    ["Peon Sleeping"] = {
        ["Lazy Peon"] = 17743,
    },
    ["Ward of Laze"] = {
        ["Ward of Laze"] = 3827,
        ["Mosh'Ogg Witch Doctor"] = 3827,
    },
    ["Gnomish Battle Chicken"] = {
        Professions = 12906,
    },
    ["Sacrifice"] = {
        Pet = 19443,
    },
    ["Healing Ward"] = {
        ["Healing Ward IV"] = 6274,
        ["Bluegill Oracle"] = 6274,
        ["Razormane Seer"] = 6274,
        ["Thistlefur Totemic"] = 6274,
        ["Healing Ward V"] = 4971,
        ["Razorfen Totemic"] = 4971,
        ["Death's Head Sage"] = 4971,
        ["Death's Head Seer"] = 4971,
        ["Healing Ward"] = 5605,
        ["Bloodscalp Witch Doctor"] = 5605,
        ["Vile Fin Lakestalker"] = 5605,
        ["Gnarlpine Totemic"] = 5605,
        ["Blackwood Totemic"] = 5605,
        ["Witherbark Witch Doctor"] = 5605,
        ["Stonevault Shaman"] = 5605,
        ["Elder Mystic Razorsnout"] = 5605,
        ["Slitherblade Oracle"] = 5605,
        ["Grimtotem Stomper"] = 5605,
        ["Greater Healing Ward"] = 11899,
        ["Mosh'Ogg Shaman"] = 11899,
        ["Sandfury Witch Doctor"] = 11899,
    },
    ["Dark Whispers"] = {
        ["Vile Tutor"] = 16587,
    },
    ["Blacksmithing"] = {
        Professions = 3100,
        Professions = 3538,
        Professions = 9785,
    },
    ["Dash"] = {
        ["DRUID"] = 9821,
        Pet = 23110,
    },
    ["Golden Rod"] = {
        Professions = 14379,
    },
    ["Backstab"] = {
        ["ROGUE"] = 25300,
        ["Thuros Lightfingers"] = 7159,
        ["Defias Pathstalker"] = 7159,
        ["Morgan the Collector"] = 7159,
        ["Defias Footpad"] = 7159,
        ["Defias Looter"] = 7159,
        ["Bloodscalp Scavenger"] = 7159,
        ["Murloc Lurker"] = 7159,
        ["Rockjaw Ambusher"] = 7159,
        ["Frostmane Hideskinner"] = 7159,
        ["Vile Fin Muckdweller"] = 7159,
        ["Defias Captive"] = 7159,
        ["Syndicate Footpad"] = 7159,
        ["Syndicate Thief"] = 7159,
        ["Syndicate Spy"] = 7159,
        ["Forsaken Intruder"] = 7159,
        ["Nethergarde Analyst"] = 7159,
        ["Razorfen Stalker"] = 7159,
        ["Defias Bodyguard"] = 7159,
        ["Snowblind Ambusher"] = 7159,
    },
    ["Shadow Oil"] = {
        Professions = 3449,
    },
    ["Fire Blast"] = {
        ["MAGE"] = 10199,
    },
    ["Stun Bomb Attack"] = {
        ["Aerie Gryphon"] = 21188,
        ["War Rider"] = 21188,
        ["Guse's War Rider"] = 21188,
        ["Jeztor's War Rider"] = 21188,
        ["Mulverick's War Rider"] = 21188,
        ["Slidore's Gryphon"] = 21188,
        ["Ichman's Gryphon"] = 21188,
        ["Vipore's Gryphon"] = 21188,
    },
    ["Enchant 2H Weapon - Lesser Impact"] = {
        Professions = 13529,
    },
    ["Powerful Seaforium Charge"] = {
        Professions = 23080,
    },
    ["Conjure Mana Agate"] = {
        ["MAGE"] = 759,
    },
    ["Demoralizing Shout"] = {
        ["WARRIOR"] = 11556,
    },
    ["Chicken Fury"] = {
        ["Battle Chicken"] = 13168,
    },
    ["Spellpower Goggles Xtreme"] = {
        Professions = 12615,
    },
    ["Heal"] = {
        ["PRIEST"] = 6064,
        ["High Inquisitor Fairbanks"] = 12039,
        ["Irondeep Surveyor"] = 15586,
        ["Veteran Irondeep Surveyor"] = 15586,
        ["Champion Irondeep Surveyor"] = 15586,
        ["Stormscale Siren"] = 11642,
        ["Wrathtail Priestess"] = 11642,
        ["Storm Bay Oracle"] = 11642,
        ["Makrinni Scrabbler"] = 11642,
        ["Holdout Medic"] = 22167,
    },
    ["Transmute: Life to Earth"] = {
        Professions = 17565,
    },
    ["Regrowth"] = {
        ["DRUID"] = 9858,
        ["Moonkin Oracle"] = 16561,
    },
    ["Touch of Weakness"] = {
        ["PRIEST"] = 19266,
    },
    ["Mithril Tube"] = {
        Professions = 12589,
    },
    ["Turtle Scale Leggings"] = {
        Professions = 10556,
    },
    ["Imperial Plate Shoulders"] = {
        Professions = 16646,
    },
    ["Wrath"] = {
        ["DRUID"] = 9912,
        ["Gnarlpine Pathfinder"] = 9739,
        ["Palemane Tanner"] = 9739,
        ["Severed Druid"] = 9739,
        ["Cenarion Botanist"] = 9739,
        ["Tyranis Malem"] = 9739,
        ["Gnarlpine Mystic"] = 9739,
        ["Moonkin Oracle"] = 9739,
    },
    ["Sayge's Dark Fortune of Intelligence"] = {
        ["Sayge"] = 23766,
    },
    ["Heavy Mithril Pants"] = {
        Professions = 9933,
    },
    ["Enchant Chest - Minor Mana"] = {
        Professions = 7443,
    },
    ["Flame Breath"] = {
        ["Narillasanz"] = 9573,
        ["Teremus the Devourer"] = 9573,
        ["Rogue Black Drake"] = 9573,
    },
    ["Heavy Armor Kit"] = {
        Professions = 3780,
    },
    ["Wicked Leather Gauntlets"] = {
        Professions = 19049,
    },
    ["Enchant Chest - Mana"] = {
        Professions = 13607,
    },
    ["Arcane Resistance"] = {
        ["HUNTER"] = 24510,
    },
    ["Healing Wave"] = {
        ["SHAMAN"] = 25357,
        ["Irondeep Shaman"] = 12492,
        ["Bloodscalp Mystic"] = 11986,
        ["Skullsplitter Mystic"] = 11986,
        ["Mosshide Mystic"] = 11986,
        ["Thistlefur Shaman"] = 11986,
        ["Arkkoran Oracle"] = 11986,
        ["Ragefire Shaman"] = 11986,
        ["Hakkari Oracle"] = 15982,
    },
    ["Devotion Aura"] = {
        ["PALADIN"] = 10293,
        ["Blackrock Champion"] = 8258,
        ["Captain Melrache"] = 8258,
        ["Dun Garok Mountaineer"] = 8258,
        ["Boulderfist Lord"] = 8258,
        ["Scarlet Protector"] = 8258,
        ["Razorfen Quilguard"] = 8258,
        ["Quilguard Champion"] = 8258,
        ["Kolkar Battle Lord"] = 8258,
    },
    ["Honor Points +82"] = {
        ["Horde Warbringer"] = 24961,
        ["Alliance Brigadier General"] = 24961,
    },
    ["Earth Shock"] = {
        ["SHAMAN"] = 10414,
        ["Grimtotem Stomper"] = 13281,
        ["Gogger Rock Keeper"] = 13281,
        ["Hakkari Oracle"] = 15501,
    },
    ["Cindercloth Vest"] = {
        Professions = 18408,
    },
    ["Smash"] = {
        ["Molten Giant"] = 18944,
    },
    ["Golden Scale Cuirass"] = {
        Professions = 3511,
    },
    ["White Woolen Dress"] = {
        Professions = 8467,
    },
    ["Bear Form"] = {
        ["DRUID"] = 5487,
        ["Grimtotem Naturalist"] = 19030,
        ["Cenarion Protector"] = 7090,
        ["Uthil Mooncall"] = 7090,
        ["Cenarion Caretaker"] = 7090,
        ["Rageclaw"] = 7090,
        ["Kerlonian Evershade"] = 18309,
    },
    ["Dark Pact"] = {
        ["WARLOCK"] = 18938,
    },
    ["Wickerman Guardian Ember"] = {
        ["Wickerman Guardian"] = 25007,
    },
    ["Mooncloth Circlet"] = {
        Professions = 18452,
    },
    ["Enslave Demon"] = {
        ["WARLOCK"] = 11726,
    },
    ["Mark of Shame"] = {
        ["Parqual Fintallas"] = 6767,
    },
    ["Flame Spike"] = {
        ["Garneg Charskull"] = 6725,
        ["Sister Riven"] = 6725,
        ["Lo'Grosh"] = 8814,
        ["Bloodmage Thalnos"] = 8814,
        ["Death's Head Geomancer"] = 8814,
    },
    ["Vengeance"] = {
        ["Thistlefur Avenger"] = 8602,
    },
    ["White Linen Robe"] = {
        Professions = 7624,
    },
    ["Teleport: Undercity"] = {
        ["MAGE"] = 3563,
    },
    ["Big Iron Bomb"] = {
        Professions = 3967,
    },
    ["Advanced Target Dummy - Event 003"] = {
        ["Mortar Team Advanced Target Dummy"] = 19723,
        ["Spotter Klemmy"] = 19723,
    },
    ["Demon Armor"] = {
        ["WARLOCK"] = 11735,
        ["Thule Ravenclaw"] = 13787,
    },
    ["Fine Leather Pants"] = {
        Professions = 7133,
    },
    ["Altered Cauldron Toxin"] = {
        ["Farmer Dalson"] = 17650,
    },
    ["Bolt of Runecloth"] = {
        Professions = 18401,
    },
    ["Water Walking"] = {
        ["SHAMAN"] = 546,
    },
    ["Elixir of Minor Agility"] = {
        Professions = 3230,
    },
    ["Hatch Eggs"] = {
        ["High Priestess Mar'li"] = 24083,
    },
    ["Blood Howl"] = {
        ["Moonrage Bloodhowler"] = 3264,
        ["Serena Bloodfeather"] = 3264,
        ["Ghostpaw Howler"] = 3264,
        ["Bloodroar the Stalker"] = 3264,
        ["Old Cliff Jumper"] = 3264,
    },
    ["Silvered Bronze Boots"] = {
        Professions = 3331,
    },
    ["Thorns"] = {
        ["DRUID"] = 9910,
    },
    ["Transmute: Iron to Gold"] = {
        Professions = 11479,
    },
    ["Summon Doomguard"] = {
        ["Doomguard Minion"] = 22865,
        ["Gordok Warlock"] = 22865,
    },
    ["Heavy Leather Ammo Pouch"] = {
        Professions = 9194,
    },
    ["Find Treasure"] = {
        Racial = 2481,
    },
    ["Grow"] = {
        ["Bom'bay"] = 16711,
    },
    ["Fire Resistance"] = {
        ["HUNTER"] = 24464,
    },
    ["Dusky Boots"] = {
        Professions = 9207,
    },
    ["Teleport: Darnassus"] = {
        ["MAGE"] = 3565,
    },
    ["Embossed Leather Pants"] = {
        Professions = 3759,
    },
    ["Nether Gem"] = {
        ["Bethor Iceshard"] = 7673,
    },
    ["Kick"] = {
        ["ROGUE"] = 1769,
        ["Scarlet Monk"] = 11978,
        ["Grimtotem Bandit"] = 11978,
        ["Undead Ravager"] = 11978,
    },
    ["Big Voodoo Pants"] = {
        Professions = 10560,
    },
    ["Veil of Shadow"] = {
        ["Terrowulf Shadow Weaver"] = 7068,
        ["Anguished Dead"] = 7068,
        ["Fellicent's Shade"] = 7068,
        ["Vol'jin"] = 17820,
    },
    ["Barbaric Iron Helm"] = {
        Professions = 9814,
    },
    ["Scatter Shot"] = {
        ["HUNTER"] = 19503,
    },
    ["Black Mageweave Shoulders"] = {
        Professions = 12074,
    },
    ["Dreadful Fright"] = {
        ["Nelson the Nice"] = 23275,
    },
    ["Mark of Flames"] = {
        ["Firebrand Grunt"] = 15128,
        ["Firebrand Legionnaire"] = 15128,
        ["Firebrand Darkweaver"] = 15128,
        ["Firebrand Invoker"] = 15128,
        ["Firebrand Dreadweaver"] = 15128,
        ["Firebrand Pyromancer"] = 15128,
    },
    ["Shiny Fish Scales"] = {
        ["SHAMAN"] = 17057,
    },
    ["Rabies"] = {
        ["Rabid Shadowhide Gnoll"] = 3150,
        ["Rabid Dire Wolf"] = 3150,
        ["Timber"] = 3150,
        ["Rabid Thistle Bear"] = 3150,
        ["Rabid Crag Coyote"] = 3150,
        ["Rabid Bonepaw"] = 3150,
        ["Rabid Longtooth"] = 3150,
        ["Rabid Blisterpaw"] = 3150,
        ["Rabid Shardtooth"] = 3150,
    },
    ["Head Crack"] = {
        ["Skullsplitter Warrior"] = 3148,
        ["Skullsplitter Axe Thrower"] = 3148,
        ["Rockjaw Skullthumper"] = 3148,
        ["Devlin Agamand"] = 3148,
        ["Gravelflint Bonesnapper"] = 3148,
        ["Hillsbrad Apprentice Blacksmith"] = 3148,
        ["Crushridge Enforcer"] = 9791,
        ["Irondeep Skullthumper"] = 16172,
    },
    ["Bestial Wrath"] = {
        ["HUNTER"] = 19574,
    },
    ["Curse of the Elements"] = {
        ["WARLOCK"] = 11722,
    },
    ["Copper Shortsword"] = {
        Professions = 2739,
    },
    ["Swell of Souls"] = {
        ["Lokholar the Ice Lord"] = 21307,
    },
    ["Holy Wrath"] = {
        ["PALADIN"] = 10318,
    },
    ["Enchant Gloves - Minor Haste"] = {
        Professions = 13948,
    },
    ["Create Cache of Mau'ari"] = {
        ["Witch Doctor Mau'ari"] = 16351,
    },
    ["Deadly Blunderbuss"] = {
        Professions = 3936,
    },
    ["Enchant Boots - Minor Stamina"] = {
        Professions = 7863,
    },
    ["Mooncloth Robe"] = {
        Professions = 22902,
    },
    ["Dire Bear Form"] = {
        ["DRUID"] = 9634,
    },
    ["Divine Spirit"] = {
        ["PRIEST"] = 27841,
    },
    ["Create Soul Gem"] = {
        ["Kin'weelay"] = 3660,
    },
    ["Green Iron Leggings"] = {
        Professions = 3506,
    },
    ["Violent Shield Effect"] = {
        ["Kurzen Subchief"] = 3260,
        ["Dalaran Shield Guard"] = 3260,
    },
    ["Chill"] = {
        ["Blizzard"] = 28547,
    },
    ["Greater Blessing of Salvation"] = {
        ["PALADIN"] = 25895,
    },
    ["Summon Hive'Ashi Drones"] = {
        ["Hive'Ashi Drone"] = 21327,
        ["Tortured Druid"] = 21327,
        ["Tortured Sentinel"] = 21327,
    },
    ["Create Pylon User's Manual"] = {
        ["J.D. Collie"] = 15211,
    },
    ["Rage of Thule"] = {
        ["Snarlmane"] = 3387,
    },
    ["Radiant Breastplate"] = {
        Professions = 16648,
    },
    ["Prayer of Healing"] = {
        ["PRIEST"] = 25316,
    },
    ["Summon Wind Howler"] = {
        ["Wind Howler"] = 8271,
        ["Razorfen Dustweaver"] = 8271,
    },
    ["Battle Fury"] = {
        ["Tharil'zun"] = 3631,
        ["Splinter Fist Taskmaster"] = 3631,
        ["Venture Co. Workboss"] = 3631,
        ["Windshear Overlord"] = 3631,
        ["Malgin Barleybrew"] = 3631,
        ["Twilight's Hammer Executioner"] = 3631,
    },
    ["Black Mageweave Robe"] = {
        Professions = 12050,
    },
    ["Summon Marduk the Black"] = {
        ["Marduk the Black"] = 18650,
        ["Captain Redpath"] = 18650,
    },
    ["Copper Battle Axe"] = {
        Professions = 3293,
    },
    ["Rain of Fire"] = {
        ["WARLOCK"] = 11678,
        ["Grel'borg the Miser"] = 11990,
        ["Gogger Geomancer"] = 11990,
    },
    ["Red Linen Vest"] = {
        Professions = 7629,
    },
    ["Heroic Strike"] = {
        ["WARRIOR"] = 25286,
    },
    ["Lightning Wave"] = {
        ["Ysondre"] = 24819,
    },
    ["Curse of Doom"] = {
        ["WARLOCK"] = 603,
    },
    ["Intimidating Shout"] = {
        ["WARRIOR"] = 5246,
    },
    ["Patch"] = {
        ["Techbot"] = 10860,
    },
    ["Summon Flamekin Rager"] = {
        ["Flamekin Rager"] = 15711,
        ["Terrorspark"] = 15711,
    },
    ["Azure Silk Cloak"] = {
        Professions = 8786,
    },
    ["Leatherworking"] = {
        Professions = 3104,
        Professions = 3811,
        Professions = 10662,
    },
    ["Crimson Silk Gloves"] = {
        Professions = 8804,
    },
    ["Smoke Bomb"] = {
        ["Kurzen Commando"] = 7964,
        ["Bazil Thredd"] = 7964,
        ["Colonel Kurzen"] = 8817,
    },
    ["Silence"] = {
        ["PRIEST"] = 15487,
        ["Witherbark Shadow Hunter"] = 6726,
        ["Arcanist Doan"] = 8988,
    },
    ["Mooncloth Gloves"] = {
        Professions = 22869,
    },
    ["Thekal Trigger"] = {
        ["Zealot Lor'Khan"] = 24172,
        ["Zealot Zath"] = 24172,
    },
    ["Special Brew"] = {
        ["Bom'bay"] = 16712,
    },
    ["Engineering"] = {
        Professions = 4037,
        Professions = 4038,
        Professions = 12656,
    },
    ["Launch"] = {
        ["Bom'bay"] = 16716,
    },
    ["Enchant Weapon - Agility"] = {
        Professions = 23800,
    },
    ["Power Word: Fortitude"] = {
        ["PRIEST"] = 10938,
        ["Holdout Medic"] = 13864,
    },
    ["Chain Heal"] = {
        ["SHAMAN"] = 10623,
        ["Aggem Thorncurse"] = 14900,
    },
    ["Noxious Cloud"] = {
        ["Noxious Slime"] = 21070,
    },
    ["Sling Dirt"] = {
        ["Oasis Snapjaw"] = 6530,
    },
    ["Curse of Thule"] = {
        ["Rot Hide Gnoll"] = 3237,
        ["Rot Hide Mongrel"] = 3237,
        ["Maggot Eye"] = 3237,
        ["Rot Hide Gladerunner"] = 3237,
        ["Rot Hide Mystic"] = 3237,
        ["Rot Hide Brute"] = 3237,
        ["Rot Hide Plague Weaver"] = 3237,
        ["Rot Hide Graverobber"] = 3237,
        ["Rot Hide Savage"] = 3237,
        ["Raging Rot Hide"] = 3237,
    },
    ["Far Sight"] = {
        ["SHAMAN"] = 6196,
    },
    ["Boar Charge"] = {
        ["Bristleback Battleboar"] = 3385,
        ["Dire Mottled Boar"] = 3385,
        ["Elder Mottled Boar"] = 3385,
        ["Corrupted Mottled Boar"] = 3385,
    },
    ["Wizard Oil"] = {
        Professions = 25128,
    },
    ["Summon Illusions"] = {
        ["Illusion of Jandice Barov"] = 17773,
        ["Jandice Barov"] = 17773,
    },
    ["Concussive Shot"] = {
        ["HUNTER"] = 5116,
    },
    ["Gnomish Cloaking Device"] = {
        Professions = 3971,
    },
    ["Mind Control"] = {
        ["PRIEST"] = 10912,
    },
    ["Elixir of Defense"] = {
        Professions = 3177,
    },
    ["Sonic Burst"] = {
        ["Greater Kraul Bat"] = 8281,
        ["Rogue Vale Screecher"] = 8281,
        ["Monstrous Plaguebat"] = 8281,
        ["Ressan the Needler"] = 8281,
    },
    ["Barbaric Belt"] = {
        Professions = 3779,
    },
    ["Shadoweave Mask"] = {
        Professions = 12086,
    },
    ["Major Mana Potion"] = {
        Professions = 17580,
    },
    ["Water"] = {
        ["Massive Geyser"] = 22422,
    },
    ["Nature Resistance"] = {
        ["HUNTER"] = 24513,
    },
    ["Plague Cloud"] = {
        ["Ardo Dirtpaw"] = 3256,
        ["Rot Hide Plague Weaver"] = 3256,
    },
    ["Creeping Mold"] = {
        ["Razormane Water Seeker"] = 6278,
        ["Mirefin Muckdweller"] = 6278,
    },
    ["Summon Felhunter"] = {
        ["WARLOCK"] = 691,
    },
    ["Skullsplitter Pet"] = {
        ["Skullsplitter Panther"] = 3621,
        ["Skullsplitter Hunter"] = 3621,
        ["Skullsplitter Beastmaster"] = 3621,
    },
    ["Mantle of the Timbermaw"] = {
        Professions = 23663,
    },
    ["Cone of Fire"] = {
        ["Flameguard"] = 19630,
    },
    ["Boots of the Enchanter"] = {
        Professions = 3860,
    },
    ["Summon Tamed Battleboar"] = {
        ["Tamed Battleboar"] = 8274,
        ["Razorfen Handler"] = 8274,
        ["Razorfen Beast Trainer"] = 8274,
    },
    ["Mighty Blow"] = {
        ["Devilsaur"] = 14099,
        ["Plague Monstrosity"] = 14099,
        ["Irondeep Skullthumper"] = 14099,
    },
    ["Black Whelp Tunic"] = {
        Professions = 24940,
    },
    ["Create Bloodpetal Zapper"] = {
        ["Larion"] = 22565,
    },
    ["Steam Jet"] = {
        ["Dark Iron Steamsmith"] = 11983,
        ["Boiling Elemental"] = 11983,
    },
    ["Stormshroud Armor"] = {
        Professions = 19079,
    },
    ["Lightning Bolt"] = {
        ["SHAMAN"] = 15208,
        ["Cloud Serpent"] = 8246,
        ["Elder Cloud Serpent"] = 8246,
        ["Mudsnout Shaman"] = 20805,
        ["Redridge Mystic"] = 20802,
        ["Grimtotem Sorcerer"] = 20802,
        ["Daggerspine Sorceress"] = 20824,
        ["Vilebranch Soothsayer"] = 20824,
        ["Primalist Thurloga"] = 15234,
        ["Irondeep Shaman"] = 15801,
        ["Riverpaw Mystic"] = 9532,
        ["Murloc Minor Oracle"] = 9532,
        ["Bloodscalp Mystic"] = 9532,
        ["Skullsplitter Mystic"] = 9532,
        ["Saltscale Oracle"] = 9532,
        ["Mosshide Mystic"] = 9532,
        ["Riverpaw Shaman"] = 9532,
        ["Venture Co. Geologist"] = 9532,
        ["Stonesplinter Seer"] = 9532,
        ["Mo'grosh Shaman"] = 9532,
        ["Mo'grosh Mystic"] = 9532,
        ["Frostmane Seer"] = 9532,
        ["Magosh"] = 9532,
        ["Vile Fin Minor Oracle"] = 9532,
        ["Deeb"] = 9532,
        ["Bloodfeather Matriarch"] = 9532,
        ["Blackwood Shaman"] = 9532,
        ["Greymist Oracle"] = 9532,
        ["Daggerspine Siren"] = 9532,
        ["Torn Fin Oracle"] = 9532,
        ["Boulderfist Shaman"] = 9532,
        ["Dustbelcher Shaman"] = 9532,
        ["Thundering Exile"] = 9532,
        ["Dustbelcher Mystic"] = 9532,
        ["Bristleback Shaman"] = 9532,
        ["Windfury Wind Witch"] = 9532,
        ["Windfury Matriarch"] = 9532,
        ["Dustwind Storm Witch"] = 9532,
        ["Thunderhawk Hatchling"] = 9532,
        ["Kolkar Stormer"] = 9532,
        ["Nak"] = 9532,
        ["Boahn"] = 9532,
        ["Shadethicket Raincaller"] = 9532,
        ["Druid of the Fang"] = 9532,
        ["Caedakar the Vicious"] = 9532,
        ["Bloodfury Storm Witch"] = 9532,
        ["Young Chimaera"] = 9532,
        ["Galak Stormer"] = 9532,
        ["Keeper Ordanus"] = 9532,
        ["Mirefin Oracle"] = 9532,
        ["Muckshell Scrabbler"] = 9532,
        ["Razorfen Geomancer"] = 9532,
        ["Kolkar Windchaser"] = 9532,
        ["Magram Windchaser"] = 9532,
        ["Gelkis Windchaser"] = 9532,
        ["Foreman Cozzle"] = 9532,
        ["Blindlight Oracle"] = 9532,
        ["Earthcaller Halmgar"] = 9532,
        ["Roogug"] = 9532,
        ["Storm Bay Oracle"] = 9532,
        ["Mistwing Rogue"] = 9532,
        ["Kolkar Stormseer"] = 9532,
        ["Ragefire Shaman"] = 9532,
        ["Snowblind Windcaller"] = 9532,
        ["Lorgus Jett"] = 12167,
    },
    ["Elixir of Minor Defense"] = {
        Professions = 7183,
    },
    ["Fire Protection Potion"] = {
        Professions = 7257,
    },
    ["Raptor Hide Harness"] = {
        Professions = 4096,
    },
    ["Smite"] = {
        ["PRIEST"] = 10934,
    },
    ["Wound Poison II"] = {
        ["ROGUE"] = 13228,
    },
    ["Earthbind Totem"] = {
        ["SHAMAN"] = 2484,
        ["Earthbind Totem"] = 15786,
        ["Irondeep Shaman"] = 15786,
        ["Primalist Thurloga"] = 15786,
    },
    ["Toxin"] = {
        ["Toxic Slime"] = 25989,
    },
    ["Throw Wrench"] = {
        ["Leprous Technician"] = 13398,
        ["Leprous Machinesmith"] = 13398,
    },
    ["Nagmara's Vanish"] = {
        ["Mistress Nagmara"] = 15341,
        ["Private Rocknot"] = 15341,
    },
    ["Enchant Bracer - Superior Strength"] = {
        Professions = 20010,
    },
    ["Enchanted Runecloth Bag"] = {
        Professions = 27659,
    },
    ["Razor Mane"] = {
        ["Razormane Quilboar"] = 5280,
    },
    ["Warbear Harness"] = {
        Professions = 19068,
    },
    ["Summon Voidwalker"] = {
        ["WARLOCK"] = 697,
    },
    ["Greater Dreamless Sleep Potion"] = {
        Professions = 24366,
    },
    ["Portal: Stormwind"] = {
        ["MAGE"] = 10059,
    },
    ["Volcanic Breastplate"] = {
        Professions = 19076,
    },
    ["Slave Drain"] = {
        ["Mogh the Undying"] = 8809,
    },
    ["Flame Lash"] = {
        ["Red Whelp"] = 3356,
        ["Sister Riven"] = 3356,
    },
    ["Summon Magram Ravager"] = {
        ["Undead Ravager"] = 18166,
        ["Outcast Necromancer"] = 18166,
    },
    ["Shadow Resistance"] = {
        ["HUNTER"] = 24516,
    },
    ["Enchant Gloves - Agility"] = {
        Professions = 13815,
    },
    ["Bottle of Poison"] = {
        ["Tunnel Rat Forager"] = 7365,
    },
    ["Summon Silithid Grub"] = {
        ["Silithid Grub"] = 6588,
        ["Silithid Creeper Egg"] = 6588,
    },
    ["Gift of Arthas"] = {
        Professions = 11466,
    },
    ["Bright Yellow Shirt"] = {
        Professions = 3869,
    },
    ["Coarse Grinding Stone"] = {
        Professions = 3326,
    },
    ["Call Infernal Destroyer"] = {
        ["El Pollo Grande"] = 23056,
        ["Niby the Almighty"] = 23056,
    },
    ["Fiery Plate Gauntlets"] = {
        Professions = 16655,
    },
    ["Mind-numbing Poison II"] = {
        ["ROGUE"] = 8694,
    },
    ["Instant Poison V"] = {
        ["ROGUE"] = 11342,
    },
    ["Mental Domination"] = {
        ["Doctor Weavil"] = 25772,
    },
    ["Minion of Morganth"] = {
        ["Minion of Morganth"] = 3611,
        ["Morganth"] = 3611,
    },
    ["Big Bronze Knife"] = {
        Professions = 3491,
    },
    ["Druid's Slumber"] = {
        ["Boahn"] = 8040,
        ["Druid of the Fang"] = 8040,
    },
    ["Healing Touch"] = {
        ["DRUID"] = 25297,
    },
    ["Enrage"] = {
        ["DRUID"] = 5229,
        ["Gurubashi Warrior"] = 18501,
        ["Herod"] = 8269,
        ["Scarlet Myrmidon"] = 8269,
        ["Scarlet Abbot"] = 8269,
        ["Agathelos the Raging"] = 8269,
        ["Raging Agam'ar"] = 8269,
        ["Irradiated Pillager"] = 8269,
        ["Drek'Thar"] = 8269,
        ["Shadowhide Brute"] = 8599,
        ["Bloodscalp Warrior"] = 8599,
        ["Bloodscalp Scout"] = 8599,
        ["Bloodscalp Hunter"] = 8599,
        ["Bloodscalp Berserker"] = 8599,
        ["Bloodscalp Witch Doctor"] = 8599,
        ["Bloodscalp Headhunter"] = 8599,
        ["Bloodscalp Axe Thrower"] = 8599,
        ["Bloodscalp Shaman"] = 8599,
        ["Bloodscalp Beastmaster"] = 8599,
        ["Bloodscalp Mystic"] = 8599,
        ["Bloodscalp Scavenger"] = 8599,
        ["Targorr the Dread"] = 8599,
        ["Moonrage Darksoul"] = 8599,
        ["Ferocious Yeti"] = 8599,
        ["Orgrimmar Grunt"] = 8599,
        ["Dark Strand Enforcer"] = 8599,
        ["Hatefury Rogue"] = 8599,
        ["Hatefury Trickster"] = 8599,
        ["Hatefury Felsworn"] = 8599,
        ["Hatefury Betrayer"] = 8599,
        ["Hatefury Shadowstalker"] = 8599,
        ["Hatefury Hellcaller"] = 8599,
        ["Raging Thunder Lizard"] = 8599,
        ["Withered Warrior"] = 8599,
        ["Withered Reaver"] = 8599,
        ["Withered Quilguard"] = 8599,
        ["Withered Spearhide"] = 8599,
        ["Mosshoof Courser"] = 8599,
        ["Angerclaw Bear"] = 8599,
        ["Angerclaw Grizzly"] = 8599,
        ["Angerclaw Mauler"] = 8599,
        ["Raging Moonkin"] = 8599,
        ["Xabraxxis"] = 8599,
        ["Arikara"] = 8599,
        ["Draka"] = 8599,
        ["Duros"] = 8599,
        ["Enraged Reef Crawler"] = 8599,
        ["Bloodfury Ripper"] = 8599,
        ["Splintertree Raider"] = 8599,
        ["Kor'kron Elite"] = 8599,
        ["Iceblood Marshal"] = 8599,
        ["Tower Point Marshal"] = 8599,
        ["East Frostwolf Marshal"] = 8599,
        ["West Frostwolf Marshal"] = 8599,
        ["Dun Baldar North Warmaster"] = 8599,
        ["Dun Baldar South Warmaster"] = 8599,
        ["Icewing Warmaster"] = 8599,
        ["Stonehearth Warmaster"] = 8599,
        ["Blackrock Champion"] = 3019,
        ["Blackrock Shadowcaster"] = 3019,
        ["Blackrock Renegade"] = 3019,
        ["Blackrock Grunt"] = 3019,
        ["Blackrock Outrunner"] = 3019,
        ["Tharil'zun"] = 3019,
        ["Blackrock Tracker"] = 3019,
        ["Rockjaw Backbreaker"] = 3019,
        ["Berserk Trogg"] = 3019,
        ["Witherbark Berserker"] = 3019,
        ["Hammerfall Grunt"] = 3019,
        ["Blackrock Scout"] = 3019,
        ["Blackrock Sentry"] = 3019,
        ["Blackrock Hunter"] = 3019,
        ["Blackrock Summoner"] = 3019,
        ["Blackrock Gladiator"] = 3019,
    },
    ["Flaying Vine"] = {
        ["Bloodpetal Flayer"] = 14112,
    },
    ["Stone Skin"] = {
        ["Cracked Golem"] = 5810,
        ["Stone Behemoth"] = 5810,
    },
    ["Terrify"] = {
        ["Skeletal Horror"] = 7399,
        ["Sneed's Shredder"] = 7399,
        ["Scorpid Terror"] = 7399,
        ["Deviate Dreadfang"] = 7399,
        ["Azshir the Sleepless"] = 7399,
        ["Frenzied Pterrordax"] = 7399,
        ["Rak'shiri"] = 7399,
    },
    ["Low Swipe"] = {
        ["Zzarc' Vul"] = 8716,
        ["Brother Ravenoak"] = 8716,
        ["Angerclaw Grizzly"] = 8716,
    },
    ["Woolen Bag"] = {
        Professions = 3757,
    },
    ["Rough Bronze Shoulders"] = {
        Professions = 3328,
    },
    ["Gouge"] = {
        ["ROGUE"] = 11286,
    },
    ["Create Yeh'kinya's Scroll"] = {
        ["Yeh'kinya"] = 12998,
    },
    ["Mortar"] = {
        ["Mortar"] = 25003,
        ["Steam Tonk"] = 25003,
    },
    ["Draw Spirit"] = {
        ["Lethon"] = 24811,
    },
    ["Parasite"] = {
        ["Blackfathom Oracle"] = 8363,
    },
    ["Encasing Webs"] = {
        ["Plains Creeper"] = 4962,
        ["Giant Plains Creeper"] = 4962,
        ["Crypt Beast"] = 4962,
    },
    ["Enchant Weapon - Unholy Weapon"] = {
        Professions = 20033,
    },
    ["Enchant Chest - Major Mana"] = {
        Professions = 20028,
    },
    ["Arantir's Anger"] = {
        ["Arantir"] = 9733,
    },
    ["Tranquil Air Totem"] = {
        ["SHAMAN"] = 25908,
    },
    ["Get Gosssip, Test"] = {
        ["Stormwind City Guard"] = 26683,
        ["Stormwind City Patroller"] = 26683,
        ["Bluffwatcher"] = 26683,
        ["Orgrimmar Grunt"] = 26683,
        ["Arnold Leland"] = 26683,
        ["Ironforge Guard"] = 26683,
        ["Grunt Korf"] = 26683,
        ["Grunt Bek'rah"] = 26683,
    },
    ["Cold Snap"] = {
        ["MAGE"] = 12472,
    },
    ["Hibernate"] = {
        ["DRUID"] = 18658,
    },
    ["Flametongue Totem"] = {
        ["SHAMAN"] = 16387,
    },
    ["Hamstring"] = {
        ["WARRIOR"] = 7373,
        ["Skeletal Warrior"] = 9080,
        ["Brack"] = 9080,
        ["Bluegill Warrior"] = 9080,
        ["Lok Orcbane"] = 9080,
        ["Foulweald Ursa"] = 9080,
        ["Aligar the Tormentor"] = 9080,
        ["Strashaz Warrior"] = 9080,
        ["Slitherblade Warrior"] = 9080,
        ["Foreman Grills"] = 9080,
        ["Addled Leper"] = 9080,
        ["Coldmine Invader"] = 9080,
        ["Royal Dreadguard"] = 9080,
        ["Claw Tentacle"] = 26141,
    },
    ["Murloc Scale Belt"] = {
        Professions = 6702,
    },
    ["Strong Troll's Blood Potion"] = {
        Professions = 3176,
    },
    ["Arcane Vacuum"] = {
        ["Azuregos"] = 21147,
    },
    ["Thorium Shells"] = {
        Professions = 19800,
    },
    ["Frost Trap"] = {
        ["HUNTER"] = 13809,
    },
    ["Deadly Leech Poison"] = {
        ["Leech Widow"] = 3388,
        ["Sorrow Wing"] = 3388,
    },
    ["Enchant Bracer - Lesser Strength"] = {
        Professions = 13536,
    },
    ["Disturb Rookery Egg"] = {
        ["Rookery Hatcher"] = 15746,
    },
    ["Touch of Death"] = {
        ["Morbent Fel"] = 3108,
    },
    ["Fevered Fatigue"] = {
        ["Mesa Buzzard"] = 8139,
        ["Elder Mesa Buzzard"] = 8139,
        ["Mangy Silvermane"] = 8139,
        ["Windshear Geomancer"] = 8139,
        ["Windshear Overlord"] = 8139,
        ["Arkkoran Muckdweller"] = 8139,
    },
    ["Renew"] = {
        ["PRIEST"] = 25315,
        ["Dun Garok Priest"] = 8362,
        ["Narillasanz"] = 8362,
        ["Scarlet Disciple"] = 8362,
        ["Scarlet Chaplain"] = 8362,
        ["Scarlet Abbot"] = 8362,
        ["Death's Head Acolyte"] = 8362,
        ["Twilight Acolyte"] = 8362,
        ["Blindlight Oracle"] = 8362,
        ["Expeditionary Priest"] = 8362,
        ["Spitelash Siren"] = 11640,
        ["Holdout Medic"] = 22168,
    },
    ["Inner Fire"] = {
        ["PRIEST"] = 10952,
    },
    ["Ghostweave Vest"] = {
        Professions = 18416,
    },
    ["Rugged Armor Kit"] = {
        Professions = 19058,
    },
    ["Corrosive Venom Spit"] = {
        ["Chimaerok"] = 20629,
    },
    ["Belt of the Archmage"] = {
        Professions = 22866,
    },
    ["Cloak of Warding"] = {
        Professions = 22870,
    },
    ["Zombie Form"] = {
        ["Infiltrator Marksen"] = 7293,
    },
    ["Red Woolen Bag"] = {
        Professions = 6688,
    },
    ["Runed Stygian Leggings"] = {
        Professions = 24901,
    },
    ["Arugal's Gift"] = {
        ["Son of Arugal"] = 7124,
    },
    ["Summon Eliza's Guard"] = {
        ["Eliza's Guard"] = 3107,
        ["Eliza"] = 3107,
    },
    ["Abolish Disease"] = {
        ["PRIEST"] = 552,
    },
    ["Spitelash"] = {
        ["Spitelash Warrior"] = 12545,
        ["Spitelash Screamer"] = 12545,
        ["Spitelash Battlemaster"] = 12545,
    },
    ["Green Woolen Vest"] = {
        Professions = 2399,
    },
    ["Health Funnel"] = {
        ["WARLOCK"] = 11695,
    },
    ["Flamethrower"] = {
        Pet = 25027,
    },
    ["Firework Cluster Launcher"] = {
        Professions = 26443,
    },
    ["Growl"] = {
        ["DRUID"] = 6795,
        ["HUNTER"] = 14927,
        Pet = 14921,
    },
    ["Larva Goo"] = {
        ["Vile Larva"] = 21069,
    },
    ["Crystal Imprisonment"] = {
        ["Doomguard Commander"] = 23020,
    },
    ["Black Whelp Cloak"] = {
        Professions = 9070,
    },
    ["Small Silk Pack"] = {
        Professions = 3813,
    },
    ["Winterfall Firewater"] = {
        ["Winterfall Den Watcher"] = 17205,
        ["Winterfall Totemic"] = 17205,
        ["Winterfall Pathfinder"] = 17205,
    },
    ["Presence of Mind"] = {
        ["MAGE"] = 12043,
    },
    ["Enchanted Mageweave Pouch"] = {
        Professions = 27658,
    },
    ["Hardened Iron Shortsword"] = {
        Professions = 3492,
    },
    ["Arcanite Rod"] = {
        Professions = 20201,
    },
    ["Smitten"] = {
        ["Kwee Q. Peddlefeet"] = 27572,
    },
    ["Elixir of Agility"] = {
        Professions = 11449,
    },
    ["Runic Leather Gauntlets"] = {
        Professions = 19055,
    },
    ["Silvered Bronze Breastplate"] = {
        Professions = 2673,
    },
    ["Scare Beast"] = {
        ["HUNTER"] = 14327,
    },
    ["Smelt Silver"] = {
        Professions = 2658,
    },
    ["Nitrous Boost"] = {
        ["Steam Tonk"] = 27746,
    },
    ["Bolt of Linen Cloth"] = {
        Professions = 2963,
    },
    ["Transmute: Earth to Life"] = {
        Professions = 17566,
    },
    ["Fire Shield II"] = {
        ["Crimson Whelp"] = 184,
        ["Venture Co. Geologist"] = 184,
        ["Splinter Fist Firemonger"] = 184,
        ["Garneg Charskull"] = 184,
        ["Lesser Felguard"] = 184,
        ["Burning Ravager"] = 184,
        ["Blackrock Summoner"] = 184,
        ["Foreman Cozzle"] = 184,
        ["Sister Riven"] = 184,
    },
    ["Summon Charger"] = {
        ["PALADIN"] = 23214,
    },
    ["Big Black Mace"] = {
        Professions = 10001,
    },
    ["Summon Theradrim Shardling"] = {
        ["Theradrim Shardling"] = 21057,
        ["Theradrim Guardian"] = 21057,
    },
    ["Enchant Boots - Minor Agility"] = {
        Professions = 7867,
    },
    ["Formal White Shirt"] = {
        Professions = 3871,
    },
    ["Melt Armor"] = {
        ["Flameguard"] = 19631,
    },
    ["Savagery"] = {
        ["Vicious Grell"] = 5515,
        ["Bloodfeather Fury"] = 5515,
    },
    ["Enchant Gloves - Skinning"] = {
        Professions = 13698,
    },
    ["Heavy Leather"] = {
        Professions = 20649,
    },
    ["Corrupted Strength"] = {
        ["Foulweald Warrior"] = 6816,
        ["Foulweald Ursa"] = 6816,
        ["Oakpaw"] = 6816,
        ["Enraged Foulweald"] = 6816,
    },
    ["Guardian Pants"] = {
        Professions = 7147,
    },
    ["Travel Form"] = {
        ["DRUID"] = 783,
    },
    ["Create Soulstone (Lesser)"] = {
        ["WARLOCK"] = 20752,
    },
    ["Arcane Intellect"] = {
        ["MAGE"] = 10157,
    },
    ["Salt Shaker"] = {
        Professions = 19567,
    },
    ["Sarilus's Elementals"] = {
        ["Minor Water Guardian"] = 6490,
        ["Sarilus Foulborne"] = 6490,
    },
    ["Counterspell"] = {
        ["MAGE"] = 2139,
    },
    ["Shadowmeld"] = {
        Racial = 20580,
    },
    ["Geyser"] = {
        ["Hatecrest Wave Rider"] = 10987,
        ["Sea Spray"] = 10987,
        ["Princess Tempestria"] = 10987,
    },
    ["Hi-Impact Mithril Slugs"] = {
        Professions = 12596,
    },
    ["Silver Rod"] = {
        Professions = 7818,
    },
    ["Goblin Land Mine"] = {
        Professions = 3968,
    },
    ["Lava Spout Totem"] = {
        ["Lava Spout Totem"] = 8264,
        ["Death's Head Seer"] = 8264,
    },
    ["Stormbolt"] = {
        ["Vanndar Stormpike"] = 19136,
    },
    ["Enchant Shield - Greater Spirit"] = {
        Professions = 13905,
    },
    ["Gnomish Goggles"] = {
        Professions = 12897,
    },
    ["Trip"] = {
        ["Sickly Refugee"] = 101,
        ["Hillsbrad Tailor"] = 101,
    },
    ["Shield Wall"] = {
        ["WARRIOR"] = 871,
    },
    ["Mighty Rage Potion"] = {
        Professions = 17552,
    },
    ["Golden Scale Gauntlets"] = {
        Professions = 11643,
    },
    ["Greater Nature Protection Potion"] = {
        Professions = 17576,
    },
    ["Marksman Hit"] = {
        ["Dark Iron Marksman"] = 12198,
    },
    ["Fire Shield III"] = {
        ["Bloodsail Mage"] = 2601,
        ["Lo'Grosh"] = 2601,
        ["Scarlet Evoker"] = 2601,
        ["Scarlet Wizard"] = 2601,
        ["Lesser Infernal"] = 2601,
    },
    ["Runed Truesilver Rod"] = {
        Professions = 13702,
    },
    ["Arcane Elixir"] = {
        Professions = 11461,
    },
    ["Heavy Mithril Breastplate"] = {
        Professions = 9959,
    },
    ["Bolt of Silk Cloth"] = {
        Professions = 3839,
    },
    ["Enchant Cloak - Minor Resistance"] = {
        Professions = 7454,
    },
    ["Chimeric Leggings"] = {
        Professions = 19073,
    },
    ["Enchant Annals of Darrowshire"] = {
        ["Chromie"] = 17285,
    },
    ["Evasion"] = {
        ["ROGUE"] = 5277,
    },
    ["Red Mageweave Bag"] = {
        Professions = 12079,
    },
    ["Swiftness Potion"] = {
        Professions = 2335,
    },
    ["Lock Down"] = {
        ["War Golem"] = 9576,
    },
    ["Reconstruction"] = {
        ["Tor'gan"] = 4093,
    },
    ["Blue Rocket Cluster"] = {
        Professions = 26423,
    },
    ["Will of the Forsaken"] = {
        Racial = 7744,
    },
    ["Summon Dreadsteed Spirit (DND)"] = {
        ["Dreadsteed Spirit"] = 23159,
        ["Xorothian Dreadsteed"] = 23159,
    },
    ["Large Green Rocket"] = {
        Professions = 26421,
    },
    ["Enchant Weapon - Lesser Elemental Slayer"] = {
        Professions = 13655,
    },
    ["Warosh Tickle"] = {
        ["Warosh"] = 16771,
    },
    ["Rampage"] = {
        ["Agathelos the Raging"] = 8285,
    },
    ["Razelikh's Tear I"] = {
        ["Razelikh the Defiler"] = 10864,
    },
    ["Runed Stygian Boots"] = {
        Professions = 24903,
    },
    ["Revive"] = {
        ["Chained Spirit"] = 24341,
    },
    ["Long Silken Cloak"] = {
        Professions = 3861,
    },
    ["Heavy Scorpid Gauntlets"] = {
        Professions = 19064,
    },
    ["Moss Covered Hands"] = {
        ["Lake Skulker"] = 6866,
        ["Elder Lake Skulker"] = 6866,
        ["Shadethicket Moss Eater"] = 6866,
    },
    ["Azure Silk Hood"] = {
        Professions = 8760,
    },
    ["Agonizing Pain"] = {
        ["Humar the Pridelord"] = 3247,
    },
    ["Green Silk Armor"] = {
        Professions = 8784,
    },
    ["Wound Poison III"] = {
        ["ROGUE"] = 13229,
    },
    ["Runecloth Belt"] = {
        Professions = 18402,
    },
    ["Craftsman's Monocle"] = {
        Professions = 3966,
    },
    ["Blessing of Nordrassil"] = {
        ["Eris Havenfire"] = 23108,
    },
    ["Gnomish Harm Prevention Belt"] = {
        Professions = 12903,
    },
    ["Guile of the Raptor"] = {
        ["Gor'mul"] = 4153,
    },
    ["Handstitched Leather Vest"] = {
        Professions = 7126,
    },
    ["Ancient Despair"] = {
        ["Ancient Core Hound"] = 19369,
    },
    ["Throw Dynamite"] = {
        ["Venture Co. Engineer"] = 7978,
        ["Digger Flameforge"] = 7978,
        ["Shadowforge Peasant"] = 7978,
    },
    ["Lovingly Crafted Boomstick"] = {
        Professions = 3939,
    },
    ["Minor Rejuvenation Potion"] = {
        Professions = 2332,
    },
    ["Curse of Vengeance"] = {
        ["Arikara"] = 17213,
    },
    ["Sleep"] = {
        ["Severed Sleeper"] = 8399,
        ["High Inquisitor Fairbanks"] = 8399,
        ["Twilight Lord Kelris"] = 8399,
        ["Somnus"] = 20989,
        ["Wyrmkin Dreamwalker"] = 15970,
        ["Syndicate Conjuror"] = 15970,
        ["Wrathtail Priestess"] = 15970,
    },
    ["Summon Risen Lackey"] = {
        ["Risen Lackey"] = 17618,
        ["Scholomance Dark Summoner"] = 17618,
    },
    ["First Aid"] = {
        ["Medic Tamberlyn"] = 7162,
        ["Medic Helaina"] = 7162,
        ["Brother Malach"] = 7162,
    },
    ["Robe of the Archmage"] = {
        Professions = 18457,
    },
    ["Left for Dead"] = {
        ["Agathelos the Raging"] = 8555,
    },
    ["Heavy Linen Gloves"] = {
        Professions = 3840,
    },
    ["Alchemy"] = {
        Professions = 3101,
        Professions = 3464,
        Professions = 11611,
    },
    ["Summon Imp"] = {
        ["WARLOCK"] = 688,
        ["Imp Minion"] = 11939,
        ["Witherbark Shadowcaster"] = 11939,
        ["Burning Blade Cultist"] = 11939,
        ["Fizzle Darkstorm"] = 11939,
        ["Dark Strand Cultist"] = 11939,
        ["Bleakheart Hellcaller"] = 11939,
        ["Balizar the Umbrage"] = 11939,
        ["Blackrock Summoner"] = 11939,
        ["Burning Blade Summoner"] = 11939,
        ["Kayla Smithe"] = 11939,
        ["Gina Lang"] = 11939,
        ["Dane Winslow"] = 11939,
        ["Cylina Darkheart"] = 11939,
        ["Wren Darkspring"] = 11939,
        ["Morloch"] = 11939,
    },
    ["Rocket, RED BIG"] = {
        ["Pat's Firework Guy - RED BIG"] = 26354,
    },
    ["Diving Sweep"] = {
        ["Vile Bat"] = 7145,
    },
    ["Green Iron Bracers"] = {
        Professions = 3501,
    },
    ["Wither Strike"] = {
        ["Withervine Mire Beast"] = 5337,
        ["Withered Protector"] = 5337,
        ["Arei"] = 5337,
        ["Darrowshire Betrayer"] = 5337,
        ["Spectral Betrayer"] = 5337,
    },
    ["Transmute: Earth to Water"] = {
        Professions = 17561,
    },
    ["Sling Mud"] = {
        ["Ardo Dirtpaw"] = 3650,
        ["Mudsnout Gnoll"] = 3650,
    },
    ["Herb Gathering"] = {
        Professions = 2368,
        Professions = 3570,
        Professions = 11993,
    },
    ["Rough Sharpening Stone"] = {
        Professions = 2660,
    },
    ["Elixir of the Sages"] = {
        Professions = 17555,
    },
    ["Rocket, YELLOW BIG"] = {
        ["Pat's Firework Guy - YELLOW BIG"] = 26356,
    },
    ["Ward of Zanzil"] = {
        ["Ward of Zanzil"] = 8832,
        ["Zanzil the Outcast"] = 8832,
    },
    ["Pierce Armor"] = {
        ["Gobbler"] = 12097,
        ["Sharptalon"] = 12097,
        ["Duriel Moonfire"] = 12097,
        ["Kobold Miner"] = 6016,
        ["Ruklar the Trapper"] = 6016,
        ["Bone Chewer"] = 6016,
        ["Hogger"] = 6016,
        ["Brack"] = 6016,
        ["Defias Miner"] = 6016,
        ["Venture Co. Miner"] = 6016,
        ["Kobold Digger"] = 6016,
        ["Timberling Bark Ripper"] = 6016,
        ["Darkshore Thresher"] = 6016,
        ["Elder Darkshore Thresher"] = 6016,
        ["Hillsbrad Peasant"] = 6016,
        ["Wrathtail Razortail"] = 6016,
        ["Silithid Invader"] = 6016,
        ["Sparkleshell Borer"] = 6016,
        ["Withervine Bark Ripper"] = 6016,
        ["Defias Strip Miner"] = 6016,
    },
    ["Phoenix Pants"] = {
        Professions = 3851,
    },
    ["Moonfire"] = {
        ["DRUID"] = 9835,
        ["Tyranis Malem"] = 15798,
        ["Moonkin Oracle"] = 15798,
        ["Arch Druid Renferal"] = 22206,
    },
    ["Suffering"] = {
        Pet = 17752,
    },
    ["Break Stuff"] = {
        ["Fel'zerul"] = 7437,
    },
    ["Dense Sharpening Stone"] = {
        Professions = 16641,
    },
    ["Iridescent Hammer"] = {
        Professions = 6518,
    },
    ["Birth"] = {
        ["Claw Tentacle"] = 26586,
        ["Eye Tentacle"] = 26586,
    },
    ["Curse of Agony"] = {
        ["WARLOCK"] = 11713,
        ["Searing Blade Cultist"] = 18266,
        ["Balizar the Umbrage"] = 14868,
    },
    ["Tailoring"] = {
        Professions = 3909,
        Professions = 3910,
        Professions = 12180,
    },
    ["Turtle Scale Bracers"] = {
        Professions = 10518,
    },
    ["Flarecore Mantle"] = {
        Professions = 20848,
    },
    ["Ghost Dye"] = {
        Professions = 11473,
    },
    ["Summon Earth Elemental"] = {
        ["Earth Elemental"] = 19704,
        ["Twilight Stonecaller"] = 19704,
    },
    ["Last Stand"] = {
        ["WARRIOR"] = 12975,
    },
    ["Drink Minor Potion"] = {
        ["Murloc Forager"] = 3368,
        ["Riverpaw Herbalist"] = 3368,
    },
    ["Lesser Wizard's Robe"] = {
        Professions = 6690,
    },
    ["Preparation"] = {
        ["ROGUE"] = 14185,
    },
    ["Enchant 2H Weapon - Agility"] = {
        Professions = 27837,
    },
    ["Petrify"] = {
        ["Glasshide Petrifier"] = 11020,
    },
    ["Summon Atal'ai Deathwalker's Spirit"] = {
        ["Atal'ai Deathwalker's Spirit"] = 12095,
        ["Atal'ai Deathwalker"] = 12095,
    },
    ["Track Dragonkin"] = {
        ["HUNTER"] = 19879,
    },
    ["Summon Spire Spiderling"] = {
        ["Spire Spiderling"] = 16103,
        ["Spire Spider"] = 16103,
    },
    ["Summon Rend Blackhand"] = {
        ["Warchief Rend Blackhand"] = 16328,
        ["Gyth"] = 16328,
    },
    ["Green Leather Belt"] = {
        Professions = 3774,
    },
    ["Disease Touch"] = {
        ["Rotting Dead"] = 3234,
        ["Ravaged Corpse"] = 3234,
        ["Hungering Dead"] = 3234,
        ["Shambling Horror"] = 3234,
    },
    ["Nightscape Shoulders"] = {
        Professions = 10516,
    },
    ["Large Seaforium Charge"] = {
        Professions = 3972,
    },
    ["Chain Burn"] = {
        ["Irradiated Horror"] = 8211,
    },
    ["White Bandit Mask"] = {
        Professions = 12059,
    },
    ["Paralyzing Poison"] = {
        ["Chatter"] = 3609,
        ["Carrion Recluse"] = 3609,
        ["Wildspawn Trickster"] = 3609,
        ["Blood Steward of Kirtonos"] = 3609,
        ["Deathclasp"] = 3609,
    },
    ["Withering Poison"] = {
        ["Scorpid Terror"] = 13884,
        ["Scorpid Reaver"] = 13884,
    },
    ["Battle Stance"] = {
        ["WARRIOR"] = 2457,
        ["Skeletal Warrior"] = 7165,
        ["Redridge Alpha"] = 7165,
        ["Bluegill Warrior"] = 7165,
        ["Mo'grosh Enforcer"] = 7165,
        ["Master Digger"] = 7165,
        ["Dextren Ward"] = 7165,
        ["Defias Inmate"] = 7165,
        ["Naga Explorer"] = 7165,
        ["Blackwood Warrior"] = 7165,
        ["Twilight Thug"] = 7165,
        ["Razormane Defender"] = 7165,
        ["Lok Orcbane"] = 7165,
        ["Saltspittle Puddlejumper"] = 7165,
        ["Foulweald Ursa"] = 7165,
        ["Aligar the Tormentor"] = 7165,
        ["Thistlefur Ursa"] = 7165,
        ["Ruuzel"] = 7165,
        ["Overlord Ramtusk"] = 7165,
        ["Razorfen Quilguard"] = 7165,
        ["Kolkar Battle Lord"] = 7165,
        ["Slitherblade Warrior"] = 7165,
        ["Guard Edward"] = 7165,
        ["Guard Jarad"] = 7165,
        ["Guard Kahil"] = 7165,
        ["Guard Lana"] = 7165,
        ["Guard Narrisha"] = 7165,
        ["Guard Tark"] = 7165,
        ["Khan Dez'hepah"] = 7165,
        ["Khan Shaka"] = 7165,
        ["Captain Flat Tusk"] = 7165,
        ["Foreman Grills"] = 7165,
        ["Takk the Leaper"] = 7165,
        ["Hagg Taurenbane"] = 7165,
        ["Swinegart Spearhide"] = 7165,
        ["Defias Raider"] = 7165,
        ["Addled Leper"] = 7165,
        ["Gelihast"] = 7165,
        ["Gnomeregan Evacuee"] = 7165,
    },
    ["Create Eternal Quintessence"] = {
        ["Duke Hydraxis"] = 28439,
    },
    ["Rough Copper Bomb"] = {
        Professions = 3923,
    },
    ["Goblin Rocket Fuel"] = {
        Professions = 11456,
    },
    ["The Mortar: Reloaded"] = {
        Professions = 13240,
    },
    ["Crimson Silk Robe"] = {
        Professions = 8802,
    },
    ["Wither Touch"] = {
        ["Witherbark Troll"] = 4974,
        ["Witherbark Axe Thrower"] = 4974,
        ["Witherbark Headhunter"] = 4974,
        ["Witherbark Berserker"] = 4974,
    },
    ["Medium Leather"] = {
        Professions = 20648,
    },
    ["Call Pet"] = {
        ["HUNTER"] = 883,
    },
    ["Shade Visual"] = {
        ["Shade of Jin'do"] = 24313,
    },
    ["Heavy Woolen Pants"] = {
        Professions = 3850,
    },
    ["Instant Poison VI"] = {
        ["ROGUE"] = 11343,
    },
    ["Soot Covering"] = {
        ["Blackened Ancient"] = 7998,
    },
    ["Mechanical Dragonling"] = {
        Professions = 3969,
    },
    ["Bronze Tube"] = {
        Professions = 3938,
    },
    ["Enchant Cloak - Lesser Fire Resistance"] = {
        Professions = 7861,
    },
    ["Flare"] = {
        ["HUNTER"] = 1543,
    },
    ["Pilferer's Gloves"] = {
        Professions = 9148,
    },
    ["World Enlarger"] = {
        Professions = 23129,
    },
    ["Dreamweave Vest"] = {
        Professions = 12070,
    },
    ["Enchant Weapon - Lesser Striking"] = {
        Professions = 13503,
    },
    ["Daunting Growl"] = {
        ["Old Icebeard"] = 3146,
    },
    ["Corrosive Ooze"] = {
        ["Corrosive Lurker"] = 9459,
    },
    ["Uppercut"] = {
        ["War Reaver"] = 10966,
        ["Arnak Grimtotem"] = 10966,
        ["Chok'sul"] = 18072,
        ["Taragaman the Hungerer"] = 18072,
    },
    ["Green Iron Shoulders"] = {
        Professions = 3504,
    },
    ["Mana Tide Totem"] = {
        ["SHAMAN"] = 17359,
    },
    ["Summon Embers"] = {
        ["Ember"] = 10869,
        ["Burning Servant"] = 10869,
    },
    ["Lightning Cloud"] = {
        ["Kolkar Stormer"] = 6535,
        ["Shadethicket Raincaller"] = 6535,
        ["Mavoris Cloudsbreak"] = 6535,
        ["Bloodfury Storm Witch"] = 6535,
        ["Galak Stormer"] = 6535,
        ["Magram Stormer"] = 6535,
        ["Sister Rathtalon"] = 6535,
        ["Kolkar Stormseer"] = 6535,
    },
    ["Gemmed Copper Gauntlets"] = {
        Professions = 3325,
    },
    ["Summon Spawn of Bael'Gar"] = {
        ["Spawn of Bael'Gar"] = 13895,
    },
    ["Shadow Bolt Volley"] = {
        ["Varimathras"] = 20741,
        ["High Priestess Hai'watna"] = 14887,
        ["Morloch"] = 17228,
        ["Balgaras the Foul"] = 9081,
    },
    ["Frostsaber Gloves"] = {
        Professions = 19087,
    },
    ["Searing Pain"] = {
        ["WARLOCK"] = 17923,
    },
    ["Kodo Hide Bag"] = {
        Professions = 5244,
    },
    ["Fade Out"] = {
        ["Snarler"] = 5543,
    },
    ["Earthborer Acid"] = {
        ["Earthborer"] = 18070,
    },
    ["Mind-numbing Poison"] = {
        ["ROGUE"] = 5763,
    },
    ["Eject Sneed"] = {
        ["Sneed"] = 5141,
        ["Sneed's Shredder"] = 5141,
    },
    ["Create Firestone (Greater)"] = {
        ["WARLOCK"] = 17952,
    },
    ["Light Feather"] = {
        ["PRIEST"] = 17056,
    },
    ["Enchant Chest - Greater Health"] = {
        Professions = 13640,
    },
    ["Summon Hammertoe's Spirit"] = {
        ["Hammertoe's Spirit"] = 4985,
        ["Historian Karnik"] = 4985,
    },
    ["Turn Undead"] = {
        ["PALADIN"] = 10326,
    },
    ["Gnomish Mind Control Cap"] = {
        Professions = 12907,
    },
    ["Golden Scale Shoulders"] = {
        Professions = 3505,
    },
    ["Frostweave Gloves"] = {
        Professions = 18411,
    },
    ["Honor Points +228"] = {
        ["Horde Warbringer"] = 24963,
        ["Alliance Brigadier General"] = 24963,
    },
    ["Curse of the Eye"] = {
        ["Cursed Sailor"] = 10653,
        ["Cursed Marine"] = 10653,
        ["First Mate Snellig"] = 10653,
        ["Captain Halyndor"] = 10653,
        ["Cursed Sailor"] = 10651,
        ["Cursed Marine"] = 10651,
        ["First Mate Snellig"] = 10651,
        ["Captain Halyndor"] = 10651,
        ["Cursed Sailor"] = 3360,
        ["Cursed Marine"] = 3360,
        ["First Mate Snellig"] = 3360,
        ["Captain Halyndor"] = 3360,
    },
    ["Mooncloth Vest"] = {
        Professions = 18447,
    },
    ["Admiral's Hat"] = {
        Professions = 12081,
    },
    ["Innervate"] = {
        ["DRUID"] = 29166,
    },
    ["Hammer of Wrath"] = {
        ["PALADIN"] = 24239,
    },
    ["Conjure Mana Ruby"] = {
        ["MAGE"] = 10054,
    },
    ["Drain Life"] = {
        ["WARLOCK"] = 11700,
        ["Varimathras"] = 20743,
    },
    ["Mining"] = {
        Professions = 2576,
        Professions = 3564,
        Professions = 10248,
    },
    ["Spidersilk Boots"] = {
        Professions = 3855,
    },
    ["Turtle Scale Helm"] = {
        Professions = 10552,
    },
    ["Web Spin"] = {
        ["Razzashi Broodwidow"] = 24600,
    },
    ["Handstitched Leather Bracers"] = {
        Professions = 9059,
    },
    ["Flame Blast"] = {
        ["Delmanis the Hated"] = 7101,
    },
    ["Eyes of the Beast"] = {
        ["HUNTER"] = 1002,
    },
    ["Robes of Arcana"] = {
        Professions = 6692,
    },
    ["Drunken Pit Crew"] = {
        ["Goblin Pit Crewman"] = 20436,
        ["Gnome Pit Crewman"] = 20436,
        ["Gnome Pit Boss"] = 20436,
        ["Goblin Pit Boss"] = 20436,
    },
    ["Shadow Command"] = {
        ["Lord Victor Nefarius"] = 22667,
    },
    ["Bloodrage"] = {
        ["WARRIOR"] = 2687,
    },
    ["Flame Cannon"] = {
        ["Wrath Hammer Construct"] = 15575,
    },
    ["Summon Gelkis Rumbler"] = {
        ["Gelkis Rumbler"] = 9653,
        ["Gelkis Earthcaller"] = 9653,
    },
    ["Accept Surrender"] = {
        ["Thontek Rumblehoof"] = 17649,
    },
    ["Blue Glittering Axe"] = {
        Professions = 9995,
    },
    ["Enchant Boots - Stamina"] = {
        Professions = 13836,
    },
    ["Whirring Bronze Gizmo"] = {
        Professions = 3942,
    },
    ["Mithril Shield Spike"] = {
        Professions = 9939,
    },
    ["Frostweave Robe"] = {
        Professions = 18404,
    },
    ["Tuxedo Jacket"] = {
        Professions = 12093,
    },
    ["Enfeeble"] = {
        ["Wretched Lost One"] = 11963,
        ["Lingering Highborne"] = 11963,
    },
    ["Double-stitched Woolen Shoulders"] = {
        Professions = 3848,
    },
    ["Curse of Blood"] = {
        ["Insane Ghoul"] = 8282,
        ["Blood of Agamaggan"] = 8282,
        ["High Inquisitor Fairbanks"] = 8282,
        ["Ambassador Malcin"] = 12279,
    },
    ["Track Hidden"] = {
        ["HUNTER"] = 19885,
    },
    ["Water Breathing"] = {
        ["SHAMAN"] = 131,
    },
    ["Torch Toss"] = {
        ["Bael'dun Foreman"] = 6257,
    },
    ["Glinting Steel Dagger"] = {
        Professions = 15972,
    },
    ["Smelt Gold"] = {
        Professions = 3308,
    },
    ["Flames of the Black Flight"] = {
        ["Emberstrife"] = 16054,
    },
    ["Enchant Gloves - Riding Skill"] = {
        Professions = 13947,
    },
    ["Argent Boots"] = {
        Professions = 23664,
    },
    ["Unstable Trigger"] = {
        Professions = 12591,
    },
    ["Enchant Bracer - Spirit"] = {
        Professions = 13642,
    },
    ["Whirlwind"] = {
        ["WARRIOR"] = 1680,
        ["Kurzen Elite"] = 17207,
        ["Syndicate Enforcer"] = 17207,
        ["Nimar the Slayer"] = 17207,
        ["Shadowforge Warrior"] = 17207,
        ["Guard Jarad"] = 17207,
        ["Guard Kahil"] = 17207,
        ["Cyclonian"] = 17207,
        ["Keetar"] = 17207,
        ["Herod"] = 8989,
        ["Thelman Slatefist"] = 13736,
        ["Drek'Thar"] = 13736,
        ["Captain Galvangar"] = 13736,
        ["Iceblood Marshal"] = 13736,
        ["Tower Point Marshal"] = 13736,
        ["East Frostwolf Marshal"] = 13736,
        ["West Frostwolf Marshal"] = 13736,
        ["Dun Baldar North Warmaster"] = 13736,
        ["Dun Baldar South Warmaster"] = 13736,
        ["East Frostwolf Warmaster"] = 13736,
        ["Icewing Warmaster"] = 13736,
        ["Stonehearth Warmaster"] = 13736,
        ["West Frostwolf Warmaster"] = 13736,
        ["Lady Hoteshem"] = 13736,
    },
    ["Holy Light"] = {
        ["PALADIN"] = 25292,
    },
    ["Curse of Tuten'kash"] = {
        ["Tuten'kash"] = 12255,
    },
    ["Greater Blessing of Light"] = {
        ["PALADIN"] = 25890,
    },
    ["Ghostweave Gloves"] = {
        Professions = 18413,
    },
    ["Tame Beast"] = {
        ["HUNTER"] = 1515,
    },
    ["Transmute: Water to Air"] = {
        Professions = 17562,
    },
    ["Greater Blessing of Wisdom"] = {
        ["PALADIN"] = 25918,
    },
    ["Mageweave Bag"] = {
        Professions = 12065,
    },
    ["Create Spellstone (Major)"] = {
        ["WARLOCK"] = 17728,
    },
    ["Transmute: Water to Undeath"] = {
        Professions = 17564,
    },
    ["Summon Spirit of Old"] = {
        ["Spirit of Old"] = 3652,
        ["Dalaran Theurgist"] = 3652,
    },
    ["Sleepwalk"] = {
        ["Dreamroarer"] = 20668,
    },
    ["Enchant Bracer - Minor Spirit"] = {
        Professions = 7766,
    },
    ["Copper Chain Pants"] = {
        Professions = 2662,
    },
    ["Detect Invisibility"] = {
        ["WARLOCK"] = 2970,
    },
    ["Curse of the Fallen Magram"] = {
        ["Magrami Spectre"] = 18159,
    },
    ["Fine Leather Boots"] = {
        Professions = 2158,
    },
    ["Web"] = {
        ["Venom Web Spider"] = 745,
        ["Sorrow Spinner"] = 745,
        ["Deepmoss Webspinner"] = 745,
        ["Glassweb Spider"] = 745,
        ["Tomb Reaver"] = 745,
        ["Timberweb Recluse"] = 745,
        ["Rock Stalker"] = 745,
        ["Besseleth"] = 745,
        ["Mother Fang"] = 12023,
        ["Webwood Silkspinner"] = 12023,
        ["Wildthorn Stalker"] = 12023,
    },
    ["Flask of Distilled Wisdom"] = {
        Professions = 17636,
    },
    ["Mind Blast"] = {
        ["PRIEST"] = 10947,
        ["Strashaz Siren"] = 15587,
        ["Twilight Lord Kelris"] = 15587,
        ["Haunting Vision"] = 13860,
    },
    ["Fear"] = {
        ["WARLOCK"] = 6215,
    },
    ["Deadly Scope"] = {
        Professions = 12597,
    },
    ["Runecloth Boots"] = {
        Professions = 18423,
    },
    ["Lesser Invisibility"] = {
        Pet = 7870,
    },
    ["Slow Fall"] = {
        ["MAGE"] = 130,
    },
    ["Drink Potion"] = {
        ["Bartleby"] = 9956,
    },
    ["Summon Netherwalker"] = {
        ["Netherwalker"] = 22876,
        ["Wandering Eye of Kilrogg"] = 22876,
    },
    ["Heavy Scorpid Leggings"] = {
        Professions = 19075,
    },
    ["Mithril Coif"] = {
        Professions = 9961,
    },
    ["Fanatic Blade"] = {
        ["Burning Blade Fanatic"] = 5262,
    },
    ["Bruising Blow"] = {
        ["Gordok Brute"] = 22572,
    },
    ["Enchant Weapon - Lesser Beastslayer"] = {
        Professions = 13653,
    },
    ["Hex of Weakness"] = {
        ["PRIEST"] = 19285,
    },
    ["Frost Oil"] = {
        Professions = 3454,
    },
    ["Serrated Bite"] = {
        ["Core Hound"] = 19771,
    },
    ["Black Mageweave Boots"] = {
        Professions = 12073,
    },
    ["Summon Remote-Controlled Golem"] = {
        ["Remote-Controlled Golem"] = 3605,
        ["Goblin Engineer"] = 3605,
    },
    ["Enchant 2H Weapon - Impact"] = {
        Professions = 13695,
    },
    ["Dampen Magic"] = {
        ["MAGE"] = 10174,
    },
    ["Righteous Fury"] = {
        ["PALADIN"] = 25780,
    },
    ["Create Soulstone"] = {
        ["WARLOCK"] = 20755,
    },
    ["Summon Ice Totem"] = {
        ["Ice Totem"] = 18975,
        ["Wildpaw Shaman"] = 18975,
    },
    ["Soul Consumption"] = {
        ["Teremus the Devourer"] = 12667,
    },
    ["Form of the Moonstalker"] = {
        ["Terenthis"] = 6236,
    },
    ["Shield Slam"] = {
        ["WARRIOR"] = 23925,
        ["Blackrock Renegade"] = 8242,
        ["Defias Henchman"] = 8242,
        ["Kam Deepfury"] = 8242,
        ["Dragonmaw Grunt"] = 8242,
        ["Searing Blade Enforcer"] = 8242,
    },
    ["Blessing of Wisdom"] = {
        ["PALADIN"] = 25290,
    },
    ["Arcane Shot"] = {
        ["HUNTER"] = 14287,
    },
    ["Hail Storm"] = {
        ["Mechano-Frostwalker"] = 10734,
    },
    ["Radiant Belt"] = {
        Professions = 16645,
    },
    ["Chill Nova"] = {
        ["Ras Frostwhisper"] = 18099,
    },
    ["Dragonscale Gauntlets"] = {
        Professions = 10619,
    },
    ["Summon Lava Burst H"] = {
        ["Core Rat"] = 21906,
    },
    ["Seething Plague"] = {
        ["Plagued Peasant"] = 23072,
    },
    ["Mend Dragon"] = {
        ["Blackhand Dragon Handler"] = 16637,
    },
    ["SnowMaster 9000"] = {
        Professions = 21940,
    },
    ["Green Woolen Bag"] = {
        Professions = 3758,
    },
    ["E.C.A.C."] = {
        ["Deviate Shambler"] = 7970,
    },
    ["Purification Potion"] = {
        Professions = 17572,
    },
    ["Freezing Trap"] = {
        ["HUNTER"] = 14311,
    },
    ["Lesser Invisibility Potion"] = {
        Professions = 3448,
    },
    ["Bright-Eye Goggles"] = {
        Professions = 12587,
    },
    ["Reflection Field"] = {
        ["Arcane Nullifier X-21"] = 10831,
    },
    ["Knockdown"] = {
        ["Lesser Felguard"] = 18812,
        ["Strashaz Myrmidon"] = 18812,
        ["Drek'Thar"] = 19128,
        ["Boulderfist Brute"] = 11428,
        ["Felguard"] = 11428,
        ["Flagglemurk the Cruel"] = 11428,
        ["Gurubashi Warrior"] = 11428,
        ["Duriel Moonfire"] = 11428,
        ["Zzarc' Vul"] = 5164,
        ["Redridge Basher"] = 5164,
        ["Sin'Dall"] = 5164,
        ["Rockjaw Bonesnapper"] = 5164,
        ["Mo'grosh Ogre"] = 5164,
        ["Stormscale Myrmidon"] = 5164,
        ["Furious Stone Spirit"] = 5164,
        ["Angerclaw Grizzly"] = 5164,
    },
    ["Nimble Reflexes"] = {
        ["Mr. Smite"] = 6264,
        ["Bael'dun Officer"] = 6264,
        ["Scarlet Warrior"] = 3238,
    },
    ["Wing Flap"] = {
        ["Somnus"] = 12882,
    },
    ["Rallying Cry of the Dragonslayer"] = {
        ["Overlord Runthak"] = 22888,
        ["Major Mattingly"] = 22888,
        ["High Overlord Saurfang"] = 22888,
        ["Field Marshal Afrasiabi"] = 22888,
    },
    ["Major Rejuvenation Potion"] = {
        Professions = 22732,
    },
    ["Crusader's Hammer"] = {
        ["Grand Crusader Dathrohan"] = 17286,
    },
    ["Simple Dress"] = {
        Professions = 8465,
    },
    ["Crazed"] = {
        ["Wild Grell"] = 5915,
        ["Firecaller Radison"] = 5915,
    },
    ["Red Rocket Cluster"] = {
        Professions = 26425,
    },
    ["Summon Lava Burst F"] = {
        ["Core Rat"] = 21904,
    },
    ["Cindercloth Boots"] = {
        Professions = 12088,
    },
    ["Wound Poison IV"] = {
        ["ROGUE"] = 13230,
    },
    ["Ornate Mithril Breastplate"] = {
        Professions = 9972,
    },
    ["Dark Leather Tunic"] = {
        Professions = 2169,
    },
    ["Summon Treant Allies"] = {
        ["Treant Ally"] = 20702,
        ["Arch Druid Fandral Staghelm"] = 20702,
    },
    ["EZ-Thro Dynamite II"] = {
        Professions = 23069,
    },
    ["Battle Roar"] = {
        ["Moonrage Sentry"] = 6507,
        ["Pyrewood Sentry"] = 6507,
        ["Terrowulf Packlord"] = 6507,
        ["Elder Rage Scar"] = 6507,
    },
    ["Piercing Howl"] = {
        ["WARRIOR"] = 12323,
    },
    ["Touch of Vaelastrasz"] = {
        ["Vaelastrasz the Red"] = 16319,
    },
    ["Healing Stream Totem"] = {
        ["SHAMAN"] = 10463,
    },
    ["Adrenaline Rush"] = {
        ["ROGUE"] = 13750,
    },
    ["Spawn Black Drakonid"] = {
        ["Black Drakonid"] = 22654,
        ["Black Drakonid Spawner"] = 22654,
    },
    ["Target Dummy - Event 001"] = {
        ["Mortar Team Target Dummy"] = 18634,
        ["Spotter Klemmy"] = 18634,
    },
    ["Green Holiday Shirt"] = {
        Professions = 21945,
    },
    ["Enchant Chest - Superior Health"] = {
        Professions = 13858,
    },
    ["Weak Poison"] = {
        ["Young Night Web Spider"] = 6751,
        ["Night Web Spider"] = 6751,
        ["Webwood Spider"] = 6751,
        ["Scorpid Worker"] = 6751,
    },
    ["Mortar Shot"] = {
        ["Shorty"] = 16786,
    },
    ["Flash Heal"] = {
        ["PRIEST"] = 10917,
    },
    ["Track Undead"] = {
        ["HUNTER"] = 19884,
    },
    ["Intoxicating Venom"] = {
        ["Razzashi Venombrood"] = 24596,
    },
    ["Decayed Agility"] = {
        ["Corrupted Dreadmaw Crocolisk"] = 7901,
    },
    ["Bloodscalp Pet"] = {
        ["Bloodscalp Tiger"] = 3612,
        ["Bloodscalp Beastmaster"] = 3612,
    },
    ["Mithril Blunderbuss"] = {
        Professions = 12595,
    },
    ["Banishment of Scale"] = {
        ["Lord Victor Nefarius"] = 16404,
    },
    ["Honor Points +398"] = {
        ["Horde Warbringer"] = 24923,
        ["Alliance Brigadier General"] = 24923,
    },
    ["Heavy Bronze Mace"] = {
        Professions = 3296,
    },
    ["Voodoo"] = {
        ["Bom'bay"] = 17009,
    },
    ["Shining Silver Breastplate"] = {
        Professions = 2675,
    },
    ["Runecloth Shoulders"] = {
        Professions = 18449,
    },
    ["Conjure Water"] = {
        ["MAGE"] = 10140,
    },
    ["Chains of Ice"] = {
        ["Defias Wizard"] = 113,
        ["Death's Head Adept"] = 113,
        ["Archmage Ataeric"] = 512,
    },
    ["Poisoned Shot"] = {
        ["Razorfen Beastmaster"] = 8275,
        ["Kurzen Shadow Hunter"] = 8806,
        ["Bristleback Hunter"] = 8806,
    },
    ["Barbaric Iron Breastplate"] = {
        Professions = 9813,
    },
    ["Sayge's Dark Fortune of Damage"] = {
        ["Sayge"] = 23768,
    },
    ["Unholy Shield"] = {
        ["Morbent Fel"] = 8909,
    },
    ["Localized Toxin"] = {
        ["Slitherblade Naga"] = 7947,
        ["Slitherblade Warrior"] = 7947,
        ["Slitherblade Myrmidon"] = 7947,
        ["Slitherblade Razortail"] = 7947,
        ["Deviate Viper"] = 7947,
        ["Toxic Horror"] = 7947,
    },
    ["Concussion Blow"] = {
        ["WARRIOR"] = 12809,
    },
    ["Xabraxxis Demon Bag"] = {
        ["Xabraxxis"] = 19127,
    },
    ["Cured Light Hide"] = {
        Professions = 3816,
    },
    ["Blue Firework"] = {
        Professions = 23067,
    },
    ["Nauseous"] = {
        ["Chicken"] = 24919,
        ["Southshore Guard"] = 24919,
        ["Farmer Kent"] = 24919,
        ["Phin Odelic"] = 24919,
        ["Sergeant Hartman"] = 24919,
    },
    ["Soul Bite"] = {
        ["Vilebranch Soul Eater"] = 11016,
        ["Sandfury Soul Eater"] = 11016,
    },
    ["Swipe"] = {
        ["DRUID"] = 9908,
    },
    ["Catseye Elixir"] = {
        Professions = 12609,
    },
    ["Chimeric Gloves"] = {
        Professions = 19053,
    },
    ["Mirefin Fungus"] = {
        ["Mirefin Muckdweller"] = 9462,
        ["Mirefin Coastrunner"] = 9462,
    },
    ["Brown Linen Pants"] = {
        Professions = 3914,
    },
    ["Call Slavering Worg"] = {
        ["Slavering Worg"] = 7488,
    },
    ["Taunt"] = {
        ["WARRIOR"] = 355,
    },
    ["Stomp"] = {
        ["Heavy War Golem"] = 12612,
    },
    ["Elixir of Frost Power"] = {
        Professions = 21923,
    },
    ["Ignite"] = {
        ["Haggard Refugee"] = 3261,
    },
    ["Chest Pains"] = {
        ["Twilight's Hammer Torturer"] = 6945,
        ["Marshal Windsor"] = 6945,
    },
    ["Announce Zul'Farrak Zombie"] = {
        ["Zul'Farrak Dead Hero"] = 10747,
        ["Zul'Farrak Zombie"] = 10747,
    },
    ["Blink"] = {
        ["MAGE"] = 1953,
    },
    ["Revenge"] = {
        ["WARRIOR"] = 25288,
        ["Stormpike Defender"] = 19130,
        ["Frostwolf Guardian"] = 19130,
        ["Seasoned Defender"] = 19130,
        ["Seasoned Guardian"] = 19130,
        ["Veteran Defender"] = 19130,
        ["Veteran Guardian"] = 19130,
        ["Champion Guardian"] = 19130,
        ["Champion Defender"] = 19130,
        ["Corporal Noreg Stormpike"] = 19130,
        ["Stormwind Guard"] = 12170,
        ["Dal Bloodclaw"] = 12170,
    },
    ["Mind Vision"] = {
        ["PRIEST"] = 10909,
    },
    ["Conjure Mana Citrine"] = {
        ["MAGE"] = 10053,
    },
    ["Copper Chain Vest"] = {
        Professions = 3321,
    },
    ["Disjunction"] = {
        ["Burrowing Thundersnout"] = 14533,
    },
    ["Piercing Shot"] = {
        ["Dun Garok Rifleman"] = 6685,
        ["Defias Taskmaster"] = 6685,
        ["Thaurissan Agent"] = 6685,
        ["Deadwood Pathfinder"] = 6685,
        ["Shadowforge Sharpshooter"] = 6685,
    },
    ["Grace of Air Totem"] = {
        ["SHAMAN"] = 25359,
    },
    ["Runecloth Bag"] = {
        Professions = 18405,
    },
    ["Toxic Saliva"] = {
        ["Slavering Ghoul"] = 7125,
    },
    ["Might of Shahram"] = {
        ["Shahram"] = 16600,
    },
    ["Barbaric Iron Gloves"] = {
        Professions = 9820,
    },
    ["Ashcrombe's Unlock"] = {
        ["Sorcerer Ashcrombe"] = 6421,
    },
    ["Runecloth Tunic"] = {
        Professions = 18407,
    },
    ["Summon Brain Wash Totem"] = {
        ["Brain Wash Totem"] = 24262,
    },
    ["Exorcism"] = {
        ["PALADIN"] = 10314,
    },
    ["Honor Points +378"] = {
        ["Horde Warbringer"] = 24964,
        ["Alliance Brigadier General"] = 24964,
    },
    ["Enveloping Winds"] = {
        ["Daggerspine Siren"] = 6728,
        ["Witchwing Windcaller"] = 6728,
        ["Bloodfury Windcaller"] = 6728,
        ["Galak Windchaser"] = 6728,
        ["Razorfen Dustweaver"] = 6728,
        ["Kolkar Windchaser"] = 6728,
        ["Sister Rathtalon"] = 6728,
    },
    ["Drinking Barleybrew Scalder"] = {
        ["Yorus Barleybrew"] = 8554,
    },
    ["Bloodlust"] = {
        ["Hamhock"] = 6742,
        ["Crushridge Mage"] = 6742,
        ["Grel'borg the Miser"] = 6742,
        ["Lo'Grosh"] = 6742,
        ["Kolkar Bloodcharger"] = 6742,
        ["Thistlefur Shaman"] = 6742,
        ["Houndmaster Loksey"] = 6742,
        ["Burning Blade Adept"] = 6742,
        ["Taskmaster Snivvle"] = 16170,
    },
    ["Soothing Kiss"] = {
        Pet = 11785,
    },
    ["Soul Shard"] = {
        ["WARLOCK"] = 6265,
    },
    ["Inferno"] = {
        ["WARLOCK"] = 1122,
    },
    ["Skull Crack"] = {
        ["Gravelsnout Digger"] = 3551,
    },
    ["Enchant Bracer - Strength"] = {
        Professions = 13661,
    },
    ["Moss Covered Feet"] = {
        ["Lake Creeper"] = 6870,
        ["Elder Lake Creeper"] = 6870,
        ["Shadethicket Moss Eater"] = 6870,
    },
    ["Curse of the Tribes"] = {
        ["Cursed Centaur"] = 21048,
    },
    ["Green Dragonscale Breastplate"] = {
        Professions = 19050,
    },
    ["Copper Chain Belt"] = {
        Professions = 2661,
    },
    ["Flash of Light"] = {
        ["PALADIN"] = 19943,
    },
    ["Thrash"] = {
        ["Riverpaw Outrunner"] = 3391,
        ["Edwin VanCleef"] = 3391,
        ["Mr. Smite"] = 3391,
        ["Tunnel Rat Kobold"] = 3391,
        ["Targorr the Dread"] = 3391,
        ["Blackwood Pathfinder"] = 3391,
        ["Raging Reef Crawler"] = 3391,
        ["Highland Thrasher"] = 3391,
        ["Dustbelcher Mauler"] = 3391,
        ["Feral Crag Coyote"] = 3391,
        ["Sunscale Scytheclaw"] = 3391,
        ["Bael'dun Officer"] = 3391,
        ["Razormane Pathfinder"] = 3391,
        ["Foulweald Den Watcher"] = 3391,
        ["Bloodfury Roguefeather"] = 3391,
        ["Strashaz Hydra"] = 3391,
        ["Murk Thresher"] = 3391,
        ["Elder Murk Thresher"] = 3391,
        ["Fardel Dabyrie"] = 3391,
        ["Scarlet Monk"] = 3391,
        ["Kolkar Mauler"] = 3391,
        ["Magram Mauler"] = 3391,
        ["Centipaar Wasp"] = 3391,
        ["Centipaar Stinger"] = 3391,
        ["Centipaar Swarmer"] = 3391,
        ["Caverndeep Pillager"] = 3391,
        ["Wavethrasher"] = 3391,
        ["Great Wavethrasher"] = 3391,
        ["War Reaver"] = 3391,
        ["Ferocitas the Dream Eater"] = 3391,
        ["Felpaw Ravager"] = 3391,
        ["Moonkin"] = 3391,
        ["Rorgish Jowl"] = 3391,
        ["Blackwood Tracker"] = 3391,
        ["Stonelash Flayer"] = 3391,
        ["Lunaclaw"] = 3391,
        ["Vorsha the Lasher"] = 3391,
        ["Frostwolf Bloodhound"] = 3391,
        ["Stormpike Owl"] = 3391,
        ["Harb Foulmountain"] = 3391,
    },
    ["Aspect of the Cheetah"] = {
        ["HUNTER"] = 5118,
    },
    ["Enchanted Leather"] = {
        Professions = 17181,
    },
    ["Simple Linen Boots"] = {
        Professions = 12045,
    },
    ["Enchant Shield - Lesser Stamina"] = {
        Professions = 13631,
    },
    ["Ground Smash"] = {
        ["Grol the Destroyer"] = 12734,
        ["Obsidion"] = 12734,
        ["Gor'tesh"] = 12734,
    },
    ["Corruption"] = {
        ["WARLOCK"] = 25311,
        Professions = 16985,
    },
    ["Dark Iron Mail"] = {
        Professions = 15293,
    },
    ["Electrified Net"] = {
        ["Mechanized Guardian"] = 11820,
    },
    ["Living Action Potion"] = {
        Professions = 24367,
    },
    ["Big Voodoo Mask"] = {
        Professions = 10531,
    },
    ["Create Healthstone (Minor)"] = {
        ["WARLOCK"] = 6201,
    },
    ["Banshee Shriek"] = {
        ["Eldreth Spirit"] = 16838,
    },
    ["Hide"] = {
        ["Wildthorn Lurker"] = 6920,
        ["Hatefury Rogue"] = 6920,
    },
    ["Earthgrab Totem"] = {
        ["Earthgrab Totem"] = 8376,
        ["Bloodscalp Witch Doctor"] = 8376,
        ["Razorfen Totemic"] = 8376,
        ["Thistleshrub Rootshaper"] = 8376,
    },
    ["Flask of Supreme Power"] = {
        Professions = 17637,
    },
    ["Lethal Toxin"] = {
        ["Heartrazor"] = 8256,
    },
    ["Mortar Animate"] = {
        ["Shorty"] = 18655,
    },
    ["Enchant 2H Weapon - Superior Impact"] = {
        Professions = 20030,
    },
    ["Jumping Lightning"] = {
        ["Maraudine Stormer"] = 9654,
    },
    ["Viper Sting"] = {
        ["HUNTER"] = 14280,
    },
    ["Goblin Mining Helmet"] = {
        Professions = 12717,
    },
    ["Challenging Roar"] = {
        ["DRUID"] = 5209,
    },
    ["Scorch"] = {
        ["MAGE"] = 10207,
    },
    ["Create Healthstone (Lesser)"] = {
        ["WARLOCK"] = 6202,
    },
    ["Linen Boots"] = {
        Professions = 2386,
    },
    ["Silvered Bronze Shoulders"] = {
        Professions = 3330,
    },
    ["Venom Sting"] = {
        ["Vile Sting"] = 8257,
        ["Venomtail Scorpid"] = 5416,
        ["Scorpashi Snapper"] = 5416,
        ["Scorpashi Lasher"] = 5416,
        ["Scorpashi Venomlash"] = 5416,
        ["Scorpid Dunestalker"] = 5416,
        ["Besseleth"] = 5416,
    },
    ["Embossed Leather Vest"] = {
        Professions = 2160,
    },
    ["White Swashbuckler's Shirt"] = {
        Professions = 8483,
    },
    ["Gyromatic Micro-Adjustor"] = {
        Professions = 12590,
    },
    ["Heavy Copper Maul"] = {
        Professions = 7408,
    },
    ["Garrote"] = {
        ["ROGUE"] = 11290,
    },
    ["Transmute: Arcanite"] = {
        Professions = 17187,
    },
    ["Acid Spit"] = {
        ["Adolescent Whelp"] = 20821,
        ["Dreaming Whelp"] = 20821,
    },
    ["Summon Gorishi Grub"] = {
        ["Gorishi Grub"] = 14206,
        ["Gorishi Egg"] = 14206,
    },
    ["Pummel"] = {
        ["WARRIOR"] = 6554,
    },
    ["Summon Lava Burst C"] = {
        ["Core Rat"] = 21901,
    },
    ["Gnomish Shrink Ray"] = {
        Professions = 12899,
    },
    ["Create Scepter of Celebras"] = {
        ["Celebras the Redeemed"] = 21939,
    },
    ["Great Stamina"] = {
        ["HUNTER"] = 5049,
    },
    ["Minions of Malathrom"] = {
        ["Sludge"] = 3537,
        ["Lord Malathrom"] = 3537,
    },
    ["Dark Iron Bomb"] = {
        Professions = 19799,
    },
    ["Summon Scarlet Trooper"] = {
        ["Scarlet Trooper"] = 19722,
        ["Scarlet Trooper"] = 19722,
    },
    ["Potion Toss"] = {
        ["Dalaran Brewmaster"] = 7638,
    },
    ["Transmute: Fire to Earth"] = {
        Professions = 17560,
    },
    ["Icy Cloak"] = {
        Professions = 3862,
    },
    ["Fist of Stone"] = {
        ["Boulderfist Ogre"] = 4955,
        ["Boulderfist Enforcer"] = 4955,
        ["Boulderfist Brute"] = 4955,
        ["Boulderfist Mauler"] = 4955,
        ["Boulderfist Lord"] = 4955,
    },
    ["Barbaric Iron Shoulders"] = {
        Professions = 9811,
    },
    ["Fireball"] = {
        ["MAGE"] = 25306,
        ["Captain Balinda Stonehearth"] = 12466,
        ["Black Dragon Whelp"] = 20793,
        ["Defias Renegade Mage"] = 20793,
        ["Kobold Geomancer"] = 20793,
        ["Surena Caledon"] = 20793,
        ["Scarlet Initiate"] = 20793,
        ["Dalaran Mage"] = 20793,
        ["Bloodfeather Sorceress"] = 20793,
        ["Bristleback Geomancer"] = 20793,
        ["Razormane Geomancer"] = 20793,
        ["Venture Co. Deforester"] = 20793,
        ["Blackrock Summoner"] = 20793,
        ["Gibblewilt"] = 20793,
        ["Gogger Geomancer"] = 20793,
        ["Syndicate Wizard"] = 20815,
        ["Dalaran Summoner"] = 20815,
        ["Gelkis Earthcaller"] = 20815,
        ["Morganth"] = 20811,
        ["Defias Enchanter"] = 20811,
        ["Magistrate Burnside"] = 20811,
        ["Grimtotem Geomancer"] = 20811,
        ["Bloodsail Mage"] = 20823,
        ["Warden Belamoore"] = 20823,
        ["Feeboz"] = 20823,
        ["Dark Iron Geologist"] = 20823,
        ["Jaedenar Adept"] = 20823,
        ["Coldmine Explorer"] = 15242,
        ["Irondeep Explorer"] = 15242,
        ["Whitewhisker Geomancer"] = 15228,
        ["Defias Conjurer"] = 9053,
        ["Defias Magician"] = 9053,
        ["Xavian Hellcaller"] = 9053,
        ["Burning Destroyer"] = 9053,
        ["Scarlet Magician"] = 9053,
        ["Scarlet Evoker"] = 9053,
        ["Scarlet Diviner"] = 9053,
        ["Scarlet Conjuror"] = 9053,
        ["Searing Whelp"] = 9053,
        ["Defias Wizard"] = 9053,
        ["Shadowforge Surveyor"] = 9053,
        ["Death's Head Geomancer"] = 9053,
        ["Singe"] = 13375,
        ["Black Broodling"] = 13375,
        ["Scalding Broodling"] = 13375,
        ["Defias Pillager"] = 19816,
        ["Splinter Fist Fire Weaver"] = 19816,
        ["Gravelflint Geomancer"] = 19816,
        ["Burning Blade Adept"] = 19816,
        ["Vile Familiar"] = 11921,
        ["Twilight Geomancer"] = 14034,
        ["Black Wyrmkin"] = 14034,
        ["Greater Lava Spider"] = 11985,
        ["Lost Whelp"] = 11839,
    },
    ["Copper Modulator"] = {
        Professions = 3926,
    },
    ["Sprint"] = {
        ["ROGUE"] = 11305,
    },
    ["Tuxedo Pants"] = {
        Professions = 12089,
    },
    ["Corrosive Acid"] = {
        ["Gravelsnout Forager"] = 8245,
    },
    ["Lightwell"] = {
        ["PRIEST"] = 27871,
    },
    ["Battle Shout"] = {
        ["WARRIOR"] = 25289,
        ["Narg the Taskmaster"] = 9128,
        ["Scalebane Captain"] = 9128,
        ["Mosshide Alpha"] = 9128,
        ["Defias Insurgent"] = 9128,
        ["Bazil Thredd"] = 9128,
        ["Ma'ruk Wyrmscale"] = 9128,
        ["Kolkar Pack Runner"] = 9128,
        ["Venture Co. Enforcer"] = 9128,
        ["Captain Fairmount"] = 9128,
        ["Overlord Ramtusk"] = 9128,
    },
    ["Smelt Bronze"] = {
        Professions = 2659,
    },
    ["Master Engineer's Goggles"] = {
        Professions = 19825,
    },
    ["Create Filled Containment Coffer"] = {
        ["Rift Spawn"] = 9010,
    },
    ["Arantir's Rage"] = {
        ["Arantir"] = 9741,
    },
    ["Felcloth Pants"] = {
        Professions = 18419,
    },
    ["Fevered Plague"] = {
        ["Mummified Atal'ai"] = 16186,
    },
    ["Summon Syndicate Spectre"] = {
        ["Syndicate Spectre"] = 3722,
        ["Bookie Herod"] = 3722,
    },
    ["Red Linen Robe"] = {
        Professions = 2389,
    },
    ["Gnomish Death Ray"] = {
        Professions = 12759,
    },
    ["Anti-Magic Shield"] = {
        ["Shadowfang Moonwalker"] = 7121,
    },
    ["Rock Skin"] = {
        ["Seer Ravenfeather"] = 8314,
        ["Kranal Fiss"] = 8314,
    },
    ["Elixir of Minor Fortitude"] = {
        Professions = 2334,
    },
    ["Summon Blackhand Dreadweaver"] = {
        ["Summoned Blackhand Dreadweaver"] = 15794,
        ["Blackhand Summoner"] = 15794,
    },
    ["Enchant Boots - Greater Stamina"] = {
        Professions = 20020,
    },
    ["Smoking Heart of the Mountain"] = {
        Professions = 15596,
    },
    ["Noxious Catalyst"] = {
        ["Corrupted Scorpid"] = 5413,
    },
    ["Summon Living Flame"] = {
        ["Living Flame"] = 5110,
        ["Defias Magician"] = 5110,
    },
    ["Readiness"] = {
        ["HUNTER"] = 23989,
    },
    ["Silver Skeleton Key"] = {
        Professions = 19666,
    },
    ["Embossed Leather Boots"] = {
        Professions = 2161,
    },
    ["Target Dummy"] = {
        Professions = 3932,
    },
    ["Black Mageweave Gloves"] = {
        Professions = 12053,
    },
    ["Rocket, WHITE BIG"] = {
        ["Pat's Firework Guy - WHITE BIG"] = 26355,
    },
    ["Red Mageweave Headband"] = {
        Professions = 12084,
    },
    ["Rough Blasting Powder"] = {
        Professions = 3918,
    },
    ["Flame Spray"] = {
        ["Inferno Elemental"] = 10733,
        ["Mechano-Flamewalker"] = 10733,
        ["Thaurissan Firewalker"] = 10733,
    },
    ["Tranquil Mechanical Yeti"] = {
        Professions = 26011,
    },
    ["Summon Succubus"] = {
        ["WARLOCK"] = 712,
    },
    ["Destroy Karang's Banner"] = {
        ["Enraged Foulweald"] = 20786,
    },
    ["Black Mageweave Leggings"] = {
        Professions = 12049,
    },
    ["Venom Spit"] = {
        ["Fledgling Chillwind"] = 16552,
        ["Mystlash Hydra"] = 6917,
        ["Wildthorn Venomspitter"] = 6917,
    },
    ["Deviate Scale Gloves"] = {
        Professions = 7954,
    },
    ["Shadow Shield"] = {
        ["Krethis Shadowspinner"] = 12040,
    },
    ["Enrage Trigger"] = {
        ["Firesworn"] = 19515,
    },
    ["Summon Water Elemental"] = {
        ["Summoned Water Elemental"] = 17162,
        ["Crimson Conjuror"] = 17162,
    },
    ["Superior Healing Potion"] = {
        Professions = 11457,
    },
    ["Summon Zombie"] = {
        ["Summoned Zombie"] = 16590,
        ["Dark Summoner"] = 16590,
    },
    ["King of the Gordok"] = {
        ["Mizzle the Crafty"] = 22799,
    },
    ["Instant Poison III"] = {
        ["ROGUE"] = 8691,
    },
    ["Major Healing Potion"] = {
        Professions = 17556,
    },
    ["Gift of Ragnaros"] = {
        ["Dark Iron Dwarf"] = 7891,
        ["Dark Iron Saboteur"] = 7891,
        ["Dark Iron Tunneler"] = 7891,
        ["Dark Iron Demolitionist"] = 7891,
        ["Dark Iron Rifleman"] = 7891,
    },
    ["Enchant Bracer - Stamina"] = {
        Professions = 13648,
    },
    ["Chain Bolt"] = {
        ["Charlga Razorflank"] = 8292,
    },
    ["Cower"] = {
        ["DRUID"] = 9892,
        Pet = 16697,
    },
    ["Biletoad Infection"] = {
        ["Biletoad"] = 10251,
    },
    ["Hammertoe's Sacrifice"] = {
        ["Hammertoe Grez"] = 4984,
    },
    ["Firework Launcher"] = {
        Professions = 26442,
    },
    ["Deadly Bronze Poniard"] = {
        Professions = 3295,
    },
    ["Flask of Petrification"] = {
        Professions = 17634,
    },
    ["Lesser Wizard Oil"] = {
        Professions = 25126,
    },
    ["Blue Dragonscale Breastplate"] = {
        Professions = 19077,
    },
    ["Swoop"] = {
        ["Dire Condor"] = 5708,
        ["Vultros"] = 5708,
        ["Wiry Swoop"] = 5708,
        ["Swoop"] = 5708,
        ["Taloned Swoop"] = 5708,
        ["Pridewing Skyhunter"] = 5708,
        ["Dread Swoop"] = 5708,
    },
    ["Ground Stomp"] = {
        ["Ancient Core Hound"] = 19364,
    },
    ["Heavy Mithril Gauntlet"] = {
        Professions = 9928,
    },
    ["Create Krom'zar's Banner"] = {
        ["Warlord Krom'zar"] = 13965,
    },
    ["Ilkrud's Guardians"] = {
        ["Voidwalker"] = 6487,
        ["Ilkrud Magthrull"] = 6487,
    },
    ["Enchant Cloak - Greater Resistance"] = {
        Professions = 20014,
    },
    ["Holy Smite"] = {
        ["Coldmine Surveyor"] = 15498,
        ["Irondeep Surveyor"] = 15498,
        ["Veteran Coldmine Surveyor"] = 15498,
        ["Veteran Irondeep Surveyor"] = 15498,
        ["Champion Irondeep Surveyor"] = 15498,
        ["Murloc Oracle"] = 9734,
        ["Stormscale Siren"] = 9734,
        ["Dun Garok Priest"] = 9734,
        ["Theramore Preserver"] = 9734,
        ["Forsaken Seeker"] = 9734,
        ["Scarlet Disciple"] = 9734,
        ["Scarlet Adept"] = 9734,
        ["Nethergarde Cleric"] = 9734,
        ["Expeditionary Priest"] = 9734,
    },
    ["Summon Carrion Scarab"] = {
        ["Carrion Scarab"] = 17169,
        ["Crypt Horror"] = 17169,
    },
    ["Maggot Slime"] = {
        ["Carrion Grub"] = 16449,
        ["Carrion Devourer"] = 16449,
        ["Plagued Maggot"] = 16449,
    },
    ["Bloodvine Leggings"] = {
        Professions = 24092,
    },
    ["Thieves' Tool Rack Conjure"] = {
        ["Wrenix's Gizmotronic Apparatus"] = 9949,
    },
    ["Corehound Boots"] = {
        Professions = 20853,
    },
    ["Simple Black Dress"] = {
        Professions = 12077,
    },
    ["Tough Scorpid Helm"] = {
        Professions = 10570,
    },
    ["Create Healthstone"] = {
        ["WARLOCK"] = 5699,
    },
    ["Hammer of Justice"] = {
        ["PALADIN"] = 10308,
    },
    ["Felcloth Hood"] = {
        Professions = 18442,
    },
    ["Festering Bites"] = {
        ["Plagued Insect"] = 16460,
    },
    ["Revive Pet"] = {
        ["HUNTER"] = 982,
    },
    ["Concentration Aura"] = {
        ["PALADIN"] = 19746,
    },
    ["Noxious Breath"] = {
        ["Ysondre"] = 24818,
        ["Lethon"] = 24818,
        ["Emeriss"] = 24818,
        ["Taerar"] = 24818,
    },
    ["Exploding Shot"] = {
        ["Captain Keelhaul"] = 7896,
        ["Scarlet Beastmaster"] = 7896,
        ["Weapon Technician"] = 7896,
        ["Wildspawn Betrayer"] = 7896,
    },
    ["Enchant Chest - Minor Absorption"] = {
        Professions = 7426,
    },
    ["Ritual of Doom"] = {
        ["WARLOCK"] = 18540,
    },
    ["Disengage"] = {
        ["HUNTER"] = 14273,
    },
    ["Enchant Bracer - Lesser Stamina"] = {
        Professions = 13501,
    },
    ["Shadoweave Boots"] = {
        Professions = 12082,
    },
    ["Immolation Trap"] = {
        ["HUNTER"] = 14305,
    },
    ["Frost Resistance"] = {
        ["HUNTER"] = 24478,
    },
    ["Wound Poison"] = {
        ["ROGUE"] = 13220,
    },
    ["Dark Leather Pants"] = {
        Professions = 7135,
    },
    ["Crimson Silk Belt"] = {
        Professions = 8772,
    },
    ["Conjure Staff of Command"] = {
        ["Raze"] = 15539,
    },
    ["Shield Bash"] = {
        ["WARRIOR"] = 1672,
        ["Defias Pathstalker"] = 11972,
        ["Gath'Ilzogg"] = 11972,
        ["Corporal Keeshan"] = 11972,
        ["Bloodscalp Warrior"] = 11972,
        ["Skullsplitter Warrior"] = 11972,
        ["Syndicate Sentry"] = 11972,
        ["Kul Tiras Marine"] = 11972,
        ["Scarlet Defender"] = 11972,
        ["Razorfen Defender"] = 11972,
        ["Khan Jehn"] = 11972,
        ["Watch Commander Zalaphil"] = 11972,
        ["Royal Dreadguard"] = 11972,
    },
    ["Create Tuber"] = {
        ["Snufflenose Gopher"] = 6900,
    },
    ["Blessing of Light"] = {
        ["PALADIN"] = 19979,
    },
    ["Terrifying Howl"] = {
        ["Nefaru"] = 8715,
        ["Rockwing Screecher"] = 8715,
        ["Mist Howler"] = 8715,
    },
    ["Pick Pocket"] = {
        ["ROGUE"] = 921,
    },
    ["Stinging Trauma"] = {
        ["Artorius the Amiable"] = 23299,
    },
    ["Runecloth Headband"] = {
        Professions = 18444,
    },
    ["Smelt Mithril"] = {
        Professions = 10097,
    },
    ["Mithril Frag Bomb"] = {
        Professions = 12603,
    },
    ["Quick Sidestep"] = {
        ["Encrusted Surf Crawler"] = 5426,
    },
    ["Enchant 2H Weapon - Lesser Intellect"] = {
        Professions = 7793,
    },
    ["Blood Pact"] = {
        Pet = 11767,
    },
    ["Goblin Rocket Fuel Recipe"] = {
        Professions = 12715,
    },
    ["Track Elementals"] = {
        ["HUNTER"] = 19880,
    },
    ["Golden Scale Boots"] = {
        Professions = 3515,
    },
    ["Speak with Heads"] = {
        ["Kin'weelay"] = 3644,
    },
    ["Enchant Chest - Greater Mana"] = {
        Professions = 13663,
    },
    ["Sniper Scope"] = {
        Professions = 12620,
    },
    ["Green Firework"] = {
        Professions = 23068,
    },
    ["Battle Squawk"] = {
        ["Battle Chicken"] = 23060,
    },
    ["Enveloping Webs"] = {
        ["High Priestess Mar'li"] = 24110,
    },
    ["Mark of the Wild"] = {
        ["DRUID"] = 9885,
    },
    ["Large Copper Bomb"] = {
        Professions = 3937,
    },
    ["Flamestrike"] = {
        ["MAGE"] = 10216,
        ["Splinter Fist Fire Weaver"] = 20296,
        ["Singe"] = 12468,
        ["Bristleback Geomancer"] = 20794,
        ["Grimtotem Geomancer"] = 20813,
        ["Defias Evoker"] = 11829,
        ["Burning Blade Seer"] = 11829,
    },
    ["Barbaric Leggings"] = {
        Professions = 7149,
    },
    ["Rejuvenation"] = {
        ["DRUID"] = 25299,
        ["Arch Druid Renferal"] = 15981,
        ["Wyrmkin Dreamwalker"] = 12160,
        ["Grimtotem Sorcerer"] = 12160,
    },
    ["Cured Rugged Hide"] = {
        Professions = 19047,
    },
    ["Suicide"] = {
        ["Silithid Creeper Egg"] = 7,
    },
    ["Deadly Poison V"] = {
        ["ROGUE"] = 25347,
    },
    ["Disarm Trap"] = {
        ["ROGUE"] = 1842,
    },
    ["Heavy Mithril Helm"] = {
        Professions = 9970,
    },
    ["Remove Lesser Curse"] = {
        ["MAGE"] = 475,
    },
    ["Honor Points +50"] = {
        ["Horde Warbringer"] = 24960,
        ["Alliance Brigadier General"] = 24960,
    },
    ["Bite"] = {
        Pet = 17261,
    },
    ["Putrid Enzyme"] = {
        ["Borer Beetle"] = 14539,
    },
    ["Bolt of Woolen Cloth"] = {
        Professions = 2964,
    },
    ["Devilsaur Gauntlets"] = {
        Professions = 19084,
    },
    ["Windfury Weapon"] = {
        ["SHAMAN"] = 16362,
    },
    ["Frost Nova"] = {
        ["MAGE"] = 10230,
        ["Vile Fin Tidehunter"] = 12748,
        ["Bristleback Water Seeker"] = 12748,
        ["Vengeful Surge"] = 14907,
        ["Strashaz Sorceress"] = 15532,
        ["Baron Aquanis"] = 15531,
        ["Murloc Tidehunter"] = 11831,
        ["Eliza"] = 11831,
        ["Balgaras the Foul"] = 11831,
        ["Vile Fin Tidecaller"] = 11831,
        ["Baron Vardus"] = 11831,
        ["Boulderfist Magus"] = 11831,
        ["Kuz"] = 11831,
        ["Wrathtail Sorceress"] = 11831,
        ["Befouled Water Elemental"] = 11831,
        ["Spitelash Siren"] = 11831,
        ["Tideress"] = 11831,
    },
    ["Distracting Shot"] = {
        ["HUNTER"] = 15632,
    },
    ["Wicked Leather Bracers"] = {
        Professions = 19052,
    },
    ["Greater Stoneshield Potion"] = {
        Professions = 17570,
    },
    ["Volcanic Leggings"] = {
        Professions = 19059,
    },
    ["Fire Ward"] = {
        ["MAGE"] = 10225,
    },
    ["Barbaric Gloves"] = {
        Professions = 3771,
    },
    ["Whirling Trip"] = {
        ["Gurubashi Headhunter"] = 24048,
        ["Skullsplitter Speaker"] = 24048,
    },
    ["Strong Cleave"] = {
        ["Silithid Ravager"] = 8255,
        ["Razelikh the Defiler"] = 8255,
    },
    ["Hukku's Guardians"] = {
        ["Hukku's Imp"] = 12790,
        ["Hukku"] = 12790,
    },
    ["Enchant Cloak - Lesser Agility"] = {
        Professions = 13882,
    },
    ["Avatar"] = {
        ["Vanndar Stormpike"] = 19135,
    },
    ["Ritual of Summoning"] = {
        ["WARLOCK"] = 698,
    },
    ["Robe of Winter Night"] = {
        Professions = 18436,
    },
    ["Heavy Earthen Gloves"] = {
        Professions = 9149,
    },
    ["Incite Flames"] = {
        ["Firewalker"] = 19635,
    },
    ["Iron Strut"] = {
        Professions = 3958,
    },
    ["Cindercloth Robe"] = {
        Professions = 12069,
    },
    ["Ghostweave Pants"] = {
        Professions = 18441,
    },
    ["Gift of the Xavian"] = {
        ["Xavian Rogue"] = 6925,
        ["Xavian Betrayer"] = 6925,
        ["Xavian Felsworn"] = 6925,
        ["Xavian Hellcaller"] = 6925,
        ["Prince Raze"] = 6925,
    },
    ["Bone Shards"] = {
        ["Wandering Skeleton"] = 17014,
    },
    ["Big Bronze Bomb"] = {
        Professions = 3950,
    },
    ["Heavy Mithril Shoulder"] = {
        Professions = 9926,
    },
    ["Haunting Phantoms"] = {
        ["Spectral Citizen"] = 16336,
        ["Ghostly Citizen"] = 16336,
    },
    ["Throw Liquid Fire"] = {
        ["Aerie Gryphon"] = 23969,
        ["War Rider"] = 23969,
    },
    ["Light Leather Bracers"] = {
        Professions = 9065,
    },
    ["Enchanted Battlehammer"] = {
        Professions = 16973,
    },
    ["Tough Scorpid Bracers"] = {
        Professions = 10533,
    },
    ["Greater Magic Wand"] = {
        Professions = 14807,
    },
    ["Rising Spirit"] = {
        ["Mangletooth"] = 10767,
    },
    ["Blinding Powder"] = {
        ["ROGUE"] = 6510,
    },
    ["Teleport: Stormwind"] = {
        ["MAGE"] = 3561,
    },
    ["Rapid Fire"] = {
        ["HUNTER"] = 3045,
    },
    ["Defensive Stance"] = {
        ["WARRIOR"] = 71,
        ["Garrick Padfoot"] = 7164,
        ["Gath'Ilzogg"] = 7164,
        ["Shadowhide Warrior"] = 7164,
        ["Dragonmaw Centurion"] = 7164,
        ["Dark Iron Tunneler"] = 7164,
        ["Stonesplinter Digger"] = 7164,
        ["Tunnel Rat Digger"] = 7164,
        ["Scarlet Vanguard"] = 7164,
        ["Captain Vachon"] = 7164,
        ["Kam Deepfury"] = 7164,
        ["Pyrewood Sentry"] = 7164,
        ["Hillsbrad Footman"] = 7164,
        ["Hillsbrad Miner"] = 7164,
        ["Daggerspine Shorestalker"] = 7164,
        ["Drywhisker Digger"] = 7164,
        ["Stromgarde Defender"] = 7164,
        ["Shadowforge Tunneler"] = 7164,
        ["Lieutenant Benedict"] = 7164,
        ["Bael'dun Excavator"] = 7164,
        ["Theramore Marine"] = 7164,
        ["Saltspittle Warrior"] = 7164,
        ["Scarlet Guardsman"] = 7164,
        ["Scarlet Defender"] = 7164,
        ["Razorfen Defender"] = 7164,
        ["Blackrock Gladiator"] = 7164,
        ["Marcel Dabyrie"] = 7164,
        ["Quilguard Champion"] = 7164,
        ["Fallenroot Satyr"] = 7164,
        ["Blindlight Murloc"] = 7164,
        ["Shadowforge Digger"] = 7164,
        ["Guard Edward"] = 7164,
        ["Guard Jarad"] = 7164,
        ["Guard Kahil"] = 7164,
        ["Guard Narrisha"] = 7164,
        ["Guard Tark"] = 7164,
        ["Khan Jehn"] = 7164,
        ["Watch Commander Zalaphil"] = 7164,
        ["Vejrek"] = 7164,
        ["Caverndeep Burrower"] = 7164,
        ["Dun Garok Soldier"] = 7164,
        ["Refuge Pointe Defender"] = 7164,
    },
    ["Diseased Spit"] = {
        ["Plagued Hatchling"] = 17745,
    },
    ["Mechanical Squirrel"] = {
        Professions = 3928,
    },
    ["Goblin Construction Helmet"] = {
        Professions = 12718,
    },
    ["Forsaken Skills"] = {
        ["Tormented Officer"] = 7054,
    },
    ["Fatal Sting"] = {
        ["Crypt Slayer"] = 17170,
        ["Krellack"] = 17170,
    },
    ["Pearl-handled Dagger"] = {
        Professions = 6517,
    },
    ["Soul Fire"] = {
        ["WARLOCK"] = 17924,
    },
    ["Greater Dispel"] = {
        ["High Priest Thel'danis"] = 24997,
    },
    ["Razorhide"] = {
        ["Mangletooth"] = 16610,
    },
    ["Ice Barrier"] = {
        ["MAGE"] = 13033,
    },
    ["Stylish Blue Shirt"] = {
        Professions = 7892,
    },
    ["Claw Grasp"] = {
        ["Makrura Snapclaw"] = 5424,
        ["Makrura Elder"] = 5424,
    },
    ["Enchant Weapon - Fiery Weapon"] = {
        Professions = 13898,
    },
    ["Retaliation"] = {
        ["WARRIOR"] = 20230,
    },
    ["Summon Foulweald Totem Basket"] = {
        ["Chief Murgut"] = 20818,
    },
    ["Frostbrand Weapon"] = {
        ["SHAMAN"] = 16356,
    },
    ["Greater Adept's Robe"] = {
        Professions = 7643,
    },
    ["Inferno Shell"] = {
        ["Magistrate Burnside"] = 7739,
        ["Stonevault Flameweaver"] = 7739,
    },
    ["Teleport: Ironforge"] = {
        ["MAGE"] = 3562,
    },
    ["Barkskin Effect (DND)"] = {
        ["DRUID"] = 22839,
    },
    ["Summon Skeleton"] = {
        ["Skeleton"] = 8853,
        ["Dragonmaw Bonewarder"] = 8853,
        ["Skeleton"] = 20464,
        ["Lady Sylvanas Windrunner"] = 20464,
    },
    ["Weak Frostbolt"] = {
        ["Frostmane Novice"] = 6949,
    },
    ["Copper Tube"] = {
        Professions = 3924,
    },
    ["Detect Greater Invisibility"] = {
        ["WARLOCK"] = 11743,
    },
    ["Cat Form"] = {
        ["DRUID"] = 768,
        ["Lord Melenas"] = 5759,
        ["Sentinel Amarassan"] = 5759,
    },
    ["Frost Shock"] = {
        ["SHAMAN"] = 10473,
        ["Spitelash Enchantress"] = 12548,
        ["Drysnap Crawler"] = 12548,
        ["Lokholar the Ice Lord"] = 19133,
        ["Grik'nir the Cold"] = 21030,
        ["Frostwolf Shaman"] = 21401,
        ["Strashaz Sorceress"] = 15499,
    },
    ["Ice Armor"] = {
        ["MAGE"] = 10220,
    },
    ["Inferno Gloves"] = {
        Professions = 22868,
    },
    ["Gloves of Spell Mastery"] = {
        Professions = 18454,
    },
    ["Fire Shield"] = {
        Pet = 11771,
        ["Defias Renegade Mage"] = 134,
        ["Tunnel Rat Geomancer"] = 134,
        ["Dalaran Watcher"] = 134,
        ["Dalaran Mage"] = 134,
        ["Minor Manifestation of Fire"] = 134,
        ["Whitewhisker Geomancer"] = 18968,
    },
    ["Minor Magic Resistance Potion"] = {
        Professions = 3172,
    },
    ["Poisoned Harpoon"] = {
        ["Captain Greenskin"] = 5208,
    },
    ["Intimidation"] = {
        ["HUNTER"] = 19577,
        ["Crushridge Enforcer"] = 7093,
    },
    ["Arcane Explosion"] = {
        ["MAGE"] = 10202,
        ["Arcanist Doan"] = 9433,
        ["Captain Balinda Stonehearth"] = 13745,
    },
    ["Slice and Dice"] = {
        ["ROGUE"] = 6774,
    },
    ["Aquatic Form"] = {
        ["DRUID"] = 1066,
    },
    ["Embossed Leather Gloves"] = {
        Professions = 3756,
    },
    ["Green Iron Hauberk"] = {
        Professions = 3508,
    },
    ["Cindercloth Gloves"] = {
        Professions = 18412,
    },
    ["Comfortable Leather Hat"] = {
        Professions = 10490,
    },
    ["Crimson Silk Cloak"] = {
        Professions = 8789,
    },
    ["Wicked Leather Headband"] = {
        Professions = 19071,
    },
    ["Red Swashbuckler's Shirt"] = {
        Professions = 8489,
    },
    ["Poisons"] = {
        ["ROGUE"] = 2842,
    },
    ["White Wedding Dress"] = {
        Professions = 12091,
    },
    ["Fel Domination"] = {
        ["WARLOCK"] = 18708,
    },
    ["Frostbolt"] = {
        ["MAGE"] = 25304,
        ["Nether Sorceress"] = 20297,
        ["Skeletal Mage"] = 20792,
        ["Mosshide Mistweaver"] = 20792,
        ["Dalaran Wizard"] = 20792,
        ["Archmage Ataeric"] = 20792,
        ["Stormscale Sorceress"] = 20792,
        ["Kuz"] = 20792,
        ["Delmanis the Hated"] = 20792,
        ["Wrathtail Sorceress"] = 20792,
        ["Windshear Geomancer"] = 20792,
        ["Strashaz Sorceress"] = 12737,
        ["Hillsbrad Councilman"] = 20806,
        ["Sarilus Foulborne"] = 20806,
        ["Gravelsnout Surveyor"] = 20806,
        ["Saltscale Tide Lord"] = 20822,
        ["Baron Vardus"] = 20822,
        ["Boulderfist Magus"] = 20822,
        ["Drywhisker Surveyor"] = 20822,
        ["Murkgill Lord"] = 20822,
        ["Makrinni Scrabbler"] = 20822,
        ["Eliza"] = 20819,
        ["Dalaran Theurgist"] = 20819,
        ["Slitherblade Sorceress"] = 20819,
        ["Baron Aquanis"] = 15043,
        ["Lokholar the Ice Lord"] = 21369,
        ["Defias Rogue Wizard"] = 13322,
        ["Darkeye Bonecaster"] = 13322,
        ["Scarlet Neophyte"] = 13322,
        ["Dalaran Apprentice"] = 13322,
        ["Windfury Sorceress"] = 13322,
        ["Murloc Minor Tidecaller"] = 9672,
        ["Skeletal Sorcerer"] = 9672,
        ["Crushridge Mage"] = 9672,
        ["Syndicate Magus"] = 9672,
        ["Scarlet Sorcerer"] = 9672,
        ["Death's Head Adept"] = 9672,
        ["Blackfathom Tide Priestess"] = 9672,
        ["Twilight Aquamancer"] = 9672,
        ["Draconic Mageweaver"] = 9672,
    },
    ["Rupture"] = {
        ["ROGUE"] = 11275,
    },
    ["Domination"] = {
        ["Grand Crusader Dathrohan"] = 17405,
    },
    ["Conflagrate"] = {
        ["WARLOCK"] = 18932,
    },
    ["Counterattack"] = {
        ["HUNTER"] = 20910,
    },
    ["Flash Freeze"] = {
        ["Freezing Ghoul"] = 16803,
    },
    ["White Linen Shirt"] = {
        Professions = 2393,
    },
    ["Ink Spray"] = {
        ["Marsh Inkspewer"] = 9612,
    },
    ["Barbaric Iron Boots"] = {
        Professions = 9818,
    },
    ["Enchant Shield - Lesser Protection"] = {
        Professions = 13464,
    },
    ["Blood Leech"] = {
        ["Moonrage Glutton"] = 6958,
        ["Bloodfeather Harpy"] = 6958,
        ["Hezrul Bloodmark"] = 6958,
    },
    ["Portal: Darnassus"] = {
        ["MAGE"] = 11419,
    },
    ["Rocket, BLUE"] = {
        ["Pat's Firework Guy - BLUE"] = 26344,
    },
    ["Savannah Cubs"] = {
        ["Savannah Cub"] = 8210,
        ["Dishu"] = 8210,
    },
    ["Blessing of Might"] = {
        ["PALADIN"] = 25291,
    },
    ["Enchant Shield - Frost Resistance"] = {
        Professions = 13933,
    },
    ["Brown Linen Shirt"] = {
        Professions = 3915,
    },
    ["Curse of Stalvan"] = {
        ["Stalvan Mistmantle"] = 3105,
        ["Forlorn Spirit"] = 3105,
    },
    ["Pearl-clasped Cloak"] = {
        Professions = 6521,
    },
    ["Siphon Life"] = {
        ["WARLOCK"] = 18881,
    },
    ["Enchant Bracer - Greater Strength"] = {
        Professions = 13939,
    },
    ["Deadly Poison IV"] = {
        ["ROGUE"] = 11358,
    },
    ["Tainted Mind"] = {
        ["Cursed Mage"] = 16567,
    },
    ["Elemental Sharpening Stone"] = {
        Professions = 22757,
    },
    ["Enchant Bracer - Greater Intellect"] = {
        Professions = 20008,
    },
    ["Touch of Ravenclaw"] = {
        ["Hand of Ravenclaw"] = 3263,
    },
    ["Summon Boar Spirit"] = {
        ["Boar Spirit"] = 8286,
        ["Aggem Thorncurse"] = 8286,
    },
    ["Summon Treant Ally"] = {
        ["Treant Ally"] = 7993,
        ["Son of Cenarius"] = 7993,
    },
    ["Enchant Bracer - Minor Strength"] = {
        Professions = 7782,
    },
    ["Red Mageweave Gloves"] = {
        Professions = 12066,
    },
    ["Stoneform"] = {
        Racial = 20594,
        ["Dark Iron Insurgent"] = 7020,
        ["Captain Ironhill"] = 7020,
    },
    ["Tough Scorpid Gloves"] = {
        Professions = 10542,
    },
    ["Create Lunar Festival Invitation"] = {
        ["Lunar Festival Harbinger"] = 26375,
    },
    ["Blood Funnel"] = {
        ["Hakkari Blood Priest"] = 24617,
    },
    ["Infected Bite"] = {
        ["Strashaz Hydra"] = 16128,
        ["Black Widow Hatchling"] = 7367,
    },
    ["Seal of Command"] = {
        ["PALADIN"] = 20920,
    },
    ["Tough Scorpid Shoulders"] = {
        Professions = 10564,
    },
    ["Demon Skin"] = {
        ["WARLOCK"] = 696,
        ["Frostmane Shadowcaster"] = 20798,
        ["Gazz'uz"] = 20798,
    },
    ["Green Dragonscale Gauntlets"] = {
        Professions = 24655,
    },
    ["Premeditation"] = {
        ["ROGUE"] = 14183,
    },
    ["Thick Murloc Armor"] = {
        Professions = 6704,
    },
    ["Enchant Boots - Spirit"] = {
        Professions = 20024,
    },
    ["Bronze Framework"] = {
        Professions = 3953,
    },
    ["Green Iron Helm"] = {
        Professions = 3502,
    },
    ["Flame Buffet"] = {
        ["Burning Blade Seer"] = 9658,
    },
    ["Enchant 2H Weapon - Greater Impact"] = {
        Professions = 13937,
    },
    ["Parachute Cloak"] = {
        Professions = 12616,
    },
    ["Runecloth Robe"] = {
        Professions = 18406,
    },
    ["Enchant Weapon - Striking"] = {
        Professions = 13693,
    },
    ["Frost Resistance Totem"] = {
        ["SHAMAN"] = 10479,
    },
    ["Small Seaforium Charge"] = {
        Professions = 3933,
    },
    ["Red Whelp Gloves"] = {
        Professions = 9072,
    },
    ["Echoing Roar"] = {
        ["Gordunni Warlord"] = 10967,
        ["Field Marshal Afrasiabi"] = 10967,
    },
    ["Abolish Poison"] = {
        ["DRUID"] = 2893,
    },
    ["Tranquility"] = {
        ["DRUID"] = 9863,
    },
    ["Deadly Poison II"] = {
        ["ROGUE"] = 2837,
    },
    ["Create Firestone (Major)"] = {
        ["WARLOCK"] = 17953,
    },
    ["Enchant Boots - Minor Speed"] = {
        Professions = 13890,
    },
    ["Rebirth"] = {
        ["DRUID"] = 20748,
    },
    ["Greater Mana Potion"] = {
        Professions = 11448,
    },
    ["Lavender Mageweave Shirt"] = {
        Professions = 12075,
    },
    ["Handstitched Leather Boots"] = {
        Professions = 2149,
    },
    ["Earthen Guards Destroyed"] = {
        ["Earthen Guardian"] = 10666,
        ["Earthen Hallshaper"] = 10666,
        ["Earthen Custodian"] = 10666,
        ["Vault Warder"] = 10666,
    },
    ["Summon Dreadsteed"] = {
        ["WARLOCK"] = 23161,
    },
    ["Crimson Silk Vest"] = {
        Professions = 8791,
    },
    ["Large Red Rocket Cluster"] = {
        Professions = 26428,
    },
    ["Dominate Mind"] = {
        ["Singer"] = 14515,
        ["Death Speaker Jargba"] = 14515,
        ["Hederine Initiate"] = 15859,
        ["Strashaz Siren"] = 7645,
        ["Twilight Shadowmage"] = 7645,
    },
    ["Fear Ward"] = {
        ["PRIEST"] = 6346,
    },
    ["Corruption of the Earth"] = {
        ["Emeriss"] = 24910,
    },
    ["Orange Mageweave Shirt"] = {
        Professions = 12061,
    },
    ["Small Blue Rocket"] = {
        Professions = 26416,
    },
    ["Rough Bronze Leggings"] = {
        Professions = 2668,
    },
    ["Enchant Bracer - Deflection"] = {
        Professions = 13931,
    },
    ["Sludge"] = {
        ["Sludge"] = 3514,
    },
    ["Volatile Infection"] = {
        ["Balgaras the Foul"] = 3586,
        ["Old Murk-Eye"] = 3584,
    },
    ["Summon Fire Elemental"] = {
        ["Fire Elemental"] = 8985,
        ["Scarlet Conjuror"] = 8985,
    },
    ["Reconstruct"] = {
        ["Earthen Hallshaper"] = 10260,
    },
    ["Cursed Blood"] = {
        ["Rotting Agam'ar"] = 8268,
        ["Rotting Agam'ar"] = 8267,
    },
    ["Dark Iron Sunderer"] = {
        Professions = 15294,
    },
    ["Thorium Rifle"] = {
        Professions = 19792,
    },
    ["Summon Zulian Guardians"] = {
        ["Zulian Guardian"] = 24183,
    },
    ["Elixir of Shadow Power"] = {
        Professions = 11476,
    },
    ["Ghostly Strike"] = {
        ["ROGUE"] = 14278,
    },
    ["Gauntlets of the Sea"] = {
        Professions = 10630,
    },
    ["Elixir of Greater Firepower"] = {
        Professions = 26277,
    },
    ["Firebolt"] = {
        Pet = 11763,
    },
    ["Shadowform"] = {
        ["PRIEST"] = 15473,
    },
    ["Wavering Will"] = {
        ["Bleak Worg"] = 7127,
    },
    ["Frost Shot"] = {
        ["Spitelash Siren"] = 12551,
        ["Razorfen Beast Trainer"] = 6984,
    },
    ["Shadow Resistance Aura"] = {
        ["PALADIN"] = 19896,
    },
    ["Enchant Cloak - Minor Protection"] = {
        Professions = 7771,
    },
    ["Moonsight Rifle"] = {
        Professions = 3954,
    },
    ["Sanctity Aura"] = {
        ["PALADIN"] = 20218,
    },
    ["Call of the Grave"] = {
        ["Undead Excavator"] = 5137,
        ["Azshir the Sleepless"] = 5137,
    },
    ["Holy Protection Potion"] = {
        Professions = 7255,
    },
    ["Sense Demons"] = {
        ["WARLOCK"] = 5500,
    },
    ["Create Aqual Quintessence"] = {
        ["Duke Hydraxis"] = 21357,
    },
    ["Ghoul Plague"] = {
        ["Plague Ghoul"] = 16458,
    },
    ["Heavy Mithril Boots"] = {
        Professions = 9968,
    },
    ["Black Rot"] = {
        ["Plagued Rat"] = 16448,
    },
    ["Summon Shadowcaster"] = {
        ["Skeletal Shadowcaster"] = 12258,
        ["Skeletal Summoner"] = 12258,
    },
    ["Stoneclaw Totem"] = {
        ["SHAMAN"] = 10428,
    },
    ["EZ-Thro Dynamite"] = {
        Professions = 8339,
    },
}
