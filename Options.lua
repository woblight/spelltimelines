local addonName, addonTable = ...

setmetatable(addonTable, {__index = getfenv() })
setfenv(1, addonTable)

local options
local of = CreateFrame("Frame")

local function deepClone(t)
    if type(t) ~= "table" then
        return t
    end
    local nt = {}
    for k, v in pairs(t) do
        nt[k] = deepClone(v)
    end
    return nt
end

function showOptions()
    if not options then
        options = EFrame.Item(nil, of, false)
        options.style = EFrame.Blizzlike
        options:attach("line")
        options.toolBar = EFrame.RowLayout(options)
        options.toolBar.marginTop = 10
        options.toolBar.marginLeft = 10
        options.toolBar.marginRight = 10
        options.toolBar.anchorTopLeft = options.topLeft
        options.toolBar.anchorRight = options.right
        options.toolBar.spacing = 2
        options.toolRow = EFrame.Rectangle(options)
        options.toolRow.color = {0.4,0.4,0.4,0.6}
        options.toolRow.height = 1
        options.toolRow.marginTop = 2
        options.toolRow.anchorTopLeft = options.toolBar.bottomLeft
        options.toolRow.anchorTopRight = options.toolBar.bottomRight
        options.unlockButton = options.style.Button(options.toolBar)
        options.unlockButton.text = EFrame.bind(function() return SpellTimelinesHandler.unlocked and "Lock Frames" or "Unlock Frames" end)
        options.unlockButton:connect("clicked", function() SpellTimelinesHandler.unlocked = not SpellTimelinesHandler.unlocked end)
        
        EFrame.Item(options.toolBar).Layout.fillWidth = true
        
        options.profileSelector = options.style.ComboBox(options.toolBar)
        local profiles = EFrame.ListModel()
        for _, p in pairs(SpellTimelinesSettings.profiles) do
            profiles:add({name = p.name, profile = p, del = false})
        end
        options.profileSelector.textRole = "name"
        options.profileSelector.model = profiles
        options.profileSelector.currentIndex = EFrame.bind(function ()
            local count = 1
            local current
            for _, p in ipairs(profiles) do
                if SpellTimelinesHandler.currentProfile == p.profile.name then
                    return count
                end
                count = count + 1
            end
        end)
        options.profileSelector:connect("activated", function(i) SpellTimelinesHandler.currentProfile = profiles[i].profile.name end)
        
--         options.profileButtonRename = options.style.Button(options.toolBar)
--         options.profileButtonRename.text = "Rename"
--         
        options.profileButtonDelete = options.style.Button(options.toolBar)
        options.profileButtonDelete.text = EFrame.bind(function() return profiles[options.profileSelector.currentIndex].del and "Restore" or "Delete" end)
        options.profileButtonDelete:connect("clicked", function ()
            local pp = profiles[options.profileSelector.currentIndex]
            local pname = pp.name
            if profiles[options.profileSelector.currentIndex].del then
                SpellTimelinesSettings.profiles[pname] = pp.profile
                pp.name = pp.profile.name
                pp.del = false
            else
                SpellTimelinesSettings.profiles[pname] = nil
                pp.name = "|c00ff0000" .. pname .. "|r"
                pp.del = true
            end
        end)
        
        options.profileButtonNew = options.style.Button(options.toolBar)
        options.profileButtonNew.text = "New"
        options.profileButtonNew:connect("clicked", function ()
            local n = 1
            while SpellTimelinesSettings.profiles["Profile "..n] do
                n = n + 1
            end
            SpellTimelinesHandler.currentProfile = "Profile "..n
            profiles:add({name = "Profile "..n, profile = SpellTimelinesSettings.profiles["Profile "..n], del = false})
            options.profileSelector.currentIndex = #profiles
        end)
        
        options.profileButtonCopy = options.style.Button(options.toolBar)
        options.profileButtonCopy.text = "New from current"
        options.profileButtonCopy:connect("clicked", function ()
            local n = 1
            local p = SpellTimelinesHandler.currentProfile .. " Copy " ..n
            while SpellTimelinesSettings.profiles[p] do
                n = n + 1
                p = SpellTimelinesProfile.currentProfile .. " Copy " ..n
            end
            SpellTimelinesSettings.profiles[p] = deepClone(SpellTimelinesProfile)
            SpellTimelinesSettings.profiles[p].name = p
            profiles:add({name = p, profile = SpellTimelinesSettings.profiles[p], del = false})
            SpellTimelinesHandler.currentProfile = p
            options.profileSelector.currentIndex = #profiles
        end)
        
        options.version = EFrame.RowLayout(options)
        options.version.label = EFrame.Label(options.version)
        options.version.label.text = "Version:"
        options.version.edit = EFrame.TextEdit(options.version)
        options.version.edit.implicitWidth = 75
        options.version.edit.readOnly = true
        options.version.edit.text = GetAddOnMetadata(addonName, "version")
        options.version.marginBottom = 10
        options.version.marginRight = 10
        options.version.anchorBottomRight = options.bottomRight
        
        options.main = EFrame.RowLayout(options)
        options.main.anchorLeft = options.left
        options.main.anchorRight = options.right
        options.main.anchorBottom = options.bottom
        options.main.anchorTop = options.toolBar.bottom
        options.main.marginLeft = 10
        options.main.marginRight = 10
        options.main.marginTop = 5
        options.main.marginBottom = 10
        options.main.spacing = 4
        
        
        options.menuColumn = EFrame.ColumnLayout(options.main)
        options.menuColumn.Layout.fillHeight = true
        options.menuColumn.Layout.preferredWidth = EFrame.bind(function() return math.min(options.menuColumn.implicitWidth, options.main.width/4) end)
        options.menuColumn.spacing = 2
        
        options.addTimelineButton = options.style.Button(options.menuColumn)
        options.addTimelineButton.text = "Add timeline"
        options.addTimelineButton.Layout.fillWidth = true
        options.addTimelineButton:connect("clicked", SpellTimelinesHandler,"addTimeline")
        options.menuBackground = EFrame.Rectangle(options.menuColumn)
        options.menuBackground.Layout.fillWidth = true
        options.menuBackground.Layout.fillHeight = true
        options.menuBackground.borderWidth = 1
        options.menuBackground.borderColor = {0.4,0.4,0.4,0.6}
        options.menuBackground.color = {0.1,0.1,0.1,0.1}
        options.menu = EFrame.ListView(options.menuBackground)
        options.menu.anchorFill = options.menuBackground
        options.menu.margins = 4
        options.menuBackground.implicitWidth = EFrame.bind(function() return options.menu.implicitWidth + 8 end)
        
        local list = EFrame.ListModel()
        list:add({id = 0, line = SpellTimelinesHandler})
        list:add({id = 0.1, line = SpellTimelinesHandler.nameplates})
        for k,v in pairs(timelines) do
            if not v.plate then
                list:add({id = k, line = v})
            end
        end
        options.menu.delegate = function (parent, index)
            local btn = options.style.Button(parent)
            btn.text = EFrame.bind(function() local idx = index return format("%d. %s%s", list[idx].id, list[idx].line.disabled and "(Off) " or "", idx == 1 and "Globals" or list[idx].line.target or "<no target>") end)
            btn.flat = true
            btn.hAlignment = "LEFT"
            btn:connect("clicked", function() options.menu.currentIndex = index end)
            return btn
        end
        options.menu.highlightDelegate = function (parent)
            local d = EFrame.Image(parent)
            d.layer = "OVERLAY"
            d.source = "Interface\\QuestFrame\\UI-QuestLogTitleHighlight"
            d.blendMode = "ADD"
            d.color = {1,1,0,.5}
            return d
        end
        options.menu.model = list
        options.menu.currentIndex = 1
        
        options.line = EFrame.bind(function() return list[options.menu.currentIndex].line end)
        options.contents = EFrame.ColumnLayout(options.main)
        options.contents.spacing = 4
        options.contents.Layout.fillWidth = true
        options.contents.Layout.alignment = EFrame.Layout.AlignTop
        
        options.enabledCheck = options.style.CheckButton(options.contents)
        options.enabledCheck.text = "Enabled"
        options.enabledCheck.visible = EFrame.bind(function() return options.line ~= SpellTimelinesHandler end)
        options.enabledCheck.checked = EFrame.bind(function() return not options.line.disabled end)
        options.enabledCheck:connect("toggled", function() options.line.disabled = not options.enabledCheck.checked end)
        
        options.targetRow = options.style.RowLayout(options.contents)
        options.targetRow.visible = EFrame.bind(function() return options.line.id and options.line.id >= 1 end)
        
        options.targetLabel = options.style.Label(options.targetRow)
        options.targetLabel.text = "Target:"
        
        options.targetEdit = options.style.TextEdit(options.targetRow)
        options.targetEdit.implicitWidth = 150
        options.targetEdit.text = EFrame.bind(function() return options.line.target or "<no target>" end)
        options.targetEdit:connect("enterPressed", function() options.line.target = options.targetEdit.text ~= "" and options.targetEdit.text or nil end)
        options.targetEdit.enabled = EFrame.bind(function() return options.line.id and options.line.id > 2 end)
        
        options.targetFocus = options.style.Button(options.targetRow)
        options.targetFocus.visible = EFrame.bind(function() return options.line.id and options.line.id > 2 end)
        options.targetFocus.text = "Target unit's GUID"
        options.targetFocus:connect("clicked", function() options.line.target = UnitGUID("target") end)
        
        options.periodRow = EFrame.RowLayout(options.contents)
        options.periodRow.Layout.fillWidth = true
        options.periodRow.spacing = 2
        options.periodLabel = EFrame.Label(options.periodRow)
        options.periodLabel.text = "Timeline period:"
        options.periodSpin = EFrame.SpinBox(options.periodRow)
        options.periodSpin.from = 1
        options.periodSpin.step = 0.5
        options.periodSpin.value = EFrame.bind(function() return options.line == SpellTimelinesHandler and options.line.period or options.line.effectivePeriod end)
        function options.periodSpin:onValueModified(v)
            options.line.period = v
        end
        options.periodLabel2 = EFrame.Label(options.periodRow)
        options.periodLabel2.text = "seconds"
        options.periodIndicator = EFrame.Label(options.periodRow)
        options.periodIndicator.Layout.fillWidth = true
        options.periodIndicator.text  = EFrame.bind(function ()
            if options.line == SpellTimelinesHandler then
                return SpellTimelinesHandler.period == defaultProfile.period and "" or format("(Default: %d)", defaultProfile.period)
            else
                return options.line.period and "" or "(From Globals)"
            end
        end)
        options.periodIndicator.hAlignment = "LEFT"
        options.periodReset = options.style.Button(options.periodRow)
        options.periodReset.text = "Reset"
        options.periodReset.visible = EFrame.bind(function ()
            if options.line == SpellTimelinesHandler then
                return options.line.period ~= defaultProfile.period
            else
                return options.line.period ~= nil
            end
        end)
        options.periodReset:connect("clicked", function ()
            if options.line == SpellTimelinesHandler then
                options.line.period = defaultProfile.period
            else
                options.line.period = nil
            end
        end)
        
        options.timeDirectionRow = EFrame.RowLayout(options.contents)
        options.timeDirectionRow.spacing = 2
        options.timeDirectionRow.contentItem = options.timeDirectionRow
        options.timeDirectionRow.Layout.fillWidth = true
        options.timeDirectionLabel = EFrame.Label(options.timeDirectionRow)
        options.timeDirectionLabel.text = "Time direction:"
        options.timeDirectionRadio1 = EFrame.RadioButton(options.timeDirectionRow)
        options.timeDirectionRadio1.text = "Right to left"
        options.timeDirectionRadio1.value = true
        options.timeDirectionRadio2 = EFrame.RadioButton(options.timeDirectionRow)
        options.timeDirectionRadio2.text = "Left to right"
        options.timeDirectionRadio2.value = false
        options.timeDirectionRow.autoExclusiveGroup.current = EFrame.bind(function ()
            if options.line == SpellTimelinesHandler then
                return options.line.timeDirection and options.timeDirectionRadio1 or options.timeDirectionRadio2
            else
                return options.line.effectiveTimeDirection and options.timeDirectionRadio1 or options.timeDirectionRadio2
            end
        end)
        function  options.timeDirectionRow.autoExclusiveGroup.onClicked(_, current)
            options.line.timeDirection = current.value
        end
        options.timeDirectionIndicator = EFrame.Label(options.timeDirectionRow)
        options.timeDirectionIndicator.Layout.fillWidth = true
        options.timeDirectionIndicator.text  = EFrame.bind(function ()
            if options.line == SpellTimelinesHandler then
                return SpellTimelinesHandler.timeDirection == defaultProfile.timeDirection and "" or format("(Default: %s)", defaultProfile.timeDirection and "right to left" or "left to right")
            else
                return options.line.timeDirection ~= nil and "" or "(From Globals)"
            end
        end)
        options.timeDirectionIndicator.hAlignment = "LEFT"
        options.timeDirectionReset = options.style.Button(options.timeDirectionRow)
        options.timeDirectionReset.text = "Reset"
        options.timeDirectionReset.visible = EFrame.bind(function ()
            if options.line == SpellTimelinesHandler then
                return options.line.timeDirection ~= defaultProfile.timeDirection
            else
                return options.line.timeDirection ~= nil
            end
        end)
        options.timeDirectionReset:connect("clicked", function ()
            if options.line == SpellTimelinesHandler then
                options.line.timeDirection = defaultProfile.timeDirection
            else
                options.line.timeDirection = nil
            end
        end)
        
        options.noBordersRow = EFrame.RowLayout(options.contents)
        options.noBordersRow.visible = EFrame.bind(function() return not options.line.id or options.line.id == 0 or options.line.id >= 1 end)
        options.noBordersRow.Layout.fillWidth = true
        options.noBordersCheck = options.style.CheckButton(options.noBordersRow)
        options.noBordersCheck.text = "Hide timeline borders"
        options.noBordersCheck.checked = EFrame.bind(function() if options.line == SpellTimelinesHandler then return options.line.noBorders else return options.line.effectiveNoBorders end end)
        options.noBordersCheck:connect("toggled", function() options.line.noBorders = options.noBordersCheck.checked end)
        options.noBordersIndicator = EFrame.Label(options.noBordersRow)
        options.noBordersIndicator.Layout.fillWidth = true
        options.noBordersIndicator.text  = EFrame.bind(function ()
            if options.line == SpellTimelinesHandler then
                return SpellTimelinesHandler.noBorders == defaultProfile.noBorders and "" or format("(Default: %s)", defaultProfile.noBorders and "yes" or "no")
            else
                return options.line.noBorders ~= nil and "" or "(From Globals)"
            end
        end)
        options.noBordersIndicator.hAlignment = "LEFT"
        options.noBordersReset = options.style.Button(options.noBordersRow)
        options.noBordersReset.text = "Reset"
        options.noBordersReset.visible = EFrame.bind(function ()
            if options.line == SpellTimelinesHandler then
                return options.line.noBorders ~= defaultProfile.noBorders
            else
                return options.line.noBorders ~= nil
            end
        end)
        options.noBordersReset:connect("clicked", function ()
            if options.line == SpellTimelinesHandler then
                options.line.noBorders = defaultProfile.noBorders
            else
                options.line.noBorders = nil
            end
        end)
        
        options.deleteButton = options.style.Button(options.contents)
        options.deleteButton.text = "DELETE"
        options.deleteButton:connect("clicked", function() SpellTimelinesHandler:removeTimeline(list[options.menu.currentIndex].id) end)
        options.deleteButton.visible = EFrame.bind(function() return options.menu.currentIndex > 3 end)
        
        options.precacheCheck = options.style.CheckButton(options.contents)
        options.precacheCheck.text = "Precache spell frames"
        options.precacheCheck.checked = SpellTimelinesSettings.precache
        options.precacheCheck:connect("checkedChanged", function(v) SpellTimelinesSettings.precache = v end)
        options.precacheCheck.visible = EFrame.bind(function() return options.menu.currentIndex == 1 end)
        
        
        options.showRanksRow = EFrame.RowLayout(options.contents)
        options.showRanksRow.visible = EFrame.bind(function() return not options.line.id or options.line.id == 0 or options.line.id >= 1 end)
        options.showRanksRow.Layout.fillWidth = true
        options.showRanksCheck = options.style.CheckButton(options.showRanksRow)
        options.showRanksCheck.text = "Show spell ranks"
        options.showRanksCheck.checked = EFrame.bind(function() if options.line == SpellTimelinesHandler then return options.line.showRanks else return options.line.effectiveShowRanks end end)
        options.showRanksCheck:connect("toggled", function() options.line.showRanks = options.showRanksCheck.checked end)
        options.showRanksIndicator = EFrame.Label(options.showRanksRow)
        options.showRanksIndicator.Layout.fillWidth = true
        options.showRanksIndicator.text  = EFrame.bind(function ()
            if options.line == SpellTimelinesHandler then
                return SpellTimelinesHandler.showRanks == defaultProfile.showRanks and "" or format("(Default: %s)", defaultProfile.showRanks and "yes" or "no")
            else
                return options.line.showRanks ~= nil and "" or "(From Globals)"
            end
        end)
        options.showRanksIndicator.hAlignment = "LEFT"
        options.showRanksReset = options.style.Button(options.showRanksRow)
        options.showRanksReset.text = "Reset"
        options.showRanksReset.visible = EFrame.bind(function ()
            if options.line == SpellTimelinesHandler then
                return options.line.showRanks ~= defaultProfile.showRanks
            else
                return options.line.showRanks ~= nil
            end
        end)
        options.showRanksReset:connect("clicked", function ()
            if options.line == SpellTimelinesHandler then
                options.line.showRanks = defaultProfile.showRanks
            else
                options.line.showRanks = nil
            end
        end)
        
        function options:addTimelineRow(k)
            local t = timelines[k]
            list:add({id = k, line = t})
        end
        
        function options:removeTimelineRow(n)
            for k, v in ipairs(list) do
                if v.id == n then
                    return list:remove(k)
                end
            end
        end
        
        SpellTimelinesHandler:connect("timelineAdded", options, "addTimelineRow")
        SpellTimelinesHandler:connect("timelineRemoved", options, "removeTimelineRow")
        
        local function ShowColorPicker(r, g, b, a, changedCallback)
            local open = OpenColorPicker or ColorPickerFrame.SetupColorPickerAndShow and function(...) ColorPickerFrame:SetupColorPickerAndShow(...) end
            if open then
                local alpha = a or 1
                if WOW_PROJECT_ID ~= WOW_PROJECT_MAINLINE then
                    alpha = 1 - alpha
                end
                open({
                    hasOpacity = a ~= nil,
                    opacityFunc = changedCallback,
                    opacity = alpha,
                    cancelFunc = changedCallback,
                    swatchFunc = changedCallback,
                    r = r,
                    g = g,
                    b = b,
                })
            else
                ColorPickerFrame:Hide()
                ColorPickerFrame.func, ColorPickerFrame.opacityFunc, ColorPickerFrame.cancelFunc = nil, nil, nil
                ColorPickerFrame:SetColorRGB(r,g,b)
                ColorPickerFrame.hasOpacity, ColorPickerFrame.opacity = (a ~= nil), 1 - a or 0
                ColorPickerFrame.previousValues = {r,g,b,a}
                ColorPickerFrame.func, ColorPickerFrame.opacityFunc, ColorPickerFrame.cancelFunc = changedCallback, changedCallback, changedCallback
                ColorPickerFrame:Show()
            end
        end
        function colorPick(p, text, colors)
            local row = EFrame.RowLayout(p)
            row.Layout.fillWidth = true
            row.spacing = 2
            row.square = EFrame.Rectangle(row)
            row.square.Layout.fillHeight = true
            row.square.Layout.preferredWidth = EFrame.bind(row.square, "height")
            row.square.color = borderColors[colors[1]]
            row.label = EFrame.Label(row)
            row.label.text = text
            row.mouse = EFrame.MouseArea(row.square)
            row.mouse.anchorFill = row.square
            row.mouse:connect("clicked", function ()
                local color = borderColors[colors[1]]
                ShowColorPicker(color[1], color[2], color[3], color[4], function (restore)
                    if restore then
                        for _, v in ipairs(colors) do
                            borderColors[v] = restore.r and {restore.r, restore.g, restore.b, WOW_PROJECT_ID == WOW_PROJECT_MAINLINE and restore.a or 1 - restore.a} or restore
                        end
                    else
                        local r, g, b = ColorPickerFrame:GetColorRGB()
                        for _, v in ipairs(colors) do
                            local alpha = ColorPickerFrame.GetColorAlpha and ColorPickerFrame:GetColorAlpha() or OpacitySliderFrame:GetValue()
                            if WOW_PROJECT_ID ~= WOW_PROJECT_MAINLINE then
                                alpha = 1 - alpha
                            end
                            borderColors[v] = {r, g, b, alpha}
                        end
                    end
                    row.square.color = borderColors[colors[1]]
                end)
            end)
            row.reset = options.style.Button(row)
            row.reset.text = "X"
            row.reset:connect("clicked", function ()
                local color = defaultColors[colors[1]]
                for _, v in ipairs(colors) do
                    borderColors[v] = color
                end
                row.square.color = color
            end)
            row.reset.Layout.alignment = EFrame.Layout.AlignRight
            return row
        end
        
        options.colors = EFrame.RowLayout(options.contents)
        options.colors.visible = EFrame.bind(function() return options.line == SpellTimelinesHandler end)
        options.colors.spacing = 2
        
        options.lineColors = EFrame.ColumnLayout(options.colors)
        options.lineColors.Layout.alignment = EFrame.Layout.AlignTop
        options.lineColors.spacing = 2
        options.lineColors.caption = EFrame.Label(options.lineColors)
        options.lineColors.caption.text = "Lines"
        for _,v in ipairs(
            {{"Success", {"SUCCESS", "CURRENT"}},
            {"Stopped", {"FAILED"}},
            {"Interrupted", {"INTERRUPTED"}},
            {"Lost", {"LOST"}}}) do
            colorPick(options.lineColors, v[1], v[2])
        end
        options.iconColors = EFrame.ColumnLayout(options.colors)
        options.iconColors.Layout.alignment = EFrame.Layout.AlignTop
        options.iconColors.spacing = 2
        options.iconColors.caption = EFrame.Label(options.iconColors)
        options.iconColors.caption.text = "Icon borders"
        for _,v in ipairs(
            {{"Success", {"SUCCESS2"}},
            {"Failed", {"FAILED2"}}}) do
            colorPick(options.iconColors, v[1], v[2]).visible = EFrame.bind(function() return options.line == SpellTimelinesHandler end)
        end
    end
end

of.name = "SpellTimelines"
of.refresh = EFrame:makeAtomic(showOptions)
of.OnRefresh = EFrame:makeAtomic(showOptions)
if InterfaceOptions_AddCategory then
  InterfaceOptions_AddCategory(of)
elseif Settings and Settings.RegisterCanvasLayoutCategory then
  of.category = Settings.RegisterCanvasLayoutCategory(of,of.name)
  Settings.RegisterAddOnCategory(of.category)
end

local function chatcmd(msg)
    if not msg or msg == "" then
        if Settings and Settings.RegisterAddOnCategory then
            Settings.OpenToCategory(of.category:GetID())
        elseif not InterfaceOptionsFrame_Show then
            Settings.OpenToCategory('SpellTimelines')
        else
            if not options then
                InterfaceOptionsFrame_Show()
            end
            InterfaceOptionsFrame_OpenToCategory("SpellTimelines")
        end
        return
    elseif msg == "unlock" then
        SpellTimelinesHandler.unlocked = true
    elseif msg == "lock" then
        SpellTimelinesHandler.unlocked = false
    elseif strmatch(msg, "^line%d+%s.+") then
        local l, r = strmatch(msg, "^line(%d+)%s(.*)$")
        l = tonumber(l)
        if r == "focus" then
            if l < 2 or not timelines[l] then
                return print("SpellTimelines: invalid line id.")
            end
            timelines[l].target = UnitGUID("target")
        end
    elseif msg == "version" then
        print(format("SpellTimelines: %s (EmeraldFramework: %s)", GetAddOnMetadata(addonName, "version"), EFrame.version or ""))
    else
        print(format("SpellTimelines %s slash commands (/stl):", GetAddOnMetadata(addonName, "version")))
        print("    unlock - allows to move and resize the timelines")
        print("    lock - locks the timelines in place")
        print("    line# focus - set current target as timeline's focus")
    end
end

_G.SLASH_SPELLTIMELINES1 = "/spelltimelines"
_G.SLASH_SPELLTIMELINES2 = "/stl"
_G.SlashCmdList["SPELLTIMELINES"] = chatcmd 
