## Interface: 20501
## Title: SpellTimelines
## Notes: Shows casts performed by player and other targets in a timeline fashion
## Version: 0.1
## Author: WobLight
## SavedVariables: SpellTimelinesSettings
## SavedVariablesPerCharacter: SpellTimelinesProfile
## Dependencies: EmeraldFramework
SpellTimelines.lua
Timeline.lua
Options.lua
