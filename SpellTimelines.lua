local addonName, addonTable = ...

_G[addonName] = addonTable

setmetatable(addonTable, {__index = getfenv() })
setfenv(1, addonTable)

defaultProfile = {
    period = 10,
    noBorders = false,
    timeDirection = true,
    showRanks = true,
}

defaultSettings = {
    precache = true,
}

local useNames = WOW_PROJECT_ID == WOW_PROJECT_CLASSIC

recycledFrames = {}
local eventHandler = CreateFrame("frame")
eventHandler:RegisterEvent("ADDON_LOADED")
eventHandler:RegisterEvent("UNIT_SPELLCAST_SUCCEEDED")
eventHandler:RegisterEvent("UNIT_SPELLCAST_STOP")
eventHandler:RegisterEvent("UNIT_SPELLCAST_FAILED")
eventHandler:RegisterEvent("UNIT_SPELLCAST_FAILED_QUIET")
eventHandler:RegisterEvent("UNIT_SPELLCAST_INTERRUPTED")
eventHandler:RegisterEvent("UNIT_SPELLCAST_START")
eventHandler:RegisterEvent("NAME_PLATE_UNIT_ADDED")
eventHandler:RegisterEvent("NAME_PLATE_UNIT_REMOVED")
eventHandler:RegisterEvent("UNIT_SPELLCAST_CHANNEL_START")
eventHandler:RegisterEvent("UNIT_SPELLCAST_CHANNEL_STOP")
-- eventHandler:RegisterEvent("UNIT_SPELLCAST_CHANNEL_UPDATE")
-- eventHandler:RegisterEvent("UNIT_SPELLCAST_SENT")
eventHandler:RegisterEvent("UNIT_NAME_UPDATE")
eventHandler:RegisterEvent("UNIT_TARGET")
eventHandler:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")

function loadTimelineSettings(tt, t, readonly)
    tt.disabled = t.disabled
    if tt.plate then
        if not readonly then
            tt.marginBottom = t.offset or 0
            tt:connect("marginBottomChanged", function(y) t.offset = EFrame.normalized(y) end)
        else
            tt.marginBottom = EFrame.bind(t, "marginBottom")
        end
    else
        tt.target = t.target
        tt.width = EFrame.normalizeBind(t.width or EFrame.normalized(EFrame.root.width * 0.2))
        tt.marginLeft = EFrame.normalizeBind(t.x  or EFrame.normalized(EFrame.root.width * 0.85 - tt.width))
        tt.marginTop = EFrame.normalizeBind(t.y or EFrame.normalized(EFrame.root.height * 0.85))
        tt.noBorders = t.noBorders
        if not readonly then
            tt:connect("noBordersChanged", function(n) t.noBorders = n end)
            tt:connect("marginLeftChanged", function(x) t.x = EFrame.normalized(x) end)
            tt:connect("marginTopChanged", function(y) t.y = EFrame.normalized(y) end)
            tt:connect("widthChanged", function(width) t.width = EFrame.normalized(width) end)
            tt:connect("targetChanged", function(x) t.target = x end)
        end
        tt.anchorTopLeft = EFrame.root.topLeft
        tt.effectiveNoBorders = EFrame.bind(function() local td = tt.noBorders return td == nil and SpellTimelinesHandler.noBorders or td end)
    end
    if not readonly then
        tt.height = EFrame.normalizeBind(t.height or 20)
        tt.period = t.period
        tt.effectivePeriod = EFrame.bind(function() return tt.period or SpellTimelinesHandler.period end)
        tt.iconScale = t.iconScale or 1
        tt.timeDirection = t.timeDirection
        tt.showRanks = t.showRanks
        tt.effectiveTimeDirection = EFrame.bind(function() local td = tt.timeDirection return td == nil and SpellTimelinesHandler.timeDirection or td end)
        tt.effectiveShowRanks = EFrame.bind(function() local td = tt.showRanks return td == nil and SpellTimelinesHandler.showRanks or td end)
        tt:connect("periodChanged", function(n) t.period = n end)
        tt:connect("timeDirectionChanged", function(n) t.timeDirection = n end)
        tt:connect("iconScaleChanged", function(x) t.iconScale = x end)
        
        tt:connect("disabledChanged", function(x) t.disabled = x end)
        
        tt:connect("heightChanged", function(height) t.height = EFrame.normalized(height) end)
        tt:connect("showRanksChanged", function(n) t.showRanks = n end)
    else
        tt.height = EFrame.bind(t, "height")
        tt.period = EFrame.bind(t, "period")
        tt.effectiveShowRanks = EFrame.bind(t, "effectiveShowRanks")
        tt.effectivePeriod = EFrame.bind(t, "effectivePeriod")
        tt.iconScale = EFrame.bind(t, "iconScale")
        tt.timeDirection = EFrame.bind(t, "timeDirection")
        tt.effectiveTimeDirection = EFrame.bind(t, "effectiveTimeDirection")
    end
end

local function initTimeline(n)
    local t = _G.SpellTimelinesProfile.timelines[n]
    local k, tt = t.target, Timeline(n)
    loadTimelineSettings(tt, t)
    timelines[n] = tt
    SpellTimelinesHandler:timelineAdded(n)
end

defaultColors = {
    CURRENT = {0,1,0,.8},
    SUCCESS = {0,1,0,.8},
    FAILED = {0.5,0,0,.8},
    INTERRUPTED = {1,.25,0,.8},
    LOST = {.5,.5,.5,.8},
    HIDE = {0,0,0,0},
    SUCCESS2 = {0,1,0,1},
    FAILED2 = {1,0,0,1},
}
    
function eventHandler.ADDON_LOADED(name)
    if name == addonName then
        if not _G.SpellTimelinesProfile then
            _G.SpellTimelinesProfile = _G.SpellTimelinesProfile or {timelines = {{target = "player"}, {target = "target", disabled = true}}}
        end
        
        if not SpellTimelinesProfile.name then
            SpellTimelinesProfile.name = format("%s (%s)", UnitName("player"), GetRealmName())
        end
        
        if not _G.SpellTimelinesSettings then
            _G.SpellTimelinesSettings = {}
        end
        
        if not SpellTimelinesSettings.profiles then
            SpellTimelinesSettings.profiles = {}
        end
        
        if not SpellTimelinesSettings.profiles[SpellTimelinesProfile.name] then
            SpellTimelinesSettings.profiles[SpellTimelinesProfile.name] = SpellTimelinesProfile
        end
        
        setmetatable(SpellTimelinesSettings, {__index = defaultSettings})
        setmetatable(SpellTimelinesProfile, {__index = defaultProfile})
        
        SpellTimelinesHandler:connect("periodChanged", function(n) SpellTimelinesProfile.period = n end)
        SpellTimelinesHandler:connect("timeDirectionChanged", function(n) SpellTimelinesProfile.timeDirection = n end)
        SpellTimelinesHandler:connect("noBordersChanged", function(n) SpellTimelinesProfile.noBorders = n end)
        SpellTimelinesHandler:connect("showRanksChanged", function(n) SpellTimelinesProfile.showRanks = n end)
        
        if not SpellTimelinesSettings.colors then
            SpellTimelinesSettings.colors = defaultColors
        end
        borderColors = SpellTimelinesSettings.colors
        SpellTimelinesHandler.currentProfile = SpellTimelinesProfile.name
        
    
        
        if not ClassicData then
            ClassicData = {}
        end
        if not _G.SpellTimelinesSettings.ClassicData or _G.SpellTimelinesSettings.ClassicData.version ~= ClassicData.version or SpellTimelinesSettings.ClassicData.lastItem or _G.SpellTimelinesSettings.ClassicData.project ~= WOW_PROJECT_ID then
            local dialog = EFrame.Window()
            dialog.anchorCenter = EFrame.root.center
            dialog.centralItem = EFrame.ColumnLayout(dialog)
            dialog.title = "SpellTimelines"
            dialog.message = EFrame.Label(dialog.centralItem)
            dialog.progress = EFrame.ProgressBar(dialog.centralItem)
            dialog:attach("i")
            dialog:attach("pause")
            dialog.i = 0
            dialog.lastItem = 172070
            dialog.message.text = EFrame.bind(function() return dialog.i == dialog.lastItem and "|c00ff0000Reload UI now to avoid peformance issues.|r" or "Building item cache, please wait..." end)
            dialog.progress.color = EFrame.bind(function() return dialog.i == dialog.lastItem and {0,1,0} or {0,0,1} end)
            dialog.progress.value = EFrame.bind(function() return dialog.i end)
            dialog.progress.to = dialog.lastItem
            dialog.progress.Layout.fillWidth = true
            dialog.progress.implicitHeight = 20
            dialog.progress.label = EFrame.Label(dialog.progress)
            dialog.progress.label.text = EFrame.bind(function() return format("%d/%d", dialog.i, dialog.lastItem) end)
            dialog.progress.label.anchorFill = dialog.progress
            dialog.closeButton.visible = false
            if not _G.SpellTimelinesSettings.ClassicData or _G.SpellTimelinesSettings.ClassicData.version ~= ClassicData.version or _G.SpellTimelinesSettings.ClassicData.project ~= WOW_PROJECT_ID then
                _G.SpellTimelinesSettings.ClassicData = ClassicData
                ClassicData.project = WOW_PROJECT_ID
            end
            local ClassicData = _G.SpellTimelinesSettings.ClassicData
            dialog.pause = ClassicData.pause
            ClassicData.pause = nil
            dialog.i = ClassicData.lastItem or 0
            local function work()
                for i = ClassicData.lastItem or 1, dialog.lastItem do
                    local name, id = GetItemSpell(i)
                    if name then
                        if useNames then
                            if not ClassicData[name] then
                                ClassicData[name] = {}
                            end
                            ClassicData[name].Item = id
                        end
                        if not ClassicData[id] then
                            ClassicData[id] = {}
                        end
                        local icon = GetItemIcon(i)
                        if not ClassicData[id].itemIcon then
                            ClassicData[id].itemIcon = icon
                        elseif ClassicData[id].itemIcon ~= icon then
                            ClassicData[id].itemIcon = select(3, GetSpellInfo(id))
                        end
                    end
                    if debugprofilestop()/1000 - GetTime() > 0.017 then
                        coroutine.yield(i)
                    end
                end
            end
            local cowork = coroutine.create(work)
            function dialog.work()
                if dialog.pause then return end
                local ok, i = coroutine.resume(cowork)
                if ok and i then
                    SpellTimelinesSettings.ClassicData.lastItem = i
                    dialog.i = i
                else
                    EFrame.root:disconnect("update", dialog.work)
                    dialog.i = dialog.lastItem
                    SpellTimelinesSettings.ClassicData.lastItem = nil
                end
            end
            dialog.message2 = EFrame.Label(dialog.centralItem)
            dialog.message2.text = "ReloadUI if stuttering"
            local displaym2 = false
            dialog.message2.visible = EFrame.bind(function() return dialog.pause and displaym2 end)
            displaym2 = true
            dialog.buttonRow = EFrame.RowLayout(dialog.centralItem)
            dialog.buttonRow.Layout.alignment = EFrame.Layout.AlignRight
            dialog.pauseButton = EFrame.Button(dialog.buttonRow)
            dialog.pauseButton.text = "Pause"
            dialog.pauseButton.checkable = true
            dialog.pauseButton.checked = dialog.pause
            dialog.pauseButton:connect("checkedChanged", function(c) dialog.pause = c end)
            dialog.reload = EFrame.Button(dialog.buttonRow)
            dialog.reload.text = "ReloadUI"
            dialog.reload.enabled = EFrame.bind(function() return dialog.i == dialog.lastItem or dialog.pause end)
            dialog.reload:connect("clicked", function() ClassicData.pause = dialog.pause ReloadUI() end)
            EFrame.root:connect("update", dialog.work)
        else
            ClassicData = _G.SpellTimelinesSettings.ClassicData
        end
    end
end

timelines = {}

function eventHandler.UNIT_SPELLCAST_SUCCEEDED(target, id, spellId)
    for _, line in pairs(timelines) do
        if line.active and (line.target == target or line.target == UnitName(target) or line.target == UnitGUID(target)) then
            line.proper = GetTime()
            line:castSucceeded(spellId, id)
        end
    end
end

function eventHandler.UNIT_SPELLCAST_STOP(target, id, spellId)
    for _, line in pairs(timelines) do
        if line.active and (line.target == target or line.target == UnitName(target) or line.target == UnitGUID(target)) then
            line.proper = GetTime()
            line:castStop(spellId, id)
        end
    end
end

function eventHandler.UNIT_SPELLCAST_FAILED(target, id, spellId)
    for _, line in pairs(timelines) do
        if line.active and (line.target == target or line.target == UnitName(target) or line.target == UnitGUID(target)) then
            line.proper = GetTime()
            line:castFailed(spellId, id)
        end
    end
end

eventHandler.UNIT_SPELLCAST_INTERRUPTED = eventHandler.UNIT_SPELLCAST_FAILED
eventHandler.UNIT_SPELLCAST_FAILED_QUIET = eventHandler.UNIT_SPELLCAST_FAILED


function eventHandler.UNIT_SPELLCAST_START(target, id, spellId)
    for _, line in pairs(timelines) do
        if line.active and (line.target == target or line.target == UnitName(target) or line.target == UnitGUID(target)) then
            line.proper = GetTime()
            line:castStart(spellId, id)
        end
    end
end

function eventHandler.UNIT_SPELLCAST_CHANNEL_START(target, _, spellId)
    for _, line in pairs(timelines) do
        if line.active and line.lastChannelStart ~= GetTime() and (line.target == target or line.target == UnitName(target) or line.target == UnitGUID(target)) then
            if line.channeling then
                line.channeling.action.channeling = false
                line.channeling.action.castEnd = GetTime()
                line.channeling.action.castResult = "SUCCESS"
                line.lastSuccess = nil
            end
            local spell = line.lastSuccess and GetSpellInfo(line.lastSuccess.action.spellId) == GetSpellInfo(spellId) and line.lastSuccess or line:getSpell(spellId)
            line.channeling = spell
            spell.action.channeling = true
            spell.action:successCast()
            if spell == line.lastSuccess then
                line.lastSuccess.action.castEnd = nil
            end
            local effect = line.effects[GetSpellInfo(spellId)]
            if effect then
                spell.action.rate = effect.rate
                spell.action.crit = effect.crit
                spell.action.rateN = effect.rateN
                effect.rate = nil
                effect.crit = 0
            end
            if not line.lastSpells[GetSpellInfo(spellId)] then
                line.lastSpells[GetSpellInfo(spellId)] = {spell.action}
            else
                line.lastSpells[GetSpellInfo(spellId)][1] = spell.action
            end
            line.lastChannelStart = GetTime()
        end
    end
end

function eventHandler.UNIT_SPELLCAST_CHANNEL_STOP(target, _, spellId)
    for _, line in pairs(timelines) do
        if line.active and line.lastChannelStop ~= GetTime() and (line.target == target or line.target == UnitName(target) or line.target == UnitGUID(target) or line.oldTarget == UnitGUID(target)) and line.channeling then
            line.channeling.action.channeling = false
            line.channeling.action.castEnd = GetTime()
            line.channeling.action.castResult = "SUCCESS"
            line.channeling = nil
            line.lastSuccess = nil
            line.lastChannelStop = GetTime()
        end
    end
end

-- function eventHandler.UNIT_SPELLCAST_CHANNEL_UPDATE(target, ...)
-- end

-- function eventHandler.UNIT_SPELLCAST_SENT(target, _, spellId, id)
-- end


function eventHandler.UNIT_NAME_UPDATE(unit)
    for _,t in ipairs(timelines) do
        if t.target == unit then
            if t.active then
                if not UnitExists(unit) then
                    if t.currentCast then
                        t:currentCastLost()
                    end
                elseif t.oldTarget ~= UnitGUID(unit) then
                    t:clear()
                    t.oldTarget = UnitGUID(unit)
                end
            end
        end
    end
end

function eventHandler.UNIT_TARGET(unit)
    eventHandler.UNIT_NAME_UPDATE(unit == "player" and "target" or unit .. "target")
end

local spellIDByName
local classCache = {}

if not useNames then
    function spellIDByName(_, spellId)
        return spellId
    end
else
    local playerClasses = {DRUID = 1, HUNTER = 1, MAGE = 1, PALADIN = 1, PRIEST = 1, ROGUE = 1, SHAMAN = 1, WARLOCK = 1, WARRIOR = 1, Item = 0, Racial = 0, Profession = 0}
    function spellIDByName(str, spellId, guid, name, unit)
        local isPlayer = guid == UnitGUID("player")
        local isHuman = strfind(guid, "Player")
        local class = not isHuman and name or select(2,UnitClass(unit)) or classCache[guid]
        if isHuman and (not classCache[guid] or classCache[guid] ~= class) then
            if class then
                classCache[guid] = class
            else
                local hits = 0
                local hit
                if ClassicData[str] then
                    for k in pairs(ClassicData[str]) do
                        if playerClasses[k] then
                            hits = hits +1
                            if playerClasses[k] == 1 then
                                hit = k
                            end
                        end
                    end
                    if hits == 1 and hit then
                        class = hit
                        classCache[guid] = class
                    end
                end
            end
        end 
        local id = spellId ~= 0 and spellId or isPlayer and select(7,GetSpellInfo(str)) or ClassicData[str] and (class and ClassicData[str][class] or isHuman and (ClassicData[str].Racial or ClassicData[str].Item or ClassicData[str].Profession))
        return GetSpellInfo(id) and id or nil
    end
end


function eventHandler.NAME_PLATE_UNIT_ADDED(unit)
    if SpellTimelinesHandler.nameplates.disabled then return end
    local v = timelines[unit]
    if not v then
        v = Timeline(unit, 1)
        loadTimelineSettings(v, SpellTimelinesHandler.nameplates, true)
        timelines[unit] = v
    end
    v.smart = true
    local frame = C_NamePlate.GetNamePlateForUnit(unit)
    v.anchorBottomRight = {frame=frame, point="TOPRIGHT"}
    v.anchorBottomLeft = {frame=frame, point="TOPLEFT"}
    v.visible = true
end

function eventHandler.NAME_PLATE_UNIT_REMOVED(unit)
    local v = timelines[unit]
    if v then
        v.visible = false
        v:clear()
        v:clearAnchors()
    end
end

local function processCombatEvent(...)
    local timestamp, subevent, _, sourceGUID, sourceName, sourceFlags, sourceRaidFlags, destGUID, destName, destFlags, destRaidFlags = ...
    for k,line in pairs(timelines) do
        local guid, name
        if subevent == "SPELL_INTERRUPT" then
            guid = destGUID
            name = destName
        else
            guid = sourceGUID
            name = sourceName
        end
        if line.active and (line.target == guid or line.target == name or UnitGUID(line.target) == guid) then
            local spellId, force, process = nil, nil, true
            local amount, overkill, school, resisted, blocked, absorbed, critical, glancing, crushing, overheal, why, spellName = 0, 0, 0, 0, 0, 0, nil, 0, 0, 0
            if subevent == "SPELL_HEAL" then
                spellId, spellName, _, amount, overheal, absorbed, critical = select(12, ...)
                spellId = spellIDByName(spellName, spellId, guid, name, line.target)
                force = true
            elseif subevent == "SPELL_PERIODIC_HEAL" then
                spellId, spellName, a, amount, overheal, absorbed, critical = select(12, ...)
                spellId = spellIDByName(spellName, spellId, guid, name, line.target)
            elseif subevent == "SPELL_DAMAGE" then
                spellId, spellName, _, amount, _, _, resisted, blocked, absorbed, critical = select(12, ...)
                spellId = spellIDByName(spellName, spellId, guid, name, line.target)
                force = true
            elseif subevent == "SPELL_PERIODIC_DAMAGE" then
                spellId, spellName, _, amount, _, _, resisted, blocked, absorbed, critical = select(12, ...)
                spellId = spellIDByName(spellName, spellId, guid, name, line.target)
            elseif subevent == "SPELL_AURA_APPLIED" or subevent == "SPELL_AURA_REFRESH" or subevent == "SPELL_AURA_APPLIED_DOSE" or subevent == "SPELL_AURA_DISPEL" then
                spellId, spellName = select(12, ...)
                spellId = spellIDByName(spellName, spellId, guid, name, line.target)
                amount = 1
                force = true
            elseif subevent == "SPELL_MISSED" or subevent == "SPELL_AURA_DISPEL_FAILED" then
                spellId, spellName = select(12, ...)
                spellId = spellIDByName(spellName, spellId, guid, name, line.target)
                amount = 1
                resisted = 1
            elseif subevent == "SPELL_CAST_SUCCESS" then
                spellId, spellName = select(12, ...)
                spellId = spellIDByName(spellName, spellId, guid, name, line.target)
                if not spellId then return end
                if not line.proper or GetTime() - line.proper > 0.25 then
                    line:castSucceeded(spellId, nil, destGUID == UnitGUID("player") or destGUID == "")
                end
                local queue = line.lastSpells[spellName]
                if queue and #queue > 0 then
                    if queue[1] == false then
                        queue[1] = nil
                    else
                        local spell = tremove(queue, 1)
                        local tt = spell.spellId == spellId and destGUID ~= "" and destGUID or "AoE"
                        if not queue[tt] then
                            queue[tt] = {}
                        end
                        tinsert(queue[tt], spell)
                    end
                end
                process = false
            elseif subevent == "SPELL_CAST_START" then
                spellId, spellName = select(12, ...)
                spellId = spellIDByName(spellName, spellId, guid, name, line.target)
                if not spellId then return end
                if not line.proper or GetTime() - line.proper > 0.25 then
                    if line.spells[spellId] then
                        if line.spells[spellId].action.status == "CURRENT" then
                            line.spells[spellId].action:lost()
                        end
                        line.spells[spellId] = nil
                    end
                    line:currentCastLost()
                    line:castStart(spellId, nil, UnitGUID(line.id.."target") == UnitGUID("player"))
                    line.currentCast.action.castResult = "LOST"
                end
                process = false
            elseif subevent == "SPELL_INTERRUPT" then
                spellId, spellName, _, interruptedId, interruptedName = select(12, ...)
                spellId = spellIDByName(interruptedName, interruptedId, guid, name, line.target)
                if not spellId then return end
                if not line.proper or GetTime() - line.proper > 0.25 then
                    line:castInterrupted(spellId, nil, destGUID == UnitGUID("player"))
                    line.currentCast = nil
                end
                process = false
            end
            if spellId and process then
                local queue = line.lastSpells[spellName] and (line.lastSpells[spellName].AoE or line.lastSpells[spellName][destGUID])
                local effect
                if queue then
                    while #queue > 1 and (queue[1] == false or (queue[1].castEnd and GetTime() - queue[1].castEnd > 3 or queue[1].rate)) do
                        tremove(queue, 1)
                    end
                    if #queue == 1 then
                        if force and (queue[1] == false or queue[1].proper and sourceGUID == destGUID) then
                            tremove(queue, 1)
                        else
                            effect = queue[1]
                        end
                    else
                        effect = tremove(queue, 1)
                    end
                end
                if not (effect == false or not effect and not force) then
                    if not effect then
                        if not line.effects[spellName] then
                            effect = {crit = 0}
                            line.effects[spellName] = effect
                        else
                            effect = line.effects[spellName]
                        end
                    end
                    effect.spellId = spellId
                    if effect.rate then
                        effect.rate = (effect.rate*effect.rateN + (amount - (resisted or 0))/amount) / (effect.rateN +1)
                        effect.rateN = effect.rateN + 1
                    else
                        effect.rate = (amount - (resisted or 0))/amount
                        effect.rateN = 1
                    end
                    effect.crit = (effect.crit * (effect.rateN -1) + (critical and 1 or 0))/effect.rateN
                end
            end
        end
    end
end

function eventHandler.COMBAT_LOG_EVENT_UNFILTERED()
--      local timestamp, subevent, _, sourceGUID, sourceName, sourceFlags, sourceRaidFlags, destGUID, destName, destFlags, destRaidFlags = CombatLogGetCurrentEventInfo()
--       if sourceName == UnitName("player") or sourceName == UnitName("target") then
--           print(CombatLogGetCurrentEventInfo())
--       end
    processCombatEvent(CombatLogGetCurrentEventInfo())
end

eventHandler:SetScript("OnEvent", EFrame:makeAtomic(function(self, event, ...) self[event](...) end))

SpellTimelinesHandler = EFrame.Object()

SpellTimelinesHandler:attach("unlocked")
SpellTimelinesHandler:attach("timeDirection")
SpellTimelinesHandler:attach("noBorders")
SpellTimelinesHandler:attach("showRanks")
SpellTimelinesHandler:attach("period")
SpellTimelinesHandler:attach("timelines")
SpellTimelinesHandler:attach("currentProfile", nil, "loadConfig")
SpellTimelinesHandler:attachSignal("timelineAdded")
SpellTimelinesHandler:attachSignal("timelineRemoved")
SpellTimelinesHandler.__timelines = {}

function SpellTimelinesHandler:addTimeline()
    local k = #self.timelines + 1
    self.timelines[k] = {}
    self.timelinesChanged(self.timelines)
    initTimeline(k)
end

function SpellTimelinesHandler:removeTimeline(k)
    self.timelines[k] = nil
    timelines[k]:deleteLater()
    timelines[k] = nil
    self.timelinesChanged(self.timelines)
    self:timelineRemoved(k)
end

function SpellTimelinesHandler:loadConfig(name)
    if self.__currentProfile == name then
        return
    end
    if #timelines > 0 then
        for k, t in pairs(timelines) do
            t:deleteLater()
            timelines[k] = nil
            SpellTimelinesHandler:timelineRemoved(k)
        end
    end
    
    if not _G.SpellTimelinesSettings.profiles[name] then
        _G.SpellTimelinesSettings.profiles[name] = {name = name, timelines = {{target = "player"}, {target = "target", disabled = true}}}
    end
    
    _G.SpellTimelinesProfile = _G.SpellTimelinesSettings.profiles[name]
    
    setmetatable(SpellTimelinesProfile, {__index = defaultProfile})
    
    self.__currentProfile = name
    
    SpellTimelinesHandler.period = SpellTimelinesProfile.period
    
    SpellTimelinesHandler.timeDirection = SpellTimelinesProfile.timeDirection
    
    SpellTimelinesHandler.showRanks = SpellTimelinesProfile.showRanks
    
    SpellTimelinesHandler.noBorders = SpellTimelinesProfile.noBorders
    
    SpellTimelinesHandler.timelines = SpellTimelinesProfile.timelines
    if SpellTimelinesProfile.timelines.player then
        SpellTimelinesProfile.timelines[1] = SpellTimelinesProfile.timelines.player
        SpellTimelinesProfile.timelines[1].target = "player"
    end
    if SpellTimelinesProfile.timelines.target then
        SpellTimelinesProfile.timelines[2] = SpellTimelinesProfile.timelines.target
        SpellTimelinesProfile.timelines[2].target = "target"
    end
    local ttime = 0
    for k,t in pairs(_G.SpellTimelinesProfile.timelines) do
        if tonumber(k) then
            initTimeline(tonumber(k))
            ttime = ttime + timelines[tonumber(k)].effectivePeriod
        else
            _G.SpellTimelinesProfile.timelines[k] = nil
        end
    end
    if _G.SpellTimelinesSettings.precache then
        for i = #recycledFrames, ttime do
            tinsert(recycledFrames, TimelineSpell(timelines[1], Spell()))
        end
    end
    if not _G.SpellTimelinesProfile.nameplates then
        _G.SpellTimelinesProfile.nameplates = {}
    end
    local plates = Timeline(0.1, 2)
    plates.target = "nameplates"
    nameplateAnchor = EFrame.Rectangle()
    nameplateAnchor.mouse = EFrame.MouseArea(nameplateAnchor)
    nameplateAnchor.mouse.anchorFill = nameplateAnchor
    nameplateAnchor.mouse.dragTarget = nameplateAnchor
    nameplateAnchor.anchorTopLeft = EFrame.root.topLeft
    nameplateAnchor.height = EFrame.normalize(20)
    nameplateAnchor.width = EFrame.normalize(150)
    nameplateAnchor.marginTop = (EFrame.root.height - nameplateAnchor.height)/2
    nameplateAnchor.marginLeft = (EFrame.root.width - nameplateAnchor.width)/2
    nameplateAnchor.label = EFrame.Label(nameplateAnchor)
    nameplateAnchor.label.anchorCenter = nameplateAnchor
    nameplateAnchor.label.text = "NAMEPLATE"
    nameplateAnchor.visible = EFrame.bind(SpellTimelinesHandler, "unlocked")
    plates.anchorBottom = nameplateAnchor.top
    plates.width = nameplateAnchor.width
    loadTimelineSettings(plates, _G.SpellTimelinesProfile.nameplates)
    plates.active = false
    
    SpellTimelinesHandler.nameplates = plates
    self:currentProfileChanged(name)
end

