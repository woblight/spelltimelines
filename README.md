# SpellTimelines

Addon for World of Warcraft (retail and classic).

## Description

It displays timelines of the spells being casted by the player and other units.

## Installation

If you are downloading from gitlab, extract the folder contained into `Interface\AddOns` and rename it to SpellTimelines. To check it's correctly installed be sure the `.toc` file is located at `Interface\AddOns\SpellTimelines\SpellTimelines.toc`.

### Required Dependency

To work, SpellTimelines requires [EmeralFramework](https://gitlab.com/woblight/EmeraldFramework) addon to be also installed.

## Usage

The timelines can be moved, resized, added and removed and options can be viewed by sending the `/stl` chat command.

## Author

WobLight [Discord](https://discord.gg/5v8Rvyq)

## Thanks to

Lezonta for his great support and testing.
