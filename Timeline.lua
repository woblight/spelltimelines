local addonName, addonTable = ...

setmetatable(addonTable, {__index = getfenv() })
setfenv(1, addonTable)

EFrame.newClass("TimelineSpell" , EFrame.Rectangle)

TimelineSpell:attach("row")
TimelineSpell.__row = 0

function TimelineSpell:new(parent, action)
    EFrame.Rectangle.new(self, parent)
    self.action = action
    action.parent = self
    self.border = 2
    self.borderColor = {.8,.8,.8,.8}
    self.mirror = SpellIcon(self, action)
    self.anchorBind = EFrame.bind(function ()
        self:clearAnchors()
        if self.parent.effectiveTimeDirection then
            self.anchorRight = self.parent.right
        else
            self.anchorLeft = self.parent.left
        end
        self.anchorTop = self.parent.top
        self.anchorBottom =  self.parent.bottom
    end)
    self.anchorBind.parent = self
    self.anchorBind:update()
    self.mirror.anchorBottom = EFrame.bind(function() return self.parent.effectiveTimeDirection and self.topLeft or self.topRight end)
    self.mirror.scale = EFrame.bind(function() return self.parent.iconScale * (self.action.crit*0.2 + 1) end)
   -- self.mirror.width = EFrame.bind(function() return 24 * self.parent.iconScale * (self.action.crit*0.2 + 1) end)
    self.mirror.height = EFrame.bind(function() return EFrame.normalize(24) * (1 - self.row * 0.15) end)
    EFrame.root:connect("update", self, "update")
    self.color = EFrame.bind(function() return borderColors[self.action.castResult] or borderColors.HIDE end)
    self.label = EFrame.Label(self)
    self.label.marginBottom = 2
    self.label.anchorBind = EFrame.bind(function() self.label:clearAnchors() if self.parent.effectiveTimeDirection then self.label.anchorBottomLeft = self.bottomLeft else self.label.anchorBottomRight = self.bottomRight end end)
    self.label.anchorBind.parent = self.label
    self.label.anchorBind:update()
    self.label.visible = EFrame.bind(function() return self.label.width < self.width and self.label.height + 2* self.label.marginBottom < self.height end)
    self.visible = EFrame.bind(function() return self.action.status ~= "HIDE" end)
end

function TimelineSpell:destroy()
    for k, v in ipairs(recycledFrames) do
        if v == self then
            tremove(recycledFrames, k)
        end
    end
    EFrame.Rectangle.destroy(self)
end

function TimelineSpell:update()
    if not self.action.castStart or self.action.__status == "HIDE" then return end
    if self.action.castEnd and GetTime() - self.action.castEnd > self.__parent.__effectivePeriod then
        self:reset()
        return
    end
    if not self.action.castEnd then
        if self.action.castResult  == "LOST" and GetTime() - self.action.castStart > select(4, GetSpellInfo(self.action.spellId)) / 1000 + 0.1 then
            self.action.castEnd = GetTime()
        end
    end
    local b = self.action.castEnd or GetTime()
    local w = (b - self.action.castStart) * self.__parent.__timeScale
    w = math.min(self.__parent.__width,math.max(w,4))
    local s
    if self.action.castEnd then
        s = (GetTime() - self.action.castEnd) * self.__parent.__timeScale
        w = math.min(w, self.__parent.__width - s)
        local t = self.action.castEnd - self.action.castStart
        self.label.text = string.format(t < 1 and "%.1f" or "%.2f" ,t)
        if w <= 0 then
            self.opacity = 0
        else
            self.opacity = w / math.max(math.min(self.__parent.__width, (self.action.castEnd - self.action.castStart)*self.__parent.__timeScale), w)
        end
    else
        local t = GetTime() - self.action.castStart
        self.label.text = string.format(t < 1 and "%.1f" or "%.2f" ,t)
        s = 0
        self.opacity = 1
    end
    w = EFrame.round(w)
    if w == 0 then
        self:reset()
        return
    end
    self.width = w
    if self.__parent.__effectiveTimeDirection then
        self.marginRight = s
    else
        self.marginLeft = s
    end
end

function TimelineSpell:reset()
    local parent = self.__parent
    parent:disconnect("clear", self, "reset")
    local id = GetSpellInfo(self.action.spellId)
    if parent.currentCast == self then parent.currentCast = nil end
    if parent.lastSuccess == self then parent.lastSuccess = nil end
    if self.action.castId and self.action.castId ~= self.action.spellId then parent.spells[self.action.castId] = nil end
    if parent.spells[self.action.spellId] == self then parent.spells[self.action.spellId] = nil end
    if parent.lastSpells[id] then
        for k,t in pairs(parent.lastSpells[id]) do
            if t[1] == self.action then
                t[1] = false
            end
            for k,s in pairs(t) do
                if s == self.action then
                    t[k] = false
                end
            end
        end
    end
    if parent.channeling == self then
        parent.channeling = nil
    end
    if parent.lastStarted == self then
        parent.lastStarted = nil
    end
    if parent.lastRowZero == self then
        parent.lastRowZero = nil
    end
    self.action.status = "HIDE"
    self.row = 0
    tinsert(recycledFrames, self)
    self.parent.nspells = self.parent.nspells - 1
end

EFrame.newClass("Timeline" , EFrame.Item)
Timeline:attach("period")
Timeline:attach("effectivePeriod")
Timeline:attach("timeScale")
Timeline:attach("timeDirection")
Timeline:attach("effectiveTimeDirection")
Timeline:attach("noBorders")
Timeline:attach("effectivePeriod")
Timeline:attach("effectiveNoBorders")
Timeline:attach("showRanks")
Timeline:attach("effectiveShowRanks")
Timeline:attach("target")
Timeline:attach("disabled")
Timeline:attach("active")
Timeline:attach("iconScale")
Timeline:attach("nspells")
Timeline:attachSignal("clear")
Timeline.__effectivePeriod = 10
Timeline.__disabled = false
Timeline.__iconScale = 1
Timeline.__nspells = 0

function Timeline:new(id, plate)
    self.spells = {}
    self.lastSpells = {}
    self.effects = {}
    self.id = id
    self.plate = plate
    if plate == 1 then self.__target = id end
    EFrame.Item.new(self)
    self.timeScale = EFrame.bind(function() return self.width/self.effectivePeriod end)
    if plate ~= 1 then
        self.anchor = EFrame.MouseArea(self)
        self.anchor.anchorFill = self
        self.anchor.visible = false
        self.dragBackground = EFrame.Rectangle(self.anchor)
        self.dragBackground.color = {0,0,0,0}
        self.dragBackground.borderColor = {.5,.5,1,1}
        self.dragBackground.anchorFill = self
        if not plate then
            self.ticks = {}
            self.border = EFrame.Item(self)
            self.border.anchorFill = self
            self.border.visible = EFrame.bind(function () return self.nspells > 0 or SpellTimelinesHandler.unlocked end)
            self.borderBind = EFrame.bind(function ()
                if not (not self.effectiveNoBorders or SpellTimelinesHandler.unlocked) then
                    if self.rightBorder then
                        self.rightBorder:deleteLater()
                        self.bottomBorder:deleteLater()
                        self.label:deleteLater()
                        self.rightBorder = nil
                        self.bottomBorder = nil
                        self.label = nil
                    end
                    if #self.ticks > 0 then
                        for k, t in ipairs(self.ticks) do
                            t:deleteLater()
                            self.ticks[k] = nil
                        end
                    end
                else
                    if not self.rightBorder then
                        self.rightBorder = EFrame.Rectangle(self.border)
                        self.rightBorder.width = 2
                        self.rightBorder.color = {0.3,0.35,0.25,.8}
                        self.bottomBorder = EFrame.Rectangle(self.border)
                        self.bottomBorder.anchorTop = self.bottom
                        self.label = EFrame.Label(self.border)
                        self.label.text = id
                        self.label.anchorBottom = self.bottom
                        self.label.margins = 2
                    end
                    self.rightBorder:clearAnchors()
                    self.label:clearAnchors()
                    self.rightBorder.anchorTop = self.top
                    self.rightBorder.anchorBottom = self.bottom
                    if self.effectiveTimeDirection then
                        self.rightBorder.anchorLeft = self.right
                        self.bottomBorder.anchorLeft = self.left
                        self.bottomBorder.anchorRight = self.rightBorder.right
                        self.label.anchorLeft = self.rightBorder.right
                    else
                        self.rightBorder.anchorRight = self.left
                        self.bottomBorder.anchorLeft = self.rightBorder.left
                        self.bottomBorder.anchorRight = self.right
                        self.label.anchorRight = self.rightBorder.left
                    end
                    self.bottomBorder.height = 2
                    self.bottomBorder.color = {0.3,0.35,0.25,.8}
                    local d = self.timeScale
                    local i = 1
                    if d > 0 then
                        local c = math.ceil(10 / self.timeScale)
                        while d * c < 4 do
                            c = c * 2
                        end
                        local m = c
                        local m2 = c
                        local last
                        while i <= self.effectivePeriod do
                            local t = self.ticks[i]
                            if not t then
                                t = EFrame.Rectangle(self.border)
                                t.z = 3
                                t.width = 1
                                t.color = {1,0.35,0.25,.8}
                                t.t = t.t or EFrame.Label(t)
                                t.t.anchorTop = t.bottom
                            end
                            t:clearAnchors()
                            if self.effectiveTimeDirection then
                                t.marginRight = i * d -1
                                t.anchorRight = self.rightBorder.left
                            else
                                t.marginLeft = i * d -1
                                t.anchorLeft = self.rightBorder.right
                            end
                            t.marginTop = EFrame.bind(function() return (self.bottomBorder.height - t.height)/2 end)
                            t.anchorTop = self.bottomBorder.top
                            t.t.text = i
                            self.ticks[i] = t
                            if i <= 3 or i % 5 == 0 or i % 10 == 2 or i % 10 == 8 then
                                t.height = 10
                                local ao = true
                                if last then
                                    if self.effectiveTimeDirection then
                                        ao = last.__marginRight + last.t.__marginRight + last.t.__width < t.__marginRight + t.t.__marginRight
                                    else
                                        ao = last.__marginLeft + last.t.__marginLeft + last.t.__width < t.__marginLeft + t.t.__marginLeft
                                    end
                                end
                                if ao then
                                    t.t.visible = true
                                    last = t
                                else
                                    t.t.visible = false
                                end
                            else
                                t.t.visible = false
                                t.height = 7
                            end
                            i = i + 1
                        end
                    end
                    for k = i, #self.ticks do
                        self.ticks[k]:deleteLater()
                        self.ticks[k] = nil
                    end
                end
            end)
            self.borderBind.parent = self
            self.borderBind:update()
        end
        
        function getClosest(self, m, l, getM, getL, getS)
            local closest = {}
            for k, v in pairs(timelines) do
                if v ~= self and not v.__disabled then
                    local diff = math.abs(m - getM(v))
                    if not closest.diff or diff < closest.diff then
                        closest.diff = diff
                        closest.v = getM(v)
                        closest.a = 0
                        closest.t = v.anchor
                    end
                    diff = math.abs(m - (getM(v) + getL(v)))
                    if not closest.diff or diff < closest.diff then
                        closest.diff = diff
                        closest.v = getM(v) + getL(v)
                        closest.a = 1
                        closest.t = v.anchor
                    end
                    if l then
                        diff = math.abs(l - getL(v))
                        if not closest.diff or diff < closest.diff then
                            closest.diff = diff
                            closest.v = getL(v)
                            closest.a = 2
                            closest.t = v.anchor
                        end
                    end
                    if getS then
                        diff = math.abs(m - getS(v))
                        if not closest.diff or diff < closest.diff then
                            closest.diff = diff
                            closest.v = getS(v)
                            closest.a = 3
                            closest.t = v.anchor
                        end
                    end
                end
            end
            return closest
        end
        
        self.anchor:connect("pressedChanged", function (p)
            local handle = self.anchor
            if p then
                handle.ox = handle.mouseX
                handle.oy = handle.mouseY
            else
                self.snapIndicatorV.visible = false
                self.snapIndicatorH.visible = false
            end
        end)
        if not plate then
            self.anchor:connect("mouseXChanged", function (x)
                local handle = self.anchor
                if handle.__pressed then
                    local m = self.__marginLeft + (x - handle.ox)
                    if IsShiftKeyDown() then
                        local closest = getClosest(self, m, nil,
                                                    function(v) return v.__marginLeft end,
                                                    function(v) return v.__width end,
                                                    function(v) return EFrame.root.__width - (v.marginLeft + v.__width) end)
                        local closest2 = getClosest(self, m + handle.__width, nil,
                                                    function(v) return v.__marginLeft end,
                                                    function(v) return v.__width end,
                                                    function(v) return EFrame.root.__width - v.marginLeft end)
                        if closest.v and (not closest2.v or closest.diff <= closest2.diff) then
                            m = closest.v
                            if closest.a == 0 then
                                self.snapIndicatorH.anchorCenter = closest.t.left
                            elseif closest.a == 1 or closest.a == 3 then
                                self.snapIndicatorH.anchorCenter = closest.t.right
                            elseif closest.a == 2 then
                                self.snapIndicatorH.anchorCenter = closest.t.center
                            end
                            self.snapIndicatorH.visible = true
                        elseif closest2.v and (not closest.v or closest2.diff < closest.diff) then
                            m = closest2.v - handle.__width
                            if closest2.a == 0 or closest2.a == 3 then
                                self.snapIndicatorH.anchorCenter = closest2.t.left
                            elseif closest2.a == 1 then
                                self.snapIndicatorH.anchorCenter = closest2.t.right
                            elseif closest2.a == 2 then
                                self.snapIndicatorH.anchorCenter = closest2.t.center
                            end
                            self.snapIndicatorH.visible = true
                        end
                    end
                    self.marginLeft = EFrame.normalizedBind(m)
                end
            end)
        end
        self.anchor:connect("mouseYChanged", function (y)
            local handle = self.anchor
            if handle.__pressed then
                local m = self.plate and self.__marginBottom - (y - handle.oy) or self.__marginTop + (y - handle.oy)
                if IsControlKeyDown() and not plate then
                    local closest = getClosest(self, m, nil,
                                                function(v) return v.__marginTop end,
                                                function(v) return v.__height end)
                    local closest2 = getClosest(self, m + handle.__height, nil,
                                                function(v) return v.__marginTop end,
                                                function(v) return v.__height end)
                    if closest.v and (not closest2.v or closest.diff <= closest2.diff) then
                        m = closest.v
                        if closest.a == 0 then
                            self.snapIndicatorV.anchorCenter = closest.t.top
                        elseif closest.a == 1 or closest.a == 3 then
                            self.snapIndicatorV.anchorCenter = closest.t.bottom
                        elseif closest.a == 2 then
                            self.snapIndicatorV.anchorCenter = closest.t.center
                        end
                        self.snapIndicatorV.visible = true
                    elseif closest2.v and (not closest.v or closest2.diff < closest.diff) then
                        m = closest2.v - handle.__height
                        if closest2.a == 0 or closest2.a == 3 then
                            self.snapIndicatorV.anchorCenter = closest2.t.top
                        elseif closest2.a == 1 then
                            self.snapIndicatorV.anchorCenter = closest2.t.bottom
                        elseif closest2.a == 2 then
                            self.snapIndicatorV.anchorCenter = closest2.t.center
                        end
                        self.snapIndicatorV.visible = true
                    end
                end
                if self.plate then
                    self.marginBottom = EFrame.normalizedBind(m)
                else
                    self.marginTop = EFrame.normalizedBind(m)
                end
            end
        end)
        self.anchor.snapLabel = EFrame.Label(self.anchor)
        self.anchor.snapLabel.visible = EFrame.bind(self.anchor, "pressed")
        self.anchor.snapLabel.text = "Hold Shift to snap horizontally\nHold Control to snap vertically"
        self.anchor.snapLabel.anchorBottom = self.anchor.top
        self.anchor.visible = EFrame.bind(SpellTimelinesHandler, "unlocked")
        
        self.tex = EFrame.Image(self.anchor)
        self.tex.marginRight = EFrame.bind(function() return self.effectiveTimeDirection and 0 or self.width - self.tex.width end)
        self.tex.anchorRight = self.right
        self.tex.anchorTop = self.top
        self.tex.anchorBottom = self.bottom
        self.tex.width = EFrame.bind(self.tex, "height")
        self.tex.source = "Interface\\AddOns\\SpellTimelines\\handle.tga"
        self.tex.rotation = EFrame.bind(function() return self.effectiveTimeDirection and -math.pi/2 or math.pi/2 end)
        
        self.scalingHandles = {}
        function makeHandle(n, reversed)
            reversed = reversed or false
            local handle = EFrame.Button(self.anchor)
            handle.icon = "Interface\\Addons\\EmeraldFramework\\Textures\\ResizeHandle_small"
            handle.width=14
            handle.height=14
            handle.image.rotation = -math.pi/2 * n
            if n == 0 then
                handle.anchorTopLeft = self.bottomRight
            elseif n == 1 then
                handle.anchorTopRight = self.bottomLeft
            elseif n == 2 then
                handle.anchorBottomRight = self.topLeft
            else
                handle.anchorBottomLeft = self.topRight
            end
            handle.snapLabel = EFrame.Label(handle)
            handle.snapLabel.visible = EFrame.bind(handle, "pressed")
            handle.snapLabel.text = "Hold Shift to snap horizontally\nHold Control to snap vertically"
            if n == 0 then
                handle.snapLabel.anchorTopLeft = handle.bottomRight
            elseif n == 1 then
                handle.snapLabel.anchorTopRight = handle.bottomLeft
            elseif n == 2 then
                handle.snapLabel.anchorBottomRight = handle.topLeft
            else
                handle.snapLabel.anchorBottomLeft = handle.topRight
            end
            handle:connect("pressedChanged", function (p)
                local handle = handle
                if p then
                    handle.ox = handle.mouseX
                    handle.oy = handle.mouseY
                else
                    self.snapIndicatorV.visible = false
                    self.snapIndicatorH.visible = false
                end
            end)
            if not plate then
                handle:connect("mouseXChanged", function (x)
                    local handle = handle
                    if handle.__pressed then
                        if n == 1 or n == 2 then
                            local w = self.__width - (x - handle.ox)
                            local m = self.__marginLeft - (w - self.__width)
                            if IsShiftKeyDown() then
                                local closest = getClosest(self, m, w,
                                                        function(v) return v.__marginLeft end,
                                                        function(v) return v.__width end)
                                if closest.v then
                                    if closest.a == 2 then
                                        m = m - (closest.v - w)
                                        w = closest.v
                                    else
                                        w = w - (closest.v - m)
                                        m = closest.v
                                    end
                                    if closest.a == 0 then
                                        self.snapIndicatorH.anchorCenter = closest.t.left
                                    elseif closest.a == 1 then
                                        self.snapIndicatorH.anchorCenter = closest.t.right
                                    elseif closest.a == 2 then
                                        self.snapIndicatorH.anchorCenter = closest.t.center
                                    end
                                    self.snapIndicatorH.visible = true
                                end
                            end
                            self.marginLeft = EFrame.normalizedBind(m)
                            self.width = EFrame.normalizedBind(math.max(w, 16))
                        else
                            local w = self.__width + (x - handle.ox)
                            local m = self.__marginLeft + w
                            if IsShiftKeyDown() then
                                local closest = getClosest(self, m, w,
                                                        function(v) return v.__marginLeft end,
                                                        function(v) return v.__width end)
                                if closest.v then
                                    if closest.a == 2 then
                                        w = closest.v
                                    else
                                        w = w + (closest.v - m)
                                    end
                                    if closest.a == 0 then
                                        self.snapIndicatorH.anchorCenter = closest.t.left
                                    elseif closest.a == 1 then
                                        self.snapIndicatorH.anchorCenter = closest.t.right
                                    elseif closest.a == 2 then
                                        self.snapIndicatorH.anchorCenter = closest.t.center
                                    end
                                    self.snapIndicatorH.visible = true
                                end
                            end
                            self.width = EFrame.normalizedBind(math.max(w, 16))
                        end
                    end
                end)
            end
            handle:connect("mouseYChanged", function (y)
                local handle = handle
                if handle.__pressed then
                    if (n == 0 or n == 1) ~= reversed then
                        local h = self.__height + (y - handle.oy) * (reversed and -1 or 1)
                        local m = self.__marginTop + h
                        if IsControlKeyDown() then
                            local closest = getClosest(self, m, h,
                                                       function(v) return v.__marginTop end,
                                                       function(v) return v.__height end)
                            if closest.v then
                                if closest.a == 2 then
                                    h = closest.v
                                else
                                    h = h + (closest.v - m)
                                end
                                if closest.a == 0 then
                                    self.snapIndicatorV.anchorCenter = closest.t.top
                                elseif closest.a == 1 then
                                    self.snapIndicatorV.anchorCenter = closest.t.bottom
                                elseif closest.a == 2 then
                                    self.snapIndicatorV.anchorCenter = closest.t.center
                                end
                                self.snapIndicatorV.visible = true
                            end
                        end
                        self.height = EFrame.normalizedBind(math.max(h, 4))
                    else
                        local h = self.__height - (y - handle.oy) * (reversed and -1 or 1)
                        local m = self.__marginTop - (h - self.__height)
                        if IsControlKeyDown() then
                            local closest = getClosest(self, m, h,
                                                       function(v) return v.__marginTop end,
                                                       function(v) return v.__height end)
                            if closest.v then
                                if closest.a == 2 then
                                    m = m - (closest.v - h)
                                    h = closest.v
                                else
                                    h = h - (closest.v - m)
                                    m = closest.v
                                end
                                if closest.a == 0 then
                                    self.snapIndicatorV.anchorCenter = closest.t.top
                                elseif closest.a == 1 then
                                    self.snapIndicatorV.anchorCenter = closest.t.bottom
                                elseif closest.a == 2 then
                                    self.snapIndicatorV.anchorCenter = closest.t.center
                                end
                                self.snapIndicatorV.visible = true
                            end
                        end
                        self.height = EFrame.normalizedBind(math.max(h, 16))
                        self.marginTop = EFrame.normalizedBind(m)
                    end
                end
            end)
            return handle
        end
        if self.plate == 2 then
            for i = 0, 1 do
                self.scalingHandles[i] = makeHandle(i+2, true)
            end
        else
            for i = 0, 3 do
                self.scalingHandles[i] = makeHandle(i)
            end
        end
        function snapIndicator(vertical)
            local i = EFrame.Rectangle(self)
            i.width = vertical and 50 or 10
            i.height = vertical and 10 or 50
            i.color = {0,1,0}
            i.z = 10
            return i
        end
        self.snapIndicatorV = snapIndicator(true)
        self.snapIndicatorH = snapIndicator()
        self.iconPlaceholder = EFrame.Rectangle(self)
        self.iconPlaceholder.width = EFrame.bind(function() return self.iconScale * EFrame.normalize(24) end)
        self.iconPlaceholder.height = EFrame.bind(self.iconPlaceholder, "width")
        self.iconPlaceholder.borderWidth = 4
        self.iconPlaceholder.borderColor = {0,1,0,1}
        self.iconPlaceholder.color = {0,0,0,0}
        self.iconPlaceholder.marginBottom = EFrame.bind("self.height*(self.scale-1)/2")
        self.iconPlaceholder.anchorBottom = EFrame.bind(function() return self.effectiveTimeDirection and self.topRight or self.topLeft end)
        
        self.iconPlaceholder.scalingHandle = EFrame.Button(self.iconPlaceholder)
        self.iconPlaceholder.scalingHandle.icon = "Interface\\Addons\\EmeraldFramework\\Textures\\ResizeHandle_small"
        self.iconPlaceholder.scalingHandle.width=14
        self.iconPlaceholder.scalingHandle.height=14
        self.iconPlaceholder.scalingHandle.image.rotation = -math.pi/2
        self.iconPlaceholder.scalingHandle.anchorBottomLeft = self.iconPlaceholder.topRight
        self.iconPlaceholder.scalingHandle:connect("pressedChanged", function (p)
            local handle = self.iconPlaceholder.scalingHandle
            if p then
                handle.ox = handle.mouseX
                handle.oy = handle.mouseY
            end
        end)
        self.iconPlaceholder.scalingHandle.snapLabel = EFrame.Label(self.iconPlaceholder.scalingHandle)
        self.iconPlaceholder.scalingHandle.snapLabel.anchorBottom = self.iconPlaceholder.scalingHandle.top
        self.iconPlaceholder.scalingHandle.snapLabel.visible = EFrame.bind(self.iconPlaceholder.scalingHandle, "pressed")
        self.iconPlaceholder.scalingHandle.snapLabel.text = "Hold Shift to snap"
        self.iconPlaceholder.scalingHandle:connect("mouseYChanged", function (y)
            local handle = self.iconPlaceholder.scalingHandle
            if handle.__pressed then
                local scale = (self.iconPlaceholder.__width + (-y + handle.oy)) / EFrame.normalize(24)
                local closest
                if IsShiftKeyDown() then
                    for k, v in pairs(timelines) do
                        if v ~= self and not v.__disabled then
                            local diff = math.abs(scale - v.iconScale)
                            if not closest or diff < math.abs(scale - closest) then
                                closest = v.iconScale
                            end
                        end
                    end
                end
                self.iconScale = math.max(closest or scale, 0.5)
            end
        end)
        self.iconPlaceholder.visible = EFrame.bind(SpellTimelinesHandler, "unlocked")
        
        self.directionButton = EFrame.Button(self)
        self.directionButton.text = EFrame.bind(function() local r = self.effectiveTimeDirection and "<<<" or ">>>" return format("Cast Flow\n%s",r,r) end)
        function self.directionButton.onClicked() self.timeDirection = not self.timeDirection end
        self.directionButton.anchorCenter = self.center
        self.directionButton.visible = EFrame.bind(SpellTimelinesHandler, "unlocked")
        self.directionButton.z = 3
    end

    
    self.visible = EFrame.bind("not self.disabled")
    self.active = EFrame.bind(function() return not self.disabled and self.target end)
end

function Timeline:getSpell(spellId, id, noCreate)
    local spell = self.spells[id] or self.currentCast and self.currentCast.action.spellId == spellId and self.currentCast
    if noCreate then return spell end
    if not spell then
        spell = tremove(recycledFrames) or TimelineSpell(self, Spell())
        if id then
            self.spells[id] = spell
        else
            self.spells[spellId] = spell
        end
        spell.parent = self
        self:connect("clear", spell, "reset")
        self.nspells = self.nspells + 1
    end
    if spell.action.castResult ~= "LOST" and (spell.action.castId ~= id or not id) then
        spell.action.castId = id
        local spellData = SpellTimelinesSettings.ClassicData[spellId]
        spell.action.texture = spellData and spellData.itemIcon or select(3, GetSpellInfo(spellId))
        spell.action.spellId = spellId
        if hasRanks and spellId > 0 then
            local rank = strmatch(GetSpellSubtext(spellId) or "", "(%d+)")
            spell.action.rank = tonumber(rank) or 0
        end
        spell.action.castStart = GetTime()
        if self.lastStarted and (GetTime() - self.lastStarted.action.castStart) * self.timeScale < EFrame.normalize(24) * self.iconScale *0.75 and self.lastRowZero and (GetTime() - self.lastRowZero.action.castStart) * self.timeScale < EFrame.normalize(24) * self.iconScale *0.75 and self.lastStarted.row < 2 then
            spell.row = self.lastStarted.row + 1
            spell.mirror.marginBottom = self.lastStarted.mirror.marginBottom + self.lastStarted.mirror.height * self.lastStarted.mirror.scale
        else
            spell.mirror.marginBottom = 0
            spell.row = 0
            self.lastRowZero = spell
        end
        self.lastStarted = spell
    end
    return spell
end

function Timeline:castStart(spellId, id)
    self.currentCast = self:getSpell(spellId, id)
    self.currentCast.action:startCast()
end

function Timeline:castFailed(spellId, id)
    local spell = self:getSpell(spellId, id, true)
    if spell then
        self.lastFail = spell
    end
    if not spell or spell.action.castId and spell.action.castId ~= id then return end
    if self.currentCast == spell then self.currentCast = nil end
    spell.action:failCast()
end

function Timeline:castInterrupted(spellId, id)
    local spell = self.lastFail and self.lastFail.action.castEnd == GetTime() and self.lastFail.action.spellId == spellId and self.lastFail or self:getSpell(spellId, id)
    if not spell then return end
    if self.currentCast == spell then self.currentCast = nil end
    spell.action:interruptCast()
end

function Timeline:castStop(spellId, id)
    local spell = self:getSpell(spellId, id).action:stopCast()
    if self.currentCast == spell then self.currentCast = nil end
end

function Timeline:currentCastLost()
    if self.currentCast then
        if not self.currentCast.action.castEnd then
            self.currentCast.action:lost()
            self.currentCast.action:stopCast()
        end
        if self.spells[self.currentCast.action.spellId] == self.currentCast then self.spells[self.currentCast.action.spellId] = nil end
        self.currentCast = nil
    end
end

function Timeline:castSucceeded(spellId, id)
    local spell = self.channeling or self:getSpell(spellId, id)
    if not self.channeling then
        self.lastSuccess = spell
    else
        spell.action.channeling = true
    end
    spell.action:successCast()
    local effect = self.effects[GetSpellInfo(spellId)]
    if effect then
        spell.action.rate = effect.rate
        spell.action.crit = effect.crit
        spell.action.rateN = effect.rateN
        effect.rate = nil
        effect.crit = 0
        spell.action.proper = self.channeling ~= spell
    end
    if not self.lastSpells[GetSpellInfo(spellId)] then
        self.lastSpells[GetSpellInfo(spellId)] = {spell.action}
    else
        self.lastSpells[GetSpellInfo(spellId)][1] = spell.action
    end
    if self.currentCast == spell then
        self.currentCast = nil
    else
        self:currentCastLost()
    end
    self.spells[spellId] = nil
end

EFrame.newClass("Spell", EFrame.Object)
Spell:attach("status")
Spell:attach("castResult")
Spell:attach("texture")
Spell:attach("rate")
Spell:attach("crit")
Spell:attach("rank")
Spell:attachSignal("castStopped")
Spell:attachSignal("castSucceeded")
Spell:attachSignal("castFailed")
Spell:attachSignal("castStarted")
Spell:attachSignal("castLost")
Spell:attachSignal("castInterrupted")
Spell.__crit = 0
Spell.__rank = 0
Spell.__status = "HIDE"

function Spell:new()
    EFrame.Object.new(self)
    self:connect("statusChanged", function (s)
        if s == "HIDE" then
            self:reset()
        end
    end)
end

function Spell:reset()
    self.channeling = false
    self.castStart = nil
    self.castEnd = nil
    self.spellId = nil
    self.proper = nil
    self.properChannel = nil
    self.crit = 0
    self.rank = 0
    self.rate = nil
    self.spellId = nil
    self.castId = nil
    self.status = "HIDE"
    self.castResult = nil
end

function Spell:startCast()
    self.castResult = "CURRENT"
    self.status = ""
    self:castStarted()
end

function Spell:failCast()
    if self.castResult == "FAILED" or self.castResult == "INTERRUPTED" then return end
    self.castResult = "FAILED"
    self.status = ""
    self:castFailed()
    self:stopCast()
end

function Spell:interruptCast()
    if self.castResult == "INTERRUPTED" then return end
    self.castResult = "INTERRUPTED"
    self.status = ""
    self:castInterrupted()
    self:stopCast()
end

function Spell:successCast()
    self.castResult = "SUCCESS"
    self.status = ""
    self:castSucceeded()
    if not self.channeling then
        self:stopCast()
    end
end

function Spell:lost()
    self.castResult = "LOST"
    self.status = ""
    self:castLost()
    self:stopCast()
end

function Spell:stopCast()
    self.castEnd = GetTime()
    self:castStopped()
end

EFrame.newClass("SpellIcon", EFrame.Item)
SpellIcon:attach("action")

rankColors = {
    [0] = {0  ,0  ,0  ,0},
    [1] = {0  ,1  ,0  ,1},
    [2] = {1  ,1  ,0  ,1},
    [3] = {1  ,0  ,0  ,1},
    [4] = {1  ,1  ,1  ,1},
}

if
    WOW_PROJECT_ID == WOW_PROJECT_CLASSIC or
    WOW_PROJECT_ID == WOW_PROJECT_BURNING_CRUSADE_CLASSIC or
    WOW_PROJECT_ID == WOW_PROJECT_WRATH_CLASSIC then
    hasRanks = true
end
hasEnemyRanks = hasRanks and WOW_PROJECT_ID ~= WOW_PROJECT_CLASSIC

function SpellIcon:new(parent, action)
    self.__action = action
    EFrame.Item.new(self, parent)
    self.width = EFrame.normalizeBind(24)
    self.height = EFrame.normalizeBind(24)
    self.tex = EFrame.Image(self)
    self.tex:setCoords(0.1,0.9,0.1,0.9)
    self.tex.margins = 2
    self.tex.anchorFill = self
    self.tex.source = EFrame.bind(function() return self.action.texture end)
    self.tex.layer = "BACKGROUND"
    
    self.border = EFrame.Image(self)
    self.border.marginTop = EFrame.bind(function() return -self.height*0.3 end)
    self.border.marginBottom = EFrame.bind(function() return -self.height*0.3 end)
    self.border.marginLeft = EFrame.bind(function() return -self.width*0.3 end)
    self.border.marginRight = EFrame.bind(function() return -self.width*0.3 end)
    self.border.anchorFill = self
    self.border.source = 'Interface\\AddOns\\SpellTimelines\\ButtonBorder.tga'
    self.border.layer = "OVERLAY"
    self.highlight = EFrame.Image(self)
    self.highlight.margins = 1
    self.highlight.anchorFill = self
    self.highlight.source = 'Interface\\AddOns\\SpellTimelines\\ButtonHighlight.tga'
    function mix(c1,c2, r)
        local a,b = r, 1 - r
        function mix2(x)
            return c1[x]*a+c2[x]*b
        end
        return {mix2(1), mix2(2), mix2(3), mix2(4)}
    end
    self.highlight.color = EFrame.bind(function() local rate = self.action.rate return rate and mix(borderColors.SUCCESS2, borderColors.FAILED2, rate) or {0,0,0,0} end)
    self.highlight.layer = "OVERLAY"
    self.highlight.blendMode = "ADD"
    self.highlight.visible = EFrame.bind(function() return self.action.status ~= "HIDE" end)
    if hasRanks then
        self.ranks = {}
        for i=1,5 do
            tinsert(self.ranks, EFrame.Image(self))
            self.ranks[i].source = "Interface\\AddOns\\SpellTimelines\\Rank"
            self.ranks[i].color = EFrame.bind(function() local rank = self.action.rank return rankColors[(rank % 5 < i and 0 or 1) + math.floor(rank/5)] end)
            self.ranks[i].height = EFrame.normalizeBind(6)
            self.ranks[i].width = EFrame.normalizeBind(8)
            self.ranks[i]:setCoords(0,1,0.25,0.75)
            self.ranks[i].layer = "OVERLAY"
            self.ranks[i].marginBottom = EFrame.normalizeBind(3)
            self.ranks[i].anchorBottom = i > 1 and self.ranks[i-1].bottom or self.bottomLeft
            self.ranks[i].z = 5
            self.ranks[i].visible = EFrame.bind(function() return self.action.parent.parent.effectiveShowRanks and self.action.rank > 0 and (self.action.parent.parent.target == "player" or hasEnemyRanks) end)
        end
        self.rankLabel = EFrame.Label(self)
        self.rankLabel.anchorCenter = self.bottomLeft
        self.rankLabel.n_text:SetFontObject("NumberFontNormal")
        self.rankLabel.text = EFrame.bind(function() return self.action.rank end)
        self.rankLabel.z = 6
        self.rankLabel.sizeMode = EFrame.Label.VerticalFit
        self.rankLabel.height = EFrame.normalizeBind(10)
        self.rankLabel.visible = EFrame.bind(function() return self.action.parent.parent.effectiveShowRanks and self.action.rank > 0 and (self.action.parent.parent.target == "player" or hasEnemyRanks) end)
    end
    self.visible = EFrame.bind(function(self) return self.action.status ~= "HIDE" end)
end

