if GetLocale() ~= "esES" and GetLocale() ~= "esMX" then return end
ClassicData = {
    ["Armadura demoníaca"] = {
        ["WARLOCK"] = 11735,
        ["Thule Corvozarpa"] = 13787,
    },
    ["Frasco de resistencia cromática"] = {
        Professions = 17638,
    },
    ["Brazales de escórpido pesados"] = {
        Professions = 19048,
    },
    ["Balas de mitril de gran impacto"] = {
        Professions = 12596,
    },
    ["Última carga"] = {
        ["WARRIOR"] = 12975,
    },
    ["Intelecto corrupto"] = {
        ["Jabalí jaspeado corrupto"] = 6818,
        ["Chamán Maderiza"] = 6818,
        ["Totémico Maderiza"] = 6818,
    },
    ["Guardia de las Sombras"] = {
        ["PRIEST"] = 19312,
    },
    ["Ofrenda de Arthas"] = {
        Professions = 11466,
    },
    ["Yelmo barbárico de hierro"] = {
        Professions = 9814,
    },
    ["Guantes de guardián"] = {
        Professions = 7156,
    },
    ["Parásito"] = {
        ["Oráculo Brazanegra"] = 8363,
    },
    ["Picadura entrópica"] = {
        ["Franklin el Amistoso"] = 23260,
    },
    ["Forma de viaje"] = {
        ["DRUID"] = 783,
    },
    ["Nube de bolas de nieve"] = {
        ["Nube de nieve de Pat"] = 26000,
    },
    ["Tótem Fuente de maná"] = {
        ["SHAMAN"] = 10497,
    },
    ["Fango"] = {
        ["Fangoso"] = 3514,
    },
    ["Cinturón Fauces de Madera pesado"] = {
        Professions = 23628,
    },
    ["Bolsa de lana"] = {
        Professions = 3757,
    },
    ["Nube nociva"] = {
        ["Babosa tóxica"] = 21070,
    },
    ["Sobrehombros de cuero terráneo"] = {
        Professions = 9147,
    },
    ["Invocar jabaguerrero domesticado"] = {
        ["Jabaguerrero domado"] = 8274,
        ["Controlador Rajacieno"] = 8274,
        ["Instructor de bestias Rajacieno"] = 8274,
    },
    ["Gran maza de hierro sólido"] = {
        Professions = 3494,
    },
    ["Aullido máculo"] = {
        ["Corrupto Nocturno"] = 3424,
        ["Gutspill"] = 3424,
    },
    ["Coraza de escamas de múrloc"] = {
        Professions = 6703,
    },
    ["Ocultar"] = {
        ["Rondador Espinaferal"] = 6920,
        ["Pícaro Odio Feroz"] = 6920,
    },
    ["Arrepentimiento"] = {
        ["PALADIN"] = 20066,
    },
    ["Toga de lino marrón"] = {
        Professions = 7623,
    },
    ["Veneno mortal III"] = {
        ["ROGUE"] = 11357,
    },
    ["Barrera de hielo"] = {
        ["MAGE"] = 13033,
    },
    ["Ultragafas ojo de gato"] = {
        Professions = 12607,
    },
    ["Yelmo de escórpido resistente"] = {
        Professions = 10570,
    },
    ["Enfurecer"] = {
        ["DRUID"] = 5229,
        ["Herod"] = 8269,
        ["Myrmidón Escarlata"] = 8269,
        ["Abad Escarlata"] = 8269,
        ["Agathelos el Furioso"] = 8269,
        ["Agam'ar enfurecido"] = 8269,
        ["Saqueador irradiado"] = 8269,
        ["Drek'Thar"] = 8269,
        ["Guerrero Gurubashi"] = 18501,
        ["Tosco Pielsombra"] = 8599,
        ["Guerrero Cabellesangre"] = 8599,
        ["Explorador Cabellesangre"] = 8599,
        ["Cazador Cabellesangre"] = 8599,
        ["Rabioso Cabellesangre"] = 8599,
        ["Médico brujo Cabellesangre"] = 8599,
        ["Rebanacabezas Cabellesangre"] = 8599,
        ["Lanzahachas Cabellesangre"] = 8599,
        ["Chamán Cabellesangre"] = 8599,
        ["Maestro de bestias Cabellesangre"] = 8599,
        ["Místico Cabellesangre"] = 8599,
        ["Carroñero Cabellesangre"] = 8599,
        ["Targor el Pavoroso"] = 8599,
        ["Almanegra Furia Lunar"] = 8599,
        ["Yeti feroz"] = 8599,
        ["Bruto de Orgrimmar"] = 8599,
        ["Déspota de la Facción Oscura"] = 8599,
        ["Pícaro Odio Feroz"] = 8599,
        ["Timador Odio Feroz"] = 8599,
        ["Jurapenas Odio Feroz"] = 8599,
        ["Traidor Odio Feroz"] = 8599,
        ["Acechasombras Odio Feroz"] = 8599,
        ["Clamainferno Odio Feroz"] = 8599,
        ["Truenagarto enfurecido"] = 8599,
        ["Guerrero marchito"] = 8599,
        ["Atracador marchito"] = 8599,
        ["Jabaguardia marchito"] = 8599,
        ["Cuerolanza marchito"] = 8599,
        ["Maledictor Pezuñamusgo"] = 8599,
        ["Oso Zarpira"] = 8599,
        ["Grisezno Zarpira"] = 8599,
        ["Destructor Zarpira"] = 8599,
        ["Lechúcico lunar enfurecido"] = 8599,
        ["Xabraxxis"] = 8599,
        ["Arikara"] = 8599,
        ["Draka"] = 8599,
        ["Duros"] = 8599,
        ["Reptador Correarrecifes iracundo"] = 8599,
        ["Desgarrador Furia Sangrienta"] = 8599,
        ["Asaltante del Hachazo"] = 8599,
        ["Élite Kor'kron"] = 8599,
        ["Alguacil Sangrehielo"] = 8599,
        ["Alguacil de Punta de la Torre"] = 8599,
        ["Alguacil del este Lobo Gélido"] = 8599,
        ["Mariscal Lobo Gélido"] = 8599,
        ["Maestro de guerra del norte de Dun Baldar"] = 8599,
        ["Maestro de guerra del sur de Dun Baldar"] = 8599,
        ["Maestro de guerra Alahielo"] = 8599,
        ["Maestro de guerra Corapernal"] = 8599,
        ["Campeón Roca Negra"] = 3019,
        ["Nigromante Roca Negra"] = 3019,
        ["Renegado Roca Negra"] = 3019,
        ["Bruto Roca Negra"] = 3019,
        ["Avanzado Roca Negra"] = 3019,
        ["Tharil'zun"] = 3019,
        ["Rastreador Roca Negra"] = 3019,
        ["Partedorsales Mentueso"] = 3019,
        ["Trogg rabioso"] = 3019,
        ["Rabioso Secacorteza"] = 3019,
        ["Bruto Sentencia"] = 3019,
        ["Explorador Roca Negra"] = 3019,
        ["Argos Roca Negra"] = 3019,
        ["Cazador Roca Negra"] = 3019,
        ["Invocador Roca Negra"] = 3019,
        ["Gladiador Roca Negra"] = 3019,
    },
    ["Hacha de batalla de cobre"] = {
        Professions = 3293,
    },
    ["Bloqueo mejorado"] = {
        ["Centurión Faucedraco"] = 3419,
        ["Kam Furiahonda"] = 3419,
        ["Lacayo de Trabalomas"] = 3419,
        ["Jíbaro Kurzen"] = 3639,
        ["Defensor de Stromgarde"] = 3639,
        ["Soldado Escarlata"] = 3639,
        ["Defensor Escarlata"] = 3639,
        ["Defensor de Refugio de la Zaga"] = 3639,
        ["Esbirro Defias"] = 3248,
        ["Capitán Vachon"] = 3248,
        ["Guardián Corvozarpa"] = 3248,
        ["Guerrero Escama Tormentosa"] = 3248,
        ["Montaraz de Dun Garok"] = 3248,
        ["Guardia de batalla Crines de Acero"] = 3248,
        ["Teniente Benedict"] = 3248,
        ["Defensor de Rajacieno"] = 3248,
        ["Soldado de Nethergarde"] = 3248,
    },
    ["Encantar pechera: maná menor"] = {
        Professions = 7443,
    },
    ["Cinturón de pellejo de raptor"] = {
        Professions = 4097,
    },
    ["Crear huevociliscopio"] = {
        ["Tinkee Vaporio"] = 16029,
    },
    ["Golpe Sagrado"] = {
        ["Montaraz de Dun Garok"] = 13953,
        ["Teniente Valeroso"] = 13953,
        ["Presertor Escarlata"] = 13953,
        ["Campeón Escarlata"] = 17143,
    },
    ["Anuncio Zombi de Zul'Farrak"] = {
        ["Héroe muerto Zul'Farrak"] = 10747,
        ["Zombi Zul'Farrak"] = 10747,
    },
    ["Piel de piedra"] = {
        ["Gólem partido"] = 5810,
        ["Behemoth de piedra"] = 5810,
    },
    ["Levantar escarabajo muerto"] = {
        ["Escarabajo no-muerto"] = 17235,
    },
    ["Encantar capa: defensa superior"] = {
        Professions = 13746,
    },
    ["Disparo penetrante"] = {
        ["Fusilero de Dun Garok"] = 6685,
        ["Capataz Defias"] = 6685,
        ["Agente Thaurissan"] = 6685,
        ["Abrecaminos Muertobosque"] = 6685,
        ["Tirador Forjatiniebla"] = 6685,
    },
    ["Leotardos de tejido mágico negros"] = {
        Professions = 12049,
    },
    ["Red"] = {
        ["Ruklar el Trampero"] = 12024,
        ["Trampero Defias"] = 12024,
        ["Redista múrloc"] = 12024,
        ["Asaltante Branquiazul"] = 12024,
        ["Redista Brumagris"] = 12024,
        ["Vaquero Kolkar"] = 12024,
        ["Truhán de Trinquete"] = 12024,
        ["Truhán de Bahía del Botín"] = 12024,
        ["Jalalanza"] = 12024,
        ["Truhán Chismetero"] = 12024,
        ["Avanzado Roca Negra"] = 6533,
        ["Tharil'zun"] = 6533,
        ["Cazador Cabellesangre"] = 6533,
        ["Vaquero Kurzen"] = 6533,
        ["Trampero Pellejomusgo"] = 6533,
        ["Asaltante Faucedraco"] = 6533,
        ["Esclavista Puñastilla"] = 6533,
        ["Asaltante Velasangre"] = 6533,
        ["Lady Miraluna"] = 6533,
        ["Jailor Marlgen"] = 6533,
        ["Jailor Borhuin"] = 6533,
        ["Rebanacabezas Secacorteza"] = 6533,
        ["Asaltante Espinadaga"] = 6533,
        ["Barak Aterrakodos"] = 6533,
        ["Vaquero Galak"] = 6533,
        ["Alcaide Rajacieno"] = 6533,
        ["Vaquero Maggram"] = 6533,
        ["Traje de seguridad de Pacificador"] = 6533,
        ["Gelihast"] = 6533,
        ["Jefe Encobre"] = 6533,
        ["Viscoso"] = 6533,
        ["Asaltante Jhash"] = 6533,
        ["Asesino Galak"] = 6533,
    },
    ["Venenos"] = {
        ["ROGUE"] = 2842,
    },
    ["Desvanecido"] = {
        ["Guardanegro Defias"] = 6408,
        ["Onin MacHammar"] = 6408,
    },
    ["Faltriquera de munición de cuero pequeña"] = {
        Professions = 9062,
    },
    ["Maleficio"] = {
        ["Suma Sacerdotisa Hai'watna"] = 18503,
        ["Bom'bay"] = 16708,
        ["Bom'bay"] = 16709,
        ["Bom'bay"] = 16707,
    },
    ["Pisotón de Mamporro"] = {
        ["Sr. Golpin"] = 6432,
    },
    ["Congelación apresurada"] = {
        ["Necrófago gélido"] = 16803,
    },
    ["Cuero medio"] = {
        Professions = 20648,
    },
    ["Pies cubiertos de musgo"] = {
        ["Trepador del Lago"] = 6870,
        ["Trepador del Lago viejo"] = 6870,
        ["Comemusgo Sombramatorral"] = 6870,
    },
    ["Sangre fría"] = {
        ["ROGUE"] = 14177,
    },
    ["Ansia de sangre presurosa"] = {
        ["Capataz Zarparrío"] = 3229,
        ["Chamán Mo'grosh"] = 3229,
        ["Gromug"] = 3229,
    },
    ["Granizo"] = {
        ["Caminaescarcha mecanizado"] = 10734,
    },
    ["Fuerza de la Naturaleza"] = {
        ["Fuerza de la Naturaleza"] = 6913,
        ["Vindicador Cenarion"] = 6913,
    },
    ["Rastrear demonios"] = {
        ["HUNTER"] = 19878,
    },
    ["Bramido amenazante"] = {
        ["Lobo de pradera"] = 5781,
        ["Lobo alfa de pradera"] = 5781,
    },
    ["Detonación"] = {
        ["Mina terrestre Hierro Negro"] = 4043,
        ["Arcanista Doan"] = 9435,
    },
    ["Recoger cesta de tótem de los Maderiza"] = {
        ["Jefe Murgut"] = 20818,
    },
    ["Explosión de hielo"] = {
        ["Caminaescarcha mecanizado"] = 11264,
    },
    ["El mortero: recargado"] = {
        Professions = 13240,
    },
    ["Maza de cobre"] = {
        Professions = 2737,
    },
    ["Sobrehombros de seda carmesíes"] = {
        Professions = 8793,
    },
    ["Quitar arma"] = {
        ["Despojador Cueva Honda"] = 10851,
    },
    ["Girotiro de mitril"] = {
        Professions = 12621,
    },
    ["Transmutar: agua a no-muerte"] = {
        Professions = 17564,
    },
    ["Llamas infernales"] = {
        ["WARLOCK"] = 11684,
    },
    ["Armadura de seda verde"] = {
        Professions = 8784,
    },
    ["Serenidad"] = {
        Professions = 16983,
    },
    ["Sufrimiento"] = {
        Pet = 17752,
    },
    ["Relámpago rebotador"] = {
        ["Tormento Maraudine"] = 9654,
    },
    ["Aprovechar debilidad"] = {
        ["Pícaro Malarraíz"] = 8355,
        ["Correoscuro Nocturno"] = 6595,
        ["Asaltatumbas"] = 6595,
        ["Saqueador Defias"] = 6595,
        ["Patriarca Acechalunas"] = 6595,
        ["Malapluma Alabruja"] = 6595,
        ["Emboscador Alabruja"] = 6595,
        ["Pícaro Xavian"] = 6595,
        ["Pícaro Putredor"] = 6595,
        ["Malapluma estridente"] = 6595,
        ["Acechasombras Odio Feroz"] = 6595,
    },
    ["Tubo de torio"] = {
        Professions = 19795,
    },
    ["Inteligencia de fortuna de las Sombras de Sayge"] = {
        ["Sayge"] = 23766,
    },
    ["Furia recta"] = {
        ["PALADIN"] = 25780,
    },
    ["Poción de maná menor"] = {
        Professions = 2331,
    },
    ["Veneno instantáneo V"] = {
        ["ROGUE"] = 11342,
    },
    ["Quemadura de las Sombras"] = {
        ["WARLOCK"] = 18871,
    },
    ["Toga del Vacío"] = {
        Professions = 18458,
    },
    ["Botas de cuero refinado"] = {
        Professions = 2158,
    },
    ["Barrido profundo"] = {
        ["Murciovil"] = 7145,
    },
    ["Madeja de paño de lana"] = {
        Professions = 2964,
    },
    ["Red Naraxis"] = {
        ["Naraxis"] = 3542,
    },
    ["Sed de sangre"] = {
        ["WARRIOR"] = 23894,
    },
    ["Desvanecerse"] = {
        ["PRIEST"] = 10942,
        ["Lady Sylvanas Brisaveloz"] = 20672,
    },
    ["Encantar escudo: aguante"] = {
        Professions = 13817,
    },
    ["Encoger"] = {
        ["Reductor de cabezas Kurzen"] = 7289,
        ["Zalazane"] = 7289,
        ["Chamán Oscuro Crepuscular"] = 7289,
    },
    ["Estoque llameante"] = {
        Professions = 16978,
    },
    ["Pluma ligera"] = {
        ["PRIEST"] = 17056,
    },
    ["Manos cubiertas de musgo"] = {
        ["Vigilante del Lago"] = 6866,
        ["Vagador del Lago viejo"] = 6866,
        ["Comemusgo Sombramatorral"] = 6866,
    },
    ["Fundir armadura"] = {
        ["Guardia llamarada"] = 19631,
    },
    ["Humus espeluznante"] = {
        ["Buscaqua Crines de Acero"] = 6278,
        ["Habitabosta Ancalodo"] = 6278,
    },
    ["Gafas de poder con hechizos Xtremo Plus"] = {
        Professions = 19794,
    },
    ["Mascota Machacacráneos"] = {
        ["Pantera Machacacráneos"] = 3621,
        ["Cazador Machacacráneos"] = 3621,
        ["Maestro de bestias Machacacráneos"] = 3621,
    },
    ["Choque Sagrado"] = {
        ["PALADIN"] = 20930,
    },
    ["Detectar invisibilidad inferior"] = {
        ["WARLOCK"] = 132,
    },
    ["Aceleración nitrosa"] = {
        ["Tonque de vapor"] = 27746,
    },
    ["Abalanzarse"] = {
        ["DRUID"] = 9827,
    },
    ["Oveja explosiva"] = {
        Professions = 3955,
    },
    ["Veneno hiriente IV"] = {
        ["ROGUE"] = 13230,
    },
    ["Tropiezo"] = {
        ["Refugiado enfermo"] = 101,
        ["Sastre de Trabalomas"] = 101,
    },
    ["Botas de tela vil"] = {
        Professions = 18437,
    },
    ["Cohete, AMARILLO"] = {
        ["Fuegos artificiales de Pat AMARILLO"] = 26349,
    },
    ["Crear piedra de fuego (superior)"] = {
        ["WARLOCK"] = 17952,
    },
    ["Llamas del Vuelo Negro"] = {
        ["Brasaliza"] = 16054,
    },
    ["Ácido de Hakkar"] = {
        ["Alevín de Hakkar"] = 12280,
    },
    ["Invocar esclavo mental de Kurzen"] = {
        ["Cazamentes Kurzen"] = 8813,
        ["Mogh el Eterno"] = 8813,
    },
    ["Aceite de zahorí menor"] = {
        Professions = 25124,
    },
    ["Malla Hierro Negro"] = {
        Professions = 15293,
    },
    ["Sabiduría de Agamaggan"] = {
        ["Quebrantadientes"] = 7764,
    },
    ["Crear Bastón de mando"] = {
        ["Arrasa"] = 15539,
    },
    ["Bendición de poderío superior"] = {
        ["PALADIN"] = 25916,
    },
    ["Crear aura de Zul"] = {
        ["Yeh'kinya"] = 24186,
    },
    ["Invocar chinche engañosa"] = {
        ["Chinche engañosa"] = 10858,
        ["Tecnobot"] = 10858,
        ["Chinche engañosa"] = 10858,
    },
    ["Quemadura en cadena"] = {
        ["Horror irradiado"] = 8211,
    },
    ["Invocar espíritu de jabalí"] = {
        ["Espíritu de jabalí"] = 8286,
        ["Aggem Malaespina"] = 8286,
    },
    ["Bomba de bronce grande"] = {
        Professions = 3950,
    },
    ["Veneno"] = {
        ["Araña del bosque"] = 11918,
        ["Araña de mina"] = 11918,
        ["Morgaine el Astuto"] = 11918,
        ["Madre Colmillo"] = 11918,
        ["Matriarca nocturácnida"] = 11918,
        ["Githyiss la Vil"] = 11918,
        ["Rondador Tejemadera"] = 11918,
        ["Venolmillo Tejemadera"] = 11918,
        ["Virasedal Tejemadera"] = 11918,
        ["Araña Tejemadera gigante"] = 11918,
        ["Duende vil"] = 11918,
        ["Escórpido castañeante"] = 11918,
        ["Escórpido corrupto"] = 11918,
        ["Sarkoth"] = 11918,
        ["Cazamareas múrloc"] = 744,
        ["Telarácnida venenosa"] = 744,
        ["Gnoll Pielsombra rabioso"] = 744,
        ["Tarantula"] = 744,
        ["Tarántula mayor"] = 744,
        ["Telarácnida pigmeovenenosa"] = 744,
        ["Ermitaño Verde"] = 744,
        ["Asesino Pielsombra"] = 744,
        ["Carroñero Cabellesangre"] = 744,
        ["Girapenas"] = 744,
        ["Batidor Escama de Sal"] = 744,
        ["Filonocturno Defias"] = 744,
        ["Corrupto Nocturno"] = 744,
        ["Viuda negra reproductora"] = 744,
        ["Rohh el Silencioso"] = 744,
        ["Batidor Branquiazul"] = 744,
        ["Correpantanos Faucedraco"] = 744,
        ["Señor del cubil"] = 744,
        ["Rondador del risco"] = 744,
        ["Rondadora maderera"] = 744,
        ["Rondador del bosque"] = 744,
        ["Nocturácnida feroz"] = 744,
        ["Acechamusgo"] = 744,
        ["Trepanieblas"] = 744,
        ["Cazamareas Brumagris"] = 744,
        ["Ladrón del Sindicato"] = 744,
        ["Asesino del Sindicato"] = 744,
        ["Pícaro del Sindicato"] = 744,
        ["Asesino oscuro"] = 744,
        ["Trepador de las llanuras"] = 744,
        ["Trepador de las llanuras gigante"] = 744,
        ["Suministrador Hierro Negro"] = 744,
        ["Buscaqua Erizapúas"] = 744,
        ["Tramposo Almaumbría"] = 744,
        ["Infiltrado Renegado"] = 744,
        ["Rondador Espinaferal"] = 744,
        ["Trepador Musgondo"] = 744,
        ["Alargullo joven"] = 744,
        ["Dracoleón Alargullo"] = 744,
        ["Cazacielos Alargullo"] = 744,
        ["Patriarca Alargullo"] = 744,
        ["Dracoleón Nido Alto"] = 744,
        ["Consorte Nido Alto"] = 744,
        ["Patriarca Nido Alto"] = 744,
        ["Araña Niebla Negra"] = 744,
        ["Rondador Colmilloumbrío"] = 744,
        ["Araña Colmilloumbrío"] = 744,
        ["Araña Colmilloumbrío gigante"] = 744,
        ["Batidor Ancaniebla"] = 744,
        ["Timador Odio Feroz"] = 744,
        ["Amigo de Friend"] = 744,
        ["Agregador descarriado"] = 744,
        ["Avispa Centipaar"] = 744,
        ["Araña Cristalderma"] = 744,
        ["Gutspill"] = 744,
        ["Gran vándalo marino"] = 744,
        ["Mefisto de las tumbas"] = 744,
        ["Asesino Galak"] = 744,
        ["Arikara"] = 744,
        ["Bazzalan"] = 744,
        ["Acechador de piedra"] = 744,
    },
    ["Escudo de relámpagos"] = {
        ["SHAMAN"] = 10432,
        ["Oráculo Arkkoran"] = 12550,
        ["Tormenta viviente"] = 12550,
        ["Lorgus Jett"] = 12550,
        ["Chamán Lobo Gélido"] = 12550,
        ["Chamán Cabellesangre"] = 8788,
        ["Tormento Maggram"] = 8788,
        ["Tormento Maraudine"] = 8788,
        ["Sirocoso"] = 19514,
        ["Chamán Muertobosque"] = 13585,
    },
    ["Leotardos de escórpido resistentes"] = {
        Professions = 10568,
    },
    ["Cuero basto"] = {
        Professions = 22331,
    },
    ["Aspecto del mono"] = {
        ["HUNTER"] = 13163,
    },
    ["Cilindro de mitril taraceado"] = {
        Professions = 11454,
    },
    ["Forma del Acechalunas"] = {
        ["Terenthis"] = 6236,
    },
    ["Inferno"] = {
        ["WARLOCK"] = 1122,
    },
    ["Choque de llamas"] = {
        ["SHAMAN"] = 29228,
        ["Garneg Hullacráneo"] = 15039,
        ["Primalista Thurloga"] = 15616,
    },
    ["Espada corta de hierro endurecido"] = {
        Professions = 3492,
    },
    ["Llave de tuerca arcoluz"] = {
        Professions = 7430,
    },
    ["Invocar sirviente ardiente"] = {
        ["Sirviente ardiente"] = 10870,
    },
    ["Lanzar tierra"] = {
        ["Quijaforte de oasis"] = 6530,
    },
    ["Golpe en el suelo"] = {
        ["Grol el Destructor"] = 12734,
        ["Obsidion"] = 12734,
        ["Gor'tesh"] = 12734,
    },
    ["Botas de paño cenizo"] = {
        Professions = 12088,
    },
    ["Cinturón de escamas de múrloc"] = {
        Professions = 6702,
    },
    ["Chaqueta de esmoquin"] = {
        Professions = 12093,
    },
    ["Poción de Rejuvenecimiento menor"] = {
        Professions = 2332,
    },
    ["Elixir de detección de demonios"] = {
        Professions = 11478,
    },
    ["Fusión de las Sombras"] = {
        Racial = 20580,
    },
    ["Guerrera de cría negra"] = {
        Professions = 24940,
    },
    ["Empollar huevos"] = {
        ["Suma Sacerdotisa Mar'li"] = 24083,
    },
    ["Desactivar trampa"] = {
        ["ROGUE"] = 1842,
    },
    ["Crear piedra de hechizo"] = {
        ["WARLOCK"] = 2362,
    },
    ["Puntos de honor +378"] = {
        ["Belisario de la Horda"] = 24964,
        ["Brigadier general de la Alianza"] = 24964,
    },
    ["Salva de bolas de Fuego"] = {
        ["Guardia de Fuego Crepuscular"] = 15243,
        ["Grifo nidal"] = 15285,
        ["Jinete de guerra"] = 15285,
    },
    ["Destello de cristal"] = {
        ["Basilisco Quijahierro"] = 5106,
        ["Panzascama"] = 5106,
        ["Basilisco Cristaldermo"] = 5106,
        ["Vitropiel Rocarroja"] = 5106,
    },
    ["Armadura de torio"] = {
        Professions = 16642,
    },
    ["Proyector redomático gnómico"] = {
        Professions = 12902,
    },
    ["Gafas de maestro ingeniero"] = {
        Professions = 19825,
    },
    ["Guanteletes de escamas doradas"] = {
        Professions = 11643,
    },
    ["Puñalada"] = {
        ["ROGUE"] = 25300,
        ["Thuros Dedos Ligeros"] = 7159,
        ["Acechacaminos Defias"] = 7159,
        ["Morgan el Coleccionista"] = 7159,
        ["Asaltante de caminos Defias"] = 7159,
        ["Despojador Defias"] = 7159,
        ["Carroñero Cabellesangre"] = 7159,
        ["Rondador múrloc"] = 7159,
        ["Emboscador Comepiedras"] = 7159,
        ["Desollador Peloescarcha"] = 7159,
        ["Habitabosta Anca Vil"] = 7159,
        ["Cautivo Defias"] = 7159,
        ["Asaltante de caminos del Sindicato"] = 7159,
        ["Ladrón del Sindicato"] = 7159,
        ["Espía del Sindicato"] = 7159,
        ["Intruso Renegado"] = 7159,
        ["Analista de Nethergarde"] = 7159,
        ["Acechador Rajacieno"] = 7159,
        ["Guardaespaldas Defias"] = 7159,
        ["Emboscador Veloneve"] = 7159,
    },
    ["Toga de lana gris"] = {
        Professions = 2403,
    },
    ["Invocar fragmentizo Theradrim"] = {
        ["Theradrim Pedazal"] = 21057,
        ["Guardián Theradrim"] = 21057,
    },
    ["Fuerza de fortuna de las Sombras de Sayge"] = {
        ["Sayge"] = 23735,
    },
    ["Loriga de escamas doradas"] = {
        Professions = 3511,
    },
    ["Guantes sombradermos"] = {
        Professions = 22711,
    },
    ["Tótem contraveneno"] = {
        ["SHAMAN"] = 8166,
    },
    ["Espinas"] = {
        ["DRUID"] = 9910,
    },
    ["Agilidad corrupta"] = {
        ["Abrecaminos Maderiza"] = 6817,
        ["Pataroble"] = 6817,
    },
    ["Veneno débil"] = {
        ["Nocturácnida joven"] = 6751,
        ["Araña nocturácnida"] = 6751,
        ["Araña Tejemadera"] = 6751,
        ["Trabajador escórpido"] = 6751,
    },
    ["Encantar brazales: aguante superior"] = {
        Professions = 13945,
    },
    ["Botas de tejido de sombra"] = {
        Professions = 12082,
    },
    ["Brazales de lino verdes"] = {
        Professions = 3841,
    },
    ["Justicia de Alto Señor"] = {
        ["Alto Señor Bolvar Fordragon"] = 20683,
    },
    ["Espuelas de mitril"] = {
        Professions = 9964,
    },
    ["Tranquilidad"] = {
        ["DRUID"] = 9863,
    },
    ["Dinamita para tontos II"] = {
        Professions = 23069,
    },
    ["Disparo de distracción"] = {
        ["HUNTER"] = 15632,
    },
    ["Carcaj de cuero ligero"] = {
        Professions = 9060,
    },
    ["Tremor terrenal"] = {
        ["Furia de piedra"] = 6524,
        ["Exiliado estruendoso"] = 6524,
        ["Fozruk"] = 6524,
        ["Estruendor"] = 6524,
        ["Rocoso tronador"] = 6524,
        ["Terremoto de Rajacieno"] = 6524,
        ["Administrador de piedra"] = 6524,
        ["Atal'alarion"] = 6524,
        ["Avalanchion"] = 6524,
        ["Gri'lek"] = 6524,
        ["Duque de las Esquirlas"] = 6524,
        ["Señor del Magma Bokk"] = 6524,
    },
    ["Veneno instantáneo III"] = {
        ["ROGUE"] = 8691,
    },
    ["Transmutar: agua a aire"] = {
        Professions = 17562,
    },
    ["Llamar a mascota"] = {
        ["HUNTER"] = 883,
    },
    ["Sobrehombros de bronce bañados en plata"] = {
        Professions = 3330,
    },
    ["Maza de bronce"] = {
        Professions = 2740,
    },
    ["Capa de amparo"] = {
        Professions = 22870,
    },
    ["Dinamita sólida"] = {
        Professions = 12586,
    },
    ["Deflagración Arcana"] = {
        ["MAGE"] = 10202,
        ["Capitán Balinda Corapernal"] = 13745,
        ["Arcanista Doan"] = 9433,
    },
    ["Desbloqueo de Ashcrombe"] = {
        ["Hechicero Ashcrombe"] = 6421,
    },
    ["Encantar escudo: aguante superior"] = {
        Professions = 20017,
    },
    ["Dolencia dragante"] = {
        ["Gusano de arrastre"] = 14535,
    },
    ["Cuero grueso"] = {
        Professions = 20650,
    },
    ["Resurrección de Yelestrón"] = {
        ["Comandante Yelestrón"] = 3488,
        ["Comandante Yelestrón"] = 3488,
    },
    ["Actitud defensiva"] = {
        ["WARRIOR"] = 71,
        ["Garrick Piesuaves"] = 7164,
        ["Gath'Ilzogg"] = 7164,
        ["Guerrero Pielsombra"] = 7164,
        ["Centurión Faucedraco"] = 7164,
        ["Tunelador Hierro Negro"] = 7164,
        ["Cavador Rompecantos"] = 7164,
        ["Cavador Ratatúnel"] = 7164,
        ["Soldado de Caballería Escarlata"] = 7164,
        ["Capitán Vachon"] = 7164,
        ["Kam Furiahonda"] = 7164,
        ["Argos Piroleño"] = 7164,
        ["Lacayo de Trabalomas"] = 7164,
        ["Minero de Trabalomas"] = 7164,
        ["Acechacostas Espinadaga"] = 7164,
        ["Cavador Mostacho Seco"] = 7164,
        ["Defensor de Stromgarde"] = 7164,
        ["Tunelador Forjatiniebla"] = 7164,
        ["Teniente Benedict"] = 7164,
        ["Excavador de Bael'dun"] = 7164,
        ["Marino de Theramore"] = 7164,
        ["Guerrero Gapo Salino"] = 7164,
        ["Custodio Escarlata"] = 7164,
        ["Defensor Escarlata"] = 7164,
        ["Defensor de Rajacieno"] = 7164,
        ["Gladiador Roca Negra"] = 7164,
        ["Marcel Dabyrie"] = 7164,
        ["Campeón Jabaguardia"] = 7164,
        ["Sátiro Malarraíz"] = 7164,
        ["Múrloc Cegaluz"] = 7164,
        ["Cavador Forjatiniebla"] = 7164,
        ["Guardia Eduardo"] = 7164,
        ["Guardia Jarad"] = 7164,
        ["Guardia Kahil"] = 7164,
        ["Guardia Narrisha"] = 7164,
        ["Guardia Tark"] = 7164,
        ["Khan Jehn"] = 7164,
        ["Comandante de vigilancia Zalafil"] = 7164,
        ["Vejrek"] = 7164,
        ["Hurgador Cueva Honda"] = 7164,
        ["Soldado de Dun Garok"] = 7164,
        ["Defensor de Refugio de la Zaga"] = 7164,
    },
    ["Venganza Tuercepinos"] = {
        ["Augurio Tuercepinos"] = 5628,
        ["Abrecaminos Tuercepinos"] = 5628,
        ["Vengador Tuercepinos"] = 5628,
        ["Totémico Tuercepinos"] = 5628,
    },
    ["Amago"] = {
        ["ROGUE"] = 25302,
    },
    ["Fuego interno"] = {
        ["PRIEST"] = 10952,
    },
    ["Camisa de lana gris"] = {
        Professions = 2406,
    },
    ["Elixir de los sabios"] = {
        Professions = 17555,
    },
    ["Veneno Sangrepétalo"] = {
        ["Latigador pétalo de sangre"] = 14110,
        ["Despellejador pétalo de sangre"] = 14110,
        ["Trillador pétalo de sangre"] = 14110,
        ["Trampero pétalo de sangre"] = 14110,
    },
    ["Veneno virulento"] = {
        ["Atracador de las tumbas"] = 12251,
    },
    ["Caos cromático"] = {
        ["Lord Víctor Nefarius"] = 16337,
    },
    ["Activador inestable"] = {
        Professions = 12591,
    },
    ["Botas de torio"] = {
        Professions = 16652,
    },
    ["Flema de tósigo"] = {
        ["Venfresco Fugalante"] = 16552,
        ["Hidra Brumazote"] = 6917,
        ["Escupeveneno Espinaferal"] = 6917,
    },
    ["Veneno hiriente II"] = {
        ["ROGUE"] = 13228,
    },
    ["Invocar ráfaga de lava I"] = {
        ["Rata del Núcleo"] = 21907,
    },
    ["Camisa de lino blanca"] = {
        Professions = 2393,
    },
    ["Pantalones de paño rúnico"] = {
        Professions = 18438,
    },
    ["Deseo de legitimación"] = {
        ["Señor de la Guerra Cumbrerroca"] = 16171,
    },
    ["Arnés barbárico"] = {
        Professions = 6661,
    },
    ["Poderío de savia"] = {
        ["Bestia de savia"] = 7997,
    },
    ["Invocar ráfaga de lava D"] = {
        ["Rata del Núcleo"] = 21902,
    },
    ["Maleficio de Corvozarpa"] = {
        ["Thule Corvozarpa"] = 7655,
        ["Thule Corvozarpa"] = 7656,
        ["Thule Corvozarpa"] = 7657,
    },
    ["Voluntad vacilante"] = {
        ["Lupo lóbrego"] = 7127,
    },
    ["Espada corta de cobre"] = {
        Professions = 2739,
    },
    ["Vestido de fiesta rojo"] = {
        Professions = 26403,
    },
    ["Acompañante Velasangre"] = {
        ["Acompañante Defias"] = 5172,
        ["Pirata Defias"] = 5172,
    },
    ["Piedra de rotura"] = {
        ["Esclavo elemental"] = 3671,
    },
    ["Yelmo de placas de acero"] = {
        Professions = 9935,
    },
    ["Acechar"] = {
        ["DRUID"] = 9913,
        Pet = 24453,
    },
    ["Maldición de Tuten'kash"] = {
        ["Tuten'kash"] = 12255,
    },
    ["Mordedura feroz"] = {
        ["DRUID"] = 31018,
    },
    ["Orden de las Sombras"] = {
        ["Lord Víctor Nefarius"] = 22667,
    },
    ["Veneno hiriente"] = {
        ["ROGUE"] = 13220,
    },
    ["Toque divino de Vaelastrasz"] = {
        ["Vaelastrasz el Rojo"] = 16332,
    },
    ["Veneno mortal"] = {
        ["ROGUE"] = 2835,
        ["Rondador nebovenenoso"] = 3583,
        ["Mazzranache"] = 3583,
        ["Serpiente nubosa venenosa"] = 3583,
        ["Ermitaña Niebla Negra"] = 3583,
        ["Fardel Dabyrie"] = 3583,
    },
    ["Tótem de resistencia a la Escarcha"] = {
        ["SHAMAN"] = 10479,
    },
    ["Piroexplosión"] = {
        ["MAGE"] = 18809,
    },
    ["Sobrehombros de escamas de mitril"] = {
        Professions = 9966,
    },
    ["Escalofrío hediondo"] = {
        ["Elemental de agua podrida"] = 6873,
        ["Tideress"] = 6873,
    },
    ["Rastrear gigantes"] = {
        ["HUNTER"] = 19882,
    },
    ["Piedra de pulir pesada"] = {
        Professions = 3337,
    },
    ["Camisa festiva verde"] = {
        Professions = 21945,
    },
    ["Casco de albañil goblin"] = {
        Professions = 12718,
    },
    ["Tótem Fuerza de la tierra"] = {
        ["SHAMAN"] = 25361,
    },
    ["Fuegos artificiales rojos"] = {
        Professions = 23066,
        ["Aparato Gizmotrónico de Wrenix"] = 6668,
    },
    ["Cambio de fase"] = {
        Pet = 4511,
    },
    ["Pantalones de embozo de tormenta"] = {
        Professions = 19067,
    },
    ["Aliento de Escarcha"] = {
        ["Edan el Aullador"] = 3129,
        ["Wendigo joven"] = 3131,
        ["Wendigo"] = 3131,
        ["Yeti montés"] = 3131,
        ["Yeti gigante"] = 3131,
    },
    ["Llamada de la tumba"] = {
        ["Excavador no-muerto"] = 5137,
        ["Azshir el Insomne"] = 5137,
    },
    ["Transmutar: mitril a veraplata"] = {
        Professions = 11480,
    },
    ["Invocar lacayo resucitado"] = {
        ["Lacayo resucitado"] = 17618,
        ["Invocador Oscuro de Scholomance"] = 17618,
    },
    ["Guantes de meditación"] = {
        Professions = 3852,
    },
    ["Manos de oscuridad"] = {
        Professions = 8780,
    },
    ["Agua mágica"] = {
        ["MAGE"] = 5350,
    },
    ["Traca de fuegos artificiales azules"] = {
        ["Explosión de artificios de Pat (AZUL)"] = 26304,
        ["Heraldo del Festival Lunar"] = 26304,
        ["Emisario del Festival Lunar"] = 26304,
    },
    ["Espada de mitril maligna"] = {
        Professions = 9997,
    },
    ["Desgarrar"] = {
        ["WARRIOR"] = 11574,
        ["Acechamaná Hederine"] = 13738,
        ["Myrmidón Colafuria"] = 11977,
        ["Guerrero Filozante"] = 11977,
        ["Soldado Hendel"] = 11977,
        ["Arnak Tótem Siniestro"] = 11977,
        ["Legionario Lobo Gélido"] = 11977,
        ["Torek"] = 11977,
        ["Guerrero Ala de Plata"] = 11977,
        ["Sargento Yazra Gruñidosangriento"] = 11977,
        ["Guardia serpiente Strashaz"] = 16509,
        ["Invasor Minafría"] = 16509,
        ["Comandante del aire Ichman"] = 16509,
        ["Comandante del aire Slidore"] = 16509,
        ["Mandisangre"] = 13445,
        ["Desgarrador Furia Sangrienta"] = 13445,
        ["Shadumbra"] = 13445,
        ["Nicroyermo"] = 13443,
        ["Batidor Escama de Sal"] = 13443,
        ["Tigre de Tuercespina mayor"] = 13443,
        ["Masadura Nicroyermo"] = 13443,
        ["Batidor Ancaniebla"] = 13443,
        ["Tenazario Arkkoran"] = 13443,
        ["Cazador Ravasaur"] = 13443,
        ["Cazador Cortezaférrea"] = 13443,
        ["Lechúcico lunar enfurecido"] = 13443,
        ["Bayne"] = 13443,
        ["Lobo Gélido"] = 13443,
        ["Carnero de Alterac"] = 13443,
        ["Tenazario Golpeseco"] = 13443,
        ["Machacador espirálico"] = 13443,
        ["Ursangous"] = 13443,
    },
    ["Espíritu ancestral"] = {
        ["SHAMAN"] = 20777,
    },
    ["Ordeño maligno"] = {
        ["John Andrajoso"] = 16472,
    },
    ["Botas prestas"] = {
        Professions = 9208,
    },
    ["Ritual de invocación"] = {
        ["WARLOCK"] = 698,
    },
    ["Pisotón de kodo"] = {
        ["Kodo de los Baldíos Perdidos"] = 6266,
        ["Kodo de los Baldíos"] = 6266,
    },
    ["Contestación"] = {
        ["ROGUE"] = 14251,
    },
    ["Patada mordedora"] = {
        ["Bandido Defias"] = 8646,
        ["Trabajador de embarcadero Defias"] = 8646,
        ["Avanzado de Vallefresno"] = 8646,
    },
    ["Poción de invisibilidad"] = {
        Professions = 11464,
    },
    ["Pantalones de tejido de sombra"] = {
        Professions = 12052,
    },
    ["Hedor pútrido"] = {
        ["Putridus"] = 12946,
        ["Fauzpeste el Putrefacto"] = 12946,
        ["Señor de la caldera Iralma"] = 12946,
    },
    ["Microajustador giromático"] = {
        Professions = 12590,
    },
    ["Bomba de fragmentación de mitril"] = {
        Professions = 12603,
    },
    ["Nova de Escarcha"] = {
        ["MAGE"] = 10230,
        ["Cazamareas múrloc"] = 11831,
        ["Eliza"] = 11831,
        ["Balgaras el Hediondo"] = 11831,
        ["Llamamareas Anca Vil"] = 11831,
        ["Barón Vardus"] = 11831,
        ["Magus Puño de Roca"] = 11831,
        ["Kuz"] = 11831,
        ["Hechicera Colafuria"] = 11831,
        ["Elemental de agua podrida"] = 11831,
        ["Sirena Látigo de Ira"] = 11831,
        ["Tideress"] = 11831,
        ["Cazamareas Anca Vil"] = 12748,
        ["Buscaqua Erizapúas"] = 12748,
        ["Marea vengativa"] = 14907,
        ["Hechicera Strashaz"] = 15532,
        ["Barón Aquanis"] = 15531,
    },
    ["Marca de lo Salvaje"] = {
        ["DRUID"] = 9885,
    },
    ["Sobrehombros de embozo de tormenta"] = {
        Professions = 19090,
    },
    ["Bramido"] = {
        ["DRUID"] = 6795,
        ["HUNTER"] = 14927,
        Pet = 14921,
    },
    ["Disposición"] = {
        ["HUNTER"] = 23989,
    },
    ["Veneno necrótico"] = {
        ["Arañita de Maexxna"] = 28776,
    },
    ["Géiser monumental"] = {
        ["Géiser masivo"] = 22421,
    },
    ["Ira cruel"] = {
        ["Salvaje Putrepellejo"] = 3258,
        ["Filoja"] = 3258,
    },
    ["Pellejo pesado curtido"] = {
        Professions = 3818,
    },
    ["Desuello"] = {
        Professions = 8617,
        Professions = 8618,
        Professions = 10768,
    },
    ["Crecer"] = {
        ["Bom'bay"] = 16711,
    },
    ["Yelmo de fuego"] = {
        Professions = 10632,
    },
    ["Tubo de mitril"] = {
        Professions = 12589,
    },
    ["Encantar arma: poderío del invierno"] = {
        Professions = 21931,
    },
    ["Disparo presuroso"] = {
        ["Furtivo Crines Pálidos"] = 1516,
    },
    ["Encantar botas: velocidad menor"] = {
        Professions = 13890,
    },
    ["Encantar botas: agilidad menor"] = {
        Professions = 7867,
    },
    ["Poción de sanación inferior"] = {
        Professions = 2337,
    },
    ["Cresa Goo"] = {
        ["Devorador carroñero"] = 17197,
    },
    ["Mira de precisión"] = {
        Professions = 3979,
    },
    ["Sobrehombros barbáricos de hierro"] = {
        Professions = 9811,
    },
    ["Fundir estaño"] = {
        Professions = 3304,
    },
    ["Encantar guantes: pesca"] = {
        Professions = 13620,
    },
    ["Géiser"] = {
        ["Jinete de olas Crestafuria"] = 10987,
        ["Rocío marino"] = 10987,
        ["Princesa Tempestria"] = 10987,
    },
    ["Botas de anillas de cobre"] = {
        Professions = 3319,
    },
    ["Sombra penetrante"] = {
        ["Nigromante Thuzadin"] = 16429,
    },
    ["Pantalones de cuero basto"] = {
        Professions = 9064,
    },
    ["Tótem Lengua de Fuego"] = {
        ["SHAMAN"] = 16387,
    },
    ["Invocar destrero"] = {
        ["PALADIN"] = 23214,
    },
    ["Actitud rabiosa"] = {
        ["WARRIOR"] = 2458,
        ["Znort"] = 7366,
        ["Maleante Magram"] = 7366,
        ["Maleante Gelkis"] = 7366,
        ["Atracador Cueva Honda"] = 7366,
        ["Campeón caído"] = 7366,
    },
    ["Botas de lino"] = {
        Professions = 2386,
    },
    ["Cinturón de seda azur"] = {
        Professions = 8766,
    },
    ["Hemorragia"] = {
        ["ROGUE"] = 17348,
    },
    ["Aliento inagotable"] = {
        ["WARLOCK"] = 5697,
    },
    ["Guantes de cría roja"] = {
        Professions = 9072,
    },
    ["Traca de cohetes azules grandes"] = {
        Professions = 26426,
    },
    ["Escopeta dragón goblin"] = {
        Professions = 12908,
    },
    ["Cohete, BLANCO"] = {
        ["Fuegos artificiales de Pat BLANCO"] = 26348,
    },
    ["Descarga de Fuego"] = {
        Pet = 11763,
    },
    ["Invocar tótem de hielo"] = {
        ["Tótem de hielo"] = 18975,
        ["Chamán Zarpa Salvaje"] = 18975,
    },
    ["Zarpa inmovilizadora"] = {
        ["Quebrador Makrura"] = 5424,
        ["Viejo Makrura"] = 5424,
    },
    ["Veneno de aturdimiento mental"] = {
        ["ROGUE"] = 5763,
    },
    ["Maldición de agonía"] = {
        ["WARLOCK"] = 11713,
        ["Fiel abrasador"] = 18266,
        ["Balizar el Agraviado"] = 14868,
    },
    ["Invocar estruendor Gelkis"] = {
        ["Estruendor Gelkis"] = 9653,
        ["Clamor de Tierra Gelkis"] = 9653,
    },
    ["Pellejo de musgo"] = {
        ["Gnoll Pellejomusgo"] = 3288,
        ["Bastardo Pellejomusgo"] = 3288,
        ["Tejebruma Pellejomusgo"] = 3288,
        ["Correcubil Pellejomusgo"] = 3288,
        ["Trampero Pellejomusgo"] = 3288,
        ["Místico Pellejomusgo"] = 3288,
        ["Alfa Pellejomusgo"] = 3288,
    },
    ["Invocar manáfago"] = {
        ["WARLOCK"] = 691,
    },
    ["Martillo del Cruzado"] = {
        ["Gran Cruzado Dathrohan"] = 17286,
    },
    ["Activador de Enfurecer"] = {
        ["Jurafuego"] = 19515,
    },
    ["Radiación"] = {
        ["Invasor irradiado"] = 9798,
        ["Saqueador irradiado"] = 9798,
    },
    ["Embate de Mamporro"] = {
        ["Sr. Golpin"] = 6435,
    },
    ["Botas de placas imperiales"] = {
        Professions = 16657,
    },
    ["Llave esqueleto dorada"] = {
        Professions = 19667,
    },
    ["Camisa de espadachín blanca"] = {
        Professions = 8483,
    },
    ["Encantar arma 2M: intelecto inferior"] = {
        Professions = 7793,
    },
    ["Amplificador de mundo"] = {
        Professions = 23129,
    },
    ["Aura de llamas"] = {
        ["Capitán Garramortal"] = 22436,
    },
    ["Dolor implacable"] = {
        ["Humar el Señor Orgulloso"] = 3247,
    },
    ["Sobrehombros de tejido mágico rojos"] = {
        Professions = 12078,
    },
    ["Pellejo medio curtido"] = {
        Professions = 3817,
    },
    ["Toque marchito"] = {
        ["Trol Secacorteza"] = 4974,
        ["Lanzahachas Secacorteza"] = 4974,
        ["Rebanacabezas Secacorteza"] = 4974,
        ["Rabioso Secacorteza"] = 4974,
    },
    ["Crear piedra de fuego (inferior)"] = {
        ["WARLOCK"] = 6366,
    },
    ["Escudo divino"] = {
        ["PALADIN"] = 1020,
    },
    ["Guantes Bengala del Núcleo"] = {
        Professions = 20849,
    },
    ["Sello del cruzado"] = {
        ["PALADIN"] = 20308,
    },
    ["Tótem de resistencia a la Naturaleza"] = {
        ["SHAMAN"] = 10601,
    },
    ["Leotardos de escamas de tortuga"] = {
        Professions = 10556,
    },
    ["Encantar guantes: fuerza superior"] = {
        Professions = 20013,
    },
    ["Muñeco-diana magistral"] = {
        Professions = 19814,
    },
    ["Aura de santidad"] = {
        ["PALADIN"] = 20218,
    },
    ["Bolsa de tejido mágico"] = {
        Professions = 12065,
    },
    ["Encantamiento"] = {
        Professions = 7412,
        Professions = 7413,
        Professions = 13920,
    },
    ["Guantes del Padre Invierno"] = {
        Professions = 21943,
    },
    ["Coraza de mitril pesado"] = {
        Professions = 9959,
    },
    ["Protección divina"] = {
        ["PALADIN"] = 5573,
    },
    ["Cinturón rúnico estigio"] = {
        Professions = 24902,
    },
    ["Camisa de lino azul"] = {
        Professions = 2394,
    },
    ["Muerte de Capataz Hierro Negro"] = {
        ["Capataz Hierro Negro"] = 12613,
    },
    ["Aflicción del tonto"] = {
        ["Simone la Discreta"] = 23504,
        ["Franklin el Amistoso"] = 23504,
        ["Artorius el Afable"] = 23504,
        ["Nelson el Amable"] = 23504,
    },
    ["Sobrehombros de tejido mágico negros"] = {
        Professions = 12074,
    },
    ["Refuerzo para armadura basto"] = {
        Professions = 19058,
    },
    ["Mente de peste"] = {
        ["Comesesos"] = 3429,
        ["Cazador de magos"] = 3429,
    },
    ["Furia del tigre"] = {
        ["DRUID"] = 9846,
    },
    ["Encantar capa: protección inferior"] = {
        Professions = 13421,
    },
    ["Reflejos eficaces"] = {
        ["Sr. Golpin"] = 6264,
        ["Oficial Bael'Dun"] = 6264,
        ["Guerrero Escarlata"] = 3238,
    },
    ["Crear piedra de alma (menor)"] = {
        ["WARLOCK"] = 693,
    },
    ["Choque de Sombras"] = {
        ["Súcubo invocado"] = 16583,
        ["Vol'jin"] = 17289,
        ["Krethis Sombravolta"] = 17439,
    },
    ["Pantalones de paño brillante"] = {
        Professions = 18439,
    },
    ["Maldición de sangre"] = {
        ["Embajador Malcin"] = 12279,
        ["Necrófago demente"] = 8282,
        ["Sangre de Agamaggan"] = 8282,
        ["Alto Inquisidor Ribalimpia"] = 8282,
    },
    ["Encantar guantes: agilidad superior"] = {
        Professions = 20012,
    },
    ["Bolsa de demonio Xabraxxis"] = {
        ["Xabraxxis"] = 19127,
    },
    ["Encantar arma 2M: impacto inferior"] = {
        Professions = 13529,
    },
    ["Aura de espinas"] = {
        ["Vermis Caminasueños"] = 7966,
        ["Tejespina Rajacieno"] = 7966,
        ["Cuerolanza de Rajacieno"] = 8148,
    },
    ["Bramido desalentador"] = {
        ["Viejo Barbahielo"] = 3146,
    },
    ["Botas de paño rúnico"] = {
        Professions = 18423,
    },
    ["Encantar arma: Destripadora de bestias inferior"] = {
        Professions = 13653,
    },
    ["Cinturón de torio"] = {
        Professions = 16643,
    },
    ["Bolsa de hierbas Cenarion"] = {
        Professions = 27724,
    },
    ["Toga del archimago"] = {
        Professions = 18457,
    },
    ["Llave esqueleto de arcanita"] = {
        Professions = 19669,
    },
    ["Tromba piroclasta"] = {
        ["Elemental de lava"] = 19641,
    },
    ["Aura de resistencia"] = {
        ["Mensajero Carmesí"] = 19726,
    },
    ["Arnés de oso de guerra"] = {
        Professions = 19068,
    },
    ["Enjambre silítido"] = {
        ["Enjambre silítido"] = 6589,
        ["Enjambrista silítido"] = 6589,
        ["Enjambrista Hazali"] = 6589,
        ["Enjambrista Centipaar"] = 6589,
        ["Enjambre silítido"] = 10722,
        ["Enjambrista silítido"] = 10722,
        ["Enjambrista Hazali"] = 10722,
        ["Enjambrista Centipaar"] = 10722,
    },
    ["Crear arca de contención llena"] = {
        ["Cría del risco"] = 9010,
    },
    ["Machacar cráneos"] = {
        ["Cavador Gravamorro"] = 3551,
    },
    ["Veneno instantáneo VI"] = {
        ["ROGUE"] = 11343,
    },
    ["Rugido desmoralizador"] = {
        ["DRUID"] = 9898,
        ["Protector Cenarion"] = 15727,
    },
    ["Toxina"] = {
        ["Babosa tóxica"] = 25989,
    },
    ["Proyectil detonante"] = {
        ["Capitán Sinquilla"] = 7896,
        ["Maestro de bestias Escarlata"] = 7896,
        ["Técnico de armas"] = 7896,
        ["Traidor Mala Hierba"] = 7896,
    },
    ["Pulverizador de tinta"] = {
        ["Vomitatintas del pantano"] = 9612,
    },
    ["Encantar brazales: agilidad menor"] = {
        Professions = 7779,
    },
    ["Picadura de dracoleón"] = {
        ["HUNTER"] = 24133,
    },
    ["Tortura mental"] = {
        ["PRIEST"] = 18807,
    },
    ["Maldición de impotencia"] = {
        ["Timador Mala Hierba"] = 22371,
        ["Ayudante de sangre de Kirtonos"] = 22371,
    },
    ["Rabiar"] = {
        Racial = 26296,
        Racial = 26297,
        Racial = 20554,
    },
    ["Espada magna de bronce"] = {
        Professions = 9986,
    },
    ["Esclavizar demonio"] = {
        ["WARLOCK"] = 11726,
    },
    ["Dominación"] = {
        ["Gran Cruzado Dathrohan"] = 17405,
    },
    ["Encantar pechera: maná inferior"] = {
        Professions = 7776,
    },
    ["Cinturón de seda carmesí"] = {
        Professions = 8772,
    },
    ["Ahuyentar no-muertos"] = {
        ["PALADIN"] = 10326,
    },
    ["Beber brebaje de Cebadiz"] = {
        ["Yorus Cebadiz"] = 8554,
    },
    ["Ataque por sorpresa"] = {
        ["Acechador del risco"] = 8151,
    },
    ["Escudo profano"] = {
        ["Morbent Vil"] = 8909,
    },
    ["Proyectil artesanal pesado"] = {
        Professions = 3930,
    },
    ["Encantar brazales: regeneración de maná"] = {
        Professions = 23801,
    },
    ["Sobrehombros de dragontina azul"] = {
        Professions = 19089,
    },
    ["Tajo profundo"] = {
        ["Segador de la cosecha"] = 7342,
        ["Latigador descarriado"] = 7342,
    },
    ["Invocar fusilero carmesí"] = {
        ["Fusilero Carmesí"] = 17279,
    },
    ["Maza negra grande"] = {
        Professions = 10001,
    },
    ["Transfusión de sangre"] = {
        ["Glotón Colmillo Oscuro"] = 7122,
    },
    ["Trampa de escarcha"] = {
        ["HUNTER"] = 13809,
    },
    ["Efecto de pereza"] = {
        ["Perezoso"] = 3510,
    },
    ["Traca de cohetes azules"] = {
        Professions = 26423,
    },
    ["Viaje giratorio"] = {
        ["Rebanacabezas Gurubashi"] = 24048,
        ["Portavoz Machacacráneos"] = 24048,
    },
    ["Regreso astral"] = {
        ["SHAMAN"] = 556,
    },
    ["Rastrear bestias"] = {
        ["HUNTER"] = 1494,
    },
    ["Granada de hierro"] = {
        Professions = 3962,
    },
    ["Brazales de cuero ligero"] = {
        Professions = 9065,
    },
    ["Elixir de agilidad menor"] = {
        Professions = 3230,
    },
    ["Reacción del sistema"] = {
        ["Emisario Roman'khan"] = 23774,
    },
    ["Guanteletes de demosaurio"] = {
        Professions = 19084,
    },
    ["Ignición"] = {
        ["Refugiado Haggard"] = 3261,
    },
    ["Ráfaga sónica"] = {
        ["Murciélago del Horado mayor"] = 8281,
        ["Estridador del valle huido"] = 8281,
        ["murcípeste monstruoso"] = 8281,
        ["Ressan el Agujas"] = 8281,
    },
    ["Puerta de la Muerte"] = {
        ["Campesino malherido"] = 23127,
        ["Campesino apestado"] = 23127,
    },
    ["Lluvia de Fuego"] = {
        ["WARLOCK"] = 11678,
        ["Grel'borg el Avaro"] = 11990,
        ["Geomántico Gogger"] = 11990,
    },
    ["Encantar escudo: aguante inferior"] = {
        Professions = 13631,
    },
    ["Descarga de relámpagos"] = {
        ["SHAMAN"] = 15208,
        ["Chamán Ferrohondo"] = 15801,
        ["Lorgus Jett"] = 12167,
        ["Serpiente nubosa"] = 8246,
        ["Serpiente nubosa anciana"] = 8246,
        ["Hechicera Espinadaga"] = 20824,
        ["Veritas Vilrama"] = 20824,
        ["Chamán Fangorroca"] = 20805,
        ["Místico de Crestagrana"] = 20802,
        ["Hechicero Tótem Siniestro"] = 20802,
        ["Primalista Thurloga"] = 15234,
        ["Místico Zarparrío"] = 9532,
        ["Oráculo menor múrloc"] = 9532,
        ["Místico Cabellesangre"] = 9532,
        ["Místico Machacacráneos"] = 9532,
        ["Oráculo Escama de Sal"] = 9532,
        ["Místico Pellejomusgo"] = 9532,
        ["Chamán Zarparrío"] = 9532,
        ["Geólogo de Ventura y Cía."] = 9532,
        ["Profeta Rompecantos"] = 9532,
        ["Chamán Mo'grosh"] = 9532,
        ["Místico Mo'grosh"] = 9532,
        ["Profeta Peloescarcha"] = 9532,
        ["Magosh"] = 9532,
        ["Oráculo menor Anca Vil"] = 9532,
        ["Deeb"] = 9532,
        ["Matriarca Sangrepluma"] = 9532,
        ["Chamán Bosque Negro"] = 9532,
        ["Oráculo Brumagris"] = 9532,
        ["Sirena Espinadaga"] = 9532,
        ["Oráculo Rompeanca"] = 9532,
        ["Chamán Puño de Roca"] = 9532,
        ["Chamán Rotapolvo"] = 9532,
        ["Exiliado tronador"] = 9532,
        ["Místico Rotapolvo"] = 9532,
        ["Chamán Lomopelo"] = 9532,
        ["Ventobruja Viento Furioso"] = 9532,
        ["Matriarca Viento Furioso"] = 9532,
        ["Bruja de la Tormenta Viento Seco"] = 9532,
        ["Prole de Halcón Trueno"] = 9532,
        ["Agitadora Kolkar"] = 9532,
        ["Nak"] = 9532,
        ["Boahn"] = 9532,
        ["Clamalluvias Sombramatorral"] = 9532,
        ["Druida del Colmillo"] = 9532,
        ["Caedakar el Vicioso"] = 9532,
        ["Bruja de la Tormenta Furia Sangrienta"] = 9532,
        ["Quimera joven"] = 9532,
        ["Agitadora Galak"] = 9532,
        ["Vigilante Ordanus"] = 9532,
        ["Oráculo Ancalodo"] = 9532,
        ["Escarbador Valvafango"] = 9532,
        ["Geomántico Rajacieno"] = 9532,
        ["Cazavientos Kolkar"] = 9532,
        ["Cazavientos Maggram"] = 9532,
        ["Cazavientos Gelkis"] = 9532,
        ["Supervisor Cozzle"] = 9532,
        ["Oráculo Cegaluz"] = 9532,
        ["Clamor de Tierra Halmgar"] = 9532,
        ["Roogug"] = 9532,
        ["Oráculo de la Bahía Tormentosa"] = 9532,
        ["Pícaro Brumala"] = 9532,
        ["Videntormento Kolkar"] = 9532,
        ["Chamán Furia Ardiente"] = 9532,
        ["Clamavientos Veloneve"] = 9532,
    },
    ["Muerte y descomposición"] = {
        ["Fiel Caramuerte"] = 11433,
    },
    ["Canibalismo"] = {
        Racial = 20577,
    },
    ["Encantar pechera: salud"] = {
        Professions = 7857,
    },
    ["Brazales de cría verde"] = {
        Professions = 9202,
    },
    ["Puntos de honor +398"] = {
        ["Belisario de la Horda"] = 24923,
        ["Brigadier general de la Alianza"] = 24923,
    },
    ["Madeja de paño de seda"] = {
        Professions = 3839,
    },
    ["Calor agostador"] = {
        ["Can del Núcleo anciano"] = 19367,
    },
    ["Veneno intoxicante"] = {
        ["Venenoso Razzashi"] = 24596,
    },
    ["Poción de Protección contra la Naturaleza"] = {
        Professions = 7259,
    },
    ["Veneno instantáneo II"] = {
        ["ROGUE"] = 8687,
    },
    ["Crear piedra de alma (sublime)"] = {
        ["WARLOCK"] = 20757,
    },
    ["Cañón de llamas"] = {
        ["Constructo Martillo de cólera"] = 15575,
    },
    ["Crear piedra de alma (inferior)"] = {
        ["WARLOCK"] = 20752,
    },
    ["Arremetida poderosa"] = {
        ["Devilsaurio"] = 14099,
        ["Monstruosidad de la Peste"] = 14099,
        ["Aplastacráneos Ferrohondo"] = 14099,
    },
    ["Frasco de sabiduría destilada"] = {
        Professions = 17636,
    },
    ["Armadura múrloc gruesa"] = {
        Professions = 6704,
    },
    ["Abrumar"] = {
        ["WARRIOR"] = 11585,
    },
    ["Garra de hielo"] = {
        ["Bjarn"] = 3130,
        ["Oso zarpahelada"] = 3130,
    },
    ["Coraza de escamas de tortuga"] = {
        Professions = 10511,
    },
    ["Elixir de defensa menor"] = {
        Professions = 7183,
    },
    ["Destruir estandarte de Karang"] = {
        ["Hedioserra enfurecido"] = 20786,
    },
    ["Capa de cuero oscuro"] = {
        Professions = 2168,
    },
    ["Jubón de tejido mágico rojo"] = {
        Professions = 12056,
    },
    ["Coraza volcánica"] = {
        Professions = 19076,
    },
    ["Cinturón de placas imperiales"] = {
        Professions = 16647,
    },
    ["Dominar mente"] = {
        ["Sirena Strashaz"] = 7645,
        ["Mago oscuro Crepuscular"] = 7645,
        ["Iniciado Hederine"] = 15859,
        ["Kantor"] = 14515,
        ["Médium Jargba"] = 14515,
    },
    ["Sentencia"] = {
        ["PALADIN"] = 20271,
    },
    ["Encantar pechera: salud excelente"] = {
        Professions = 13858,
    },
    ["Sanación en cadena"] = {
        ["SHAMAN"] = 10623,
        ["Aggem Malaespina"] = 14900,
    },
    ["Encantar escudo: bloqueo inferior"] = {
        Professions = 13689,
    },
    ["Polimorfia superior"] = {
        ["Vinculador de hechizos Alanegra"] = 22274,
        ["Grethok el Controlador"] = 22274,
    },
    ["Botas de hierro verdes"] = {
        Professions = 3334,
    },
    ["Aullido aterrorizador"] = {
        ["Nefaru"] = 8715,
        ["Estridador Alaroca"] = 8715,
        ["Aullanieblas"] = 8715,
    },
    ["Silencio"] = {
        ["PRIEST"] = 15487,
        ["Cazador de las Sombras Secacorteza"] = 6726,
        ["Arcanista Doan"] = 8988,
    },
    ["Invocar guardianes Zulian"] = {
        ["Guardián Zulian"] = 24183,
    },
    ["Crear piedra de fuego"] = {
        ["WARLOCK"] = 17951,
    },
    ["Encantar guantes: fuerza"] = {
        Professions = 13887,
    },
    ["Invocar espíritu del pantano"] = {
        ["Espíritu del pantano"] = 9636,
        ["Hablador del pantano"] = 9636,
    },
    ["Bomba de hierro grande"] = {
        Professions = 3967,
    },
    ["Crear piedra de salud (inferior)"] = {
        ["WARLOCK"] = 6202,
    },
    ["Embate con escudo"] = {
        ["WARRIOR"] = 23925,
        ["Renegado Roca Negra"] = 8242,
        ["Esbirro Defias"] = 8242,
        ["Kam Furiahonda"] = 8242,
        ["Bruto Faucedraco"] = 8242,
        ["Déspota abrasador"] = 8242,
    },
    ["Fatigado"] = {
        ["Esclavo Corvozarpa"] = 3271,
    },
    ["Agrandar"] = {
        ["Cazatumbres Crepuscular"] = 8365,
    },
    ["Asustar bestia"] = {
        ["HUNTER"] = 14327,
    },
    ["Tótem de resistencia al Fuego"] = {
        ["SHAMAN"] = 10538,
    },
    ["Flagelo"] = {
        ["DRUID"] = 9908,
    },
    ["Desfibriladores goblin"] = {
        Professions = 9273,
    },
    ["Descarga de las Sombras"] = {
        ["WARLOCK"] = 25307,
        ["Tejeoscuro Pielsombra"] = 9613,
        ["Nigromante Roca Negra"] = 9613,
        ["Tejesombras Nocturno"] = 9613,
        ["Sanador esquelético"] = 9613,
        ["Reductor de cabezas Kurzen"] = 9613,
        ["Médico brujo Zanzil"] = 9613,
        ["Duende sombrío"] = 9613,
        ["Conjurador del Sindicato"] = 9613,
        ["Darbel Montrosa"] = 9613,
        ["Augurio Escarlata"] = 9613,
        ["Arúspice Escarlata"] = 9613,
        ["Médium Jargba"] = 9613,
        ["Sacerdote Caramuerte"] = 9613,
        ["Mago sangriento Thalnos"] = 9613,
        ["Clamainferno Malarraíz"] = 9613,
        ["Mago oscuro Crepuscular"] = 9613,
        ["Tejeyel Scronn"] = 9613,
        ["Necromántico Caramuerte"] = 9613,
        ["Fiel Caramuerte"] = 9613,
        ["Mannoroc Latiguez"] = 9613,
        ["Necromántico paria"] = 20298,
        ["Nigromante Peloescarcha"] = 20791,
        ["Conjurador Dalaran"] = 20791,
        ["Licillin"] = 20791,
        ["Mago oscuro del Sindicato"] = 20791,
        ["Aprendiz Filo Ardiente"] = 20791,
        ["Fizzle Necromenta"] = 20791,
        ["Gazz'uz"] = 20791,
        ["Ilkrud Magthrull"] = 20791,
        ["Fiel de la Facción Oscura"] = 20791,
        ["Adepto de la Facción Oscura"] = 20791,
        ["Boticario Falthis"] = 20791,
        ["Balizar el Agraviado"] = 20791,
        ["Brujo abrasador"] = 20791,
        ["Bookie Herod"] = 20816,
        ["Mago oscuro Artus"] = 20816,
        ["Nigromante de las Sombras Secacorteza"] = 20816,
        ["Nigromante Hierro Negro"] = 20816,
        ["Mago oscuro Filo Ardiente"] = 20816,
        ["Brujo Velasangre"] = 20825,
        ["Acólito esquelético"] = 20825,
        ["Tejeoscuro Forjatiniebla"] = 20825,
        ["Invocador Filo Ardiente"] = 20825,
        ["Pastor Faucedraco"] = 20807,
        ["Llamacío de la Facción Oscura"] = 20807,
        ["Augur Filo Ardiente"] = 20807,
        ["Iniciado Hederine"] = 15232,
        ["Morloch"] = 15537,
    },
    ["Armadura de cuero fortalecido"] = {
        Professions = 2166,
    },
    ["Tótem de tremor"] = {
        ["SHAMAN"] = 8143,
    },
    ["Guantes de tejido de Escarcha"] = {
        Professions = 18411,
    },
    ["Filacteria de Araj"] = {
        ["Araj el Invocador"] = 18661,
    },
    ["Chirrido"] = {
        Pet = 24579,
    },
    ["Armadura ósea"] = {
        ["Necromántico Caramuerte"] = 11445,
    },
    ["Bolsa sin fondo"] = {
        Professions = 18455,
    },
    ["Furia sangrienta"] = {
        Racial = 20572,
    },
    ["Invocar guardián de agua"] = {
        ["Guardián de Agua"] = 8372,
        ["Aquamántivo Crepuscular"] = 8372,
    },
    ["Anulación en masa"] = {
        ["Nulificador X21 Arcano"] = 10832,
    },
    ["Capa de Fuego"] = {
        Professions = 18422,
    },
    ["Piedra de alquimista"] = {
        Professions = 17632,
    },
    ["Botas de bronce bañadas en plata"] = {
        Professions = 3331,
    },
    ["Socavar"] = {
        ["Perdido desdichado"] = 11963,
        ["Altonato persistente"] = 11963,
    },
    ["Bendición de salvaguardia superior"] = {
        ["PALADIN"] = 25899,
    },
    ["Botas de lana"] = {
        Professions = 2401,
    },
    ["Aparición de dracónido verde"] = {
        ["Dracónido verde"] = 22656,
        ["Green Drakonid Spawner"] = 22656,
    },
    ["Choque de tierra"] = {
        ["SHAMAN"] = 10414,
        ["Vapuleador Tótem Siniestro"] = 13281,
        ["Vigilapedras Gogger"] = 13281,
        ["Oráculo Hakkari"] = 15501,
    },
    ["Pica llameante"] = {
        ["Garneg Hullacráneo"] = 6725,
        ["Hermana Riven"] = 6725,
        ["Lo'Grosh"] = 8814,
        ["Mago sangriento Thalnos"] = 8814,
        ["Geomántico Caramuerte"] = 8814,
    },
    ["Jubón de seda carmesí"] = {
        Professions = 8791,
    },
    ["Crear piedra de alma"] = {
        ["WARLOCK"] = 20755,
    },
    ["Bola de cuero pesado"] = {
        Professions = 23190,
    },
    ["Fuegos artificiales con destellos rojos"] = {
        ["Maestro Destructor Emi Plomocorto"] = 11542,
    },
    ["Leotardos de tela lunar"] = {
        Professions = 18440,
    },
    ["Crear piedra de fuego (sublime)"] = {
        ["WARLOCK"] = 17953,
    },
    ["Aparición elemental"] = {
        ["Barón Kazum"] = 25035,
        ["Duque de las Brasas"] = 25035,
        ["Duque de las Profundidades"] = 25035,
        ["Duque de las Esquirlas"] = 25035,
        ["Templario Carmesí"] = 25035,
        ["Templario azur"] = 25035,
        ["Templario vetusto"] = 25035,
        ["Duque de los Céfiros"] = 25035,
        ["Templario terráneo"] = 25035,
    },
    ["Guanteletes de dragontina verde"] = {
        Professions = 24655,
    },
    ["Invocar incendiario flamante"] = {
        ["Incendiario flamante"] = 15710,
        ["Terrorspark"] = 15710,
    },
    ["Leotardos vid de sangre"] = {
        Professions = 24092,
    },
    ["Aura de concentración"] = {
        ["PALADIN"] = 19746,
    },
    ["Invocar guardia apocalíptico"] = {
        ["Esbirro del guardia apocalíptico"] = 22865,
        ["Brujo Gordok"] = 22865,
    },
    ["Relámpago bifurcado"] = {
        ["Lady Sarevess"] = 8435,
        ["Lady Vespira"] = 12549,
    },
    ["Torbellino"] = {
        ["WARRIOR"] = 1680,
        ["Thelman Puñopizarra"] = 13736,
        ["Drek'Thar"] = 13736,
        ["Capitán Galvangar"] = 13736,
        ["Alguacil Sangrehielo"] = 13736,
        ["Alguacil de Punta de la Torre"] = 13736,
        ["Alguacil del este Lobo Gélido"] = 13736,
        ["Mariscal Lobo Gélido"] = 13736,
        ["Maestro de guerra del norte de Dun Baldar"] = 13736,
        ["Maestro de guerra del sur de Dun Baldar"] = 13736,
        ["Maestro de guerra del este Lobo Gélido"] = 13736,
        ["Maestro de guerra Alahielo"] = 13736,
        ["Maestro de guerra Corapernal"] = 13736,
        ["Maestro de guerra del oeste Lobo Gélido"] = 13736,
        ["Lady Hoteshem"] = 13736,
        ["Herod"] = 8989,
        ["Élite Kurzen"] = 17207,
        ["Déspota del Sindicato"] = 17207,
        ["Nimar el Destripador"] = 17207,
        ["Guerrero Forjatiniebla"] = 17207,
        ["Guardia Jarad"] = 17207,
        ["Guardia Kahil"] = 17207,
        ["Ciclonio"] = 17207,
        ["Keetar"] = 17207,
    },
    ["Cuchilla Sagrada"] = {
        ["Alto señor Taelan Vadín"] = 18819,
        ["Lord Tirion Fordring"] = 18819,
    },
    ["Grito de batalla"] = {
        ["WARRIOR"] = 25289,
        ["Narg el Capataz"] = 9128,
        ["Capitán Horrocrusto"] = 9128,
        ["Alfa Pellejomusgo"] = 9128,
        ["Insurgente Defias"] = 9128,
        ["Bazil Thredd"] = 9128,
        ["Ma'ruk Vermiscala"] = 9128,
        ["Corremanadas Kolkar"] = 9128,
        ["Déspota de Ventura y Cía."] = 9128,
        ["Capitán Justamonta"] = 9128,
        ["Señor Supremo Colmicarnero"] = 9128,
    },
    ["Invocar espíritu de Caminamuerte Atal'ai"] = {
        ["Espíritu de Caminamuerte Atal'ai"] = 12095,
        ["Caminamuerte Atal'ai"] = 12095,
    },
    ["Sombra de espíritu visual"] = {
        ["Espíritu sombrío"] = 24809,
    },
    ["Atropello"] = {
        ["Panzallena"] = 5568,
        ["Revientaenemigos 4000"] = 5568,
        ["Gorlash"] = 5568,
        ["Pisoteador Brezomadera"] = 5568,
        ["Furia de piedra"] = 5568,
        ["Fozruk"] = 5568,
        ["Toro Kodo"] = 5568,
        ["Matriarca Kodo"] = 5568,
        ["Timbrador Gelkis"] = 5568,
        ["Ghamoo-ra"] = 5568,
        ["Gólem de guerra pesado"] = 5568,
        ["Vasallo de Arkkoroc"] = 5568,
    },
    ["Berrido de convocación del matadragones"] = {
        ["Señor Supremo Runthak"] = 22888,
        ["Mayor Mattingly"] = 22888,
        ["Alto señor supremo Colmillosauro"] = 22888,
        ["Alguacil de campo Afrasiabi"] = 22888,
    },
    ["Botas de mitril ornamentado"] = {
        Professions = 9979,
    },
    ["Mutis"] = {
        ["Gruñidor"] = 5543,
    },
    ["Fundir bronce"] = {
        Professions = 2659,
    },
    ["Tótem emanador de lava"] = {
        ["Tótem emanador de lava"] = 8264,
        ["Profeta Caramuerte"] = 8264,
    },
    ["Poción de presteza"] = {
        Professions = 2335,
    },
    ["Esbirro de Antu'sul"] = {
        ["Sirviente de Antu'sul"] = 11894,
    },
    ["Furia del pollo"] = {
        ["Gallo de batalla"] = 13168,
    },
    ["Elixir de sabiduría"] = {
        Professions = 3171,
    },
    ["Bomba"] = {
        ["Demoledor Hierro Negro"] = 8858,
        ["Bombardero Hierro Negro"] = 8858,
        ["Geólogo Hierro Negro"] = 8858,
        ["Técnico Negador"] = 8858,
        ["Jefe Encobre"] = 9143,
    },
    ["Pólvora sólida"] = {
        Professions = 12585,
    },
    ["Esfumarse"] = {
        ["ROGUE"] = 1857,
    },
    ["Armadura de fortuna de las Sombras de Sayge"] = {
        ["Sayge"] = 23767,
    },
    ["Zurrar"] = {
        ["WARRIOR"] = 6554,
    },
    ["Bendición de salvación superior"] = {
        ["PALADIN"] = 25895,
    },
    ["Martillo volcánico"] = {
        Professions = 16984,
    },
    ["Habilidad de los Renegados"] = {
        ["Oficial atormentado"] = 7054,
    },
    ["Cachorro de la sabana"] = {
        ["Cachorro de la savana"] = 6598,
        ["Matriarca de la savana"] = 6598,
    },
    ["Llamas cauterizantes"] = {
        ["Can del Núcleo anciano"] = 19366,
    },
    ["Cinta de tejido mágico roja"] = {
        Professions = 12084,
    },
    ["Invocar Tótem Lavado de cerebro"] = {
        ["Totém Lavamentes"] = 24262,
    },
    ["Fundir oro"] = {
        Professions = 3308,
    },
    ["Crear estandarte de Krom'zar"] = {
        ["Señor de la Guerra Krom'zar"] = 13965,
    },
    ["Piedra de pulir sólida"] = {
        Professions = 9920,
    },
    ["Caperuza de seda azur"] = {
        Professions = 8760,
    },
    ["Aparato de invisibilidad gnómico"] = {
        Professions = 3971,
    },
    ["Encantar guantes: herboristería"] = {
        Professions = 13617,
    },
    ["Salpicadura de ácido"] = {
        ["El Chef"] = 6306,
        ["Vermis verde"] = 6306,
        ["Ancestro dragonante"] = 6306,
        ["Gluggle"] = 6306,
    },
    ["Fundir plata"] = {
        Professions = 2658,
    },
    ["Deflagración de cadáver"] = {
        ["Necromántico de Scholomance"] = 17616,
    },
    ["Colisión de olas"] = {
        ["Gazban"] = 5403,
    },
    ["Succionar vida"] = {
        ["WARLOCK"] = 18881,
    },
    ["Captar demonios"] = {
        ["WARLOCK"] = 5500,
    },
    ["Sobrehombros de placas imperiales"] = {
        Professions = 16646,
    },
    ["Invocar sirviente esquelético"] = {
        ["Sirviente esquelético"] = 12420,
        ["Necromántico Caramuerte"] = 12420,
        ["Embajador Sanguinarius"] = 12420,
        ["Necromántico"] = 12420,
        ["Necromántico Thuzadin"] = 12420,
    },
    ["Pozo de Luz"] = {
        ["PRIEST"] = 27871,
    },
    ["Aguijón con púas"] = {
        ["Aguijonero profundo"] = 14534,
    },
    ["Brazales de bronce estampados"] = {
        Professions = 2672,
    },
    ["Escudo de fuego II"] = {
        ["Cría Carmesí"] = 184,
        ["Geólogo de Ventura y Cía."] = 184,
        ["Temófogo Puñastilla"] = 184,
        ["Garneg Hullacráneo"] = 184,
        ["Guardia maldito inferior"] = 184,
        ["Devastador ardiente"] = 184,
        ["Invocador Roca Negra"] = 184,
        ["Supervisor Cozzle"] = 184,
        ["Hermana Riven"] = 184,
    },
    ["Leotardos de guerra orcos"] = {
        Professions = 9957,
    },
    ["Fluir de Nortearroyo"] = {
        ["Harpía Nortearroyo"] = 11014,
        ["Malapluma Nortearroyo"] = 11014,
        ["Destripador Nortearroyo"] = 11014,
        ["Clamaviento Nortearroyo"] = 11014,
    },
    ["Brazales Hierro Negro"] = {
        Professions = 20874,
    },
    ["Susurros oscuros"] = {
        ["Tutor vil"] = 16587,
    },
    ["Cauce de salud"] = {
        ["WARLOCK"] = 11695,
    },
    ["Cinturón de seda terránea"] = {
        Professions = 8797,
    },
    ["Maldición de alma en pena"] = {
        ["Altonato maldito"] = 5884,
        ["Altonato en pena"] = 5884,
        ["Altonato de los Lamentos"] = 5884,
        ["Anaia Correalba"] = 5884,
    },
    ["Rugido glacial"] = {
        ["Vagash"] = 3143,
    },
    ["Invocar caminante abisal"] = {
        ["Infracaminante"] = 22876,
        ["Ojo de Kilrogg deambulante"] = 22876,
    },
    ["Lanzamiento de hachas"] = {
        ["Tallamadera goblin"] = 6466,
    },
    ["Marca de la vergüenza"] = {
        ["Parqual Fintallas"] = 6767,
    },
    ["Transformación de Kirtonos"] = {
        ["Kirtonos el Heraldo"] = 16467,
    },
    ["Toga de paño cenizo"] = {
        Professions = 12069,
    },
    ["Contrapeso denso"] = {
        Professions = 16640,
    },
    ["Cobertura de hollín"] = {
        ["Anciano ennegrecido"] = 7998,
    },
    ["Coraza barbárica de hierro"] = {
        Professions = 9813,
    },
    ["Rebote"] = {
        ["PRIEST"] = 19275,
    },
    ["Peste iracunda"] = {
        ["Campesino apestado"] = 23072,
    },
    ["Ira salvaje"] = {
        ["Furiadiente Colmillo Oscuro"] = 7072,
    },
    ["Cinta de tejido mágico negra"] = {
        Professions = 12072,
    },
    ["Forma de lechúcico lunar"] = {
        ["DRUID"] = 24858,
    },
    ["Encantar arma 2M: agilidad"] = {
        Professions = 27837,
    },
    ["Guerrera de cuero refinado"] = {
        Professions = 3761,
    },
    ["Gema abisal"] = {
        ["Bethor Duroyelo"] = 7673,
    },
    ["Brazales de cuero maligno"] = {
        Professions = 19052,
    },
    ["Portal: Ventormenta"] = {
        ["MAGE"] = 10059,
    },
    ["Rastrear humanoides"] = {
        ["DRUID"] = 5225,
        ["HUNTER"] = 19883,
    },
    ["Furia desalmada"] = {
        ["Mefisto esquelético"] = 3416,
    },
    ["Baba de larva"] = {
        ["Larva vil"] = 21069,
    },
    ["Pisotón vil"] = {
        ["Corcel nefasto"] = 7139,
    },
    ["Encantar botas: espíritu"] = {
        Professions = 20024,
    },
    ["Desarmar"] = {
        ["WARRIOR"] = 676,
        ["Rastricacas"] = 8379,
        ["Jailor Borhuin"] = 8379,
        ["Kenata Dabyrie"] = 8379,
        ["Mirmidón Brazanegra"] = 8379,
        ["Quebrador Sombroscura"] = 8379,
        ["Guerrero Cabellesangre"] = 6713,
        ["Despojador Defias"] = 6713,
        ["Sneed"] = 6713,
        ["Primer oficial Esnelim"] = 6713,
        ["Prisionero Defias"] = 6713,
        ["Ladrón del Sindicato"] = 6713,
        ["Granjero de Trabalomas"] = 6713,
        ["Matón Crepuscular"] = 6713,
        ["Merodeador del Sindicato"] = 6713,
        ["Lord Cresta de Halcón"] = 6713,
        ["Déspota de Ventura y Cía."] = 6713,
        ["Marino de Theramore"] = 6713,
        ["Murciovil"] = 6713,
        ["Dal Zarpasangrante"] = 6713,
        ["Volater aterrador"] = 6713,
        ["Guardaespaldas Defias"] = 6713,
        ["Bandido Tótem Siniestro"] = 6713,
    },
    ["Descarga en cadena"] = {
        ["Charlga Filonavaja"] = 8292,
        ["Owatanka"] = 6254,
        ["Margol la Furiosa"] = 15549,
    },
    ["Tormenta de maná"] = {
        ["Azuregos"] = 21097,
    },
    ["Bolsa de lana roja"] = {
        Professions = 6688,
    },
    ["Rugido de batalla"] = {
        ["Argos Furia Lunar"] = 6507,
        ["Argos Piroleño"] = 6507,
        ["Señor de la manada Luporror"] = 6507,
        ["Ancestro Estigmodio"] = 6507,
    },
    ["Brazales de cuero verdes"] = {
        Professions = 3776,
    },
    ["Encantar guantes: herboristería avanzada"] = {
        Professions = 13868,
    },
    ["Caperuza de las Sombras"] = {
        Professions = 3858,
    },
    ["Elixir de fuerza de ogro"] = {
        Professions = 3188,
    },
    ["Ocultafilo"] = {
        ["Quebrantadientes"] = 16610,
    },
    ["Sprint"] = {
        ["ROGUE"] = 11305,
    },
    ["Cuero ligero"] = {
        Professions = 2881,
    },
    ["Nube de peste"] = {
        ["Ardo Patasoez"] = 3256,
        ["Siembrapestes Putrepellejo"] = 3256,
    },
    ["Agilidad de fortuna de las Sombras de Sayge"] = {
        ["Sayge"] = 23736,
    },
    ["Invocar soldado de caballería Escarlata"] = {
        ["Tropista Escarlata"] = 19722,
        ["Tropista Escarlata"] = 19722,
    },
    ["Veneno instantáneo"] = {
        ["ROGUE"] = 8681,
    },
    ["Madeja de paño de lino"] = {
        Professions = 2963,
    },
    ["Botas de acero pulido"] = {
        Professions = 3513,
    },
    ["Invocar ráfaga de lava F"] = {
        ["Rata del Núcleo"] = 21904,
    },
    ["Transfusión de vida"] = {
        ["WARLOCK"] = 11689,
    },
    ["Hacer picadillo"] = {
        ["ROGUE"] = 6774,
    },
    ["Tajo de acechasombras"] = {
        ["Acechasombras Lobrecuore"] = 6927,
    },
    ["Mordeduras degenerativas"] = {
        ["Insecto infestado"] = 16460,
    },
    ["Encantar arma: golpear superior"] = {
        Professions = 13943,
    },
    ["Visión mental"] = {
        ["PRIEST"] = 10909,
    },
    ["Lanzallamas"] = {
        Pet = 25027,
    },
    ["Desfibriladores goblin XL"] = {
        Professions = 23078,
    },
    ["Encantar arma: escalofrío gélido"] = {
        Professions = 20029,
    },
    ["Llamar a atracador"] = {
        ["Atracador Gordok"] = 22860,
    },
    ["Tótem agostador"] = {
        ["Tótem del desdén"] = 15038,
    },
    ["Invocar veterano Puño Negro"] = {
        ["Veterano Puño Negro invocado"] = 15792,
        ["Invocador Puño Negro"] = 15792,
    },
    ["Drenar maná"] = {
        ["WARLOCK"] = 11704,
    },
    ["Jubón nemoroso"] = {
        Professions = 28480,
    },
    ["Resurrección"] = {
        ["PRIEST"] = 20770,
    },
    ["Aliento nocivo"] = {
        ["Ysondre"] = 24818,
        ["Lethon"] = 24818,
        ["Emeriss"] = 24818,
        ["Taerar"] = 24818,
    },
    ["Aceptar rendición"] = {
        ["Thontek Facestruendo"] = 17649,
    },
    ["Elixir de detección de invisibilidad inferior"] = {
        Professions = 3453,
    },
    ["Camisa de tejido mágico naranja"] = {
        Professions = 12061,
    },
    ["Invocar huevo de gallina"] = {
        ["Gallina"] = 13563,
    },
    ["Pantalones de anillas de cobre"] = {
        Professions = 2662,
    },
    ["Crea Ágata de maná"] = {
        ["MAGE"] = 759,
    },
    ["Toque de la muerte"] = {
        ["Morbent Vil"] = 3108,
    },
    ["Pantalones de lino marrones"] = {
        Professions = 3914,
    },
    ["Antorcha de la falla"] = {
        ["Buscagujeros de Los Perdidos"] = 9614,
    },
    ["Corona nemorosa"] = {
        Professions = 28481,
    },
    ["Sobrehombros de lana reforzada"] = {
        Professions = 3849,
    },
    ["Veneno fulminante"] = {
        ["Terror escórpido"] = 13884,
        ["Atracador escórpido"] = 13884,
    },
    ["Encantar Anales de Villa Darrow"] = {
        ["Cromi"] = 17285,
    },
    ["Sobrehombros de tela lunar"] = {
        Professions = 18448,
    },
    ["Cinturón barbárico"] = {
        Professions = 3779,
    },
    ["Encantar capa: resistencia menor"] = {
        Professions = 7454,
    },
    ["Poción de Protección contra la Escarcha superior"] = {
        Professions = 17575,
    },
    ["Coraza de mitril ornamentado"] = {
        Professions = 9972,
    },
    ["Fatalidad espeluznante"] = {
        ["Maldior trepador"] = 23589,
        ["Nelson el Amable"] = 23589,
    },
    ["Aspersión"] = {
        ["Jinete de olas Escama Tormentosa"] = 13586,
        ["Lord Pecatripador"] = 13586,
    },
    ["Mando universal gnómico"] = {
        Professions = 9269,
    },
    ["Guerrera de tejido de Escarcha"] = {
        Professions = 18403,
    },
    ["Sacrificio de Piemartillo"] = {
        ["Grez Dedomaza"] = 4984,
    },
    ["Cinturón de tejido fantasmal"] = {
        Professions = 18410,
    },
    ["Guantes de tejido mágico rojos"] = {
        Professions = 12066,
    },
    ["Cauce de sangre"] = {
        ["Sacerdote de sangre Hakkari"] = 24617,
    },
    ["Disipar magia"] = {
        ["PRIEST"] = 988,
    },
    ["Cocina de Cocinitas"] = {
        ["El Chef"] = 5174,
    },
    ["Aura de Resistencia a las Sombras"] = {
        ["PALADIN"] = 19896,
    },
    ["Deflagración"] = {
        ["Maestro Destructor Emi Plomocorto"] = 12158,
        ["Maestro Destructor Emi Plomocorto"] = 12159,
    },
    ["Tótem Gracia del aire"] = {
        ["SHAMAN"] = 25359,
    },
    ["Coraza de acero"] = {
        Professions = 9916,
    },
    ["Cortar alas"] = {
        ["HUNTER"] = 14268,
    },
    ["Imposición de manos"] = {
        ["PALADIN"] = 10310,
        ["Comandante Escarlata Mograine"] = 9257,
    },
    ["Filo del invierno"] = {
        Professions = 21913,
    },
    ["Escudo de fuego III"] = {
        ["Mago Velasangre"] = 2601,
        ["Lo'Grosh"] = 2601,
        ["Evocador Escarlata"] = 2601,
        ["Zahorí Escarlata"] = 2601,
        ["Infernal inferior"] = 2601,
    },
    ["Golpe en los riñones"] = {
        ["ROGUE"] = 8643,
    },
    ["Recombobulador menor"] = {
        Professions = 3952,
    },
    ["Fuegos artificiales azules"] = {
        Professions = 23067,
    },
    ["Salvajismo"] = {
        ["Grell feroz"] = 5515,
        ["Furia Sangrepluma"] = 5515,
    },
    ["Encantar capa: resistencia superior"] = {
        Professions = 20014,
    },
    ["Encantar arma: arma profana"] = {
        Professions = 20033,
    },
    ["Tótem abrasador"] = {
        ["SHAMAN"] = 10438,
    },
    ["Martillo de guerra de bronce"] = {
        Professions = 9985,
    },
    ["Botas oscurecidas"] = {
        Professions = 9207,
    },
    ["Resguardo contra la Escarcha presuroso"] = {
        ["Oráculo múrloc"] = 4980,
        ["Místico Machacacráneos"] = 4980,
        ["Guarda esquelético"] = 4980,
        ["Cazamareas Anca Vil"] = 4980,
        ["Zahorí Dalaran"] = 4980,
        ["Hechicera Colafuria"] = 4980,
        ["Lady Vespia"] = 4980,
    },
    ["Invocar demonio del orbe"] = {
        ["Demonio del orbe"] = 9097,
        ["Tabetha"] = 9097,
    },
    ["Disparo enfermizo"] = {
        ["Cuerolanza marchito"] = 11397,
    },
    ["Guantes terráneos gruesos"] = {
        Professions = 9149,
    },
    ["Traje pantalón de fiesta rojo"] = {
        Professions = 26407,
    },
    ["Cohete, GRANDE AZUL"] = {
        ["Tipo de los fuegos artificiales de Pat AZUL GRANDE"] = 26351,
    },
    ["Bomba grande de cobre"] = {
        Professions = 3937,
    },
    ["Toga de zahorí inferior"] = {
        Professions = 6690,
    },
    ["Muñeco-diana - Evento 002"] = {
        ["Muñeco-diana del equipo mortero"] = 18907,
        ["Avistador Klemmy"] = 18907,
    },
    ["Elixir de fuerza bruta"] = {
        Professions = 17557,
    },
    ["Contrapeso sólido"] = {
        Professions = 9921,
    },
    ["Pantalones de lino sencillos"] = {
        Professions = 12044,
    },
    ["Tótem de resistencia al fuego"] = {
        ["SHAMAN"] = 8184,
    },
    ["Cristalizar"] = {
        ["Araña de la cumbre"] = 16104,
    },
    ["Mira mortal"] = {
        Professions = 12597,
    },
    ["Punición"] = {
        ["PRIEST"] = 10934,
    },
    ["Jubón glacial"] = {
        Professions = 28207,
    },
    ["Rejuvenecimiento"] = {
        ["DRUID"] = 25299,
        ["Archidruida Renferal"] = 15981,
        ["Vermis Caminasueños"] = 12160,
        ["Hechicero Tótem Siniestro"] = 12160,
    },
    ["Invocar a Marduk el Negro"] = {
        ["Marduk el Negro"] = 18650,
        ["Capitán Rutagrana"] = 18650,
    },
    ["Fundir torio"] = {
        Professions = 16153,
    },
    ["Gafas de Fuego"] = {
        Professions = 12594,
    },
    ["Sigilo"] = {
        ["ROGUE"] = 1787,
        ["Coronel Kurzen"] = 8822,
    },
    ["Gafas de poder con hechizos Xtremo"] = {
        Professions = 12615,
    },
    ["Presteza de la Naturaleza"] = {
        ["DRUID"] = 17116,
        ["SHAMAN"] = 16188,
    },
    ["Coraza plumahierro"] = {
        Professions = 19086,
    },
    ["Dolores torácicos"] = {
        ["Torturador del Martillo Crepuscular"] = 6945,
        ["Alguacil Windsor"] = 6945,
    },
    ["Fuego de jade"] = {
        ["Sátiro Fuego de Jade"] = 13578,
        ["Pícaro Fuego de Jade"] = 13578,
        ["Timador Fuego de Jade"] = 13578,
        ["Traidor Fuego de Jade"] = 13578,
        ["Jurapenas Fuego de Jade"] = 13578,
        ["Acechasombras Fuego de Jade"] = 13578,
        ["Clamainferno Fuego de Jade"] = 13578,
        ["Príncipe Xavalis"] = 13578,
    },
    ["Botas de cuero oscuro"] = {
        Professions = 2167,
    },
    ["Encantar pechera: maná"] = {
        Professions = 13607,
    },
    ["Leotardos de dragontina verde"] = {
        Professions = 19060,
    },
    ["Aullido de terror"] = {
        ["WARLOCK"] = 17928,
    },
    ["Veneno hiriente III"] = {
        ["ROGUE"] = 13229,
    },
    ["Espíritu de fortuna de las Sombras de Sayge"] = {
        ["Sayge"] = 23738,
    },
    ["Hender armadura"] = {
        ["WARRIOR"] = 11597,
        ["Redista múrloc"] = 11971,
        ["Minero Zarparrío"] = 11971,
        ["Tosco Putrepellejo"] = 11971,
        ["Guardia de Forjaz"] = 11971,
        ["Viscoso"] = 11971,
        ["Deforestador de la Horda"] = 11971,
        ["Zarpador Makrinni"] = 13444,
        ["Tenazario Golpeseco"] = 13444,
        ["Campeón Jabaguardia"] = 15572,
        ["Cavador Mostacho Blanco"] = 15572,
        ["Comandante del aire Guse"] = 15572,
        ["Comandante del aire Jeztor"] = 15572,
        ["Comandante del aire Mulverick"] = 15572,
        ["Peón Minafría"] = 15572,
        ["Minero Minafría"] = 15572,
        ["Minero Ferrohondo"] = 15572,
        ["Peón Ferrohondo"] = 15572,
        ["Oficial Jaxon"] = 15572,
    },
    ["Toque de debilidad"] = {
        ["PRIEST"] = 19266,
    },
    ["Red electrificada"] = {
        ["Guardián mecanizado"] = 11820,
    },
    ["Racha de viento"] = {
        ["Demonio del polvo"] = 6982,
        ["Bruja del viento Sangrepluma"] = 6982,
        ["Soplaviento Bosquenegro"] = 6982,
        ["Ventobruja Viento Furioso"] = 6982,
        ["Clamavientos estridente"] = 6982,
        ["Hermana Rathtalon"] = 6982,
        ["Ciclonio"] = 6982,
        ["Vórtice ráfaga"] = 6982,
    },
    ["Romper cosa"] = {
        ["Fel'zerul"] = 7437,
    },
    ["Casco de inmersión"] = {
        Professions = 12617,
    },
    ["Silenciatruenos"] = {
        Pet = 26090,
    },
    ["Peón durmiendo"] = {
        ["Peón vago"] = 17743,
    },
    ["Crear esencia de cría"] = {
        ["Cachorrillo negro"] = 16027,
        ["Cachorrillo hirviente"] = 16027,
        ["Cachorrillo flamascala"] = 16027,
    },
    ["Vara rúnica de plata"] = {
        Professions = 7795,
    },
    ["Bendición de Luz superior"] = {
        ["PALADIN"] = 25890,
    },
    ["Emplazar mina"] = {
        ["Mina de tonque"] = 25024,
        ["Tonque de vapor"] = 25024,
    },
    ["Histeria ancestral"] = {
        ["Can del Núcleo anciano"] = 19372,
    },
    ["Arremetida dolorosa"] = {
        ["Tosco Gordok"] = 22572,
    },
    ["Toga de poder"] = {
        Professions = 8770,
    },
    ["Capa glacial"] = {
        Professions = 28208,
    },
    ["Camisa de seda morada viva"] = {
        Professions = 3872,
    },
    ["Gran maza de cobre pesada"] = {
        Professions = 7408,
    },
    ["Beber poción"] = {
        ["Bartleby"] = 9956,
    },
    ["Corrupción"] = {
        ["WARLOCK"] = 25311,
        Professions = 16985,
    },
    ["Get Gosssip, Test"] = {
        ["Guardia urbano de Ventormenta"] = 26683,
        ["Patrulla urbana de Ventormenta"] = 26683,
        ["Guardián del Trueno"] = 26683,
        ["Bruto de Orgrimmar"] = 26683,
        ["Arnold Leland"] = 26683,
        ["Guardia de Forjaz"] = 26683,
        ["Bruto Korf"] = 26683,
        ["Bruto Bek'rah"] = 26683,
    },
    ["Bendición de sabiduría"] = {
        ["PALADIN"] = 25290,
    },
    ["Superrayo reductor"] = {
        ["Manitas Mayor Mekkatorque"] = 22742,
    },
    ["Pólvora burda"] = {
        Professions = 3929,
    },
    ["Conflagrar"] = {
        ["WARLOCK"] = 18932,
    },
    ["Botas radiantes"] = {
        Professions = 16656,
    },
    ["Rayo reductor gnómico"] = {
        Professions = 12899,
    },
    ["Resguardo de miedo"] = {
        ["PRIEST"] = 6346,
    },
    ["Elixir de agilidad inferior"] = {
        Professions = 2333,
    },
    ["Inmolar"] = {
        ["WARLOCK"] = 25309,
        ["Iniciado Hederine"] = 17883,
        ["Fiel Filo Ardiente"] = 11962,
        ["Thule Corvozarpa"] = 20800,
        ["Jergosh el Conjurador"] = 20800,
        ["Interrogador Vishas"] = 9034,
        ["Torturador Escarlata"] = 9275,
        ["Bestia entrópica"] = 15661,
    },
    ["Mina terrestre goblin"] = {
        Professions = 3968,
    },
    ["Invocar ilusiones"] = {
        ["Ilusión de Jandice Barov"] = 17773,
        ["Jandice Barov"] = 17773,
    },
    ["Explosión de llamas"] = {
        ["Delmanis el Odiado"] = 7101,
    },
    ["Red de batalla"] = {
        ["Tecnobot"] = 10852,
    },
    ["Brazales de cuero cosidos a mano"] = {
        Professions = 9059,
    },
    ["Arma Muerdepiedras"] = {
        ["SHAMAN"] = 16316,
    },
    ["Crear piedra de salud (menor)"] = {
        ["WARLOCK"] = 6201,
    },
    ["Brazales de hierro verdes"] = {
        Professions = 3501,
    },
    ["Aspecto salvaje"] = {
        ["HUNTER"] = 20190,
    },
    ["Armadura de embozo de tormenta"] = {
        Professions = 19079,
    },
    ["Mandoble de cobre"] = {
        Professions = 9983,
    },
    ["Guantes de sable de hielo"] = {
        Professions = 19087,
    },
    ["Falda sencilla"] = {
        Professions = 12046,
    },
    ["Pantalones de cuero estampado"] = {
        Professions = 3759,
    },
    ["Manteo de lino reforzado"] = {
        Professions = 2397,
    },
    ["Veneno entorpecedor II"] = {
        ["ROGUE"] = 3421,
    },
    ["Espada ancha de acero lunar"] = {
        Professions = 3496,
    },
    ["Eliminar maldición"] = {
        ["DRUID"] = 2782,
    },
    ["Recombobulador sublime"] = {
        Professions = 23079,
    },
    ["Coraza de plata resplandeciente"] = {
        Professions = 2675,
    },
    ["Elixir de respiración acuática superior"] = {
        Professions = 22808,
    },
    ["Encantar arma: agilidad"] = {
        Professions = 23800,
    },
    ["Bolsa de lana verde"] = {
        Professions = 3758,
    },
    ["Aparición de dracónido rojo"] = {
        ["Dracónido rojo"] = 22655,
        ["Engendrador de dracónido rojo"] = 22655,
    },
    ["Cadenas de hielo"] = {
        ["Zahorí Defias"] = 113,
        ["Adepto Caramuerte"] = 113,
        ["Archimago Ataeric"] = 512,
    },
    ["Vacío Arcano"] = {
        ["Azuregos"] = 21147,
    },
    ["Invocar ánima ilusoria"] = {
        ["Ánima ilusoria"] = 17231,
        ["Araj el Invocador"] = 17231,
    },
    ["Aceite de bocanegra"] = {
        Professions = 7836,
    },
    ["Detectar invisibilidad superior"] = {
        ["WARLOCK"] = 11743,
    },
    ["Bendición de sabiduría superior"] = {
        ["PALADIN"] = 25918,
    },
    ["Fuerzas corruptas de la Naturaleza"] = {
        ["Fuerza de la naturaleza corrupta"] = 21968,
    },
    ["Amplificar magia"] = {
        ["MAGE"] = 10170,
    },
    ["Bendición de reyes superior"] = {
        ["PALADIN"] = 25898,
    },
    ["Encantar arma 2M: impacto menor"] = {
        Professions = 7745,
    },
    ["Domesticar bestia"] = {
        ["HUNTER"] = 1515,
    },
    ["Herrería"] = {
        Professions = 3100,
        Professions = 3538,
        Professions = 9785,
    },
    ["Guantes de flechero"] = {
        Professions = 9145,
    },
    ["Flema enfermiza"] = {
        ["Trampedor infestado"] = 17745,
    },
    ["Poción de resistencia a la magia menor"] = {
        Professions = 3172,
    },
    ["Evasión"] = {
        ["ROGUE"] = 5277,
    },
    ["Varita mágica superior"] = {
        Professions = 14807,
    },
    ["Trabuco de mitril"] = {
        Professions = 12595,
    },
    ["Encantar brazales: fuerza menor"] = {
        Professions = 7782,
    },
    ["Guantes de cuero fortalecido"] = {
        Professions = 3770,
    },
    ["Cinturón de cuero refinado"] = {
        Professions = 3763,
    },
    ["Robar espíritu"] = {
        ["Latipesadilla"] = 3477,
    },
    ["Escabullirse"] = {
        ["Noctámbulo Defias"] = 22766,
        ["Asesino Pielsombra"] = 22766,
        ["Pantera Faucesombría"] = 22766,
        ["Comando Kurzen"] = 22766,
        ["Trepador del cubil"] = 22766,
        ["Pantera Faucesombría vieja"] = 22766,
        ["Garrasombría"] = 22766,
        ["Espía del Sindicato"] = 22766,
        ["Asesino del Sindicato"] = 22766,
        ["Bandolero del Sindicato"] = 22766,
        ["Merodeador del Sindicato"] = 22766,
        ["Acechador de la cresta"] = 22766,
        ["Acechador melenargenta"] = 22766,
        ["Emboscador Alabruja"] = 22766,
        ["Acechador Crines de Acero"] = 22766,
        ["Acechador descarriado"] = 22766,
        ["Acechasombras Putredor"] = 22766,
        ["Acechasombras Lobrecuore"] = 22766,
        ["Acechador del risco"] = 22766,
        ["Rondadora Niebla Negra"] = 22766,
        ["Rondador Colmilloumbrío"] = 22766,
        ["Pícaro Malarraíz"] = 22766,
        ["Acechasombras Malarraíz"] = 22766,
        ["Infiltrador de Theramore"] = 22766,
        ["Acechador Rajacieno"] = 22766,
    },
    ["Alivio presto"] = {
        ["DRUID"] = 18562,
    },
    ["Pantalones de cuero ligero"] = {
        Professions = 9068,
    },
    ["Sello de sabiduría"] = {
        ["PALADIN"] = 20357,
    },
    ["Renovar"] = {
        ["PRIEST"] = 25315,
        ["Sirena Látigo de Ira"] = 11640,
        ["Médico Negador"] = 22168,
        ["Sacerdote de Dun Garok"] = 8362,
        ["Narillasanz"] = 8362,
        ["Discípulo Escarlata"] = 8362,
        ["Capellán Escarlata"] = 8362,
        ["Abad Escarlata"] = 8362,
        ["Acólito Caramuerte"] = 8362,
        ["Acólito Crepuscular"] = 8362,
        ["Oráculo Cegaluz"] = 8362,
        ["Sacerdote expedicionario"] = 8362,
    },
    ["Peste febril"] = {
        ["Atal'ai momificado"] = 16186,
    },
    ["Tótem Piel de piedra"] = {
        ["SHAMAN"] = 10408,
    },
    ["Cohete, GRANDE AMARILLO"] = {
        ["Fuegos artificiales de Pat GRAN AMARILLO"] = 26356,
    },
    ["Cinturón de cuero oscuro"] = {
        Professions = 3766,
    },
    ["Resguardo contra la Escarcha"] = {
        ["MAGE"] = 28609,
    },
    ["Consagración"] = {
        ["PALADIN"] = 20924,
    },
    ["Temblor mental"] = {
        ["Rompesuelos de Rajacieno"] = 8272,
    },
    ["Tótem Corriente de sanación"] = {
        ["SHAMAN"] = 10463,
    },
    ["Pantalones rúnicos de cobre"] = {
        Professions = 3324,
    },
    ["Crear gema de alma"] = {
        ["Kin'weelay"] = 3660,
    },
    ["Guanteletes rúnicos de cobre"] = {
        Professions = 3323,
    },
    ["Quemadura de alma"] = {
        ["Señor del fuego"] = 19393,
    },
    ["Toga de seda carmesí"] = {
        Professions = 8802,
    },
    ["Encantar capa: resistencia"] = {
        Professions = 13794,
    },
    ["Invocar espíritu de lo antiguo"] = {
        ["Espíritu de lo antiguo"] = 3652,
        ["Teurgista Dalaran"] = 3652,
    },
    ["Poción de acción libre"] = {
        Professions = 6624,
    },
    ["Poción de escudo de piedra inferior"] = {
        Professions = 4942,
    },
    ["Rezo de Protección contra las Sombras"] = {
        ["PRIEST"] = 27683,
    },
    ["Podredumbre negra"] = {
        ["Rata infestada"] = 16448,
    },
    ["Invocar engendro de Bael'Gar"] = {
        ["Alevín de Bael'Gar"] = 13895,
    },
    ["Maldición del Ojo"] = {
        ["Marinero maldito"] = 3360,
        ["Marino maldito"] = 3360,
        ["Primer oficial Esnelim"] = 3360,
        ["Capitán Halyndor"] = 3360,
        ["Marinero maldito"] = 10653,
        ["Marino maldito"] = 10653,
        ["Primer oficial Esnelim"] = 10653,
        ["Capitán Halyndor"] = 10653,
        ["Marinero maldito"] = 10651,
        ["Marino maldito"] = 10651,
        ["Primer oficial Esnelim"] = 10651,
        ["Capitán Halyndor"] = 10651,
    },
    ["Brasa guardiana del Hombre de Mimbre"] = {
        ["Guardián mimbroso"] = 25007,
    },
    ["Capa de lino"] = {
        Professions = 2387,
    },
    ["Levitar"] = {
        ["PRIEST"] = 1706,
    },
    ["Espíritu de Kirith"] = {
        ["Espíritu de Kirith"] = 10853,
        ["Kirith el Maldito"] = 10853,
    },
    ["Bendición de Thule"] = {
        ["Esclavo Corvozarpa"] = 3269,
    },
    ["Encantar arma: golpear menor"] = {
        Professions = 7788,
    },
    ["Disparo de mortero"] = {
        ["Enanín"] = 16786,
    },
    ["Tósigo de piel venenosa"] = {
        ["Ravasauro"] = 14792,
    },
    ["Encantar arma 2M: impacto"] = {
        Professions = 13695,
    },
    ["Bomba Hierro Negro"] = {
        Professions = 19799,
    },
    ["Invocar hiena domesticada"] = {
        ["Hiena domada"] = 8276,
        ["Maestro de bestias Rajacieno"] = 8276,
    },
    ["Dinamita potente"] = {
        Professions = 3946,
    },
    ["Armadura natural"] = {
        ["HUNTER"] = 24632,
    },
    ["Peste necrófaga"] = {
        ["Necrófago de peste"] = 16458,
    },
    ["Resucitar soldado"] = {
        ["Demetria"] = 19721,
        ["Tropista Escarlata"] = 19721,
    },
    ["Atronar"] = {
        ["WARRIOR"] = 11581,
        ["Halcón Trueno superior"] = 8078,
        ["Desechanube Halcón Trueno"] = 8078,
        ["Tormentabisal enfurecido"] = 8078,
        ["Guardia de Forjaz"] = 8078,
        ["Señor de la Guerra Kolkanis"] = 8078,
        ["Torek"] = 8078,
        ["Belicista Mosh'Ogg"] = 8147,
        ["Tronarisco"] = 8147,
        ["Umi Thorson"] = 8147,
        ["Señor Supremo Colmicarnero"] = 15548,
        ["Jabaguardia Rajacieno"] = 15548,
        ["Vanndar Pico Tormenta"] = 15588,
    },
    ["Enlace de alma"] = {
        ["WARLOCK"] = 19028,
    },
    ["Ira de Arantir"] = {
        ["Arantir"] = 9741,
    },
    ["Manteo de lana"] = {
        Professions = 2402,
    },
    ["Petrificar"] = {
        ["Petrificador Cristaldermo"] = 11020,
    },
    ["Guarda de Zanzil"] = {
        ["Guardia de Zanzil"] = 8832,
        ["Zanzil el Desarraigado"] = 8832,
    },
    ["Sello de orden"] = {
        ["PALADIN"] = 20920,
    },
    ["Transmutar: tierra a vida"] = {
        Professions = 17566,
    },
    ["Cinturón radiante"] = {
        Professions = 16645,
    },
    ["Guantes de cuero oscuro"] = {
        Professions = 3765,
    },
    ["Invocar habitante del pantano"] = {
        ["Habitante del cubil"] = 8857,
        ["Señor del cubil"] = 8857,
    },
    ["Resistir al Arcano"] = {
        ["Médico Escarlata"] = 17175,
    },
    ["Fuerza Peloescarcha"] = {
        ["Grik'nir el Frío"] = 6957,
    },
    ["Elixir de defensa excelente"] = {
        Professions = 17554,
    },
    ["Piel de demonio"] = {
        ["WARLOCK"] = 696,
        ["Nigromante Peloescarcha"] = 20798,
        ["Gazz'uz"] = 20798,
    },
    ["Despedazamiento mental"] = {
        ["Doctor Weavil"] = 25774,
    },
    ["Aliados de VanCleef"] = {
        ["Guardanegro Defias"] = 5200,
        ["Edwin VanCleef"] = 5200,
    },
    ["Gran hacha de guerra"] = {
        Professions = 3294,
    },
    ["Piedra de afilar sólida"] = {
        Professions = 9918,
    },
    ["Tótem Muro de viento"] = {
        ["SHAMAN"] = 15112,
    },
    ["Frenesí de Windsor"] = {
        ["Alguacil Windsor"] = 15167,
    },
    ["Vestido de boda blanco"] = {
        Professions = 12091,
    },
    ["Cinta de cuero maligno"] = {
        Professions = 19071,
    },
    ["Aceite de zahorí"] = {
        Professions = 25128,
    },
    ["Sanación inferior"] = {
        ["PRIEST"] = 2053,
    },
    ["Invocar a Hakkar"] = {
        ["Sombra de Hakkar"] = 12639,
    },
    ["Tripulación borracha del foso"] = {
        ["Operario de fosa goblin"] = 20436,
        ["Operario de fosa gnomo"] = 20436,
        ["Encargado de fosa gnomo"] = 20436,
        ["Encargado de fosa goblin"] = 20436,
    },
    ["Contrapeso pesado"] = {
        Professions = 3117,
    },
    ["Toxina alterada de la caldera"] = {
        ["Granjero Dalson"] = 17650,
    },
    ["Magulladura"] = {
        ["Truhán Filo Ardiente"] = 4134,
    },
    ["Sombrero de almirante"] = {
        Professions = 12081,
    },
    ["Crear barril de Weegli"] = {
        ["Weegli Plomofundido"] = 10772,
    },
    ["Aguardiente Nevada"] = {
        ["Vigilante del cubil Nevada"] = 17205,
        ["Totémico Nevada"] = 17205,
        ["Abrecaminos Nevada"] = 17205,
    },
    ["Fingir muerte"] = {
        ["HUNTER"] = 5384,
    },
    ["Sonda mental"] = {
        ["Infiltrador Escudo del Estigma"] = 16037,
    },
    ["Gracia de Elune"] = {
        ["PRIEST"] = 19293,
    },
    ["Carga de zapador goblin"] = {
        Professions = 12760,
    },
    ["Leotardos de bronce férreos"] = {
        Professions = 2668,
    },
    ["Amplificar llamas"] = {
        ["Tejeoscuro Forjatiniebla"] = 9482,
    },
    ["Jubón de cobre férreo"] = {
        Professions = 12260,
    },
    ["Palabra de poder: entereza"] = {
        ["PRIEST"] = 10938,
        ["Médico Negador"] = 13864,
    },
    ["Alas mágicas"] = {
        ["Activador de cañón de feria"] = 24742,
    },
    ["Piel de corteza"] = {
        ["DRUID"] = 22812,
    },
    ["Dominación vil"] = {
        ["WARLOCK"] = 18708,
    },
    ["Elixir de defensa superior"] = {
        Professions = 11450,
    },
    ["Avatar"] = {
        ["Vanndar Pico Tormenta"] = 19135,
    },
    ["Resistencia a lo Arcano"] = {
        ["HUNTER"] = 24510,
    },
    ["Fuerza de piedra"] = {
        ["Muevepiedras Sombramatorral"] = 6864,
    },
    ["Manto de los Fauces de Madera"] = {
        Professions = 23663,
    },
    ["Ceguera"] = {
        ["ROGUE"] = 2094,
    },
    ["Cinta de seda"] = {
        Professions = 8762,
    },
    ["Invocar exiliado centelleante"] = {
        ["Exiliado Prismático"] = 10388,
        ["Myzrael"] = 10388,
    },
    ["Llamar a huargo babeante"] = {
        ["Lupo esclavizante"] = 7488,
    },
    ["Sobrehombros de lana con doble pespunte"] = {
        Professions = 3848,
    },
    ["Teletransporte a: Darnassus"] = {
        ["MAGE"] = 3565,
    },
    ["Crear pergamino de Yeh'kinya"] = {
        ["Yeh'kinya"] = 12998,
    },
    ["Latigazo doloroso"] = {
        Pet = 11780,
    },
    ["Leotardos barbáricos"] = {
        Professions = 7149,
    },
    ["Incitar llamas"] = {
        ["Caminafuego"] = 19635,
    },
    ["Portal: Entrañas"] = {
        ["MAGE"] = 11418,
    },
    ["Camisa azul con estilo"] = {
        Professions = 7892,
    },
    ["Descarga tóxica"] = {
        ["Marea apestada"] = 16554,
    },
    ["Coraza rúnica de cobre"] = {
        Professions = 2667,
    },
    ["Botas de cuero cosidas a mano"] = {
        Professions = 2149,
    },
    ["Vestido negro sencillo"] = {
        Professions = 12077,
    },
    ["Forma acuática"] = {
        ["DRUID"] = 1066,
    },
    ["Aliviar mascota"] = {
        ["HUNTER"] = 13544,
    },
    ["Renacer"] = {
        ["DRUID"] = 20748,
    },
    ["Detonación zapadora"] = {
        ["Saboteador Hierro Negro"] = 3204,
        ["Zapador Hierro Negro"] = 3204,
    },
    ["Guantes inferno"] = {
        Professions = 22868,
    },
    ["Generador de escudo"] = {
        ["Tonque de vapor"] = 27759,
    },
    ["Cinta Nochefugaz"] = {
        Professions = 10507,
    },
    ["Espíritu erguido"] = {
        ["Quebrantadientes"] = 10767,
    },
    ["Chispa"] = {
        ["Duende Ley"] = 21027,
        ["Duende de maná"] = 21027,
    },
    ["Bomba cegadora"] = {
        Professions = 8243,
    },
    ["Resistencia a las Sombras"] = {
        ["HUNTER"] = 24516,
    },
    ["Loriga de bronce férrea"] = {
        Professions = 2670,
    },
    ["Maleficio de debilidad"] = {
        ["PRIEST"] = 19285,
    },
    ["Vara de veraplata"] = {
        Professions = 14380,
    },
    ["Anular maná"] = {
        ["Dragón clarividente"] = 7994,
    },
    ["Recolectar hierbas"] = {
        Professions = 2368,
        Professions = 3570,
        Professions = 11993,
    },
    ["Vudú"] = {
        ["Bom'bay"] = 17009,
    },
    ["Reconstrucción"] = {
        ["Tor'gan"] = 4093,
    },
    ["Disparo de veneno"] = {
        ["Maestro de bestias Rajacieno"] = 8275,
        ["Cazador de las Sombras Kurzen"] = 8806,
        ["Cazador Lomopelo"] = 8806,
    },
    ["Bloquear con escudo"] = {
        ["WARRIOR"] = 2565,
        ["Guardia de Ventormenta"] = 12169,
        ["Guardaespaldas Escarlata"] = 12169,
        ["Argos Piroleño"] = 12169,
        ["Defensor espectral"] = 12169,
        ["Defensor de Pico Tormenta"] = 12169,
        ["Guardián Lobo Gélido"] = 12169,
        ["Aggi Piesdeplomo"] = 12169,
        ["Defensor de estación"] = 12169,
        ["Guardián de estación"] = 12169,
        ["Defensor veterano"] = 12169,
        ["Guardián veterano"] = 12169,
        ["Guardián Campeón"] = 12169,
        ["Defensor Campeón"] = 12169,
        ["Cabo Noreg Pico Tormenta"] = 12169,
    },
    ["Gafas tintadas de rosa"] = {
        Professions = 12618,
    },
    ["Pisotón de guerra"] = {
        Racial = 20549,
        ["Caminarisco"] = 11876,
        ["Protector deambulante"] = 45,
        ["Harb Montediendo"] = 45,
    },
    ["Abrellamas"] = {
        ["Evocador Hacha de Sangre"] = 15743,
        ["Volchan"] = 15743,
    },
    ["Jubón de tela lunar"] = {
        Professions = 18447,
    },
    ["Peste vagante"] = {
        ["Propagador de la Peste"] = 3436,
        ["Hiena Pestillejo"] = 3436,
    },
    ["Explosión de relámpagos"] = {
        ["Bruja de mar Colafuria"] = 8598,
    },
    ["Pisotón"] = {
        ["Gólem de guerra pesado"] = 12612,
    },
    ["Tótem Sacudida de llamas"] = {
        ["Tótem del Manjar de Fuego"] = 15867,
        ["Médico Brujo Espina Ahumada"] = 15867,
    },
    ["Elixir de resistencia al veneno"] = {
        Professions = 3174,
    },
    ["Llama de alma"] = {
        ["Nelson el Amable"] = 23272,
    },
    ["Guardias terráneos destruidos"] = {
        ["Guardián de tierra"] = 10666,
        ["Formajol de tierra"] = 10666,
        ["Custodio de tierra"] = 10666,
        ["Guarda de la cámara acorazada"] = 10666,
    },
    ["Picadura de escórpido"] = {
        ["HUNTER"] = 14277,
        ["Avanzado de Vallefresno"] = 18545,
    },
    ["Pantalones de mitril pesado"] = {
        Professions = 9933,
    },
    ["Guerrera de cuero oscuro"] = {
        Professions = 2169,
    },
    ["Guantes de seda carmesíes"] = {
        Professions = 8804,
    },
    ["Fuerza de Arko'narin"] = {
        ["Arko'narin capturado"] = 18163,
    },
    ["Machaque arqueado"] = {
        ["Atracador Filo Ardiente"] = 8374,
        ["Atracador Crepuscular"] = 8374,
        ["Atracador Cueva Honda"] = 8374,
    },
    ["Crear quintaesencia eterna"] = {
        ["Duque Hydraxis"] = 28439,
    },
    ["Agilidad de Agamaggan"] = {
        ["Quebrantadientes"] = 17013,
    },
    ["Golpes de barrido"] = {
        ["WARRIOR"] = 12292,
        ["Constructo resucitado"] = 18765,
        ["Drek'Thar"] = 18765,
    },
    ["Sobrehombros barbáricos"] = {
        Professions = 7151,
    },
    ["Piedra de afilar burda"] = {
        Professions = 2665,
    },
    ["Trasto de torio"] = {
        Professions = 19791,
    },
    ["Peletería"] = {
        Professions = 3104,
        Professions = 3811,
        Professions = 10662,
    },
    ["Retirar mascota"] = {
        ["HUNTER"] = 2641,
    },
    ["Aura de celeridad"] = {
        ["Sobrestante de Yunque Colérico"] = 13589,
        ["Sobrestante Mostacho Blanco"] = 13589,
    },
    ["Poción de sanación sublime"] = {
        Professions = 17556,
    },
    ["Rezo de entereza"] = {
        ["PRIEST"] = 21564,
    },
    ["Vara de arcanita"] = {
        Professions = 20201,
    },
    ["Crear piedra de hechizo (superior)"] = {
        ["WARLOCK"] = 17727,
    },
    ["Contraataque"] = {
        ["HUNTER"] = 20910,
    },
    ["Maldición de Shahram"] = {
        ["Shahram"] = 16597,
    },
    ["Hongos Mirkfallon"] = {
        ["Vigilante Mirkfallon"] = 8138,
    },
    ["Pantalones de cuero refinado"] = {
        Professions = 7133,
    },
    ["Leotardos quiméricos"] = {
        Professions = 19073,
    },
    ["Toque de Vaelastrasz"] = {
        ["Vaelastrasz el Rojo"] = 16319,
    },
    ["Toque de sanación"] = {
        ["DRUID"] = 25297,
    },
    ["Traslación"] = {
        ["MAGE"] = 1953,
    },
    ["Ofuscamiento"] = {
        ["Almanegra Colmillo Oscuro"] = 8140,
        ["Aspecto de banalidad"] = 8140,
    },
    ["Capa de seda azur"] = {
        Professions = 8786,
    },
    ["Chirrido aterrorizador"] = {
        ["Cantor de la Muerte"] = 6605,
        ["Murcipeste"] = 6605,
        ["Pterrordáctilo"] = 6605,
        ["Rastreasueños"] = 6605,
    },
    ["Poción de Protección contra lo Sagrado"] = {
        Professions = 7255,
    },
    ["Raíces enredadoras"] = {
        ["DRUID"] = 9853,
        ["Trepador del Lago viejo"] = 11922,
        ["Tejespina Rajacieno"] = 11922,
        ["Protector deambulante"] = 11922,
        ["Archidruida Renferal"] = 22127,
        ["Druida de la Alameda"] = 22127,
        ["Tejespina Lomopelo"] = 12747,
        ["Formadera Sombramatorral"] = 12747,
        ["Anciano carbonizado"] = 12747,
        ["Vigilante Ordanus"] = 12747,
        ["Señor Tar"] = 12747,
    },
    ["Poción de sangre de trol sublime"] = {
        Professions = 24368,
    },
    ["Atenuar magia"] = {
        ["MAGE"] = 10174,
    },
    ["Guantes radiantes"] = {
        Professions = 16654,
    },
    ["Invocar cráneo humano"] = {
        ["Cráneo humano"] = 19031,
    },
    ["Toga de tejido de sombra"] = {
        Professions = 12055,
    },
    ["Rayo mortal gnómico"] = {
        Professions = 12759,
    },
    ["Sobrehombros Nochefugaz"] = {
        Professions = 10516,
    },
    ["Golpe mortal"] = {
        ["WARRIOR"] = 21553,
        ["Teniente Rugba"] = 15708,
        ["Teniente Spencer"] = 15708,
        ["Teniente Pezuñaforte"] = 15708,
        ["Teniente Vol'talar"] = 15708,
        ["Teniente Grummus"] = 15708,
        ["Teniente Murp"] = 15708,
        ["Teniente Lewis"] = 15708,
        ["Teniente Largent"] = 15708,
        ["Teniente Durasa"] = 15708,
        ["Teniente Greywand"] = 15708,
        ["Teniente Lonadin"] = 15708,
        ["Guerrero Strashaz"] = 16856,
        ["Destripador Hederine"] = 16856,
        ["Capitán Galvangar"] = 16856,
        ["Guardia Ferrohondo"] = 16856,
        ["Guardia veterano de Ferrohondo"] = 16856,
        ["Guardia Campeón Ferrohondo"] = 16856,
        ["Comandante de jinetes de carneros de Pico Tormenta"] = 16856,
        ["Jotek"] = 16856,
    },
    ["Guanteletes del mar"] = {
        Professions = 10630,
    },
    ["Leotardos vivos"] = {
        Professions = 19078,
    },
    ["Dormir"] = {
        ["Vermis Caminasueños"] = 15970,
        ["Conjurador del Sindicato"] = 15970,
        ["Sacerdotisa Colafuria"] = 15970,
        ["Durmiente amputado"] = 8399,
        ["Alto Inquisidor Ribalimpia"] = 8399,
        ["Señor Crepuscular Kelris"] = 8399,
        ["Somnus"] = 20989,
    },
    ["Muñeco-diana avanzado"] = {
        Professions = 3965,
    },
    ["Mente envenenada"] = {
        ["Esclavizador Colmen'Regal"] = 19469,
    },
    ["Cohete azul grande"] = {
        Professions = 26420,
    },
    ["Encantar arma: arma ígnea"] = {
        Professions = 13898,
    },
    ["Bomba de bronce pequeña"] = {
        Professions = 3941,
    },
    ["Hacha de mano de torio ornamentada"] = {
        Professions = 16969,
    },
    ["Jubón de lino barbárico"] = {
        Professions = 2395,
    },
    ["Aceite de maná inferior"] = {
        Professions = 25127,
    },
    ["Aliento de ácido"] = {
        ["Jade"] = 12533,
        ["Lord Capitán Wyrmak"] = 12533,
    },
    ["Coraza de demonio forjada"] = {
        Professions = 16667,
    },
    ["Puntos de honor +138"] = {
        ["Belisario de la Horda"] = 24962,
        ["Brigadier general de la Alianza"] = 24962,
    },
    ["Sobrehombros plumahierro"] = {
        Professions = 19062,
    },
    ["Mirada de sapta"] = {
        ["Tiev Mordune"] = 9735,
    },
    ["Poción de amor de Nagmara"] = {
        ["Maestra Nagmara"] = 14928,
    },
    ["Togas de Arcana"] = {
        Professions = 6692,
    },
    ["Escamas de pez lustrosas"] = {
        ["SHAMAN"] = 17057,
    },
    ["Arpón envenenado"] = {
        ["Capitán Verdepel"] = 5208,
    },
    ["Exorcismo"] = {
        ["PALADIN"] = 10314,
    },
    ["Tormento"] = {
        Pet = 11775,
    },
    ["Encantar brazales: fuerza excelente"] = {
        Professions = 20010,
    },
    ["Encantar brazales: espíritu menor"] = {
        Professions = 7766,
    },
    ["Poción de sanación excelente"] = {
        Professions = 11457,
    },
    ["Botas cohete gnómicas"] = {
        Professions = 12905,
    },
    ["Picadura fatal"] = {
        ["Destripador de la cripta"] = 17170,
        ["Krellack"] = 17170,
    },
    ["Teletransporte a: Entrañas"] = {
        ["MAGE"] = 3563,
    },
    ["Forma de zombi"] = {
        ["Infiltrador Marksen"] = 7293,
    },
    ["Ahuyentar a no-muerto"] = {
        ["PALADIN"] = 5627,
    },
    ["Luminosidad Arcana"] = {
        ["MAGE"] = 23028,
    },
    ["Modulador de amplificación de voz"] = {
        Professions = 19819,
    },
    ["Animar mortero"] = {
        ["Enanín"] = 18655,
    },
    ["Rajar"] = {
        ["WARRIOR"] = 20569,
        ["Tallamadera goblin"] = 5532,
        ["Capitán Verdepel"] = 5532,
        ["Leñador de Ventura y Cía."] = 5532,
        ["Atracador escórpido"] = 5532,
        ["Ogglesílex"] = 5532,
        ["Mirmidón Strashaz"] = 15754,
        ["Destripador Hederine"] = 15754,
        ["Capitán Galvangar"] = 15754,
        ["Herod"] = 22540,
        ["Alto Señor Bolvar Fordragon"] = 20684,
        ["Horrocrusto verde"] = 15496,
        ["Élite Kurzen"] = 15496,
        ["Asaltador Aplastacresta"] = 15496,
        ["Bruto de Orgrimmar"] = 15496,
        ["Campeón caído"] = 15496,
        ["Atracador Tótem Siniestro"] = 15496,
        ["Lord Kragaru"] = 15496,
        ["Élite Kor'kron"] = 15496,
        ["Atracador marchito"] = 11427,
        ["Teremus el Devorador"] = 11427,
        ["Alguacil Sangrehielo"] = 15579,
        ["Alguacil de Punta de la Torre"] = 15579,
        ["Alguacil del este Lobo Gélido"] = 15579,
        ["Mariscal Lobo Gélido"] = 15579,
        ["Maestro de guerra del norte de Dun Baldar"] = 15579,
        ["Maestro de guerra del sur de Dun Baldar"] = 15579,
        ["Maestro de guerra Alahielo"] = 15579,
        ["Maestro de guerra Corapernal"] = 15579,
        ["Maestro de guerra del oeste Lobo Gélido"] = 15579,
        ["Sobrestante Mostacho Blanco"] = 15663,
        ["Guardia Ferrohondo"] = 15663,
        ["Comandante Randolf"] = 15663,
        ["Comandante Dardosh"] = 15663,
        ["Comandante Malgor"] = 15663,
        ["Comandante Mulfort"] = 15663,
        ["Comandante Luis Filis"] = 15663,
        ["Comandante Mortimer"] = 15663,
        ["Comandante Duffy"] = 15663,
        ["Comandante Karl Filis"] = 15663,
        ["Guardia Ferrohondo estacional"] = 15663,
        ["Guardia veterano de Ferrohondo"] = 15663,
        ["Guardia Campeón Ferrohondo"] = 15663,
        ["Comandante de jinetes de carneros de Pico Tormenta"] = 15663,
        ["Jotek"] = 15663,
    },
    ["Guantes de lana gruesos"] = {
        Professions = 3843,
    },
    ["Arma Lengua de Fuego"] = {
        ["SHAMAN"] = 16342,
    },
    ["Receta de combustible de cohete goblin"] = {
        Professions = 12715,
    },
    ["Cohete, AZUL"] = {
        ["Explosión de artificios de Pat AZUL"] = 26344,
    },
    ["Poción de Protección contra las Sombras superior"] = {
        Professions = 17578,
    },
    ["Efecto de sufrimiento de Taelan"] = {
        ["Alto señor Taelan Vadín"] = 18811,
    },
    ["Paso lateral presuroso"] = {
        ["Correspuma escamado"] = 5426,
    },
    ["Flema de ácido"] = {
        ["Cría adolescente"] = 20821,
        ["Cría dormida"] = 20821,
    },
    ["Prendas de lana de oso de guerra"] = {
        Professions = 19080,
    },
    ["Cohete azul pequeño"] = {
        Professions = 26416,
    },
    ["Bendición de salvación"] = {
        ["PALADIN"] = 1038,
    },
    ["Toga de adepto superior"] = {
        Professions = 7643,
    },
    ["Porrazo"] = {
        ["ROGUE"] = 11297,
    },
    ["Babosa enfermiza"] = {
        ["Babosa putrefacta"] = 6907,
        ["Moco del bosque"] = 6907,
    },
    ["Botas de tejido mágico negras"] = {
        Professions = 12073,
    },
    ["Cólera Sagrada"] = {
        ["PALADIN"] = 10318,
    },
    ["Poder Arcano"] = {
        ["MAGE"] = 12042,
    },
    ["Trampa explosiva"] = {
        ["HUNTER"] = 14317,
    },
    ["Botas de lana roja"] = {
        Professions = 3847,
    },
    ["Placa Hierro Negro"] = {
        Professions = 15296,
    },
    ["Lágrima de Razelikh II"] = {
        ["Razelikh el Rapiñador"] = 10866,
    },
    ["Ojo de Kilrogg"] = {
        ["WARLOCK"] = 126,
    },
    ["Contrapeso de hierro"] = {
        Professions = 7222,
    },
    ["Invocar espíritu de Piemartillo"] = {
        ["Espíritu de Dedomaza"] = 4985,
        ["Karnik el Historiador"] = 4985,
    },
    ["Caída lenta"] = {
        ["MAGE"] = 130,
    },
    ["Capa de paño rúnico"] = {
        Professions = 18409,
    },
    ["Barrera de relámpagos"] = {
        ["Hermana Azote de Odio"] = 6960,
    },
    ["Fogonazo"] = {
        ["MAGE"] = 10216,
        ["Evocador Defias"] = 11829,
        ["Profeta Filo Ardiente"] = 11829,
        ["Tejefuego Puñastilla"] = 20296,
        ["Brasillas"] = 12468,
        ["Geomántico Lomopelo"] = 20794,
        ["Geomántico Tótem Siniestro"] = 20813,
    },
    ["Semillas de arce"] = {
        ["DRUID"] = 17034,
    },
    ["Bendición de Shahram"] = {
        ["Shahram"] = 16599,
    },
    ["Veneno paralizante"] = {
        ["Parloteador"] = 3609,
        ["Ermitaño carroñero"] = 3609,
        ["Timador Mala Hierba"] = 3609,
        ["Ayudante de sangre de Kirtonos"] = 3609,
        ["Pinzamorten"] = 3609,
    },
    ["Vara de plata"] = {
        Professions = 7818,
    },
    ["Camisa de tejido mágico rosa"] = {
        Professions = 12080,
    },
    ["Encantar escudo: espíritu excelente"] = {
        Professions = 20016,
    },
    ["Crear invitación al Festival Lunar"] = {
        ["Presagista del Festival Lunar"] = 26375,
    },
    ["Pellejo ligero curtido"] = {
        Professions = 3816,
    },
    ["Fortuna Lunar"] = {
        ["Tipo de los fuegos artificiales de Pat (ELUNE)"] = 26522,
    },
    ["Traca de cohetes rojos grandes"] = {
        Professions = 26428,
    },
    ["Graznido de batalla"] = {
        ["Gallo de batalla"] = 23060,
    },
    ["Modulador de cobre"] = {
        Professions = 3926,
    },
    ["Contagio de putrefacción"] = {
        ["Cuerpo fétido"] = 7102,
        ["Herborista Renegado"] = 7102,
    },
    ["Jubón de seda azur"] = {
        Professions = 3859,
    },
    ["Encantar brazales: aguante menor"] = {
        Professions = 7457,
    },
    ["Somnolencia cristalina"] = {
        ["Basilisco Ojogélido"] = 3636,
        ["Basilisco desdeñado"] = 3636,
        ["Basilisco chamuscado"] = 3636,
        ["Basilisco ennegrecido"] = 3636,
        ["Basilisco Gapo Salino"] = 3636,
        ["Basilisco Gritfauces"] = 3636,
        ["Basilisco gritfauces descomunal"] = 3636,
        ["Basilisco Dorsaljade"] = 3636,
    },
    ["Escudo Sagrado"] = {
        ["PALADIN"] = 20928,
    },
    ["Enfoque interno"] = {
        ["PRIEST"] = 14751,
    },
    ["Invocar efigie de Gunther"] = {
        ["Cara de Gunther"] = 7762,
    },
    ["Gran aguante"] = {
        ["HUNTER"] = 5049,
    },
    ["Almófar de mitril"] = {
        Professions = 9961,
    },
    ["Dejado a muerte"] = {
        ["Agathelos el Furioso"] = 8555,
    },
    ["Nova de escalofrío"] = {
        ["Ras Levescarcha"] = 18099,
    },
    ["Ola explosiva"] = {
        ["MAGE"] = 13021,
    },
    ["Chillido de alma en pena"] = {
        ["Espíritu Eldreth"] = 16838,
    },
    ["Poción de maná superior"] = {
        Professions = 11448,
    },
    ["Remolino de telaraña"] = {
        ["Proleviuda Razzashi"] = 24600,
    },
    ["Fatalidad demoníaca"] = {
        ["Artorius el Afable"] = 23298,
    },
    ["Cono de Fuego"] = {
        ["Guardia llamarada"] = 19630,
    },
    ["Disparo de dispersión"] = {
        ["HUNTER"] = 19503,
    },
    ["Rastrear no-muertos"] = {
        ["HUNTER"] = 19884,
    },
    ["Proyectiles de torio"] = {
        Professions = 19800,
    },
    ["Encantar guantes: minería avanzada"] = {
        Professions = 13841,
    },
    ["Piel de roca"] = {
        ["Profeta Plumacuervo"] = 8314,
        ["Kranal Fiss"] = 8314,
    },
    ["Represalias"] = {
        ["WARRIOR"] = 20230,
    },
    ["Veneno ralentizante"] = {
        ["Reptanoches múrloc"] = 7992,
        ["Filonocturno Defias"] = 7992,
        ["Asaltante esquelético"] = 7992,
        ["Malapluma Alabruja"] = 7992,
        ["Asesino Renegado"] = 7992,
        ["Trepador Colmilloumbrío"] = 7992,
        ["Amigo de Friend"] = 7992,
        ["Cazador escórpido"] = 7992,
    },
    ["Invocar pesadilla ilusoria"] = {
        ["Pesadilla ilusorio"] = 6905,
        ["Soñador amputado"] = 6905,
    },
    ["Brazales de escamas doradas"] = {
        Professions = 7223,
    },
    ["Picadura de víbora"] = {
        ["HUNTER"] = 14280,
    },
    ["Disparo automático"] = {
        ["HUNTER"] = 75,
    },
    ["Nauseabundo"] = {
        ["Gallina"] = 24919,
        ["Guardia de Costasur"] = 24919,
        ["Granjero Kent"] = 24919,
        ["Phin Odelic"] = 24919,
        ["Sargento Hartman"] = 24919,
    },
    ["Aceite de zahorí luminoso"] = {
        Professions = 25129,
    },
    ["Grito desmoralizador"] = {
        ["WARRIOR"] = 11556,
    },
    ["Rabia animal"] = {
        ["Gnoll Pielsombra rabioso"] = 3150,
        ["Lobo feroz rabioso"] = 3150,
        ["Maderero"] = 3150,
        ["Oso Cardo rabioso"] = 3150,
        ["Coyote del risco rabioso"] = 3150,
        ["Zarpósea rabiosa"] = 3150,
        ["Dientelargo rabioso"] = 3150,
        ["Llagapata rabiosa"] = 3150,
        ["Dentoesquirla rabioso"] = 3150,
    },
    ["Elixir de respiración acuática"] = {
        Professions = 7179,
    },
    ["Bloqueo de hechizo"] = {
        Pet = 19647,
    },
    ["Yelmo de placas imperiales"] = {
        Professions = 16658,
    },
    ["Beso calmante"] = {
        Pet = 11785,
    },
    ["Deflector de hielo"] = {
        Professions = 3957,
    },
    ["Ola de relámpagos"] = {
        ["Ysondre"] = 24819,
    },
    ["Desgarre de alma"] = {
        ["Alapenas"] = 3405,
    },
    ["Ataque con bomba de aturdimiento"] = {
        ["Grifo nidal"] = 21188,
        ["Jinete de guerra"] = 21188,
        ["Jinete de guerra de Guse"] = 21188,
        ["Jinete de guerra de Jeztor"] = 21188,
        ["Jinete de guerra Mulverick"] = 21188,
        ["Grifo de Slidore"] = 21188,
        ["Grifo de Ichman"] = 21188,
        ["Grifo de Vipore"] = 21188,
    },
    ["Encantar arma: golpear"] = {
        Professions = 13693,
    },
    ["Ventisca"] = {
        ["MAGE"] = 10187,
        ["Lokholar el Señor del Hielo"] = 21367,
    },
    ["Bendición de reyes"] = {
        ["PALADIN"] = 20217,
    },
    ["Invocar zángano Colmen'Ashi"] = {
        ["Zángano Colmen'Ashi"] = 21327,
        ["Druida torturado"] = 21327,
        ["Centinela torturado"] = 21327,
    },
    ["Invocar ráfaga de lava E"] = {
        ["Rata del Núcleo"] = 21903,
    },
    ["Guanteletes de cuero maligno"] = {
        Professions = 19049,
    },
    ["Trauma por aguijón"] = {
        ["Artorius el Afable"] = 23299,
    },
    ["Jubón de cuero cosido a mano"] = {
        Professions = 7126,
    },
    ["Pantalones Fénix"] = {
        Professions = 3851,
    },
    ["Botas de lino sencillas"] = {
        Professions = 12045,
    },
    ["Bendición de Luz"] = {
        ["PALADIN"] = 19979,
    },
    ["Poción de maná"] = {
        Professions = 3452,
    },
    ["Terremoto de Myzrael"] = {
        ["Myzrael"] = 4938,
    },
    ["Encadenar no-muerto"] = {
        ["PRIEST"] = 10955,
    },
    ["Fragmentos estelares"] = {
        ["PRIEST"] = 19305,
    },
    ["Aceite de escama pétrea"] = {
        Professions = 17551,
    },
    ["Limpieza serpenteante"] = {
        ["Discípulo de Naralex"] = 6270,
    },
    ["Encontrar tesoro"] = {
        Racial = 2481,
    },
    ["Palabra de las Sombras: dolor"] = {
        ["PRIEST"] = 10894,
        ["Xabraxxis"] = 11639,
        ["Vol'jin"] = 17146,
        ["Sirena Strashaz"] = 15654,
        ["Supervisor Minafría"] = 15654,
        ["Perito Ferrohondo"] = 15654,
        ["Perito Minafría veterano"] = 15654,
        ["Perito veterano de Ferrohondo"] = 15654,
        ["Supervisor Campeón Ferrohondo"] = 15654,
    },
    ["Ojos de la bestia"] = {
        ["HUNTER"] = 1002,
    },
    ["Brazales de torio"] = {
        Professions = 16644,
    },
    ["Vientos envolventes"] = {
        ["Sirena Espinadaga"] = 6728,
        ["Clamavientos Alabruja"] = 6728,
        ["Clamavientos Furia Sangrienta"] = 6728,
        ["Cazavientos Galak"] = 6728,
        ["Tejepolvo Rajacieno"] = 6728,
        ["Cazavientos Kolkar"] = 6728,
        ["Hermana Rathtalon"] = 6728,
    },
    ["Tela lunar"] = {
        Professions = 18560,
    },
    ["Dragonizo mecánico"] = {
        Professions = 3969,
    },
    ["Botas cohete goblin"] = {
        Professions = 8895,
    },
    ["Invocar elemental de agua"] = {
        ["Elemental de agua invocado"] = 17162,
        ["Conjurador Carmesí"] = 17162,
    },
    ["Baba ácida"] = {
        ["Moco pegajoso"] = 14147,
    },
    ["Palabra de poder: escudo"] = {
        ["PRIEST"] = 10901,
        ["Alto Inquisidor Ribalimpia"] = 11647,
        ["Clérico de Nethergarde"] = 11974,
    },
    ["Maldición del Cuerno Sombrío"] = {
        ["Venado Necraste"] = 6922,
        ["Venado Necraste viejo"] = 6922,
    },
    ["Encantar arma: fuerza"] = {
        Professions = 23799,
    },
    ["Encantar escudo: espíritu inferior"] = {
        Professions = 13485,
    },
    ["Bendición de salvaguardia"] = {
        ["PALADIN"] = 20914,
    },
    ["Pisotón de trueno"] = {
        Pet = 26188,
    },
    ["Elementales de Sarilus"] = {
        ["Guardián de agua menor"] = 6490,
        ["Sarilus Fuenteviciosa"] = 6490,
    },
    ["Guanteletes de hierro verdes"] = {
        Professions = 3336,
    },
    ["Código de tropista"] = {
        ["Tropista Escarlata"] = 19749,
    },
    ["Varita mística inferior"] = {
        Professions = 14809,
    },
    ["Augurio de claridad"] = {
        ["DRUID"] = 16864,
    },
    ["Crear orbe de mago"] = {
        ["Tabetha"] = 9156,
    },
    ["Pólvora densa"] = {
        Professions = 19788,
    },
    ["Escudo de caparazón"] = {
        Pet = 26064,
    },
    ["Encantar brazales: espíritu"] = {
        Professions = 13642,
    },
    ["Entorpecer"] = {
        ["Rompedor del risco"] = 11443,
        ["Necromántico Caramuerte"] = 11443,
    },
    ["Brazales de placas imperiales"] = {
        Professions = 16649,
    },
    ["Leotardos Bengala del Núcleo"] = {
        Professions = 23667,
    },
    ["Maldición de los Elementos"] = {
        ["WARLOCK"] = 11722,
    },
    ["Don de lo Salvaje"] = {
        ["DRUID"] = 21850,
    },
    ["Guantes de cuero de montañero"] = {
        Professions = 3764,
    },
    ["Sello de rectitud"] = {
        ["PALADIN"] = 20293,
    },
    ["Elixir de agilidad superior"] = {
        Professions = 11467,
    },
    ["Invocar escarabajo carroñero"] = {
        ["Escarabajo carroñero"] = 17169,
        ["Horror de la cripta"] = 17169,
    },
    ["Agostar"] = {
        ["MAGE"] = 10207,
    },
    ["Llave esqueleto de veraplata"] = {
        Professions = 19668,
    },
    ["Cinturón de araña"] = {
        Professions = 3863,
    },
    ["Picadura de serpiente"] = {
        ["HUNTER"] = 25295,
    },
    ["Piedra de pulir densa"] = {
        Professions = 16639,
    },
    ["Pavor ancestral"] = {
        ["Can del Núcleo anciano"] = 19365,
    },
    ["Mira estándar"] = {
        Professions = 3978,
    },
    ["Capa de cuero estampado"] = {
        Professions = 2162,
    },
    ["Martillo de hierro poderoso"] = {
        Professions = 3297,
    },
    ["Elixir de poder de Fuego"] = {
        Professions = 7845,
    },
    ["Destierro de escama"] = {
        ["Lord Víctor Nefarius"] = 16404,
    },
    ["Carga feral"] = {
        ["DRUID"] = 16979,
    },
    ["Ira frenética"] = {
        ["Vigilante del cubil Piel de Cardo"] = 3490,
        ["Aku'mai"] = 3490,
    },
    ["Invocar hidrizo"] = {
        ["Hydrolinga"] = 22714,
    },
    ["Crear piedra de salud (superior)"] = {
        ["WARLOCK"] = 11729,
    },
    ["Azote"] = {
        ["DRUID"] = 8983,
    },
    ["Falda de colores"] = {
        Professions = 12047,
    },
    ["Toga de Noche invernal"] = {
        Professions = 18436,
    },
    ["Cuchillo de bronce grande"] = {
        Professions = 3491,
    },
    ["Veneno parasitario mortal"] = {
        ["Viuda sanguijuela"] = 3388,
        ["Alapenas"] = 3388,
    },
    ["Martillo de batalla encantado"] = {
        Professions = 16973,
    },
    ["Bocarda férrea"] = {
        Professions = 3925,
    },
    ["Forzar cerradura"] = {
        ["ROGUE"] = 1804,
    },
    ["Guantes de tela vil"] = {
        Professions = 22867,
    },
    ["Camisa amarillo brillante"] = {
        Professions = 3869,
    },
    ["Trampa congelante"] = {
        ["HUNTER"] = 14311,
    },
    ["Jubón de tejido onírico"] = {
        Professions = 12070,
    },
    ["Exponer armadura"] = {
        ["ROGUE"] = 11198,
    },
    ["Espada ancha de cobre pesada"] = {
        Professions = 3292,
    },
    ["Cosquilla de Warosh"] = {
        ["Warosh"] = 16771,
    },
    ["Salero"] = {
        Professions = 19567,
    },
    ["Mira rudimentaria"] = {
        Professions = 3977,
    },
    ["Salir de fase"] = {
        ["Gazban"] = 3648,
    },
    ["Decadencia espiritual"] = {
        ["Bastardo Zarparrío"] = 8016,
        ["Bastardo Pellejomusgo"] = 8016,
        ["Colazote solescama"] = 8016,
    },
    ["Invocar esqueleto"] = {
        ["Esqueletis"] = 20464,
        ["Lady Sylvanas Brisaveloz"] = 20464,
        ["Esqueletis"] = 8853,
        ["Guardahuesos Faucedraco"] = 8853,
    },
    ["Ráfaga de antorcha"] = {
        ["Vigilante del Sindicato"] = 3602,
        ["Jailor Eston"] = 3602,
        ["Excavador de la Facción Oscura"] = 3602,
    },
    ["Maldición de los Pirotigma"] = {
        ["Tejeoscuro Pirotigma"] = 16071,
        ["Tejedor de tienblas Pirotigma"] = 16071,
    },
    ["Botas barbáricas de hierro"] = {
        Professions = 9818,
    },
    ["Encantar pechera: maná sublime"] = {
        Professions = 20028,
    },
    ["Punta de escudo de mitril"] = {
        Professions = 9939,
    },
    ["Rifle Hierro Negro"] = {
        Professions = 19796,
    },
    ["Guantes de tejido de sombra"] = {
        Professions = 12071,
    },
    ["Rastrear lo oculto"] = {
        ["HUNTER"] = 19885,
    },
    ["Lanzador de fuegos artificiales"] = {
        Professions = 26442,
    },
    ["Carcaj rápido"] = {
        Professions = 14930,
    },
    ["Guanteletes de veraplata"] = {
        Professions = 9954,
    },
    ["Muñeco-diana - Evento 001"] = {
        ["Muñeco-diana del equipo mortero"] = 18634,
        ["Avistador Klemmy"] = 18634,
    },
    ["Capa de paño brillante"] = {
        Professions = 18420,
    },
    ["Invocar llama viva"] = {
        ["Llama viviente"] = 5110,
        ["Magiero Defias"] = 5110,
    },
    ["Cólera de Amnennar"] = {
        ["Amnennar el Gélido"] = 13009,
    },
    ["Poción de sanación menor"] = {
        Professions = 2330,
    },
    ["Arma Viento Furioso"] = {
        ["SHAMAN"] = 16362,
    },
    ["Encantar pechera: salud menor"] = {
        Professions = 7420,
    },
    ["Bolsa de tela vil"] = {
        Professions = 26086,
    },
    ["Ralentizar"] = {
        ["Hechicero Escarlata"] = 6146,
        ["Oráculo Filozante"] = 246,
        ["Lady Sarevess"] = 246,
        ["Cazatumbres Crepuscular"] = 18972,
        ["Capitán Balinda Corapernal"] = 19137,
        ["Místico Pellejomusgo"] = 11436,
        ["Geomántico Caramuerte"] = 11436,
        ["Geomántico Tótem Siniestro"] = 11436,
    },
    ["Faltriquera de almas"] = {
        Professions = 26085,
    },
    ["Rotura muscular"] = {
        ["Desgarrador mayor"] = 12166,
        ["Desgarrador"] = 12166,
        ["Sable de la Noche salvaje"] = 12166,
        ["Correarrecifes"] = 12166,
        ["Crocolisco Fauzatroz"] = 12166,
        ["Intruso Lomopelo"] = 12166,
        ["El Rastrillo"] = 12166,
    },
    ["Machaque"] = {
        ["Gigante fundido"] = 18944,
    },
    ["¡Zas!"] = {
        ["Milton Golpiza"] = 6754,
    },
    ["Campeón de veraplata"] = {
        Professions = 10015,
    },
    ["Jubón de tejido fantasmal"] = {
        Professions = 18416,
    },
    ["Aliviar dragón"] = {
        ["Domadragones Puño Negro"] = 16637,
    },
    ["Magullar"] = {
        ["DRUID"] = 9881,
        ["Ursal el Violento"] = 15793,
        ["Destructor dentoesquirla"] = 15793,
        ["Destructor Zarpira"] = 15793,
        ["Señor Supremo Ror"] = 15793,
        ["Gran Jefe Nevada"] = 15793,
        ["Uthil Llamaluna"] = 12161,
        ["Custodio Cenarion"] = 12161,
        ["Zarpafurecida"] = 12161,
    },
    ["Orden frenética"] = {
        ["Sobrestante Zarparrío"] = 3136,
        ["Sargento Garravil"] = 3136,
    },
    ["Guantes de ratero"] = {
        Professions = 9148,
    },
    ["Jubón de escórpido pesado"] = {
        Professions = 19051,
    },
    ["Coraza de bronce bañada en plata"] = {
        Professions = 2673,
    },
    ["Teletransporte a: Cima del Trueno"] = {
        ["MAGE"] = 3566,
    },
    ["Hacha de bronce"] = {
        Professions = 2741,
    },
    ["Sobrehombros vivos"] = {
        Professions = 19061,
    },
    ["Golpe de raptor"] = {
        ["HUNTER"] = 14266,
    },
    ["Capa de cría negra"] = {
        Professions = 9070,
    },
    ["Encantar escudo: resistencia a la Escarcha"] = {
        Professions = 13933,
    },
    ["Aguante de fortuna de las Sombras de Sayge"] = {
        ["Sayge"] = 23737,
    },
    ["Poderío de los Fauces de Madera"] = {
        Professions = 23703,
    },
    ["Fin de manifestación"] = {
        ["Espíritu de Kolk"] = 21965,
        ["Espíritu de Veng"] = 21965,
    },
    ["Explosión de Fuego"] = {
        ["MAGE"] = 10199,
    },
    ["Teletransporte a: Orgrimmar"] = {
        ["MAGE"] = 3567,
    },
    ["Granada de torio"] = {
        Professions = 19790,
    },
    ["Temblor monumental"] = {
        ["Destructor fundido"] = 19129,
        ["Barón Kazum"] = 19129,
    },
    ["Guantes de cuero refinado"] = {
        Professions = 2164,
    },
    ["Marejada de almas"] = {
        ["Lokholar el Señor del Hielo"] = 21307,
    },
    ["Encantar pechera: salud inferior"] = {
        Professions = 7748,
    },
    ["Protección contra las Sombras"] = {
        ["PRIEST"] = 10958,
    },
    ["Poción de sanación descolorida"] = {
        Professions = 4508,
    },
    ["Beso de tentadora"] = {
        ["Simone la Discreta"] = 23205,
    },
    ["Portal: Darnassus"] = {
        ["MAGE"] = 11419,
    },
    ["Contrahechizo"] = {
        ["MAGE"] = 2139,
    },
    ["Rezo de espíritu"] = {
        ["PRIEST"] = 27681,
    },
    ["Viento veloz"] = {
        ["Prate Vidente de las Nubes"] = 8385,
    },
    ["Sanar"] = {
        ["PRIEST"] = 6064,
        ["Sirena Escama Tormentosa"] = 11642,
        ["Sacerdotisa Colafuria"] = 11642,
        ["Oráculo de la Bahía Tormentosa"] = 11642,
        ["Escarbador Makrinni"] = 11642,
        ["Médico Negador"] = 22167,
        ["Alto Inquisidor Ribalimpia"] = 12039,
        ["Perito Ferrohondo"] = 15586,
        ["Perito veterano de Ferrohondo"] = 15586,
        ["Supervisor Campeón Ferrohondo"] = 15586,
    },
    ["Ofrendas de los Xavian"] = {
        ["Pícaro Xavian"] = 6925,
        ["Desertor Xavian"] = 6925,
        ["Jurapenas Xavian"] = 6925,
        ["Clamainferno Xavian"] = 6925,
        ["Príncipe Raze"] = 6925,
    },
    ["Encantar capa: resistencia al Fuego inferior"] = {
        Professions = 7861,
    },
    ["Rugido resonante"] = {
        ["Señor de la Guerra Gordunni"] = 10967,
        ["Alguacil de campo Afrasiabi"] = 10967,
    },
    ["Corazón humeante de la montaña"] = {
        Professions = 15596,
    },
    ["Invocar exiliados centelleantes"] = {
        ["Exiliado Prismático"] = 4937,
        ["Myzrael"] = 4937,
    },
    ["Máscara de gran vudú"] = {
        Professions = 10531,
    },
    ["Bendición de libertad"] = {
        ["PALADIN"] = 1044,
    },
    ["Elixir de agilidad"] = {
        Professions = 11449,
    },
    ["Cohete, VERDE"] = {
        ["Fuegos artificiales de Pat VERDE"] = 26345,
    },
    ["Deflector de llamas"] = {
        Professions = 3944,
    },
    ["Fuerza corrupta"] = {
        ["Guerrero Maderiza"] = 6816,
        ["Ursa Maderiza"] = 6816,
        ["Pataroble"] = 6816,
        ["Hedioserra enfurecido"] = 6816,
    },
    ["Descenso en picado"] = {
        ["Dire Condor"] = 5708,
        ["Vultros"] = 5708,
        ["Aguilón nervudo"] = 5708,
        ["Aguilón"] = 5708,
        ["Aguilón garrudo"] = 5708,
        ["Cazacielos Alargullo"] = 5708,
        ["Aguilón aterrador"] = 5708,
    },
    ["Toxina localizada"] = {
        ["Naga Filozante"] = 7947,
        ["Guerrero Filozante"] = 7947,
        ["Mirmidón Filozante"] = 7947,
        ["Filón Filozante"] = 7947,
        ["Víbora descarriada"] = 7947,
        ["Horror tóxico"] = 7947,
    },
    ["Guantes de seda azur"] = {
        Professions = 3854,
    },
    ["Endurecer piel"] = {
        ["Antárbol petrificado"] = 22693,
    },
    ["Fuego de alma"] = {
        ["WARLOCK"] = 17924,
    },
    ["Huracán"] = {
        ["DRUID"] = 17402,
    },
    ["Percepción"] = {
        Racial = 20600,
    },
    ["Crines de espinas"] = {
        ["Jabaespín Crines de Acero"] = 5280,
    },
    ["Cono de frío"] = {
        ["MAGE"] = 10161,
    },
    ["Forma de oso temible"] = {
        ["DRUID"] = 9634,
    },
    ["Bramido de entereza"] = {
        ["Oso viejo de Vallefresno"] = 4148,
        ["Oso giagante de Vallefresno"] = 4148,
    },
    ["Enfoque Arcano"] = {
        ["Maestro espectral"] = 17633,
    },
    ["Crear Manual de usuario de Pylon"] = {
        ["J.D. Collie"] = 15211,
    },
    ["Leotardos de cuero oscurecido"] = {
        Professions = 9195,
    },
    ["Pisotón en la tierra"] = {
        ["Can del Núcleo anciano"] = 19364,
    },
    ["Aceite de inmolación"] = {
        Professions = 11451,
    },
    ["Brazales de cobre"] = {
        Professions = 2663,
    },
    ["Revivir mascota"] = {
        ["HUNTER"] = 982,
    },
    ["Encantar escudo: aguante menor"] = {
        Professions = 13378,
    },
    ["Gafas de tigre volador"] = {
        Professions = 3934,
    },
    ["Elixir de detección de no-muertos"] = {
        Professions = 11460,
    },
    ["Encantar brazal: desvío"] = {
        Professions = 13931,
    },
    ["Infección volátil"] = {
        ["Viejo Ojosombrío"] = 3584,
        ["Balgaras el Hediondo"] = 3586,
    },
    ["Cachorros de la sabana"] = {
        ["Cachorro de la savana"] = 8210,
        ["Dishu"] = 8210,
    },
    ["Puño de piedra"] = {
        ["Ogro Puño de Roca"] = 4955,
        ["Déspota Puño de Roca"] = 4955,
        ["Tosco Puño de Roca"] = 4955,
        ["Destructor Puño de Roca"] = 4955,
        ["Señor Puño de Roca"] = 4955,
    },
    ["Llamas de Shahram"] = {
        ["Shahram"] = 16596,
    },
    ["Maldición de debilidad"] = {
        ["WARLOCK"] = 11708,
        ["Nigromante Peloescarcha"] = 11980,
        ["Médico brujo Mosh'Ogg"] = 11980,
        ["Licillin"] = 11980,
        ["Balizar el Agraviado"] = 11980,
        ["Xabraxxis"] = 11980,
        ["Jergosh el Conjurador"] = 18267,
        ["Darbel Montrosa"] = 12741,
        ["Morloch"] = 12741,
        ["Dorsacerado"] = 21007,
        ["Garrasombría"] = 17227,
    },
    ["Misión: impacto polimorfo"] = {
        ["Yeh'kinya"] = 24180,
    },
    ["Ofrenda de Ragnaros"] = {
        ["Enano Hierro Negro"] = 7891,
        ["Saboteador Hierro Negro"] = 7891,
        ["Tunelador Hierro Negro"] = 7891,
        ["Demoledor Hierro Negro"] = 7891,
        ["Fusilero Hierro Negro"] = 7891,
    },
    ["Martillo de Justicia"] = {
        ["PALADIN"] = 853,
    },
    ["Toque marchitado"] = {
        ["Jabaguardia marchito"] = 11442,
        ["Pútrido viviente"] = 11442,
    },
    ["Sacar espíritu"] = {
        ["Lethon"] = 24811,
    },
    ["Infusión de poder"] = {
        ["PRIEST"] = 10060,
    },
    ["Juego de segador de la cosecha compacto"] = {
        Professions = 3963,
    },
    ["Embate"] = {
        ["WARRIOR"] = 11605,
        ["Guardia de batalla de Rajacieno"] = 11430,
    },
    ["Calma mental"] = {
        ["PRIEST"] = 10953,
    },
    ["Traca de cohetes verdes grandes"] = {
        Professions = 26427,
    },
    ["Voluntad de Hakkar"] = {
        ["Gurubashi"] = 24178,
    },
    ["Minería"] = {
        Professions = 2576,
        Professions = 3564,
        Professions = 10248,
    },
    ["Invocar destrero caído de atracoscuro"] = {
        ["Destrero caído de Atracoscuro"] = 23261,
        ["Caballero de la Muerte Atracoscuro"] = 23261,
    },
    ["Enviar a combatiente exaltado"] = {
        ["Twiggy Cabezaplana"] = 8645,
    },
    ["Crear Rubí de maná"] = {
        ["MAGE"] = 10054,
    },
    ["Toque gélido de Trelane"] = {
        ["Kor'gresh Ira Helada"] = 4320,
    },
    ["Invocar ráfagas de maná"] = {
        ["Chorro de maná"] = 22939,
        ["Monstruosidad residual"] = 22939,
    },
    ["Carga de cuernosombrío"] = {
        ["Venado Necraste"] = 6921,
        ["Venado Necraste viejo"] = 6921,
    },
    ["Camisa de tejido mágico lavanda"] = {
        Professions = 12075,
    },
    ["Mina terrestre Hierro Negro"] = {
        ["Mina terrestre Hierro Negro"] = 11802,
        ["Agente Hierro Negro"] = 11802,
        ["Thauris Balgarr"] = 11802,
    },
    ["Intervención divina"] = {
        ["PALADIN"] = 19752,
    },
    ["Fuego estelar"] = {
        ["DRUID"] = 25298,
    },
    ["Forma de oso de Kinelory"] = {
        ["Kinelory"] = 4948,
    },
    ["Disyunción"] = {
        ["Cavatror Truemorro"] = 14533,
    },
    ["Pólvora férrea"] = {
        Professions = 3918,
    },
    ["Tótem derribador"] = {
        ["SHAMAN"] = 8177,
    },
    ["Defensa de Valiente"] = {
        ["Archimago Tervosh"] = 7120,
    },
    ["Fantasmas encantados"] = {
        ["Ciudadano espectral"] = 16336,
        ["Ciudadano fantasmal"] = 16336,
    },
    ["Detectar invisibilidad"] = {
        ["WARLOCK"] = 2970,
    },
    ["Poción de sangre de trol fuerte"] = {
        Professions = 3176,
    },
    ["Agilidad deteriorada"] = {
        ["Crocolisco Fauzatroz corrupto"] = 7901,
    },
    ["Encantar brazales: intelecto"] = {
        Professions = 13822,
    },
    ["Grito intimidador"] = {
        ["WARRIOR"] = 5246,
    },
    ["Bendición de Nordrassil"] = {
        ["Eris Feleste"] = 23108,
    },
    ["Piedra de pulir burda"] = {
        Professions = 3326,
    },
    ["Cinturón de anillas de cobre"] = {
        Professions = 2661,
    },
    ["Ira acuciante"] = {
        ["Abisario invocado"] = 7750,
    },
    ["Arremetida de conmoción"] = {
        ["WARRIOR"] = 12809,
    },
    ["Invocar a Ciclonio"] = {
        ["Bath'rah el Cazavientos"] = 8606,
    },
    ["Dinamita férrea"] = {
        Professions = 3919,
    },
    ["Disparo de Escarcha"] = {
        ["Sirena Látigo de Ira"] = 12551,
        ["Instructor de bestias Rajacieno"] = 6984,
    },
    ["Destripar"] = {
        ["DRUID"] = 9896,
    },
    ["Cohete verde pequeño"] = {
        Professions = 26417,
    },
    ["Guerrera de paño rúnico"] = {
        Professions = 18407,
    },
    ["Lobo Crines de Acero"] = {
        ["Lobo Crines de Acero"] = 6479,
        ["Cazador Crines de Acero"] = 6479,
    },
    ["Cohete, GRANDE BLANCO"] = {
        ["Fuegos artificiales de Pat GRAN BLANCO"] = 26355,
    },
    ["Aparición de dracónido cromático"] = {
        ["Dracónido cromático"] = 22680,
        ["Black Drakonid Spawner"] = 22680,
        ["Engendrador de dracónido rojo"] = 22680,
        ["Green Drakonid Spawner"] = 22680,
        ["Bronze Drakonid Spawner"] = 22680,
        ["Blue Drakonid Spawner"] = 22680,
    },
    ["Huevo de trepador silítido"] = {
        ["Ovotrepador silítido"] = 6587,
        ["Trepador silítido"] = 6587,
    },
    ["Botas de mitril pesado"] = {
        Professions = 9968,
    },
    ["Bomba de humo"] = {
        ["Comando Kurzen"] = 7964,
        ["Bazil Thredd"] = 7964,
        ["Coronel Kurzen"] = 8817,
    },
    ["Tótem avizor"] = {
        ["SHAMAN"] = 6495,
    },
    ["Cohete rojo grande"] = {
        Professions = 26422,
    },
    ["Jubón vid de sangre"] = {
        Professions = 24091,
    },
    ["Toga de tela lunar"] = {
        Professions = 22902,
    },
    ["Encantar brazal: desvío inferior"] = {
        Professions = 13646,
    },
    ["Forma de piedra"] = {
        Racial = 20594,
        ["Insurgente Hierro Negro"] = 7020,
        ["Capitán Ferrocolina"] = 7020,
    },
    ["Guantes quiméricos"] = {
        Professions = 19053,
    },
    ["Mordedura"] = {
        Pet = 17261,
    },
    ["Sobrehombros de mitril pesado"] = {
        Professions = 9926,
    },
    ["Encantar arma: Destripadora de bestias menor"] = {
        Professions = 7786,
    },
    ["Encantar arma: golpear inferior"] = {
        Professions = 13503,
    },
    ["Guarda del ojo"] = {
        ["Capitán Halyndor"] = 3389,
    },
    ["Aura de reprensión"] = {
        ["PALADIN"] = 10301,
        ["Alto Señor Bolvar Fordragon"] = 8990,
        ["Comandante Escarlata Mograine"] = 8990,
    },
    ["Encantar arma 2M: impacto superior"] = {
        Professions = 13937,
    },
    ["Mascota Sangrapellejo"] = {
        ["Tigre Cabellesangre"] = 3612,
        ["Maestro de bestias Cabellesangre"] = 3612,
    },
    ["Dispensador de bombas goblin"] = {
        Professions = 12755,
    },
    ["Armadura de cuero oscurecido"] = {
        Professions = 9196,
    },
    ["Tétanos"] = {
        ["Gólem de la cosecha oxidado"] = 8014,
        ["Águila ratonera montés"] = 8014,
        ["Maleante Kolkar"] = 8014,
        ["Invasor Kolkar"] = 8014,
    },
    ["Muñeco-diana"] = {
        Professions = 3932,
    },
    ["Ira rabiosa"] = {
        ["WARRIOR"] = 18499,
    },
    ["Martillo de cólera"] = {
        ["PALADIN"] = 24239,
    },
    ["Poción de acción viva"] = {
        Professions = 24367,
    },
    ["Veneno mortal II"] = {
        ["ROGUE"] = 2837,
    },
    ["Ultravelocidad"] = {
        ["Machacador Grito de Guerra"] = 18546,
    },
    ["Transformación de Warosh"] = {
        ["Warosh"] = 16801,
    },
    ["Disparo de puntería"] = {
        ["HUNTER"] = 20904,
    },
    ["Bolsa de pellejo de kodo"] = {
        Professions = 5244,
    },
    ["Invocar marionetas de Helcular"] = {
        ["Títere de Helcular"] = 4950,
        ["Restos de Helcular"] = 4950,
    },
    ["Portal: Orgrimmar"] = {
        ["MAGE"] = 11417,
    },
    ["Miedo"] = {
        ["WARLOCK"] = 6215,
    },
    ["Barkskin Effect (DND)"] = {
        ["DRUID"] = 22839,
    },
    ["Invocar matriarca Musgondo"] = {
        ["Matriarca Musgondo"] = 6536,
        ["Trampedor Musgondo"] = 6536,
    },
    ["Invocar fantasma ilusorio"] = {
        ["Fantasma ilusorio"] = 8986,
        ["Fantasma encantado"] = 8986,
    },
    ["Cinturón de escamas descarriadas"] = {
        Professions = 7955,
    },
    ["Fuego rápido"] = {
        ["HUNTER"] = 3045,
    },
    ["Latigazo de llamas"] = {
        ["Cría roja"] = 3356,
        ["Hermana Riven"] = 3356,
    },
    ["Lamento de muerto"] = {
        ["Alma perdida"] = 7713,
        ["Espíritu deambulante"] = 7713,
        ["Espíritu atormentado"] = 7713,
        ["Ancestro de los Lamentos"] = 7713,
        ["Muerte de los Lamentos"] = 7713,
        ["Cartero no-muerto"] = 7713,
        ["Espectro de los Lamentos"] = 7713,
        ["Espíritu del Valle del Terror"] = 7713,
    },
    ["Encantar arma: mataelementales inferior"] = {
        Professions = 13655,
    },
    ["Teletransporte a: Ventormenta"] = {
        ["MAGE"] = 3561,
    },
    ["Forma de las Sombras"] = {
        ["PRIEST"] = 15473,
    },
    ["Forma de oso"] = {
        ["DRUID"] = 5487,
        ["Kerlonian Semprumbrío"] = 18309,
        ["Naturalista Tótem Siniestro"] = 19030,
        ["Protector Cenarion"] = 7090,
        ["Uthil Llamaluna"] = 7090,
        ["Custodio Cenarion"] = 7090,
        ["Zarpafurecida"] = 7090,
    },
    ["Dinamita burda"] = {
        Professions = 3931,
        ["Ashlan Risapétrea"] = 9009,
        ["Ashlan Risapétrea"] = 9004,
        ["Ashlan Risapétrea"] = 9002,
        ["Ashlan Risapétrea"] = 9003,
    },
    ["Bendición de poderío"] = {
        ["PALADIN"] = 25291,
    },
    ["Invocar brasales"] = {
        ["Brasal"] = 10869,
        ["Sirviente ardiente"] = 10869,
    },
    ["Capa larga de seda"] = {
        Professions = 3861,
    },
    ["Trabazón con la Naturaleza"] = {
        ["DRUID"] = 17329,
    },
    ["Cinturón de escórpido pesado"] = {
        Professions = 19070,
    },
    ["Espíritu de Garraluna"] = {
        ["Espíritu de Garraluna"] = 18986,
        ["Garraluna"] = 18986,
    },
    ["Drenar vida"] = {
        ["WARLOCK"] = 11700,
        ["Varimathras"] = 20743,
    },
    ["Camisa de espadachín negra"] = {
        Professions = 3873,
    },
    ["Fetidez sobrecogedora"] = {
        ["Sátiro Putredor"] = 6942,
        ["Pícaro Putredor"] = 6942,
        ["Jurapenas Putredor"] = 6942,
        ["Acechasombras Putredor"] = 6942,
    },
    ["Cinturón de prevención de daño gnómico"] = {
        Professions = 12903,
    },
    ["Sobrehombros de hierro verdes"] = {
        Professions = 3504,
    },
    ["Puño de Shahram"] = {
        ["Shahram"] = 16601,
    },
    ["Vapulear"] = {
        ["Avanzado Zarparrío"] = 3391,
        ["Edwin VanCleef"] = 3391,
        ["Sr. Golpin"] = 3391,
        ["Kóbold Ratatúnel"] = 3391,
        ["Targor el Pavoroso"] = 3391,
        ["Abrecaminos Bosque Negro"] = 3391,
        ["Reptador Correarrecifes enfurecido"] = 3391,
        ["Vándalo de las Tierras Altas"] = 3391,
        ["Destructor Rotapolvo"] = 3391,
        ["Coyote del risco feral"] = 3391,
        ["Segador Solescama"] = 3391,
        ["Oficial Bael'Dun"] = 3391,
        ["Abrecaminos Crines de Acero"] = 3391,
        ["Vigilante del cubil Maderiza"] = 3391,
        ["Malapluma Furia Sangrienta"] = 3391,
        ["Hidra Strashaz"] = 3391,
        ["Trillador de la oscuridad"] = 3391,
        ["Trillador de la oscuridad mayor"] = 3391,
        ["Fardel Dabyrie"] = 3391,
        ["Monje Escarlata"] = 3391,
        ["Destructor Kolkar"] = 3391,
        ["Destructor Maggram"] = 3391,
        ["Avispa Centipaar"] = 3391,
        ["Aguijonero Centipaar"] = 3391,
        ["Enjambrista Centipaar"] = 3391,
        ["Saqueador Cueva Honda"] = 3391,
        ["Vándalo marino"] = 3391,
        ["Gran vándalo marino"] = 3391,
        ["Atracador de guerra"] = 3391,
        ["Ferocitas el Comesueños"] = 3391,
        ["Devastador Zarpayel"] = 3391,
        ["Lechúcico lunar"] = 3391,
        ["Rorgish Jowl"] = 3391,
        ["Rastreador Bosque Negro"] = 3391,
        ["Cazador Latigosólido"] = 3391,
        ["Garraluna"] = 3391,
        ["Vorsha la Azotadora"] = 3391,
        ["Canisangre Lobo Gélido"] = 3391,
        ["Lechuza de Pico Tormenta"] = 3391,
        ["Harb Montediendo"] = 3391,
    },
    ["Vara rúnica de veraplata"] = {
        Professions = 13702,
    },
    ["Aullido perforador"] = {
        ["WARRIOR"] = 12323,
    },
    ["Retraso"] = {
        ["Tecnobot"] = 10855,
    },
    ["Marca del cazador"] = {
        ["HUNTER"] = 14325,
    },
    ["Ardilla mecánica"] = {
        Professions = 3928,
    },
    ["Bolsa de lino roja"] = {
        Professions = 6686,
    },
    ["Maldición de espinas"] = {
        ["Mago oscuro del Sindicato"] = 6909,
        ["Anciano enloquecido"] = 6909,
        ["Anciano marchito"] = 6909,
        ["Ancestro Vengativo"] = 6909,
        ["Augur Filo Ardiente"] = 6909,
    },
    ["Elixir de matanza de demonios"] = {
        Professions = 11477,
    },
    ["Espiral de la muerte"] = {
        ["WARLOCK"] = 17926,
    },
    ["Tótem Garra de piedra"] = {
        ["SHAMAN"] = 10428,
    },
    ["Nacimiento"] = {
        ["Tentáculo Garral"] = 26586,
        ["Tentáculo ocular"] = 26586,
    },
    ["Guantes de escamas descarriadas"] = {
        Professions = 7954,
    },
    ["Tenacidad ardiente"] = {
        ["Telf Joolam"] = 8383,
    },
    ["Invocar serpiente de Dalaran"] = {
        ["Serpiente Dalaran"] = 3615,
        ["Protector Dalaran"] = 3615,
        ["Pastor Dalaran"] = 3615,
    },
    ["Hebilla de hierro"] = {
        Professions = 8768,
    },
    ["Poción de escudo de piedra superior"] = {
        Professions = 17570,
    },
    ["Efecto de Inferno"] = {
        ["Inférno"] = 22703,
        ["Infernal corrupto"] = 22703,
    },
    ["Elixir de ojo de gato"] = {
        Professions = 12609,
    },
    ["Encantar brazales: Intelecto superior"] = {
        Professions = 20008,
    },
    ["Invocar caballo de guerra"] = {
        ["PALADIN"] = 13819,
    },
    ["Punta de escudo de torio"] = {
        Professions = 16651,
    },
    ["Salva"] = {
        ["HUNTER"] = 14295,
    },
    ["Telaraña"] = {
        ["Madre Colmillo"] = 12023,
        ["Virasedal Tejemadera"] = 12023,
        ["Acechador Espinaferal"] = 12023,
        ["Telarácnida venenosa"] = 745,
        ["Girapenas"] = 745,
        ["Tejerred Musgondo"] = 745,
        ["Araña Cristalderma"] = 745,
        ["Atracador de las tumbas"] = 745,
        ["Ermitaña Telamadera"] = 745,
        ["Acechador de piedra"] = 745,
        ["Besseleth"] = 745,
    },
    ["Sabiduría de los Fauces de Madera"] = {
        Professions = 23662,
    },
    ["Voluntad de Shahram"] = {
        ["Shahram"] = 16598,
    },
    ["Coraza de dragontina verde"] = {
        Professions = 19050,
    },
    ["Roboalarma"] = {
        Professions = 23096,
    },
    ["Ácido corrosivo"] = {
        ["Batidor Gravamorro"] = 8245,
    },
    ["Poción de invisibilidad inferior"] = {
        Professions = 3448,
    },
    ["Dragonizo mecánico de mitril"] = {
        Professions = 12624,
    },
    ["Faltriquera de munición de cuero grueso"] = {
        Professions = 14932,
    },
    ["Encantar escudo: protección inferior"] = {
        Professions = 13464,
    },
    ["Puñado de tornillos de cobre"] = {
        Professions = 3922,
    },
    ["Nube de radiación"] = {
        ["Babosa irradiada"] = 10341,
    },
    ["Guantes de herborista"] = {
        Professions = 9146,
    },
    ["Toga de lino azul"] = {
        Professions = 7633,
    },
    ["Desesperación ancestral"] = {
        ["Can del Núcleo anciano"] = 19369,
    },
    ["Faltriquera de tejido mágico encantada"] = {
        Professions = 27658,
    },
    ["Crear Cetro de Celebras"] = {
        ["Celebras el Redimido"] = 21939,
    },
    ["Limpiar"] = {
        ["PALADIN"] = 4987,
    },
    ["Añublo"] = {
        Professions = 10011,
    },
    ["Zarpa"] = {
        ["DRUID"] = 9850,
        Pet = 3009,
    },
    ["Invocar zombi"] = {
        ["Zombi invocado"] = 16590,
        ["Invocador Oscuro"] = 16590,
    },
    ["Poder de Shahram"] = {
        ["Shahram"] = 16600,
    },
    ["Encantar pechera: maná superior"] = {
        Professions = 13663,
    },
    ["Botas rúnicas estigias"] = {
        Professions = 24903,
    },
    ["Aparición de guardia vil iracundo"] = {
        ["Guardia maldito enfurecido"] = 22393,
    },
    ["Burbuja Arcana"] = {
        ["Arcanista Doan"] = 9438,
    },
    ["Invocar arañita de la cumbre"] = {
        ["Arañita de la cumbre"] = 16103,
        ["Araña de la cumbre"] = 16103,
    },
    ["Golpe fantasmal"] = {
        ["ROGUE"] = 14278,
    },
    ["Elixir de gigantes"] = {
        Professions = 11472,
    },
    ["Contacto de plata"] = {
        Professions = 3973,
    },
    ["Flema de veneno corrosivo"] = {
        ["Quimerok"] = 20629,
    },
    ["Bendición de protección"] = {
        ["PALADIN"] = 10278,
    },
    ["Tótem Marea de maná"] = {
        ["SHAMAN"] = 17359,
    },
    ["Captar no-muertos"] = {
        ["PALADIN"] = 5502,
    },
    ["Crear caché de Mau'ari"] = {
        ["Médico bruja Mau'ari"] = 16351,
    },
    ["Daga con empuñadura de perla"] = {
        Professions = 6517,
    },
    ["Poción de sanación"] = {
        Professions = 3447,
    },
    ["Preparación"] = {
        ["ROGUE"] = 14185,
    },
    ["Casco de cuero salvaje"] = {
        Professions = 10546,
    },
    ["Gafas tintadas de verde"] = {
        Professions = 3956,
    },
    ["Tinte fantasmal"] = {
        Professions = 11473,
    },
    ["Poción de resistencia a la magia"] = {
        Professions = 11453,
    },
    ["Fuegos artificiales serpenteantes"] = {
        Professions = 23507,
    },
    ["Sobrehombros de bronce férreos"] = {
        Professions = 3328,
    },
    ["Sobrehombros de cuero salvaje"] = {
        Professions = 10529,
    },
    ["Muñeco-diana avanzado: Evento 003"] = {
        ["Muñeco-diana avanzado del equipo mortero"] = 19723,
        ["Avistador Klemmy"] = 19723,
    },
    ["Transmutar: tierra a agua"] = {
        Professions = 17561,
    },
    ["Mordedura serrada"] = {
        ["Can del Núcleo"] = 19771,
    },
    ["Crear G.E.A.Q."] = {
        ["Aparato Gizmotrónico de Wrenix"] = 9977,
    },
    ["Martillo iridiscente"] = {
        Professions = 6518,
    },
    ["Resistencia al Fuego"] = {
        ["HUNTER"] = 24464,
    },
    ["Cadena de acero"] = {
        Professions = 7224,
    },
    ["Brazales de cuero rúnico"] = {
        Professions = 19065,
    },
    ["Fuego feérico"] = {
        ["DRUID"] = 9907,
        ["Zancanieve Peloescarcha"] = 6950,
        ["Duende granuja"] = 6950,
        ["Abrecaminos Bosque Negro"] = 6950,
        ["Correpolvo Crines de Acero"] = 6950,
        ["Tejespina Crines de Acero"] = 6950,
        ["Rastreador Bosque Negro"] = 6950,
        ["Marosh el Taimado"] = 6950,
    },
    ["Elixir de poder de Escarcha"] = {
        Professions = 21923,
    },
    ["Invocar ráfaga de lava G"] = {
        ["Rata del Núcleo"] = 21905,
    },
    ["Pantalones de esmoquin"] = {
        Professions = 12089,
    },
    ["Guantes Fénix"] = {
        Professions = 3868,
    },
    ["Mordedura infectada"] = {
        ["Hidra Strashaz"] = 16128,
        ["Viuda negra reproductora"] = 7367,
    },
    ["Aspecto del guepardo"] = {
        ["HUNTER"] = 5118,
    },
    ["Elixir de fuerza de león"] = {
        Professions = 2329,
    },
    ["Rifle de vista lunar"] = {
        Professions = 3954,
    },
    ["Aguijón de tósigo"] = {
        ["escórpido Venocola"] = 5416,
        ["Sacudidor Scorpashi"] = 5416,
        ["Latigador Scorpashi"] = 5416,
        ["Lativenenoso Scorpashi"] = 5416,
        ["Acechadunas escórpido"] = 5416,
        ["Besseleth"] = 5416,
        ["Aguijón vil"] = 8257,
    },
    ["Invocar ráfaga de lava B"] = {
        ["Rata del Núcleo"] = 21900,
    },
    ["Vista de mago"] = {
        ["Archimago Ansirem Tejepista"] = 3659,
    },
    ["Zapatillas de seda de araña"] = {
        Professions = 3856,
    },
    ["Rugido desafiante"] = {
        ["DRUID"] = 5209,
    },
    ["Invocar guardia escudero"] = {
        ["Guardia escudero Dalaran"] = 3655,
        ["Invocador Dalaran"] = 3655,
    },
    ["Devorar magia"] = {
        Pet = 19736,
    },
    ["Parar"] = {
        ["SHAMAN"] = 16268,
    },
    ["Camisa verde con estilo"] = {
        Professions = 7893,
    },
    ["Transmutar: fuego a tierra"] = {
        Professions = 17560,
    },
    ["Cólera"] = {
        ["DRUID"] = 9912,
        ["Abrecaminos Tuercepinos"] = 9739,
        ["Peletero Crines Pálidos"] = 9739,
        ["Druida amputado"] = 9739,
        ["Botánico Cenarion"] = 9739,
        ["Tyranis Malem"] = 9739,
        ["Místico Tuercepinos"] = 9739,
        ["Oráculo lechúcico lunar"] = 9739,
    },
    ["Toque de corvozarpa"] = {
        ["Mano de Corvozarpa"] = 3263,
    },
    ["Jubón terráneo"] = {
        Professions = 8764,
    },
    ["Carga de seforio potente"] = {
        Professions = 23080,
    },
    ["Espina infectada"] = {
        ["Cuerolanza marchito"] = 12245,
    },
    ["Botas de cuero salvaje"] = {
        Professions = 10566,
    },
    ["Cinturón de guardián"] = {
        Professions = 3775,
    },
    ["Rezo desesperado"] = {
        ["PRIEST"] = 19243,
    },
    ["Enfoque caótico"] = {
        ["Jurapenas Mala Hierba"] = 22418,
    },
    ["Encantar guantes: minería"] = {
        Professions = 13612,
    },
    ["Aceite de Escarcha"] = {
        Professions = 3454,
    },
    ["Latigazo"] = {
        ["Raptor colazote"] = 6607,
        ["Colazote de las Tierras Altas"] = 6607,
        ["Basilisco Colastador"] = 6607,
        ["Zancador Pedalar"] = 6607,
        ["Colazote solescama"] = 6607,
        ["Aguijolatigador descarriado"] = 6607,
        ["Colazote escórpido"] = 6607,
        ["Latigador pétalo de sangre"] = 6607,
        ["Sombra de Hakkar"] = 6607,
        ["Triturador de arrastre"] = 6607,
        ["Vorsha la Azotadora"] = 6607,
    },
    ["Invocar cofre oxidado"] = {
        ["Babosa putrefacta"] = 6464,
    },
    ["Máscara de bandido blanca"] = {
        Professions = 12059,
    },
    ["Poción de Protección contra el Fuego superior"] = {
        Professions = 17574,
    },
    ["Pulverizador Hierro Negro"] = {
        Professions = 15292,
    },
    ["Forma de serpiente"] = {
        ["Boahn"] = 8041,
        ["Druida del Colmillo"] = 8041,
    },
    ["Hacha de batalla de bronce"] = {
        Professions = 9987,
    },
    ["Herida profunda"] = {
        ["Espíritu malicioso"] = 12721,
        ["Aspecto de malicia"] = 12721,
    },
    ["Brazales de escórpido resistentes"] = {
        Professions = 10533,
    },
    ["Invocar aliado Antárbol"] = {
        ["Aliado Antárbol"] = 7993,
        ["Hijo de Cenarius"] = 7993,
    },
    ["Ejecutar"] = {
        ["WARRIOR"] = 20662,
        ["Destripador Pielsombra"] = 7160,
        ["Rebanacabezas Secacorteza"] = 7160,
        ["Destripador Alabruja"] = 7160,
        ["Destripadora Furia Sangrienta"] = 7160,
        ["Carroñero de los Llanos de Sal"] = 7160,
        ["Buitre de los Llanos de Sal"] = 7160,
    },
    ["Escopeta de chapa de plata"] = {
        Professions = 3949,
    },
    ["Aspecto del halcón"] = {
        ["HUNTER"] = 25296,
    },
    ["Esfumación de Nagmara"] = {
        ["Maestra Nagmara"] = 15341,
        ["Soldado Sinrroca"] = 15341,
    },
    ["Guanteletes de dragontina"] = {
        Professions = 10619,
    },
    ["Expulsar vestigio"] = {
        ["Sneed"] = 5141,
        ["Machacador de Sneed"] = 5141,
    },
    ["Golpear"] = {
        ["Brack"] = 11976,
        ["Partehuesos Rompecantos"] = 11976,
        ["Dextren Tutor"] = 11976,
        ["Capitán Melrache"] = 11976,
        ["Granjero Solliden"] = 11976,
        ["Guerrero Tuercepinos"] = 11976,
        ["Destructor Aplastacresta"] = 11976,
        ["Partehuesos Grutacanto"] = 11976,
        ["Maleante Kolkar"] = 11976,
        ["Myrmidón Colafuria"] = 11976,
        ["Kóbold Gravamorro"] = 11976,
        ["Guardia Eduardo"] = 11976,
        ["Guardia Jarad"] = 11976,
        ["Guardia Kahil"] = 11976,
        ["Guardia Lana"] = 11976,
        ["Guardia Narrisha"] = 11976,
        ["Guardia Tark"] = 11976,
        ["Oficial de Nethergarde"] = 11976,
        ["Guerrero de de la Bahía Tormentosa"] = 11976,
        ["Guerrero Negador"] = 11976,
        ["Flagglemurk el Cruel"] = 11976,
        ["Señor de la Guerra Krom'zar"] = 11976,
        ["Invasor Kolkar"] = 11976,
        ["Atracador Tótem Siniestro"] = 11976,
        ["Defensor espectral"] = 11976,
        ["Trogg Furia Ardiente"] = 11976,
        ["Custodio de Pico Tormenta"] = 11976,
        ["Montaraz Bramibum"] = 11976,
        ["Threggil"] = 11976,
        ["Minero esquelético"] = 12057,
        ["Guardia serpiente Strashaz"] = 12057,
        ["Lord Pecatripador"] = 12057,
        ["Cavador Mostacho Blanco"] = 12057,
        ["Umi Thorson"] = 12057,
        ["Invasor Minafría"] = 12057,
        ["Somnus"] = 18368,
        ["Trogg Ferrohondo"] = 14516,
        ["Guardia Real del Terror"] = 14516,
        ["Viejo mentovilo"] = 13446,
        ["Guardia Minafría"] = 15580,
        ["Comandante del aire Guse"] = 15580,
        ["Comandante del aire Jeztor"] = 15580,
        ["Comandante del aire Mulverick"] = 15580,
        ["Comandante del aire Ichman"] = 15580,
        ["Comandante del aire Slidore"] = 15580,
        ["Guardia Minafría veterano"] = 15580,
        ["Asaltante de caminos borracho"] = 13584,
    },
    ["Llave esqueleto de plata"] = {
        Professions = 19666,
    },
    ["Invocar huargo Guardialobo"] = {
        ["Lupo Guardialobo"] = 7107,
        ["Guardialobo Colmillo Oscuro"] = 7107,
    },
    ["Encantar arma 2M: espíritu inferior"] = {
        Professions = 13380,
    },
    ["Piedra de afilar densa"] = {
        Professions = 16641,
    },
    ["Conjuro de conjunto de herramientas de pícaro"] = {
        ["Aparato Gizmotrónico de Wrenix"] = 9949,
    },
    ["Guantes de tela lunar"] = {
        Professions = 22869,
    },
    ["Botas de cuero estampado"] = {
        Professions = 2161,
    },
    ["Yelmo de hierro verde"] = {
        Professions = 3502,
    },
    ["Invocar aliados Antárbol"] = {
        ["Aliado Antárbol"] = 20702,
        ["Archidruida Fandral Corzocelada"] = 20702,
    },
    ["Guantes de cuero estampado"] = {
        Professions = 3756,
    },
    ["Hibernar"] = {
        ["DRUID"] = 18658,
    },
    ["Aura de devoción"] = {
        ["PALADIN"] = 10293,
        ["Campeón Roca Negra"] = 8258,
        ["Capitán Melrache"] = 8258,
        ["Montaraz de Dun Garok"] = 8258,
        ["Señor Puño de Roca"] = 8258,
        ["Protector Escarlata"] = 8258,
        ["Jabaguardia Rajacieno"] = 8258,
        ["Campeón Jabaguardia"] = 8258,
        ["Señor de Batalla Kolkar"] = 8258,
    },
    ["Sangre mácula"] = {
        Pet = 19660,
    },
    ["Azote de escudo"] = {
        ["WARRIOR"] = 1672,
        ["Acechacaminos Defias"] = 11972,
        ["Gath'Ilzogg"] = 11972,
        ["Cabo Keeshan"] = 11972,
        ["Guerrero Cabellesangre"] = 11972,
        ["Guerrero Machacacráneos"] = 11972,
        ["Argos del Sindicato"] = 11972,
        ["Marinero Kul Tiras"] = 11972,
        ["Defensor Escarlata"] = 11972,
        ["Defensor de Rajacieno"] = 11972,
        ["Khan Jehn"] = 11972,
        ["Comandante de vigilancia Zalafil"] = 11972,
        ["Guardia Real del Terror"] = 11972,
    },
    ["Cinta de paño rúnico"] = {
        Professions = 18444,
    },
    ["Botas de sable de hielo"] = {
        Professions = 19066,
    },
    ["Refuerzo para armadura grueso"] = {
        Professions = 10487,
    },
    ["Traca de cohetes verdes"] = {
        Professions = 26424,
    },
    ["Tótem de protección elemental"] = {
        ["Tótem de protección elemental"] = 8262,
        ["Sabio Caramuerte"] = 8262,
        ["Totémico Fauces de Madera"] = 8262,
        ["Médico Brujo Espina Ahumada"] = 8262,
    },
    ["Aceite de Fuego"] = {
        Professions = 7837,
    },
    ["Poción de Protección contra las Sombras"] = {
        Professions = 7256,
    },
    ["Dinamita de las Minas de la Muerte"] = {
        ["Dinamitador no-muerto"] = 7395,
    },
    ["Armadura de guardián"] = {
        Professions = 3773,
    },
    ["Invocar tejetinieblas Puño Negro"] = {
        ["Tejedor de tienieblas Puño Negro invocado"] = 15794,
        ["Invocador Puño Negro"] = 15794,
    },
    ["Invocar sirviente infernal"] = {
        ["Sirviente Infernal"] = 12740,
        ["Lady Sevine"] = 12740,
    },
    ["Ansia de sangre"] = {
        ["Capataz Sniwle"] = 16170,
        ["Hamhock"] = 6742,
        ["Mago Aplastacresta"] = 6742,
        ["Grel'borg el Avaro"] = 6742,
        ["Lo'Grosh"] = 6742,
        ["Destrero de sangre Kolkar"] = 6742,
        ["Chamán Piel de Cardo"] = 6742,
        ["Domador de jaurías Loksey"] = 6742,
        ["Adepto Filo Ardiente"] = 6742,
    },
    ["Arañazo"] = {
        ["DRUID"] = 9904,
    },
    ["Bomba altamente explosiva"] = {
        Professions = 12619,
    },
    ["Cinturón oscurecido"] = {
        Professions = 9206,
    },
    ["Aliento de ácido corrosivo"] = {
        ["Gyth"] = 16359,
        ["Somnus"] = 20667,
    },
    ["Crear piedra de hechizo (sublime)"] = {
        ["WARLOCK"] = 17728,
    },
    ["Transmutar: aire en fuego"] = {
        Professions = 17559,
    },
    ["Guantes glaciales"] = {
        Professions = 28205,
    },
    ["Presencia mental"] = {
        ["MAGE"] = 12043,
    },
    ["Bloque de hielo"] = {
        ["MAGE"] = 11958,
    },
    ["Lobo fantasmal"] = {
        ["SHAMAN"] = 2645,
    },
    ["Invocar espectro de la Hermandad"] = {
        ["Espectro de la Hermandad"] = 3722,
        ["Bookie Herod"] = 3722,
    },
    ["Proyectil artesanal sólido"] = {
        Professions = 3947,
    },
    ["Bramido intimidador"] = {
        ["Gruñidor Pestillejo"] = 6576,
    },
    ["Poción de Protección contra lo Arcano superior"] = {
        Professions = 17577,
    },
    ["Cinturón de lino"] = {
        Professions = 8776,
    },
    ["Poción de maná sublime"] = {
        Professions = 17580,
    },
    ["Camisa marcial naranja"] = {
        Professions = 12064,
    },
    ["Botas de escórpido resistentes"] = {
        Professions = 10554,
    },
    ["Invocar delusiones lupinas"] = {
        ["Falsa ilusión Lupina"] = 7132,
        ["Horror Lupino"] = 7132,
    },
    ["Guarda de sanación"] = {
        ["Tutor sanador"] = 5605,
        ["Médico brujo Cabellesangre"] = 5605,
        ["Acechalagos Anca Vil"] = 5605,
        ["Totémico Tuercepinos"] = 5605,
        ["Totémico Bosque Negro"] = 5605,
        ["Médico brujo Secacorteza"] = 5605,
        ["Chamán Grutacanto"] = 5605,
        ["Anciano místico Filocico"] = 5605,
        ["Oráculo Filozante"] = 5605,
        ["Vapuleador Tótem Siniestro"] = 5605,
        ["Tutor de curación mayor"] = 11899,
        ["Chamán Mosh'Ogg"] = 11899,
        ["Médico Brujo Furiarena"] = 11899,
        ["Tutor sanador IV"] = 6274,
        ["Oráculo Branquiazul"] = 6274,
        ["Profeta Crines de Acero"] = 6274,
        ["Totémico Piel de Cardo"] = 6274,
        ["Tutor sanador V"] = 4971,
        ["Totémico de Rajacieno"] = 4971,
        ["Sabio Caramuerte"] = 4971,
        ["Profeta Caramuerte"] = 4971,
    },
    ["Estoque de mitril deslumbrante"] = {
        Professions = 10005,
    },
    ["Invocar guardia de Eliza"] = {
        ["Guarda de Eliza"] = 3107,
        ["Eliza"] = 3107,
    },
    ["Envoltura de mitril"] = {
        Professions = 12599,
    },
    ["Premeditación"] = {
        ["ROGUE"] = 14183,
    },
    ["Descarga de saurio"] = {
        ["Truenagarto"] = 5401,
        ["Rayopiel"] = 5401,
        ["Tormentero"] = 5401,
        ["Tronatesta"] = 5401,
        ["Morrotormenta"] = 5401,
        ["Tormentabisal"] = 5401,
        ["Truenagarto enfurecido"] = 5401,
        ["Ancestro truenagarto"] = 5401,
    },
    ["Voluntad de los Renegados"] = {
        Racial = 7744,
    },
    ["Crear tubérculo"] = {
        ["Husmeador taltuza"] = 6900,
    },
    ["Portal: Forjaz"] = {
        ["MAGE"] = 11416,
    },
    ["Invocar elemental de fuego"] = {
        ["Elemental de fuego"] = 8985,
        ["Conjurador Escarlata"] = 8985,
    },
    ["Descarga de Escarcha"] = {
        ["MAGE"] = 25304,
        ["Llamamareas menor múrloc"] = 9672,
        ["Hechicero esquelético"] = 9672,
        ["Mago Aplastacresta"] = 9672,
        ["Magus del Sindicato"] = 9672,
        ["Hechicero Escarlata"] = 9672,
        ["Adepto Caramuerte"] = 9672,
        ["Sacerdote de las mareas Brazanegra"] = 9672,
        ["Aquamántivo Crepuscular"] = 9672,
        ["Tejemagia dracónico"] = 9672,
        ["Hechicera infernal"] = 20297,
        ["Mago esquelético"] = 20792,
        ["Tejebruma Pellejomusgo"] = 20792,
        ["Zahorí Dalaran"] = 20792,
        ["Archimago Ataeric"] = 20792,
        ["Hechicera Escama Tormentosa"] = 20792,
        ["Kuz"] = 20792,
        ["Delmanis el Odiado"] = 20792,
        ["Hechicera Colafuria"] = 20792,
        ["Geomántico Cortaviento"] = 20792,
        ["Señor de las Mareas Escama de Sal"] = 20822,
        ["Barón Vardus"] = 20822,
        ["Magus Puño de Roca"] = 20822,
        ["Perito Mostacho Seco"] = 20822,
        ["Señor Ancaniebla"] = 20822,
        ["Escarbador Makrinni"] = 20822,
        ["Eliza"] = 20819,
        ["Teurgista Dalaran"] = 20819,
        ["Hechicera Filozante"] = 20819,
        ["Consejero de Trabalomas"] = 20806,
        ["Sarilus Fuenteviciosa"] = 20806,
        ["Supervisor Gravamorro"] = 20806,
        ["Hechicera Strashaz"] = 12737,
        ["Barón Aquanis"] = 15043,
        ["Lokholar el Señor del Hielo"] = 21369,
        ["Zahorí bribón Defias"] = 13322,
        ["Tirahuesos Ojosombra"] = 13322,
        ["Neófito Escarlata"] = 13322,
        ["Aprendiz Dalaran"] = 13322,
        ["Hechicera Viento Furioso"] = 13322,
    },
    ["Guantes de paño brillante"] = {
        Professions = 18415,
    },
    ["Hacha de hierro macizo"] = {
        Professions = 3498,
    },
    ["Sobrehombros de escamas doradas"] = {
        Professions = 3505,
    },
    ["Tótem de limpieza de enfermedades"] = {
        ["SHAMAN"] = 8170,
    },
    ["Maldición de agotamiento"] = {
        ["WARLOCK"] = 18223,
    },
    ["Carga de seforio grande"] = {
        Professions = 3972,
    },
    ["Resistencia de fortuna de las Sombras de Sayge"] = {
        ["Sayge"] = 23769,
    },
    ["Encantar brazales: fuerza inferior"] = {
        Professions = 13536,
    },
    ["Brazales de escamas de mitril"] = {
        Professions = 9937,
    },
    ["Yelmo de escamas de tortuga"] = {
        Professions = 10552,
    },
    ["Bolsa de paño rúnico"] = {
        Professions = 18405,
    },
    ["Escudo antimagia"] = {
        ["Caminalunas Colmillo Oscuro"] = 7121,
    },
    ["Bolsa de lino"] = {
        Professions = 3755,
    },
    ["Yelmo de torio"] = {
        Professions = 16653,
    },
    ["Actitud de batalla"] = {
        ["WARRIOR"] = 2457,
        ["Guerrero esquelético"] = 7165,
        ["Alfa de Crestagrana"] = 7165,
        ["Guerrero Branquiazul"] = 7165,
        ["Déspota Mo'grosh"] = 7165,
        ["Maestro excavador"] = 7165,
        ["Dextren Tutor"] = 7165,
        ["Recluso Defias"] = 7165,
        ["Expedicionario Naga"] = 7165,
        ["Guerrero Bosque Negro"] = 7165,
        ["Matón Crepuscular"] = 7165,
        ["Defensor Crines de Acero"] = 7165,
        ["Lok Aterraorcos"] = 7165,
        ["Saltacharco Gapo Salino"] = 7165,
        ["Ursa Maderiza"] = 7165,
        ["Aligar el Torturador"] = 7165,
        ["Ursa Piel de Cardo"] = 7165,
        ["Ruuzel"] = 7165,
        ["Señor Supremo Colmicarnero"] = 7165,
        ["Jabaguardia Rajacieno"] = 7165,
        ["Señor de Batalla Kolkar"] = 7165,
        ["Guerrero Filozante"] = 7165,
        ["Guardia Eduardo"] = 7165,
        ["Guardia Jarad"] = 7165,
        ["Guardia Kahil"] = 7165,
        ["Guardia Lana"] = 7165,
        ["Guardia Narrisha"] = 7165,
        ["Guardia Tark"] = 7165,
        ["Khan Dez'hepah"] = 7165,
        ["Khan Shaka"] = 7165,
        ["Capitán Colmillo Plano"] = 7165,
        ["Supervisor Asas"] = 7165,
        ["Takk el Saltarín"] = 7165,
        ["Hagg Taurruina"] = 7165,
        ["Suingart Cuerolanza"] = 7165,
        ["Asaltante Defias"] = 7165,
        ["Paria loco"] = 7165,
        ["Gelihast"] = 7165,
        ["Evacuado de Gnomeregan"] = 7165,
    },
    ["Reducido a escombros"] = {
        ["Esclavo elemental"] = 3673,
    },
    ["Encantar capa: protección menor"] = {
        Professions = 7771,
    },
    ["Espada fanática"] = {
        ["Fanático Filo Ardiente"] = 5262,
    },
    ["Paranoia"] = {
        Pet = 19480,
    },
    ["Armadura de cría verde"] = {
        Professions = 9197,
    },
    ["Invocación de Lotwil"] = {
        ["Lotwil Veriatus"] = 5001,
    },
    ["Botines del Alba"] = {
        Professions = 23705,
    },
    ["Hoja serpentina de jade"] = {
        Professions = 3493,
    },
    ["Consumir Sombras"] = {
        Pet = 17854,
    },
    ["Control de batalla"] = {
        ["Sobrestante Defias"] = 5115,
        ["Supervisor de Trabalomas"] = 5115,
        ["Capataz Hierro Negro"] = 5115,
        ["Supervisor de Nethergarde"] = 5115,
        ["Jinete de muerciélagos Gurubashi"] = 5115,
    },
    ["Sacrificio demoníaco"] = {
        ["WARLOCK"] = 18788,
    },
    ["Guanteletes de tormenta"] = {
        Professions = 16661,
    },
    ["Rey de los Gordok"] = {
        ["Mizzle el Astuto"] = 22799,
    },
    ["Encantar brazales: espíritu excelente"] = {
        Professions = 20009,
    },
    ["Leotardos de cuero salvaje"] = {
        Professions = 10572,
    },
    ["Bombachos de seda carmesíes"] = {
        Professions = 8799,
    },
    ["Cinturón de cuero verde"] = {
        Professions = 3774,
    },
    ["Habla con las cabezas"] = {
        ["Kin'weelay"] = 3644,
    },
    ["Talega de seda negra"] = {
        Professions = 6695,
    },
    ["Disparo de conmoción"] = {
        ["HUNTER"] = 5116,
    },
    ["Elixir de entereza menor"] = {
        Professions = 2334,
    },
    ["Coraza de Forjaz"] = {
        Professions = 8367,
    },
    ["Succión de alma"] = {
        ["Sirviente Corvozarpa"] = 7290,
        ["Fizzle Necromenta"] = 7290,
        ["Azshir el Insomne"] = 9373,
    },
    ["Grito desafiante"] = {
        ["WARRIOR"] = 1161,
    },
    ["Aura de disparo certero"] = {
        ["HUNTER"] = 19506,
    },
    ["Nube de veneno"] = {
        ["Aku'mai"] = 3815,
        ["Dorsacerado"] = 3815,
    },
    ["Trabazón helado"] = {
        ["Viejo Barbahielo"] = 3145,
    },
    ["Guantes de mitril ornamentado"] = {
        Professions = 9950,
    },
    ["Toque enfermizo"] = {
        ["Muerto putrefacto"] = 3234,
        ["Cuerpo estragado"] = 3234,
        ["Muerte hambrienta"] = 3234,
        ["Horror dergarbado"] = 3234,
    },
    ["Pantalones de gran vudú"] = {
        Professions = 10560,
    },
    ["Cohete, GRANDE ROJO"] = {
        ["Fuegos artificiales de Pat GRAN ROJO"] = 26354,
    },
    ["Poción de ira poderosa"] = {
        Professions = 17552,
    },
    ["Explosión mental"] = {
        ["PRIEST"] = 10947,
        ["Visión encantada"] = 13860,
        ["Sirena Strashaz"] = 15587,
        ["Señor Crepuscular Kelris"] = 15587,
    },
    ["Nova de hielo"] = {
        ["Latigador de fase"] = 22519,
    },
    ["Encantar pechera: salud sublime"] = {
        Professions = 20026,
    },
    ["Llamar a destructor infernal"] = {
        ["El Pollo Grande"] = 23056,
        ["Niby el Todopoderoso"] = 23056,
    },
    ["Tótem Nexo Terrestre"] = {
        ["SHAMAN"] = 2484,
        ["Tótem de la tierra"] = 15786,
        ["Chamán Ferrohondo"] = 15786,
        ["Primalista Thurloga"] = 15786,
    },
    ["Espíritu divino"] = {
        ["PRIEST"] = 27841,
    },
    ["Puntos de honor +50"] = {
        ["Belisario de la Horda"] = 24960,
        ["Brigadier general de la Alianza"] = 24960,
    },
    ["Guantes de maestría en hechizos"] = {
        Professions = 18454,
    },
    ["Monóculo de artesano"] = {
        Professions = 3966,
    },
    ["Mustiar"] = {
        ["Emisario Roman'khan"] = 23772,
    },
    ["Primeros auxilios"] = {
        ["Médico Tamberlyn"] = 7162,
        ["Médico Helaina"] = 7162,
        ["Hermano Malach"] = 7162,
    },
    ["Pesadilla venenosa"] = {
        ["Kayneth Ventoleve"] = 6354,
    },
    ["Botas de bronce férreas"] = {
        Professions = 7817,
    },
    ["Combustible de cohete goblin"] = {
        Professions = 11456,
    },
    ["Brazales de escamas de múrloc"] = {
        Professions = 6705,
    },
    ["Poción de rejuvenecimiento sublime"] = {
        Professions = 22732,
    },
    ["Capa de cuero refinado"] = {
        Professions = 2159,
    },
    ["Nova de Fuego"] = {
        ["Chamán Cabellesangre"] = 11969,
        ["Magistrado Ladoquemao"] = 11969,
        ["Elemental de magma"] = 11970,
        ["Caminallamas mecanizado"] = 11970,
        ["Taragaman el Hambriento"] = 11970,
        ["Mago sangriento Thalnos"] = 12470,
    },
    ["Control mental"] = {
        ["PRIEST"] = 10912,
    },
    ["Revivir"] = {
        ["Espíritu encadenado"] = 24341,
    },
    ["Tubo de cobre"] = {
        Professions = 3924,
    },
    ["Muñequeras glaciales"] = {
        Professions = 28209,
    },
    ["Fundir hierro"] = {
        Professions = 3307,
    },
    ["Poción de velocidad de nado"] = {
        Professions = 7841,
    },
    ["Elixir de poder de Fuego superior"] = {
        Professions = 26277,
    },
    ["Separación"] = {
        ["HUNTER"] = 14273,
    },
    ["Gancho"] = {
        ["Chok'sul"] = 18072,
        ["Taragaman el Hambriento"] = 18072,
        ["Atracador de guerra"] = 10966,
        ["Arnak Tótem Siniestro"] = 10966,
    },
    ["Invocar elementales de agua"] = {
        ["Elemental de agua invocado"] = 20681,
        ["Lady Jaina Valiente"] = 20681,
    },
    ["Bolsa de demonio Xandivious"] = {
        ["Xandivious"] = 25791,
    },
    ["Madeja de paño rúnico"] = {
        Professions = 18401,
    },
    ["Crear liquidador de pétalos de sangre"] = {
        ["Larion"] = 22565,
    },
    ["Flema tóxica"] = {
        ["Escupeveneno Musgondo"] = 7951,
        ["Escupeveneno Colmilloumbrío"] = 7951,
        ["Venala descarriada"] = 7951,
    },
    ["Cohete verde grande"] = {
        Professions = 26421,
    },
    ["Leotardos volcánicos"] = {
        Professions = 19059,
    },
    ["Pantalones de escamas de mitril"] = {
        Professions = 9931,
    },
    ["Rayo descombobulador"] = {
        Professions = 3959,
    },
    ["Encantar pechera: estadísticas menores"] = {
        Professions = 13626,
    },
    ["Provocar"] = {
        ["WARRIOR"] = 355,
    },
    ["Lanzar lodo"] = {
        ["Ardo Patasoez"] = 3650,
        ["Gnoll Fangorroca"] = 3650,
    },
    ["Encantar botas: aguante superior"] = {
        Professions = 20020,
    },
    ["Beber poción menor"] = {
        ["Batidor múrloc"] = 3368,
        ["Herborista Zarparrío"] = 3368,
    },
    ["Capa de paño cenizo"] = {
        Professions = 18418,
    },
    ["Invocar larva Gorishi"] = {
        ["Larva Gorishi"] = 14206,
        ["Huevo Gorishi"] = 14206,
    },
    ["Jubón de lino rojo"] = {
        Professions = 7629,
    },
    ["Diseño de cilindro de mitril taraceado"] = {
        Professions = 12895,
    },
    ["Supercarga"] = {
        ["Técnico leproso"] = 10732,
        ["Maquinista leproso"] = 10732,
    },
    ["Purificar"] = {
        ["PALADIN"] = 1152,
    },
    ["Puñalada venenosa"] = {
        ["Correcostas múrloc"] = 7357,
        ["Bandido Zarparrío"] = 7357,
    },
    ["Truco o trato"] = {
        ["Tabernero Farley"] = 24751,
        ["Tabernero Belm"] = 24751,
        ["Tabernero Helbrek"] = 24751,
        ["Tabernero Anderson"] = 24751,
        ["Tabernero Shay"] = 24751,
        ["Tabernero Boorand Ventollano"] = 24751,
        ["Tabernero Brebefuego"] = 24751,
        ["Tabernero Renee"] = 24751,
        ["Tabernero Thulbek"] = 24751,
        ["Tabernero Janene"] = 24751,
        ["Tabernero Brianna"] = 24751,
        ["Tabernero Hogartufa"] = 24751,
        ["Tabernero Saelienne"] = 24751,
        ["Tabernero Keldamyr"] = 24751,
        ["Tabernero Shaussiy"] = 24751,
        ["Tabernero Kimlya"] = 24751,
        ["Tabernero Bates"] = 24751,
        ["Tabernera Allison"] = 24751,
        ["Tabernero Norman"] = 24751,
        ["Tabernero Pala"] = 24751,
        ["Tabernero Kauth"] = 24751,
        ["Tabernero Trelayne"] = 24751,
        ["Tabernero Wiley"] = 24751,
        ["Tabernero Skindle"] = 24751,
        ["Tabernero Grosk"] = 24751,
        ["Tabernero Gryshka"] = 24751,
        ["Posadero Karakul"] = 24751,
        ["Tabernera Byula"] = 24751,
        ["Tabernero Jayka"] = 24751,
        ["Tabernero Fizzgrimble"] = 24751,
        ["Tabernero Shyria"] = 24751,
        ["Tabernera Greul"] = 24751,
        ["Tabernero Thulfram"] = 24751,
        ["Tabernera Heather"] = 24751,
        ["Tabernero Shul'kar"] = 24751,
        ["Tabernero Abegwa"] = 24751,
        ["Tabernero Lyshaerya"] = 24751,
        ["Tabernero Sikewa"] = 24751,
        ["Tabernero Abeqwa"] = 24751,
        ["Tabernero Vizzie"] = 24751,
        ["Tabernero Kaylisk"] = 24751,
        ["Lard"] = 24751,
        ["Calandrath"] = 24751,
        ["Tabernero Faralia"] = 24751,
    },
    ["Sello de Rectitud"] = {
        ["PALADIN"] = 20154,
    },
    ["Encantar botas: aguante menor"] = {
        Professions = 7863,
    },
    ["Encantar capa: Resistencia al Fuego"] = {
        Professions = 13657,
    },
    ["Veneno de escórpido"] = {
        Pet = 24587,
    },
    ["Flema de llamas"] = {
        ["Roc de fuego"] = 11021,
    },
    ["Ola de sanación"] = {
        ["SHAMAN"] = 25357,
        ["Oráculo Hakkari"] = 15982,
        ["Místico Cabellesangre"] = 11986,
        ["Místico Machacacráneos"] = 11986,
        ["Místico Pellejomusgo"] = 11986,
        ["Chamán Piel de Cardo"] = 11986,
        ["Oráculo Arkkoran"] = 11986,
        ["Chamán Furia Ardiente"] = 11986,
        ["Chamán Ferrohondo"] = 12492,
    },
    ["Botella de veneno"] = {
        ["Batidor Ratatúnel"] = 7365,
    },
    ["Encantar escudo: espíritu"] = {
        Professions = 13659,
    },
    ["Botas quiméricas"] = {
        Professions = 19063,
    },
    ["Sobrehombros Albar"] = {
        Professions = 16660,
    },
    ["Cohete rojo pequeño"] = {
        Professions = 26418,
    },
    ["Martillo de justicia"] = {
        ["PALADIN"] = 10308,
    },
    ["Escudo de las Sombras"] = {
        ["Krethis Sombravolta"] = 12040,
    },
    ["Invocar estruendor de tierra"] = {
        ["Estruendor de piedra"] = 8270,
        ["Geomántico Rajacieno"] = 8270,
        ["Clamor de Tierra Halmgar"] = 8270,
        ["Roogug"] = 8270,
    },
    ["Redes atrapantes"] = {
        ["Trepador de las llanuras"] = 4962,
        ["Trepador de las llanuras gigante"] = 4962,
        ["Bestia de la cripta"] = 4962,
    },
    ["Jubón de lana verde"] = {
        Professions = 2399,
    },
    ["Camisa de espadachín roja"] = {
        Professions = 8489,
    },
    ["Vestimentas de fe verdadera"] = {
        Professions = 18456,
    },
    ["Muro de escudo"] = {
        ["WARRIOR"] = 871,
    },
    ["Hambre enloquecedora"] = {
        ["Lobo invernal hambriento"] = 3151,
    },
    ["Aparición de dracónido negro"] = {
        ["Dracónido negro"] = 22654,
        ["Black Drakonid Spawner"] = 22654,
    },
    ["Poción de sanación superior"] = {
        Professions = 7181,
    },
    ["Aullido sangriento"] = {
        ["Aullasangre Furia Lunar"] = 3264,
        ["Serena Sangrepluma"] = 3264,
        ["Aullador Fangarra"] = 3264,
        ["Rugesangre el Acechador"] = 3264,
        ["Viejo Saltariscos"] = 3264,
    },
    ["Poción de fuerza II"] = {
        ["Herborista Zarparrío"] = 3369,
    },
    ["Cartera de Cenarius"] = {
        Professions = 27725,
    },
    ["Toga de tejido mágico negra"] = {
        Professions = 12050,
    },
    ["Encantar pechera: maná excelente"] = {
        Professions = 13917,
    },
    ["Elixir Arcano superior"] = {
        Professions = 17573,
    },
    ["Perforar armadura"] = {
        ["Engullidor"] = 12097,
        ["Garrafilada"] = 12097,
        ["Duriel Fuegolunar"] = 12097,
        ["Minero klbold"] = 6016,
        ["Ruklar el Trampero"] = 6016,
        ["Masticahuesos Oseral"] = 6016,
        ["Hogger"] = 6016,
        ["Brack"] = 6016,
        ["Minero Defias"] = 6016,
        ["Minero de Ventura y Cía."] = 6016,
        ["Cavador kóbold"] = 6016,
        ["Desgarrador Brezomadera"] = 6016,
        ["Trillador de la Costa Oscura"] = 6016,
        ["Trillador de la Costa Oscura"] = 6016,
        ["Campesino de Trabalomas"] = 6016,
        ["Filón Colafuria"] = 6016,
        ["Invasor silítido"] = 6016,
        ["Cavapozos caparallante"] = 6016,
        ["Desgarrador Parramustia"] = 6016,
        ["Mineroaberto Defias"] = 6016,
    },
    ["Abrazo vampírico"] = {
        ["PRIEST"] = 15286,
    },
    ["Sangre maldita"] = {
        ["Agam'ar putrefacto"] = 8268,
        ["Agam'ar putrefacto"] = 8267,
    },
    ["Misiles Arcanos"] = {
        ["MAGE"] = 25345,
    },
    ["Salva de descarga de las Sombras"] = {
        ["Varimathras"] = 20741,
        ["Suma Sacerdotisa Hai'watna"] = 14887,
        ["Balgaras el Hediondo"] = 9081,
        ["Morloch"] = 17228,
    },
    ["Capa de lana gruesa"] = {
        Professions = 3844,
    },
    ["Aparición de dracónido azul"] = {
        ["Dracónido azul"] = 22658,
        ["Blue Drakonid Spawner"] = 22658,
    },
    ["Cólera de las bestias"] = {
        ["HUNTER"] = 19574,
    },
    ["Guantes de paño cenizo"] = {
        Professions = 18412,
    },
    ["Tótem Nova de Fuego"] = {
        ["SHAMAN"] = 11315,
    },
    ["Jubón de cuero de montañero"] = {
        Professions = 3762,
    },
    ["Clon"] = {
        ["Ectoplasma clonado"] = 7952,
        ["Ectolasma devorador"] = 7952,
        ["Moco clonado"] = 14146,
        ["Moco primario"] = 14146,
    },
    ["Yelmo de mitril ornamentado"] = {
        Professions = 9980,
    },
    ["Golpe bajo"] = {
        ["ROGUE"] = 1833,
    },
    ["Encantar pechera: estadísticas inferiores"] = {
        Professions = 13700,
    },
    ["Esbirro de Morganth"] = {
        ["Esbirro de Morganth"] = 3611,
        ["Morganth"] = 3611,
    },
    ["Frasco de los titanes"] = {
        Professions = 17635,
    },
    ["Guarda de llama presurosa"] = {
        ["Mago renegado Defias"] = 4979,
        ["Místico Cabellesangre"] = 4979,
        ["Guarda esquelético"] = 4979,
        ["Geólogo de Ventura y Cía."] = 4979,
        ["Geomántico Rompecantos"] = 4979,
        ["Geomántico Ratatúnel"] = 4979,
        ["Temófogo Puñastilla"] = 4979,
        ["Evocador Defias"] = 4979,
        ["Mago Dalaran"] = 4979,
        ["Grel'borg el Avaro"] = 4979,
        ["Geomántico Lomopelo"] = 4979,
        ["Zahorí Defias"] = 4979,
        ["Supervisor Cozzle"] = 4979,
    },
    ["Botas Nochefugaz"] = {
        Professions = 10558,
    },
    ["Yeti mecánico tranquilo"] = {
        Professions = 26011,
    },
    ["Leotardos de escamas doradas"] = {
        Professions = 3507,
    },
    ["Rifle de gran calibre de mitril"] = {
        Professions = 12614,
    },
    ["Espada corta de bronce"] = {
        Professions = 2742,
    },
    ["Esbirros de Malathrom"] = {
        ["Fangoso"] = 3537,
        ["Lord Malathrom"] = 3537,
    },
    ["Aceite de maná luminoso"] = {
        Professions = 25130,
    },
    ["Maldición de las tribus"] = {
        ["Centauro maldito"] = 21048,
    },
    ["Armonización con el Núcleo"] = {
        ["Lothos Levantagrietas"] = 22877,
    },
    ["Arma Estigma de Escarcha"] = {
        ["SHAMAN"] = 16356,
    },
    ["Invocar cadáver deshecho"] = {
        ["Cadáver deshecho"] = 16324,
        ["Cadáver devastado"] = 16324,
    },
    ["Capa de cuero de Escarcha"] = {
        Professions = 9198,
    },
    ["Flaqueza"] = {
        ["Necrófago desalmado"] = 12530,
        ["Zul'Lor"] = 12530,
    },
    ["Roca desmenuzadora"] = {
        ["Esclavo elemental"] = 3672,
    },
    ["Devastar"] = {
        ["DRUID"] = 9867,
        ["Quijaforte Aku'mai"] = 8391,
        ["Murciumbrío mayor"] = 3242,
        ["Murciumbrío vampiro"] = 3242,
        ["Sarnagarra"] = 3242,
        ["Oso Cardo"] = 3242,
        ["Grisoso cardo"] = 3242,
        ["Madre del cubil"] = 3242,
        ["Cachorro Cardo"] = 3242,
    },
    ["Pantalones de tela vil"] = {
        Professions = 18419,
    },
    ["Curar envenenamiento"] = {
        ["DRUID"] = 8946,
        ["SHAMAN"] = 526,
    },
    ["Armadura de Escarcha"] = {
        ["MAGE"] = 7301,
    },
    ["Hacha curva de las Sombras"] = {
        Professions = 3500,
    },
    ["Forma felina"] = {
        ["DRUID"] = 768,
        ["Lord Melenis"] = 5759,
        ["Centinela Amarassan"] = 5759,
    },
    ["Revés"] = {
        ["Kazon"] = 6253,
        ["Camorrista"] = 6253,
        ["Presidiario Defias"] = 6253,
        ["Destructor Aplastacresta"] = 6253,
        ["Otto"] = 6253,
        ["Brigadier del Mar del Sur"] = 6253,
        ["Prospector Khazgorm"] = 6253,
        ["Matón Renegado"] = 6253,
        ["Tazan"] = 6253,
        ["Guardia Minafría"] = 6253,
        ["Guardia Minafría veterano"] = 6253,
    },
    ["Invocar ráfaga de lava H"] = {
        ["Rata del Núcleo"] = 21906,
    },
    ["Maldición de Thule"] = {
        ["Gnoll Putrepellejo"] = 3237,
        ["Bastardo Putrepellejo"] = 3237,
        ["Ojo de gusano"] = 3237,
        ["Asaltaclaros Putrepellejo"] = 3237,
        ["Místico Putrepellejo"] = 3237,
        ["Tosco Putrepellejo"] = 3237,
        ["Siembrapestes Putrepellejo"] = 3237,
        ["Asaltatumbas Putrepellejo"] = 3237,
        ["Salvaje Putrepellejo"] = 3237,
        ["Putrepellejo enfurecido"] = 3237,
    },
    ["Toga Bengala del Núcleo"] = {
        Professions = 23666,
    },
    ["Pantalones de tejido mágico rojos"] = {
        Professions = 12060,
    },
    ["Máscara de tejido de sombra"] = {
        Professions = 12086,
    },
    ["Hacha de mitril pesado"] = {
        Professions = 9993,
    },
    ["Combustión"] = {
        ["MAGE"] = 11129,
    },
    ["Bramido temible"] = {
        ["Canisangre"] = 13692,
        ["Diemetradón"] = 13692,
        ["Tirosangre"] = 13692,
    },
    ["Lanzar"] = {
        ["Bom'bay"] = 16716,
    },
    ["Piedra filosofal"] = {
        Professions = 11459,
    },
    ["Poción de maná excelente"] = {
        Professions = 17553,
    },
    ["Invocar abisario"] = {
        ["WARLOCK"] = 697,
    },
    ["Lágrima de Razelikh I"] = {
        ["Razelikh el Rapiñador"] = 10864,
    },
    ["Dinamita para tontos"] = {
        Professions = 8339,
    },
    ["Maldición de temeridad"] = {
        ["WARLOCK"] = 11717,
        ["Destripadora Furia Sangrienta"] = 16231,
    },
    ["Rezo de sanación"] = {
        ["PRIEST"] = 25316,
    },
    ["Botas de seda de araña"] = {
        Professions = 3855,
    },
    ["Sacrificio"] = {
        Pet = 19443,
    },
    ["Puesta a punto"] = {
        ["Técnico leproso"] = 10348,
        ["Maquinista leproso"] = 10348,
    },
    ["Vara de cobre"] = {
        Professions = 6217,
    },
    ["Leotardos de hierro verdes"] = {
        Professions = 3506,
    },
    ["Diadema de tejido onírico"] = {
        Professions = 12092,
    },
    ["Gubia"] = {
        ["ROGUE"] = 11286,
    },
    ["Descarga de radiación"] = {
        ["Invasor irradiado"] = 9771,
        ["Saqueador irradiado"] = 9771,
    },
    ["Dinamita densa"] = {
        Professions = 23070,
    },
    ["Fatiga febril"] = {
        ["Águila ratonera Mesa"] = 8139,
        ["Águila ratonera Mesa"] = 8139,
        ["Melenargenta sarnoso"] = 8139,
        ["Geomántico Cortaviento"] = 8139,
        ["Señor Supremo Cortaviento"] = 8139,
        ["Habitabosta Arkkoran"] = 8139,
    },
    ["Jubón de tejido mágico negro"] = {
        Professions = 12048,
    },
    ["Golpe de cruzado"] = {
        ["Galante Escarlata"] = 14517,
        ["Comandante Escarlata Mograine"] = 14518,
    },
    ["Vara rúnica de oro"] = {
        Professions = 13628,
    },
    ["Elixir Arcano"] = {
        Professions = 11461,
    },
    ["Sobrehombros Hierro Negro"] = {
        Professions = 15295,
    },
    ["Crear pergamino"] = {
        ["Collin Mauren"] = 6671,
    },
    ["Efecto de polvo de reno"] = {
        ["Metzen el Reno"] = 25952,
    },
    ["Crear piedra de salud (sublime)"] = {
        ["WARLOCK"] = 11730,
    },
    ["Venganza"] = {
        ["Vengador Piel de Cardo"] = 8602,
    },
    ["Toga de paño rúnico"] = {
        Professions = 18406,
    },
    ["Desenfreno"] = {
        ["Agathelos el Furioso"] = 8285,
    },
    ["Guanteletes de cobre con gemas"] = {
        Professions = 3325,
    },
    ["Piedra de pulir férrea"] = {
        Professions = 3320,
    },
    ["Nube de enfermedad"] = {
        ["Necrófago malsano"] = 17742,
    },
    ["Aleteo"] = {
        ["Somnus"] = 12882,
    },
    ["Aparición de espíritu"] = {
        ["Rutagrana el Corrompido"] = 17321,
        ["Espíritu de Villa Darrow"] = 17321,
        ["Espíritu de Garraluna"] = 17321,
        ["Espíritu Corcelorror"] = 17321,
        ["Destrero caído de Atracoscuro"] = 17321,
        ["Vasallo de la Mano"] = 17321,
    },
    ["Garrote"] = {
        ["ROGUE"] = 11290,
    },
    ["Poción de gran ira"] = {
        Professions = 6618,
    },
    ["Capa de cuero salvaje"] = {
        Professions = 10574,
    },
    ["Manto Bengala del Núcleo"] = {
        Professions = 20848,
    },
    ["Bolsa de paño rúnico encantada"] = {
        Professions = 27659,
    },
    ["Polimorfia"] = {
        ["MAGE"] = 12826,
        ["Zahorí del Sindicato"] = 13323,
        ["Alcaide Belamoore"] = 13323,
        ["Arcanista Doan"] = 13323,
        ["Capitán Balinda Corapernal"] = 13323,
    },
    ["Invocar can escarlata"] = {
        ["Can Escarlata"] = 17164,
        ["Cazador Escarlata"] = 17164,
    },
    ["Piedra de afilar pesada"] = {
        Professions = 2674,
    },
    ["Activador Theka"] = {
        ["Integrista Lor'Khan"] = 24172,
        ["Integrista Zath"] = 24172,
    },
    ["Aliento de relámpagos"] = {
        Pet = 25012,
        ["Quimera venfresco"] = 15797,
    },
    ["Capa helada"] = {
        Professions = 3862,
    },
    ["Emboscada"] = {
        ["ROGUE"] = 11269,
    },
    ["Invocar devastador Magram"] = {
        ["Devastador no-muerto"] = 18166,
        ["Necromántico paria"] = 18166,
    },
    ["Tubo de bronce"] = {
        Professions = 3938,
    },
    ["Espíritus encantados"] = {
        ["Servidor encantado"] = 7057,
    },
    ["Toga de tejido de Escarcha"] = {
        Professions = 18404,
    },
    ["Jubón de lino azul"] = {
        Professions = 7630,
    },
    ["Triturar"] = {
        ["DRUID"] = 9830,
        ["Machacador Anca Vil"] = 3252,
    },
    ["Encantar pechera: amortiguación inferior"] = {
        Professions = 13538,
    },
    ["Llamar a Tirosangre"] = {
        ["Tirosangre"] = 18262,
        ["Cazador Radley"] = 18262,
    },
    ["Crear Gambito del Alba"] = {
        ["Betina Bigglezink"] = 18367,
    },
    ["Poción del druida"] = {
        ["Discípulo de Naralex"] = 8141,
    },
    ["Fiebre muculenta"] = {
        ["Moco muculento"] = 14130,
    },
    ["Detonaciones polimorfas"] = {
        ["Clon polimórfico"] = 28406,
        ["Guerrera Látigo de Ira"] = 28406,
        ["Gritona Látigo de Ira"] = 28406,
        ["Guardia Serpiente Látigo de Ira"] = 28406,
        ["Sirena Látigo de Ira"] = 28406,
    },
    ["Tela envolvente"] = {
        ["Reptador de la cripta"] = 15471,
        ["Sobrestante Nerubiano"] = 15471,
    },
    ["Aliento de llamas"] = {
        ["Narillasanz"] = 9573,
        ["Teremus el Devorador"] = 9573,
        ["Pícaro Draco Negro"] = 9573,
    },
    ["Capa de cuero cosida a mano"] = {
        Professions = 9058,
    },
    ["Disparo de Fuego"] = {
        ["Saboteador del Sindicato"] = 6980,
        ["Explorador Escarlata"] = 6979,
    },
    ["Contrapeso burdo"] = {
        Professions = 3116,
    },
    ["Cinturón de estrella"] = {
        Professions = 3864,
    },
    ["Viruela silítida"] = {
        ["Invasor silítido"] = 8137,
        ["Avispa Gorishi"] = 8137,
        ["Obrero Gorishi"] = 8137,
        ["Atracador Gorishi"] = 8137,
        ["Aguijonero Gorishi"] = 8137,
        ["Tunelador Gorishi"] = 8137,
        ["Guardián de la colmena Gorishi"] = 8137,
        ["Reina de la colmena Gorishi"] = 8137,
    },
    ["Guantes de lino gruesos"] = {
        Professions = 3840,
    },
    ["Bolsa de encantamiento grande"] = {
        Professions = 27660,
    },
    ["Leotardos rúnicos estigios"] = {
        Professions = 24901,
    },
    ["Sobrehombros de tejido de sombra"] = {
        Professions = 12076,
    },
    ["Encantar capa: agilidad inferior"] = {
        Professions = 13882,
    },
    ["Coraza de dragontina azul"] = {
        Professions = 19077,
    },
    ["Pulverizador de llamas"] = {
        ["Elemental inferno"] = 10733,
        ["Caminallamas mecanizado"] = 10733,
        ["Caminafuego Thaurissan"] = 10733,
    },
    ["Trampa de inmolación"] = {
        ["HUNTER"] = 14305,
    },
    ["Corte entorpecedor"] = {
        ["Nelson el Amable"] = 23279,
    },
    ["Lazos compartidos"] = {
        ["Esclavo Corvozarpa"] = 7761,
    },
    ["Efecto de escudo violento"] = {
        ["Subjefe Kurzen"] = 3260,
        ["Guardia escudero Dalaran"] = 3260,
    },
    ["Gran flagelo"] = {
        ["Señor Supremo Mok'Morkk"] = 6749,
    },
    ["Teletransporte a: Forjaz"] = {
        ["MAGE"] = 3562,
    },
    ["Catalizador nocivo"] = {
        ["Escórpido corrupto"] = 5413,
    },
    ["Escudo de maná"] = {
        ["MAGE"] = 10193,
    },
    ["Invocar a Rend Puño Negro"] = {
        ["Jefe de Guerra Desgarro Puño Negro"] = 16328,
        ["Gyth"] = 16328,
    },
    ["En picado"] = {
        Pet = 23148,
    },
    ["Metal de arrabio"] = {
        ["Gilnid"] = 5213,
        ["Smoldar"] = 5213,
    },
    ["Ácido perforatierra"] = {
        ["Cavatierra"] = 18070,
    },
    ["Choque de aura"] = {
        ["Estridador sombrío"] = 14538,
    },
    ["Espíritu del viento"] = {
        ["Quebrantadientes"] = 16618,
    },
    ["Golpe craneal"] = {
        ["Hogger"] = 6730,
        ["Tormentero"] = 6730,
        ["Sacudidor caparallante"] = 6730,
    },
    ["Veneno corrosivo"] = {
        ["Trepamusgo mayor"] = 3396,
        ["Trepamusgo gigante"] = 3396,
        ["Trepamusgo del bosque"] = 3396,
        ["Bestia de savia corrosiva"] = 3396,
        ["Quimera Fugalante"] = 3396,
        ["Matriarca Quimera"] = 3396,
    },
    ["Faltriquera para munición de cuero pesado"] = {
        Professions = 9194,
    },
    ["Cohete, GRANDE VERDE"] = {
        ["Fuegos artificiales de Pat GRAN VERDE"] = 26352,
    },
    ["Esencia de dolor"] = {
        ["ROGUE"] = 2930,
    },
    ["Mordedura del alma"] = {
        ["Comealmas Vilrama"] = 11016,
        ["Comealmas Furiarena"] = 11016,
    },
    ["Rendirse"] = {
        ["Hulfnar Petrotótem"] = 17648,
    },
    ["Camisa de esmoquin"] = {
        Professions = 12085,
    },
    ["Hoja fantasma"] = {
        Professions = 10007,
    },
    ["Eliminar maldición inferior"] = {
        ["MAGE"] = 475,
    },
    ["Subidón de adrenalina"] = {
        ["ROGUE"] = 13750,
    },
    ["Llamas abrasadoras"] = {
        ["Embajador Infernus"] = 9552,
    },
    ["Guantes de cuero livianos"] = {
        Professions = 9074,
    },
    ["Veneno mortal V"] = {
        ["ROGUE"] = 25347,
    },
    ["Poción de Protección contra la Escarcha"] = {
        Professions = 7258,
    },
    ["Pantalones de cuero cosidos a mano"] = {
        Professions = 2153,
    },
    ["Bolsa de tela lunar"] = {
        Professions = 18445,
    },
    ["Transmutar: vida a tierra"] = {
        Professions = 17565,
    },
    ["Pantalones de cuero maligno"] = {
        Professions = 19083,
    },
    ["Huevo Gorishi"] = {
        ["Huevo Gorishi"] = 14205,
        ["Madrezarpa Zavas"] = 14205,
    },
    ["Capa paracaídas"] = {
        Professions = 12616,
    },
    ["Capa de montañero"] = {
        Professions = 3760,
    },
    ["Luz Sagrada"] = {
        ["PALADIN"] = 25292,
    },
    ["Lanza Fuego líquido"] = {
        ["Grifo nidal"] = 23969,
        ["Jinete de guerra"] = 23969,
    },
    ["Jubón de cuero salvaje"] = {
        Professions = 10544,
    },
    ["Transformador de veraplata"] = {
        Professions = 23071,
    },
    ["Encantar brazales: aguante inferior"] = {
        Professions = 13501,
    },
    ["Hoja tigre de Escarcha"] = {
        Professions = 3497,
    },
    ["Crear piedra de salud"] = {
        ["WARLOCK"] = 5699,
    },
    ["Martillo rúnico de mitril"] = {
        Professions = 10009,
    },
    ["Regeneración frenética"] = {
        ["DRUID"] = 22896,
    },
    ["Equipo de reparación mecánica"] = {
        Professions = 15255,
    },
    ["Bolsa de tejido mágico roja"] = {
        Professions = 12079,
    },
    ["Pellejo basto curtido"] = {
        Professions = 19047,
    },
    ["Tótem Viento Furioso"] = {
        ["SHAMAN"] = 10614,
    },
    ["Hacha de cobre"] = {
        Professions = 2738,
    },
    ["Salva de descarga de Escarcha"] = {
        ["Sirviente Aku'mai"] = 8398,
        ["Alascarcha Hakkari"] = 8398,
        ["Ras Levescarcha"] = 8398,
    },
    ["Marco de bronce"] = {
        Professions = 3953,
    },
    ["Daño de fortuna de las Sombras de Sayge"] = {
        ["Sayge"] = 23768,
    },
    ["Maldición de los Muertobosque"] = {
        ["Guerrero Muertobosque"] = 13583,
        ["Jardinero Muertobosque"] = 13583,
        ["Abrecaminos Muertobosque"] = 13583,
        ["Vigilante del cubil Muertobosque"] = 13583,
        ["Vengador Muertobosque"] = 13583,
        ["Chamán Muertobosque"] = 13583,
        ["Jefe Sangrefauce"] = 13583,
    },
    ["Truhán durmiente"] = {
        ["Truhán borracho"] = 26115,
    },
    ["Bola de Fuego"] = {
        ["MAGE"] = 25306,
        ["Saqueador Defias"] = 19816,
        ["Tejefuego Puñastilla"] = 19816,
        ["Geomántico Gravacal"] = 19816,
        ["Adepto Filo Ardiente"] = 19816,
        ["Cría perdida"] = 11839,
        ["Familiar vil"] = 11921,
        ["Geomántico Crespuscular"] = 14034,
        ["Vermis negro"] = 14034,
        ["Lavaraña superior"] = 11985,
        ["Capitán Balinda Corapernal"] = 12466,
        ["Cría de dragón negro"] = 20793,
        ["Mago renegado Defias"] = 20793,
        ["Geomántico kóbold"] = 20793,
        ["Surena Caledon"] = 20793,
        ["Iniciado Escarlata"] = 20793,
        ["Mago Dalaran"] = 20793,
        ["Hechicera Sangrepluma"] = 20793,
        ["Geomántico Lomopelo"] = 20793,
        ["Geomántico Crines de Acero"] = 20793,
        ["Deforestador de Ventura y Cía."] = 20793,
        ["Invocador Roca Negra"] = 20793,
        ["Gibblewilt"] = 20793,
        ["Geomántico Gogger"] = 20793,
        ["Mago Velasangre"] = 20823,
        ["Alcaide Belamoore"] = 20823,
        ["Feeboz"] = 20823,
        ["Geólogo Hierro Negro"] = 20823,
        ["Adepto Jaedenar"] = 20823,
        ["Zahorí del Sindicato"] = 20815,
        ["Invocador Dalaran"] = 20815,
        ["Clamor de Tierra Gelkis"] = 20815,
        ["Morganth"] = 20811,
        ["Encantador Defias"] = 20811,
        ["Magistrado Ladoquemao"] = 20811,
        ["Geomántico Tótem Siniestro"] = 20811,
        ["Adjurador Defias"] = 9053,
        ["Magiero Defias"] = 9053,
        ["Clamainferno Xavian"] = 9053,
        ["Destructor ardiente"] = 9053,
        ["Magiero Escarlata"] = 9053,
        ["Evocador Escarlata"] = 9053,
        ["Adivinador Escarlata"] = 9053,
        ["Conjurador Escarlata"] = 9053,
        ["Cría abrasadora"] = 9053,
        ["Zahorí Defias"] = 9053,
        ["Perito Forjatiniebla"] = 9053,
        ["Geomántico Caramuerte"] = 9053,
        ["Geomántico Mostacho Blanco"] = 15228,
        ["Expedicionario Minafría"] = 15242,
        ["Expedicionario Ferrohondo"] = 15242,
        ["Brasillas"] = 13375,
        ["Cachorrillo negro"] = 13375,
        ["Cachorrillo hirviente"] = 13375,
    },
    ["Poción de maná inferior"] = {
        Professions = 3173,
    },
    ["Invocar taumaturgo umbrío"] = {
        ["Nigromante esquelético"] = 12258,
        ["Invocador esquelético"] = 12258,
    },
    ["Demonio invocado"] = {
        ["Abisario invocado"] = 7741,
        ["Súcubo invocado"] = 7741,
        ["Manáfago invocado"] = 7741,
        ["Vigilasangre Hakkari"] = 7741,
        ["Supresor pesadilla"] = 7741,
        ["Espíritu estridador"] = 7741,
        ["Arugal"] = 7741,
        ["Espíritu demoníaco"] = 7741,
    },
    ["Encantar capa: resistencia a las Sombras inferior"] = {
        Professions = 13522,
    },
    ["Calzones de lino cosidos a mano"] = {
        Professions = 3842,
    },
    ["Hacha azul relumbrante"] = {
        Professions = 9995,
    },
    ["Crear Citrino de maná"] = {
        ["MAGE"] = 10053,
    },
    ["Irradiado"] = {
        ["Invasor irradiado"] = 9775,
        ["Saqueador irradiado"] = 9775,
    },
    ["Reflector de hielo girohielo"] = {
        Professions = 23077,
    },
    ["Capa de escamas descarriadas"] = {
        Professions = 7953,
    },
    ["Invocar larva silítida"] = {
        ["Larva silítida"] = 6588,
        ["Ovotrepador silítido"] = 6588,
    },
    ["Camisa de lino roja"] = {
        Professions = 2392,
    },
    ["Puntos de honor +2388"] = {
        ["Belisario de la Horda"] = 24966,
        ["Brigadier general de la Alianza"] = 24966,
    },
    ["Lodo fangoso"] = {
        ["Bestia del lodo Brezomadera"] = 5567,
    },
    ["Gallo de batalla gnómico"] = {
        Professions = 12906,
    },
    ["Veneno de aturdimiento mental II"] = {
        ["ROGUE"] = 8694,
    },
    ["Lanzamiento de antorcha"] = {
        ["Supervisor de Bael'dun"] = 6257,
    },
    ["Brebaje especial"] = {
        ["Bom'bay"] = 16712,
    },
    ["Invocar reptador"] = {
        ["Reptador"] = 8656,
        ["Cazador múrloc"] = 8656,
    },
    ["Piedra de afilar elemental"] = {
        Professions = 22757,
    },
    ["Dinamita"] = {
        ["Minero exterior de Ventura y Cía."] = 8800,
    },
    ["Nube enfermiza"] = {
        ["Guardián de Entrañas"] = 12187,
        ["Gordo"] = 12187,
    },
    ["Cargar"] = {
        ["WARRIOR"] = 11578,
        Pet = 27685,
    },
    ["Guantes de escamas de tortuga"] = {
        Professions = 10509,
    },
    ["Efecto de aparición muñeco-diana"] = {
        ["Muñeco-diana"] = 4507,
        ["Muñeco-diana avanzado"] = 4507,
        ["Muñeco-diana de lujo"] = 4507,
        ["Loco de amor"] = 4507,
    },
    ["Recolectar enjambre"] = {
        ["Cosechador silítido"] = 7277,
    },
    ["Mirada de cristal"] = {
        ["Basilisco Morropietra"] = 3635,
        ["Basilisco Espina de Cristal"] = 3635,
        ["Observador Gapo Salino"] = 3635,
        ["Observador Cristaldermo"] = 3635,
        ["Basilisco Rocarroja"] = 3635,
        ["Ojo de la Muerte"] = 3635,
    },
    ["Encantar guantes: equitación"] = {
        Professions = 13947,
    },
    ["Moco corrosivo"] = {
        ["Rondador corrosivo"] = 9459,
    },
    ["Tótem de Magma"] = {
        ["SHAMAN"] = 8190,
    },
    ["Camisa roja con estilo"] = {
        Professions = 3866,
    },
    ["Cinturón de cuero con gemas incrustadas"] = {
        Professions = 3778,
    },
    ["Segador de la cosecha compacto"] = {
        ["Segador de la cosecha compacto"] = 7979,
        ["Maquinista de Ventura y Cía."] = 7979,
        ["Ingeniero Giralesín"] = 7979,
    },
    ["Fundir mitril"] = {
        Professions = 10097,
    },
    ["Recrecimiento"] = {
        ["DRUID"] = 9858,
        ["Oráculo lechúcico lunar"] = 16561,
    },
    ["Pureza"] = {
        ["Charlga Filonavaja"] = 8361,
    },
    ["Lanzatracas de fuegos de artificio"] = {
        Professions = 26443,
    },
    ["Veneno de trepador"] = {
        ["Trepador de cuevas"] = 14532,
    },
    ["Madeja de tejido mágico"] = {
        Professions = 3865,
    },
    ["Destello de Luz"] = {
        ["PALADIN"] = 19943,
    },
    ["Maldición de las Sombras"] = {
        ["WARLOCK"] = 17937,
    },
    ["Velo de Sombras"] = {
        ["Vol'jin"] = 17820,
        ["Tejesombra Luporror"] = 7068,
        ["Muerto angustioso"] = 7068,
        ["Sombra de Felicent"] = 7068,
    },
    ["Poción de purificación"] = {
        Professions = 17572,
    },
    ["Vara rúnica de arcanita"] = {
        Professions = 20051,
    },
    ["Pacto oscuro"] = {
        ["WARLOCK"] = 18938,
    },
    ["Tótem Pillaterra"] = {
        ["Tótem Pillaterra"] = 8376,
        ["Médico brujo Cabellesangre"] = 8376,
        ["Totémico de Rajacieno"] = 8376,
        ["Enraizador Cardizal"] = 8376,
    },
    ["Armadura de cuero verde"] = {
        Professions = 3772,
    },
    ["Lanzar dinamita"] = {
        ["Ingeniero de Ventura y Cía."] = 7978,
        ["Cavador Flamaforja"] = 7978,
        ["Campesino Forjatiniebla"] = 7978,
    },
    ["Llamar a huargo lóbrego"] = {
        ["Lupo lóbrego"] = 7487,
    },
    ["Invocar diablillo"] = {
        ["WARLOCK"] = 688,
        ["Esbirro Diablillo"] = 11939,
        ["Nigromante de las Sombras Secacorteza"] = 11939,
        ["Fiel Filo Ardiente"] = 11939,
        ["Fizzle Necromenta"] = 11939,
        ["Fiel de la Facción Oscura"] = 11939,
        ["Clamainferno Lobrecuore"] = 11939,
        ["Balizar el Agraviado"] = 11939,
        ["Invocador Roca Negra"] = 11939,
        ["Invocador Filo Ardiente"] = 11939,
        ["Kayla Herrera"] = 11939,
        ["Gina Lang"] = 11939,
        ["Dane Suaven"] = 11939,
        ["Cilina Negrozón"] = 11939,
        ["Wren Fonteoscura"] = 11939,
        ["Morloch"] = 11939,
    },
    ["Escalofrío"] = {
        ["Ventisca"] = 28547,
    },
    ["Parche"] = {
        ["Tecnobot"] = 10860,
    },
    ["Inquina de Arantir"] = {
        ["Arantir"] = 9733,
    },
    ["Rotura craneal"] = {
        ["Déspota Aplastacresta"] = 9791,
        ["Aplastacráneos Ferrohondo"] = 16172,
        ["Guerrero Machacacráneos"] = 3148,
        ["Lanzahachas Machacacráneos"] = 3148,
        ["Aplastacráneos Comepiedras"] = 3148,
        ["Devlin Agamand"] = 3148,
        ["Partehuesos Gravacal"] = 3148,
        ["Herrero aprendiz de Trabalomas"] = 3148,
    },
    ["Drenar alma"] = {
        ["WARLOCK"] = 11675,
    },
    ["Incendiar área"] = {
        ["Destructor ardiente"] = 8000,
    },
    ["Pisotón de Azrethoc"] = {
        ["Imagen de Lord Azrethoc"] = 7961,
    },
    ["Casco de cohete goblin"] = {
        Professions = 12758,
    },
    ["Botas de tela lunar"] = {
        Professions = 19435,
    },
    ["Aura de podredumbre"] = {
        ["Tiritas"] = 3106,
    },
    ["Pantalones de lana gruesos"] = {
        Professions = 3850,
    },
    ["Elixir de crecimiento gigante"] = {
        Professions = 8240,
    },
    ["Ofrenda de Arugal"] = {
        ["Hijo de Arugal"] = 7124,
    },
    ["Cuero pesado"] = {
        Professions = 20649,
    },
    ["Malla Espina Salvaje"] = {
        Professions = 16650,
    },
    ["Elixir de defensa"] = {
        Professions = 3177,
    },
    ["Encantar pechera: salud superior"] = {
        Professions = 13640,
    },
    ["Curar enfermedad"] = {
        ["PRIEST"] = 528,
        ["SHAMAN"] = 2870,
    },
    ["Caparazón inferno"] = {
        ["Magistrado Ladoquemao"] = 7739,
        ["Tejellama Grutacanto"] = 7739,
    },
    ["Guanteletes de bronce bañados en plata"] = {
        Professions = 3333,
    },
    ["Maestría elemental"] = {
        ["SHAMAN"] = 16166,
    },
    ["La grande"] = {
        Professions = 12754,
    },
    ["Transmutar: hierro a oro"] = {
        Professions = 11479,
    },
    ["Catalejo ornamentado"] = {
        Professions = 6458,
    },
    ["Consumir carne"] = {
        ["Rechín"] = 3393,
        ["Acechacarne de las Tierras Altas"] = 3393,
    },
    ["Mordedura de frío"] = {
        ["MAGE"] = 12472,
    },
    ["Refuerzo para armadura ligero"] = {
        Professions = 2152,
    },
    ["Pantalones de mitril ornamentado"] = {
        Professions = 9945,
    },
    ["Sastrería"] = {
        Professions = 3909,
        Professions = 3910,
        Professions = 12180,
    },
    ["Rifle de torio"] = {
        Professions = 19792,
    },
    ["Suicidio"] = {
        ["Ovotrepador silítido"] = 7,
    },
    ["Peste devoradora"] = {
        ["PRIEST"] = 19280,
    },
    ["Estimular"] = {
        ["DRUID"] = 29166,
    },
    ["Caída"] = {
        ["Nigromante Hakkari"] = 6869,
    },
    ["Cacharros zumbones de bronce"] = {
        Professions = 3942,
    },
    ["Ingeniería"] = {
        Professions = 4037,
        Professions = 4038,
        Professions = 12656,
    },
    ["Invocar Corcel vil"] = {
        ["WARLOCK"] = 5784,
    },
    ["Atraepeces acuadinámico"] = {
        Professions = 9271,
    },
    ["Arnés de pellejo de raptor"] = {
        Professions = 4096,
    },
    ["Faja de anillas ígneas"] = {
        Professions = 20872,
    },
    ["Ola de sanación inferior"] = {
        ["SHAMAN"] = 10468,
    },
    ["Maldición de las lenguas"] = {
        ["WARLOCK"] = 11719,
    },
    ["Disuasión"] = {
        ["HUNTER"] = 19263,
    },
    ["Armadura de hielo"] = {
        ["MAGE"] = 10220,
    },
    ["Patada"] = {
        ["ROGUE"] = 1769,
        ["Monje Escarlata"] = 11978,
        ["Bandido Tótem Siniestro"] = 11978,
        ["Devastador no-muerto"] = 11978,
    },
    ["Agua"] = {
        ["Géiser masivo"] = 22422,
    },
    ["Astucia del raptor"] = {
        ["Gor'mul"] = 4153,
    },
    ["Llamar a un horror lupino"] = {
        ["Horror Lupino"] = 7489,
    },
    ["Ritual de fatalidad"] = {
        ["WARLOCK"] = 18540,
    },
    ["Guanteletes de cuero rúnico"] = {
        Professions = 19055,
    },
    ["Tótem Centella lunar"] = {
        ["Tótem Luzlunar"] = 15787,
        ["Totémico Nevada"] = 15787,
    },
    ["Pantalones de tejido fantasmal"] = {
        Professions = 18441,
    },
    ["Veneno de aturdimiento mental III"] = {
        ["ROGUE"] = 11400,
    },
    ["Sobrehombros de tela vil"] = {
        Professions = 18453,
    },
    ["Guarda de sanación excelente"] = {
        ["Guarda sanador superior"] = 15869,
        ["Médico Brujo Espina Ahumada"] = 15869,
        ["Portavoz Secacorteza"] = 15869,
    },
    ["Elixir de poder de las Sombras"] = {
        Professions = 11476,
    },
    ["Camisa de lino marrón"] = {
        Professions = 3915,
    },
    ["Daga de acero destellante"] = {
        Professions = 15972,
    },
    ["Carga de seforio pequeña"] = {
        Professions = 3933,
    },
    ["Fuego Sagrado"] = {
        ["PRIEST"] = 15261,
    },
    ["Invocar Corcel nefasto"] = {
        ["WARLOCK"] = 23161,
    },
    ["Guantes barbáricos"] = {
        Professions = 3771,
    },
    ["Reflector de llamas hiperradiante"] = {
        Professions = 23081,
    },
    ["Crear Jade de maná"] = {
        ["MAGE"] = 3552,
    },
    ["Sobrehombros de paño rúnico"] = {
        Professions = 18449,
    },
    ["Invocar fragmento de obsidiana"] = {
        ["Fragmento de obsidiana"] = 10061,
    },
    ["Vides despellejadoras"] = {
        ["Despellejador pétalo de sangre"] = 14112,
    },
    ["Caperuza de tela vil"] = {
        Professions = 18442,
    },
    ["Consumo de alma"] = {
        ["Teremus el Devorador"] = 12667,
    },
    ["Traca de cohetes rojos"] = {
        Professions = 26425,
    },
    ["Maldición del Apocalipsis"] = {
        ["WARLOCK"] = 603,
    },
    ["Frenesí profano"] = {
        ["Guarda esquelético"] = 8699,
    },
    ["Dolor abrasador"] = {
        ["WARLOCK"] = 17923,
    },
    ["Dominación mental"] = {
        ["Doctor Weavil"] = 25772,
    },
    ["Invocar ráfaga de lava A"] = {
        ["Rata del Núcleo"] = 21886,
    },
    ["Redención"] = {
        ["PALADIN"] = 20773,
    },
    ["Látigo de Ira"] = {
        ["Guerrera Látigo de Ira"] = 12545,
        ["Gritona Látigo de Ira"] = 12545,
        ["Maestra de batalla Látigo de Ira"] = 12545,
    },
    ["Toga de lino roja"] = {
        Professions = 2389,
    },
    ["Capa con broche de perlas"] = {
        Professions = 6521,
    },
    ["Pantalones Nochefugaz"] = {
        Professions = 10548,
    },
    ["Fuerza deteriorada"] = {
        ["Gnomo paria"] = 6951,
        ["Corruspuma corrupto"] = 6951,
    },
    ["Frasco de petrificación"] = {
        Professions = 17634,
    },
    ["Moco ralentizante"] = {
        ["Moco primario"] = 16050,
    },
    ["Encantar arma 2M: impacto excelente"] = {
        Professions = 20030,
    },
    ["Pellejo grueso curtido"] = {
        Professions = 10482,
    },
    ["Almófar de escamas doradas"] = {
        Professions = 3503,
    },
    ["Toque debilitador"] = {
        ["Espectro hambriento"] = 16333,
        ["Sombra inquieta"] = 16333,
        ["Ciudadano espectral"] = 16333,
        ["Guardián decrépito"] = 16333,
    },
    ["Talega de seda verde"] = {
        Professions = 6693,
    },
    ["Encantar capa: agilidad menor"] = {
        Professions = 13419,
    },
    ["Chirrido ensordecedor"] = {
        ["Gritón Espinadaga"] = 3589,
        ["Arpía estridente"] = 3589,
        ["Gritona Látigo de Ira"] = 3589,
        ["Estridador Cortezaférrea"] = 3589,
        ["Estridador de Cuna del Invierno"] = 3589,
        ["Chillador odial"] = 3589,
        ["Alma en pena plañidera"] = 3589,
        ["Harpía veloneve"] = 3589,
        ["Furia Shelda"] = 3589,
    },
    ["Leotardos de sable de hielo"] = {
        Professions = 19074,
    },
    ["Maldición de alivio"] = {
        ["Fanático de la Facción Oscura"] = 7098,
        ["Arpía Alabruja"] = 7098,
    },
    ["Aluvión de acero"] = {
        ["ROGUE"] = 13877,
    },
    ["Punta de escudo de hierro"] = {
        Professions = 7221,
    },
    ["Partículas cegadoras"] = {
        ["ROGUE"] = 6510,
    },
    ["Frasco de poder supremo"] = {
        Professions = 17637,
    },
    ["Nube de polvo"] = {
        ["Zancador Pedalar"] = 7272,
        ["Zancudo de llanura viejo"] = 7272,
        ["Esclavo Kolkar"] = 7272,
    },
    ["Transmutar: no-muerte a agua"] = {
        Professions = 17563,
    },
    ["Dolores convulsivos"] = {
        ["Torturador del Martillo Crepuscular"] = 13619,
        ["Aspecto de corrupción"] = 13619,
    },
    ["Guantes de escórpido resistentes"] = {
        Professions = 10542,
    },
    ["Lente verde"] = {
        Professions = 12622,
    },
    ["Golpe siniestro"] = {
        ["ROGUE"] = 11294,
    },
    ["Aro de tela lunar"] = {
        Professions = 18452,
    },
    ["Aura de Resistencia a la Escarcha"] = {
        ["PALADIN"] = 19898,
    },
    ["Intimidación"] = {
        ["HUNTER"] = 19577,
        ["Déspota Aplastacresta"] = 7093,
    },
    ["Fijar"] = {
        ["Atal'ai sometido"] = 12021,
        ["Guardarrama Fuego de Jade"] = 12021,
        ["Dragauro Garra de Furia"] = 12021,
        ["Aberración resucitada"] = 12021,
        ["Guardarrama Emeraldón"] = 12021,
        ["Guardarrama Verdantine"] = 12021,
    },
    ["Enzima pútrido"] = {
        ["Escarbaracha"] = 14539,
    },
    ["Fango negro"] = {
        ["Bestia barroso"] = 7279,
        ["Rondador Tar"] = 7279,
        ["El Rik"] = 7279,
        ["Barroso putrefacto"] = 7279,
    },
    ["Calmar animal"] = {
        ["DRUID"] = 9901,
    },
    ["Sobrehombros de seda verdes"] = {
        Professions = 8774,
    },
    ["Efecto de Guardia de Laze"] = {
        ["Guardia de Laze"] = 3826,
    },
    ["Toga de gran vudú"] = {
        Professions = 10520,
    },
    ["Desterrar"] = {
        ["WARLOCK"] = 18647,
    },
    ["Toxina letal"] = {
        ["Cuorevaja"] = 8256,
    },
    ["Armadura de mago"] = {
        ["MAGE"] = 22783,
    },
    ["Encantar brazales: espíritu inferior"] = {
        Professions = 7859,
    },
    ["Botas de lino de suela suave"] = {
        Professions = 3845,
    },
    ["Lágrimas del Hijo del Viento"] = {
        ["Príncipe Truenoraan"] = 23011,
    },
    ["Gafas gnómicas"] = {
        Professions = 12897,
    },
    ["Mordedura de mangosta"] = {
        ["HUNTER"] = 14271,
    },
    ["Elixir de la mangosta"] = {
        Professions = 17571,
    },
    ["Crear falla"] = {
        ["Tabetha"] = 9079,
    },
    ["Antorcha arrojadiza"] = {
        ["Videntormento Kolkar"] = 14292,
        ["Invasor Kolkar"] = 14292,
    },
    ["Resguardo contra las Sombras"] = {
        ["WARLOCK"] = 28610,
    },
    ["Coraza de veraplata"] = {
        Professions = 9974,
    },
    ["Cinturón de cuero rúnico"] = {
        Professions = 19072,
    },
    ["Choque de Escarcha"] = {
        ["SHAMAN"] = 10473,
        ["Encantadora Látigo de Ira"] = 12548,
        ["Reptador Golpeseco"] = 12548,
        ["Grik'nir el Frío"] = 21030,
        ["Lokholar el Señor del Hielo"] = 19133,
        ["Chamán Lobo Gélido"] = 21401,
        ["Hechicera Strashaz"] = 15499,
    },
    ["Cinturón de montañero"] = {
        Professions = 3767,
    },
    ["Visión lejana"] = {
        ["SHAMAN"] = 6196,
    },
    ["Resguardo contra el Fuego"] = {
        ["MAGE"] = 10225,
    },
    ["Guantes de tejido onírico"] = {
        Professions = 12067,
    },
    ["Pólvora potente"] = {
        Professions = 3945,
    },
    ["Guantes de tejido fantasmal"] = {
        Professions = 18413,
    },
    ["Sanación superior"] = {
        ["PRIEST"] = 25314,
    },
    ["Aceite de maná menor"] = {
        Professions = 25125,
    },
    ["Piedra de afilar férrea"] = {
        Professions = 2660,
    },
    ["Derribo"] = {
        ["Guardia maldito inferior"] = 18812,
        ["Mirmidón Strashaz"] = 18812,
        ["Drek'Thar"] = 19128,
        ["Zzarc' Vul"] = 5164,
        ["Partecaras de Crestagrana"] = 5164,
        ["Sin'Dall"] = 5164,
        ["Cascahuesos Comepiedras"] = 5164,
        ["Ogro Mo'grosh"] = 5164,
        ["Myrmidón Escama Tormentosa"] = 5164,
        ["Espíritu de Piedra furioso"] = 5164,
        ["Grisezno Zarpira"] = 5164,
        ["Tosco Puño de Roca"] = 11428,
        ["Guardia maldito"] = 11428,
        ["Flagglemurk el Cruel"] = 11428,
        ["Guerrero Gurubashi"] = 11428,
        ["Duriel Fuegolunar"] = 11428,
    },
    ["Puñal de bronce mortal"] = {
        Professions = 3295,
    },
    ["Camisa blanca formal"] = {
        Professions = 3871,
    },
    ["Falsa arremetida"] = {
        ["WARRIOR"] = 20560,
    },
    ["Bombita"] = {
        Professions = 15628,
    },
    ["Guardianes de Ilkrud"] = {
        ["Abisario"] = 6487,
        ["Ilkrud Magthrull"] = 6487,
    },
    ["Leotardos de bronce bañados en plata"] = {
        Professions = 12259,
    },
    ["Hoja dorada abrasadora"] = {
        Professions = 15973,
    },
    ["Abisario"] = {
        ["Esbirro abisario"] = 5108,
        ["Adjurador Defias"] = 5108,
        ["Brujo Sombraída"] = 5108,
    },
    ["Tromba giratoria"] = {
        ["Cuerolanza de Rajacieno"] = 8259,
    },
    ["Encantar brazales: intelecto inferior"] = {
        Professions = 13622,
    },
    ["Maldición de Stalvan"] = {
        ["Stalvan Mantoniebla"] = 3105,
        ["Espíritu abandonado"] = 3105,
    },
    ["Capa de seda carmesí"] = {
        Professions = 8789,
    },
    ["Mordedura sañosa"] = {
        ["Can del Núcleo anciano"] = 19319,
        ["Faucenestra"] = 19319,
    },
    ["Bendición de sacrificio"] = {
        ["PALADIN"] = 20729,
    },
    ["Brazales oscurecidos"] = {
        Professions = 9201,
    },
    ["Temeridad"] = {
        ["WARRIOR"] = 1719,
    },
    ["Ira sangrienta"] = {
        ["WARRIOR"] = 2687,
    },
    ["Perturbar huevo del Grajero"] = {
        ["Criador de la Torre"] = 15746,
    },
    ["Varita mística superior"] = {
        Professions = 14810,
    },
    ["Guantes de fe verdadera"] = {
        Professions = 8782,
    },
    ["Purgar"] = {
        ["SHAMAN"] = 8012,
    },
    ["Barrido de cola"] = {
        ["Ysondre"] = 15847,
        ["Lethon"] = 15847,
        ["Emeriss"] = 15847,
        ["Taerar"] = 15847,
    },
    ["Encantar botas: agilidad inferior"] = {
        Professions = 13637,
    },
    ["Encantar pechera: amortiguación menor"] = {
        Professions = 7426,
    },
    ["Terror dracónico"] = {
        ["Legionario Alanegra"] = 23967,
        ["Dragauro Garramortal"] = 23967,
        ["Custodio Alanegra"] = 23967,
    },
    ["Coraza radiante"] = {
        Professions = 16648,
    },
    ["Maldición del maestro oscuro"] = {
        ["Maestro oscuro Gandling"] = 18702,
    },
    ["Lanzamiento de poción"] = {
        ["Cebador Dalaran"] = 7638,
    },
    ["Símbolo de divinidad"] = {
        ["PALADIN"] = 17033,
    },
    ["Conversor de arcanita delicado"] = {
        Professions = 19815,
    },
    ["Brazales rúnicos de cobre"] = {
        Professions = 2664,
    },
    ["Cadena de relámpagos"] = {
        ["SHAMAN"] = 10605,
        ["Oráculo Hakkari"] = 16006,
        ["Primalista Thurloga"] = 16006,
        ["Thrall"] = 16033,
        ["Consorte tronador"] = 12058,
    },
    ["Regeneración salvaje"] = {
        ["Presertor Machacahielo"] = 9616,
        ["Arrastrado descarriado"] = 7948,
    },
    ["Destructora de hierro dorado"] = {
        Professions = 3495,
    },
    ["Trabuco mortal"] = {
        Professions = 3936,
    },
    ["Botas de can del Núcleo"] = {
        Professions = 20853,
    },
    ["Favor divino"] = {
        ["PALADIN"] = 20216,
    },
    ["Sapo mecánico casi vivo"] = {
        Professions = 19793,
    },
    ["Descuartizar"] = {
        ["Bhag'thera"] = 3147,
        ["Segador de la Tierras Altas"] = 3147,
        ["Segador jaspeado"] = 3147,
        ["Bjarn"] = 3147,
        ["Matriarca Tajobuche"] = 3147,
        ["Picovaja instruído"] = 3147,
        ["Grifo Picovaja"] = 3147,
        ["Señor del Cielo Picovaja"] = 3147,
        ["Saqueador Viento Seco"] = 3147,
        ["Segador Faucerroja"] = 3147,
        ["Zarpador Valvafango"] = 3147,
        ["Desgarrador aterrador"] = 3147,
        ["Estigmodio feroz"] = 3147,
    },
    ["Teletransporte a: Claro de la Luna"] = {
        ["DRUID"] = 18960,
    },
    ["Grito de orden"] = {
        ["Capitán Garramortal"] = 22440,
    },
    ["Carga impetuosa"] = {
        ["Herod"] = 8260,
        ["Agathelos el Furioso"] = 8260,
        ["Gruñón"] = 8260,
        ["Jabalí colmillopétreo"] = 6268,
        ["Morrolargo"] = 6268,
        ["Dentosangre"] = 6268,
        ["Princesa"] = 6268,
        ["Panzallena"] = 6268,
        ["Séquito porcino"] = 6268,
        ["Hogger"] = 6268,
        ["Dentosangre joven"] = 6268,
        ["Jabalí Ocultorroca"] = 6268,
        ["Gran dentosangre"] = 6268,
        ["Zancorillas múrloc"] = 6268,
        ["Jabalí del risco"] = 6268,
        ["Grande jabalí del risco"] = 6268,
        ["Jabalí del risco mayor"] = 6268,
        ["Jabalí montés"] = 6268,
        ["Jabalí montés sarnoso"] = 6268,
        ["Jabalí montés mayor"] = 6268,
        ["Jabalí del risco cicatríceo"] = 6268,
        ["Caballista de Stromgarde"] = 6268,
        ["Ternero Kodo"] = 6268,
        ["Mazzranache"] = 6268,
        ["Colazote Garrasangre"] = 6268,
        ["Garrasangre Rojomorro"] = 6268,
        ["Navegante Kul Tiras"] = 6268,
        ["Rojomorro Garrasangre corrupto"] = 6268,
        ["Kodo mayor de Los Baldíos"] = 6268,
        ["Kodo lanudo"] = 6268,
        ["Maleante Kolkar"] = 6268,
        ["Saltacharco Gapo Salino"] = 6268,
        ["Ciervo salvaje"] = 6268,
        ["Tormentabisal enfurecido"] = 6268,
        ["Antlered Maledictor"] = 6268,
        ["Merodeador Galak"] = 6268,
        ["Agam'ar"] = 6268,
        ["Brontus"] = 6268,
        ["Jabalí Cenicrín"] = 6268,
        ["Patriarca tronador"] = 6268,
        ["Guerrero marchito"] = 6268,
        ["Invasor Kolkar"] = 6268,
        ["Tosco Tótem Siniestro"] = 6268,
    },
    ["Resistencia a la Naturaleza"] = {
        ["HUNTER"] = 24513,
    },
    ["Cinturón de paño rúnico"] = {
        Professions = 18402,
    },
    ["Haz ocular"] = {
        ["Ojo de C'Thun"] = 26134,
    },
    ["Yelmo cabeza de lobo"] = {
        Professions = 10621,
    },
    ["Disparo Arcano"] = {
        ["HUNTER"] = 14287,
    },
    ["Puntos de honor +82"] = {
        ["Belisario de la Horda"] = 24961,
        ["Brigadier general de la Alianza"] = 24961,
    },
    ["Lamento de latipesadilla"] = {
        ["Latipesadilla"] = 3485,
    },
    ["Aullido furioso"] = {
        Pet = 24597,
        ["Coyote líder"] = 3149,
        ["Masadura Nicroyermo"] = 3149,
    },
    ["Aceite de las Sombras"] = {
        Professions = 3449,
    },
    ["Alaridos del pasado"] = {
        ["Custodio de los Lamentos"] = 7074,
        ["Fantasma de cólera"] = 7074,
    },
    ["Herida infectada"] = {
        ["Colmillovil Nocturno"] = 3427,
        ["Gath'Ilzogg"] = 3427,
        ["Bastardo de Crestagrana"] = 3427,
        ["Tajobuche de las Tierras Altas"] = 3427,
        ["Tajobuche jaspeado"] = 3427,
        ["Sarldente"] = 3427,
        ["Cautivo Defias"] = 3427,
        ["Oso negro malsano"] = 3427,
        ["Corremareas escamado"] = 3427,
        ["Habitabosta Rompeanca"] = 3427,
        ["Grande crocolisco del lago"] = 3427,
        ["Bruto Sentencia"] = 3427,
        ["Águila ratonera"] = 3427,
        ["Águila ratonera gigante"] = 3427,
        ["Trepador descarriado"] = 3427,
        ["Alimaña Gravamorro"] = 3427,
        ["Horror carroñero"] = 3427,
        ["Lobo Zarpayel"] = 3427,
        ["Carroñero Zarpayel"] = 3427,
        ["Devastador Zarpayel"] = 3427,
        ["Alimana Mostacho Blanco"] = 17230,
    },
    ["Sobrehombros de cuero oscuro"] = {
        Professions = 3769,
    },
    ["Vestido sencillo"] = {
        Professions = 8465,
    },
    ["Brazales barbáricos"] = {
        Professions = 23399,
    },
    ["Deseo de muerte"] = {
        ["WARRIOR"] = 12328,
    },
    ["Invocar elemental de Tierra"] = {
        ["Elemental de tierra"] = 19704,
        ["Clamapiedras Crepuscular"] = 19704,
    },
    ["Gorro de control mental gnómico"] = {
        Professions = 12907,
    },
    ["Flagelo bajo"] = {
        ["Zzarc' Vul"] = 8716,
        ["Hermano Roblecuervo"] = 8716,
        ["Grisezno Zarpira"] = 8716,
    },
    ["Encantar brazales: fuerza superior"] = {
        Professions = 13939,
    },
    ["Locamente enamorado"] = {
        ["Kwee Q. Mercachifle"] = 27572,
    },
    ["Sobrehombros de mitril ornamentados"] = {
        Professions = 9952,
    },
    ["Punta de maná"] = {
        ["Charlga Filonavaja"] = 8358,
    },
    ["Refuerzo para armadura pesado"] = {
        Professions = 3780,
    },
    ["Invocar príncipe de lava"] = {
        ["Príncipe de lava"] = 19392,
        ["Señor del fuego"] = 19392,
    },
    ["Ruptura terrenal"] = {
        ["Tentáculo Garral"] = 26139,
        ["Tentáculo ocular"] = 26139,
    },
    ["Visual de sombra"] = {
        ["Sombra de Jin'do"] = 24313,
    },
    ["Crear agua"] = {
        ["MAGE"] = 10140,
    },
    ["Cuero encantado"] = {
        Professions = 17181,
    },
    ["Erupción monumental"] = {
        ["Jurafuego"] = 20483,
    },
    ["Pantalones de guardián"] = {
        Professions = 7147,
    },
    ["Invocar súcubo"] = {
        ["WARLOCK"] = 712,
    },
    ["Guantes de paño rúnico"] = {
        Professions = 18417,
    },
    ["Transmutar: arcanita"] = {
        Professions = 17187,
    },
    ["Disparar"] = {
        ["Fusilero de mediocampo"] = 16572,
        ["Fusilero Trapis"] = 16767,
        ["Fusilero de mediocampo"] = 16772,
        ["Fusilero Trapis"] = 16775,
        ["Fusilero de mediocampo"] = 16768,
        ["Fusilero de mediocampo"] = 16780,
        ["Fusilero de mediocampo"] = 16776,
        ["Fusilero de mediocampo"] = 16777,
        ["Fusilero Trapis"] = 16778,
        ["Fusilero de mediocampo"] = 16779,
        ["Roggo Jartumul"] = 9008,
        ["Motley Garmason"] = 9008,
        ["Helena Emadera"] = 9008,
        ["Fusilero Hierro Negro"] = 8996,
        ["Fusilero Hierro Negro"] = 8997,
        ["Fusilero Hierro Negro"] = 8995,
    },
    ["Desgarre de tendón"] = {
        ["Nefaru"] = 3604,
        ["Tethis"] = 3604,
        ["Ancestro Tajobuche"] = 3604,
        ["Sacudidor Denteserra"] = 3604,
        ["Crocolisco fluvial"] = 3604,
        ["Crocolisco Aguasalada"] = 3604,
        ["Crocolisco Quijaforte"] = 3604,
        ["La Cáscara"] = 3604,
        ["Crocolisco gigante de Los Humedales"] = 3604,
        ["Crocolisco de agua salada viejo"] = 3604,
        ["Desgarracortezas Sombramatorral"] = 3604,
        ["Desgarrador Luporror"] = 3604,
        ["Pinzador Valvafango"] = 3604,
        ["Trillasín de la fosa profunda"] = 3604,
        ["Crocolisco descarriado"] = 3604,
        ["Bufo el Molesto"] = 3604,
        ["Quebrador Makrinni"] = 3604,
        ["Cazador Ravasaur"] = 3604,
        ["Oso dentoesquirla"] = 3604,
        ["Pícaro Cardo Nevado"] = 3604,
        ["Yeti Cardo Nevado"] = 3604,
        ["Garrafilada"] = 3604,
        ["Viejo Saltariscos"] = 3604,
        ["Gruñón"] = 3604,
        ["Manacán"] = 3604,
        ["Campesino Forjatiniebla"] = 3604,
        ["Carroñero Zarpayel"] = 3604,
        ["Diemetradón joven"] = 3604,
        ["Amigo de Lar'korwi"] = 3604,
        ["Lar'korwi"] = 3604,
        ["Lupo Exclavaliza"] = 3604,
        ["Faucemuerte"] = 3604,
        ["Bayne"] = 3604,
        ["Aullanieblas"] = 3604,
        ["Shi-Rotam"] = 3604,
        ["Sian-Rotam"] = 3604,
        ["Tnazario Latigosólido"] = 3604,
        ["Mastín Gordok"] = 3604,
        ["Acechapolvo"] = 3604,
        ["Crocolisco Zulian"] = 3604,
    },
    ["Encantar pechera: estadísticas"] = {
        Professions = 13941,
    },
    ["Guantes de tejido mágico negros"] = {
        Professions = 12053,
    },
    ["Elixir de intelecto superior"] = {
        Professions = 11465,
    },
    ["Tornapunta de hierro"] = {
        Professions = 3958,
    },
    ["Puñetazo de Rhahk'Zor"] = {
        ["Rhahk'Zor"] = 6304,
    },
    ["Bocarda fabricada con amor"] = {
        Professions = 3939,
    },
    ["Alquimia"] = {
        Professions = 3101,
        Professions = 3464,
        Professions = 11611,
    },
    ["Aspecto de la manada"] = {
        ["HUNTER"] = 13159,
    },
    ["Poción de atriplex salvaje"] = {
        Professions = 11458,
    },
    ["Ruptura"] = {
        ["ROGUE"] = 11275,
    },
    ["Refuerzo para armadura medio"] = {
        Professions = 2165,
    },
    ["Parasitar sangre"] = {
        ["Glotón Furia Lunar"] = 6958,
        ["Arpía Sangrepluma"] = 6958,
        ["Hezrul Marcasangre"] = 6958,
    },
    ["Jubón de anillas de cobre"] = {
        Professions = 3321,
    },
    ["Guantes barbáricos de hierro"] = {
        Professions = 9820,
    },
    ["Tótem de magma"] = {
        ["SHAMAN"] = 10587,
    },
    ["Recuerdos enfurecedores"] = {
        ["Mor'Ladim"] = 3547,
    },
    ["Susto espantoso"] = {
        ["Nelson el Amable"] = 23275,
    },
    ["Carrerilla"] = {
        ["DRUID"] = 9821,
        Pet = 23110,
    },
    ["La escapada de la avanzada de Vallefresno"] = {
        ["Avanzado de Vallefresno"] = 20540,
    },
    ["Quemadura de maná"] = {
        ["PRIEST"] = 10876,
        ["Acólito Caramuerte"] = 15785,
        ["Acechamaná Hederine"] = 15980,
        ["Visión encantada"] = 11981,
        ["Bruja de mar Colafuria"] = 2691,
        ["Destripador vil"] = 2691,
        ["Vigilante amputado"] = 2691,
        ["Manáfago invocado"] = 2691,
        ["Dragón vidente Wily"] = 17630,
    },
    ["Pantalones de cuero oscuro"] = {
        Professions = 7135,
    },
    ["Sobrehombros de montañero"] = {
        Professions = 3768,
    },
    ["Fuego lunar"] = {
        ["DRUID"] = 9835,
        ["Tyranis Malem"] = 15798,
        ["Oráculo lechúcico lunar"] = 15798,
        ["Archidruida Renferal"] = 22206,
    },
    ["Fango oscuro"] = {
        ["Babosa negra"] = 3335,
        ["Moco corrupto"] = 3335,
        ["Barrosín"] = 3335,
    },
    ["Encantar brazales: espíritu superior"] = {
        Professions = 13846,
    },
    ["Invocar gólem teledirigido"] = {
        ["Gólem a control remoto"] = 3605,
        ["Ingeniero goblin"] = 3605,
    },
    ["Aceite de zahorí inferior"] = {
        Professions = 25126,
    },
    ["Carga de jabalí"] = {
        ["Jabaguerrero Lomopelo"] = 3385,
        ["Jabalí jaspeado feroz"] = 3385,
        ["Jabalí jaspeado mayor"] = 3385,
        ["Jabalí jaspeado corrupto"] = 3385,
    },
    ["Multidisparo"] = {
        ["HUNTER"] = 25294,
        ["Mecánico de Ventura y Cía."] = 14443,
        ["Defensor leproso"] = 14443,
        ["Lady Sylvanas Brisaveloz"] = 20735,
    },
    ["Pulverizador de telaraña"] = {
        ["Tuten'kash"] = 12252,
    },
    ["Liberar sapos"] = {
        ["Sapo de la selva"] = 24058,
        ["Médico brujo Hakkari"] = 24058,
    },
    ["Encantar guantes: celeridad menor"] = {
        Professions = 13948,
    },
    ["Crear quintaesencia acuosa"] = {
        ["Duque Hydraxis"] = 21357,
    },
    ["Mando de tonque de vapor"] = {
        Professions = 28327,
    },
    ["Golpe de tormenta"] = {
        ["SHAMAN"] = 17364,
    },
    ["Suprimir veneno"] = {
        ["DRUID"] = 2893,
    },
    ["Transmutar: fuego elemental"] = {
        Professions = 25146,
    },
    ["Caminar sobre el agua"] = {
        ["SHAMAN"] = 546,
    },
    ["Maza de bronce pesada"] = {
        Professions = 3296,
    },
    ["Camisote de hierro verde"] = {
        Professions = 3508,
    },
    ["Drenar esclavo"] = {
        ["Mogh el Eterno"] = 8809,
    },
    ["Bomba de cobre férrea"] = {
        Professions = 3923,
    },
    ["Yelmo de mitril pesado"] = {
        Professions = 9970,
    },
    ["Coraza de dragontina negra"] = {
        Professions = 19085,
    },
    ["Encantar capa: defensa excelente"] = {
        Professions = 20015,
    },
    ["Reabastecimiento"] = {
        ["Suministrador Hierro Negro"] = 4961,
    },
    ["Poción de Protección contra la Naturaleza superior"] = {
        Professions = 17576,
    },
    ["Carcaj pesado"] = {
        Professions = 9193,
    },
    ["Casco de minero goblin"] = {
        Professions = 12717,
    },
    ["Summon Dreadsteed Spirit (DND)"] = {
        ["Espíritu Corcelorror"] = 23159,
        ["Corcel de la Muerte Xorothiano"] = 23159,
    },
    ["Golpe de tirador"] = {
        ["Tirador Hierro Negro"] = 12198,
    },
    ["Eviscerar"] = {
        ["ROGUE"] = 31016,
    },
    ["Distraer"] = {
        ["ROGUE"] = 1725,
    },
    ["Invocación PNJ muñeco"] = {
        ["Zahorí bribón Defias"] = 3361,
    },
    ["Descarga Arcana"] = {
        ["Sombra de Felicent"] = 13901,
    },
    ["Disipar superior"] = {
        ["Sumo Sacerdote Thel'danis"] = 24997,
    },
    ["Guardianes de Hukku"] = {
        ["Diablillo de Hukku"] = 12790,
        ["Hukku"] = 12790,
    },
    ["Suprimir enfermedad"] = {
        ["PRIEST"] = 552,
    },
    ["Aspecto de la bestia"] = {
        ["HUNTER"] = 13161,
    },
    ["Aura de Resistencia al Fuego"] = {
        ["PALADIN"] = 19900,
    },
    ["Presencia de muerte"] = {
        ["Morbent Vil"] = 3109,
    },
    ["Gafas de las Sombras"] = {
        Professions = 3940,
    },
    ["Poción de letargo sin sueños"] = {
        Professions = 15833,
    },
    ["Maldición de venganza"] = {
        ["Arikara"] = 17213,
    },
    ["Hongos Ancalodo"] = {
        ["Habitabosta Ancalodo"] = 9462,
        ["Correcostas Mirefín"] = 9462,
    },
    ["Leotardos de tejido zahorí"] = {
        Professions = 18421,
    },
    ["Faja del Alba"] = {
        Professions = 23632,
    },
    ["Rastrear dragonantes"] = {
        ["HUNTER"] = 19879,
    },
    ["Furia de batalla"] = {
        ["Tharil'zun"] = 3631,
        ["Capataz Puñastilla"] = 3631,
        ["Jefe de sector de Ventura y Cía."] = 3631,
        ["Señor Supremo Cortaviento"] = 3631,
        ["Malgin Cebadiz"] = 3631,
        ["Ejecutor del Martillo Crepuscular"] = 3631,
    },
    ["Núcleo de poder de oro"] = {
        Professions = 12584,
    },
    ["Infección de sapobilis"] = {
        ["Sapo bílico"] = 10251,
    },
    ["Encantar guantes: agilidad"] = {
        Professions = 13815,
    },
    ["Cerradura de prácticas"] = {
        Professions = 8334,
    },
    ["Hacha de batalla de torio enorme"] = {
        Professions = 16971,
    },
    ["Filo del Alba"] = {
        Professions = 16970,
    },
    ["Veneno mortal IV"] = {
        ["ROGUE"] = 11358,
    },
    ["Daga de cobre"] = {
        Professions = 8880,
    },
    ["Portal del Escudo del Estigma"] = {
        ["Portal de Escudo del Estigma"] = 15125,
        ["Brujo Escudo del Estigma"] = 15125,
    },
    ["Agazapar"] = {
        ["DRUID"] = 9892,
        Pet = 16697,
    },
    ["Cinturón rúnico de cobre"] = {
        Professions = 2666,
    },
    ["Tumba de hielo"] = {
        ["Lokholar el Señor del Hielo"] = 16869,
    },
    ["Encantar botas: aguante"] = {
        Professions = 13836,
    },
    ["Guanteletes de mitril pesado"] = {
        Professions = 9928,
    },
    ["Encantar brazales: fuerza"] = {
        Professions = 13661,
    },
    ["Girocronátomo"] = {
        Professions = 3961,
    },
    ["Paliza general"] = {
        ["Ograbisi"] = 10887,
        ["Setis"] = 10887,
    },
    ["Botas del encantador"] = {
        Professions = 3860,
    },
    ["Bolsa de tela vil del Núcleo"] = {
        Professions = 26087,
    },
    ["Talega de seda pequeña"] = {
        Professions = 3813,
    },
    ["Polimorfia: vaca"] = {
        ["MAGE"] = 28270,
    },
    ["Respiración acuática"] = {
        ["SHAMAN"] = 131,
    },
    ["Jubón resplandor lunar"] = {
        Professions = 8322,
    },
    ["Bendición del Jefe de Guerra"] = {
        ["Thrall"] = 16609,
        ["Heraldo de Thrall"] = 16609,
    },
    ["Sello de Justicia"] = {
        ["PALADIN"] = 20164,
    },
    ["Disparo tranquilizante"] = {
        ["HUNTER"] = 19801,
    },
    ["Fuego feérico (feral)"] = {
        ["DRUID"] = 17392,
    },
    ["Mortero de bronce portátil"] = {
        Professions = 3960,
    },
    ["Fundir mineral"] = {
        ["Artesano goblin"] = 5159,
    },
    ["Invocar cachorro acechalunas"] = {
        ["Jabato Acechalunas"] = 8594,
        ["Matriarca Acechalunas"] = 8594,
    },
    ["Invocar cosechaenjambres"] = {
        ["Cosechaenjambres"] = 7278,
        ["Cosechador silítido"] = 7278,
    },
    ["Corrupción de la Tierra"] = {
        ["Emeriss"] = 24910,
    },
    ["Aro radiante"] = {
        Professions = 16659,
    },
    ["Pantalones de seda azur"] = {
        Professions = 8758,
    },
    ["Resistencia a la Escarcha"] = {
        ["HUNTER"] = 24478,
    },
    ["Toga de lino blanca"] = {
        Professions = 7624,
    },
    ["Tradición de bestias"] = {
        ["HUNTER"] = 1462,
    },
    ["Poción de sangre de mago"] = {
        Professions = 24365,
    },
    ["Choque"] = {
        ["Exiliado tronador"] = 11824,
        ["Destructor Kolkar"] = 11824,
        ["Oráculo Arkkoran"] = 11824,
        ["Caminatormentas espirálico"] = 11824,
        ["Thrall"] = 16034,
        ["Patriarca tronador"] = 12553,
        ["Consorte tronador"] = 12553,
        ["Oráculo Gapo Salino"] = 2608,
        ["Emboscador Furia Sangrienta"] = 2608,
        ["Elementalista Crepuscular"] = 2609,
        ["Nezzliok el Feroz"] = 2610,
        ["Oráculo Anca Vil"] = 2606,
        ["Zarpaverde"] = 2606,
        ["Chamán Bosque Negro"] = 2606,
        ["Oráculo menor Anca Vil"] = 2607,
        ["Chamán Oscuro Crepuscular"] = 15500,
    },
    ["Empujar"] = {
        ["Hijo de Arkkoroc"] = 10101,
        ["Ursangous"] = 10101,
        ["Ciclonio"] = 18670,
        ["Gigante fundido"] = 18945,
        ["Teremus el Devorador"] = 11130,
    },
    ["Hachas y mazas de dos manos"] = {
        ["SHAMAN"] = 16269,
    },
    ["Mortero"] = {
        ["Mortero"] = 25003,
        ["Tonque de vapor"] = 25003,
    },
    ["Veneno parasitario"] = {
        ["Habitabosta Cegaluz"] = 8382,
        ["Acechador sanguijuela"] = 3358,
        ["Acechador de cuevas"] = 3358,
    },
    ["Mejunje inestable"] = {
        ["Jinete de muerciélagos Gurubashi"] = 24024,
    },
    ["Escarabajos de la cripta"] = {
        ["Escarabajo de la cripta"] = 16418,
        ["Reptador de la cripta"] = 16418,
    },
    ["Veneno instantáneo IV"] = {
        ["ROGUE"] = 11341,
    },
    ["Contrapeso férreo"] = {
        Professions = 3115,
    },
    ["Guerrera Nochefugaz"] = {
        Professions = 10499,
    },
    ["Encantar arma: matanza de demonios"] = {
        Professions = 13915,
    },
    ["Aterrorizar"] = {
        ["Horror esquelético"] = 7399,
        ["Machacador de Sneed"] = 7399,
        ["Terror escórpido"] = 7399,
        ["Colminferno descarriado"] = 7399,
        ["Azshir el Insomne"] = 7399,
        ["Pterrordáctilo demente"] = 7399,
        ["Rak'shiri"] = 7399,
    },
    ["Vaporizador"] = {
        ["Vapoherrero Hierro Negro"] = 11983,
        ["Elemental de ebullición"] = 11983,
    },
    ["Cosechar vida"] = {
        ["Ojo de gusano"] = 3243,
    },
    ["Coraza de dragontina"] = {
        Professions = 10650,
    },
    ["Botas de escamas doradas"] = {
        Professions = 3515,
    },
    ["Capa de guardián"] = {
        Professions = 7153,
    },
    ["Festividad: San Valentín, obsequio entregado"] = {
        ["Kwee Q. Mercachifle"] = 27663,
    },
    ["Encantar botas: agilidad"] = {
        Professions = 13935,
    },
    ["Elixir de entereza"] = {
        Professions = 3450,
    },
    ["Portal: Cima del Trueno"] = {
        ["MAGE"] = 11420,
    },
    ["Toga de paño brillante"] = {
        Professions = 18414,
    },
    ["Erupción degenerativa"] = {
        ["Lechubestia andrajosa"] = 15848,
    },
    ["Poción de Protección contra el Fuego"] = {
        Professions = 7257,
    },
    ["Poción de invulnerabilidad limitada"] = {
        Professions = 3175,
    },
    ["Encantar capa: defensa"] = {
        Professions = 13635,
    },
    ["Detectar magia"] = {
        ["MAGE"] = 2855,
    },
    ["Lanzar llave"] = {
        ["Técnico leproso"] = 13398,
        ["Maquinista leproso"] = 13398,
    },
    ["Crear comida"] = {
        ["MAGE"] = 28612,
    },
    ["Camisa de seda oscura"] = {
        Professions = 3870,
    },
    ["Sobrehombros nemorosos"] = {
        Professions = 28482,
    },
    ["Golpe heroico"] = {
        ["WARRIOR"] = 25286,
    },
    ["Sobrehombros de escórpido resistentes"] = {
        Professions = 10564,
    },
    ["Exponer debilidad"] = {
        ["Rastreasangre"] = 7140,
    },
    ["Aprisionamiento de cristal"] = {
        ["Comandante guardia apocalíptico"] = 23020,
    },
    ["Fundir acero"] = {
        Professions = 3569,
    },
    ["Poción de letargo sin sueños superior"] = {
        Professions = 24366,
    },
    ["Ácido tunelador"] = {
        ["Tunelador Gorishi"] = 14120,
        ["Guardián de la colmena Gorishi"] = 14120,
        ["Cavapiedras"] = 14120,
    },
    ["Esclavizar"] = {
        ["Jailor Eston"] = 3442,
    },
    ["Mira de francotirador"] = {
        Professions = 12620,
    },
    ["Coraza de escórpido resistente"] = {
        Professions = 10525,
    },
    ["Encierro"] = {
        ["Gólem de guerra"] = 9576,
    },
    ["Encantar botas: aguante inferior"] = {
        Professions = 13644,
    },
    ["Somnolencia del druida"] = {
        ["Boahn"] = 8040,
        ["Druida del Colmillo"] = 8040,
    },
    ["Descarga tormentosa"] = {
        ["Rey Magni Barbabronce"] = 20685,
        ["Vanndar Pico Tormenta"] = 19136,
    },
    ["Enloquecido"] = {
        ["Grel salvaje"] = 5915,
        ["Clamafuegos Radison"] = 5915,
    },
    ["Invocar ráfaga de lava C"] = {
        ["Rata del Núcleo"] = 21901,
    },
    ["Invisibilidad inferior"] = {
        Pet = 7870,
    },
    ["Robar"] = {
        ["ROGUE"] = 921,
    },
    ["Artista del escape"] = {
        Racial = 20589,
    },
    ["Fuegos artificiales verdes"] = {
        Professions = 23068,
    },
    ["Poción de sangre de trol débil"] = {
        Professions = 3170,
    },
    ["Noctambulear"] = {
        ["Bramasueños"] = 20668,
    },
    ["Rastrear elementales"] = {
        ["HUNTER"] = 19880,
    },
    ["Aparición de dracónido de bronce"] = {
        ["Dracónido de bronce"] = 22657,
        ["Bronze Drakonid Spawner"] = 22657,
    },
    ["Jubón de paño cenizo"] = {
        Professions = 18408,
    },
    ["Sello de Luz"] = {
        ["PALADIN"] = 20349,
    },
    ["Invocar aullavientos"] = {
        ["Aullavientos"] = 8271,
        ["Tejepolvo Rajacieno"] = 8271,
    },
    ["Guanteletes de placas ígneas"] = {
        Professions = 16655,
    },
    ["Invocar teúrgo"] = {
        ["Teurgista Dalaran"] = 3658,
        ["Invocador Dalaran"] = 3658,
    },
    ["Sobrehombros Argenta"] = {
        Professions = 23665,
    },
    ["Ojo de águila"] = {
        ["HUNTER"] = 6197,
    },
    ["Fragmento de alma"] = {
        ["WARLOCK"] = 6265,
    },
    ["Chaleco de cuero blanco"] = {
        Professions = 2163,
    },
    ["Encantar botas: agilidad superior"] = {
        Professions = 20023,
    },
    ["Dolor de distracción"] = {
        ["Machacador de Sneed"] = 3603,
    },
    ["Enjambre de insectos"] = {
        ["DRUID"] = 24977,
    },
    ["Coraza de plumas"] = {
        Professions = 10647,
    },
    ["Ira de Thule"] = {
        ["Melegruños"] = 3387,
    },
    ["Encantar ágata azur"] = {
        ["Aprendiz Kryten"] = 4319,
    },
    ["Sacudida de llamas"] = {
        ["Profeta Filo Ardiente"] = 9658,
    },
    ["Cohete, ROJO"] = {
        ["Juerguista de Ventormenta"] = 26347,
        ["Juerguista de Cima del Trueno"] = 26347,
        ["Fuegos artificiales de Pat ROJO"] = 26347,
        ["Bufón de Darnassus"] = 26347,
        ["Bufón de Forjaz"] = 26347,
        ["Bufón de Entrañas"] = 26347,
        ["Juerguista de Orgrimmar"] = 26347,
    },
    ["Sombrero de cuero cómodo"] = {
        Professions = 10490,
    },
    ["Humillo"] = {
        Professions = 15633,
    },
    ["Mono azul"] = {
        Professions = 7639,
    },
    ["Sanación relámpago"] = {
        ["PRIEST"] = 10917,
    },
    ["Golpe marchito"] = {
        ["Lodobestia Parramustia"] = 5337,
        ["Protector marchivo"] = 5337,
        ["Arei"] = 5337,
        ["Traidor de Villa Darrow"] = 5337,
        ["Traidor espectral"] = 5337,
    },
    ["Pacificar"] = {
        ["Traje de seguridad de Pacificador"] = 10730,
    },
    ["Guanteletes de escórpido pesados"] = {
        Professions = 19064,
    },
    ["Brazales de escamas de tortuga"] = {
        Professions = 10518,
    },
    ["Campo de reflejo"] = {
        ["Nulificador X21 Arcano"] = 10831,
    },
    ["Interceptar"] = {
        ["WARRIOR"] = 20617,
    },
    ["Señor de las nieves 9000"] = {
        Professions = 21940,
    },
    ["Pantalones de paño cenizo"] = {
        Professions = 18434,
    },
    ["Abrazo de Gaia"] = {
        Professions = 28210,
    },
    ["Presura encantada"] = {
        ["Encantador Defias"] = 3443,
        ["Encantadora Látigo de Ira"] = 3443,
        ["Mith'rethis el Encantador"] = 3443,
    },
    ["Oscurecer visión"] = {
        ["Duende oscuro"] = 5514,
    },
    ["Pacto de sangre"] = {
        Pet = 11767,
    },
    ["Alimentar mascota"] = {
        ["HUNTER"] = 6991,
    },
    ["Fuerza de Agamaggan"] = {
        ["Quebrantadientes"] = 16612,
    },
    ["Encantar brazales: aguante"] = {
        Professions = 13648,
    },
    ["Fragmentos óseos"] = {
        ["Esqueleto deambulante"] = 17014,
    },
    ["Veneno entorpecedor"] = {
        ["ROGUE"] = 3420,
    },
    ["Barrera de Nefarian"] = {
        ["Lord Víctor Nefarius"] = 22663,
    },
    ["Restauración oscura"] = {
        ["Guardialobo Colmillo Oscuro"] = 7106,
    },
    ["Vara dorada"] = {
        Professions = 14379,
    },
    ["Descarga Arcana cargada"] = {
        ["Príncipe Raze"] = 16570,
    },
    ["Roca"] = {
        ["Cazavermis Rotapolvo"] = 9483,
    },
    ["Encantar escudo: espíritu superior"] = {
        Professions = 13905,
    },
    ["Cinta de cuero rúnico"] = {
        Professions = 19082,
    },
    ["Leotardos de escórpido pesados"] = {
        Professions = 19075,
    },
    ["Vestido de lana blanco"] = {
        Professions = 8467,
    },
    ["Toxina fangosa"] = {
        ["Barroso de jade"] = 6814,
    },
    ["Reconstruir ensamblaje"] = {
        ["Formajol de tierra"] = 10260,
    },
    ["Jubón de cuero estampado"] = {
        Professions = 2160,
    },
    ["Poción de sangre de trol poderosa"] = {
        Professions = 3451,
    },
    ["Torio encantado"] = {
        Professions = 17180,
    },
    ["Encantar botas: espíritu inferior"] = {
        Professions = 13687,
    },
    ["Elixir de Visión onírica"] = {
        Professions = 11468,
    },
    ["Ofrenda oscura"] = {
        ["Abisario de Arugal"] = 7154,
    },
    ["Tótem de Aire sosegado"] = {
        ["SHAMAN"] = 25908,
    },
    ["Maldición del Magram caído"] = {
        ["Espectro Magrami"] = 18159,
    },
    ["Puntos de honor +228"] = {
        ["Belisario de la Horda"] = 24963,
        ["Brigadier general de la Alianza"] = 24963,
    },
    ["Varita mágica inferior"] = {
        Professions = 14293,
    },
    ["Marca de las llamas"] = {
        ["Bruto Pirotigma"] = 15128,
        ["Legionario Pirotigma"] = 15128,
        ["Tejeoscuro Pirotigma"] = 15128,
        ["Conjurador Pirotigma"] = 15128,
        ["Tejedor de tienblas Pirotigma"] = 15128,
        ["Pirómano Pirotigma"] = 15128,
    },
    ["Cinturón de cuero cosido a mano"] = {
        Professions = 3753,
    },
    ["Pantalones de tejido de Escarcha"] = {
        Professions = 18424,
    },
    ["Desaparición de Brasadivino"] = {
        ["Piroguardián brasadivino"] = 16078,
    },
    ["Punición Sagrada"] = {
        ["Oráculo múrloc"] = 9734,
        ["Sirena Escama Tormentosa"] = 9734,
        ["Sacerdote de Dun Garok"] = 9734,
        ["Presertor de Theramonte"] = 9734,
        ["Buscador Renegado"] = 9734,
        ["Discípulo Escarlata"] = 9734,
        ["Adepto Escarlata"] = 9734,
        ["Clérico de Nethergarde"] = 9734,
        ["Sacerdote expedicionario"] = 9734,
        ["Supervisor Minafría"] = 15498,
        ["Perito Ferrohondo"] = 15498,
        ["Perito Minafría veterano"] = 15498,
        ["Perito veterano de Ferrohondo"] = 15498,
        ["Supervisor Campeón Ferrohondo"] = 15498,
    },
    ["Alarido psíquico"] = {
        ["PRIEST"] = 10890,
    },
    ["Revancha"] = {
        ["WARRIOR"] = 25288,
        ["Guardia de Ventormenta"] = 12170,
        ["Dal Zarpasangrante"] = 12170,
        ["Defensor de Pico Tormenta"] = 19130,
        ["Guardián Lobo Gélido"] = 19130,
        ["Defensor de estación"] = 19130,
        ["Guardián de estación"] = 19130,
        ["Defensor veterano"] = 19130,
        ["Guardián veterano"] = 19130,
        ["Guardián Campeón"] = 19130,
        ["Defensor Campeón"] = 19130,
        ["Cabo Noreg Pico Tormenta"] = 19130,
    },
    ["Encantar guantes: desuello"] = {
        Professions = 13698,
    },
    ["Crear falla corazón de Hakkar"] = {
        ["Corazón de Hakkar"] = 24202,
    },
    ["Capa de gran vudú"] = {
        Professions = 10562,
    },
    ["Cinturón del archimago"] = {
        Professions = 22866,
    },
    ["Guarda de la pereza"] = {
        ["Guardia de Laze"] = 3827,
        ["Médico brujo Mosh'Ogg"] = 3827,
    },
    ["El Despedazador"] = {
        Professions = 10003,
    },
    ["Poción restauradora"] = {
        Professions = 11452,
    },
    ["Intelecto Arcano"] = {
        ["MAGE"] = 10157,
    },
    ["Jubón de lino marrón"] = {
        Professions = 2385,
    },
    ["Brazales de cuero de guardián"] = {
        Professions = 3777,
    },
    ["Descarga débil de hielo"] = {
        ["Novicio Peloescarcha"] = 6949,
    },
    ["Sobrehombros azur"] = {
        Professions = 8795,
    },
    ["Jubón quimérico"] = {
        Professions = 19081,
    },
    ["Mente mácula"] = {
        ["Mago maldito"] = 16567,
    },
    ["Bengala"] = {
        ["HUNTER"] = 1543,
    },
    ["Chafarote de ébano"] = {
        Professions = 10013,
    },
    ["Invocar furibundo flamante"] = {
        ["Furibundo flamante"] = 15711,
        ["Terrorspark"] = 15711,
    },
    ["Telas envolventes"] = {
        ["Suma Sacerdotisa Mar'li"] = 24110,
    },
    ["Nova Sagrada"] = {
        ["PRIEST"] = 27801,
    },
    ["Escudo de Fuego"] = {
        Pet = 11771,
        ["Mago renegado Defias"] = 134,
        ["Geomántico Ratatúnel"] = 134,
        ["Vigilante Dalaran"] = 134,
        ["Mago Dalaran"] = 134,
        ["Manifestación de fuego menor"] = 134,
        ["Geomántico Mostacho Blanco"] = 18968,
    },
    ["Botas vid de sangre"] = {
        Professions = 24093,
    },
    ["Taladora Hierro Negro"] = {
        Professions = 15294,
    },
    ["Camisa de lino verde"] = {
        Professions = 2396,
    },
    ["Nube de relámpagos"] = {
        ["Agitadora Kolkar"] = 6535,
        ["Clamalluvias Sombramatorral"] = 6535,
        ["Mavoris Quiebranubes"] = 6535,
        ["Bruja de la Tormenta Furia Sangrienta"] = 6535,
        ["Agitadora Galak"] = 6535,
        ["Tormento Maggram"] = 6535,
        ["Hermana Rathtalon"] = 6535,
        ["Videntormento Kolkar"] = 6535,
    },
    ["Saliva tóxica"] = {
        ["Necrófago esclavizante"] = 7125,
    },
    ["Botas Argenta"] = {
        Professions = 23664,
    },
    ["Seccionar"] = {
        ["WARRIOR"] = 7373,
        ["Tentáculo Garral"] = 26141,
        ["Guerrero esquelético"] = 9080,
        ["Brack"] = 9080,
        ["Guerrero Branquiazul"] = 9080,
        ["Lok Aterraorcos"] = 9080,
        ["Ursa Maderiza"] = 9080,
        ["Aligar el Torturador"] = 9080,
        ["Guerrero Strashaz"] = 9080,
        ["Guerrero Filozante"] = 9080,
        ["Supervisor Asas"] = 9080,
        ["Paria loco"] = 9080,
        ["Invasor Minafría"] = 9080,
        ["Guardia Real del Terror"] = 9080,
    },
    ["Capucha de encantador"] = {
        Professions = 3857,
    },
    ["Seducción"] = {
        Pet = 6358,
    },
    ["Aguante corrupto"] = {
        ["Vigilante del cubil Maderiza"] = 6819,
        ["Pataroble"] = 6819,
    },
    ["Poción de ira"] = {
        Professions = 6617,
    },
    ["Evocación"] = {
        ["MAGE"] = 12051,
    },
    ["Gafas ojo brillante"] = {
        Professions = 12587,
    },
    ["Crear piedra de alma (superior)"] = {
        ["WARLOCK"] = 20756,
    },
    ["Maldición de los Almaumbría"] = {
        ["Sátiro Lobrecuore"] = 6946,
        ["Tramposo Almaumbría"] = 6946,
        ["Acechasombras Lobrecuore"] = 6946,
        ["Clamainferno Lobrecuore"] = 6946,
    },
    ["Yelmo de escórpido pesado"] = {
        Professions = 19088,
    },
    ["Cresa babosa"] = {
        ["Larva carroñera"] = 16449,
        ["Devorador carroñero"] = 16449,
        ["Gusano infestado"] = 16449,
    },
    ["Mella profunda"] = {
        ["Devastador silítido"] = 8255,
        ["Razelikh el Rapiñador"] = 8255,
    },
}
